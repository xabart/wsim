C
C               DIRECT LINEAR SYSTEM SOLVER
C
C--------------------------------------------------------------------
      SUBROUTINE SOLVE(A,B,NOMM2)
C--------------------------------------------------------------------
C0  SOLVEF    SOLVE
C1  PURPOSE   Compute the solution Xj of linear system Aij * Xj = Bi
C1            by the Kaletsky method (general full nonsym. matrix)
C2  CALL      CALL SOLVE(A,B)
C3  CALL ARG. A(NOM,NOM)          = system matrix
C3            B(NOM)              = system load vector
C3            IFLAG               = matrix elimination or not (1/2)
C4  RET. ARG. B(NOM)
C6  INT.CALL  ERRORS
CE  ERRORS    01= Singular matrix(first diag. component)
CE            02= Singular matrix(one encountered diagonal comp.)
C9  Jan. 99   S. GRILLI, INLN
CLSOLVE SUB. COMPUTES SOLUTION OF LINEAR SYSTEM FOR THE 3D-BEM
C-----------------------------------------------------------------------
       IMPLICIT REAL*8 (A-H,O-Z)


       include 'param.inc'

       CHARACTER*8 TEXTE1,TEXTE2
C
       DIMENSION A(NOMM2,NOM),B(NOM)
C
       COMMON/DATGEN/  ZL0,W0,H0,X0,D0,MX,MY,MZ,NX,NY,NZ,NSUBC,
     &                NINTR,NOM,MOM
       include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
c---------------------------------------------------------------------------
c	Old version
c      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
c      COMMON /SYSTEM/ DUM(31+8*MYMAX),IDU,IFLAG,IDW(3)
c---------------------------------------------------------------------------
C
       DATA ZERO/0.D0/,EPS/1.D-10/,TEXTE1/'SOLVE 01'/,TEXTE2/'SOLVE 02'/
C
C     SAVE
C     SAVE /DATGEN/,/SYSTEM/
C
       IF(IFLAG.EQ.1) THEN
C
C        Matrix elimination (KALETSKI)
C
         SP = A(1,1)
         IF(DABS(SP).LT.EPS) THEN
            CALL ERRORS(TEXTE1)
C           -----------
         END IF
C
         DO J=2,NOM
            A(1,J) = A(1,J)/SP
         END DO
         NM = NOM - 1
C
         DO J=2,NM
            JP = J + 1
            JM = J - 1
C
            DO I=J,NOM
               S = ZERO
               DO K=1,JM
                  S = S + A(I,K)*A(K,J)
               END DO
               A(I,J) = A(I,J) - S
            END DO
C
            IF(DABS(A(J,J)).LE.EPS) THEN
               WRITE(*,*) J,A(J,J)
               CALL ERRORS(TEXTE2)
C              -----------
            END IF
C
            DO I=JP,NOM
               R = ZERO
               DO K=1,JM
                  R = R + A(J,K)*A(K,I)
               END DO
               A(J,I) = (A(J,I) - R)/A(J,J)
            END DO
         END DO
         Q = ZERO
C
         DO K=1,NM
            Q = Q + A(NOM,K)*A(K,NOM)
         END DO
         A(NOM,NOM) = A(NOM,NOM) - Q
      END IF
C
C     Load vector back substitution
C
      B(1) = B(1)/A(1,1)
C
      DO K=2,NOM
         KM = K - 1
         S  = ZERO
         DO KK=1,KM
            S = S + A(K,KK)*B(KK)
         END DO
         B(K) = (B(K) - S)/A(K,K)
      END DO
C
      DO K=1,NM
         I = NOM - K
         R = ZERO
         DO KK=1,K
            MM = I + KK
            R  = R + A(I,MM)*B(MM)
         END DO
         B(I) = B(I) - R
      END DO
C
      RETURN
C
      END
