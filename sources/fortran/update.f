C
C                  UPDATING SUBROUTINES
C
C----------------------------------------------------------------------
      SUBROUTINE DUDTPR(PHI,PHIN,PHIS,PHIM,PHISS,PHIMM,PHISM,PHINS,
     .                  PHINM,UVW)  
C----------------------------------------------------------------------  
C0  DUDTPRF   DUDTPR  
C1  PURPOSE   Computes current (u,v,w) values over the boundary as funct.  
C1            of known (Phis,Phim,Phin). Also compute Phiss,Phimm,Phins,
C1            Phinm, for later use. 
C1  
C2  CALL      CALL DUDTPR(PHI,PHIN,PHIS,PHIM,PHISS,PHIMM,PHISM,PHINS,
C2                        PHINM,UVW) 
C3  CALL ARG. PHI(NOM),PHIN(NOM)  = Potential and norm. grad. (Phi,Phin)  
C3            /FSLID/,/TPLID/     = SLIDING functions
C3
C4  RET. ARG. PHIS,PHISS(NOM),... = Potential s,m-derv (Phis,Phiss,...)  
C4            UVWEL(NOM,3)        = Calcul. vel. for curr. step (K=1-6)  
C5  
C10 Feb 1999  S. GRILLI, INLN 
CLDUDTPR SUB. WHICH COMPUTES VELOCITIES AND PREDICT ACCELERATIONS  
C-----------------------------------------------------------------------  
      IMPLICIT REAL*8 (A-H,O-Z) 
      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM1(5),IDUM1(8),NOM,MOM
      COMMON/MAILLE/  DUM2(NOMM*3+MOMM*2+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7+2*NOMM+66),
     .                INODF(6,2),IBCOND(6)
      COMMON /FSLID/ FI0XE(NDM,NDM),FI1XE(NDM,NDM),FI2XE(NDM,NDM),
     .               FISL(NDM2,NDM,NDM),FISLX(NDM2,NDM,NDM),
     .               FISLE(NDM2,NDM,NDM),FISLXX(NDM2,NDM,NDM),
     .               FISLEE(NDM2,NDM,NDM),FISLXE(NDM2,NDM,NDM),
     .               ISLCON(NDM2,NOMM),IXIET(2,NOMM)
      COMMON /TPLID/ VS(NOMM,3),VM(NOMM,3),VN(NOMM,3),DVS(NOMM),
     .               DVM(NOMM),DVN(NOMM),DUM3(12*NOMM),XKA(NOMM)
      COMMON /BOTFCT/ DUMBOT(NXM*NYM+2),IBOT
      COMMON /CGAGE/  DUMGAG(17*NGAM),IDUGAG(NGAM),NGA,INTZED
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION PHI(NOM),PHIN(NOM),PHIS(NOM),PHIM(NOM),PHISS(NOM),
     .          PHIMM(NOM),PHISM(NOM),PHINS(NOM),PHINM(NOM),UVW(3,NOM)
C
      DATA ZERO/0.D0/,ONE/1.D0/
C
       SAVE
C     SAVE /DATGEN/,/MAILLE/,/FSLID/,/TPLID/
C
C.....Loop over the 6 or 5 boundaries  (image method)
C
CSYM...
      KFMAX = 6
      IF(IBOT.EQ.10) KFMAX = KFMAX-1
      IF(ISYM.EQ.1) KFMAX = KFMAX-1
      DO KF=1,KFMAX
         IF((IBCOND(KF).NE.0).OR.(NGA.NE.0.AND.KF.EQ.KFMAX)) THEN   
            DO IK=INODF(KF,1),INODF(KF,2)
               PKA = XKA(IK)
               SKA = ONE/(ONE - PKA*PKA)
C
C           ...Node IK location in 5x5 grid
C
               IX = IXIET(1,IK)
               KE = IXIET(2,IK)
C
C              Phis,Phim are computed for fixing Dirichlet B.C. for Phit.
C              Phiss,Phimm,Phins,Phinm are computed for correcting
C              the advection on the moving Neuman b.c.
C
               PHIS(IK)   = ZERO
               PHIM(IK)   = ZERO
               PHISS(IK)  = ZERO
               PHIMM(IK)  = ZERO
               PHISM(IK)  = ZERO
               PHINS(IK)  = ZERO
               PHINM(IK)  = ZERO
C
               DO J=1,NDM2
	          K = ISLCON(J,IK)
                  PHIS(IK)  = PHIS(IK)  + FISLX(J,IX,KE)*PHI(K)
                  PHIM(IK)  = PHIM(IK)  + FISLE(J,IX,KE)*PHI(K)
                  PHISS(IK) = PHISS(IK) + FISLXX(J,IX,KE)*PHI(K)
                  PHIMM(IK) = PHIMM(IK) + FISLEE(J,IX,KE)*PHI(K)
                  PHISM(IK) = PHISM(IK) + FISLXE(J,IX,KE)*PHI(K)
                  PHINS(IK) = PHINS(IK) + FISLX(J,IX,KE)*PHIN(K)
                  PHINM(IK) = PHINM(IK) + FISLE(J,IX,KE)*PHIN(K)
               END DO
C
C              Division by scale factors
C
               PHIS(IK)  = PHIS(IK)/DVS(IK)
               PHIM(IK)  = PHIM(IK)/DVM(IK)
               PHISS(IK) = PHISS(IK)/(DVS(IK)*DVS(IK))
               PHIMM(IK) = PHIMM(IK)/(DVM(IK)*DVM(IK))
               PHISM(IK) = PHISM(IK)/(DVS(IK)*DVM(IK))
               PHINS(IK) = PHINS(IK)/DVS(IK)
               PHINM(IK) = PHINM(IK)/DVM(IK)
C
C              Cartesian velocities (u,w)
C
               DO K=1,3
	          UVW(K,IK) = SKA*(PHIS(IK) - PKA*PHIM(IK))*VS(IK,K) +
     .                     SKA*(PHIM(IK) - PKA*PHIS(IK))*VM(IK,K) +
     .                     PHIN(IK)*VN(IK,K)
               END DO
            END DO
	 END IF
      END DO
C
      RETURN
C
      END
C
C----------------------------------------------------------------------
      SUBROUTINE PREFIX(PRES,PHIT,PHIN,PHIS,PHIM)
C----------------------------------------------------------------------
C0  PREFIXF   PREFIX
C1  PURPOSE   Computes Phit by Bernouilli on all Dirichlet boundaries,
C1            using the pressure.
C1            Both Phit and p are imposed to the free surface value on
C1            the 1st upper node of lateral boundaries.
C1
C1            Implementation of dissipation on the free
C1            surface by artificial pressure (IBREAK is TRUE) :
C1               PDAMP = CFUNC()*sign(PHIN)*PHIS**2, where
C1               CFUNC() = 0 IF x_s,x_m < TRESH ELSE
C1               CFUNC is fixed in  SUBROUT ROLLER()
C1
C1            Implementation of absorbing boundary (AB) on the free
C1            surface by artificial pressure (IFLAB is TRUE) :
C1               PDAMP = DNU*PHIN, where
C1               DNU = 0 IF X < XDAMP  ELSE
C1               DNU = DNU0*((X-XDAMP)/XLDAMP)^(NPOLY)
C1
C1            Implementation of surface tension pressure jump (ITEMS  is
C1            TRUE) :
C1               PSTEN = SUTENS*DBS
C1
C2  CALL PREFIX(PRES,PHIT,PHIN,PHIS,PHIM,PHISS,PHIMM)
C3  CALL ARG. PHIS,PHIM,PHIN()    = Curv. veloc. (Phis,Phim,Phin)
C3            RHO                 = Water specific mass
C3            GE                  = Acceleration of gravity
C3            PATM                = Atmospheric pressure
C3            IFLAB               = flag for AB existence
C3            IBREAK              = flag for roller existence
C3            ITENS               = flag for surface tension computations
C3
C4  RET. ARG. PRES,PHIT(NOM)      = Pressure and Pot t-derv (p,Phit)
C4
C6  INT. CALL ROLLER
C10 Feb. 99   S. Grilli, INLN
CLPREFIX SUB. WHICH COMPUTES PRESSURE AND POTENTIAL TIME DERIVATIVE
C-------------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

      LOGICAL*4 IFLAB,IBREAK,ITENS
C-------------------------------------------------------------------------------
      COMMON/DATGEN/  DUM1(5),IDUM1(8),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(MOMM*2+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7+2*NOMM+66),
     .                INODF(6,2),IBCOND(6)
      COMMON /TPLID/ VS(NOMM,3),VM(NOMM,3),VN(NOMM,3),DVS(NOMM),
     .               DVM(NOMM),DVN(NOMM),XSS(NOMM,3),XMM(NOMM,3),
     .               XSM(NOMM,3),VCSSN(NOMM),VCMMN(NOMM),VCSMN(NOMM),
     .               XKA(NOMM)
      COMMON /BOUNDC/ DUM3(5),RHO,PATM,DUM4,GE,DUM5(5),IDW
      COMMON /DISSIP/ XDAMP,XLDAMP,DNU0,TRESH,SUTENS,COEF(NOMM),
     .                COEFX(NOMM),ISLOP(NOMM),NPOLY,IFLAB,IBREAK,ITENS
      COMMON /OPTION/ ISYM,IPOST,IVECT
C-------------------------------------------------------------------------------
      DIMENSION PRES(NOM),PHIN(NOM),PHIS(NOM),PHIM(NOM),PHIT(NOM),
     .          XLP(3),XKP(3)

      DATA ZERO/0.D0/,EPS/1.D-06/,HALF/0.5D0/,FOUR/4.D0/,TWO/2.D0/,
     .     ONE/1.D0/,THREE/3.D0/,ITEMAX/20/,BIG/1.D06/
C-------------------------------------------------------------------------------
       SAVE

C-------------------------------------------------------------------------------
C.....Initializations on the free surface with Pa (K=1) (Pa cst can
C     be improved by reading exact atmospheric pressure variation)
C-------------------------------------------------------------------------------
      DO IK=INODF(1,1),INODF(1,2)
         PRES(IK) = PATM
      END DO
C-------------------------------------------------------------------------------
C.....Fix Phit boundary condition when Dirichlet condition (K=1,..,5)
C-------------------------------------------------------------------------------
CSYM
      KFMAX = 5
      IF(ISYM.EQ.1) KFMAX = KFMAX-1
      DO KF=1,KFMAX
         IF(IBCOND(KF).EQ.1) THEN
C-------------------------------------------------------------------------------
C           Dirichlet boundary condition
C-------------------------------------------------------------------------------
C           Contains pressure correct. for absorbing beach on free surf.
C           for surface tension on free surface and roller interface.
C-------------------------------------------------------------------------------
            DO IK=INODF(KF,1),INODF(KF,2)
               PKA = XKA(IK)
               SKA = ONE/(ONE - PKA*PKA)
C-------------------------------------------------------------------------------
C              Surface tension effect
C-------------------------------------------------------------------------------
               PSTEN = ZERO
               IF(ITENS) THEN
                  CURV  = VCSSN(IK) + VCMMN(IK)
                  PSTEN = SUTENS*CURV
C-------------------------------------------------------------------------------
C                 Calculate principal curvatures and vectors ! VOID
C-------------------------------------------------------------------------------
                  RR = DSQRT((VCSSN(IK) - VCMMN(IK))**2 - FOUR*
     .                 (VCSSN(IK)*VCMMN(IK) - VCSMN(IK)*VCSMN(IK)))
                  ZLAM1 = HALF*(VCSSN(IK) + VCMMN(IK) + RR)
                  ZLAM2 = HALF*(VCSSN(IK) + VCMMN(IK) - RR)
		  ZLAM  = DMIN1(ZLAM1,ZLAM2)
C-------------------------------------------------------------------------------
		  IF(ZLAM.LT.ZERO) THEN
C-------------------------------------------------------------------------------
C                    Eigenvector for min EV<0
C-------------------------------------------------------------------------------
		     EV1 = ONE
		     EV2 = (ZLAM - VCSSN(IK))/VCSMN(IK)
		     EVL = DSQRT(ONE + EV2*EV2)
		     EV1 = EV1/EVL
		     EV2 = EV2/EVL
C-------------------------------------------------------------------------------
C                    Principal slopes
C-------------------------------------------------------------------------------
		     DO K=1,3
		        XLP(K) =  EV1*VS(IK,K) + EV2*VM(IK,K)
		        XKP(K) = -EV2*VS(IK,K) + EV1*VM(IK,K)
		     END DO
                     SLL = XLP(3)/DSQRT(XLP(1)**2 + XLP(2)**2)
                     SLK = XKP(3)/DSQRT(XKP(1)**2 + XKP(2)**2)
		     SLM = DMIN1(SLL,SLM)
		  END IF
	       END IF
C-------------------------------------------------------------------------------
C              Abs. beach, effect of roller dissipation
C-------------------------------------------------------------------------------
               PDAMP  = ZERO
               IF (IFLAB.AND.(KF.EQ.1)) THEN
                  IF (XYZNOD(IK,1).GT.XDAMP) THEN
                     DXD   = XYZNOD(IK,1) - XDAMP
                     DNU   = DNU0*((DXD/XLDAMP)**(NPOLY))
                     PDAMP = PDAMP + PHIN(IK)*DNU
                  END IF
               END IF
C-------------------------------------------------------------------------------
               IF (IBREAK.AND.(KF.EQ.1)) THEN
C-------------------------------------------------------------------------------
C                 s and m slopes as a first test
C-------------------------------------------------------------------------------
                  SLS = VS(IK,3)/DSQRT(VS(IK,1)**2 + VS(IK,2)**2)
                  SLM = VM(IK,3)/DSQRT(VM(IK,1)**2 + VM(IK,2)**2)
                  SLT = DMIN1(SLS,SLM)
		  ISLOP(IK) = 0
		  WRITE(6,*) IK,SLT
C-------------------------------------------------------------------------------
C                 Calculate maximum horizontal slope of FS
C-------------------------------------------------------------------------------
                  IF (SLT.LE.(-HALF*TRESH)) THEN
                     XSM12 = VS(IK,1)*VM(IK,1) + VS(IK,2)*VM(IK,2)
                     XSM13 = VS(IK,1)*VM(IK,3) + VS(IK,3)*VM(IK,1)
                     XSM23 = VS(IK,2)*VM(IK,3) + VS(IK,3)*VM(IK,2)
                     XS2   = VS(IK,1)*VS(IK,1) + VS(IK,2)*VS(IK,2)
                     XM2   = VM(IK,1)*VM(IK,1) + VM(IK,2)*VM(IK,2)
                     XM13  = VM(IK,1)*VM(IK,3)
                     XM23  = VM(IK,2)*VM(IK,3)
                     XS13  = VS(IK,1)*VS(IK,3)
                     XS23  = VS(IK,2)*VS(IK,3)
C-------------------------------------------------------------------------------
		     CA = (XM2*XMM(IK,3) - XM13*XMM(IK,1)
     .                     - XM23*XMM(IK,2)) / DVS(IK)**4
		     CB = TWO*(XSM12*XMM(IK,3) + XM2*XSM(IK,3) - XM13*
     .                 XSM(IK,1) - HALF*XSM13*XMM(IK,1) - XM23*XSM(IK,2)
     .                 -HALF*XSM23*XMM(IK,2)) / (DVM(IK) * DVS(IK)**3)
		     CC = (XS2*XMM(IK,3) + XM2*XSS(IK,3) + FOUR*XSM12*
     .                 XSM(IK,3) - XM13*XSS(IK,1) - XS13*XMM(IK,1) -TWO*
     .                 XSM13*XSM(IK,1) - XM23*XSS(IK,2) - XS23*XMM(IK,2)
     .                 -TWO*XSM23*XSM(IK,2)) / (DVS(IK)**2 * DVM(IK)**2)
		     CD = TWO*(XS2*XSM(IK,3) + XSM12*XSS(IK,3) -
     .                    HALF*XSM13*XSS(IK,1) - XS13*XSM(IK,1)
     .                   -HALF*XSM23*XSS(IK,2) - XS23*XSM(IK,2)) /
     .                    (DVM(IK)**3 * DVS(IK))
		     CE = (XS2*XSS(IK,3) - XS13*XSS(IK,1) -
     .                     XS23*XSS(IK,2)) / DVM(IK)**4
C-------------------------------------------------------------------------------
                     TG0 = SLT
		     DTG = BIG
		     ITE = 0
C-------------------------------------------------------------------------------
                     DO WHILE((ITE.LE.ITEMAX).AND.(DABS(DTG).GT.EPS))
		        FCT = CE + TG0*(CD + TG0*(CC + TG0*(CB+CA*TG0)))
			DFC = CD + TG0*(TWO*CC + TG0*(THREE*CB +
     .                             FOUR*CA*TG0))
                        DTG = -FCT/DFC
			TG0 = TG0 + DTG
			ITE = ITE + 1
		     END DO
		     WRITE(6,*) IK,TG0
C-------------------------------------------------------------------------------
C                    Front slopes of breakers
C-------------------------------------------------------------------------------
                     IF (TG0.LE.(-TRESH)) THEN
		        ISLOP(IK) = 1
 		        CALL ROLLER(IK,TG0)
C                       -----------
                        COEF(IK) = ONE
                        PDAMP = PDAMP - COEF(IK)*PHIN(IK)
                     END IF
		  END IF
               END IF
C-------------------------------------------------------------------------------
               PRES(IK) = PRES(IK) + PDAMP - PSTEN
C-------------------------------------------------------------------------------
C              Set boundary condition for phit
C-------------------------------------------------------------------------------
               PHIT(IK) = -HALF * ( SKA*( PHIS(IK)*PHIS(IK) +
     .                                    PHIM(IK)*PHIM(IK) -
     .                                    TWO*PKA*PHIS(IK)*PHIM(IK) ) 
     .                           + PHIN(IK)*PHIN(IK)) -
     .                     GE*XYZNOD(IK,3) - PRES(IK)/RHO
            END DO
         END IF
      END DO
C-------------------------------------------------------------------------------
      RETURN
C-------------------------------------------------------------------------------
      END
C-------------------------------------------------------------------------------






C----------------------------------------------------------------------
      SUBROUTINE ROLLER(IK,SLOPE)
C----------------------------------------------------------------------
C
C     Absorbing pressure coefficient for slective absorbtion of breaking
C     wave energy similar to a roller.
C
C     SLOPE    = Maximum negative surface slope calculated at point IK
C     TRESH    = Slope treshhold fixed in input data
C     COEF(),COEFX() = pressure coef and d/dx of coeff.
C
C     S. Grilli, 3/99, INLN
C
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM1(5),IDUM1(8),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(MOMM*2+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7+2*NOMM+84)
      COMMON /TPLID/ VS(NOMM,3),VM(NOMM,3),VN(NOMM,3),DVS(NOMM),
     .               DVM(NOMM),DVN(NOMM),XSS(NOMM,3),XMM(NOMM,3),
     .               XSM(NOMM,3),VCSSN(NOMM),VCMMN(NOMM),VCSMN(NOMM),
     .               XKA(NOMM)
      COMMON /BOUNDC/ DUM3(5),RHO,PATM,DUM4,GE,DUM5(5),IDW
      COMMON /DISSIP/ DUM6(3),TRESH,DUM7,COEF(NOMM),COEFX(NOMM),
     .                IDUM3(NOMM+4)
C
      DATA ONE/1.D0/,ZERO/0.D0/
C
      COEF(IK)  = ONE + (TRESH + SLOPE)/TRESH
      COEFX(IK) = ZERO
C
      RETURN
C
      END
C
C-------------------------------------------------------------------------------
      SUBROUTINE D2UTPR(PHIT,PHITS,PHITM,PHITN,PHIS,PHIM,PHIN,PHINS,
     .                  PHINM,PHISS,PHIMM,PHISM,DUVWDT,PHINN)
C-------------------------------------------------------------------------------
C0  D2UTPRF   D2UTPR
C1  PURPOSE   Computes values of the acceleration (Du/Dt,Dv/Dt,Dw/Dt)
C1            based on the kinematics, as a function of (Phis,Phim,
C1            Phin,Phits,Phitm,Phitn,Phiss,Phimm,Phism,Phins,Phinm)
C1            from the BEM solution for (Phi,Phin),(Phit,Phitn) and the
C1            s,m-derivatives on the boundary.
C1
C2  CALL D2UTPR(PHIT,PHITS,PHITM,PHITN,PHIS,PHIM,PHIN,PHINS,
C2              PHINM,PHISS,PHIMM,PHISM,DUVWDT,PHINN)
C3  CALL ARG. DUVWDT(3,NOM)       = Accelera. for current step
C3
C4  RET. ARG. DUVWDT(3,NOM)       = current accelerat. (K=1,..,5)
C4            PHITS,PHITM         = Other s,m-deriv. (Phits,Phitm)
C4
C10 Feb. 99   S. Grilli,INLN
CCLD2UTPR SUB. WHICH COMPUTES ACCELERATION AND PRESSURE D/DT
C-------------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM1(5),IDUM1(8),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(MOMM*2+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7+2*NOMM+66),
     .                INODF(6,2),IBCOND(6)
      COMMON /FSLID/ FI0XE(NDM,NDM),FI1XE(NDM,NDM),FI2XE(NDM,NDM),
     .               FISL(NDM2,NDM,NDM),FISLX(NDM2,NDM,NDM),
     .               FISLE(NDM2,NDM,NDM),FISLXX(NDM2,NDM,NDM),
     .               FISLEE(NDM2,NDM,NDM),FISLXE(NDM2,NDM,NDM),
     .               ISLCON(NDM2,NOMM),IXIET(2,NOMM)
      COMMON /TPLID/ VS(NOMM,3),VM(NOMM,3),VN(NOMM,3),DVS(NOMM),
     .               DVM(NOMM),DVN(NOMM),XSS(NOMM,3),XMM(NOMM,3),
     .               XSM(NOMM,3),VCSSN(NOMM),VCMMN(NOMM),VCSMN(NOMM),
     .               XKA(NOMM)
      COMMON /BOUNDC/ DUM3(5),RHO,PATM,DUM4,GE,DUM5(5),IDW
      COMMON /OPTION/ ISYM,IPOST,IVECT
C-------------------------------------------------------------------------------
      DIMENSION PHIS(NOM),PHIM(NOM),PHIN(NOM),PHITS(NOM),
     .          PHITM(NOM),PHITN(NOM),PHISS(NOM),PHIMM(NOM),PHISM(NOM),
     .          PHIT(NOM),PHINS(NOM),PHINM(NOM),DUVWDT(3,NOM),
     .          PHINN(NOM)
C-------------------------------------------------------------------------------
      DATA ZERO/0.D0/,ONE/1.D0/,TWO/2.D0/,THREE/3.D0/
C-------------------------------------------------------------------------------
       SAVE

C-------------------------------------------------------------------------------
C.....Loop over the 5 first boundaries
C-------------------------------------------------------------------------------
CSYM
      KMAX=5
      IF (ISYM.EQ.1) THEN
        KMAX=KMAX-1
      END IF
C-------------------------------------------------------------------------------
      DO KF=1,KMAX
         IF(IBCOND(KF).EQ.1) THEN
            DO IK=INODF(KF,1),INODF(KF,2)
               PKA  = XKA(IK)
               PKA2 = PKA*PKA
               PKA3 = PKA*PKA2
               SKA  = ONE/(ONE - PKA2)
               SKA2 = SKA*SKA
               SKA3 = SKA*SKA2
               TKA  = ONE + PKA2
               TKA2 = ONE + THREE*PKA2
               TKA3 = THREE + PKA2
C-------------------------------------------------------------------------------
C           ...Node IK location in 5x5 grid
C-------------------------------------------------------------------------------
               IX = IXIET(1,IK)
               KE = IXIET(2,IK)
C-------------------------------------------------------------------------------
C              Phits,Phitm are computed for claculating acc. on Dirich
C-------------------------------------------------------------------------------
               PHITS(IK) = ZERO
               PHITM(IK) = ZERO
C-------------------------------------------------------------------------------
               DO J=1,NDM2
	          K = ISLCON(J,IK)
                  PHITS(IK) = PHITS(IK) + FISLX(J,IX,KE)*PHIT(K)
                  PHITM(IK) = PHITM(IK) + FISLE(J,IX,KE)*PHIT(K)
               END DO
C-------------------------------------------------------------------------------
C              Division by scale factors
C-------------------------------------------------------------------------------
               PHITS(IK)  = PHITS(IK)/DVS(IK)
               PHITM(IK)  = PHITM(IK)/DVM(IK)
C-------------------------------------------------------------------------------
C              Curvature and torsion terms for IK
C-------------------------------------------------------------------------------
               CSSS = ZERO
               CSSM = ZERO
	       CMMS = ZERO
	       CMMM = ZERO
	       CSMS = ZERO
	       CSMM = ZERO
               DO K=1,3
                  CSSS = CSSS + XSS(IK,K)*VS(IK,K)
                  CSSM = CSSM + XSS(IK,K)*VM(IK,K)
	          CMMS = CMMS + XMM(IK,K)*VS(IK,K)
	          CMMM = CMMM + XMM(IK,K)*VM(IK,K)
	          CSMS = CSMS + XSM(IK,K)*VS(IK,K)
	          CSMM = CSMM + XSM(IK,K)*VM(IK,K)
	       END DO
               CSSS = CSSS/DVS(IK)**2
               CSSM = CSSM/DVS(IK)**2
	       CMMS = CMMS/DVM(IK)**2
	       CMMM = CMMM/DVM(IK)**2
	       CSMS = CSMS/(DVS(IK)*DVM(IK))
	       CSMM = CSMM/(DVS(IK)*DVM(IK))
C-------------------------------------------------------------------------------
	       CSSN = VCSSN(IK)
	       CMMN = VCMMN(IK)
	       CSMN = VCSMN(IK)
C-------------------------------------------------------------------------------
               PHINN(IK) = -SKA*(PHISS(IK) + PHIMM(IK) -
     .                      TWO*PKA*PHISM(IK)) +
     .                      SKA2*PHIS(IK)*(CSSS + CMMS - PKA*CSSM -
     .                      PKA*CMMM - TWO*PKA*CSMS + TWO*PKA2*CSMM) +
     .                      SKA2*PHIM(IK)*(CSSM + CMMM - PKA*CSSS -
     .                      PKA*CMMS - TWO*PKA*CSMM + TWO*PKA2*CSMS) +
     .                      SKA*PHIN(IK)*(CSSN + CMMN - TWO*PKA*CSMN)
C-------------------------------------------------------------------------------
C              Cartesian accelerations (Du/Dt,Dv/Dt,Dw/Dt)
C-------------------------------------------------------------------------------
               DO K=1,3
                  CPS = SKA*(PHITS(IK) - PKA*PHITM(IK) +
     .                  PHIN(IK)*PHINS(IK) - PKA*PHIN(IK)*PHINM(IK)) +
     .                  SKA2*(PHIS(IK)*PHISS(IK) -
     .                  TWO*PKA*PHIS(IK)*PHISM(IK) +
     .                  PKA2*PHIS(IK)*PHIMM(IK) -
     .                  PKA*PHIM(IK)*PHIMM(IK) +
     .                  TKA*PHIM(IK)*PHISM(IK) -
     .                  PKA*PHIM(IK)*PHISS(IK)) +
     .                  SKA3*PHIS(IK)*PHIS(IK)*(-CSSS + PKA*CSSM +
     .                  TWO*PKA*CSMS - TWO*PKA2*CSMM -
     .                  PKA2*CMMS + PKA3*CMMM) +
     .                  SKA3*PHIS(IK)*PHIM(IK)*(TWO*PKA*CSSS -
     .                  TKA*CSSM - TKA2*CSMS + PKA*TKA3*CSMM +
     .                  PKA*TKA*CMMS - TWO*PKA2*CMMM)
                  CPS = CPS + SKA3*PHIM(IK)*PHIM(IK)*(-PKA2*CSSS +
     .                  PKA*CSSM + PKA*TKA*CSMS - TKA*CSMM -
     .                  PKA2*CMMS + PKA*CMMM) +
     .                  SKA2*PHIS(IK)*PHIN(IK)*(-PKA2*CSSN +
     .                  TWO*PKA*CSMN - PKA2*CMMN) +
     .                  SKA2*PHIM(IK)*PHIN(IK)*(PKA*CSSN -
     .                  TKA*CSMN + PKA3*CMMN)
                  CPM = SKA*(-PKA*PHITS(IK) + PHITM(IK) -
     .                  PKA*PHIN(IK)*PHINS(IK) + PHIN(IK)*PHINM(IK)) +
     .                  SKA2*(-PKA*PHIS(IK)*PHISS(IK) +
     .                  TKA*PHIS(IK)*PHISM(IK) -
     .                  PKA*PHIS(IK)*PHIMM(IK) + PHIM(IK)*PHIMM(IK) -
     .                  TWO*PKA*PHIM(IK)*PHISM(IK) +
     .                  PKA2*PHIM(IK)*PHISS(IK)) +
     .                  SKA3*PHIS(IK)*PHIS(IK)*(PKA*CSSS - PKA2*CSSM -
     .                  TKA*CSMS + PKA*TKA*CSMM +
     .                  PKA*CMMS - PKA2*CMMM) +
     .                  SKA3*PHIS(IK)*PHIM(IK)*(-TWO*PKA2*CSSS +
     .                  PKA*TKA*CSSM + PKA*TKA3*CSMS - TKA2*CSMM -
     .                  TKA*CMMS + TWO*PKA*CMMM)
                  CPM = CPM + SKA3*PHIM(IK)*PHIM(IK)*(PKA3*CSSS -
     .                  PKA2*CSSM - TWO*PKA2*CSMS + TWO*PKA*CSMM +
     .                  PKA*CMMS - CMMM) +
     .                  SKA2*PHIS(IK)*PHIN(IK)*(PKA3*CSSN -
     .                  TKA*CSMN + PKA*CMMN) +
     .                  SKA2*PHIM(IK)*PHIN(IK)*(-PKA2*CSSN +
     .                  TWO*PKA*CSMN - PKA2*CMMN)
                  CPN = PHITN(IK) + SKA*(PHIS(IK)*PHINS(IK) -
     .                  PKA*PHIS(IK)*PHINM(IK) -
     .                  PKA*PHIM(IK)*PHINS(IK) +
     .                  PHIM(IK)*PHINM(IK) - PHIN(IK)*PHISS(IK) +
     .                  TWO*PKA*PHIN(IK)*PHISM(IK) -
     .                  PHIN(IK)*PHIMM(IK)) +
     .                  SKA2*PHIS(IK)*PHIS(IK)*(CSSN -
     .                  TWO*PKA*CSMN + PKA2*CMMN) +
     .                  TWO*SKA2*PHIS(IK)*PHIM(IK)*(-PKA*CSSN +
     .                  TKA*CSMN - PKA*CMMN) +
     .                  SKA2*PHIM(IK)*PHIM(IK)*(PKA2*CSSN -
     .                  TWO*PKA*CSMN + CMMN)
                  CPN = CPN + SKA2*PHIS(IK)*PHIN(IK)*(CSSS -
     .                  PKA*CSSM - TWO*PKA*CSMS + TWO*PKA2*CSMM +
     .                  CMMS - PKA*CMMM) +
     .                  SKA2*PHIM(IK)*PHIN(IK)*(-PKA*CSSS + CSSM +
     .                  TWO*PKA2*CSMS - TWO*PKA*CSMM -
     .                  PKA*CMMS + CMMM) +
     .                  SKA*PHIN(IK)*PHIN(IK)*(CSSN -
     .                  TWO*PKA*CSMN + CMMN)
  	          DUVWDT(K,IK) = CPS*VS(IK,K) + CPM*VM(IK,K) +
     .                           CPN*VN(IK,K)
               END DO
            END DO
         END IF
      END DO
C-------------------------------------------------------------------------------
      RETURN
C-------------------------------------------------------------------------------
      END
C-------------------------------------------------------------------------------





C-----------------------------------------------------------------------
      SUBROUTINE DPRFIX(DPRDT,PHIS,PHIM,PHIN,PHINS,PHINM,PHITN,PHINN)
C-----------------------------------------------------------------------
C0  DPRFIXF   DPRFIX
C1  PURPOSE   Computes current D/Dt pres. on surface Dirichlet boundar.
C1            Implementation of dissipation on the free
C1            surface by artificial pressure gradient :
C1
C1            Implementation of absorbing boundary (AB) on the free
C1            surface by artificial pressure gradient (IFLAB is TRUE) :
C1               DPDAMP = DPDAMP + DNU*(PHIS()*PHINS() +
C1                       PHIM()*PHINM(IK) + PHIN()*PHINN()
C1                     + PHITN()) + DNUX*PHIN(IK)*(PHIS()*
C1                       VS(,1) + PHIM()*VM(,1))
C1               DXD = X(IB) - XDAMP
C1               DNU = 0 IF X(IB)< XDAMP  ELSE
C1               DNU = DNU0*(DXD/XLDAMP)^(NPOLY)
C1               DNUX= DNU0*DFLOAT(NPOLY)*((DXD)**(NPOLY-1))/
C1                                      (XLDAMP**(NPOLY))
C1
C1            Implementation of surface tension pressure jump :
C1               DPSTEN = SUTENS*DCURDT
C1
C2  CALL DPRFIX(DPRDT,PHIS,PHIM,PHIN,PHINS,PHINM,PHITN,PHINN)
C3
C3  CALL ARG. PHIS,PHINS(NOM),etc = Pot. and norm.grad. d/ds (Phis,Phin)
C3            DPATM               = Atmospheric pressure gradient
C3            IFLAB               = flag for AB existence
C3            XDAMP,XLDAMP,DNU0   = AB parameters
C3
C4  RET. ARG. DPRDT(NOM)          = D/Dt of pressure on Dir. B (K=1,..,5)
C4
C9  Feb.  99   S. Grilli, INLN
CLDPRFIX SUB. WHICH COMPUTES D/DT OF PRESSURE ON DIRICHLET BOUNDARIES
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      LOGICAL*4 IFLAB,IBREAK,ITENS
C
      COMMON/DATGEN/  DUM1(5),IDUM1(8),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(MOMM*2+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7+2*NOMM+66),
     .                INODF(6,2),IBCOND(6)
      COMMON /FSLID/ FI0XE(NDM,NDM),FI1XE(NDM,NDM),FI2XE(NDM,NDM),
     .               FISL(NDM2,NDM,NDM),FISLX(NDM2,NDM,NDM),
     .               FISLE(NDM2,NDM,NDM),FISLXX(NDM2,NDM,NDM),
     .               FISLEE(NDM2,NDM,NDM),FISLXE(NDM2,NDM,NDM),
     .               ISLCON(NDM2,NOMM),IXIET(2,NOMM)
      COMMON /TPLID/ VS(NOMM,3),VM(NOMM,3),VN(NOMM,3),DVS(NOMM),
     .               DVM(NOMM),DVN(NOMM),XSS(NOMM,3),XMM(NOMM,3),
     .               XSM(NOMM,3),VCSSN(NOMM),VCMMN(NOMM),VCSMN(NOMM),
     .               XKA(NOMM)
      COMMON /BOUNDC/ DUM3(5),RHO,DUM4,DPATM,GE,DUM5(5),IDW
      COMMON /DISSIP/ XDAMP,XLDAMP,DNU0,TRESH,SUTENS,COEF(NOMM),
     .                COEFX(NOMM),ISLOP(NOMM),NPOLY,IFLAB,IBREAK,ITENS
	COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION PHIS(NOM),PHIM(NOM),PHIN(NOM),PHITN(NOM),PHINN(NOM),
     .          PHINS(NOM),PHINM(NOM),DPRDT(NOM)
C
      DATA ZERO/0.D0/
C
       SAVE
C     SAVE /DATGEN//MAILLE/,/BOUNDC/,/FSLID/,/TPLID/,/DISSIP/
C
C.....Initializations on the free surface with Dpa/Dt (K=1) (as cst can
C     be improved by reading exact atmospheric pressure grad. variation)
C
      DO IK=INODF(1,1),INODF(1,2)
         DPRDT(IK) = DPATM
      END DO
C
C.....Fix Phit boundary condition when Dirichlet condition (K=1,..,5)
C
CSYM
      KFMAX=5
      IF (ISYM.EQ.1) KFMAX=KFMAX-1 
      DO KF=1,KFMAX
         IF(IBCOND(KF).EQ.1) THEN
C
C           Dirichlet boundary condition
C
C           Contains pressure correct. for absorbing beach on free surf.
C           for surface tension on free surface and roller interface.
C
            DO IK=INODF(KF,1),INODF(KF,2)
C
C              Surface tension effect
C
               DPSTEN = ZERO
               IF(ITENS) THEN
                  DCURDT  = 0
	          DO K=1,3
		     DCURDT = ZERO
		  END DO
                  DPSTEN = SUTENS*DCURDT
	       END IF
C
C              Abs. beach, effect of roller dissipation
C
               DPDAMP  = ZERO
               IF (IFLAB.AND.(K.EQ.1)) THEN
                  IF (XYZNOD(IK,1).GT.XDAMP) THEN
                     DXD   = XYZNOD(IK,1) - XDAMP
                     DNU   = DNU0*((DXD/XLDAMP)**(NPOLY))
                     DNUX  = DNU0*DFLOAT(NPOLY)*((DXD)**(NPOLY-1))/
     .                                          (XLDAMP**(NPOLY))
                     DPDAMP = DPDAMP + DNU*(PHIS(IK)*PHINS(IK) +
     .                        PHIM(IK)*PHINM(IK) + PHIN(IK)*PHINN(IK)
     .                      + PHITN(IK)) + DNUX*PHIN(IK)*(PHIS(IK)*
     .                        VS(IK,1) + PHIM(IK)*VM(IK,1))
                  END IF
               END IF
C
               IF (IBREAK.AND.(K.EQ.1).AND.ISLOP(IK).EQ.1) THEN
                  DPDAMP = DPDAMP + COEF(IK)*(PHIS(IK)*PHINS(IK) +
     .                     PHIM(IK)*PHINM(IK) + PHIN(IK)*PHINN(IK)
     .                   + PHITN(IK)) + COEFX(IK)*PHIN(IK)*(PHIS(IK)*
     .                     VS(IK,1) + PHIM(IK)*VM(IK,1))
               END IF
C
               DPRDT(IK) = DPRDT(IK) + DPDAMP - DPSTEN
            END DO
         END IF
      END DO
C
      RETURN
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE UPDTDB(PHI,PHIT,UVW,DUVWDT,DPRDT)
C-----------------------------------------------------------------------
C0  UPDTDBF   UPDTDB
C1  PURPOSE   Update the geometry of the moving Dirichlet boundaries
C1            based on a second order Taylor expansion in time using the
C1            already known kinematics (u,v,w),(Du/Dt,Dv/Dt,Dw/Dt).
C1            The potential is identically updated based of the 2nd ord.
C1            expans. derived as function of (p,Dp/Dt,Phi,Phin,Phit,
C1            Phitn,Phits,Phitn,Phiss,Phins,cos(B),sin(B),Bs), all
C1            previously determined.
C1            The maximum absolute free surface amplitude is computed
C1            and the program stops if it is > 100*d1.
C1
C2  CALL UPDTDB(PHI,PHIT,UVW,DUVWDT,DPRDT)
C3  CALL ARG. XYZNOD(NOM,3)       = Grid current node coordinates
C3            PHI                 = Potential Phi
C3            PHIT                = Pot.  d/dt, Phit
C3            UVW(3,NOM)          = Velocities for current step
C3            DUVWDT(3,NOM)       = Accelera. for current step
C3            DPRDT(NOM)     = Pressure and pressure d/dt (p.Dp/Dt)
C3            DT,DT22             = Time step (dt, Dt2/2)
C4  RET. ARG. XYZNOD,PHI
C4
C6  INT. CALL ERRORS
CE  ERRORS    01 = Max amplitude on the free surface greater than 100*d1
C10 Feb. 99   S. Grilli, INLN
CLUPDTDB SUB. WHICH COMPUTES GRID AND POTENTIAL UPDATING IN TIME
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      CHARACTER*8 TEXTE
C
      COMMON/DATGEN/  DUM1(5),IDUM1(8),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(MOMM*2+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7+2*NOMM+66),
     .                INODF(6,2),IBCOND(6)  
      COMMON /BOUNDC/ DT,DUM3,DT22,DUM4(2),RHO,DUM5(2),GE,DEC,
     .                DUM6(4),IDW 
      COMMON /OPTION/ ISYM,IPOST,IVECT
C  
      DIMENSION PHI(NOM),PHIT(NOM),DPRDT(NOMM),UVW(3,NOMM),
     .          DUVWDT(3,NOMM)  
C  
      DATA ZERO/0.D0/,CENT/100.D0/,TEXTE/'UPDTFS01'/
C  
       SAVE  
C     SAVE /DATGEN/,/MAILLE/,/BOUNDC/
C
C.....Updating of boundary K=1-3 when Dirichlet condition is imposed  
C  
CSYM
      KFMAX=5
      IF(ISYM.EQ.1) KFMAX=KFMAX-1
      DO KF=1,KFMAX
         IF(IBCOND(KF).EQ.1) THEN  
            DO IK=INODF(KF,1),INODF(KF,2)
C
C              Geometry updating
C
               U2   = ZERO
	       UDOT = ZERO
               DO K=1,3
	          XYZNOD(IK,K) = XYZNOD(IK,K) + DT*UVW(K,IK) + 
     .                                          DT22*DUVWDT(K,IK)  
                  U2 = U2 + UVW(K,IK)*UVW(K,IK)
		  UDOT = UDOT + UVW(K,IK)*DUVWDT(K,IK)
               END DO
C  
C              Potential updating  
C  
               DPHDT  = PHIT(IK) + U2  
               D2PHDT = UDOT - GE*UVW(3,IK)- DPRDT(IK)/RHO
C  
               PHI(IK) = PHI(IK) + DT*DPHDT + DT22*D2PHDT  
            END DO  
         END IF  
      END DO  
C  
C.....Test of overall divergence of the computations by checking the  
C     maximum free surface amplitude  
C  
      ZEMAX = ZERO  
C  
      DO IK=INODF(1,1),INODF(1,2)
         IF(DABS(XYZNOD(IK,3)).GT.ZEMAX) THEN  
            ZEMAX = DABS(XYZNOD(IK,3))  
         END IF  
      END DO  
C  
      IF(ZEMAX.GT.CENT*DEC) THEN  
         CALL ERRORS(TEXTE)  
C        -----------  
      END IF  
C  
      RETURN  
C  
      END  
  
