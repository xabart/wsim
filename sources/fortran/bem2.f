C
C                     BEM ANALYSIS FOR DIRECT CONTRIBUTIONS
C
C-----------------------------------------------------------------------
      SUBROUTINE BEMK2(ZKQLIE,ZKULIE,NIE,IELTS,NBNODES,INODES,NNIE)
C-----------------------------------------------------------------------
C0  BEMKF     BEMK2
C1  PURPOSE   makes the BEM analysis for near interactions of Fast Multipole
C1            algorithm
C1
C3  CALL ARG. XYZNOD(NOM)         = Coordinates of discretization nodes
C3            XICON(MOM,2)        = xo,eta for MII elements
C3            IGCON(MOM,4,4)      = MII elts connectivity
C3            ILCON(MOM,4)        = 4-node elt. connectivity
C3            IFACE(MOM)          = Face code for each element
C3            ISUB(MOMM)          = Max subdivisions for elts.
C3            INTG(MOMM)          = Nb. of integ. points per elt.
C3            LSUB(NOM)           = Subdivision status for element IE
C3            LXYZI(NOM,2)        = Addresses of double/triple nodes
C3            JSLIM(6)            = Record limits for saving in /SAVEF/
C3            NELEF(6),NNODF(6)   = Nbs. of elts/nodes for each face
C3            IELEF(6,2),INODF()  = Begin/ending node/elemt Nbs. for face
C3            IBCOND(6)           = Boundary condition type for each face
C3            ZKQLIE(NBNODES,MNODM) = Kq matrix from elements to nodes in current cells
C3            ZKULIE(NBNODES,MNODM) = Ku matrix from elements to nodes in current cells
C3            NOM,MOM             = Discr. nb. of nodes and elements
C3            NOMM,MOMM           = Discr. max nb. of nodes and elts.
C3            NNODM               = Discr. max nb. of nodes per element
C3            ALMAX [0,PI/2]      = Limit angle for adapt. integration
C3            XYZLO1(NNODM,3)     = XYZ-coordinates of element IE MII nodes
C3            NODE1(NNODE)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = XYZ-coordinates of element IE 4 nodes
C3            NODE2(NNODE)        = Nodes of 4-node element IE
C3            NSUBM               = Max nb. of subdivisions for IE (0-8)
C3            NINTR               = Nb. of regular and singular int. pts
C3            ETARP()             = Regular integr. points
C3            WEIGTR()            = Regular integr. weights
C3            INCOND(NOMM)        = Boundary condition types for all nodes
C3            INFACE(NOMM)        = Face types for all nodes
C3            ZKQLIE,ZKULIE(NNIE*NBNODES) = Results of local integrations
C3            NNIE                = Number of nodes related to the elements
C3                                  of the current cell
C3            NIE                 = Nb of elts in the current cell
C3            IELTS(NIE)          = Elts numbers of the contributing cell
C3            NBNODES             = Nb of nodes in the evaluating cell
C3            IELTS(NIE)          = Elts numbers of the current cell
C4
C4  RET. ARG. ZKQLIE,ZKULIE,JSLIM
C6
C6  INT.CALL  GAUSSP,BILMAT,BIDMAT,ERRORS
C6            SHAPFN,SHAPD1
CE  ERRORS    01= Insufficient length of saving buffer
C10 Jan. 99   S. Grilli at INLN
C10           adapted for FMA by C.Fochesato, CMLA, 2003
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      INCLUDE 'param.inc'
      CHARACTER*8 TEXTE
C
      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),
     .                DUM3(18),IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),IDUM2(36),JSLIM(6),NELEF(6),
     .                NNODF(6),IELEF(6,2),INODF(6,2),IBCOND(6)
      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/  ETARP(NINTM),WEIGTR(NINTM),PHIP12(NINTM),
     .                PHIP23(NINTM),RMIP12(NINTM),RMIP23(NINTM),
     .                RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .                XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .                ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /FSCUB/ FN0XI(NDM,NINTM),FN0ET(NDM,NINTM),
     .               FN1XI(NDM,NINTM),FN1ET(NDM,NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),DUM5(7*NINTM*NINTM)
      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
     .               FIS23(MNODM,NINTM,NINTM,MNOD),
     .               DFIX12(MNODM,NINTM,NINTM,MNOD),
     .               DFIX23(MNODM,NINTM,NINTM,MNOD),
     .               DFIE12(MNODM,NINTM,NINTM,MNOD),
     .               DFIE23(MNODM,NINTM,NINTM,MNOD)
c      COMMON /SYSTEM/ DUM6(39),IO6,IFLAGS,IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
C
C.....IO1 is added in /SAVEF/, /SUBDI/ is created, CF03
C
      COMMON /SUBDI/ ALMAX,LSUB(NOMM)
C
C.....common to store the vector for rigid mode technique RM
C     and preconditioning matrix SPA, CF03
C
      COMMON /RMPREC/ RM(NOMM),SPA(NOMM)
C
C.....a common is added for some variables which need to be initialized
C     outside the multiple calls to BEMK2 and BEMKFMA, CF03
C
      COMMON /BEMINI/ EFJXIP,EFJETP,JSAVE,NINTPR,NSIDEP
C
C.....common for options, here choice for vectorized integrations
C
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
C.....local matrix ZKQ/UL only for nodes of the current cell
C
      DIMENSION ZKQL(NBNODES,MNODM),ZKUL(NBNODES,MNODM),
     .          FSF(NDM),FSD(NDM),FSFX12(NDM),FSFE12(NDM),
     .          FSDX12(NDM),FSDE12(NDM),FSFX23(NDM),FSFE23(NDM),
     .          FSDX23(NDM),FSDE23(NDM),XELT(MNOD,2)
C
C.....Indexes of elements and nodes
C
      DIMENSION IELTS(NIE),INODES(NBNODES)
C
C.....Results of local integrations
C
      DIMENSION ZKQLIE(NNIE*NBNODES),ZKULIE(NNIE*NBNODES)
C
C.....Global indexes of nodes related to integrated elements
C
      DIMENSION JGCOEF(NNIE)
C
      SAVE
C
      DATA TEXTE/'BEMK01'/,ZERO/0.D0/,ONE/1.D0/,TWO/2.D0/,THREE/3.D0/,
     .     PIS8/0.3926990816987241D0/,PIS2/1.570796326794896D0/,
     .     OTHREE/0.3333333333333333D0/,S3/0.3333333333333333D0/,
     .     XELT/-1.D0,1.D0,1.D0,-1.D0,-1.D0,-1.D0,1.D0,1.D0/,
     .     HALF/0.5D0/
C
      DO J=1,NNIE
         DO I=1,NBNODES
	    K=(J-1)*NBNODES+I
            ZKQLIE(K)=ZERO
            ZKULIE(K)=ZERO
	 END DO
      END DO
      DO J=1,NNIE
         JGCOEF(J)=0
      END DO
      INDJG = 0
C
      NNODE  = MNOD
C
C=====Loop on the elements of the current cell, local computations, assembling
C
      DO INDIE=1,NIE
         IE=IELTS(INDIE)
C
C     ...Local values assigned to element IE
C
         NINTR = INTG(IE)
         NSUBM = ISUB(IE)
         NSIDE = IFACE(IE)
C
C        4-node connectivity in NODE1(NDM)
C
         DO J=1,NNODE
            NODE2(J)         = ILCON(IE,J)
            XYZLO2(J,1)      = XYZNOD(NODE2(J),1)
            XYZLO2(J,2)      = XYZNOD(NODE2(J),2)
            XYZLO2(J,3)      = XYZNOD(NODE2(J),3)
            INCOND(NODE2(J)) = IBCOND(IFACE(IE))
            INFACE(NODE2(J)) = IFACE(IE)
         END DO
C
C        Assign NOD1E(NDM) and NNODE for correct assembly of ZK(U/Q)L
C        i.e., considering MII elements
C
         ILOC = 0
         DO J=1,NNODE
            DO I=1,NNODE
               ILOC = ILOC + 1
               NODE1(ILOC)    = IGCON(IE,I,J)
               XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
               XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
               XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYM
	       IF (ISYM.EQ.1) THEN
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.6.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYS
            END DO
         END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
C     ...Filling up of the common INTEG when the elements have different
C        node or integration points numbers (accelerator)
C
         IRECAL = 1
         IF (NINTR.NE.NINTPR) THEN
C
C           Regular Gauss points
C
            CALL GAUSSP
C           -----------
C
C           Singular integrals characteristics
C
            DO IP = 1,NINTR
               PHIP12(IP) = PIS8*(ONE   + ETARP(IP))
               PHIP23(IP) = PIS8*(THREE + ETARP(IP))
               RMIP12(IP) = TWO/DCOS(PHIP12(IP))
               RMIP23(IP) = TWO/DSIN(PHIP23(IP))
C
               DO JP=1,NINTR
                  RIJP12(IP,JP) = HALF*RMIP12(IP)*(ONE + ETARP(JP))
                  RIJP23(IP,JP) = HALF*RMIP23(IP)*(ONE + ETARP(JP))
C
                  DO I=1,NNODE
                     XI12(IP,JP,I)  = XELT(I,1) + RIJP12(IP,JP)*
     .                                DCOS(PHIP12(IP) + (I-1)*PIS2)
                     XI23(IP,JP,I)  = XELT(I,1) + RIJP23(IP,JP)*
     .                                DCOS(PHIP23(IP) + (I-1)*PIS2)
                     ET12(IP,JP,I)  = XELT(I,2) + RIJP12(IP,JP)*
     .                                DSIN(PHIP12(IP) + (I-1)*PIS2)
                     ET23(IP,JP,I)  = XELT(I,2) + RIJP23(IP,JP)*
     .                                DSIN(PHIP23(IP) + (I-1)*PIS2)
                  END DO
               END DO
            END DO
C
            IRECAL = 0
         END IF
C
C        MII cubic 4-node shape functions
C        Compute shape functions and their derivatives for
C        cubic element on the boundary and store in common FSCUB
C
         IF(EFJXI.NE.EFJXIP.OR.NINTR.NE.NINTPR) THEN
C
C           XI direction
C
            IRECAL = 0
            DO IP=1,NINTR
               CHI = (ETARP(IP) + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0XI(J,IP) = FSF(J)
                  FN1XI(J,IP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
         IF(EFJET.NE.EFJETP.OR.NINTR.NE.NINTPR) THEN
C
C           ETA direction
C
            IRECAL = 0
            DO JP=1,NINTR
               CHI = (ETARP(JP) + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0ET(J,JP) = FSF(J)
                  FN1ET(J,JP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
C
C        Calculates bidim shape functions at all reg/sing integ. points
C
         IF(IRECAL.EQ.0) THEN
            ILOC = 0
            DO J=1,NNODE
               DO I=1,NNODE
                  ILOC = ILOC + 1
                  DO IP=1,NINTR
                     DO JP=1,NINTR
C
C                       Shape functions at all regular integ. points
C
                        FIRIP(ILOC,IP,JP)  = FN0XI(I,IP)*FN0ET(J,JP)
                        DFIXIP(ILOC,IP,JP) = FN1XI(I,IP)*FN0ET(J,JP)
                        DFIETP(ILOC,IP,JP) = FN0XI(I,IP)*FN1ET(J,JP)
                     END DO
                  END DO
               END DO
            END DO
C
C           Shape functions at all singular integ. points
C
            DO IP=1,NINTR
               DO JP=1,NINTR
                  DO L=1,NNODE
                     CHX12 = (XI12(IP,JP,L) + ONE)*OTHREE + EFJXI
                     CHX23 = (XI23(IP,JP,L) + ONE)*OTHREE + EFJXI
                     CHE12 = (ET12(IP,JP,L) + ONE)*OTHREE + EFJET
                     CHE23 = (ET23(IP,JP,L) + ONE)*OTHREE + EFJET
C
                     CALL SHAPFN(CHX12,FSFX12,NNODE)
C                    -----------
                     CALL SHAPD1(CHX12,FSDX12,NNODE)
C                    -----------
                     CALL SHAPFN(CHE12,FSFE12,NNODE)
C                    -----------
                     CALL SHAPD1(CHE12,FSDE12,NNODE)
C                    -----------
                     CALL SHAPFN(CHX23,FSFX23,NNODE)
C                    -----------
                     CALL SHAPD1(CHX23,FSDX23,NNODE)
C                    -----------
                     CALL SHAPFN(CHE23,FSFE23,NNODE)
C                    -----------
                     CALL SHAPD1(CHE23,FSDE23,NNODE)
C                    -----------
                     ILOC = 0
                     DO J=1,NNODE
                        DO I=1,NNODE
                           ILOC = ILOC + 1
                           FIS12(ILOC,IP,JP,L)  = FSFX12(I)*FSFE12(J)
                           DFIX12(ILOC,IP,JP,L) = S3*FSDX12(I)*FSFE12(J)
                           DFIE12(ILOC,IP,JP,L) = S3*FSFX12(I)*FSDE12(J)
                           FIS23(ILOC,IP,JP,L)  = FSFX23(I)*FSFE23(J)
                           DFIX23(ILOC,IP,JP,L) = S3*FSDX23(I)*FSFE23(J)
                           DFIE23(ILOC,IP,JP,L) = S3*FSFX23(I)*FSDE23(J)
                        END DO
                     END DO
                  END DO
               END DO
            END DO
         END IF
C
C     ...Computation of local matrices, save geometry and shape
C        functions in common SAVEF, beginning at JSAVE (change for
C        no subdivisions or subdivision integrals)
C
         IF (IVECT.EQ.0) THEN
           IF(NSUBM.EQ.0) THEN
	     CALL BILMAT2(ZKQL,ZKUL,NBNODES,INODES)
C            ------------
           ELSE
             CALL BIDMAT2(ZKQL,ZKUL,IE,NBNODES,INODES)
C            ------------
           END IF
         ELSE IF (IVECT.EQ.2) THEN
           IF(NSUBM.EQ.0) THEN
	     CALL BILMAT2_V(ZKQL,ZKUL,NBNODES,INODES)
C            ------------
           ELSE
             CALL BIDMAT2(ZKQL,ZKUL,IE,NBNODES,INODES)
C            ------------
           END IF
         ELSE
           IF(NSUBM.EQ.0) THEN
	     CALL BILMAT2_V(ZKQL,ZKUL,NBNODES,INODES)
C            ------------
           ELSE
             CALL BIDMAT2_V(ZKQL,ZKUL,IE,NBNODES,INODES)
C            ------------
           END IF
	 END IF
C
C     ...Assembling and storage for all the nodes of the
C        current cell:all the constituting nodes of the
C        elements
C
	 DO J=1,MNODM
	     JG=NODE1(J)
	     CALL ISINVECT(ISIN,JG,JGCOEF,INDJG)
C            -------------
	     IF (ISIN.GT.0) THEN
	       INDJGK=ISIN
	     ELSE
	       INDJG=INDJG+1
	       JGCOEF(INDJG)=JG
	       INDJGK=INDJG
	     END IF
             DO I=1,NBNODES
	       K=(INDJGK-1)*NBNODES+I
               ZKQLIE(K)=ZKQLIE(K)+ZKQL(I,J)
               ZKULIE(K)=ZKULIE(K)+ZKUL(I,J)
	     END DO
	 END DO

         NINTPR = NINTR
         NSIDEP = NSIDE
         EFJXIP = EFJXI
         EFJETP = EFJET
C
      END DO
C
C     diagonal preconditioning matrix
C
      DO J=1,NNIE
         JG=JGCOEF(J)
	 DO I=1,NBNODES
            IG=INODES(I)
            IF (IG.EQ.JG) THEN
	       K=(J-1)*NBNODES+I
	       IF (INCOND(JG).EQ.1.) THEN
	          SPA(IG)=SPA(IG)-ZKULIE(K)
	       ELSE
	          SPA(IG)=SPA(IG)+ZKQLIE(K)
               END IF
	    END IF
	 END DO
      END DO
C
      RETURN
C
 2000 FORMAT(3I5,6F10.5)
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE BILMAT2(ZKQL,ZKUL,NBNODES,INODES)
C-----------------------------------------------------------------------
C1  BILMATF   BILMAT2
C1  PURPOSE   Compute the local Kul and Kql to the elemt IE
C1            for the NBNODES of INODES and save
C1            geometric and intrinsic data for the 3D-BEM (no elt. subdi)
C2  CALL      CALL BILMAT2(ZKQL,ZKUL,NBNODES,INODES)
C3  CALL ARG. XYZNOD(NOMM,3)      = Coordinates of discretization nodes
C3            LXYZ(NOM)           = Addresses of possible double nodes
C3            NOM,NOMM            = Number of nodes and max nb. of nodes
C3            ZKQL(NBNODES,NNODE)     = Local Kq matrix of element IE
C3            ZKUL(NBNODES,NNODE)     = Local Ku matrix of element IE
C3            NINTR               = Nb. of reg. and sing. int. pts of IE
C3            WEIGTR              = Regular integr. weights
C3            IP,JP               = current integration point of IE
C3            XYZLO1(MNODM,3)     = MII nodes for IE
C3            NODE1(NNODM)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = 4-nodes for IE
C3            NODE2(NNODM)        = Nodes of 4-node element IE
C3            FIRIP(NNODM,NINTM,.)= Reg. shape fcts. and   )  At all
C3            DFIXIP(),DXIETP()   = their derivatives      )  points IP,JP
C3            XYZPR(NINTM,.,3)    = All integr. point coordinates
C3            ZNPR(NINTM,.,3)     = All integr. point normal vector
C3            ZJACPR(NINTM,NINTM) = All integr. point jacobian
C3            FIS12(),FIS23()     = Sing. shape fcts. and  )  At all
C3            DFIX12(),DFIX23()   = their derivatives      )  points IP,JP
C3            DFIE12(),DFIE23()   = their derivatives      )  and 4-nodes
C3            FI(NNODM)           = Shape functions and    )  At point
C3            DFIX(NNODM),DFIE()  = their derivatives      )  IP,JP
C3            XYZP(3)             = Current integr. point coordinates
C3            ZNP(3)              = Current integr. point normal vector
C3            ZJACP               = Current integr. point jacobian
C4  RET. ARG. ZKUL,ZKQL, for element IE
C6  INT.CALL  CARAC2,SAVGEO
C9  Jan. 99   S. GRILLI, INLN
C9            adapetd for FMA, C.Fochesato, CMLA, 2003
CLBILMAT SUB. WHICH COMPUTES THE MATRIX Kul AND Kql OF IE FOR THE 3D-BEM
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
C
      INCLUDE 'param.inc'
      COMMON /DATGEN/ DUM1(2),H0,DUM1B(2),IDUM1(8),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*4),IFACE(MOMM),
     .                ISUB(MOMM),INTG(MOMM),LXYZ(NOMM,2),IDUM3(36),
     .                JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),INODF(6,2),
     .                IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/ DUM3(NINTM),WEIGTR(NINTM),DUM4(2*NINTM),
     .               RMIP12(NINTM),RMIP23(NINTM),
     .               RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .               XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .               ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM5(6),
     .                ZNP(3),ZJACP
      COMMON /FSCUB/ DUM6(4*NDM*NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),XYZPR(NINTM,NINTM,3),
     .               ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
     .               FIS23(MNODM,NINTM,NINTM,MNOD),
     .               DFIX12(MNODM,NINTM,NINTM,MNOD),
     .               DFIX23(MNODM,NINTM,NINTM,MNOD),
     .               DFIE12(MNODM,NINTM,NINTM,MNOD),
     .               DFIE23(MNODM,NINTM,NINTM,MNOD)
      COMMON /BOTFCT/ DUMBOT(NXM*NYM+2),IBOT
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION ZKQL(NBNODES,MNODM),ZKUL(NBNODES,MNODM),XX(3)
      DIMENSION INODES(NBNODES)
C
      SAVE
C
      DATA ZERO/0.D0/,S64/64.D0/,PII4/0.7957747154594768D-01/,
     .     ONE/1.D0/,TWO/2.D0/,FOUR/4.D0/,EPS/1.D-08/
C
C.....Initialisation of local matrices
C
      NNODM = NNODE*NNODE
      DO J=1,NNODM
         DO I=1,NBNODES
            ZKQL(I,J) = ZERO
            ZKUL(I,J) = ZERO
         END DO
      END DO
C
C.....Regular integration
C
      DO IP=1,NINTR
         DO JP=1,NINTR
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
               DFIX(J) = DFIXIP(J,IP,JP)
               DFIE(J) = DFIETP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Save geometry in common INTEG
C
            DO J=1,3
               XYZPR(IP,JP,J) = XYZP(J)
               ZNPR(IP,JP,J)  = ZNP(J)
            END DO
            ZJACPR(IP,JP) = ZJACP
C
C           Final Gauss Jacobian
C
            ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
            CST   = PII4*ZJACP
C
C        ...Computation of ZKQL,ZKUL
C
            DO IL=1,NBNODES
	       L=INODES(IL)
               RS     = ZERO
               DGNUM = ZERO
               DO J=1,3
                  XX(J) = XYZP(J) - XYZNOD(L,J)
                  DGNUM = DGNUM + XX(J)*ZNP(J)
                  RS     = RS + XX(J)*XX(J)
               END DO
               R  = DSQRT(RS)
C
C              Computation of Green's functions at (IP,JP)
C
               DGDNP = -CST*DGNUM/(R*R*R)
               GP    =  CST/R
C
C              Kernel transformation, in case of L on 4-node IE
C
               ISING = 0
               DO I=1,NNODE
                  IF (L.EQ.NODE2(I)) THEN
                    DGDNP    = ZERO
                    GP = ZERO
		     ISING = 1
                  END IF
               END DO
C
C              Image method flat bottom (affects regular/one sg. integrals)
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + XYZNOD(L,3)
C
C                 Test for bottom nodes which lead to sg. integral
C                 of the image part
C
		  IF((DABS(HP).GT.EPS).OR.(ISING.EQ.0)) THEN
		     RP     = RS + FOUR*HP*(HP + XX(3))
                     RP     = DSQRT(RP)
		     DGPNUM = DGNUM + TWO*HP*ZNP(3)
		     GP     = GP + CST/RP
		     DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
		  END IF
	       END IF
C
CSYM           Image method y-symmetry (affects regular/one sg. integrals)
C
               IF(ISYM.EQ.1) THEN
                  YP = XYZNOD(L,2)
C
C                 Test for nodes which lead to sg. integral
C                 of the image part
C
		  IF((DABS(YP).GT.EPS).OR.(ISING.EQ.0)) THEN
		     RP     = RS + FOUR*YP*XYZP(2)
                     RP     = DSQRT(RP)
		     DGPNUM = DGNUM + TWO*YP*ZNP(2)
		     GP     = GP + CST/RP
		     DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
		  END IF
C
C                 + Image method flat bottom (affects regular/one sg. integrals)
C
                  IF(IBOT.EQ.10) THEN
C
		     IF((DABS(HP).GT.EPS).OR.(ISING.EQ.0)) THEN
		        RP     = RS+FOUR*(YP*XYZP(2)+HP*(HP+XX(3)))
                        RP     = DSQRT(RP)
		        DGPNUM = DGNUM + TWO*(YP*ZNP(2)+ HP*ZNP(3))
		        GP     = GP + CST/RP
		        DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
		     END IF
	          END IF
	       END IF
CMYS
C              Regular integrals
C
               DO J=1,NNODM
                  ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP
                  ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP
	       END DO
            END DO
         END DO
      END DO
C
C.....Save for later use of geometric and intrinsic data
C
C      CALL SAVGEO
C     -----------
C
C.....Singular integrals for ZKUL
C
      DO IL=1,NBNODES
         L=INODES(IL)
         DO I=1,NNODE
          IF (L.EQ.NODE2(I)) THEN
C
            DO IP=1,NINTR
              DO JP=1,NINTR
               CS12 = RMIP12(IP)*WEIGTR(IP)*WEIGTR(JP)/
     .                S64
               CS23 = RMIP23(IP)*WEIGTR(IP)*WEIGTR(JP)/
     .                S64
C
C           ...Gauss integration point intrins. coord. and weight for 12
C              unit vectors and Jacob.
C
               DO J=1,NNODM
                  FI(J)   = FIS12(J,IP,JP,I)
                  DFIX(J) = DFIX12(J,IP,JP,I)
                  DFIE(J) = DFIE12(J,IP,JP,I)
               END DO
C
               CALL CARAC2
C              -----------
C
               R     = ZERO
               DGNUM = ZERO
               DO J=1,3
                  XX(J) = XYZP(J) - XYZNOD(L,J)
                  DGNUM = DGNUM + XX(J)*ZNP(J)
                  R     = R + XX(J)*XX(J)
               END DO
               R     = DSQRT(R)
               GP    = CS12*ZJACP*RIJP12(IP,JP)/R
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + XYZNOD(L,3)
		  IF(DABS(HP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM + HP*ZNP(3))
		  END IF
	       END IF
CSYM
C              Test for lateral nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(ISYM.EQ.1) THEN
                  YP = XYZNOD(L,2)
		  IF(DABS(YP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM+YP*ZNP(2))
		  END IF
	       END IF
CMYS
	       DGDNP = -CS12*ZJACP*RIJP12(IP,JP)*DGNUM/(R*R*R)
C
               DO J=1,NNODM
                 ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP
                 ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP
               END DO
C
C           ...Gauss integration point intrins. coord. and weight for 23
C              unit vectors and Jacob.
C
               DO J=1,NNODM
                  FI(J)   = FIS23(J,IP,JP,I)
                  DFIX(J) = DFIX23(J,IP,JP,I)
                  DFIE(J) = DFIE23(J,IP,JP,I)
               END DO
C
               CALL CARAC2
C              -----------
C
               R     = ZERO
               DGNUM = ZERO
               DO J=1,3
                  XX(J) = XYZP(J) - XYZNOD(L,J)
                  DGNUM = DGNUM + XX(J)*ZNP(J)
                  R     = R + XX(J)*XX(J)
               END DO
               R     = DSQRT(R)
               GP    = CS23*ZJACP*RIJP23(IP,JP)/R
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + XYZNOD(L,3)
		  IF(DABS(HP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM + HP*ZNP(3))
		  END IF
	       END IF
CSYM
C              Test for lateral nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(ISYM.EQ.1) THEN
                  YP = XYZNOD(L,2)
		  IF(DABS(YP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM+YP*ZNP(2))
		  END IF
	       END IF
CMYS
               DGDNP = -CS23*ZJACP*RIJP23(IP,JP)*DGNUM/(R*R*R)
C
               DO J=1,NNODM
                 ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP
                 ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP
               END DO
              END DO
	    END DO
	  END IF
        END DO
      END DO
C
C.....Double/triple nodes, taking into account for the sing. and part. integ.
C
      DO IL=1,NBNODES
         L=INODES(IL)
         DO I=1,NNODE
            IF (L.EQ.NODE2(I)) THEN
               DO IK=1,NBNODES
                  LK=INODES(IK)
                  IF(LXYZ(L,1).EQ.LK.OR.LXYZ(L,2).EQ.LK) THEN
                    DO J=1,NNODM
                       ZKUL(IK,J) = ZKUL(IL,J)
                       ZKQL(IK,J) = ZKQL(IL,J)
                    END DO
                  END IF
               END DO
           END IF
        END DO
      END DO
C
      RETURN
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE BIDMAT2(ZKQL,ZKUL,IE,NBNODES,INODES)
C-----------------------------------------------------------------------
C1  BIDMATF   BIDMAT2
C1  PURPOSE   Compute the local Kul and Kql to the elemt IE
C1            for the NBNODES of INODES and save
C1            geometric and intrinsic data for the 3D-BEM (with elt. subdi)
C2  CALL      CALL BIDMAT2(ZKQL,ZKUL,IE,NBNODES,INODES)
C3  CALL ARG. XYZNOD(NOMM,3)      = Coordinates of discretization nodes
C3            LXYZ(NOM)           = Addresses of possible double nodes
C3            LSUB(NOM)           = Subdivision vector for IE
C3            ALMAXB              = Limit angle for subdivisions
C3            NOM,NOMM            = Number of nodes and max nb. of nodes
C3            ZKQL(NBNODES,NNODE)     = Local Kq matrix of element IE
C3            ZKUL(NBNODES,NNODE)     = Local Ku matrix of element IE
C3            NINTR               = Nb. of reg. and sing. int. pts of IE
C3            WEIGTR              = Regular integr. weights
C3            IP,JP               = current integration point of IE
C3            XYZLO1(MNODM,3)     = MII nodes for IE
C3            NODE1(NNODM)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = 4-nodes for IE
C3            NODE2(NNODM)        = Nodes of 4-node element IE
C3            FIRIP(NNODM,NINTM,.)= Reg. shape fcts. and   )  At all
C3            DFIXIP(),DXIETP()   = their derivatives      )  points IP,JP
C3            XYZPR(NINTM,.,3)    = All integr. point coordinates
C3            ZNPR(NINTM,.,3)     = All integr. point normal vector
C3            ZJACPR(NINTM,NINTM) = All integr. point jacobian
C3            FIS12(),FIS23()     = Sing. shape fcts. and  )  At all
C3            DFIX12(),DFIX23()   = their derivatives      )  points IP,JP
C3            DFIE12(),DFIE23()   = their derivatives      )  and 4-nodes
C3            FI(NNODM)           = Shape functions and    )  At point
C3            DFIX(NNODM),DFIE()  = their derivatives      )  IP,JP
C3            XYZP(3)             = Current integr. point coordinates
C3            ZNP(3)              = Current integr. point normal vector
C3            ZJACP               = Current integr. point jacobian
C4  RET. ARG. ZKUL,ZKQL, for element IE
C6  INT.CALL  CARAC2,SAVGEO
C9  Feb. 99   S. GRILLI, INLN
C9            adapetd for FMA, C.Fochesato, CMLA, 2003
CLBIDMAT SUB. WHICH COMPUTES THE MATRIX Kul AND Kql OF IE FOR THE 3D-BEM
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
      INCLUDE 'param.inc'
      LOGICAL IOUT
C
      COMMON /DATGEN/ DUM1(2),H0,DUM1B(2),IDUM1(8),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),DUM2(18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*4),IFACE(MOMM),
     .                ISUB(MOMM),INTG(MOMM),LXYZ(NOMM,2),IDUM3(36),
     .                JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),INODF(6,2),
     .                IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/ ETARP(NINTM),WEIGTR(NINTM),DUM4(2*NINTM),
     .               RMIP12(NINTM),RMIP23(NINTM),
     .               RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .               XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .               ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM5(6),
     .                ZNP(3),ZJACP
      COMMON /FSCUB/ DUM6(4*NDM*NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),XYZPR(NINTM,NINTM,3),
     .               ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
     .               FIS23(MNODM,NINTM,NINTM,MNOD),
     .               DFIX12(MNODM,NINTM,NINTM,MNOD),
     .               DFIX23(MNODM,NINTM,NINTM,MNOD),
     .               DFIE12(MNODM,NINTM,NINTM,MNOD),
     .               DFIE23(MNODM,NINTM,NINTM,MNOD)
      COMMON /SUBDI/ ALMAXB,LSUB(NOMM)
      COMMON /BOTFCT/ DUMBOT(NXM*NYM+2),IBOT
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION ZKQL(NBNODES,MNODM),ZKUL(NBNODES,MNODM)
      DIMENSION XX(3),DIAG(2),XCIE(3),VIE(3,3),V12(3),V13(3),
     .          FSFX(NDM),FSDX(NDM),FSFE(NDM),FSDE(NDM),DISND(MNOD)
      DIMENSION INODES(NBNODES)
C
      SAVE
C
      DATA ZERO/0.D0/,QUART/0.25D0/,HALF/0.5D0/,ONE/1.0D0/,
     .     TWO/2.D0/,THREE/3.D0/,FOUR/4.D0/,EIGHT/8.D0/,S64/64.D0/,
     .     PII4/0.7957747154594768D-01/,DLOG2I/3.321928094D0/,
     .     CRAT/0.4999D0/,OTHREE/0.3333333333333333D0/,EPS/1.D-08/
C
C.....Initialisation of local matrices
C
      NNODM = NNODE*NNODE
      DO J=1,NNODM
         DO IL=1,NBNODES
            ZKQL(IL,J) = ZERO
            ZKUL(IL,J) = ZERO
         END DO
      END DO
C
C.....Computation of subdivisions for IE
C
      EFJXI = XICON(IE,1)
      EFJET = XICON(IE,2)
      TANA  = DTAN(ALMAXB)
C
C     Preparing for minimum distance computations
C
      DO I=2,NNODE
         DO J=1,3
            VIE(I-1,J) = XYZLO2(I,J) - XYZLO2(1,J)
	 END DO
      END DO
      V12(1) = VIE(1,2)*VIE(2,3) - VIE(2,2)*VIE(1,3)
      V12(2) = VIE(1,3)*VIE(2,1) - VIE(2,3)*VIE(1,1)
      V12(3) = VIE(1,1)*VIE(2,2) - VIE(2,1)*VIE(1,2)
      V13(1) = VIE(1,2)*VIE(3,3) - VIE(3,2)*VIE(1,3)
      V13(2) = VIE(1,3)*VIE(3,1) - VIE(3,3)*VIE(1,1)
      V13(3) = VIE(1,1)*VIE(3,2) - VIE(3,1)*VIE(1,2)
      ZNV12 = DSQRT(V12(1)*V12(1)+V12(2)*V12(2)+V12(3)*V12(3))
      ZNV13 = DSQRT(V13(1)*V13(1)+V13(2)*V13(2)+V13(3)*V13(3))
      DO J=1,3
         V12(J) = V12(J)/ZNV12
         V13(J) = V13(J)/ZNV13
      END DO
C
C     Longest 1/2 diagonal chord in elt. IE
C
      DO K=1,2
         DIAG(K) = ZERO
	 DO J=1,3
	    DIAG(K) = DIAG(K) + (XYZLO2(K+2,J) - XYZLO2(K,J))**2
         END DO
      END DO
      DIAM = HALF*DSQRT(MAX(DIAG(1),DIAG(2)))
C
C     Coordinates of elt. IE center
C
      DO J=1,3
         XCIE(J) = ZERO
         DO I=1,NNODE
            XCIE(J) = XCIE(J) + XYZLO2(I,J)
         END DO
         XCIE(J) = QUART*XCIE(J)
      END DO
C
C     Loop over exterior points
C
      DO IID=1,NBNODES
         ID=INODES(IID)
         IOUT = .TRUE.
         NSUB = 0
C
C        Check if node belongs to element or is a double node
C
         DO J=1,NNODE
            IF((ID.EQ.NODE2(J)).OR.(LXYZ(ID,1).EQ.NODE2(J)).OR.
     .          (LXYZ(ID,2).EQ.NODE2(J))) THEN
               IOUT =.FALSE.
            END IF
         END DO
C
C        Subdivision analysis
C
         IF(IOUT) THEN
C
C            Distance of I from elt IE center plus minima d12, d13
C
	     DISTCI = ZERO
	     D12    = ZERO
	     D13    = ZERO
	     DO J=1,3
	        DISTCI = DISTCI + (XCIE(J) - XYZNOD(ID,J))**2
                D12    = D12 + (XYZLO2(1,J) - XYZNOD(ID,J))*V12(J)
  	        D13    = D13 + (XYZLO2(1,J) - XYZNOD(ID,J))*V13(J)
             END DO
	     DISTCI = DSQRT(DISTCI)
	     DMINPP = MIN(ABS(D12), ABS(D13))
C
C            Distance of JOI from elt IE 4 nodes
C
	     DO I=1,NNODE
	        DISND(I) = ZERO
	        DO J=1,3
	           DISND(I) = DISND(I) + (XYZLO2(I,J)-XYZNOD(ID,J))**2
	        END DO
	        DISND(I) = DSQRT(DISND(I))
             END DO
             DISMI = MIN(DISND(1),DISND(2),DISND(3),DISND(4))
	     DISDA = DIAM/DISTCI
C
             DISTB = DSQRT(DISTCI*DISTCI + DMINPP*DMINPP)
	     IF(DISTCI.GT.DISTB) THEN
	        ANGLE = TWO*DATAN2(DIAM*DMINPP,DISTCI*DISTCI)
	     ELSE
	        IF(DMINPP.LT.EPS) THEN
		   ANGLE = ZERO
		ELSE
                   ANGLE = TWO*DATAN2(DIAM,DMINPP)
		END IF
             END IF
C
             IF((ANGLE.GT.ALMAXB).AND.(DISDA.GT.ONE)) THEN
C
C               Cas d'un angle obtu avec noeuds proche interieur elt
C
                IF(DMINPP.LT.EPS) THEN
	           DISDA = DIAM/DISTCI
		ELSE
	           DISDA = DIAM/DMINPP
		END IF
                NSUB  = INT(LOG10(DISDA/TANA)*DLOG2I + CRAT)
	     ELSE
                IF(DISMI.LT.DIAM) THEN
C
C                  Cas d'un angle aigu avec noeuds alignes en dehors elt
C
                   DISDA = DIAM/(DISMI*TANA)
                   NSUB = 0
                   IF((DISDA.GT.TWO).AND.(DISDA.LT.FOUR)) THEN
                      NSUB = 1
                   ELSE IF((DISDA.GE.FOUR).AND.(DISDA.LT.EIGHT)) THEN
                      NSUB = 2
                   ELSE IF(DISDA.GE.EIGHT) THEN
		      NSUB = 3
                   END IF
                ELSE
                   NSUB = 0
                END IF
             END IF
C
             IF(NSUB.GT.NSUBM) THEN
                NSUB = NSUBM
             END IF
 	  END IF
C
          LSUB(ID) = NSUB
      END DO
C
C.....Regular integration
C
      DO IP=1,NINTR
         DO JP=1,NINTR
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
               DFIX(J) = DFIXIP(J,IP,JP)
               DFIE(J) = DFIETP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Save geometry in common INTEG
C
            DO J=1,3
               XYZPR(IP,JP,J) = XYZP(J)
               ZNPR(IP,JP,J)  = ZNP(J)
            END DO
            ZJACPR(IP,JP) = ZJACP
C
C           Final Gauss Jacobian
C
            ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
C
C        ...Computation of ZKUL, ZKQL
C
            NSUBP = 0
            DO IL=1,NBNODES
	       L=INODES(IL)
C
C              Subdivisions for L
C
               NSUB = LSUB(L)
               NSG  = 2**NSUB
	       NSG2 = NSG*NSG
C
               IF(NSUB.EQ.0) THEN
                  IF(NSUBP.NE.0) THEN
                     DO J=1,NNODM
                        FI(J)   = FIRIP(J,IP,JP)
               		DFIX(J) = DFIXIP(J,IP,JP)
               		DFIE(J) = DFIETP(J,IP,JP)
            	     END DO
C
                     DO J=1,3
                        XYZP(J) = XYZPR(IP,JP,J)
       		        ZNP(J)  = ZNPR(IP,JP,J)
                     END DO
C
                     ZJACP = ZJACPR(IP,JP)*WEIGTR(IP)*WEIGTR(JP)
	     	     NSUBP = 0
                  END IF
	       ELSE
		  DSG   = TWO/FLOAT(NSG)
                  NSUBP = NSUB
	       END IF
C
               DO ISG=1,NSG
		  IF(NSUB.NE.0) THEN
		     XILI = TWO*(ISG-1)/FLOAT(NSG) - ONE
		     XIRI = XILI + DSG
	             XIM  = HALF*(XILI + XIRI)
	             XI   = ETARP(IP)/FLOAT(NSG) + XIM
                     CHI  = (XI + ONE)*OTHREE + EFJXI
                     CALL SHAPFN(CHI,FSFX,NNODE)
C                    -----------
                     CALL SHAPD1(CHI,FSDX,NNODE)
C                    -----------
	          END IF
                  DO JSG=1,NSG
		     IF(NSUB.NE.0) THEN
	                ETLI = TWO*(JSG-1)/FLOAT(NSG) - ONE
		      	ETRI = ETLI + DSG
	                ETM  = HALF*(ETLI + ETRI)
		        ETA  = ETARP(JP)/FLOAT(NSG) + ETM
                        CHI  = (ETA + ONE)*OTHREE + EFJET
                        CALL SHAPFN(CHI,FSFE,NNODE)
C                       -----------
                        CALL SHAPD1(CHI,FSDE,NNODE)
C                       -----------
C
C                       Shape functions at points IP,JP,ISG,JSG
C
                        ILOC = 0
                        DO J=1,NNODE
			   DO I=1,NNODE
                              ILOC = ILOC + 1
                              FI(ILOC)   = FSFX(I)*FSFE(J)
                              DFIX(ILOC) = OTHREE*FSDX(I)*FSFE(J)
                              DFIE(ILOC) = OTHREE*FSFX(I)*FSDE(J)
                           END DO
                        END DO
C
C                       Cord., unit vectors and Jacob.
C
                        CALL CARAC2
C                       -----------
                        ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
                     END IF
C
                     CST   = PII4*ZJACP/FLOAT(NSG2)
C
          	     RS     = ZERO
                     DGNUM = ZERO
                     DO J=1,3
                        XX(J) = XYZP(J) - XYZNOD(L,J)
                        DGNUM = DGNUM + XX(J)*ZNP(J)
                        RS     = RS + XX(J)*XX(J)
                     END DO
                     R = DSQRT(RS)
C
C                    Computation of Green's functions at (IP,JP)
C
                     DGDNP = -CST*DGNUM/(R*R*R)
                     GP    =  CST/R
C
C                    Kernel transformation, in case of L on 4-node IE
C
                     ISING = 0
                     DO I=1,NNODE
                        IF (L.EQ.NODE2(I)) THEN
                           DGDNP    = ZERO
                           GP = ZERO
		           ISING = 1
                        END IF
                     END DO
C
C                    Image method flat bottom (affects regular/one sg. integrals)
C
                     IF(IBOT.EQ.10) THEN
                        HP = H0 + XYZNOD(L,3)
C
C                       Test for bottom nodes which lead to sg. integral
C                       of the image part
C
		        IF((DABS(HP).GT.EPS).OR.(ISING.EQ.0)) THEN
   		           RP     = RS + FOUR*HP*(HP + XX(3))
                           RP     = DSQRT(RP)
		           DGPNUM = DGNUM + TWO*HP*ZNP(3)
		           GP     = GP + CST/RP
		           DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
			END IF
	             END IF
CSYM
C                    Image method y-symmetry (affects regular/one sg. integrals)
C
                     IF(ISYM.EQ.1) THEN
                        YP = XYZNOD(L,2)
C
		        IF((DABS(YP).GT.EPS).OR.(ISING.EQ.0)) THEN
   		           RP     = RS + FOUR*YP*XYZP(2)
                           RP     = DSQRT(RP)
		           DGPNUM = DGNUM + TWO*YP*ZNP(2)
		           GP     = GP + CST/RP
		           DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
			END IF
C
C                       + Image method flat bottom (affects regular/one sg. integrals)
C
                        IF(IBOT.EQ.10) THEN
C
		           IF((DABS(HP).GT.EPS).OR.(ISING.EQ.0)) THEN
		              RP    = RS+FOUR*(YP*XYZP(2)+HP*(HP+XX(3)))
                              RP    = DSQRT(RP)
		              DGPNUM= DGNUM + TWO*(YP*ZNP(2)+HP*ZNP(3))
		              GP    = GP + CST/RP
		              DGDNP = DGDNP - CST*DGPNUM/(RP*RP*RP)
		           END IF
	                END IF
	             END IF
CMYS
C                    Regular integrals
C
                     DO J=1,NNODM
                       ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP
                       ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP
                     END DO
		  END DO
	       END DO
            END DO
         END DO
      END DO
C
C.....Save for later use of geometric and intrinsic data
C
C      CALL SAVGEO
C     -----------
C
C.....Singular integrals for ZKUL,(ZKQL)
C
      DO IL=1,NBNODES
        L=INODES(IL)
        DO I=1,NNODE
          IF (L.EQ.NODE2(I)) THEN
            DO IP=1,NINTR
              DO JP=1,NINTR
               CS12 = RMIP12(IP)*WEIGTR(IP)*WEIGTR(JP)/
     .                S64
               CS23 = RMIP23(IP)*WEIGTR(IP)*WEIGTR(JP)/
     .                S64
C
C           ...Gauss integration point intrins. coord. and weight for 12
C              unit vectors and Jacob.
C
               DO J=1,NNODM
                  FI(J)   = FIS12(J,IP,JP,I)
                  DFIX(J) = DFIX12(J,IP,JP,I)
                  DFIE(J) = DFIE12(J,IP,JP,I)
               END DO
C
               CALL CARAC2
C              -----------
C
               R     = ZERO
               DGNUM = ZERO
               DO J=1,3
                  XX(J) = XYZP(J) - XYZNOD(L,J)
                  DGNUM = DGNUM + XX(J)*ZNP(J)
                  R     = R + XX(J)*XX(J)
               END DO
               R     = DSQRT(R)
               GP    = CS12*ZJACP*RIJP12(IP,JP)/R
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + XYZNOD(L,3)
		  IF(DABS(HP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM + HP*ZNP(3))
		  END IF
	       END IF
CSYM
C              Test for lateral nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(ISYM.EQ.1) THEN
                  YP = XYZNOD(L,2)
		  IF(DABS(YP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM+YP*ZNP(2))
		  END IF
	       END IF
CMYS
               DGDNP = -CS12*ZJACP*RIJP12(IP,JP)*DGNUM/(R*R*R)
C
               DO J=1,NNODM
                 ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP
                 ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP
               END DO
C
C           ...Gauss integration point intrins. coord. and weight for 23
C              unit vectors and Jacob.
C
               DO J=1,NNODM
                  FI(J)   = FIS23(J,IP,JP,I)
                  DFIX(J) = DFIX23(J,IP,JP,I)
                  DFIE(J) = DFIE23(J,IP,JP,I)
               END DO
C
               CALL CARAC2
C              -----------
C
               R     = ZERO
               DGNUM = ZERO
               DO J=1,3
                  XX(J) = XYZP(J) - XYZNOD(L,J)
                  DGNUM = DGNUM + XX(J)*ZNP(J)
                  R     = R + XX(J)*XX(J)
               END DO
               R     = DSQRT(R)
               GP    = CS23*ZJACP*RIJP23(IP,JP)/R
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + XYZNOD(L,3)
		  IF(DABS(HP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM + HP*ZNP(3))
		  END IF
	       END IF
CSYM
C              Test for lateral nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(ISYM.EQ.1) THEN
                  YP = XYZNOD(L,2)
		  IF(DABS(YP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM+YP*ZNP(2))
		  END IF
	       END IF
CMYS
               DGDNP = -CS23*ZJACP*RIJP23(IP,JP)*DGNUM/(R*R*R)
C
               DO J=1,NNODM
                 ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP
                 ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP
               END DO
              END DO
	    END DO
	  END IF
        END DO
      END DO
C
C.....Double/triple nodes, taking into account for the sing. and part. integ.
C
      DO IL=1,NBNODES
         L=INODES(IL)
         DO I=1,NNODE
            IF (L.EQ.NODE2(I)) THEN
               DO IK=1,NBNODES
                  LK=INODES(IK)
                  IF(LXYZ(L,1).EQ.LK.OR.LXYZ(L,2).EQ.LK) THEN
                    DO J=1,NNODM
                       ZKUL(IK,J) = ZKUL(IL,J)
                       ZKQL(IK,J) = ZKQL(IL,J)
                    END DO
                  END IF
               END DO
            END IF
        END DO
      END DO
C
      RETURN
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE BILMAT2_V(ZKQL,ZKUL,NBNODES,INODES)
C-----------------------------------------------------------------------
C1  BILMATF   BILMAT2
C1  PURPOSE   Compute the local Kul and Kql to the elemt IE
C1            for the NBNODES of INODES and save
C1            geometric and intrinsic data for the 3D-BEM (no elt. subdi)
C2  CALL      CALL BILMAT2(ZKQL,ZKUL,NBNODES,INODES)
C3  CALL ARG. XYZNOD(NOMM,3)      = Coordinates of discretization nodes
C3            LXYZ(NOM)           = Addresses of possible double nodes
C3            NOM,NOMM            = Number of nodes and max nb. of nodes
C3            ZKQL(NBNODES,NNODE)     = Local Kq matrix of element IE
C3            ZKUL(NBNODES,NNODE)     = Local Ku matrix of element IE
C3            NINTR               = Nb. of reg. and sing. int. pts of IE
C3            WEIGTR              = Regular integr. weights
C3            IP,JP               = current integration point of IE
C3            XYZLO1(MNODM,3)     = MII nodes for IE
C3            NODE1(NNODM)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = 4-nodes for IE
C3            NODE2(NNODM)        = Nodes of 4-node element IE
C3            FIRIP(NNODM,NINTM,.)= Reg. shape fcts. and   )  At all
C3            DFIXIP(),DXIETP()   = their derivatives      )  points IP,JP
C3            XYZPR(NINTM,.,3)    = All integr. point coordinates
C3            ZNPR(NINTM,.,3)     = All integr. point normal vector
C3            ZJACPR(NINTM,NINTM) = All integr. point jacobian
C3            FIS12(),FIS23()     = Sing. shape fcts. and  )  At all
C3            DFIX12(),DFIX23()   = their derivatives      )  points IP,JP
C3            DFIE12(),DFIE23()   = their derivatives      )  and 4-nodes
C3            FI(NNODM)           = Shape functions and    )  At point
C3            DFIX(NNODM),DFIE()  = their derivatives      )  IP,JP
C3            XYZP(3)             = Current integr. point coordinates
C3            ZNP(3)              = Current integr. point normal vector
C3            ZJACP               = Current integr. point jacobian
C4  RET. ARG. ZKUL,ZKQL, for element IE
C6  INT.CALL  CARAC2,SAVGEO
C9  Jan. 99   S. GRILLI, INLN
C9            adapetd for FMA, C.Fochesato, CMLA, 2003
CLBILMAT SUB. WHICH COMPUTES THE MATRIX Kul AND Kql OF IE FOR THE 3D-BEM
C-----------------------------------------------------------------------

      IMPLICIT REAL*8 (A-H,O-Z)
C
      INCLUDE 'param.inc'
      COMMON /DATGEN/ DUM1(2),H0,DUM1B(2),IDUM1(8),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*4),IFACE(MOMM),
     .                ISUB(MOMM),INTG(MOMM),LXYZ(NOMM,2),IDUM3(36),
     .                JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),INODF(6,2),
     .                IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/ DUM3(NINTM),WEIGTR(NINTM),DUM4(2*NINTM),
     .               RMIP12(NINTM),RMIP23(NINTM),
     .               RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .               XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .               ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM5(6),
     .                ZNP(3),ZJACP
      COMMON /FSCUB/ DUM6(4*NDM*NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),XYZPR(NINTM,NINTM,3),
     .               ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
     .               FIS23(MNODM,NINTM,NINTM,MNOD),
     .               DFIX12(MNODM,NINTM,NINTM,MNOD),
     .               DFIX23(MNODM,NINTM,NINTM,MNOD),
     .               DFIE12(MNODM,NINTM,NINTM,MNOD),
     .               DFIE23(MNODM,NINTM,NINTM,MNOD)
      COMMON /BOTFCT/ DUMBOT(NXM*NYM+2),IBOT
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION ZKQL(NBNODES,MNODM),ZKUL(NBNODES,MNODM),XX(3)
      DIMENSION R_V(NBNODES),DGNUM_V(NBNODES),GP_V(NBNODES)
      DIMENSION RP_V(NBNODES),DGPNUM_V(NBNODES),DGDNP_V(NBNODES)
      DIMENSION HP_V(NBNODES),YP_V(NBNODES)
      DIMENSION INODES(NBNODES),ISING_V(NBNODES)
      DIMENSION XYZNOD_V(NBNODES,3)
C
      SAVE
C
      DATA ZERO/0.D0/,S64/64.D0/,PII4/0.7957747154594768D-01/,
     .     ONE/1.D0/,TWO/2.D0/,FOUR/4.D0/,EPS/1.D-08/
C
C.....Initialisation of local matrices
C
      NNODM = NNODE*NNODE
      DO J=1,NNODM
         DO I=1,NBNODES
            ZKQL(I,J) = ZERO
            ZKUL(I,J) = ZERO
         END DO
      END DO
      DO J=1,3
         DO I=1,NBNODES
            XYZNOD_V(I,J)=XYZNOD(INODES(I),J)
	 END DO
      END DO
C
C.....Regular integration
C
      DO IP=1,NINTR
         DO JP=1,NINTR
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
               DFIX(J) = DFIXIP(J,IP,JP)
               DFIE(J) = DFIETP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Save geometry in common INTEG
C
            DO J=1,3
               XYZPR(IP,JP,J) = XYZP(J)
               ZNPR(IP,JP,J)  = ZNP(J)
            END DO
            ZJACPR(IP,JP) = ZJACP
C
C           Final Gauss Jacobian
C
            ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
            CST   = PII4*ZJACP
C
C        ...Computation of ZKUL, ZKQL
C
            DGNUM_V(1:NBNODES) = ZERO
            R_V(1:NBNODES)     = ZERO
C
            DO J=1,3
               DO IL=1,NBNODES
                  DGNUM_V(IL)=DGNUM_V(IL)+(XYZP(J)
     .                     -XYZNOD_V(IL,J))*ZNP(J)
                  R_V(IL)    =R_V(IL) +
     .              (XYZP(J)-XYZNOD_V(IL,J))*(XYZP(J)-XYZNOD_V(IL,J))
               END DO
            END DO
C
            R_V(1:NBNODES) = DSQRT(R_V(1:NBNODES))
C
            DO IL=1,NBNODES
               GP_V(IL)    = CST/R_V(IL)
               DGDNP_V(IL) = -CST*DGNUM_V(IL)/(R_V(IL)*R_V(IL)*R_V(IL))
            END DO
C
C        comment mettre a 0 GP,DGNUM pour les points de l'elt (vectorisable) ?
C
            ISING_V(1:NBNODES) = 0
            DO IL=1,NBNODES
	      L=INODES(IL)
	      DO I=1,NNODE
	        IF (L.EQ.NODE2(I)) THEN
                  GP_V(IL)=ZERO
                  DGDNP_V(IL)=ZERO
		  ISING_V(IL) = 1
		END IF
              END DO
	    END DO
C
C              Image method flat bottom (affects regular/one sg. integrals)
C
            IF(IBOT.EQ.10) THEN
               RP_V(1:NBNODES)     = ZERO
               DO IL=1,NBNODES
                  HP_V(IL) = H0 + XYZNOD_V(IL,3)
               END DO
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part
C
                  DO IL=1,NBNODES
	           IF((DABS(HP_V(IL)).GT.EPS)
     .                              .OR.(ISING_V(IL).EQ.0)) THEN
		     RP_V(IL) = R_V(IL)*R_V(IL)
     .                +FOUR*HP_V(IL)*(HP_V(IL)+(XYZP(3)-XYZNOD_V(IL,3)))
		     DGPNUM_V(IL) = DGNUM_V(IL) + TWO*HP_V(IL)*ZNP(3)
	           END IF
                  END DO
                  RP_V(1:NBNODES) = DSQRT(RP_V(1:NBNODES))
                  DO IL=1,NBNODES
	           IF((DABS(HP_V(IL)).GT.EPS)
     .                              .OR.(ISING_V(IL).EQ.0)) THEN
		     GP_V(IL)     = GP_V(IL) + CST/RP_V(IL)
		     DGDNP_V(IL)  = DGDNP_V(IL) - CST*DGPNUM_V(IL)/
     .                             (RP_V(IL)*RP_V(IL)*RP_V(IL))
	           END IF
                  END DO
	    END IF
C
CSYM        Image method y-symmetry (affects regular/one sg. integrals)
C
            IF(ISYM.EQ.1) THEN
               RP_V(1:NBNODES)     = ZERO
               DO IL=1,NBNODES
                  YP_V(IL) = XYZNOD_V(IL,2)
               END DO
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part
C
                  DO IL=1,NBNODES
 	           IF((DABS(YP_V(IL)).GT.EPS)
     .                                .OR.(ISING_V(IL).EQ.0)) THEN
		     RP_V(IL) = R_V(IL)*R_V(IL)
     .                          +FOUR*YP_V(IL)*XYZP(2)
		     DGPNUM_V(IL) = DGNUM_V(IL) + TWO*YP_V(IL)*ZNP(2)
	           END IF
                  END DO
                  RP_V(1:NBNODES) = DSQRT(RP_V(1:NBNODES))
                  DO IL=1,NBNODES
 	           IF((DABS(YP_V(IL)).GT.EPS)
     .                                .OR.(ISING_V(IL).EQ.0)) THEN
		     GP_V(IL)     = GP_V(IL) + CST/RP_V(IL)
		     DGDNP_V(IL)  = DGDNP_V(IL) - CST*DGPNUM_V(IL)/
     .                              (RP_V(IL)*RP_V(IL)*RP_V(IL))
	           END IF
                  END DO
C
C                 + Image method flat bottom (affects regular/one sg. integrals)
C
                  IF(IBOT.EQ.10) THEN
C
                     RP_V(1:NBNODES)     = ZERO
                        DO IL=1,NBNODES
	                 IF((DABS(HP_V(IL)).GT.EPS)
     .                                .OR.(ISING_V(IL).EQ.0)) THEN
		           RP_V(IL) = R_V(IL)*R_V(IL)
     .               +FOUR*YP_V(IL)*XYZP(2)
     .               +FOUR*HP_V(IL)*(HP_V(IL)+(XYZP(3)-XYZNOD_V(IL,3)))
		           DGPNUM_V(IL) = DGNUM_V(IL)
     .                       +TWO*(YP_V(IL)*ZNP(2)+HP_V(IL)*ZNP(3))
	                 END IF
                        END DO
                        RP_V(1:NBNODES) = DSQRT(RP_V(1:NBNODES))
                        DO IL=1,NBNODES
	                 IF((DABS(HP_V(IL)).GT.EPS)
     .                                .OR.(ISING_V(IL).EQ.0)) THEN
		           GP_V(IL)    = GP_V(IL) + CST/RP_V(IL)
		           DGDNP_V(IL) = DGDNP_V(IL)-CST*DGPNUM_V(IL)/
     .                             (RP_V(IL)*RP_V(IL)*RP_V(IL))
	                 END IF
                        END DO
	          END IF
	    END IF
CMYS
C
C              Regular integrals
C
            DO J=1,NNODM
	       DO IL=1,NBNODES
                  ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP_V(IL)
                  ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP_V(IL)
	       END DO
            END DO
         END DO
      END DO
C
C.....Save for later use of geometric and intrinsic data
C
C      CALL SAVGEO
C     -----------
C
C.....Singular integrals for ZKUL
C
      DO IL=1,NBNODES
         L=INODES(IL)
         DO I=1,NNODE
          IF (L.EQ.NODE2(I)) THEN
C
            DO IP=1,NINTR
              DO JP=1,NINTR
               CS12 = RMIP12(IP)*WEIGTR(IP)*WEIGTR(JP)/
     .                S64
               CS23 = RMIP23(IP)*WEIGTR(IP)*WEIGTR(JP)/
     .                S64
C
C           ...Gauss integration point intrins. coord. and weight for 12
C              unit vectors and Jacob.
C
               DO J=1,NNODM
                  FI(J)   = FIS12(J,IP,JP,I)
                  DFIX(J) = DFIX12(J,IP,JP,I)
                  DFIE(J) = DFIE12(J,IP,JP,I)
               END DO
C
               CALL CARAC2
C              -----------
C
               R     = ZERO
               DGNUM = ZERO
               DO J=1,3
                  XX(J) = XYZP(J) - XYZNOD(L,J)
                  DGNUM = DGNUM + XX(J)*ZNP(J)
                 R     = R + XX(J)*XX(J)
               END DO
               R     = DSQRT(R)
               GP    = CS12*ZJACP*RIJP12(IP,JP)/R
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + XYZNOD(L,3)
		  IF(DABS(HP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM + HP*ZNP(3))
		  END IF
	       END IF
CSYM
C              Test for lateral nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(ISYM.EQ.1) THEN
                  YP = XYZNOD(L,2)
		  IF(DABS(YP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM+YP*ZNP(2))
		  END IF
	       END IF
CMYS
               DGDNP = -CS12*ZJACP*RIJP12(IP,JP)*DGNUM/(R*R*R)
C
               DO J=1,NNODM
                 ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP
                 ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP
               END DO
C
C           ...Gauss integration point intrins. coord. and weight for 23
C              unit vectors and Jacob.
C
               DO J=1,NNODM
                  FI(J)   = FIS23(J,IP,JP,I)
                  DFIX(J) = DFIX23(J,IP,JP,I)
                  DFIE(J) = DFIE23(J,IP,JP,I)
               END DO
C
               CALL CARAC2
C              -----------
C
               R     = ZERO
               DGNUM = ZERO
               DO J=1,3
                  XX(J) = XYZP(J) - XYZNOD(L,J)
                  DGNUM = DGNUM + XX(J)*ZNP(J)
                  R     = R + XX(J)*XX(J)
               END DO
               R     = DSQRT(R)
               GP    = CS23*ZJACP*RIJP23(IP,JP)/R
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + XYZNOD(L,3)
		  IF(DABS(HP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM + HP*ZNP(3))
		  END IF
	       END IF
CSYM
C              Test for lateral nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(ISYM.EQ.1) THEN
                  YP = XYZNOD(L,2)
		  IF(DABS(YP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM+YP*ZNP(2))
		  END IF
	       END IF
CMYS
               DGDNP = -CS23*ZJACP*RIJP23(IP,JP)*DGNUM/(R*R*R)
C
               DO J=1,NNODM
                 ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP
                 ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP
               END DO
              END DO
	    END DO
	  END IF
        END DO
      END DO
C
C.....Double/triple nodes, taking into account for the sing. and part. integ.
C
      DO IL=1,NBNODES
         L=INODES(IL)
         DO I=1,NNODE
            IF (L.EQ.NODE2(I)) THEN
               DO IK=1,NBNODES
                  LK=INODES(IK)
                  IF(LXYZ(L,1).EQ.LK.OR.LXYZ(L,2).EQ.LK) THEN
                    DO J=1,NNODM
                       ZKUL(IK,J) = ZKUL(IL,J)
                       ZKQL(IK,J) = ZKQL(IL,J)
                    END DO
                  END IF
               END DO
           END IF
        END DO
      END DO
C
      RETURN
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE BIDMAT2_V(ZKQL,ZKUL,IE,NBNODES,INODES)
C-----------------------------------------------------------------------
C1  BIDMATF   BIDMAT2
C1  PURPOSE   Compute the local Kul and Kql to the elemt IE
C1            for the NBNODES of INODES and save
C1            geometric and intrinsic data for the 3D-BEM (with elt. subdi)
C2  CALL      CALL BIDMAT2(ZKQL,ZKUL,IE,NBNODES,INODES)
C3  CALL ARG. XYZNOD(NOMM,3)      = Coordinates of discretization nodes
C3            LXYZ(NOM)           = Addresses of possible double nodes
C3            LSUB(NOM)           = Subdivision vector for IE
C3            ALMAXB              = Limit angle for subdivisions
C3            NOM,NOMM            = Number of nodes and max nb. of nodes
C3            ZKQL(NBNODES,NNODE)     = Local Kq matrix of element IE
C3            ZKUL(NBNODES,NNODE)     = Local Ku matrix of element IE
C3            NINTR               = Nb. of reg. and sing. int. pts of IE
C3            WEIGTR              = Regular integr. weights
C3            IP,JP               = current integration point of IE
C3            XYZLO1(MNODM,3)     = MII nodes for IE
C3            NODE1(NNODM)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = 4-nodes for IE
C3            NODE2(NNODM)        = Nodes of 4-node element IE
C3            FIRIP(NNODM,NINTM,.)= Reg. shape fcts. and   )  At all
C3            DFIXIP(),DXIETP()   = their derivatives      )  points IP,JP
C3            XYZPR(NINTM,.,3)    = All integr. point coordinates
C3            ZNPR(NINTM,.,3)     = All integr. point normal vector
C3            ZJACPR(NINTM,NINTM) = All integr. point jacobian
C3            FIS12(),FIS23()     = Sing. shape fcts. and  )  At all
C3            DFIX12(),DFIX23()   = their derivatives      )  points IP,JP
C3            DFIE12(),DFIE23()   = their derivatives      )  and 4-nodes
C3            FI(NNODM)           = Shape functions and    )  At point
C3            DFIX(NNODM),DFIE()  = their derivatives      )  IP,JP
C3            XYZP(3)             = Current integr. point coordinates
C3            ZNP(3)              = Current integr. point normal vector
C3            ZJACP               = Current integr. point jacobian
C4  RET. ARG. ZKUL,ZKQL, for element IE
C6  INT.CALL  CARAC2,SAVGEO
C9  Feb. 99   S. GRILLI, INLN
C9            adapetd for FMA, C.Fochesato, CMLA, 2003
CLBIDMAT SUB. WHICH COMPUTES THE MATRIX Kul AND Kql OF IE FOR THE 3D-BEM
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
      INCLUDE 'param.inc'
      LOGICAL IOUT
C
      COMMON /DATGEN/ DUM1(2),H0,DUM1B(2),IDUM1(8),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),DUM2(18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*4),IFACE(MOMM),
     .                ISUB(MOMM),INTG(MOMM),LXYZ(NOMM,2),IDUM3(36),
     .                JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),INODF(6,2),
     .                IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/ ETARP(NINTM),WEIGTR(NINTM),DUM4(2*NINTM),
     .               RMIP12(NINTM),RMIP23(NINTM),
     .               RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .               XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .               ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM5(6),
     .                ZNP(3),ZJACP
      COMMON /FSCUB/ DUM6(4*NDM*NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),XYZPR(NINTM,NINTM,3),
     .               ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
     .               FIS23(MNODM,NINTM,NINTM,MNOD),
     .               DFIX12(MNODM,NINTM,NINTM,MNOD),
     .               DFIX23(MNODM,NINTM,NINTM,MNOD),
     .               DFIE12(MNODM,NINTM,NINTM,MNOD),
     .               DFIE23(MNODM,NINTM,NINTM,MNOD)
      COMMON /SUBDI/ ALMAXB,LSUB(NOMM)
      COMMON /BOTFCT/ DUMBOT(NXM*NYM+2),IBOT
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION ZKQL(NBNODES,MNODM),ZKUL(NBNODES,MNODM),XX(3)
      DIMENSION R_V(NOMM),DGNUM_V(NOMM),GP_V(NOMM)
      DIMENSION FSFX(NDM),FSDX(NDM),FSFE(NDM),FSDE(NDM),DISND(MNOD)
      DIMENSION RP_V(NBNODES),DGPNUM_V(NBNODES),DGDNP_V(NBNODES)
      DIMENSION HP_V(NBNODES),YP_V(NBNODES)
      DIMENSION INODES(NBNODES),ISING_V(NBNODES)
      DIMENSION XYZNOD_V(NBNODES,3)
C
      SAVE
C
      DATA ZERO/0.D0/,QUART/0.25D0/,HALF/0.5D0/,ONE/1.0D0/,
     .     TWO/2.D0/,THREE/3.D0/,FOUR/4.D0/,EIGHT/8.D0/,S64/64.D0/,
     .     PII4/0.7957747154594768D-01/,DLOG2I/3.321928094D0/,
     .     CRAT/0.4999D0/,OTHREE/0.3333333333333333D0/,EPS/1.D-08/
C
C.....Initialisation of local matrices
C
      NNODM = NNODE*NNODE
      DO J=1,NNODM
         DO IL=1,NBNODES
            ZKQL(IL,J) = ZERO
            ZKUL(IL,J) = ZERO
         END DO
      END DO
      DO J=1,3
         DO I=1,NBNODES
            XYZNOD_V(I,J)=XYZNOD(INODES(I),J)
	 END DO
      END DO
C
C.....Computation of subdivisions for IE
C
      EFJXI = XICON(IE,1)
      EFJET = XICON(IE,2)
C
C.....Regular integration
C
      DO IP=1,NINTR
         DO JP=1,NINTR
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
               DFIX(J) = DFIXIP(J,IP,JP)
               DFIE(J) = DFIETP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Save geometry in common INTEG
C
            DO J=1,3
               XYZPR(IP,JP,J) = XYZP(J)
               ZNPR(IP,JP,J)  = ZNP(J)
            END DO
            ZJACPR(IP,JP) = ZJACP
C
C           Final Gauss Jacobian
C
            ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
C
C        ...Computation of ZKUL, ZKQL
C
            NSG  = 2**NSUB
	    NSG2 = NSG*NSG
            DSG  = TWO/FLOAT(NSG)
C
            DO ISG=1,NSG
	       XILI = TWO*(ISG-1)/FLOAT(NSG) - ONE
	       XIRI = XILI + DSG
	       XIM  = HALF*(XILI + XIRI)
	       XI   = ETARP(IP)/FLOAT(NSG) + XIM
               CHI  = (XI + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSFX,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSDX,NNODE)
C              -----------
               DO JSG=1,NSG
	          ETLI = TWO*(JSG-1)/FLOAT(NSG) - ONE
		  ETRI = ETLI + DSG
	          ETM  = HALF*(ETLI + ETRI)
		  ETA  = ETARP(JP)/FLOAT(NSG) + ETM
                  CHI  = (ETA + ONE)*OTHREE + EFJET
                  CALL SHAPFN(CHI,FSFE,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDE,NNODE)
C                 -----------
C
C                 Shape functions at points IP,JP,ISG,JSG
C
                  ILOC = 0
                  DO J=1,NNODE
		     DO I=1,NNODE
                        ILOC = ILOC + 1
                        FI(ILOC)   = FSFX(I)*FSFE(J)
                        DFIX(ILOC) = OTHREE*FSDX(I)*FSFE(J)
                        DFIE(ILOC) = OTHREE*FSFX(I)*FSDE(J)
                     END DO
                  END DO
C
C                 Cord., unit vectors and Jacob.
C
                  CALL CARAC2
C                 -----------
C
                  ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
                  CST   = PII4*ZJACP/FLOAT(NSG2)
C
                  DGNUM_V(1:NBNODES) = ZERO
                  R_V(1:NBNODES)     = ZERO
C
                  DO J=1,3
                     DO IL=1,NBNODES
                        DGNUM_V(IL) = DGNUM_V(IL) +
     .                                (XYZP(J)-XYZNOD_V(IL,J))*ZNP(J)
                        R_V(IL)     = R_V(IL) +
     .	         (XYZP(J)-XYZNOD_V(IL,J))*(XYZP(J)-XYZNOD_V(IL,J))
                     END DO
                  END DO
C
                  R_V(1:NBNODES) = DSQRT(R_V(1:NBNODES))
C
                  DO IL=1,NBNODES
                     GP_V(IL)    = CST/R_V(IL)
                     DGDNP_V(IL) = -CST*DGNUM_V(IL)/
     .                               (R_V(IL)*R_V(IL)*R_V(IL))
                  END DO
C
C                 Kernel transformation, in case of L on 4-node IE
C
C        comment mettre a 0 GP,DGNUM pour les points de l'elt (vectorisable) ?
C
                  ISING_V(1:NBNODES) = 0
                  DO IL=1,NBNODES
	            L=INODES(IL)
	            DO I=1,NNODE
	               IF (L.EQ.NODE2(I)) THEN
                          GP_V(IL)=ZERO
                          DGDNP_V(IL)=ZERO
		          ISING_V(IL) = 1
		       END IF
                    END DO
	          END DO
C
C              Image method flat bottom (affects regular/one sg. integrals)
C
            IF(IBOT.EQ.10) THEN
               RP_V(1:NBNODES)     = ZERO
               DO IL=1,NBNODES
                  HP_V(IL) = H0 + XYZNOD_V(IL,3)
               END DO
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part
C
                  DO IL=1,NBNODES
	           IF((DABS(HP_V(IL)).GT.EPS)
     .                              .OR.(ISING_V(IL).EQ.0)) THEN
		     RP_V(IL) = R_V(IL)*R_V(IL)
     .                +FOUR*HP_V(IL)*(HP_V(IL)+(XYZP(3)-XYZNOD_V(IL,3)))
		     DGPNUM_V(IL) = DGNUM_V(IL) + TWO*HP_V(IL)*ZNP(3)
	           END IF
                  END DO
                  RP_V(1:NBNODES) = DSQRT(RP_V(1:NBNODES))
                  DO IL=1,NBNODES
	           IF((DABS(HP_V(IL)).GT.EPS)
     .                              .OR.(ISING_V(IL).EQ.0)) THEN
		     GP_V(IL)     = GP_V(IL) + CST/RP_V(IL)
		     DGDNP_V(IL)  = DGDNP_V(IL) - CST*DGPNUM_V(IL)/
     .                             (RP_V(IL)*RP_V(IL)*RP_V(IL))
	           END IF
                  END DO
	    END IF
C
CSYM        Image method y-symmetry (affects regular/one sg. integrals)
C
            IF(ISYM.EQ.1) THEN
               RP_V(1:NBNODES)     = ZERO
               DO IL=1,NBNODES
                  YP_V(IL) = XYZNOD_V(IL,2)
               END DO
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part
C
                  DO IL=1,NBNODES
 	           IF((DABS(YP_V(IL)).GT.EPS)
     .                                .OR.(ISING_V(IL).EQ.0)) THEN
		     RP_V(IL) = R_V(IL)*R_V(IL)
     .                          +FOUR*YP_V(IL)*XYZP(2)
		     DGPNUM_V(IL) = DGNUM_V(IL) + TWO*YP_V(IL)*ZNP(2)
	           END IF
                  END DO
                  RP_V(1:NBNODES) = DSQRT(RP_V(1:NBNODES))
                  DO IL=1,NBNODES
 	           IF((DABS(YP_V(IL)).GT.EPS)
     .                                .OR.(ISING_V(IL).EQ.0)) THEN
		     GP_V(IL)     = GP_V(IL) + CST/RP_V(IL)
		     DGDNP_V(IL)  = DGDNP_V(IL) - CST*DGPNUM_V(IL)/
     .                              (RP_V(IL)*RP_V(IL)*RP_V(IL))
	           END IF
                  END DO
C
C                 + Image method flat bottom (affects regular/one sg. integrals)
C
                  IF(IBOT.EQ.10) THEN
C
                     RP_V(1:NBNODES)     = ZERO
                        DO IL=1,NBNODES
	                 IF((DABS(HP_V(IL)).GT.EPS)
     .                                .OR.(ISING_V(IL).EQ.0)) THEN
		           RP_V(IL) = R_V(IL)*R_V(IL)
     .               +FOUR*YP_V(IL)*XYZP(2)
     .               +FOUR*HP_V(IL)*(HP_V(IL)+(XYZP(3)-XYZNOD_V(IL,3)))
		           DGPNUM_V(IL) = DGNUM_V(IL)
     .                       +TWO*(YP_V(IL)*ZNP(2)+HP_V(IL)*ZNP(3))
	                 END IF
                        END DO
                        RP_V(1:NBNODES) = DSQRT(RP_V(1:NBNODES))
                        DO IL=1,NBNODES
	                 IF((DABS(HP_V(IL)).GT.EPS)
     .                                .OR.(ISING_V(IL).EQ.0)) THEN
		           GP_V(IL)    = GP_V(IL) + CST/RP_V(IL)
		           DGDNP_V(IL) = DGDNP_V(IL)-CST*DGPNUM_V(IL)/
     .                             (RP_V(IL)*RP_V(IL)*RP_V(IL))
	                 END IF
                        END DO
	          END IF
	    END IF
CMYS
C
C                 Regular integrals
C
                  DO J=1,NNODM
	             DO IL=1,NBNODES
                        ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP_V(IL)
                        ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP_V(IL)
	             END DO
		  END DO
	       END DO
            END DO
         END DO
      END DO
C
C.....Save for later use of geometric and intrinsic data
C
C      CALL SAVGEO
C     -----------
C
C.....Singular integrals for ZKUL,(ZKQL)
C
      DO IL=1,NBNODES
        L=INODES(IL)
        DO I=1,NNODE
          IF (L.EQ.NODE2(I)) THEN
            DO IP=1,NINTR
              DO JP=1,NINTR
               CS12 = RMIP12(IP)*WEIGTR(IP)*WEIGTR(JP)/
     .                S64
               CS23 = RMIP23(IP)*WEIGTR(IP)*WEIGTR(JP)/
     .                S64
C
C           ...Gauss integration point intrins. coord. and weight for 12
C              unit vectors and Jacob.
C
               DO J=1,NNODM
                  FI(J)   = FIS12(J,IP,JP,I)
                  DFIX(J) = DFIX12(J,IP,JP,I)
                  DFIE(J) = DFIE12(J,IP,JP,I)
               END DO
C
               CALL CARAC2
C              -----------
C
               R     = ZERO
               DGNUM = ZERO
               DO J=1,3
                  XX(J) = XYZP(J) - XYZNOD(L,J)
                  DGNUM = DGNUM + XX(J)*ZNP(J)
                  R     = R + XX(J)*XX(J)
               END DO
               R     = DSQRT(R)
               GP    = CS12*ZJACP*RIJP12(IP,JP)/R
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + XYZNOD(L,3)
		  IF(DABS(HP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM + HP*ZNP(3))
		  END IF
	       END IF
CSYM
C              Test for lateral nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(ISYM.EQ.1) THEN
                  YP = XYZNOD(L,2)
		  IF(DABS(YP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM+YP*ZNP(2))
		  END IF
	       END IF
CMYS
               DGDNP = -CS12*ZJACP*RIJP12(IP,JP)*DGNUM/(R*R*R)
C
               DO J=1,NNODM
                 ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP
                 ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP
               END DO
C
C           ...Gauss integration point intrins. coord. and weight for 23
C              unit vectors and Jacob.
C
               DO J=1,NNODM
                  FI(J)   = FIS23(J,IP,JP,I)
                  DFIX(J) = DFIX23(J,IP,JP,I)
                  DFIE(J) = DFIE23(J,IP,JP,I)
               END DO
C
               CALL CARAC2
C              -----------
C
               R     = ZERO
               DGNUM = ZERO
               DO J=1,3
                  XX(J) = XYZP(J) - XYZNOD(L,J)
                  DGNUM = DGNUM + XX(J)*ZNP(J)
                  R     = R + XX(J)*XX(J)
               END DO
               R     = DSQRT(R)
               GP    = CS23*ZJACP*RIJP23(IP,JP)/R
C
C              Test for bottom nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + XYZNOD(L,3)
		  IF(DABS(HP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM + HP*ZNP(3))
		  END IF
	       END IF
CSYM
C              Test for lateral nodes which lead to sg. integral
C              of the image part with RP=R and np=n
C
               IF(ISYM.EQ.1) THEN
                  YP = XYZNOD(L,2)
		  IF(DABS(YP).LT.EPS) THEN
		     GP    = TWO*GP
		     DGNUM = TWO*(DGNUM+YP*ZNP(2))
		  END IF
	       END IF
CMYS
               DGDNP = -CS23*ZJACP*RIJP23(IP,JP)*DGNUM/(R*R*R)
C
               DO J=1,NNODM
                 ZKQL(IL,J) = ZKQL(IL,J) + FI(J)*DGDNP
                 ZKUL(IL,J) = ZKUL(IL,J) + FI(J)*GP
               END DO
              END DO
	    END DO
	  END IF
        END DO
      END DO
C
C.....Double/triple nodes, taking into account for the sing. and part. integ.
C
      DO IL=1,NBNODES
         L=INODES(IL)
         DO I=1,NNODE
            IF (L.EQ.NODE2(I)) THEN
               DO IK=1,NBNODES
                  LK=INODES(IK)
                  IF(LXYZ(L,1).EQ.LK.OR.LXYZ(L,2).EQ.LK) THEN
                    DO J=1,NNODM
                       ZKUL(IK,J) = ZKUL(IL,J)
                       ZKQL(IK,J) = ZKQL(IL,J)
                    END DO
                  END IF
               END DO
            END IF
        END DO
      END DO
C
      RETURN
C
      END
