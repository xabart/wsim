C-------------------------------------------------------------------------------
C
C                       POST-PROCESSING
C
C-------------------------------------------------------------------------------
C-------------------------------------------------------------------------------
c	old line
c      SUBROUTINE POSTPROC(PHI,PHIN,PHIS,PHIM,PHIT,PHITN,UVW,DUVWDT,PRES,
c     .              TIMEP,ALMAXI,
c     .              IO10,IO11,IO15,IO60,IO61,IO62,IO71,IO72,
c     .              IPRINT,TPRINT,ALMAXB,NHT,ILMAX,ISTART,IGRID,INFIE,
c     .              IPOST,ILOOP,NOM,TIME,DT)
C-------------------------------------------------------------------------------
c	New Lines for the restart, XB
      SUBROUTINE POSTPROC(
     .  	    TIMEP,ALMAXI,
     .  	    IO10,IO11,IO15,IO60,IO61,IO62,IO71,IO72,
     .  	    IPRINT,TPRINT,ALMAXB,NHT,ILMAX,ISTART,IGRID,INFIE,
     .  	    IPOST,ILOOP,NOM,TIME,DT,ILOOP0)
C-------------------------------------------------------------------------------
C-------------------------------------------------------------------------------
C
C     intermediate subroutine for choosing postprocessing
C
C-------------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C-------------------------------------------------------------------------------
      INCLUDE 'param.inc'

C-------------------------------------------------------------------------------
      INTEGER*4 IO1
C-------------------------------------------------------------------------------
c	old line
c      DIMENSION PHI(NOMM),PHIN(NOMM),PHIT(NOMM),PHITN(NOMM),
c     .          PHIS(NOMM),PHIM(NOMM),UVW(3,NOMM),DUVWDT(3,NOMM),
c     .          PRES(NOMM)
c	New line, XB
C-------------------------------------------------------------------------------
      include 'result.h' ! pg(nomm),phi(nomm),phin(nomm),phit(nomm),phitn(nomm),
c                          phis(nomm),phim(nomm),phiss(nomm),phimm(nomm),
c                          phins(nomm),phinm(nomm),phits(nomm),phitm(nomm),
c                          phism(nomm),phinn(nomm),
c                          pres(nomn),dprdt(nomm),uvw(3,nomm),duvwdt uvw(3,nomm)

      DATA IO1/1/

C-------------------------------------------------------------------------------
C.....J:call postprocess every J step
C-------------------------------------------------------------------------------
      J=1
      IF (MOD(ILOOP,J).EQ.0.OR.ILOOP.EQ.ILMAX) THEN
         IF (IPOST.EQ.0) THEN
C-------------------------------------------------------------------------------
C...........classic without interior points
C-------------------------------------------------------------------------------
C           POSTPR adapted for FMA, CF03
C-------------------------------------------------------------------------------
c	Old line, 
c            CALL POSTPR0(PHI,PHIN,PHIT,PHITN,UVW,DUVWDT,ALMAXI,

c	New restart
            CALL POSTPR0(ALMAXI,
C           ------------
     .                  IO10,IO11,IO15,
     .                  IPRINT,TPRINT,ALMAXB,NHT,ILMAX,ISTART,IGRID,
     .                  INFIE)
C-------------------------------------------------------------------------------
         ELSE IF (IPOST.EQ.1) THEN
C-------------------------------------------------------------------------------
C...........coupling with VOF
C-------------------------------------------------------------------------------
C           POSTPR adapted for FMA, CF03
C-------------------------------------------------------------------------------
	    WRITE(*,*) 'Boundary fluxes and internal points'
            IF(INFIE.EQ.1) CALL DISTPI2(IO1)
C                      ------------
	    WRITE(*,*) '   Inside points selected'
c	Old restart
c            CALL POSTPRVOF(PHI,PHIN,PHIT,PHITN,UVW,DUVWDT,ALMAXI,
c	New restart, XB
            CALL POSTPRVOF(ALMAXI,
C           --------------
     .                IO10,IO11,IO15,IO61,IO71,IO72,
     .                IPRINT,TPRINT,ALMAXB,NHT,ILMAX,ISTART,IGRID,INFIE)
	    WRITE(*,*) '   Inside fields calculated'
C-------------------------------------------------------------------------------
         ELSE IF (IPOST.EQ.2) THEN
C-------------------------------------------------------------------------------
C...........freak waves or tsunamis
C-------------------------------------------------------------------------------
C           POSTPR adapted for FMA, CF03
C-------------------------------------------------------------------------------
	ILOOP1= ILOOP+ILOOP0
            CALL POSTPR(TIMEP,
C           -----------
     .               ALMAXI,IO10,IO11,IO15,IO60,IO61,IO62,IO71,IO72,
     .               IPRINT,TPRINT,ALMAXB,NHT,ILMAX,ISTART,IGRID,INFIE)
         END IF
C
c         IF (ISTART.EQ.1) THEN
c            OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/input.dat2')
c            READ(IO,*)ILOOP0
c            ILOOP=ILOOP+ILOOP0
c            CLOSE(IO)
c         END IF  
	ILOOP1= ILOOP+ILOOP0
         CALL OUTPUTVTK(ILOOP1,PHI,PHIN,PHIS,PHIM,PHIT,PHITN,UVW,DUVWDT,
     .                  PRES,ISTART,ILOOP0,IO)
c         IF (ISTART.EQ.1) THEN
c            ILOOP=ILOOP-ILOOP0
c         else 
c         CALL OUTPUTVTK(ILOOP,PHI,PHIN,PHIS,PHIM,PHIT,PHITN,UVW,DUVWDT,
c     .                  PRES,ISTART,ILOOP0,IO)
c	 END IF

      END IF
C-------------------------------------------------------------------------------
C.....test for break point
C-------------------------------------------------------------------------------
C      IF (ISBP.EQ.0) THEN
C         CALL ISBREAKPT(ISBP,ILOOPBP)
C         --------------
C         WRITE(IO6,*) 'ISBP : ',ISBP
C      END IF
C-------------------------------------------------------------------------------
      RETURN
C-------------------------------------------------------------------------------
      END
C-------------------------------------------------------------------------------







C-------------------------------------------------------------------------------
      subroutine outputvtk
     &           (iloop1,phi,phin,phis,phim,phit,phitn,uvw,duvwdt,pres,
     &              ISTART,ILOOP0,IO)
C-------------------------------------------------------------------------------
C     COMMENT: MADE BY CK (2007)
C-------------------------------------------------------------------------------
      implicit none
C-------------------------------------------------------------------------------
      include 'param.inc'

      real*8  xyznod(nomm,3),xicon(momm,2),xstart(6,3)
      integer igcon(momm,mnod,mnod),ilcon(momm,4),iface(momm),
     .        isub(momm),intg(momm),lxyz(nomm,2),icumul(6),
     .        ndst(6,2),idst(6,3),jslim(6),nelef(6),nnodf(6),
     .        ielef(6,2),inodf(6,2),ibcond(6)
      real*8  DUM3(24),TIME,DUM31(2+4+8*MYMAX),IDU(5)

      COMMON /MAILLE/ XYZNOD,XICON,XSTART,
     .                IGCON,ILCON,
     .                IFACE,ISUB,INTG,
     .                LXYZ,ICUMUL,NDST,IDST,
     .                JSLIM,NELEF,NNODF,IELEF,
     .                INODF,IBCOND
      COMMON/SYSTEM/  DUM3,TIME,DUM31,IDU

      real*8  phi(nomm),uvw(3,nomm),duvwdt(3,nomm),pres(nomm),
     .        phin(nomm),phis(nomm),phim(nomm),phit(nomm),phitn(nomm)

      character*50 plot_file_name
      integer iloop,i,j,element_type,ISTART,ILOOP0,ILOOP1,IO

c-------------------------------------------------------------------------------
c taking in account the restart
c-------------------------------------------------------------------------------
c       ILOOP0=0
c       IF (ISTART.EQ.1) THEN 
c            OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/input.dat2')
c            READ(IO,*)ILOOP0
c	    close(IO)
c       endif    

c	ILOOP1=ILOOP+ILOOP0
c-------------------------------------------------------------------------------
c Give a name to the output file
c-------------------------------------------------------------------------------
      write(plot_file_name,3000) 
     .      'output/vtk','wave',iloop1

3000  format(a,'/',a,'_',i6.6,'.vtk')

c      write(6,*) '\n iloop:',iloop1,'\n',plot_file_name,'\n'
      write(6,*) '\n iloop:',iloop1,'\n',plot_file_name,'\n'

c-------------------------------------------------------------------------------
c Write in the output file
c-------------------------------------------------------------------------------
      open(20,file=plot_file_name,status='unknown',form='formatted')
      write(20,3100)

3100  format('# vtk DataFile Version 2.0'/
     . 'Results computed with the BEM code from CMLA 
     . (Author of the plot method: Christophe Kassiotis 
     . (2007), ENS-Cachan, TU-Braunschweig )'/
     . 'ASCII'/
     . 'DATASET UNSTRUCTURED_GRID'/)

C      write(*,31001) TIME
C
C 31001  format('DATA_TIME',2x,ES12.4) 


c-------------------------------------------------------------------------------
c Nodes
c-------------------------------------------------------------------------------
      write(20,3101) nomm

3101  format('POINTS',2x,i7,2x,'float') 
 
      do i = 1,nomm
         write(20,*) ( xyznod(i,j), j = 1,3 )
      enddo
      write(20,*)

c-------------------------------------------------------------------------------
c Element
c-------------------------------------------------------------------------------
      write(20,3102) momm, 5*momm

3102  format('CELLS',2x,i7,2x,i6)

      do i = 1,momm
         write(20,*) 4,( ilcon(i,j)-1, j = 1,4 )
      enddo
      write(20,*)

c-------------------------------------------------------------------------------
c Element type
c-------------------------------------------------------------------------------
      write(20,3103) momm
      element_type = 9

3103  format('CELL_TYPES',2x,i7)

      do i=1,momm
         write(20,*) element_type
      enddo !i
      write(20,*)

c-------------------------------------------------------------------------------
c Datas
c-------------------------------------------------------------------------------

c potential
c----------
      write(20,3201) nomm

3201  format('POINT_DATA',2x,i7/
     .       'SCALARS potential float 1'/
     .       'LOOKUP_TABLE default')

      do i=1,nomm
        write(20,*) phi(i)
      enddo
      write(20,*)

c velocity
c---------

      write(20,3202)

3202  format('VECTORS velocity float')

      do i=1,nomm
        write(20,*) (uvw(j,i), j = 1,3)
      enddo
      write(20,*)

c acceleration
c-------------
      write(20,3203)

3203  format('VECTORS acceleration float')

      do i=1,nomm
	write(20,*) (duvwdt(j,i), j = 1,3)
      enddo
      write(20,*)

c potential gradient
c-------------------
      write(20,3204)

3204  format('VECTORS phi,(n,s,m) float')

      do i=1,nomm
	 write(20,*) phin(i),phis(i),phim(i)
      enddo
      write(20,*)

C d(phi)/dt
C-------------------
      write(20,3205) 

3205  format('SCALARS d(phi)/dt float 1'/
     .       'LOOKUP_TABLE default')

      do i=1,nomm
	write(20,*) phit(i)
      enddo
      write(20,*)

C d2(phi)/(dtdn)
C-------------------
      write(20,3206) 

3206  format('SCALARS d2(phi)/dtdn float 1'/
     .       'LOOKUP_TABLE default')

      do i=1,nomm
	 write(20,*) phitn(i)
      enddo
      write(20,*)

C pressure
C-------------------
      write(20,3207)

3207  format('SCALARS pressure float 1'/
     .       'LOOKUP_TABLE default')

      do i=1,nomm
	 write(20,*) pres(i)
      enddo
      write(20,*)

Ccc phin
Cc-------------------
      write(20,3208)

 3208 format('SCALARS phin float 1'/
     .     'LOOKUP_TABLE default')

      do i=1,nomm
	 write(20,*) phin(i)
      enddo
      write(20,*)


C-------------------------------------------------------------------------------
      close(20)

C-------------------------------------------------------------------------------
      RETURN
C-------------------------------------------------------------------------------
      END
C-------------------------------------------------------------------------------



C-----------------------------------------------------------------------
c      SUBROUTINE POSTPR0(PHI,PHIN,PHIT,PHITN,UVW,DUVWDT,ALMAXI,
      SUBROUTINE POSTPR0(ALMAXI,
     .           IO10,IO11,IO15,IPRINT,TPRINT,
     .           ALMAXB,NHT,ILMAX,ISTART,IGRID,INFIE)
C-----------------------------------------------------------------------
C
C     VOLTOT : Total fluid volume in domain
C
C     adapted for FMA : now compute all necessary (info from bem.f not
C                       available because of disorder?)
C                       future : call to PMTA
C
C                       without interior points
C
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      LOGICAL IFIELD,IFLAB,IBREAK,ITENS
C
      COMMON /DATGEN/ ZL0,W0,H0,X0,D0,MX,MY,MZ,NX,NY,NZ,NSUBC,
     .                NINTRT,NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),
     .                DUM3(18),IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),ICUMUL(6),NDST(6,2),
     .                IDUM2(18),JSLIM(6),NELEF(6),NNODF(6),
     .                IELEF(6,2),INODF(6,2),IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /FSCUB/ FN0XI(NDM,NINTM),FN0ET(NDM,NINTM),
     .               FN1XI(NDM,NINTM),FN1ET(NDM,NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),DUM5(7*NINTM*NINTM)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM6(6),
     .                ZNP(3),ZJAC
      COMMON /INTEG/  ETARP(NINTM),WEIGTR(NINTM),PHIP12(NINTM),
     .                PHIP23(NINTM),RMIP12(NINTM),RMIP23(NINTM),
     .                RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .                XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .                ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /BOUNDC/ DT,DT2,DT22,TSTART,TMAX,RHO,CPRESS,CPREST,GE,
     .                DE1,DE2,OMEGA,V0H,TOLMAX,NBS
      COMMON /DISSIP/ XDAMP,XLDAMP,DNU0,TRESH,SUTENS,COEF(NOMM),
     .                COEFX(NOMM),ISLOP(NOMM),NPOLY,
     .                IFLAB,IBREAK,ITENS
      COMMON /BOTFCT/ DUM13(NXM*NYM),SLOPE,ZK,IBOT
      COMMON /SYSTEM/ DUM11(24),TIME,TDAMP,TDOWN,UPNEW(4,MYMAX),
     .                DUPDT(4,MYMAX),DUM12(4),IO6,IDUV(2),IPTYP,ILOOP
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
c-------------------------------------------------------------------
c	old line
c      DIMENSION PHI(NOM),PHIN(NOM),PHIT(NOM),PHITN(NOM),UVW(3,NOMM),
c     .          DUVWDT(3,NOMM)
c-------------------------------------------------------------------
c	New restart, XB
c-------------------------------------------------------------------
      include 'result.h' ! pg(nomm),phi(nomm),phin(nomm),phit(nomm),phitn(nomm),
c                          phis(nomm),phim(nomm),phiss(nomm),phimm(nomm),
c                          phins(nomm),phinm(nomm),phits(nomm),phitm(nomm),
c                          phism(nomm),phinn(nomm),
c                          pres(nomn),dprdt(nomm),uvw(3,nomm),duvwdt uvw(3,nomm)
c-------------------------------------------------------------------
      DIMENSION DISCH(6),XX(3),DELGP(3),DELGNP(3),DIAG(2),XCIE(3),
     .          VIE(3,3),V12(3),V13(3),DISND(MNOD),FSF(NDM),FSD(NDM)
C
      SAVE
C
      DATA ZERO/0.D0/,TWO/2.D0/,PII4/0.7957747154594768D-01/,
     .     THREE/3.D0/,ONE/1.0D0/,HALF/0.5D0/,QUART/0.25D0/,
     .     DLOG2I/3.321928094D0/,FOUR/4.D0/,CRAT/0.4999D0/,
     .     SIX/6.D0/,OTHREE/0.3333333333333333D0/,EPS/1.D-08/,
     .     PIS8/0.3926990816987241D0/,PIS2/1.570796326794896D0/,
     .     S3/0.3333333333333333D0/
C
C.....Save commons and local variables to enable static commons
C
C.....Initialize flow rates on boundary, total volume and energy
C
CSYM...
      KMAX = 6
      IF(IBOT.EQ.10) KMAX = KMAX-1
      IF(ISYM.EQ.1) KMAX = KMAX-1
      DO K=1,KMAX
         DISCH(K) = ZERO
      END DO
      TANA = DTAN(ALMAXI)
C
      VOLTOT = ZERO
      VOLBOT = ZERO
      EPTOT  = ZERO
      EPBOT  = ZERO
      EKTOT  = ZERO
C
C=====Elements per element integral computations (MAIN LOOP)
C
      JSAVE  = 0
      NINTPR = 0
      NSIDEP = 0
      EFJXIP = ZERO
      EFJETP = ZERO
      NNODE = MNOD
      NNODM = MNODM
C
      DO IE=1,MOM
C
C     ...General parameters for elt. IE
C
         NINTR = INTG(IE)
         NSIDE = IFACE(IE)
C
C        4-node connectivity in NODE1(NDM)
C
         DO J=1,NNODE
            NODE2(J)         = ILCON(IE,J)
            XYZLO2(J,1)      = XYZNOD(NODE2(J),1)
            XYZLO2(J,2)      = XYZNOD(NODE2(J),2)
            XYZLO2(J,3)      = XYZNOD(NODE2(J),3)
         END DO
C
C        Assign NOD1E(NDM) and NNODE for correct assembly of ZK(U/Q)L
C        i.e., considering MII elements
C
         ILOC = 0
         DO J=1,NNODE
            DO I=1,NNODE
               ILOC = ILOC + 1
               NODE1(ILOC)    = IGCON(IE,I,J)
               XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
               XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
               XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYMD
               IF (ISYM.EQ.1) THEN
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.5.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYSD
            END DO
         END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
C     ...Filling up of the common INTEG when the elements have different
C        node or integration points numbers (accelerator)
C
         IRECAL = 1
         IF (NINTR.NE.NINTPR) THEN
C
C           Regular Gauss points
C
            CALL GAUSSP
C           -----------
C
            IRECAL = 0
         END IF
C
C        MII cubic 4-node shape functions
C        Compute shape functions and their derivatives for
C        cubic element on the boundary and store in common FSCUB
C
         IF(EFJXI.NE.EFJXIP.OR.NINTR.NE.NINTPR) THEN
C
C           XI direction
C
            IRECAL = 0
            DO IP=1,NINTR
               CHI = (ETARP(IP) + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0XI(J,IP) = FSF(J)
                  FN1XI(J,IP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
         IF(EFJET.NE.EFJETP.OR.NINTR.NE.NINTPR) THEN
C
C           ETA direction
C
            IRECAL = 0
            DO JP=1,NINTR
               CHI = (ETARP(JP) + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0ET(J,JP) = FSF(J)
                  FN1ET(J,JP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
C
C        Calculates bidim shape functions at all reg/sing integ. points
C
         IF(IRECAL.EQ.0) THEN
            ILOC = 0
            DO J=1,NNODE
               DO I=1,NNODE
                  ILOC = ILOC + 1
                  DO IP=1,NINTR
                     DO JP=1,NINTR
C
C                       Shape functions at all regular integ. points
C
                        FIRIP(ILOC,IP,JP)  = FN0XI(I,IP)*FN0ET(J,JP)
                        DFIXIP(ILOC,IP,JP) = FN1XI(I,IP)*FN0ET(J,JP)
                        DFIETP(ILOC,IP,JP) = FN0XI(I,IP)*FN1ET(J,JP)
                     END DO
                  END DO
               END DO
            END DO
         END IF
C
C     ...Start integral loop
C
         DISCHE = ZERO
         DO IP=1,NINTR
            DO JP=1,NINTR
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
               DFIX(J) = DFIXIP(J,IP,JP)
               DFIE(J) = DFIETP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Final Gauss Jacobian
C
            WWJAC = ZJAC*WEIGTR(IP)*WEIGTR(JP)
C
C              Compute Phi,Phin,... at integration points by
C              shape functions summation and contributions to volume and
C              energy for element IE, int. pt. IP,JP
C
               PHIP   = ZERO
               PHINP  = ZERO
               PHITP  = ZERO
               PHITNP = ZERO
C
               DO I=1,NNODM
                  L      = NODE1(I)
                  PHIP   = PHIP   + FI(I)*PHI(L)
                  PHINP  = PHINP  + FI(I)*PHIN(L)
                  PHITP  = PHITP  + FI(I)*PHIT(L)
                  PHITNP = PHITNP + FI(I)*PHITN(L)
               END DO
C
C              Compute kinetic and potential energy, volume, local flux
C
               EKTOT  = EKTOT  + PHIP*PHINP*WWJAC
	       EPTOT  = EPTOT  + XYZP(3)*XYZP(3)*ZNP(3)*WWJAC
	       VOLTOT = VOLTOT + XYZP(3)*ZNP(3)*WWJAC
	       DISCHE = DISCHE + PHINP*WWJAC
C
C              Below z=0 values for possible corrections
C
	       IF(IFACE(IE).NE.1) THEN
	          VOLBOT = VOLBOT + XYZP(3)*ZNP(3)*WWJAC
		  EPBOT  = EPBOT  + XYZP(3)*XYZP(3)*ZNP(3)*WWJAC
	       END IF
            END DO
         END DO
C
C........Flux per side
C
         DISCH(NSIDE) = DISCH(NSIDE) + DISCHE
C
         NINTPR = NINTR
         NSIDEP = NSIDE
         EFJXIP = EFJXI
         EFJETP = EFJET
      END DO
CBOT
      IF (IBOT.EQ.10) THEN
         EPTOT = EPTOT - NX*NY*H0*H0
         VOLTOT = VOLTOT + NX*NY*H0
         EPBOT = EPBOT - NX*NY*H0*H0
         VOLBOT = VOLBOT + NX*NY*H0         
      END IF
CSYM
      IF (ISYM.EQ.1) THEN
         EPTOT = TWO*EPTOT
         VOLTOT = TWO*VOLTOT
         EPBOT = TWO*EPBOT
         VOLBOT = TWO*VOLBOT
         DO K=1,KMAX
            DISCH(K) = TWO*DISCH(K)
         END DO
      END IF
C
C.....Dimensional energies
C
      EPTOT  = HALF*GE*RHO*EPTOT
      EPBOT  = HALF*GE*RHO*EPBOT
      EPSUR  = EPTOT - EPBOT
      VOLSUR = VOLTOT - VOLBOT
      EKTOT  = HALF*RHO*EKTOT
      ETTOT  = EPTOT + EKTOT
C
C.....Total flux
C
      OUTFLO = ZERO
C
      DO K=1,KMAX
         OUTFLO = OUTFLO + DISCH(K)
      END DO
C
C.....Output results on files
C
C      WRITE(IO6,2000) TIME,EPTOT,EKTOT,ETTOT,VOLTOT,OUTFLO,EPSUR,VOLSUR
C      WRITE(IO6,2000) (DISCH(K),K=1,6)
C      WRITE(IO10,2000) TIME,EKTOT,EPTOT,ETTOT,EPSUR,EPSUR+EKTOT
      WRITE(IO10,2002) TIME,EKTOT,EPTOT,ETTOT,EPSUR,EPSUR+EKTOT,NX,NY,NZ
      WRITE(IO11,2000) TIME,VOLTOT,VOLSUR,OUTFLO,OUTFLO/VOLSUR
C
C.....Restart results every IPRINT loop
C
      IF(IPRINT.GT.0) THEN
         IF(DABS(DFLOAT(ILOOP)/DFLOAT(IPRINT)-ILOOP/IPRINT).LT.EPS
     .      .OR.ILOOP.GE.ILMAX.OR.(TIME+DT).GE.TMAX
     .      .OR.((TIME+DT).GE.TPRINT.AND.TIME.LT.TPRINT)
     .      ) THEN
	    TMAXC  = DT*ILMAX
	    ISTART = 1
	    WRITE(IO15,*) MX,MY,MZ,NINTRT,NSUBC,ALMAXB,ALMAXI,
     .                   ' : MX,MY,MZ,NINTR,NSUBC,ALMAXB,ALMAXI'
            WRITE(IO15,*) ZL0,W0,H0,X0,D0,IBOT,SLOPE,ZK,
     .                   ' : ZL0,W0,H0,X0,D0,IBOT,SLOPE,ZK'
            WRITE(IO15,*) (IBCOND(K),K=1,6),' : (IBCOND(K),K=1,6)'
            WRITE(IO15,*) ILMAX,IPTYP,NBS,IPRINT,
     .                   ' : ILMAX,IPTYP,NBS,IPRINT'
            WRITE(IO15,*) DT,TIME,TMAXC,RHO,CPRESS,CPREST,GE,TDAMP,
     .                    TDOWN, 
     .                   ' : DT,TSTART,TMAX,RHO,CPRESS,CPREST,GE,TDAMP'
            WRITE(IO15,*) DE1,DE2,OMEGA,V0H,INFIE,NOIX,NOIY,NOIZ,NHT,
     .                    TOLMAX,
     .                   ' : DE1,DE2,OMEGA,V0H,INFIE,NOIX,NOIY,NOIZ,NHT,
     .                       TOLMAX'
            WRITE(IO15,*) XLDAMP,DNU0,TRESH,SUTENS,NPOLY,
     .                   ' : XLDAMP,DNU0,TRESH,SUTENS,NPOLY'
            WRITE(IO15,*) ISTART,IGRID,' : ISTART,IGRID'
C
 1666	    format(3(i7,1x),2(E24.16,1x),i7)    
c            WRITE(IO15,*) NOM,NX,NY,TIME,DT,ILOOP
            WRITE(IO15,1666) NOM,NX,NY,TIME,DT,ILOOP
            DO I=1,NOM

c-----------------------------------------------------------------
c	oldline
c	       WRITE(IO15,2020) I,XYZNOD(I,1),XYZNOD(I,2),XYZNOD(I,3),
c     .                          PHI(I),PHIN(I)
c-----------------------------------------------------------------
c	New restart, XB
            WRITE(IO15,2001) I,XYZNOD(I,1),XYZNOD(I,2),XYZNOD(I,3),
     &                  PHI(I),PHIN(I),phit(i),phitn(i),
     &			  phis(i),phim(i),phiss(i),phimm(i),
     &			  phins(i),phinm(i),phits(i),phitm(i),
     &			  phism(i),phinn(i),
     &			  pres(i),dprdt(i),uvw(1,i),uvw(2,i),
     &			  uvw(3,i),
     &			  duvwdt(1,i),duvwdt(2,i),duvwdt(3,i)
	    END DO
	 END IF
      END IF
C
      RETURN
C
 2001  format(i7,1x,22(E24.16,1x))
 2000 FORMAT(8E16.8)
 2002 FORMAT(6(E16.8,1x),3(i5,1x))
 2010 FORMAT(I5,3E16.8)
 2020 FORMAT(I5,5E24.16)
 2030 FORMAT(8E24.16)
 2040 FORMAT(1E24.16)
C
      END
C-----------------------------------------------------------------------
c      SUBROUTINE POSTPRVOF(PHI,PHIN,PHIT,PHITN,UVW,DUVWDT,ALMAXI,
      SUBROUTINE POSTPRVOF(ALMAXI,
     .           IO10,IO11,IO15,IO61,IO71,IO72,
     .           IPRINT,TPRINT,ALMAXB,NHT,ILMAX,ISTART,IGRID,INFIE)
C-----------------------------------------------------------------------
C
C     VOLTOT : Total fluid volume in domain
C
C     adapted for FMA : now compute all necessary (info from bem.f not
C                       available because of disorder?)
C                       future : call to PMTA
C
C     VOF coupling
C
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      INCLUDE 'param.inc'
C
      LOGICAL*4 IFIELD,IFLAB,IBREAK,ITENS
C
      COMMON/DATGEN/ ZL0,W0,H0,X0,D0,MX,MY,MZ,NX,NY,NZ,NSUBC,
     .               NINTRT,NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),XICON(MOMM,2),
     .                DUM3(18),IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),ICUMUL(6),NDST(6,2),
     .                IDUM2(18),JSLIM(6),NELEF(6),NNODF(6),
     .                IELEF(6,2),INODF(6,2),IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /FSCUB/ FN0XI(NDM,NINTM),FN0ET(NDM,NINTM),
     .               FN1XI(NDM,NINTM),FN1ET(NDM,NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),DUM5(7*NINTM*NINTM)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM6(6),
     .                ZNP(3),ZJAC
      COMMON /INTEG/  ETARP(NINTM),WEIGTR(NINTM),
     .                DUM7(4*NINTM+2*NINTM*NINTM+4*NINTM*NINTM*MNOD)
      COMMON /BOUNDC/ DT,DT2,DT22,TSTART,TMAX,RHO,CPRESS,CPREST,GE,
     .                DE1,DE2,OMEGA,V0H,TOLMAX,NBS
      COMMON /INTERF/ FIELD(11,NOIM),LISUB(NOIM),IREP(NOIM),
     .                NXI,NYI,NZI,NOI,IFIELD
      COMMON /DISSIP/ XDAMP,XLDAMP,DNU0,TRESH,SUTENS,COEF(NOMM),
     .                COEFX(NOMM),ISLOP(NOMM),NPOLY,IFLAB,IBREAK,ITENS
      COMMON /BOTFCT/ DUM13(NXM*NYM),SLOPE,ZK,IBOT
      COMMON /CGAGE/  XYZGAG(NGAM,3),UVWG(NGAM,3),PHITB(NGAM),
     .                PHIG(NGAM),PHING(NGAM),PHIB(NGAM),PHINB(NGAM),
     .                XYZBOT(NGAM,3),UVWB(NGAM,3),IGAG(NGAM),NGA,INTZED
      COMMON /SYSTEM/ DUM11(24),TIME,TDAMP,TDOWN,DUM12(4+8*MYMAX),IO6,
     &                IFLAGS,
     .                IFLAGV,IPTYP,ILOOP
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
c-------------------------------------------------------------------
c	old line
c      DIMENSION PHI(NOM),PHIN(NOM),PHIT(NOM),PHITN(NOM),UVW(3,NOMM),
c     .          DUVWDT(3,NOMM)
c-------------------------------------------------------------------
c	New restart, XB
c-------------------------------------------------------------------
      include 'result.h' ! pg(nomm),phi(nomm),phin(nomm),phit(nomm),phitn(nomm),
c                          phis(nomm),phim(nomm),phiss(nomm),phimm(nomm),
c                          phins(nomm),phinm(nomm),phits(nomm),phitm(nomm),
c                          phism(nomm),phinn(nomm),
c                          pres(nomn),dprdt(nomm),uvw(3,nomm),duvwdt uvw(3,nomm)
c-------------------------------------------------------------------

      DIMENSION DISCH(6),XX(3),DELGP(3),DELGNP(3),DIAG(2),XCIE(3),
     .          VIE(3,3),V12(3),V13(3),DISND(MNOD),FSF(NDM),FSD(NDM)
C
      DATA ZERO/0.D0/,TWO/2.D0/,PII4/0.7957747154594768D-01/,
     .     THREE/3.D0/,ONE/1.0D0/,HALF/0.5D0/,QUART/0.25D0/,
     .     DLOG2I/3.321928094D0/,FOUR/4.D0/,CRAT/0.4999D0/,
     .     SIX/6.D0/,OTHREE/0.3333333333333333D0/,EPS/1.D-08/
C
C.....Save commons and local variables to enable static commons
C
      SAVE
C     SAVE /DATGEN/,/MAILLE/,/DELEM1/,/DELEM2/,/SAVEF/,/FUNCTN/,/FSCUB/
C
C.....Initialize flow rates on boundary, total volume and energy
C
CSYM...
      KMAX = 6
      IF(IBOT.EQ.10) KMAX = KFMAX-1
      IF(ISYM.EQ.1) KMAX = KFMAX-1
      DO K=1,KMAX
         DISCH(K) = ZERO
      END DO
      TANA = DTAN(ALMAXI)
C
      VOLTOT = ZERO
      VOLBOT = ZERO
      EPTOT  = ZERO
      EPBOT  = ZERO
      EKTOT  = ZERO
C
C.....Initialize field values
C
      IF(IFIELD) THEN
         DO JOI=1,NOI
            DO I=4,9
               FIELD(I,JOI) = ZERO
            END DO
         END DO
      END IF
C
C=====Compute solution at surface gages
C
      IF(NGA.GT.0) THEN
         CALL GAGEVOF(UVW,PHI,PHIN,IO71)
C        ---------
         WRITE(*,*) '   Surface gage fields computed'
C
CBOT
         IF (IBOT.NE.10) THEN
           CALL BOTTOM(UVW,PHI,PHIN,PHIT,IO72)
C          -----------
           WRITE(*,*) '   Bottom gage fields computed'
         ELSE
	   DO IG=1,NGA
             XYZBOT(IG,3)=-H0
           END DO
         END IF
C
C        Fine tune eliminated interior points based on gages
C
	 IF(IFIELD.AND.(NGA.EQ.NXI*NYI)) THEN
	    DO IG=1,NGA
	       JOI1 = (IG-1)*NZI + 1
	       JOI2 = JOI1 + NZI - 1
	       DO JOI=JOI1,JOI2
	          IF(INTZED.EQ.0) THEN
	             IF(IREP(JOI).EQ.1) THEN
	                IF(FIELD(3,JOI).GE.XYZGAG(IG,3).OR.
     .                     FIELD(3,JOI).LE.XYZBOT(IG,3)) THEN
		           IREP(JOI) = 0
		        END IF
		     END IF
		  ELSE IF(INTZED.EQ.1) THEN
		     ZB = XYZBOT(IG,3)
		     ZF = XYZGAG(IG,3)
		     DZINT = (ZF - ZB)/DFLOAT(NZI + 1)
                     FIELD(3,JOI) = ZB + (JOI - JOI1 + 1)*DZINT
		     IREP(JOI) = 1
		  END IF
	       END DO
	    END DO
	 END IF
      END IF
C
C=====Elements per element integral computations (MAIN LOOP)
C
      JSAVE  = 0
      NINTPR = 0
      NSIDEP = 0
      EFJXIP = ZERO
      EFJETP = ZERO
      EFJXI = ZERO
      EFJET = ZERO
      NNODE = MNOD
      NNODM = MNODM
C
      DO IE=1,MOM
C
C     ...General parameters for elt. IE
C
         NINTR = INTG(IE)
         NSIDE = IFACE(IE)
C
C        4-node connectivity in NODE1(NDM)
C
         DO J=1,NNODE
            NODE2(J)         = ILCON(IE,J)
            XYZLO2(J,1)      = XYZNOD(NODE2(J),1)
            XYZLO2(J,2)      = XYZNOD(NODE2(J),2)
            XYZLO2(J,3)      = XYZNOD(NODE2(J),3)
         END DO
C
C        Assign NOD1E(NDM) and NNODE for correct assembly of ZK(U/Q)L
C        i.e., considering MII elements
C
 	 ILOC = 0
	 DO J=1,NNODE
	    DO I=1,NNODE
	       ILOC = ILOC + 1
	       NODE1(ILOC)    = IGCON(IE,I,J)
	       XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
	       XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
	       XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYMD
               IF (ISYM.EQ.1) THEN
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.5.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYSD
	    END DO
	 END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
C     ...Filling up of the common INTEG when the elements have different
C        node or integration points numbers (accelerator)
C
         IRECAL = 1
         IF (NINTR.NE.NINTPR) THEN
C
C           Regular Gauss points
C
            CALL GAUSSP
C           -----------
C
            IRECAL = 0
         END IF
C
C        MII cubic 4-node shape functions
C        Compute shape functions and their derivatives for
C        cubic element on the boundary and store in common FSCUB
C
         IF(EFJXI.NE.EFJXIP.OR.NINTR.NE.NINTPR) THEN
C
C           XI direction
C
            IRECAL = 0
            DO IP=1,NINTR
               CHI = (ETARP(IP) + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0XI(J,IP) = FSF(J)
                  FN1XI(J,IP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
         IF(EFJET.NE.EFJETP.OR.NINTR.NE.NINTPR) THEN
C
C           ETA direction
C
            IRECAL = 0
            DO JP=1,NINTR
               CHI = (ETARP(JP) + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0ET(J,JP) = FSF(J)
                  FN1ET(J,JP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
C
C        Calculates bidim shape functions at all reg/sing integ. points
C
         IF(IRECAL.EQ.0) THEN
            ILOC = 0
            DO J=1,NNODE
               DO I=1,NNODE
                  ILOC = ILOC + 1
                  DO IP=1,NINTR
                     DO JP=1,NINTR
C
C                       Shape functions at all regular integ. points
C
                        FIRIP(ILOC,IP,JP)  = FN0XI(I,IP)*FN0ET(J,JP)
                        DFIXIP(ILOC,IP,JP) = FN1XI(I,IP)*FN0ET(J,JP)
                        DFIETP(ILOC,IP,JP) = FN0XI(I,IP)*FN1ET(J,JP)
                     END DO
                  END DO
               END DO
            END DO
         END IF
C
C     ...Analysis of possible subdivisions for element IE and nodes NOI
C        in case of interior field computations
C
         IF(IFIELD) THEN
            EFJXI = XICON(IE,1)
            EFJET = XICON(IE,2)
C
C           4-node connectivity
C
            DO I=1,NNODE
               NODE2(I)    = ILCON(IE,I)
               XYZLO2(I,1) = XYZNOD(NODE2(I),1)
               XYZLO2(I,2) = XYZNOD(NODE2(I),2)
               XYZLO2(I,3) = XYZNOD(NODE2(I),3)
            END DO
C
C           Preparing for minimum distance computations
C
            DO I=2,NNODE
	       DO J=1,3
                  VIE(I-1,J) = XYZLO2(I,J) - XYZLO2(1,J)
               END DO
            END DO
	    V12(1) = VIE(1,2)*VIE(2,3) - VIE(2,2)*VIE(1,3)
	    V12(2) = VIE(1,3)*VIE(2,1) - VIE(2,3)*VIE(1,1)
	    V12(3) = VIE(1,1)*VIE(2,2) - VIE(2,1)*VIE(1,2)
	    V13(1) = VIE(1,2)*VIE(3,3) - VIE(3,2)*VIE(1,3)
	    V13(2) = VIE(1,3)*VIE(3,1) - VIE(3,3)*VIE(1,1)
	    V13(3) = VIE(1,1)*VIE(3,2) - VIE(3,1)*VIE(1,2)
	    ZNV12 = DSQRT(V12(1)*V12(1)+V12(2)*V12(2)+V12(3)*V12(3))
	    ZNV13 = DSQRT(V13(1)*V13(1)+V13(2)*V13(2)+V13(3)*V13(3))
	    DO J=1,3
	       V12(J) = V12(J)/ZNV12
	       V13(J) = V13(J)/ZNV13
	    END DO
C
C           Longest 1/2 diagonal chord in elt. IE
C
            DO K=1,2
	       DIAG(K) = ZERO
	       DO J=1,3
	          DIAG(K) = DIAG(K) + (XYZLO2(K+2,J) - XYZLO2(K,J))**2
	       END DO
	    END DO
	    DIAM = HALF*DSQRT(DMAX1(DIAG(1),DIAG(2)))
C
C           Coordinates of elt. IE center
C
	    DO J=1,3
	       XCIE(J) = ZERO
	       DO I=1,NNODE
	          XCIE(J) = XCIE(J) + XYZLO2(I,J)
	       END DO
	       XCIE(J) = QUART*XCIE(J)
	    END DO
C
C           Loop over interior points
C
            DO JOI=1,NOI
	     IF(IREP(JOI).EQ.1) THEN
C
C              Distance of JOI from elt IE center plus minima d12, d13
C
	       DISTCI = ZERO
	       D12    = ZERO
	       D13    = ZERO
	       DO J=1,3
	          DISTCI = DISTCI + (XCIE(J) - FIELD(J,JOI))**2
		  D12    = D12 + (XYZLO2(1,J) - FIELD(J,JOI))*V12(J)
		  D13    = D13 + (XYZLO2(1,J) - FIELD(J,JOI))*V13(J)
	       END DO
	       DISTCI = DSQRT(DISTCI)
	       DMINPP = DMIN1(DABS(D12), DABS(D13))
C
C              Distance of JOI from elt IE 4 nodes
C
	       DO I=1,NNODE
	          DISND(I) = ZERO
		  DO J=1,3
		     DISND(I) = DISND(I) + (XYZLO2(I,J)-FIELD(J,JOI))**2
		  END DO
		  DISND(I) = DSQRT(DISND(I))
	       END DO
	       DISMI = DMIN1(DISND(1),DISND(2),DISND(3),DISND(4))
	       DISDA = DIAM/DISTCI
C
               DISTB = DSQRT(DISTCI*DISTCI + DMINPP*DMINPP)
	       IF(DISTCI.GT.DISTB) THEN
	          ANGLE = TWO*DATAN2(DIAM*DMINPP,DISTCI*DISTCI)
	       ELSE
	          ANGLE = TWO*DATAN2(DIAM,DMINPP)
	       END IF
C
               IF((ANGLE.GT.ALMAXI).AND.(DISDA.GT.ONE)) THEN
C
C                 Cas d'un angle obtu avec noeuds proche interieur elt
C
	          DISDA = DIAM/DMINPP
                  NSUB  = INT(DLOG10(DISDA/TANA)*DLOG2I + CRAT)
	       ELSE
                  IF(DISMI.LT.TWO*DIAM) THEN
C
C                    Cas d'un angle aigu avec noeuds alignes en dehors elt
C
                     DISDA = DIAM/(DISMI*TANA)
                     NSUB = 0
                     IF((DISDA.GT.ONE).AND.(DISDA.LT.THREE)) THEN
                        NSUB = 1
                     ELSE IF((DISDA.GE.THREE).AND.(DISDA.LT.SIX)) THEN
                        NSUB = 2
                     ELSE IF(DISDA.GE.SIX) THEN
                        NSUB = 3
                     END IF
                  ELSE
                     NSUB = 0
                  END IF
               END IF
C
               IF(NSUB.GT.4) THEN
                  NSUB = 4
               END IF
C
               LISUB(JOI) = NSUB
C 	       IF(NSUB.NE.0) WRITE(IO6,*) IE,JOI,NSUB,DISTCI,
C     .                                  DISMI,DMINPP,DISDA,ANGLE
             END IF
            END DO
	 END IF
C
C     ...Start integral loop
C
         DISCHE = ZERO
         DO IP=1,NINTR
            DO JP=1,NINTR
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
               DFIX(J) = DFIXIP(J,IP,JP)
               DFIE(J) = DFIETP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Final Gauss Jacobian
C
            WWJAC = ZJAC*WEIGTR(IP)*WEIGTR(JP)
C
C              Compute Phi,Phin,... at integration points by
C              shape functions summation and contributions to volume and
C              energy for element IE, int. pt. IP,JP
C
               PHIP   = ZERO
               PHINP  = ZERO
               PHITP  = ZERO
               PHITNP = ZERO
C
               DO I=1,NNODM
                  L      = NODE1(I)
                  PHIP   = PHIP   + FI(I)*PHI(L)
                  PHINP  = PHINP  + FI(I)*PHIN(L)
                  PHITP  = PHITP  + FI(I)*PHIT(L)
                  PHITNP = PHITNP + FI(I)*PHITN(L)
               END DO
C
C              Compute kinetic and potential energy, volume, local flux
C
               EKTOT  = EKTOT  + PHIP*PHINP*WWJAC
	       EPTOT  = EPTOT  + XYZP(3)*XYZP(3)*ZNP(3)*WWJAC
	       VOLTOT = VOLTOT + XYZP(3)*ZNP(3)*WWJAC
	       DISCHE = DISCHE + PHINP*WWJAC
C
C              Below z=0 values for possible corrections
C
	       IF(IFACE(IE).NE.1) THEN
	          VOLBOT = VOLBOT + XYZP(3)*ZNP(3)*WWJAC
		  EPBOT  = EPBOT  + XYZP(3)*XYZP(3)*ZNP(3)*WWJAC
	       END IF
C
C              Internal fields
C
               IF(IFIELD) THEN
                  NSUBP = 0
		  DO JOI=1,NOI
		   IF(IREP(JOI).EQ.1) THEN
                     NSUB = LISUB(JOI)
                     NSG  = 2**NSUB
		     NSG2 = NSG*NSG
C
                     IF(NSUB.EQ.0) THEN
                        IF(NSUBP.NE.0) THEN
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Final Gauss Jacobian
C
            WWJAC = ZJAC*WEIGTR(IP)*WEIGTR(JP)
C
                           PHIP   = ZERO
                           PHINP  = ZERO
                           PHITP  = ZERO
                           PHITNP = ZERO
                           DO I=1,NNODM
                              L      = NODE1(I)
                              PHIP   = PHIP   + FI(I)*PHI(L)
                              PHINP  = PHINP  + FI(I)*PHIN(L)
                              PHITP  = PHITP  + FI(I)*PHIT(L)
                              PHITNP = PHITNP + FI(I)*PHITN(L)
			   END DO
	     	           NSUBP = 0
                        END IF
		     ELSE
		        DSG   = TWO/DFLOAT(NSG)
                        NSUBP = NSUB
		     END IF
C
                     DO ISG=1,NSG
		        IF(NSUB.NE.0) THEN
		           XILI = TWO*(ISG-1)/DFLOAT(NSG) - ONE
			   XIRI = XILI + DSG
	                   XIM  = HALF*(XILI + XIRI)
			   XI   = ETARP(IP)/DFLOAT(NSG) + XIM
                           CHI  = (XI + ONE)*OTHREE + EFJXI
                           CALL SHAPFN(CHI,FSF,NNODE)
C                          -----------
                           CALL SHAPD1(CHI,FSD,NNODE)
C                          -----------
                           DO J=1,NNODE
                              FN0XI(J,IP) = FSF(J)
                              FN1XI(J,IP) = OTHREE*FSD(J)
                           END DO
			END IF
C
                        DO JSG=1,NSG
		           IF(NSUB.NE.0) THEN
		              ETLI = TWO*(JSG-1)/DFLOAT(NSG) - ONE
		      	      ETRI = ETLI + DSG
			      ETM  = HALF*(ETLI + ETRI)
			      ETA  = ETARP(JP)/DFLOAT(NSG) + ETM
                              CHI  = (ETA + ONE)*OTHREE + EFJET
                              CALL SHAPFN(CHI,FSF,NNODE)
C                             -----------
                              CALL SHAPD1(CHI,FSD,NNODE)
C                             -----------
                              DO J=1,NNODE
                                 FN0ET(J,JP) = FSF(J)
                                 FN1ET(J,JP) = OTHREE*FSD(J)
                              END DO
C
C                             Shape functions at points IP,JP,ISG,JSG
C
                              ILOC = 0
                              DO J=1,NNODE
                                 DO I=1,NNODE
                                    ILOC = ILOC + 1
                                    FI(ILOC)   = FN0XI(I,IP)*FN0ET(J,JP)
                                    DFIX(ILOC) = FN1XI(I,IP)*FN0ET(J,JP)
                                    DFIE(ILOC) = FN0XI(I,IP)*FN1ET(J,JP)
                                 END DO
                              END DO
C
C                             Cord., unit vectors and Jacob.
C
                              CALL CARAC2
C                             -----------
                              WWJAC = ZJAC*WEIGTR(IP)*WEIGTR(JP)
C
C                             Recalculate boundary fields at IP,JP,ISG,JSG
C
                              PHIP   = ZERO
                              PHINP  = ZERO
                              PHITP  = ZERO
                              PHITNP = ZERO
                              DO I=1,NNODM
                                 L      = NODE1(I)
                                 PHIP   = PHIP   + FI(I)*PHI(L)
                                 PHINP  = PHINP  + FI(I)*PHIN(L)
                                 PHITP  = PHITP  + FI(I)*PHIT(L)
                                 PHITNP = PHITNP + FI(I)*PHITN(L)
                              END DO
			   END IF
C
C                          Compute quantities related to GREEN's function
C
                           CST   = PII4*WWJAC/DFLOAT(NSG2)
                           R2    = ZERO
                           DGNUM = ZERO
                           DO J=1,3
                              XX(J) = XYZP(J) - FIELD(J,JOI)
                              DGNUM = DGNUM + XX(J)*ZNP(J)
                              R2    = R2    + XX(J)*XX(J)
                           END DO
                           R  = DSQRT(R2)
	        	   R3 = R2*R
                           R5 = R2*R3
C
C                          Computation of Green's functions at (IP,JP)
C                          Computation of integral contributions
C
                           GP    =  CST/R
                           DGDNP = -CST*DGNUM/R3
C
C              Kernel transformation, in case of L on 4-node IE
C
               ISING = 0
               DO I=1,NNODE
                  IF (L.EQ.NODE2(I)) THEN
                    DGDNP    = ZERO
                    GP = ZERO
		     ISING = 1
                  END IF
               END DO
C
C              Image method flat bottom (affects regular/one sg. integrals)
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + XYZNOD(L,3)
C
C                 Test for bottom nodes which lead to sg. integral
C                 of the image part
C
		  IF((DABS(HP).GT.EPS).OR.(ISING.EQ.0)) THEN
		     RP     = R5 + FOUR*HP*(HP + XX(3))
                     RP     = DSQRT(RP)
		     DGPNUM = DGNUM + TWO*HP*ZNP(3)
		     GP     = GP + CST/RP
		     DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
		  END IF
	       END IF
C
CSYM           Image method y-symmetry (affects regular/one sg. integrals)
C
               IF(ISYM.EQ.1) THEN
                  YP = XYZNOD(L,2)
C
C                 Test for nodes which lead to sg. integral
C                 of the image part
C
		  IF((DABS(YP).GT.EPS).OR.(ISING.EQ.0)) THEN
		     RP     = R5 + FOUR*YP*XYZP(2)
                     RP     = DSQRT(RP)
		     DGPNUM = DGNUM + TWO*YP*ZNP(2)
		     GP     = GP + CST/RP
		     DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
		  END IF
C
C                 + Image method flat bottom (affects regular/one sg. integrals)
C
                  IF(IBOT.EQ.10) THEN
C
		     IF((DABS(HP).GT.EPS).OR.(ISING.EQ.0)) THEN
		        RP     = R5 + FOUR*(YP*XYZP(2)+HP*(HP+XX(3)))
                        RP     = DSQRT(RP)
		        DGPNUM = DGNUM + TWO*(YP*ZNP(2)+ HP*ZNP(3))
		        GP     = GP + CST/RP
		        DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
		     END IF
	          END IF
	       END IF
CMYS
                           FIELD(4,JOI) = FIELD(4,JOI) + PHINP*GP
     .                                                 - PHIP*DGDNP
                           FIELD(8,JOI) = FIELD(8,JOI) + PHITNP*GP
     .                                                 - PHITP*DGDNP
		           DO J=1,3
		  	      DELGP(J)  = CST*XX(J)/R3
			      DELGNP(J) = CST*(ZNP(J) -
     .                                    THREE*DGNUM*XX(J)/R2)/R3
                              FIELD(4+J,JOI) = FIELD(4+J,JOI) +
     .                                   PHINP*DELGP(J) - PHIP*DELGNP(J)
		           END DO
			END DO
		     END DO
		   END IF
		  END DO
	       END IF
            END DO
         END DO
C
C        Flux per side
C
         DISCH(NSIDE) = DISCH(NSIDE) + DISCHE
C
         NINTPR = NINTR
         NSIDEP = NSIDE
         EFJXIP = EFJXI
         EFJETP = EFJET
      END DO
CBOT
      IF (IBOT.EQ.10) THEN
         EPTOT = EPTOT - NX*NY*H0*H0
         VOLTOT = VOLTOT + NX*NY*H0
         EPBOT = EPBOT - NX*NY*H0*H0
         VOLBOT = VOLBOT + NX*NY*H0         
      END IF
CSYM
      IF (ISYM.EQ.1) THEN
         EPTOT = TWO*EPTOT
         VOLTOT = TWO*VOLTOT
         EPBOT = TWO*EPBOT
         VOLBOT = TWO*VOLBOT
         DO K=1,KMAX
            DISCH(K) = TWO*DISCH(K)
         END DO
      END IF
C
C     Dimensional energies
C
      EPTOT = HALF*GE*RHO*EPTOT
      EPBOT = HALF*GE*RHO*EPBOT
      EPSUR = EPTOT - EPBOT
      VOLSUR = VOLTOT - VOLBOT
      EKTOT = HALF*RHO*EKTOT
      ETTOT = EPTOT + EKTOT
C
C     Total flux
C
      OUTFLO = ZERO
C
      DO K=1,KMAX
         OUTFLO = OUTFLO + DISCH(K)
      END DO
C
C.....Output results on files
C
      WRITE(IO6,2000) TIME,EPTOT,EKTOT,ETTOT,VOLTOT,OUTFLO,EPSUR,VOLSUR
      WRITE(IO6,2000) (DISCH(K),K=1,6)
!      WRITE(IO10,2000) TIME,EKTOT,EPTOT,ETTOT,EPSUR,EPSUR+EKTOT
      WRITE(IO10,2002) TIME,EKTOT,EPTOT,ETTOT,EPSUR,EPSUR+EKTOT,NX,NY,NZ
      WRITE(IO11,2000) TIME,VOLTOT,VOLSUR,OUTFLO,OUTFLO/VOLSUR
C
C     Restart results every IPRINT loop
C
      IF(IPRINT.GT.0) THEN
         IF(DABS(DFLOAT(ILOOP)/DFLOAT(IPRINT)-ILOOP/IPRINT).LT.EPS
     .      .OR.ILOOP.EQ.ILMAX.OR.(TIME+DT).GE.TMAX
     .      .OR.((TIME+DT).GE.TPRINT.AND.TIME.LT.TPRINT)
     .      ) THEN
	    TMAXC  = DT*ILMAX
	    ISTART = 1
	    WRITE(IO15,*) MX,MY,MZ,NINTRT,NSUBC,ALMAXB,ALMAXI,
     .                   '      MX,MY,MZ,NINTR,NSUBC,ALMAXB,ALMAXI'
            WRITE(IO15,*) ZL0,W0,H0,X0,D0,IBOT,SLOPE,ZK,
     .                   '      ZL0,W0,H0,X0,D0,IBOT,SLOPE,ZK'
            WRITE(IO15,*) (IBCOND(K),K=1,6),'     (IBCOND(K),K=1,6)'
            WRITE(IO15,*) ILMAX,IPTYP,NBS,IPRINT,
     .                   '      ILMAX,IPTYP,NBS,IPRINT'
            WRITE(IO15,*) DT,TIME,TMAXC,RHO,CPRESS,CPREST,GE,TDAMP,
     .                    TDOWN,  
     .                 '      DT,TSTART,TMAX,RHO,CPRESS,CPREST,GE,TDAMP'
            WRITE(IO15,*) DE1,DE2,OMEGA,V0H,INFIE,NOI,NHT,TOLMAX,
     .                   '      DE1,DE2,OMEGA,V0H,INFIE,NOI,NHT,TOLMAX'
            WRITE(IO15,*) XLDAMP,DNU0,TRESH,SUTENS,NPOLY,NXI,NYI,NZI,
     .              '       XLDAMP,DNU0,TRESH,SUTENS,NPOLY,NXI,NYI,NZI,'
            WRITE(IO15,*) ISTART,IGRID,'ISTART,IGRID'
C
 1666	    format(3(i7,1x),2(E24.16,1x),i7)    
c            WRITE(IO15,*) NOM,NX,NY,TIME,DT,ILOOP
            WRITE(IO15,1666) NOM,NX,NY,TIME,DT,ILOOP
            DO I=1,NOM
c-----------------------------------------------------------------
c	oldline
c	       WRITE(IO15,2020) I,XYZNOD(I,1),XYZNOD(I,2),XYZNOD(I,3),
c     .                          PHI(I),PHIN(I)
c-----------------------------------------------------------------
c	New restart, XB
            WRITE(IO15,2001) I,XYZNOD(I,1),XYZNOD(I,2),XYZNOD(I,3),
     &                  PHI(I),PHIN(I),phit(i),phitn(i),
     &			  phis(i),phim(i),phiss(i),phimm(i),
     &			  phins(i),phinm(i),phits(i),phitm(i),
     &			  phism(i),phinn(i),
     &			  pres(i),dprdt(i),uvw(1,i),uvw(2,i),
     &			  uvw(3,i),
     &			  duvwdt(1,i),duvwdt(2,i),duvwdt(3,i)
	    END DO
	 END IF
      END IF
C
C.....Internal fields (save pts for loop 1 and fields for all loops)
C
      IF(IFIELD) THEN
         DO JOI=1,NOI
	    FIELD(9,JOI) = -RHO*(FIELD(8,JOI) + GE*FIELD(3,JOI) +
     .                     HALF*(FIELD(5,JOI)**2 + FIELD(6,JOI)**2 +
     .                           FIELD(7,JOI)**2))
            WRITE(IO61,2005) IREP(JOI),(FIELD(J,JOI),J=5,7),
     .                       FIELD(9,JOI),FIELD(3,JOI)
	 END DO
      END IF
C
      RETURN
C
 2001  format(i7,1x,22(E24.16,1x))
 2000 FORMAT(8E16.8)
 2002 FORMAT(6(E16.8,1x),3(i5,1x))
 2005 FORMAT(I3,5E16.8)
 2010 FORMAT(I5,3E16.8)
 2020 FORMAT(I5,5D24.16)
C
      END
C-----------------------------------------------------------------------
c      SUBROUTINE POSTPR(PHI,PHIN,PHIS,PHIM,PHIT,PHITN,UVW,DUVWDT,TIMEP,
      SUBROUTINE POSTPR(TIMEP,
     .               ALMAXI,IO10,IO11,IO15,IO60,IO61,IO62,IO71,IO72,
     .               IPRINT,TPRINT,ALMAXB,NHT,ILMAX,ISTART,IGRID,INFIE)
C-----------------------------------------------------------------------
C
C     VOLTOT : Total fluid volume in domain
C
C     adapted for FMA : now compute all necessary (info from bem.f not
C                       available because of disorder?)
C                       future : call to PMTA
C
C     snake wavemaker-Absorbing piston
C     freak waves
C     tsunamis
C
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      INCLUDE 'param.inc'
      PARAMETER (NMVAV=10)
C
      LOGICAL*4 IFIELD,IFLAB,IBREAK,ITENS
      CHARACTER*50::file_name1,file_name2
C
      COMMON/DATGEN/ ZL0,W0,H0,X0,D0,MX,MY,MZ,NX,NY,NZ,NSUBC,
     .               NINTRT,NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),XICON(MOMM,2),
     .                DUM3(18),IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),ICUMUL(6),NDST(6,2),
     .                IDUM2(18),JSLIM(6),NELEF(6),NNODF(6),
     .                IELEF(6,2),INODF(6,2),IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /FSCUB/ FN0XI(NDM,NINTM),FN0ET(NDM,NINTM),
     .               FN1XI(NDM,NINTM),FN1ET(NDM,NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),DUM5(7*NINTM*NINTM)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM6(6),
     .                ZNP(3),ZJAC
      COMMON /INTEG/  ETARP(NINTM),WEIGTR(NINTM),PHIP12(NINTM),
     .                PHIP23(NINTM),RMIP12(NINTM),RMIP23(NINTM),
     .                RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .                XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .                ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /BOUNDC/ DT,DT2,DT22,TSTART,TMAX,RHO,CPRESS,CPREST,GE,
     .                DE1,DE2,OMEGA,V0H,TOLMAX,NBS
      COMMON /INTERF/ FIELD(11,NOIM),LISUB(NOIM),IREP(NOIM),
     .                NXI,NYI,NZI,NOI,IFIELD
      COMMON /DISSIP/ XDAMP,XLDAMP,DNU0,TRESH,SUTENS,COEF(NOMM),
     .                COEFX(NOMM),ISLOP(NOMM),NPOLY,IFLAB,IBREAK,ITENS
c      COMMON /SYSTEM/ DUM11(24),TIME,TDAMP,TDOWN,UPNEW(4,MYMAX),
c     .                DUPDT(4,MYMAX),DUM12(4),IO6,IDUV(2),IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOTFCT/ DUM13(NXM*NYM),SLOPE,ZK,IBOT
      COMMON /LNDSLD/ THETA,BSLIDE,WSLIDE,TSLIDE,DSLIDE,ZL2,RHOL,GAMMA,
     .                CM,CD,ZEPS,ZMU,ZMUC,ZAL,DUM15(34)
      COMMON /CGAGE/  XYZGAG(NGAM,3),UVWG(NGAM,3),PHITB(NGAM),
     .                PHIG(NGAM),PHING(NGAM),PHIB(NGAM),PHINB(NGAM),
     .                XYZBOT(NGAM,3),UVWB(NGAM,3),IGAG(NGAM),NGA,INTZED
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
c-------------------------------------------------------------------
c	old line
c      DIMENSION PHI(NOM),PHIN(NOM),PHIS(NOM),PHIM(NOM),PHIT(NOM),
c     .          PHITN(NOM),UVW(3,NOMM),DUVWDT(3,NOMM),GRAD(3,3,NOIM)
c-------------------------------------------------------------------
c	New restart, XB
c-------------------------------------------------------------------
      include 'result.h' ! pg(nomm),phi(nomm),phin(nomm),phit(nomm),phitn(nomm),
c                          phis(nomm),phim(nomm),phiss(nomm),phimm(nomm),
c                          phins(nomm),phinm(nomm),phits(nomm),phitm(nomm),
c                          phism(nomm),phinn(nomm),
c                          pres(nomn),dprdt(nomm),uvw(3,nomm),duvwdt uvw(3,nomm)
      DIMENSION GRAD(3,3,NOIM)
c-------------------------------------------------------------------

      DIMENSION DISCH(6),XX(3),DELGP(3),DELGNP(3),DIAG(2),XCIE(3),
     .          VIE(3,3),V12(3),V13(3),DISND(MNOD),FSF(NDM),FSD(NDM),
     .          FPXY(4,MYMAX),UPIST(4,MYMAX),UPIST1(4,MYMAX),
     .          UPIST2(4,MYMAX),DELSM(4,MYMAX),BZ(4),BXY(4),ICOUNT(4),
     .          ET(4),ETAVG(4,NMVAV+1),DUAVG(4,NMVAV+1)
C
      DATA ZERO/0.D0/,TWO/2.D0/,PII4/0.7957747154594768D-01/,
     .     THREE/3.D0/,ONE/1.0D0/,HALF/0.5D0/,QUART/0.25D0/,
     .     DLOG2I/3.321928094D0/,FOUR/4.D0/,CRAT/0.4999D0/,
     .     SIX/6.D0/,OTHREE/0.3333333333333333D0/,EPS/1.D-08/,
     .     EPSI/1.D-04/,
     .     PIS8/0.3926990816987241D0/,PIS2/1.570796326794896D0/,
     .     S3/0.3333333333333333D0/,FIFT/0.2d0/
! correction bug?
      data FIFT/0.2d0/   
      
C
C.....Save commons and local variables to enable static commons
C
      SAVE
C     SAVE /DATGEN/,/MAILLE/,/DELEM1/,/DELEM2/,/SAVEF/,/FUNCTN/,/FSCUB/
C
C.....Initialize flow rates on boundary, total volume and energy
C
CSYM...
      KFMAX = 6
      IF(IBOT.EQ.10) KFMAX = KFMAX-1
      IF(ISYM.EQ.1) KFMAX = KFMAX-1
      DO K=1,KFMAX
         DISCH(K) = ZERO
      END DO
      TANA = DTAN(ALMAXI)
C
      VOLTOT = ZERO
      VOLBOT = ZERO
      VOLSLI = ZERO
      VOLWAV = ZERO
      EPTOT  = ZERO
      EPBOT  = ZERO
      EKTOT  = ZERO
      EPPAD  = ZERO
      EKPAD  = ZERO
C
C.....Initialize field values
C
      IF(IFIELD) THEN
         DO JOI=1,NOI
            DO I=4,11
               FIELD(I,JOI) = ZERO
            END DO
            DO J=1,3
               DO I=1,3
                  GRAD(I,J,JOI) = ZERO
               END DO
            END DO
         END DO
      END IF
C
C=====Compute solution at surface gages
C
      IF(NGA.GT.0) THEN
C         CALL GAGEVOF(UVW,PHI,PHIN,IO71)
         CALL GAGE_FB(PHI,PHIN,IO71)
C        ---------
         WRITE(*,*) '   Surface gage fields computed'
C
CBOT
         IF (IBOT.NE.10) THEN
C           CALL BOTTOM(UVW,PHI,PHIN,PHIT,IO72)
C          -----------
           WRITE(*,*) '   Bottom gage fields computed'
           DO IG=1,NGA
             XYZBOT(IG,3)=-H0
           END DO
         END IF
C
C        Find tune eliminated interior points
C
C         IF(IFIELD.AND.(NXI.NE.0.AND.NYI.NE.0.AND.NZI.NE.0)
C     .      .AND.INTZED.EQ.0) THEN
C              CALL DISTPI()
C         END IF
C
C        Find tune eliminated interior points based on gages
C
	 IF(IFIELD.AND.(NGA.GT.0)) THEN
C EQ.NXI*NYI)) THEN
	    DO IG=1,NGA
	       JOI1 = (IG-1)*NZI + 1
	       JOI2 = JOI1 + NZI - 1
	       DO JOI=JOI1,JOI2
	          IF(INTZED.EQ.0) THEN
                     IREP(JOI)=1
C	             IF(IREP(JOI).EQ.1) THEN
	                IF(FIELD(3,JOI).GE.XYZGAG(IG,3).OR.
     .                     FIELD(3,JOI).LE.XYZBOT(IG,3)) THEN
		           IREP(JOI) = 0
		        END IF
C		     END IF
		  ELSE IF(INTZED.EQ.1) THEN
		     ZB = XYZBOT(IG,3)
		     ZF = XYZGAG(IG,3)
		     DZINT = (ZF - ZB)/DFLOAT(NZI + 1)
                     FIELD(3,JOI) = ZB + (JOI - JOI1 + 1)*DZINT
		     IREP(JOI) = 1
		  END IF
	       END DO
	    END DO
	 END IF
      END IF
C
C.....Initialize absorbing piston for time t on boundary k
C     WITH UP POSITIVE IN THE +X OR +Y DIRECTION !
C
CSYM...
      DO K=2,KFMAX
         IF(IBCOND(K).EQ.3) THEN
	    IF(K.EQ.2.OR.K.EQ.4) THEN
	       IST = K - 3
	       NK  = MY + 1
	    ELSE
	       IST = K - 4
               NK  = MX + 1
	    END IF
	    DO J=1,NK
	       IF(ISTART.EQ.0.OR.ILOOP.GT.1) THEN
                  UPIST2(K-1,J) = PHIN(INODF(K,1) + J-1)*IST
  	       ELSE
C
C                 Initializes AP(K) using input data for restart
C
	          UPIST2(K-1,J) = UPNEW(K-1,J)
	          UPIST(K-1,J)  = UPNEW(K-1,J) - DUPDT(K-1,J)*DT
  	       END IF
               IF(DABS(UPIST2(K-1,J)).LE.EPS) THEN
                  UPIST1(K-1,J) = ZERO
               ELSE
                  UPIST1(K-1,J) = UPIST(K-1,J)
               END IF
               UPIST(K-1,J)  = UPIST2(K-1,J)
C
C              Fpxy is for the absorbing piston dynamic pressure
C              force on k.
C
               FPXY(K-1,J)  = ZERO
	       DELSM(K-1,J) = ZERO
	    END DO
            ICOUNT(K-1) = 0
         END IF
      END DO
C
C=====Elements per element integral computations (MAIN LOOP)
C
      NNODE = MNOD
      JSAVE  = 0
      NINTPR = 0
      NSIDEP = 0
      EFJXIP = ZERO
      EFJETP = ZERO
      EFJXI = ZERO
      EFJET = ZERO
      NNODM = MNODM
C
      DO IE=1,MOM
C
C     ...General parameters for elt. IE
C
         NSIDE = IFACE(IE)
         NINTR = INTG(IE)
C
C        16-node connectivity
C
 	 ILOC = 0
	 DO J=1,NNODE
	    DO I=1,NNODE
	       ILOC = ILOC + 1
	       NODE1(ILOC)    = IGCON(IE,I,J)
	       XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
	       XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
	       XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYMD
               IF (ISYM.EQ.1) THEN
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.5.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYSD
	    END DO
	 END DO
C
C        4-node connectivity
C
         DO I=1,NNODE
            NODE2(I)    = ILCON(IE,I)
            XYZLO2(I,1) = XYZNOD(NODE2(I),1)
            XYZLO2(I,2) = XYZNOD(NODE2(I),2)
            XYZLO2(I,3) = XYZNOD(NODE2(I),3)
         END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
C     ...Filling up of the common INTEG when the elements have different
C        node or integration points numbers (accelerator)
C
         IRECAL = 1
         IF (NINTR.NE.NINTPR) THEN
C
C           Regular Gauss points
C
            CALL GAUSSP
C           -----------
C
            IRECAL = 0
         END IF
C
C        MII cubic 4-node shape functions
C        Compute shape functions and their derivatives for
C        cubic element on the boundary and store in common FSCUB
C
         IF(EFJXI.NE.EFJXIP.OR.NINTR.NE.NINTPR) THEN
C
C           XI direction
C
            IRECAL = 0
            DO IP=1,NINTR
               CHI = (ETARP(IP) + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0XI(J,IP) = FSF(J)
                  FN1XI(J,IP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
         IF(EFJET.NE.EFJETP.OR.NINTR.NE.NINTPR) THEN
C
C           ETA direction
C
            IRECAL = 0
            DO JP=1,NINTR
               CHI = (ETARP(JP) + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0ET(J,JP) = FSF(J)
                  FN1ET(J,JP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
C
C        Calculates bidim shape functions at all reg/sing integ. points
C
         IF(IRECAL.EQ.0) THEN
            ILOC = 0
            DO J=1,NNODE
               DO I=1,NNODE
                  ILOC = ILOC + 1
                  DO IP=1,NINTR
                     DO JP=1,NINTR
C
C                       Shape functions at all regular integ. points
C
                        FIRIP(ILOC,IP,JP)  = FN0XI(I,IP)*FN0ET(J,JP)
                        DFIXIP(ILOC,IP,JP) = FN1XI(I,IP)*FN0ET(J,JP)
                        DFIETP(ILOC,IP,JP) = FN0XI(I,IP)*FN1ET(J,JP)
                     END DO
                  END DO
               END DO
            END DO
         END IF
C
C        AP treatment for locating pressure force contribution of IE
C
	 IF(IBCOND(NSIDE).EQ.3) THEN
   	    ICOUNT(NSIDE-1) = ICOUNT(NSIDE-1) + 1
	    IB     = INODF(NSIDE,1)
	    IF(NSIDE.EQ.2.OR.NSIDE.EQ.4) MK = MY
	    IF(NSIDE.EQ.3.OR.NSIDE.EQ.5) MK = MX
	    IPRES1 = NODE2(1) - IB - ((ICOUNT(NSIDE-1)-1)/MK)*(MK+1) + 1
	    IPRES2 = IPRES1 + 1
	    DELZM  = HALF*(XYZLO2(3,3) + XYZLO2(4,3) -
     .                     XYZLO2(1,3) - XYZLO2(2,3))
	 END IF
C
C     ...Analysis of possible subdivisions for element IE and nodes NOI
C        in case of interior field computations
C
         IF(IFIELD) THEN
            EFJXI = XICON(IE,1)
            EFJET = XICON(IE,2)
C
C           Preparing for minimum distance computations
C
            DO I=2,NNODE
	       DO J=1,3
                  VIE(I-1,J) = XYZLO2(I,J) - XYZLO2(1,J)
               END DO
            END DO
	    V12(1) = VIE(1,2)*VIE(2,3) - VIE(2,2)*VIE(1,3)
	    V12(2) = VIE(1,3)*VIE(2,1) - VIE(2,3)*VIE(1,1)
	    V12(3) = VIE(1,1)*VIE(2,2) - VIE(2,1)*VIE(1,2)
	    V13(1) = VIE(1,2)*VIE(3,3) - VIE(3,2)*VIE(1,3)
	    V13(2) = VIE(1,3)*VIE(3,1) - VIE(3,3)*VIE(1,1)
	    V13(3) = VIE(1,1)*VIE(3,2) - VIE(3,1)*VIE(1,2)
	    ZNV12 = DSQRT(V12(1)*V12(1)+V12(2)*V12(2)+V12(3)*V12(3))
	    ZNV13 = DSQRT(V13(1)*V13(1)+V13(2)*V13(2)+V13(3)*V13(3))
	    DO J=1,3
	       V12(J) = V12(J)/ZNV12
	       V13(J) = V13(J)/ZNV13
	    END DO
C
C           Longest 1/2 diagonal chord in elt. IE
C
            DO K=1,2
	       DIAG(K) = ZERO
	       DO J=1,3
	          DIAG(K) = DIAG(K) + (XYZLO2(K+2,J) - XYZLO2(K,J))**2
	       END DO
	    END DO
	    DIAM = HALF*DSQRT(DMAX1(DIAG(1),DIAG(2)))
C
C           Coordinates of elt. IE center
C
	    DO J=1,3
	       XCIE(J) = ZERO
	       DO I=1,NNODE
	          XCIE(J) = XCIE(J) + XYZLO2(I,J)
	       END DO
	       XCIE(J) = QUART*XCIE(J)
	    END DO
C
C           Loop over interior points
C
            DO JOI=1,NOI
C
C              Check if the interior point is beneath the free surface
C
               IF(IREP(JOI).EQ.1) THEN
C
C              Distance of JOI from elt IE center plus minima d12, d13
C
	       DISTCI = ZERO
	       D12    = ZERO
	       D13    = ZERO
	       DO J=1,3
	          DISTCI = DISTCI + (XCIE(J) - FIELD(J,JOI))**2
		  D12    = D12 + (XYZLO2(1,J) - FIELD(J,JOI))*V12(J)
		  D13    = D13 + (XYZLO2(1,J) - FIELD(J,JOI))*V13(J)
	       END DO
	       DISTCI = DSQRT(DISTCI)
	       DMINPP = DMIN1(DABS(D12), DABS(D13))
C
C              Distance of JOI from elt IE 4 nodes
C
	       DO I=1,NNODE
	          DISND(I) = ZERO
		  DO J=1,3
		     DISND(I) = DISND(I) + (XYZLO2(I,J)-FIELD(J,JOI))**2
		  END DO
		  DISND(I) = DSQRT(DISND(I))
	       END DO
	       DISMI = DMIN1(DISND(1),DISND(2),DISND(3),DISND(4))
	       DISDA = DIAM/DISTCI
C
               DISTB = DSQRT(DISTCI*DISTCI + DMINPP*DMINPP)
	       IF(DISTCI.GT.DISTB) THEN
	          ANGLE = TWO*DATAN2(DIAM*DMINPP,DISTCI*DISTCI)
	       ELSE
	          ANGLE = TWO*DATAN2(DIAM,DMINPP)
	       END IF
C
               IF((ANGLE.GT.ALMAXI).AND.(DISDA.GT.ONE)) THEN
C
C                 Cas d'un angle obtu avec noeuds proche interieur elt
C
	          DISDA = DIAM/DMINPP
                  NSUB  = INT(DLOG10(DISDA/TANA)*DLOG2I + CRAT)
	       ELSE
                  IF(DISMI.LT.TWO*DIAM) THEN
C
C                    Cas d'un angle aigu avec noeuds alignes en dehors elt
C
                     DISDA = DIAM/(DISMI*TANA)
                     NSUB = 0
                     IF((DISDA.GT.ONE).AND.(DISDA.LT.THREE)) THEN
                        NSUB = 1
                     ELSE IF((DISDA.GE.THREE).AND.(DISDA.LT.SIX)) THEN
                        NSUB = 2
                     ELSE IF(DISDA.GE.SIX) THEN
                        NSUB = 3
                     END IF
                  ELSE
                     NSUB = 0
                  END IF
               END IF
C
               IF(NSUB.GT.4) THEN
                  NSUB = 4
               END IF
C
               LISUB(JOI) = NSUB
 	       IF(NSUB.NE.0) WRITE(6,*) IE,JOI,NSUB,DISTCI,
     .                                  DISMI,DMINPP,DISDA,ANGLE
               END IF
            END DO
	 END IF
C
C     ...Start integral loop
C
         DISCHE = ZERO
         DO IP=1,NINTR
            DO JP=1,NINTR
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
               DFIX(J) = DFIXIP(J,IP,JP)
               DFIE(J) = DFIETP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Final Gauss Jacobian
C
            WWJAC = ZJAC*WEIGTR(IP)*WEIGTR(JP)
C
C              Compute Phi,Phin,... at integration points by
C              shape functions summation and contributions to volume and
C              energy for element IE, int. pt. IP,JP
C
               PHIP   = ZERO
               PHINP  = ZERO
               PHISP  = ZERO
               PHIMP  = ZERO
               PHITP  = ZERO
               PHITNP = ZERO
C
               DO I=1,NNODM
                  L      = NODE1(I)
                  PHIP   = PHIP   + FI(I)*PHI(L)
                  PHINP  = PHINP  + FI(I)*PHIN(L)
                  PHISP  = PHISP  + FI(I)*PHIS(L)
                  PHIMP  = PHIMP  + FI(I)*PHIM(L)
                  PHITP  = PHITP  + FI(I)*PHIT(L)
                  PHITNP = PHITNP + FI(I)*PHITN(L)
               END DO
C
C              Compute kinetic and potential energy, volume, local flux
C
               EKTOT  = EKTOT  + PHIP*PHINP*WWJAC
	       EPTOT  = EPTOT  + XYZP(3)*XYZP(3)*ZNP(3)*WWJAC
	       VOLTOT = VOLTOT + XYZP(3)*ZNP(3)*WWJAC
	       IF(NSIDE.EQ.1) THEN
	          VOLWAV = VOLWAV + DABS(XYZP(3))*ZNP(3)*WWJAC
	       END IF
	       DISCHE = DISCHE + PHINP*WWJAC
C
C              Below z=0 values for possible corrections
C
	       IF(NSIDE.NE.1) THEN
	          VOLBOT = VOLBOT + XYZP(3)*ZNP(3)*WWJAC
		  EPBOT  = EPBOT  + XYZP(3)*XYZP(3)*ZNP(3)*WWJAC
	       END IF
	       IF(NSIDE.EQ.6) THEN
	          VOLSLI = VOLSLI + XYZP(3)*ZNP(3)*WWJAC
	       END IF
C
C              Energy related to the wavemaker supposed at face 2
C
	       IF(NSIDE.EQ.2) THEN
                  EKPAD  = EKPAD  + PHIP*PHINP*WWJAC
		  EPPAD  = EPPAD  + XYZP(3)*XYZP(3)*ZNP(3)*WWJAC
	       END IF
C
C              Case of absorbing piston on NSIDE
C
	       IF(IBCOND(NSIDE).EQ.3) THEN
C
C                 Compute pressure and integrated dynamic
C                 pressure force on AP
C
                  U2         = PHISP*PHISP + PHIMP*PHIMP + PHINP*PHINP
		  FPAPJ      = -HALF*(PHITP + U2*HALF)*RHO*WWJAC
                  FPXY(NSIDE-1,IPRES1) = FPXY(NSIDE-1,IPRES1) + FPAPJ
                  FPXY(NSIDE-1,IPRES2) = FPXY(NSIDE-1,IPRES2) + FPAPJ
		  DELSM(NSIDE-1,IPRES1) = DELSM(NSIDE-1,IPRES1) +
     .                                    HALF*WWJAC/(DELZM*MZ)
		  DELSM(NSIDE-1,IPRES2) = DELSM(NSIDE-1,IPRES2) +
     .                                    HALF*WWJAC/(DELZM*MZ)
	       END IF
C
C              Internal fields
C
               IF(IFIELD) THEN
                  NSUBP = 0
		  DO JOI=1,NOI
C
C                   Check if the interior point is beneath the free surface
C
                    IF(IREP(JOI).EQ.1) THEN
                     NSUB = LISUB(JOI)
                     NSG  = 2**NSUB
		     NSG2 = NSG*NSG
C
                     IF(NSUB.EQ.0) THEN
                        IF(NSUBP.NE.0) THEN
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Final Gauss Jacobian
C
            WWJAC = ZJAC*WEIGTR(IP)*WEIGTR(JP)
C
                           PHIP   = ZERO
                           PHINP  = ZERO
                           PHITP  = ZERO
                           PHITNP = ZERO
                           DO I=1,NNODM
                              L      = NODE1(I)
                              PHIP   = PHIP   + FI(I)*PHI(L)
                              PHINP  = PHINP  + FI(I)*PHIN(L)
                              PHITP  = PHITP  + FI(I)*PHIT(L)
                              PHITNP = PHITNP + FI(I)*PHITN(L)
			   END DO
	     	           NSUBP = 0
                        END IF
		     ELSE
		        DSG   = TWO/DFLOAT(NSG)
                        NSUBP = NSUB
		     END IF
C
                     DO ISG=1,NSG
		        IF(NSUB.NE.0) THEN
		           XILI = TWO*(ISG-1)/DFLOAT(NSG) - ONE
			   XIRI = XILI + DSG
	                   XIM  = HALF*(XILI + XIRI)
			   XI   = ETARP(IP)/DFLOAT(NSG) + XIM
                           CHI  = (XI + ONE)*OTHREE + EFJXI
                           CALL SHAPFN(CHI,FSF,NNODE)
C                          -----------
                           CALL SHAPD1(CHI,FSD,NNODE)
C                          -----------
                           DO J=1,NNODE
                              FN0XI(J,IP) = FSF(J)
                              FN1XI(J,IP) = OTHREE*FSD(J)
                           END DO
			END IF
C
                        DO JSG=1,NSG
		           IF(NSUB.NE.0) THEN
		              ETLI = TWO*(JSG-1)/DFLOAT(NSG) - ONE
		      	      ETRI = ETLI + DSG
			      ETM  = HALF*(ETLI + ETRI)
			      ETA  = ETARP(JP)/DFLOAT(NSG) + ETM
                              CHI  = (ETA + ONE)*OTHREE + EFJET
                              CALL SHAPFN(CHI,FSF,NNODE)
C                             -----------
                              CALL SHAPD1(CHI,FSD,NNODE)
C                             -----------
                              DO J=1,NNODE
                                 FN0ET(J,JP) = FSF(J)
                                 FN1ET(J,JP) = OTHREE*FSD(J)
                              END DO
C
C                             Shape functions at points IP,JP,ISG,JSG
C
                              ILOC = 0
                              DO J=1,NNODE
                                 DO I=1,NNODE
                                    ILOC = ILOC + 1
                                    FI(ILOC)   = FN0XI(I,IP)*FN0ET(J,JP)
                                    DFIX(ILOC) = FN1XI(I,IP)*FN0ET(J,JP)
                                    DFIE(ILOC) = FN0XI(I,IP)*FN1ET(J,JP)
                                 END DO
                              END DO
C
C                             Cord., unit vectors and Jacob.
C
                              CALL CARAC2
C                             -----------
                              WWJAC = ZJAC*WEIGTR(IP)*WEIGTR(JP)
C
C                             Recalculate boundary fields at IP,JP,ISG,JSG
C
                              PHIP   = ZERO
                              PHINP  = ZERO
                              PHITP  = ZERO
                              PHITNP = ZERO
                              DO I=1,NNODM
                                 L      = NODE1(I)
                                 PHIP   = PHIP   + FI(I)*PHI(L)
                                 PHINP  = PHINP  + FI(I)*PHIN(L)
                                 PHITP  = PHITP  + FI(I)*PHIT(L)
                                 PHITNP = PHITNP + FI(I)*PHITN(L)
                              END DO
			   END IF
C
C                          Compute quantities related to GREEN's function
C
                           CST   = PII4*WWJAC/DFLOAT(NSG2)
                           R2    = ZERO
                           DGNUM = ZERO
                           DO J=1,3
                              XX(J) = XYZP(J) - FIELD(J,JOI)
                              DGNUM = DGNUM + XX(J)*ZNP(J)
                              R2    = R2    + XX(J)*XX(J)
C                   IF((iloop.EQ.1).AND.(IE.EQ.10).AND.(JOI.EQ.10)) THEN
C                           write(*,*) FIELD(J,JOI)
C                           END IF
                           END DO
                           R  = DSQRT(R2)
C                   IF((iloop.EQ.1).AND.(IE.EQ.10).AND.(JOI.EQ.10)) THEN
C                           write(*,*) R,NSG 
C                           END IF
	        	   R3 = R2*R
                           R5 = R3*R2
C
C                          Computation of Green's functions at (IP,JP)
C                          Computation of integral contributions
C
                           GP    =  CST/R
                           DGDNP = -CST*DGNUM/R3
C
C              Image method flat bottom (affects regular/one sg. integrals)
C
               IF(IBOT.EQ.10) THEN
                  HP = H0 + FIELD(3,JOI)
                     RP     = R2 + FOUR*HP*(HP + XX(3))
                     RP     = DSQRT(RP)
		     DGPNUM = DGNUM + TWO*HP*ZNP(3)
		     GP     = GP + CST/RP
		     DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
	       END IF
C
CSYM           Image method y-symmetry (affects regular/one sg. integrals)
C
               IF(ISYM.EQ.1) THEN
		  YP = FIELD(2,JOI)
                     RP     = R2 + FOUR*YP*XYZP(2)
                     RP     = DSQRT(RP)
		     DGPNUM = DGNUM + TWO*YP*ZNP(2)
		     GP     = GP + CST/RP
		     DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
C
C                 + Image method flat bottom (affects regular/one sg. integrals)
C
                  IF(IBOT.EQ.10) THEN
                        RP     = R2+FOUR*(YP*XYZP(2)+HP*(HP+XX(3)))
                        RP     = DSQRT(RP)
		        DGPNUM = DGNUM + TWO*(YP*ZNP(2)+ HP*ZNP(3))
		        GP     = GP + CST/RP
		        DGDNP  = DGDNP - CST*DGPNUM/(RP*RP*RP)
	          END IF
	       END IF
CMYS
                           FIELD(4,JOI) = FIELD(4,JOI) + PHINP*GP
     .                                                 - PHIP*DGDNP
                           FIELD(8,JOI) = FIELD(8,JOI) + PHITNP*GP
     .                                                 - PHITP*DGDNP
		           DO J=1,3
		  	      DELGP(J)  = CST*XX(J)/R3
			      DELGNP(J) = CST*(ZNP(J) -
     .                                    THREE*DGNUM*XX(J)/R2)/R3
C
C              Image method flat bottom (affects regular/one sg. integrals)
C
               IF(IBOT.EQ.10) THEN
                     RP2     = R2 + FOUR*HP*(HP + XX(3))
                     RP     = DSQRT(RP2)
                     RP3    = RP2*RP
		     DGPNUM = DGNUM + TWO*HP*ZNP(3)
		     IF(J.EQ.3) THEN
		     DELGP(J)=DELGP(J)+CST*(-XX(J)-TWO*HP)/RP3
		     DELGNP(J)=DELGNP(J)+CST*(-ZNP(J) -
     .                        THREE*DGPNUM*(-XX(J)-TWO*HP)/RP2)/RP3
                     ELSE
		     DELGP(J)=DELGP(J)+CST*XX(J)/RP3
		     DELGNP(J)=DELGNP(J)+CST*(ZNP(J) -
     .                        THREE*DGPNUM*XX(J)/RP2)/RP3
		     END IF
	       END IF
C
CSYM           Image method y-symmetry (affects regular/one sg. integrals)
C
               IF(ISYM.EQ.1) THEN
		     RP2     = R2 + FOUR*YP*XYZP(2)
                     RP     = DSQRT(RP2)
                     RP3    = RP2*RP
                     DGPNUM = DGNUM + TWO*YP*ZNP(2)
		     IF(J.EQ.2) THEN
		     DELGP(J)=DELGP(J)+CST*(-XX(J)-TWO*YP)/RP3
		     DELGNP(J)=DELGNP(J)+CST*(-ZNP(J) -
     .                        THREE*DGPNUM*(-XX(J)-TWO*YP)/RP2)/RP3
                     ELSE
		     DELGP(J)=DELGP(J)+CST*XX(J)/RP3
		     DELGNP(J)=DELGNP(J)+CST*(ZNP(J) -
     .                        THREE*DGPNUM*XX(J)/RP2)/RP3
		     END IF
C
C                 + Image method flat bottom (affects regular/one sg. integrals)
C
                  IF(IBOT.EQ.10) THEN
                        RP2     = R2+FOUR*(YP*XYZP(2)+HP*(HP+XX(3)))
                        RP     = DSQRT(RP2)
                        RP3    = RP2*RP
		        DGPNUM = DGNUM + TWO*(YP*ZNP(2)+ HP*ZNP(3))
		        IF(J.EQ.2) THEN
		        DELGP(J)=DELGP(J)+CST*(-XX(J)-TWO*YP)/RP3
		        DELGNP(J)=DELGNP(J)+CST*(-ZNP(J) -
     .                        THREE*DGPNUM*(-XX(J)-TWO*YP)/RP2)/RP3
                        ELSEIF(J.EQ.3) THEN
		        DELGP(J)=DELGP(J)+CST*(-XX(J)-TWO*HP)/RP3
		        DELGNP(J)=DELGNP(J)+CST*(-ZNP(J) -
     .                        THREE*DGPNUM*(-XX(J)-TWO*HP)/RP2)/RP3

			ELSE
		        DELGP(J)=DELGP(J)+CST*XX(J)/RP3
		        DELGNP(J)=DELGNP(J)+CST*(ZNP(J) -
     .                        THREE*DGPNUM*XX(J)/RP2)/RP3
		        END IF
	          END IF
	       END IF
CMYS
                              FIELD(4+J,JOI) = FIELD(4+J,JOI) +
     .                                   PHINP*DELGP(J) - PHIP*DELGNP(J)
                              FIELD(8+J,JOI) = FIELD(8+J,JOI) +
     .                                 PHITNP*DELGP(J) - PHITP*DELGNP(J)
		           END DO
C
C                          Computation of internal particle accelerations
C
                           DELQP = CST*(THREE*XX(1)*XX(1)/R2 - ONE)/R3
                           DELQNP = CST*(SIX*XX(1)*ZNP(1) + THREE*DGNUM
     .                              - FIFT*XX(1)*XX(1)*DGNUM/R2)/R5
                           GRAD(1,1,JOI) = GRAD(1,1,JOI) +
     .                                     PHINP*DELQP - PHIP*DELQNP
                           DELQP = CST*THREE*XX(1)*XX(2)/R5
                           DELQNP = CST*(THREE*XX(2)*ZNP(1) +
     .                              THREE*XX(1)*ZNP(2) -
     .                              FIFT*XX(1)*XX(2)*DGNUM/R2)/R5
                           GRAD(1,2,JOI) = GRAD(1,2,JOI) +
     .                                     PHINP*DELQP - PHIP*DELQNP
                           GRAD(2,1,JOI) = GRAD(2,1,JOI) +
     .                                     PHINP*DELQP - PHIP*DELQNP
                           DELQP = CST*THREE*XX(1)*XX(3)/R5
                           DELQNP = CST*(THREE*XX(3)*ZNP(1) +
     .                              THREE*XX(1)*ZNP(3) -
     .                              FIFT*XX(1)*XX(3)*DGNUM/R2)/R5
                           GRAD(1,3,JOI) = GRAD(1,3,JOI) +
     .                                     PHINP*DELQP - PHIP*DELQNP
                           GRAD(3,1,JOI) = GRAD(3,1,JOI) +
     .                                     PHINP*DELQP - PHIP*DELQNP
                           DELQP = CST*(THREE*XX(2)*XX(2)/R2 - ONE)/R3
                           DELQNP = CST*(SIX*XX(2)*ZNP(2) + THREE*DGNUM
     .                              - FIFT*XX(2)*XX(2)*DGNUM/R2)/R5
                           GRAD(2,2,JOI) = GRAD(2,2,JOI) +
     .                                     PHINP*DELQP - PHIP*DELQNP
                           DELQP = CST*THREE*XX(2)*XX(3)/R5
                           DELQNP = CST*(THREE*XX(3)*ZNP(2) +
     .                              THREE*XX(2)*ZNP(3) -
     .                              FIFT*XX(2)*XX(3)*DGNUM/R2)/R5
                           GRAD(2,3,JOI) = GRAD(2,3,JOI) +
     .                                     PHINP*DELQP - PHIP*DELQNP
                           GRAD(3,2,JOI) = GRAD(3,2,JOI) +
     .                                     PHINP*DELQP - PHIP*DELQNP
                           DELQP = CST*(THREE*XX(3)*XX(3)/R2 - ONE)/R3
                           DELQNP = CST*(SIX*XX(3)*ZNP(3) + THREE*DGNUM
     .                              - FIFT*XX(3)*XX(3)*DGNUM/R2)/R5
                           GRAD(3,3,JOI) = GRAD(3,3,JOI) +
     .                                     PHINP*DELQP - PHIP*DELQNP
C
C              Image method flat bottom (affects regular/one sg. integrals)
C
               IF(IBOT.EQ.10) THEN
		     RP2     = R2 + FOUR*HP*(HP + XX(3))
                     RP     = DSQRT(RP2)
		     RP3    = RP*RP2
		     RP5    = RP3*RP2
		     DGPNUM = DGNUM + TWO*HP*ZNP(3)
                     DELQP = CST*(THREE*XX(1)*XX(1)/RP2-ONE)/RP3
                     DELQNP = CST*(SIX*XX(1)*ZNP(1) + THREE*DGPNUM
     .                        - FIFT*XX(1)*XX(1)*DGPNUM/RP2)/RP5
                     GRAD(1,1,JOI) = GRAD(1,1,JOI) +
     .                              PHINP*DELQP - PHIP*DELQNP
                     DELQP = CST*THREE*XX(1)*XX(2)/RP5
                     DELQNP = CST*(THREE*XX(2)*ZNP(1) +
     .                         THREE*XX(1)*ZNP(2) -
     .                         FIFT*XX(1)*XX(2)*DGPNUM/RP2)/RP5
                     GRAD(1,2,JOI) = GRAD(1,2,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     GRAD(2,1,JOI) = GRAD(2,1,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     DELQP = CST*THREE*XX(1)*(-XX(3)-TWO*HP)/RP5
                     DELQNP = CST*(THREE*(-XX(3)-TWO*HP)*ZNP(1) +
     .                        THREE*XX(1)*(-ZNP(3)) -
     .                        FIFT*XX(1)*(-XX(3)-TWO*HP)*DGPNUM/RP2)/RP5
                     GRAD(1,3,JOI) = GRAD(1,3,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     GRAD(3,1,JOI) = GRAD(3,1,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     DELQP = CST*(THREE*XX(2)*XX(2)/RP2 - ONE)/RP3
                     DELQNP = CST*(SIX*XX(2)*ZNP(2) + THREE*DGPNUM
     .                       - FIFT*XX(2)*XX(2)*DGPNUM/RP2)/RP5
                     GRAD(2,2,JOI) = GRAD(2,2,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     DELQP = CST*THREE*XX(2)*(-XX(3)-TWO*HP)/RP5
                     DELQNP = CST*(THREE*(-XX(3)-TWO*HP)*ZNP(2) +
     .                        THREE*XX(2)*(-ZNP(3)) -
     .                       FIFT*XX(2)*(-XX(3)-TWO*HP)*DGPNUM/RP2)/RP5
                     GRAD(2,3,JOI) = GRAD(2,3,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     GRAD(3,2,JOI) = GRAD(3,2,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     DELQP = CST*(THREE*(-XX(3)-TWO*HP)**2/RP2-ONE)/RP3
                     DELQNP = CST*(SIX*(-XX(3)-TWO*HP)*(-ZNP(3))
     .         +THREE*DGPNUM-FIFT*(-XX(3)-TWO*HP)**2*DGPNUM/RP2)/RP5
                     GRAD(3,3,JOI) = GRAD(3,3,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
	       END IF
C
CSYM           Image method y-symmetry (affects regular/one sg. integrals)
C
               IF(ISYM.EQ.1) THEN
		     RP2     = R2 + FOUR*YP*XYZP(2)
                     RP     = DSQRT(RP2)
                     RP3    = RP*RP2
		     RP5    = RP3*RP2
		     DGPNUM = DGNUM + TWO*YP*ZNP(2)
                     DELQP = CST*(THREE*XX(1)*XX(1)/RP2-ONE)/RP3
                     DELQNP = CST*(SIX*XX(1)*ZNP(1) + THREE*DGPNUM
     .                        - FIFT*XX(1)*XX(1)*DGPNUM/RP2)/RP5
                     GRAD(1,1,JOI) = GRAD(1,1,JOI) +
     .                              PHINP*DELQP - PHIP*DELQNP
                     DELQP = CST*THREE*XX(1)*(-XX(2)-TWO*YP)/RP5
                     DELQNP = CST*(THREE*(-XX(2)-TWO*YP)*ZNP(1) +
     .                        THREE*XX(1)*(-ZNP(2)) -
     .                      FIFT*XX(1)*(-XX(2)-TWO*YP)*DGPNUM/RP2)/RP5
                     GRAD(1,2,JOI) = GRAD(1,2,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     GRAD(2,1,JOI) = GRAD(2,1,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     DELQP = CST*THREE*XX(1)*XX(3)/RP5
                     DELQNP = CST*(THREE*XX(3)*ZNP(1) +
     .                        THREE*XX(1)*ZNP(3) -
     .                        FIFT*XX(1)*XX(3)*DGPNUM/RP2)/RP5
                     GRAD(1,3,JOI) = GRAD(1,3,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     GRAD(3,1,JOI) = GRAD(3,1,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     DELQP = CST*(THREE*(-XX(2)-TWO*YP)**2/RP2-ONE)/RP3
                     DELQNP = CST*(SIX*(-XX(2)-TWO*YP)*(-ZNP(2))
     .         + THREE*DGPNUM-FIFT*(-XX(2)-TWO*YP)**2*DGPNUM/RP2)/RP5
                     GRAD(2,2,JOI) = GRAD(2,2,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     DELQP = CST*THREE*(-XX(2)-TWO*YP)*XX(3)/RP5
                     DELQNP = CST*(THREE*XX(3)*(-ZNP(2)) +
     .                        THREE*(-XX(2)-TWO*YP)*ZNP(3) -
     .                       FIFT*(-XX(2)-TWO*YP)*XX(3)*DGPNUM/RP2)/RP5
                     GRAD(2,3,JOI) = GRAD(2,3,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     GRAD(3,2,JOI) = GRAD(3,2,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                     DELQP = CST*(THREE*XX(3)*XX(3)/RP2 - ONE)/RP3
                     DELQNP = CST*(SIX*XX(3)*ZNP(3) + THREE*DGPNUM
     .                       - FIFT*XX(3)*XX(3)*DGPNUM/RP2)/RP5
                     GRAD(3,3,JOI) = GRAD(3,3,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
C
C                 + Image method flat bottom (affects regular/one sg. integrals)
C
                  IF(IBOT.EQ.10) THEN
		      RP2     = R2+FOUR*(YP*XYZP(2)+HP*(HP+XX(3)))
                      RP     = DSQRT(RP2)
		      RP3    = RP*RP2
		      RP5    = RP3*RP2
		      DGPNUM = DGNUM + TWO*(YP*ZNP(2)+ HP*ZNP(3))
                   DELQP = CST*(THREE*XX(1)*XX(1)/RP2-ONE)/RP3
                   DELQNP = CST*(SIX*XX(1)*ZNP(1) + THREE*DGPNUM
     .                        - FIFT*XX(1)*XX(1)*DGPNUM/RP2)/RP5
                   GRAD(1,1,JOI) = GRAD(1,1,JOI) +
     .                              PHINP*DELQP - PHIP*DELQNP
                   DELQP = CST*THREE*XX(1)*(-XX(2)-TWO*YP)/RP5
                   DELQNP = CST*(THREE*(-XX(2)-TWO*YP)*ZNP(1) +
     .                         THREE*XX(1)*(-ZNP(2)) -
     .                       FIFT*XX(1)*(-XX(2)-TWO*YP)*DGPNUM/RP2)/RP5
                   GRAD(1,2,JOI) = GRAD(1,2,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                   GRAD(2,1,JOI) = GRAD(2,1,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                   DELQP = CST*THREE*XX(1)*(-XX(3)-TWO*HP)/RP5
                   DELQNP = CST*(THREE*(-XX(3)-TWO*HP)*ZNP(1) +
     .                      THREE*XX(1)*(-ZNP(3)) -
     .                      FIFT*XX(1)*(-XX(3)-TWO*HP)*DGPNUM/RP2)/RP5
                   GRAD(1,3,JOI) = GRAD(1,3,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                   GRAD(3,1,JOI) = GRAD(3,1,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                   DELQP = CST*(THREE*(-XX(2)-TWO*YP)**2/RP2 - ONE)/RP3
                   DELQNP = CST*(SIX*(-XX(2)-TWO*YP)*(-ZNP(2))
     .           + THREE*DGPNUM-FIFT*(-XX(2)-TWO*YP)**2*DGPNUM/RP2)/RP5
                   GRAD(2,2,JOI) = GRAD(2,2,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                   DELQP = CST*THREE*(-XX(2)-TWO*YP)*(-XX(3)-TWO*HP)/RP5
                   DELQNP = CST*(THREE*(-XX(3)-TWO*HP)*(-ZNP(2)) +
     .                        THREE*(-XX(2)-TWO*YP)*(-ZNP(3)) -
     .             FIFT*(-XX(2)-TWO*YP)*(-XX(3)-TWO*HP)*DGPNUM/RP2)/RP5
                   GRAD(2,3,JOI) = GRAD(2,3,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                   GRAD(3,2,JOI) = GRAD(3,2,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
                   DELQP = CST*(THREE*(-XX(3)-TWO*HP)**2/RP2 - ONE)/RP3
                   DELQNP = CST*(SIX*(-XX(3)-TWO*HP)*(-ZNP(3))
     .          + THREE*DGPNUM - FIFT*(-XX(3)-TWO*HP)**2*DGPNUM/RP2)/RP5
                   GRAD(3,3,JOI) = GRAD(3,3,JOI) +
     .                               PHINP*DELQP - PHIP*DELQNP
	          END IF
	       END IF
CMYS
			END DO
		     END DO
                    END IF
		  END DO
	       END IF
            END DO
         END DO
C
C        Flux per side
C
         DISCH(NSIDE) = DISCH(NSIDE) + DISCHE
C
         NINTPR = NINTR
         NSIDEP = NSIDE
         EFJXIP = EFJXI
         EFJETP = EFJET
      END DO
CBOT
      IF (IBOT.EQ.10) THEN
         EPTOT = EPTOT - NX*NY*H0*H0
         VOLTOT = VOLTOT + NX*NY*H0
         EPBOT = EPBOT - NX*NY*H0*H0
         VOLBOT = VOLBOT + NX*NY*H0
      END IF
CSYM
      IF (ISYM.EQ.1) THEN
         EKTOT = TWO*EKTOT
         EPTOT = TWO*EPTOT
         VOLTOT = TWO*VOLTOT
         EPBOT = TWO*EPBOT
         VOLBOT = TWO*VOLBOT
         VOLWAV = TWO*VOLWAV
         EKPAD = TWO*EKPAD
         EPPAD = TWO*EPPAD
         DO K=1,KFMAX
            DISCH(K) = TWO*DISCH(K)
         END DO
      END IF
C
C.....Dimensional energies
C
      EPTOT = HALF*GE*RHO*EPTOT
      EPBOT = HALF*GE*RHO*EPBOT
      EPPAD = HALF*GE*RHO*EPPAD
      EKSUR = EKTOT - EKBOT
      EPSUR = EPTOT - EPBOT
      VOLSUR = VOLTOT - VOLBOT
      EKTOT = HALF*RHO*EKTOT
      EKPAD = HALF*RHO*EKPAD
      ETTOT = EPTOT + EKTOT
C
C     Total flux
C
      OUTFLO = ZERO
C
      DO K=1,KFMAX
         OUTFLO = OUTFLO + DISCH(K)
      END DO
C
C=====Motion of AP on k boundaries if applied force > threshold
C
      DO K=2,MAX(5,KFMAX)
         IF(IBCOND(K).EQ.3) THEN
C
C           Initialize moving averages for AP
C
	    IF(ILOOP.EQ.1) THEN
	       ETMAV = ZERO
	       DUMAV = ZERO
	       DO L=1,NMVAV
	          ETAVG(K-1,L) = ZERO
	          DUAVG(K-1,L) = ZERO
	       END DO
	    END IF
C
            IF(K.EQ.2.OR.K.EQ.4) THEN
C
C              Constant depth AP with DE1 (2) or DE2 (4)
C
               IST      = K - 3
	       IB       = INODF(K,1)
	       NK       = MY+1
	       NU1      = IB + MZ*NK
	       NU2      = NU1 + MY
	       WID      = W0
	       BZ(K-1)  = XYZNOD(IB,3)
	       BXY(K-1) = XYZNOD(IB + NK/2,1)
	       WRITE(IO6,*) 'K,BZ(K),BXY(K)',K,BZ(K-1),BXY(K-1)
	    ELSE
C
C              Variable depth AP => mean depth for eq.
C
               IST     = K - 4
	       IB      = INODF(K,1)
	       NK      = MX+1
	       NU1     = IB + MZ*NK
	       NU2     = NU1 + MX
	       WID     = ZL0
	       BZ(K-1) = ZERO
               DO I=1,NK
	          JB = IB + I - 1
	          BZ(K-1) = BZ(K-1) + XYZNOD(JB,3)
	       END DO
	       BZ(K-1)  = BZ(K-1)/NK
	       BXY(K-1) = XYZNOD(IB+ NK/2,2)
  	    END IF
C
C           Average surface amplitude at AP
C
	    ET(K-1) = ZERO
	    DO I=NU1,NU2
	       ET(K-1) = ET(K-1) + XYZNOD(I,3)
	    END DO
	    ET(K-1) = ET(K-1)/(NU2 - NU1 + 1)
C
C           Total hydrodynamic force
C
            FPTOT = ZERO
	    WIDS  = ZERO
	    DO I=1,NK
	       FPTOT = FPTOT + FPXY(K-1,I)
	       WIDS  = WIDS + DELSM(K-1,I)
	    END DO
C
C           Reference hydrostatic pressure
C
            FPREF = HALF*RHO*GE*(BZ(K-1)**2)*WID
C
C           AP moves when forces > threshold at beginning (up < eps)
C
	    WRITE(IO6,*) 'K,FPTOT,FPREF,WIDS',K,FPTOT,FPREF,WIDS
            IF((DABS(FPTOT/FPREF).GT.EPSI).OR.
     .         (DABS(UPNEW(K-1,NK/2)).GT.EPS)) THEN
C
C              Piston-like boundary condition for fin
C              FPXY >0 as pressure => positive (x,y) direction by *IST
C
               DO I=1,NK
                  FPXY(K-1,I)  = FPXY(K-1,I)*IST
                  UPNEW(K-1,I) = FPXY(K-1,I)/(DSQRT(GE*DABS(BZ(K-1)))*
     .                              RHO*DABS(BZ(K-1))*DELSM(K-1,I))
C
C                 Trying to avoid alternate oscillations in value =>
C                 moving average over
C                 past two values (note this leads to negative
C                 EABSP of small value in a few cases
C
                  UPNEW(K-1,I) = HALF*(UPNEW(K-1,I) + UPIST(K-1,I))
C
C                 Acceleration is based on previous time step
C
                  IF(ILOOP.GT.2.OR.ISTART.EQ.1) THEN
                     DTIM1   = TIME - TIMEP
                     DTI     = DT
                     DUPDT1  = (UPNEW(K-1,I)-UPIST1(K-1,I))/(DTI+DTIM1)
                     DUPDT(K-1,I) = HALF*(DUPDT(K-1,I) + DUPDT1)
C
C                    Capping of AP aceleration ! arbitrary so far.
C
		     IF(DABS(DUPDT(K-1,I)).GT.(GE/50.)) THEN
		        DUPDT(K-1,I) = (GE/50.)*DSIGN(ONE,DUPDT(K-1,I))
		     END IF
                  ELSE
                     DUPDT(K-1,I) = ZERO
                  END IF
                  WRITE(IO6,*) 'I,UPNEW(K,I),DUPDT(K)',I,UPNEW(K-1,I),
     .                          DUPDT(K-1,I)
	       END DO
            ELSE
               DO I=1,NK
                  UPNEW(K-1,I) = ZERO
                  DUPDT(K-1,I) = ZERO
	       END DO
	    END IF
C
C           Moving averages of mean eta and dupdt, avg. up, at AP
C
	    DO L=1,NMVAV
	       ETAVG(K-1,L+1) = ETAVG(K-1,L)
	       DUAVG(K-1,L+1) = DUAVG(K-1,L)
	    END DO
	    ETAVG(K-1,1) = ET(K-1)
	    DUAVG(K-1,1) = ZERO
	    UPAVG        = ZERO
	    DO I=1,NK
	       DUAVG(K-1,1) = DUAVG(K-1,1) + DUPDT(K-1,I)
	       UPAVG        = UPAVG + UPNEW(K-1,I)
	    END DO
	    DUAVG(K-1,1) = DUAVG(K-1,1)/NK
	    UPAVG        = UPAVG/NK
C
	    ETMAV = ZERO
	    DUMAV = ZERO
	    DO L=1,NMVAV
	       DUMAV = DUMAV + DUAVG(K-1,L)
	       ETMAV = ETMAV + ETAVG(K-1,L)
	    END DO
            DUMAV = DUMAV/NMVAV
            ETMAV = ETMAV/NMVAV
C
            WRITE(88+K,2000) TIME,BXY(K-1),ET(K-1),FPTOT,
     .                      UPIST1(K-1,NK/2),UPIST(K-1,NK/2),
     .                      UPAVG,DUAVG(K-1,1),DUMAV,ETMAV
C
C           Adjustment/smoothing of AP acceleration using MAVG values
C
            DO I=1,NK
	       IF(DABS(DUAVG(K-1,1)).GT.EPS) THEN
                  DUPDT(K-1,I) = DUPDT(K-1,I)*DUMAV/DUAVG(K-1,1)
	       ELSE
	          DUPDT(K-1,I) = DUMAV
	       END IF
	    END DO
	 END IF
      END DO
      IF(ILOOP.GE.2) THEN
         TIMEP = TIME
      END IF
C
C=====Output results on files
C
      write(file_name1,3000) 
     .      'output/intspeeds','speed',iloop1

      write(file_name2,3000) 
     .      'output/intpoints','grid',iloop1

3000  format(a,'/',a,'_',i6.6,'.dat')
      open(33,file=file_name1)
      open(34,file=file_name2)
      do JOI=1,NOI
      write(34,*)(FIELD(J,JOI),J=1,3)
      write(33,*)(FIELD(4+J,JOI),J=1,3)
      enddo
      close(33)
      close(34)
C      WRITE(IO6,2000) TIME,EPTOT,EKTOT,ETTOT,VOLTOT,OUTFLO,EPSUR,VOLSUR
      WRITE(IO6,2000) TIME,EKPAD,EPPAD,EKBOT,EPBOT
      WRITE(IO6,2000) (DISCH(K),K=1,6)
C      WRITE(IO10,2000) TIME,EKTOT,EPTOT,ETTOT,EPSUR,EPSUR+EKTOT
!      WRITE(IO10,2000) TIME,EKTOT,EPTOT,ETTOT,EKSUR,EPSUR
      WRITE(IO10,2002) TIME,EKTOT,EPTOT,ETTOT,EPSUR,EPSUR+EKTOT,NX,NY,NZ
      IF(VOLWAV.LT.EPS) VOLWAV = ONE/EPS
      IF((IPTYP.LT.9).OR.(IPTYP.EQ.11)) THEN
         WRITE(IO11,2000) TIME,VOLTOT,VOLSUR,VOLWAV,OUTFLO,
     .                    OUTFLO*DT/VOLWAV
      ELSE
C
C        Calculate landslide volume
C
         VCORR = W0*(H0*D0 + DE2*ZL2 +
     .           HALF*(H0 + DE2)*(ZL0-ZL2-D0))
         VOLSLI = VCORR - VOLSLI
         WRITE(IO11,2000) TIME,VOLTOT,VOLWAV,OUTFLO,
     .                    OUTFLO*DT/VOLWAV,VOLSLI,OUTFLO*DT/VOLSLI
      END IF
C
C     Restart results every IPRINT loop
C
      IF(IPRINT.GT.0) THEN
         IF(DABS(DFLOAT(ILOOP)/DFLOAT(IPRINT)-ILOOP/IPRINT).LT.EPS
     .      .OR.ILOOP.EQ.ILMAX.OR.(TIME+DT).GE.TMAX
     .      .OR.((TIME+DT).GE.TPRINT.AND.TIME.LT.TPRINT)
     .      ) THEN
            write(*,*)'iprint detected'
            open(12,file='output/restart2.dat')
	    TMAXC  = DT*ILMAX
	    ISTART = 1
	    WRITE(IO15,*) MX,MY,MZ,NINTRT,NSUBC,ALMAXB,ALMAXI,
     .                   '      MX,MY,MZ,NINTR,NSUBC,ALMAXB,ALMAXI'
            WRITE(IO15,*) ZL0,W0,H0,X0,D0,IBOT,SLOPE,ZK,
     .                   '      ZL0,W0,H0,X0,D0,IBOT,SLOPE,ZK'
            WRITE(IO15,*) (IBCOND(K),K=1,6),'     (IBCOND(K),K=1,6)'
            WRITE(IO15,*) ILMAX,IPTYP,NBS,IPRINT,
     .                   '      ILMAX,IPTYP,NBS,IPRINT'
            WRITE(IO15,*) DT,TIME,TMAXC,RHO,CPRESS,CPREST,GE,TDAMP,
     .                    TDOWN,
     .                 '      DT,TSTART,TMAX,RHO,CPRESS,CPREST,GE,TDAMP'
            WRITE(IO15,*) DE1,DE2,OMEGA,V0H,INFIE,NOI,NHT,TOLMAX,
     .                   '      DE1,DE2,OMEGA,V0H,INFIE,NOI,NHT,TOLMAX'
            WRITE(IO15,*) XLDAMP,DNU0,TRESH,SUTENS,NPOLY,NGA,
     .                  '       XLDAMP,DNU0,TRESH,SUTENS,NPOLY,NGA'
            IF(NGA.GT.0) THEN
               DO IG=1,NGA
                  WRITE(IO15,*) XYZGAG(IG,1),XYZGAG(IG,2)
	       END DO
	    END IF
            WRITE(IO15,*) ISTART,IGRID,'ISTART,IGRID'
C
            IF((IPTYP.GE.9).AND.(IPTYP.NE.11)) THEN
C
C              Write data for underwater landslides
C
               WRITE(IO15,*) THETA,BSLIDE,WSLIDE,TSLIDE,DSLIDE,ZL2,
     .               RHOL,CM,CD,ZEPS,ZMU,ZAL,'THETA,BSLIDE,WSLIDE,',
     .               'TSLIDE,DSLIDE,ZL2,RHOL,CM,CD,ZEPS,ZMU,ZAL'
            END IF
C
            WRITE(IO15,*) NOM,NX,NY
 1666	    format(3(i7,1x),2(E24.16,1x),i7)    
            WRITE(12,1666) NOM,NX,NY,TIME,DT,ILOOP+ILOOP0
c            WRITE(*,1666) NOM,NX,NY,TIME,DT,ILOOP+ILOOP0
            DO I=1,NOM
c-----------------------------------------------------------------
c	oldline
c	       WRITE(IO15,2020) I,XYZNOD(I,1),XYZNOD(I,2),XYZNOD(I,3),
c     .                          PHI(I),PHIN(I)
c	       WRITE(12,2020) I,XYZNOD(I,1),XYZNOD(I,2),XYZNOD(I,3),
c     .                          PHI(I),PHIN(I)
c-----------------------------------------------------------------
c	New restart, XB
            WRITE(IO15,2001) I,XYZNOD(I,1),XYZNOD(I,2),XYZNOD(I,3),
     &                  PHI(I),PHIN(I),phit(i),phitn(i),
     &			  phis(i),phim(i),phiss(i),phimm(i),
     &			  phins(i),phinm(i),phits(i),phitm(i),
     &			  phism(i),phinn(i),
     &			  pres(i),dprdt(i),uvw(1,i),uvw(2,i),
     &			  uvw(3,i),
     &			  duvwdt(1,i),duvwdt(2,i),duvwdt(3,i)
            WRITE(12,2001) I,XYZNOD(I,1),XYZNOD(I,2),XYZNOD(I,3),
     &                  PHI(I),PHIN(I),phit(i),phitn(i),
     &			  phis(i),phim(i),phiss(i),phimm(i),
     &			  phins(i),phinm(i),phits(i),phitm(i),
     &			  phism(i),phinn(i),
     &			  pres(i),dprdt(i),uvw(1,i),uvw(2,i),
     &			  uvw(3,i),
     &			  duvwdt(1,i),duvwdt(2,i),duvwdt(3,i)

	    END DO
C
C           Initialization of AP boundaries
C
	    DO K=2,KFMAX
	       IF(IBCOND(K).EQ.3) THEN
	          IF(K.EQ.2.OR.K.EQ.4) NK = MY+1
	          IF(K.EQ.3.OR.K.EQ.5) NK = MX+1
	          DO J=1,NK
	             WRITE(12,*) K,UPNEW(K-1,J),DUPDT(K-1,J)
                  END DO
	       END IF
	    END DO
          close(12)
	 END IF
      END IF
C
C     Internal fields
C
      IF(IFIELD) THEN
         DO JOI=1,NOI
            DO J=1,3
               DO I=1,3
                  FIELD(8+J,JOI) = FIELD(8+J,JOI) + 
     .                             GRAD(J,I,JOI)*FIELD(4+I,JOI)
               END DO
            END DO
c            WRITE(IO60,2005) JOI,IREP(JOI),(FIELD(J,JOI),J=1,3)
c            WRITE(IO61,2005) JOI,IREP(JOI),(FIELD(J,JOI),J=5,7)
c            WRITE(IO62,2005) JOI,IREP(JOI),(FIELD(J,JOI),J=9,11)
	 END DO
      END IF
C
      RETURN
C
 2001  format(i7,1x,22(E24.16,1x))
 2000 FORMAT(10E16.8)
 2002 FORMAT(6(E16.8,1x),3(i5,1x))
 2005 FORMAT(I7,I3,3E16.8)
 2010 FORMAT(I5,3E16.8)
 2020 FORMAT(I5,5D24.16)
 2030 FORMAT(I5,1E16.8)
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE ISBREAKPT(ISBP,ILOOPBP)
C-----------------------------------------------------------------------
C     Determine si on est au break point (ISBP=1), et fixe la valeur de
C     ILOOP dans ILOOPBP
C
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

      COMMON /DATGEN/ ZL0,W0,H0,X0,D0,MX,MY,MZ,NX,NY,NZ,NSUBC,
     .                NINTRT,NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),
     .                DUM3(18),IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),ICUMUL(6),NDST(6,2),
     .                IDUM2(18),JSLIM(6),NELEF(6),NNODF(6),
     .                IELEF(6,2),INODF(6,2),IBCOND(6)
      COMMON /BOUNDC/ DT,DT2,DT22,TSTART,TMAX,RHO,CPRESS,CPREST,GE,
     .                DE1,DE2,OMEGA,V0H,TOLMAX,NBS
      COMMON /SYSTEM/ DUM1(24),TIME,TDAMP,TDOWN,DUM2(12),IO6,IFLAGS,
     .                IFLAGV,IPTYP,ILOOP
C
      SAVE
C
C.....Save commons and local variables to enable static commons
C
      ISBP=0
      ILOOPBP=0
      NS=NDST(1,1)
      NM=NDST(1,2)
      I=0
      J=0
   11 I=I+1
C         WRITE(IO6,*) 'I : ',I
   12 J=J+1
C         WRITE(IO6,*) 'J : ',J
      IND1=ICUMUL(1)+(J-1)*NS+I
      IND2=ICUMUL(1)+(J-1)*NS+I+1
      X1=XYZNOD(IND1,1)
      X2=XYZNOD(IND2,1)
      IF (X2.GT.X1) THEN
         IF (J.LT.NM) THEN
            GOTO 12
         ELSE
            IF (I.LT.NS-1) THEN
               J=0
               GOTO 11
            END IF
         END IF
      ELSE
         ISBP=1
         WRITE(IO6,*) 'I,J,IND1,X1,IND2,X2',I,J,IND1,X1,IND2,X2
         ILOOPBP=ILOOP
      END IF
C
      RETURN
C
      END
C------------------------------------------------------------------------- 
      SUBROUTINE DISTPI()
C-------------------------------------------------------------------------
C
C     Determine the interior points among those occupying the whole domain
C
C------------------------------------------------------------------------- 
      IMPLICIT REAL*8 (A-H,O-Z)
      INCLUDE 'param.inc'

      LOGICAL INIE
      CHARACTER*8 TEXTE
C 

      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,NX,NY,NZ,IDUM1(2),NOM,IDUM2       
      COMMON /SYSTEM/ DUM2(24),TIME,TDAMP,TDOWN,UPNEW(4,MYMAX),
     .                DUPDT(4,MYMAX),DUM3(4),IO6,IDUV(2),IPTYP,ILOOP
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),DUM4(18), 
     .                IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),IDUM5(MOMM),INTG(MOMM),LXYZ(NOMM,2),
     .                IDUM6(54),IELEF(6,2),INODF(6,2),IDUM7(6)  
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,IDUM8(2)
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM) 
      COMMON /INTEG/  ETARP(NINTM),WEIGTR(NINTM),PHIP12(NINTM),
     .                PHIP23(NINTM),RMIP12(NINTM),RMIP23(NINTM),
     .                RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .                XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .                ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /FSCUB/ FN0XI(NDM,NINTM),FN0ET(NDM,NINTM),
     .               FN1XI(NDM,NINTM),FN1ET(NDM,NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),DUM5(7*NINTM*NINTM)
      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
     .               FIS23(MNODM,NINTM,NINTM,MNOD),
     .               DFIX12(MNODM,NINTM,NINTM,MNOD),
     .               DFIX23(MNODM,NINTM,NINTM,MNOD),
     .               DFIE12(MNODM,NINTM,NINTM,MNOD),
     .               DFIE23(MNODM,NINTM,NINTM,MNOD)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM6(6),
     .                ZNP(3),ZJACP
      COMMON /INTERF/ FIELD(11,NOIM),LISUB(NOIM),IREP(NOIM),
     .                NXI,NYI,NZI,NOI,IFIELD
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION XCIE(3)
      DIMENSION FSF(NDM),FSD(NDM),FSFX12(NDM),FSFE12(NDM),
     .          FSDX12(NDM),FSDE12(NDM),FSFX23(NDM),FSFE23(NDM),
     .          FSDX23(NDM),FSDE23(NDM),XELT(MNOD,2)
C
      SAVE
C
      DATA BIG/1.E+10/,EPS/1.E-2/,QUART/0.25/,ZERO/0./
C
C     Initialisation
C
      NNODE = MNOD
C
C     Loop over the interior points
C
      DO JOI=1,NOI
C
C        First considered as exterior points
C
         IREP(JOI) = 0
C
C        Initial minimal distance interior point-element center
C
         DISIN = BIG
C
C        Loop over the elements on the free surface
C
         DO IE=IELEF(1,1),IELEF(1,2)
C
C        ...General parameters for elt. IE
C
            NSIDE = IFACE(IE)
            NINTR = INTG(IE)
C
C           Central Gauss point of the element
C
            NINTR2 = NINTR*NINTR
            IF(MOD(NINTR2,2).EQ.0) THEN
               NIN2 = NINTR2/2
            ELSE
               NIN2 = (NINTR2 + 1)/2
            END IF
C
C           If the interior point is inside the element 
C           There could be several elements satisfying the condition
C
C           Normal vector for the center of the element
C
            INIE = .FALSE.
C
            CALL POLYIN2(INIE,FIELD(1,JOI),FIELD(2,JOI))
C           ------------
C
           WRITE(*,*)'points internes1',
     .      IREP(JOI),(FIELD(J,JOI),J=1,3),DISTCI,EPS,DISTA,INIE
            IF(INIE) THEN
C
C           16-node connectivity
C
 	    ILOC = 0
	    DO J=1,NNODE
	     DO I=1,NNODE
	       ILOC = ILOC + 1
	       NODE1(ILOC)    = IGCON(IE,I,J)
	       XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
	       XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
	       XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYMD
               IF (ISYM.EQ.1) THEN
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.5.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYSD
	     END DO
	    END DO
C
C           4-node connectivity
C
            DO I=1,NNODE
             NODE2(I)    = ILCON(IE,I)
             XYZLO2(I,1) = XYZNOD(NODE2(I),1)
             XYZLO2(I,2) = XYZNOD(NODE2(I),2)
             XYZLO2(I,3) = XYZNOD(NODE2(I),3)
            END DO
C
            EFJXI = XICON(IE,1)
            EFJET = XICON(IE,2)
C
C        ...Filling up of the common INTEG when the elements have different
C           node or integration points numbers (accelerator)
C
            IRECAL = 1
            IF (NINTR.NE.NINTPR) THEN
C
C              Regular Gauss points
C
               CALL GAUSSP
C              -----------
C
               IRECAL = 0
            END IF
C
C           MII cubic 4-node shape functions
C           Compute shape functions and their derivatives for
C           cubic element on the boundary and store in common FSCUB
C
            IF(EFJXI.NE.EFJXIP.OR.NINTR.NE.NINTPR) THEN
C
C            XI direction
C
             IRECAL = 0
             DO IP=1,NINTR
               CHI = (ETARP(IP) + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0XI(J,IP) = FSF(J)
                  FN1XI(J,IP) = OTHREE*FSD(J)
               END DO
             END DO
            END IF
            IF(EFJET.NE.EFJETP.OR.NINTR.NE.NINTPR) THEN
C
C            ETA direction
C
             IRECAL = 0
             DO JP=1,NINTR
               CHI = (ETARP(JP) + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0ET(J,JP) = FSF(J)
                  FN1ET(J,JP) = OTHREE*FSD(J)
               END DO
             END DO
            END IF
C
C           Calculates bidim shape functions at all reg/sing integ. points
C 
            IF(IRECAL.EQ.0) THEN
             ILOC = 0
             DO J=1,NNODE
               DO I=1,NNODE
                  ILOC = ILOC + 1
                  DO IP=1,NINTR
                     DO JP=1,NINTR
C
C                       Shape functions at all regular integ. points
C
                        FIRIP(ILOC,IP,JP)  = FN0XI(I,IP)*FN0ET(J,JP)
                        DFIXIP(ILOC,IP,JP) = FN1XI(I,IP)*FN0ET(J,JP)
                        DFIETP(ILOC,IP,JP) = FN0XI(I,IP)*FN1ET(J,JP)
                     END DO
                  END DO
               END DO
             END DO
            END IF
            DO IP=1,NINTR
               DO JP=1,NINTR
                  DO ILOC=1,NNODM
                     FI(ILOC)   = FIRIP(ILOC,IP,JP)
                     DFIX(ILOC) = DFIXIP(ILOC,IP,JP)
                     DFIE(ILOC) = DFIETP(ILOC,IP,JP)
                  END DO
               END DO
            END DO
C
C           ...Gauss integration point cartes. coord., unit vectors and Jacob.
C              for ZNP
C
            CALL CARAC2
C           -----------
C
C              Coordinates of the element center
C
               DO J=1,3
                  XCIE(J) = ZERO
                  DO I=1,NNODE
                     XCIE(J) = XCIE(J) + XYZLO2(I,J)
                  END DO
                  XCIE(J) = QUART*XCIE(J)
               END DO
C
C              Distance interior point-element center
C
               DISTA = ZERO
               DO J=1,3
                  DISTA = DISTA + (FIELD(J,JOI) - XCIE(J))**2
               END DO
               DISTA = SQRT(DISTA)
C
C              Looking for the closest element to the interior point
C
           WRITE(*,*)'points internes1',
     .      IREP(JOI),(FIELD(J,JOI),J=1,3),DISTCI,EPS,DISTA
               IF(DISTA.LT.DISIN) THEN
C
C                 Scalar product to determine the orientation
C
                  DISTCI = ZERO
                  DO J=1,3
                     COMPOS = (FIELD(J,JOI) - XCIE(J))/DISTA
                     DISTCI = DISTCI + COMPOS*ZNP(J)
                  END DO
C
C                 Flag for the interior points
C                 If the scalar product is negative --> interior point 1
C                 ________________________ positive --> exterior point 0
C
C                  IF(DISTCI.LT.ZERO) THEN
                  IF(DISTCI.LT.-EPS) THEN
                     IREP(JOI) = 1
                  ELSE
                     IREP(JOI) = 0
                  END IF
C
                  DISIN = DISTA
               END IF
            END IF               
         END DO
C
C        Loop over the elements on the bottom
C
         DO IE=IELEF(6,1),IELEF(6,2)
C
C        ...General parameters for elt. IE
C
            NSIDE = IFACE(IE)
            NINTR = INTG(IE)
C
C           Central Gauss point of the element
C
            NINTR2 = NINTR*NINTR
            IF(MOD(NINTR2,2).EQ.0) THEN
               NIN2 = NINTR2/2
            ELSE
               NIN2 = (NINTR2 + 1)/2
            END IF
            INIE = .FALSE.
C
            CALL POLYIN2(INIE,FIELD(1,JOI),FIELD(2,JOI))
C           ------------
C
C           If the interior point is inside the element 
C           There could be several elements satisfying the condition
C
            IF(INIE) THEN
C
C           16-node connectivity
C
 	    ILOC = 0
	    DO J=1,NNODE
	     DO I=1,NNODE
	       ILOC = ILOC + 1
	       NODE1(ILOC)    = IGCON(IE,I,J)
	       XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
	       XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
	       XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYMD
               IF (ISYM.EQ.1) THEN
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.5.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYSD
	     END DO
	    END DO
C
C           4-node connectivity
C
            DO I=1,NNODE
             NODE2(I)    = ILCON(IE,I)
             XYZLO2(I,1) = XYZNOD(NODE2(I),1)
             XYZLO2(I,2) = XYZNOD(NODE2(I),2)
             XYZLO2(I,3) = XYZNOD(NODE2(I),3)
            END DO
C
            EFJXI = XICON(IE,1)
            EFJET = XICON(IE,2)
C
C        ...Filling up of the common INTEG when the elements have different
C           node or integration points numbers (accelerator)
C
            IRECAL = 1
            IF (NINTR.NE.NINTPR) THEN
C
C              Regular Gauss points
C
               CALL GAUSSP
C              -----------
C
               IRECAL = 0
            END IF
C
C           MII cubic 4-node shape functions
C           Compute shape functions and their derivatives for
C           cubic element on the boundary and store in common FSCUB
C
            IF(EFJXI.NE.EFJXIP.OR.NINTR.NE.NINTPR) THEN
C
C            XI direction
C
             IRECAL = 0
             DO IP=1,NINTR
               CHI = (ETARP(IP) + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0XI(J,IP) = FSF(J)
                  FN1XI(J,IP) = OTHREE*FSD(J)
               END DO
             END DO
            END IF
            IF(EFJET.NE.EFJETP.OR.NINTR.NE.NINTPR) THEN
C
C            ETA direction
C
             IRECAL = 0
             DO JP=1,NINTR
               CHI = (ETARP(JP) + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0ET(J,JP) = FSF(J)
                  FN1ET(J,JP) = OTHREE*FSD(J)
               END DO
             END DO
            END IF
C
C           Calculates bidim shape functions at all reg/sing integ. points
C 
            IF(IRECAL.EQ.0) THEN
             ILOC = 0
             DO J=1,NNODE
               DO I=1,NNODE
                  ILOC = ILOC + 1
                  DO IP=1,NINTR
                     DO JP=1,NINTR
C
C                       Shape functions at all regular integ. points
C
                        FIRIP(ILOC,IP,JP)  = FN0XI(I,IP)*FN0ET(J,JP)
                        DFIXIP(ILOC,IP,JP) = FN1XI(I,IP)*FN0ET(J,JP)
                        DFIETP(ILOC,IP,JP) = FN0XI(I,IP)*FN1ET(J,JP)
                     END DO
                  END DO
               END DO
             END DO
            END IF
            DO IP=1,NINTR
               DO JP=1,NINTR
                  DO ILOC=1,NNODM
                     FI(ILOC)   = FIRIP(ILOC,IP,JP)
                     DFIX(ILOC) = DFIXIP(ILOC,IP,JP)
                     DFIE(ILOC) = DFIETP(ILOC,IP,JP)
                  END DO
               END DO
            END DO
C
C           ...Gauss integration point cartes. coord., unit vectors and Jacob.
C              for ZNP
C
            CALL CARAC2
C           -----------
C
C
C              Coordinates of the element center
C
               DO J=1,3
                  XCIE(J) = ZERO
                  DO I=1,NNODE
                     XCIE(J) = XCIE(J) + XYZLO2(I,J)
                  END DO
                  XCIE(J) = QUART*XCIE(J)
               END DO
C
C              Distance interior point-element center
C
               DISTA = ZERO
               DO J=1,3
                  DISTA = DISTA + (FIELD(J,JOI) - XCIE(J))**2
               END DO
               DISTA = SQRT(DISTA)
C
C              Looking for the closest element to the interior point
C
               IF(DISTA.LT.DISIN) THEN
C
C                 Scalar product to determine the orientation
C
                  DISTCI = ZERO
                  DO J=1,3
                     COMPOS = (FIELD(J,JOI) - XCIE(J))/DISTA
                     DISTCI = DISTCI + COMPOS*ZNP(J)
                  END DO
C
C                 Flag for the interior points
C                 If the scalar product is negative --> interior point 1
C                 ________________________ positive --> exterior point 0
C
                  IF(DISTCI.GE.-EPS) IREP(JOI) = 0
C
                  DISIN = DISTA
               END IF
            END IF               
         END DO
         WRITE(*,*)'points internes',IREP(JOI),(FIELD(J,JOI),J=1,3)
      END DO
C
      RETURN
C
 2000 FORMAT(I5,5F10.5)
 2010 FORMAT(5I5,2F10.5)
 2020 FORMAT(17I5)
C
      END
