C
C                  SHAPE FUNCTIONS AND SLIDING DERIVATIVES
C
C----------------------------------------------------------------------
      SUBROUTINE SHAPFN(ETA,FI,NNODE)
C---------------------------------------------------------------------
C
C     Computes 1D shape functions for given values of ETA and NNODE
C     and returns it in FI
C
c     Jan. 99 S. Grilli, INLN
C---------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      include 'param.inc'
C
      DIMENSION FI(NDM)
C
      SAVE
C
      DATA ONE/1.D0/,HALF/.5D0/,S/0.0625D0/,S9/0.5625D0/,RNINE/9.D0/,
     .     OS/.166666666666667D0/,FT/1.33333333333333D0/,FOUR/4.D0/,
     .     THREE/3.D0/,TWO/2.D0/
C
      IF(NNODE.EQ.2) THEN
         FI(1)=HALF*(ONE-ETA)
         FI(2)=HALF*(ONE+ETA)
      ELSE IF(NNODE.EQ.3) THEN
         FI(1)=HALF*ETA*(ETA-ONE)
         FI(2)=ONE-ETA*ETA
         FI(3)=HALF*ETA*(ETA+ONE)
      ELSE IF(NNODE.EQ.4) THEN
         FI(1)=S*(ONE-ETA)*(RNINE*ETA*ETA-ONE)
         FI(2)=S9*(ONE-ETA*ETA)*(ONE-THREE*ETA)
         FI(3)=S9*(ONE-ETA*ETA)*(ONE+THREE*ETA)
         FI(4)=S*(ONE+ETA)*(RNINE*ETA*ETA-ONE)
      ELSE IF(NNODE.EQ.5) THEN
         FI(1)=OS*ETA*(ETA-ONE)*(FOUR*ETA*ETA-ONE)
         FI(2)=FT*ETA*(ONE-ETA*ETA)*(TWO*ETA-ONE)
         FI(3)=(ETA*ETA-ONE)*(FOUR*ETA*ETA-ONE)
         FI(4)=FT*ETA*(ONE-ETA*ETA)*(TWO*ETA+ONE)
         FI(5)=OS*ETA*(ETA+ONE)*(FOUR*ETA*ETA-ONE)
      END IF
C
      RETURN
C
      END
C---------------------------------------------------------------------
      SUBROUTINE SHAPD1(ETA,DFI,NNODE)
C---------------------------------------------------------------------
C
C     Computes 1D shape fcts 1st derivatives for given values of ETA and
C     NNODE and returns it in DFI
C
C     Jan. 99 S. Grilli, INLN
C---------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      include 'param.inc'
C
      DIMENSION DFI(NDM)
C
      SAVE
C
      DATA TWO/2.D0/,HALF/.5D0/,S/.0625D0/,S18/1.125D0/,S27/1.6875D0/,
     .     OS/.166666666666667D0/,FT/1.33333333333333D0/,FOUR/4.D0/,
     .     S81/5.0625D0/,THREE/3.D0/,FIVE/5.D0/,EIGHT/8.D0/,ONE/1.D0/
C
      IF(NNODE.EQ.2) THEN
         DFI(1)=-HALF
         DFI(2)= HALF
      ELSE IF(NNODE.EQ.3) THEN
         DFI(1)= ETA-HALF
         DFI(2)= -TWO*ETA
         DFI(3)= ETA+HALF
      ELSE IF(NNODE.EQ.4) THEN
         DFI(1)=  -S27*ETA*ETA+S18*ETA+S
         DFI(2)=  S81*ETA*ETA-S18*ETA-S27
         DFI(3)=  -S81*ETA*ETA-S18*ETA+S27
         DFI(4)=  S27*ETA*ETA+S18*ETA-S
      ELSE IF(NNODE.EQ.5) THEN
         DFI(1)=  OS*(FOUR*ETA*ETA*(FOUR*ETA-THREE)-TWO*ETA+ONE)
         DFI(2)= -FT*(ETA*ETA*(EIGHT*ETA-THREE)-FOUR*ETA+ONE)
         DFI(3)=  TWO*ETA*(EIGHT*ETA*ETA-FIVE)
         DFI(4)= -FT*(ETA*ETA*(EIGHT*ETA+THREE)-FOUR*ETA-ONE)
         DFI(5)=  OS*(FOUR*ETA*ETA*(FOUR*ETA+THREE)-TWO*ETA-ONE)
      END IF
C
      RETURN
C
      END
C---------------------------------------------------------------------
      SUBROUTINE SHAPD2(ETA,D2FI,NNODE)
C---------------------------------------------------------------------
C
C     Computes 1D shape fcts 2nd derivatives for given values of ETA and
C     NNODE and returns it in D2FI
C
C     Feb. 99 S. Grilli, INLN
C---------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      include 'param.inc'
C
      DIMENSION D2FI(NDM)
C
      SAVE
C
      DATA ZERO/0.D0/,ONE/1.D0/,TWO/2.D0/,S18/1.125D0/,S54/3.375D0/,
     .     OT/.333333333333333D0/,FOUR/4.D0/,TEN/10.D0/,EIGHT/8.D0/,
     .     S162/10.125D0/,ST/5.33333333333333D0/,FE/48.D0/
C
      IF(NNODE.EQ.2) THEN
         D2FI(1)= ZERO
         D2FI(2)= ZERO
      ELSE IF(NNODE.EQ.3) THEN
         D2FI(1)=  ONE
         D2FI(2)= -TWO
         D2FI(3)=  ONE
      ELSE IF(NNODE.EQ.4) THEN
         D2FI(1)= -S54*ETA  + S18
         D2FI(2)=  S162*ETA - S18
         D2FI(3)= -S162*ETA - S18
         D2FI(4)=  S54*ETA  + S18
      ELSE IF(NNODE.EQ.5) THEN
         D2FI(1)= FOUR*ETA*(TWO*ETA - ONE)   - OT
         D2FI(2)= EIGHT*ETA*(ONE - FOUR*ETA) + ST
         D2FI(3)= FE*ETA*ETA - TEN
         D2FI(4)=-EIGHT*ETA*(ONE + FOUR*ETA) + ST
         D2FI(5)= FOUR*ETA*(TWO*ETA + ONE)   - OT
      END IF
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE SLIDING(IKF)
C----------------------------------------------------------------------
C0  SLIDINGF  SLIDING
C1  PURPOSE   Compute the s-der. (d/ds,d2/ds2) on the >=IKF bo. as a funct.
C1            of the domain geometry (X,Y,Z) and save them into /FSLID/
C1            /TPLID/ in the form of shape functions for later use.
C1            The computation uses 2D sliding 4th order (5-node) boundary
C1            element. The derivatives are evaluated at the center of
C1            a 5x5 grid of points ISLCON, except at the borders of each face.
C1            The points are then at all locations IXIET of the grid.
C1            Note : Calculates only for IBCOND <> 0
C1
C2  CALL      CALL SLIDING
C3  CALL ARG. XYZNOD(NOM,3)       = Coordin. of nofes for current step
C3            INODF(6,2)          = Begin/ending node Nbs. for each face
C3            ISLCON(NDM2,NOM)    = NDM2=25 node numbers for each point
C3            IXIET(2,NOM)        = xi,eta coordinates of point in grid
C3            IKF                 => boundaries GE.IKF calculated
C3
C4  RET. ARG. /FSLID/,/TPLID/
C6  INT. CALL SHAPFN,SHAPD1,SHAPD2,ERRORS
C10 Feb. 1999  S. GRILLI, INLN/Aug. 2000  S. GRILLI, non-ortho CF/PG
CLSLIDING SUB. WHICH COMPUTES S-DERIV. ON THE BOUNDARY FOR 3D-BEM (1st,2nd)
C-----------------------------------------------------------------------
      IMPLICIT REAL*8(A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM1(5),IDUM1(3),NX,NY,NZ,NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(MOMM*2+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7+2*NOMM+66),
     .                INODF(6,2),IBCOND(6)
      COMMON /FSLID/ FI0XE(NDM,NDM),FI1XE(NDM,NDM),FI2XE(NDM,NDM),
     .               FISL(NDM2,NDM,NDM),FISLX(NDM2,NDM,NDM),
     .               FISLE(NDM2,NDM,NDM),FISLXX(NDM2,NDM,NDM),
     .               FISLEE(NDM2,NDM,NDM),FISLXE(NDM2,NDM,NDM),
     .               ISLCON(NDM2,NOMM),IXIET(2,NOMM)
      COMMON /TPLID/ VS(NOMM,3),VM(NOMM,3),VN(NOMM,3),DVS(NOMM),
     .               DVM(NOMM),DVN(NOMM),XSS(NOMM,3),XMM(NOMM,3),
     .               XSM(NOMM,3),VCSSN(NOMM),VCMMN(NOMM),VCSMN(NOMM),
     .               XKA(NOMM)
      COMMON /CGAGE/  DUMGAG(17*NGAM),IDUGAG(NGAM),NGA,INTZED
c      COMMON/SYSTEM/  DUM3(31+8*MYMAX),IDUM4(4),ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOTFCT/ BOT(NXM,NYM),SLOPE,ZK,IBOT
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION FSD0(NDM),FSD1(NDM),FSD2(NDM),XYZLOC(NDM2,3),NODE(NDM2)
C
      DATA ZERO/0.D0/,QUART/0.25D0/,ONE/1.D0/,TWO/2.D0/,SIX/6.D0/
C
      SAVE
C     SAVE DATGEN/,/MAILLE/,/FSLID/,/TPLID/,/SYSTEM/
C
      NNODE = NDM
C
C.....Create sliding values for first loop ie 2D 4th-order shape fcts
C     and their first and second derivatives on a 5x5 grid of possible
C     locations of collocation point.
C
      IF(ILOOP.EQ.0) THEN
         DO I=1,NNODE
            XE = QUART*(TWO*I - SIX)
            CALL SHAPFN(XE,FSD0,NNODE)
C           -----------
            CALL SHAPD1(XE,FSD1,NNODE)
C           -----------
            CALL SHAPD2(XE,FSD2,NNODE)
C           -----------
            DO J=1,NNODE
               FI0XE(J,I) = FSD0(J)
               FI1XE(J,I) = FSD1(J)
               FI2XE(J,I) = FSD2(J)
            END DO
         END DO
         J = 0
         DO K=1,NNODE
            DO I=1,NNODE
               J = J + 1
               DO IX=1,NNODE
                  DO KE=1,NNODE
                     FISL(J,IX,KE)   = FI0XE(K,KE)*FI0XE(I,IX)
                     FISLX(J,IX,KE)  = FI0XE(K,KE)*FI1XE(I,IX)
                     FISLE(J,IX,KE)  = FI1XE(K,KE)*FI0XE(I,IX)
                     FISLXX(J,IX,KE) = FI0XE(K,KE)*FI2XE(I,IX)
                     FISLEE(J,IX,KE) = FI2XE(K,KE)*FI0XE(I,IX)
                     FISLXE(J,IX,KE) = FI1XE(K,KE)*FI1XE(I,IX)
                  END DO
               END DO
            END DO
         END DO
      END IF
C
C.....Loop over the 6 boundaries   (if surface gages also compute fields
C     for bottom (KF=6))
C
      KFMAX = 6
      IF(IBOT.EQ.10) KFMAX = KFMAX-1
      IF(ISYM.EQ.1) KFMAX = KFMAX-1
      DO KF=IKF,KFMAX
       IF((IBCOND(KF).NE.0).OR.
     .    (IBOT.NE.10.AND.NGA.NE.0.AND.KF.EQ.6)) THEN
         DO IK=INODF(KF,1),INODF(KF,2)
C
C        ...Local values on boundary segment for the NDM2 nodes of node IK
C
CSYM
               J=0
               DO JETA=1,NDM
                DO IXI=1,NDM
                  J=J+1
                  NODE(J) = ISLCON(J,IK)
                  DO K=1,3
                     XYZLOC(J,K) = XYZNOD(NODE(J),K)
                  END DO
                  IF (ISYM.EQ.1) THEN
                    IF (KF.EQ.1.AND.
     .              ((IK.GT.(INODF(KF,2)-2*NX).AND.JETA.EQ.NDM).OR.
     .              (IK.GT.(INODF(KF,2)-NX).AND.JETA.EQ.(NDM-1)))) THEN
                       XYZLOC(J,2) = -XYZLOC(J,2)
                    END IF
                    IF (KF.EQ.6.AND.
     .                  ((IK.LT.(INODF(KF,1)+2*NX).AND.JETA.EQ.1).OR.
     .                   (IK.LT.(INODF(KF,1)+NX).AND.JETA.EQ.2))) THEN
                       XYZLOC(J,2) = -XYZLOC(J,2)
                    END IF
                    IF (KF.EQ.4.AND.
     .                  ((MOD(IK-INODF(KF,1)+1,NY).EQ.0.AND.
     .                    (IXI.EQ.NDM.OR.IXI.EQ.(NDM-1))).OR.
     .                   (MOD(IK-INODF(KF,1)+2,NY).EQ.0.AND.
     .                    IXI.EQ.NDM))) THEN
                       XYZLOC(J,2) = -XYZLOC(J,2)
                    END IF
                    IF (KF.EQ.2.AND.
     .                  ((MOD(IK-INODF(KF,1),NY).EQ.0.AND.
     .                    (IXI.EQ.1.OR.IXI.EQ.2)).OR.
     .                   (MOD(IK-INODF(KF,1)-1,NY).EQ.0.AND.
     .                    IXI.EQ.2))) THEN
                       XYZLOC(J,2) = -XYZLOC(J,2)
                    END IF
                  END IF
                END DO
               END DO
CMYS
C
C        ...Node IK location in 5x5 grid
C
            IX = IXIET(1,IK)
            KE = IXIET(2,IK)
C
C        ...Unit vectors (s,m,n) for point IK
C
            DVS(IK) = ZERO
            DVM(IK) = ZERO
C
C           Tangential vectors s,m and curvatures x_ss, x_mm, x_sm
C
            DO K=1,3
               VS(IK,K)  = ZERO
               VM(IK,K)  = ZERO
               XSS(IK,K) = ZERO
               XMM(IK,K) = ZERO
               XSM(IK,K) = ZERO
C
               DO J=1,NDM2
                  VS(IK,K)  = VS(IK,K)  + FISLX(J,IX,KE)*XYZLOC(J,K)
                  VM(IK,K)  = VM(IK,K)  + FISLE(J,IX,KE)*XYZLOC(J,K)
                  XSS(IK,K) = XSS(IK,K) + FISLXX(J,IX,KE)*XYZLOC(J,K)
                  XMM(IK,K) = XMM(IK,K) + FISLEE(J,IX,KE)*XYZLOC(J,K)
                  XSM(IK,K) = XSM(IK,K) + FISLXE(J,IX,KE)*XYZLOC(J,K)
               END DO
               DVS(IK) = DVS(IK) + VS(IK,K)*VS(IK,K)
               DVM(IK) = DVM(IK) + VM(IK,K)*VM(IK,K)
            END DO
            DVS(IK) = DSQRT(DVS(IK))
            DVM(IK) = DSQRT(DVM(IK))
C
C        ...Get unit vectors
C
            SCAL = ZERO
            DO K=1,3
               VS(IK,K) = VS(IK,K)/DVS(IK)
               VM(IK,K) = VM(IK,K)/DVM(IK)
               SCAL = SCAL + VS(IK,K)*VM(IK,K)
            END DO
            XKA(IK) = SCAL
            SQKA = ONE/DSQRT(ONE - SCAL*SCAL)
C
C           Normal vector n = s x m
C
            VN(IK,1) = VS(IK,2)*VM(IK,3) - VS(IK,3)*VM(IK,2)
            VN(IK,2) = VS(IK,3)*VM(IK,1) - VS(IK,1)*VM(IK,3)
            VN(IK,3) = VS(IK,1)*VM(IK,2) - VS(IK,2)*VM(IK,1)
            VN(IK,1) = SQKA*VN(IK,1)
            VN(IK,2) = SQKA*VN(IK,2)
            VN(IK,3) = SQKA*VN(IK,3)
            DVN(IK)  = DSQRT(VN(IK,1)*VN(IK,1) + VN(IK,2)*VN(IK,2) +
     .                       VN(IK,3)*VN(IK,3))
            VN(IK,1) = VN(IK,1)/DVN(IK)
            VN(IK,2) = VN(IK,2)/DVN(IK)
            VN(IK,3) = VN(IK,3)/DVN(IK)
         END DO
       END IF
      END DO
C
C.....Curvature terms on free surface
C
      DO IK=INODF(1,1),INODF(2,2)
         VCSSN(IK) = ZERO
	 VCMMN(IK) = ZERO
	 VCSMN(IK) = ZERO
         DO K=1,3
            VCSSN(IK) = VCSSN(IK) + XSS(IK,K)*VN(IK,K)
	    VCMMN(IK) = VCMMN(IK) + XMM(IK,K)*VN(IK,K)
	    VCSMN(IK) = VCSMN(IK) + XSM(IK,K)*VN(IK,K)
	 END DO
         VCSSN(IK) = VCSSN(IK)/DVS(IK)**2
	 VCMMN(IK) = VCMMN(IK)/DVM(IK)**2
	 VCSMN(IK) = VCSMN(IK)/(DVS(IK)*DVM(IK))
      END DO
C
      RETURN
C
      END
C
