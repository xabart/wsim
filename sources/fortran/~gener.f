C
C                             MESH GENERATION
C                             REGRIDDING
C                             TIME STEP UPDATING
C                             CELES 
C			      Autoregrid, XB
C-------------------------------------------------------------------------------
c      SUBROUTINE GENER(PHI,PHIN,ISTART,IGRID,IO5,IO60,IO63,IO16,IO8,
c     .                 XMIN,DXMIN,DYMIN)
      SUBROUTINE GENER(ISTART,IGRID,IO5,IO60,IO63,IO16,IO8,
     .                 XMIN,DXMIN,DYMIN,DT)
C-------------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'
      PARAMETER (NSLMAX=800)

C-------------------------------------------------------------------------------
      CHARACTER*8 TEXTE
      LOGICAL*4 IFIELD
      EXTERNAL DACOSH
C-------------------------------------------------------------------------------
      COMMON/DATGEN/  ZL0,W0,H0,X0,D0,MX,MY,MZ,NX,NY,NZ,NSUBC,
     .                NINTR,NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),XICON(MOMM,2),XSTART(6,3),
     .                IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),ICUMUL(6),NDST(6,2),IDST(6,3),
     .                JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),
     .                INODF(6,2),IBCOND(6)
c      COMMON /SYSTEM/ DUM1(27),UPNEW(4,MYMAX),DUPDT(4,MYMAX),UB(4),IO6,
c     .                IDUM1(2),IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /INTERF/ FIELD(11,NOIM),LISUB(NOIM),IREP(NOIM),
     .                NXI,NYI,NZI,NOI,IFIELD
      COMMON /FSLID/  DUM2(3*NDM2+NDM2*NDM2*6),ISLCON(NDM2,NOMM),
     .                IXIET(2,NOMM)
      COMMON /BOTFCT/ BOT(NXM,NYM),SLOPE,ZK,IBOT
c      COMMON /REGRD/  XYZNDR(NOMM,3),PHIR(NOMM),PHINR(NOMM),
c     .                DUM3(MOMM,2),IDUM2(NOMM,MNOD,MNOD),
c     .                IDUM3(MOMM,4),IREG(NOMM),NOMP,NXP,NYP
      COMMON /BOUNDC/ DUM4(3),TSTART,TMAX,RHO,DUM5(2),GE,DE1,DE2,
     .                DUM6,V0HL,DUM7,IDUM4
      COMMON /LNDSLD/ THETA,BSLIDE,WSLIDE,TSLIDE,DSLIDE,ZL2,RHOL,GAMMA,
     .                CM,CD,ZEPS,ZMU,ZMUC,ZAL,UT,A0,S0,T0,WLEGTH,
     .                ZKX,ZKY,STHET,CTHET,TTHET,COSHEP,COSHMU,
     .                XINIT,TSLMAX,S,ST,STT,XBEGIN,XEND,XCENTR,XI0,
     .                SDELTT,DELTAS,ZKS,TSLOPE,DELTAT,AINIT,TANHXI,
     .                ZKRAMP,UINIT,TANHTI,SINIT,COSHTI,RATIO2
      COMMON /SLKINE/ TSL(NSLMAX),SSL(NSLMAX),USL(NSLMAX),ASL(NSLMAX),
     .                NSTART,NSL
      COMMON /CGAGE/  XYZGAG(NGAM,3),UVWG(NGAM,3),PHITB(NGAM),
     .                PHIG(NGAM),PHING(NGAM),PHIB(NGAM),PHINB(NGAM),
     .                XYZBOT(NGAM,3),UVWB(NGAM,3),IGAG(NGAM),NGA,INTZED
      COMMON /OPTION/ ISYM,IPOST,IVECT

      include 'result.h' ! pg(nomm),phi(nomm),phin(nomm),phit(nomm),phitn(nomm),
c                          phis(nomm),phim(nomm),phiss(nomm),phimm(nomm),
c                          phins(nomm),phinm(nomm),phits(nomm),phitm(nomm),
c                          phism(nomm),phinn(nomm),
c                          pres(nomn),dprdt(nomm),uvw(3,nomm),duvwdt uvw(3,nomm)
      include 'regrid.h' 	

C-------------------------------------------------------------------------------
c      DIMENSION DELNOD(3),PHI(NOM),PHIN(NOM)
c
c	correction new restart, XB
      DIMENSION DELNOD(3)
C-------------------------------------------------------------------------------
      SAVE
C-------------------------------------------------------------------------------
      DATA ONE/1.D0/,TWTHI/0.6666666666666667D0/,IDST/1,-2,1,2,-1,1,
     .     2,3,3,3,3,-2,3,1,2,1,2,3/,HALF/0.5D0/,ZERO/0.D0/,EPS/1.D-08/,
     .     EIGHT/8.D0/,PI/3.1415926535898D0/,TEXTE/'GENER01 '/,NMAX/10/,
     .     TWO/2.D0/,ZEPSMA/0.5D0/,PIDEG/180.D0/,FRACT/0.1D0/,FIVE/5./,
     .     FRACA/5.D-02/,EPST/1.D-03/,XIRAMP/3.D0/,FOUR/4.D0/,
     .     THREE/3.D0/,NTIME/100/,
     .     ZMAX/0.8D0/,P01/0.1D0/

C-------------------------------------------------------------------------------
C     Function
C-------------------------------------------------------------------------------
      UDASH(TEMPS) = HALF*AINIT*DLOG(DCOSH(ZKRAMP*TEMPS -
     .               XIRAMP)/DCOSH(XIRAMP))/(ZKRAMP*TANHXI)
C-------------------------------------------------------------------------------
C.....General constants
C-------------------------------------------------------------------------------
      NX  = MX + 1
      NY  = MY + 1
      NZ  = MZ + 1
      IF((NX.LT.5).OR.(NY.LT.5).OR.(NZ.LT.5)) CALL ERRORS(TEXTE)
C-------------------------------------------------------------------------------
C     Cumulative node numbers on faces,number of nodes in s,n
C     directions for each face
C-------------------------------------------------------------------------------
      ICUMUL(1) = 0
      ICUMUL(2) = NX*NY
      ICUMUL(3) = NX*NY + NY*NZ
      ICUMUL(4) = NX*NY + NY*NZ + NX*NZ
      ICUMUL(5) = NX*NY + 2*NY*NZ + NX*NZ
      ICUMUL(6) = NX*NY + 2*NY*NZ + 2*NX*NZ
C-------------------------------------------------------------------------------
      NDST(1,1) = NX
      NDST(1,2) = NY
      NDST(2,1) = NY
      NDST(2,2) = NZ
      NDST(3,1) = NX
      NDST(3,2) = NZ
      NDST(4,1) = NY
      NDST(4,2) = NZ
      NDST(5,1) = NX
      NDST(5,2) = NZ
      NDST(6,1) = NX
      NDST(6,2) = NY
C-------------------------------------------------------------------------------
CSYM  Skip plan of symetry if ISYM=1
C-------------------------------------------------------------------------------
      IF (ISYM.EQ.1) THEN
         ICUMUL(5)=NX*NY+2*NY*NZ+NX*NZ
         ICUMUL(6)=-1

         NDST(5,1)=NX
         NDST(5,2)=NY
         NDST(6,1)=-1
         NDST(6,2)=-1

         IDST(5,1)=1
         IDST(5,2)=-2
         IDST(5,3)=3
         IDST(6,1)=0
         IDST(6,2)=0
         IDST(6,3)=0

         IBCOND(5)=IBCOND(6)
         IBCOND(6)=-1
      END IF

C-------------------------------------------------------------------------------
C     Delta x,y,z, for initial discretization box
C-------------------------------------------------------------------------------
      DELNOD(1) = ZL0/MX
      DELNOD(2) = W0/MY
      DELNOD(3) = H0/MZ

C-------------------------------------------------------------------------------
C.....Bottom geometry (flat with IBOT=1) and start node coordinates
C     Irregular bottom is introduced here for IBOT > 1
C-------------------------------------------------------------------------------
      W02  = HALF*W0
      W0SL = W0

C-------------------------------------------------------------------------------
CSYM     y-symmetry for other cases than IBOT=6
C-------------------------------------------------------------------------------
      IF((IBOT.NE.6).AND.(ISYM.EQ.1)) THEN
         W02  = W0
         W0SL = TWO*W0
      END IF

C-------------------------------------------------------------------------------
C THIS FUNCTION IS DEFINE BY CHRISTOPHE KASSIOTIS
C-------------------------------------------------------------------------------
      IF(IBOT.EQ.7) THEN
         DELX = DELNOD(1)
         DO I=1,NX
            XB = X0+DELX*(I-1)
            IF(XB.LE.(X0+D0).OR.XB.GT.(X0+D0+SLOPE)) THEN
               ZB = -H0
               DO J=1,NY
                  BOT(I,J) = ZB
               END DO
            ELSE
               ZB = -H0 + ZK
               DO J=1,NY
                  BOT(I,J) = ZB
               END DO
            ENDIF
            
         END DO
      
C-------------------------------------------------------------------------------
C        Constant depth
C-------------------------------------------------------------------------------
      ELSE IF(IBOT.EQ.1) THEN
         DO I=1,NX
            DO J=1,NY
               BOT(I,J) = -H0
            END DO
         END DO

C-------------------------------------------------------------------------------
C        Constant slope SLOPE at starting at do
C-------------------------------------------------------------------------------
      ELSE IF(IBOT.EQ.2) THEN
         DELX = DELNOD(1)
         DO I=1,NX
            XB = X0 + DELX*(I-1)
            IF(XB.LE.(X0 + D0)) THEN
               ZB = -H0
            ELSE
               ZB = SLOPE*(XB - D0 - X0) - H0
            END IF
            DO J=1,NY
               BOT(I,J) = ZB
            END DO
         END DO

C-------------------------------------------------------------------------------
C        Constant SLOPE laterally modulated starting at do
C-------------------------------------------------------------------------------
      ELSE IF(IBOT.EQ.3) THEN
         DELX = DELNOD(1)
         DELY = DELNOD(2)
         CSTB = ONE/DCOSH(ZK*HALF*W0SL)**2
         CSTD = ONE/(ONE - CSTB)
         DO I=1,NX
            XB = X0 + DELX*(I-1)
            IF(XB.LE.(X0 + D0)) THEN
               ZB  = -H0
               DO J=1,NY
                  BOT(I,J) = ZB
               END DO
            ELSE
               ZU  = SLOPE*(XB - D0 - X0)
               DO J=1,NY
                  YB = -W02 + DELY*(J-1)
                  BOT(I,J) = ZU*(ONE/(DCOSH(ZK*YB)**2) - CSTB)*CSTD - H0
               END DO
            END IF
         END DO

C-------------------------------------------------------------------------------
C        Constant SLOPE laterally modulated starting at do
C-------------------------------------------------------------------------------
      ELSE IF(IBOT.EQ.4) THEN
         DELX = DELNOD(1)
         DELY = DELNOD(2)
         DO I=1,NX
            XB = X0 + DELX*(I-1)
            IF(XB.LT.(X0 + D0)) THEN
               ZB  = -H0
               DO J=1,NY
                  BOT(I,J) = ZB
               END DO
            ELSE
               ZU  = SLOPE*(XB - D0 - X0)
               DO J=1,NY
                  YB = -W02 + DELY*(J-1)
                  BOT(I,J) = ZU/(DCOSH(ZK*YB)**2) - H0
               END DO
            END IF
         END DO

C-------------------------------------------------------------------------------
C        Double sech^2 landslide (collab. with Ph. Watts 8/00)
C-------------------------------------------------------------------------------
C        Landslide physical variables
C-------------------------------------------------------------------------------
      ELSE IF((IBOT.EQ.5.OR.IBOT.EQ.6).AND.(IPTYP.GE.9)) THEN
         GAMMA = RHOL/RHO
         THETC = THETA*PI/PIDEG
         STHET = DSIN(THETC)
         CTHET = DCOS(THETC)
         TTHET = DTAN(THETC)
         SLOPE = TTHET
C-------------------------------------------------------------------------------
C     ...Kinematics
C-------------------------------------------------------------------------------
         IF(IPTYP.EQ.9) THEN
C-------------------------------------------------------------------------------
C           Underwater rigid landslide (see Grilli and Watts EABE99)
C-------------------------------------------------------------------------------
            UT = DSQRT(GE*BSLIDE)*DSQRT(HALF*PI*(GAMMA-ONE)*
     .                 STHET/CD)
            A0 = GE*(GAMMA-ONE)*STHET/(GAMMA+CM)
         ELSE IF(IPTYP.EQ.10) THEN
C-------------------------------------------------------------------------------
C           Underwater rigid slump (see Watts and Grilli ASCE00)
C-------------------------------------------------------------------------------
C-------------------------------------------------------------------------------
C           Underwater rigid slide with experimental acceleration curve
C           read from file here for ILOOP=0: (t,s,u,a)
C-------------------------------------------------------------------------------
         ELSE IF(IPTYP.EQ.11) THEN
            OPEN(UNIT=100,STATUS='UNKNOWN',FILE='slkinem.dat')
            NK = 1
   10 	   READ(100,*,END=20) TSL(NK),SSL(NK),USL(NK),ASL(NK)
            NK = NK + 1
            IF(NK.LE.NSLMAX) GOTO 10
   20       NSL = NK - 1
         END IF

C-------------------------------------------------------------------------------
C        Center of mass motion for t=TSTART
C-------------------------------------------------------------------------------
         IF(IPTYP.NE.11) THEN
C-------------------------------------------------------------------------------
C           Nondimensional landslide motion variables
C-------------------------------------------------------------------------------
            BS2D   = HALF*BSLIDE/DSLIDE
            CST    = BS2D*STHET
            T0     = UT/A0
            S0     = UT*T0
            WLEGTH = BS2D*T0*DSQRT(GE*DSLIDE)*TTHET/
     .                      (DSQRT(ONE+CST) - DSQRT(ONE-CST))
            TIMEI  = FRACA*T0
            AINIT  = A0 /(DCOSH(TIMEI/T0)**2)
            TANHXI = DTANH(XIRAMP)
            ZKRAMP = TWO*XIRAMP/TIMEI
            UINIT  = HALF*AINIT*TIMEI + UDASH(TIMEI)
            TANHTI = DTANH(TIMEI/T0)
            COSHTI = DCOSH(TIMEI/T0)

C-------------------------------------------------------------------------------
C              Ramp up of initial acceleration
C-------------------------------------------------------------------------------
            IF(TSTART.LE.TIMEI) THEN
               STT    = HALF*(ONE + DTANH(ZKRAMP*TSTART - XIRAMP)/
     .                  TANHXI)*AINIT
               ST     = HALF*AINIT*TSTART + UDASH(TSTART)
               S      = AINIT*(TSTART**2)/FOUR
C-------------------------------------------------------------------------------
C                 Integration by trapezoidal rule
C-------------------------------------------------------------------------------

               IF(TSTART.GT.EPS) THEN
                  DTIME = TSTART/(NTIME-1)
                  DO IT=2,NTIME
                     S = S + HALF*(UDASH((IT-1)*DTIME) +
     .                   UDASH(IT*DTIME))*DTIME
                  END DO
               END IF
C-------------------------------------------------------------------------------
C              Integration by trapezoidal rule
C-------------------------------------------------------------------------------
            ELSE
               DTIME = TIMEI/(NTIME-1)
               SINIT = AINIT*(TIMEI**2)/FOUR
               DO IT=2,NTIME
                  SINIT = SINIT + HALF*(UDASH((IT-1)*DTIME) +
     .                    UDASH(IT*DTIME))*DTIME
               END DO
               TERM = TSTART/T0
               S    = S0 * DLOG(DCOSH(TERM)/COSHTI) + SINIT
               ST   = UT * (DTANH(TERM) - TANHTI) + UINIT
               STT  = A0 /(DCOSH(TERM)**2)
            END IF
         ELSE
C-------------------------------------------------------------------------------
C           Linearly interpolate s,u,a from file slikinem.dat data
C-------------------------------------------------------------------------------
            NK = 1
            DO WHILE(TSTART.GE.TSL(NK))
               NK = NK + 1
            END DO
            NK = NK - 1
            NSTART = NK
            IF(NSTART.LT.1) NSTART = 1
            PROP   = (TSTART-TSL(NK))/(TSL(NK+1)-TSL(NK))
            S      = SSL(NK) + (SSL(NK+1) - SSL(NK))*PROP
            ST     = USL(NK) + (USL(NK+1) - USL(NK))*PROP
            STT    = ASL(NK) + (ASL(NK+1) - ASL(NK))*PROP
            WLEGTH = FIVE*BSLIDE
         END IF
C-------------------------------------------------------------------------------
         WRITE (IO6,*) 't =            ',TSTART
         WRITE (IO6,*) 'S =            ',S
         WRITE (IO6,*) 'dS/dt =        ',ST
         WRITE (IO6,*) 'd2S/dt2 =      ',STT
         WRITE (97,*) TSTART,S,ST,STT

C-------------------------------------------------------------------------------
C     ...Domain length and width based on tsunami charact. wavel. or not
C        Not normally for lab measurements of s,u,a
C-------------------------------------------------------------------------------
         IF(ZL0.LT.EPS) THEN
            ZL0 = V0HL*WLEGTH
         END IF
         IF(W0.LT.EPS) THEN
            W0   = HALF*ZL0
            W02  = HALF*W0
         END IF
         W0SL = W0

C-------------------------------------------------------------------------------
C           y-symmetry case
C-------------------------------------------------------------------------------
         IF(IBOT.EQ.6) THEN
            W0SL = TWO*W0
            W02  = HALF*W0SL
         END IF
         DE1 = TTHET*(ZL0 - D0 - ZL2)/(ONE - ZAL)
         DE2 = DE1*ZAL
         H0  = DE1
C-------------------------------------------------------------------------------
         WRITE(IO6,*) 'Landslide physical data :'
         WRITE(IO6,*) 'THETA,BSLIDE,WSLIDE,TSLIDE,DSLIDE,ZL2,',
     .                'RHOL,GAMMA,CM,CD :',
     .              THETA,BSLIDE,WSLIDE,TSLIDE,DSLIDE,ZL2,RHOL,
     .              GAMMA,CM,CD
         WRITE(IO6,*) 'Landslide shape and motion data :'
         WRITE(IO6,*) 'ZEPS,ZMU,ZAL,UT,A0,T0,S0,LAMDA,DE1,DE2 :',
     .              ZEPS,ZMU,ZAL,UT,A0,T0,S0,WLEGTH,DE1,DE2
         WRITE(IO6,*) 'ZL0,W0,H0',ZL0,W0,H0
C-------------------------------------------------------------------------------
C     ...Generation of initial bottom geometry as double sech^2 on SLOPE
C-------------------------------------------------------------------------------
         DELNOD(1) = ZL0/MX
         DELNOD(2) = W0/MY
         DELNOD(3) = H0/MZ
C-------------------------------------------------------------------------------
         IF(ZEPS.GT.ZEPSMA) THEN
            WRITE(IO6,*) 'Decrease ZEPS value'
            STOP
         END IF

C-------------------------------------------------------------------------------
C           Case of quasi 2D or square patch
C-------------------------------------------------------------------------------
         COSHEP = DACOSH(DSQRT(ONE/ZEPS))
         IF(ZMU.NE.ZEPS) THEN
            COSHMU = DACOSH(DSQRT(ONE/ZMU))
C-------------------------------------------------------------------------------
C           x-variation taylored to slide length BSLIDE
C-------------------------------------------------------------------------------
            ZKX    = EIGHT*(DTANH(COSHEP) - ZEPS*COSHEP)/
     .              (PI*BSLIDE*(ONE-ZEPS))
            IF(DABS(WSLIDE).LT.EPS) THEN
C-------------------------------------------------------------------------------
C              y-variation covers full width of tank
C-------------------------------------------------------------------------------
               ZKY = TWO*COSHMU/W0SL
            ELSE
C-------------------------------------------------------------------------------
C              y-variation taylored to slide width WSLIDE
C-------------------------------------------------------------------------------
               ZKY = EIGHT*(DTANH(COSHMU) - ZMU*COSHMU)/
     .              (PI*WSLIDE*(ONE-ZMU))
            END IF
            XI0 = COSHEP/ZKX
            WRITE(IO6,*) 'COSHEP,COSHMU,ZKX,ZKY',COSHEP,COSHMU,ZKX,ZKY

            IF(ZMU.LT.ONE) THEN
               TSLMAX = TSLIDE/((ONE-ZEPS)*(ONE-ZMU))
               ZMUC   = ZMU
            ELSE
C-------------------------------------------------------------------------------
C              Case of no y-variation of landslide shape (2D)
C-------------------------------------------------------------------------------
               TSLMAX = TSLIDE/(ONE-ZEPS)
               ZKY    = ZERO
               ZMUC   = ZERO
            END IF
         ELSE
C-------------------------------------------------------------------------------
C           Case of 3D slide with elliptical footprint (b/2,w/2)
C-------------------------------------------------------------------------------
            SQRTE  = DSQRT(ONE - ZEPS)
            FEPS   = SQRTE*COSHEP/DSQRT(THREE*(SQRTE*COSHEP -
     .               DLOG(ONE/DSQRT(ZEPS)) - HALF*ZEPS*COSHEP**2))
            BPRINT = BSLIDE*FEPS
            RATIO  = BSLIDE/WSLIDE
            RATIO2 = RATIO**2
            WPRINT = BPRINT/RATIO
            XI0    = HALF*BPRINT
            TSLMAX = TSLIDE/(ONE-ZEPS)
            WRITE(IO6,*) 'FEPS,BPRINT,WPRINT,TSLMAX',
     .                    FEPS,BPRINT,WPRINT,TSLMAX
         END IF
C-------------------------------------------------------------------------------
C        x-location of slide axis on slope for t=0
C-------------------------------------------------------------------------------
         XINIT  = (H0 - TSLIDE*CTHET - DSLIDE)/TTHET + X0 + D0
C-------------------------------------------------------------------------------
C        Instantaneous location of slide axis on slope
C        and slide extension along slope for t=TSTART
C-------------------------------------------------------------------------------
         XCENTR = XINIT  - S*CTHET
         XBEGIN = XCENTR - XI0*CTHET
         XEND   = XCENTR + XI0*CTHET

         WRITE(IO6,*) 'XINIT,XCENTR,XI0,XBEGIN,XEND',XINIT,
     .                 XCENTR,XI0,XBEGIN,XEND

         IF(XEND.GT.(X0 + ZL0 - ZL2)) THEN
            WRITE(IO6,*) 'Increase d or ZEPS, or reduce b'
            STOP
         END IF
C-------------------------------------------------------------------------------
C        Limit on slide motion as a function of length of slope
C-------------------------------------------------------------------------------
         IF(IPTYP.NE.11) THEN
            SSLOPE = S + (XCENTR - (X0+D0+XI0*CTHET))/CTHET
            TSLOPE = TIMEI + T0*DACOSH(COSHTI*DEXP((SSLOPE-SINIT)/S0))
            DELTAT = FRACT*T0
C-------------------------------------------------------------------------------
C           s and s_t at t=TSLOPE - DELTAT
C-------------------------------------------------------------------------------
            TERM2  = (TSLOPE - DELTAT)/T0
            SDELTT = S0 * DLOG(DCOSH(TERM2)/COSHTI) + SINIT
            STDELT = UT * (DTANH(TERM2) - TANHTI) + UINIT

            DELTAS = SSLOPE - SDELTT
            ZKS    = STDELT/DELTAS
            WRITE(IO6,*) 'SSLOPE,TSLOPE,DELTAT,SDELTT,DELTAS,ZKS',
     .                    SSLOPE,TSLOPE,DELTAT,SDELTT,DELTAS,ZKS
         END IF
C-------------------------------------------------------------------------------
C        Grid generation
C-------------------------------------------------------------------------------
         DELX = DELNOD(1)
         DELY = DELNOD(2)
         DO I=1,NX
            XB = X0 + DELX*(I-1)
            IF(XB.LE.(X0 + D0)) THEN
C-------------------------------------------------------------------------------
C              Flat ocean bottom in front of slope
C-------------------------------------------------------------------------------
               ZB  = -H0
               DO J=1,NY
                  BOT(I,J) = ZB
               END DO
            ELSE IF(XB.GE.(X0 + ZL0 - ZL2)) THEN
C-------------------------------------------------------------------------------
C              Flat shallow shelf shoreward of slope
C-------------------------------------------------------------------------------
               ZB  = -DE2
               DO J=1,NY
                  BOT(I,J) = ZB
               END DO
            ELSE
               IF((XB.LE.XBEGIN).OR.(XB.GE.XEND)) THEN
C-------------------------------------------------------------------------------
C                 Plane slope in front and behind slide
C-------------------------------------------------------------------------------
                  ZOFX  =  TTHET*(XB - D0 - X0) - H0
                  DO J=1,NY
                     BOT(I,J) = ZOFX
                  END DO
               ELSE
C-------------------------------------------------------------------------------
C                 Double sech^2 slide
C-------------------------------------------------------------------------------
                  XI   = (XB - XCENTR)/CTHET
                  ZOFX = TTHET*(XB - X0 - D0) - H0
                  DO J=1,NY
                     YB     = -W02 + DELY*(J-1)
                     DXI0   = EPS
                     DXIERR = ONE
                     N = 1
                     DO WHILE((DXIERR.GT.EPS).AND.(N.LE.NMAX))
                        IF(ZEPS.NE.ZMU) THEN
C-------------------------------------------------------------------------------
C                          Quasi-2D and square footprint
C-------------------------------------------------------------------------------
                           DXI1 = TSLMAX*(ONE/DCOSH(ZKX*(XI+DXI0))**2
     .                         - ZEPS)*(ONE/DCOSH(ZKY*YB)**2-ZMUC)*TTHET
                        ELSE
C-------------------------------------------------------------------------------
C                          3D with elliptical footprint in (r,phi) axes
C-------------------------------------------------------------------------------
                           RPDR = DSQRT((XI+DXI0)**2 + YB**2)
                           CPHI = (XI+DXI0)/RPDR
                           SPHI = YB/RPDR
                           R0   = XI0/DSQRT(CPHI*CPHI +
     .                                      RATIO2*SPHI*SPHI)
                           IF(RPDR.GE.R0) THEN
C-------------------------------------------------------------------------------
C                             No slide outside of footprint
C-------------------------------------------------------------------------------
                              DXI1 = ZERO
                           ELSE
                              ZKR  = COSHEP/R0
                              DXI1 = TSLMAX*(ONE/DCOSH(ZKR*RPDR)**2 -
     .                               ZEPS)*TTHET
                           END IF
                        END IF
                        DXIERR = DABS((DXI1-DXI0)/DXI0)
                        DXI0   = DXI1
                        N      = N + 1
                     END DO
                     BOT(I,J) = ZOFX + DXI0/STHET
                  END DO
               END IF
            END IF
         END DO
      ELSE IF(IBOT.EQ.10) THEN
C-------------------------------------------------------------------------------
C        Constant depth and image method
C-------------------------------------------------------------------------------
         DO I=1,NX
            DO J=1,NY
               BOT(I,J) = -H0
            END DO
         END DO
      ELSE IF(IBOT.EQ.11) THEN
C-------------------------------------------------------------------------------
C        sloping bottom with double transversal modulation
C-------------------------------------------------------------------------------
         DELX = DELNOD(1)
         DELY = DELNOD(2)
         DO I=1,NX
            XB = X0 + DELX*(I-1)
            IF (XB.LT.(X0 + D0)) THEN
               ZB  = -H0
               DO J=1,NY
                  YB = -HALF*W0 + DELY*(J-1)
                  BOT(I,J) = ZB
                  WRITE(IO6,*) XB,YB,BOT(I,J)
               END DO
            ELSE
               ZU  = SLOPE*(XB - D0 - X0)
               DO J=1,NY
                  YB = -HALF*W0 + DELY*(J-1)
                  YS = 2.0
                  BOT(I,J) = ZU/(1+1/DCOSH(ZK*2*YS)**2)
     .           *(1/(DCOSH(ZK*(YB-YS))**2)+1/(DCOSH(ZK*(YB+YS))**2))-H0
               END DO
            END IF
         END DO
      ELSE IF(IBOT.EQ.12) THEN
C-------------------------------------------------------------------------------
C        barred sloping bottom with a transversal modulation
C-------------------------------------------------------------------------------
         DELX = DELNOD(1)
         DELY = DELNOD(2)
         DO I=1,NX
            XB = X0 + DELX*(I-1)
            IF (XB.LT.(X0 + D0)) THEN
               ZB  = -H0
               DO J=1,NY
                  YB = -HALF*W0 + DELY*(J-1)
                  BOT(I,J) = ZB
                  WRITE(IO6,*) XB,YB,BOT(I,J)
               END DO
            ELSE
               ZK2=2.0
               Xbarre=19.0
               ZU  = SLOPE*(XB - D0 - X0)
               IF (XB.GT.Xbarre-2.5) THEN
                  ZU=ZU*HALF*(1-DTANH(ZK2*(XB-Xbarre)))
               END IF
               DO J=1,NY
                  YB = -HALF*W0 + DELY*(J-1)
                  BOT(I,J) = ZU/(DCOSH(ZK*YB)**2)-H0
               END DO
            END IF
         END DO
      ELSE IF(IBOT.EQ.13) THEN
C-------------------------------------------------------------------------------
C        sloping bottom with an asymetric double transversal modulation
C-------------------------------------------------------------------------------
         DELX = DELNOD(1)
        	DELY = DELNOD(2)
         DO I=1,NX
            XB = X0 + DELX*(I-1)
            IF (XB.LT.(X0 + D0)) THEN
               ZB  = -H0
               DO J=1,NY
                  YB = -HALF*W0 + DELY*(J-1)
                  BOT(I,J) = ZB
                  WRITE(IO6,*) XB,YB,BOT(I,J)
               END DO
            ELSE
               ZU  = SLOPE*(XB - D0 - X0)
               DO J=1,NY
                  YB = -HALF*W0 + DELY*(J-1)
                  YS = 2.0
                  BOT(I,J) = ZU/(1+1/DCOSH(ZK*2*YS)**2)
     .        *(1/(DCOSH(ZK*(YB-YS))**2)+0.75/(DCOSH(ZK*(YB+YS))**2))-H0
               END DO
            END IF
         END DO
      END IF

C-------------------------------------------------------------------------------
      XSTART(1,1) = X0
      XSTART(1,2) =-W02
      XSTART(1,3) = ZERO
      XSTART(2,1) = X0
      XSTART(2,2) = W02
CSYM
      IF((IBOT.EQ.6).OR.(ISYM.EQ.1)) XSTART(2,2) = ZERO
      XSTART(2,3) = BOT(1,1)
      XSTART(3,1) = X0
      XSTART(3,2) =-W02
      XSTART(3,3) = BOT(1,NY)
      XSTART(4,1) = X0 + ZL0
      XSTART(4,2) =-W02
      XSTART(4,3) = BOT(NX,NY)
      XSTART(5,1) = X0 + ZL0
      XSTART(5,2) = W02
      IF(IBOT.EQ.6) XSTART(5,2) = ZERO
      XSTART(5,3) = BOT(NX,1)
      XSTART(6,1) = X0
      XSTART(6,2) = W02
      IF(IBOT.EQ.6) XSTART(6,2) = ZERO
      XSTART(6,3) = BOT(1,1)
CSYM
      IF (ISYM.EQ.1) THEN
         XSTART(5,1)=X0
         XSTART(5,2)=ZERO
         XSTART(5,3)=BOT(1,1)
      END IF
CMYS
C-------------------------------------------------------------------------------
C.....Generation of nodes and BE/SLIDING connectivity matrices
C-------------------------------------------------------------------------------
      MELT = 0
      NELT = 0
      KFP  = 1

C-------------------------------------------------------------------------------
C     Image method skips bottom
C-------------------------------------------------------------------------------
      KFMAX = 6
      IF(IBOT.EQ.10) KFMAX = KFMAX-1

C-------------------------------------------------------------------------------
C     Symetry Image method skips the plan of symetry
C-------------------------------------------------------------------------------
      IF(ISYM.EQ.1) KFMAX = KFMAX-1
      DO KF=1,KFMAX
         NS    = NDST(KF,1)
         NM    = NDST(KF,2)
         IXYZ  = IABS(IDST(KF,1))
         JXYZ  = IABS(IDST(KF,2))
         KXYZ  = IDST(KF,3)
         ISGN  = IDST(KF,1)/IXYZ
         JSGN  = IDST(KF,2)/JXYZ
         MNODE = ICUMUL(KF)
         INODF(KF,1) = MNODE + 1
C-------------------------------------------------------------------------------
C        Node coord. generation
C-------------------------------------------------------------------------------
         DO J=1,NM
            ZINC = DFLOAT(J-1)/DFLOAT(MZ)
            DO I=1,NS
               MNODE = MNODE + 1
               XYZNOD(MNODE,KXYZ) = XSTART(KF,KXYZ)
               XYZNOD(MNODE,IXYZ) = XSTART(KF,IXYZ) + ISGN*DELNOD(IXYZ)*
     .                              (I-1)
               IF(KF.EQ.1) THEN
                  XYZNOD(MNODE,JXYZ) = XSTART(KF,JXYZ) + JSGN*
     .                                                DELNOD(JXYZ)*(J-1)
               ELSE
C-------------------------------------------------------------------------------
C                 Correction for irregular bottom
C-------------------------------------------------------------------------------
	          IF(KF.EQ.2) THEN
                     XYZNOD(MNODE,JXYZ) = BOT(1,I)*(ONE-JSGN*ZINC)
                  ELSE IF(KF.EQ.3) THEN
                     XYZNOD(MNODE,JXYZ) = BOT(I,NY)*(ONE-JSGN*ZINC)
                  ELSE IF(KF.EQ.4) THEN
                     XYZNOD(MNODE,JXYZ) = BOT(NX,NY-I+1)*(ONE-JSGN*ZINC)
CSYM----------------------------------------------------------------------------
                  ELSE IF((KF.EQ.5).AND.(ISYM.NE.1)) THEN
                     XYZNOD(MNODE,JXYZ) = BOT(NX-I+1,1)*(ONE-JSGN*ZINC)
                  ELSE IF(IBOT.NE.10.AND.
     .                    (KF.EQ.6.OR.(KF.EQ.5.AND.ISYM.EQ.1))) THEN
                     XYZNOD(MNODE,KXYZ) = BOT(I,NY-J+1)
                     XYZNOD(MNODE,JXYZ) = XSTART(KF,JXYZ) + JSGN*
     .                                                DELNOD(JXYZ)*(J-1)
CMYS----------------------------------------------------------------------------
                  END IF
               END IF
            END DO
         END DO

C
         INODF(KF,2) = MNODE
         NNODF(KF)   = INODF(KF,2) - INODF(KF,1) + 1
C
C        Element connectivity for both cubic sliding MII and rectangular
C        4-node isoparametric elements. Also for SLIDING.
C        Adjustments at extremities are made.
C
         IELEF(KF,1) = MELT + 1
         DO J=1,NM-1
            JM = -1
            IF(J.EQ.1) THEN
               JM = 0
            ELSE IF(J.EQ.(NM-1)) THEN
               JM = -2
            END IF
CSYMD
C           prise en compte de l'axe de symetrie
C
            IF (ISYM.EQ.1) THEN
               IF ((KF.EQ.1.AND.J.EQ.(NM-1)).OR.
     .             (KF.EQ.5.AND.J.EQ.1)) THEN
                      JM = -1
               END IF
            END IF
CMYSD
C
            DO I=1,NS-1
C
C              Current element data
C
               MELT        = MELT + 1
               IFACE(MELT) =  KF
               INTG(MELT)  =  NINTR
	       ISUB(MELT)  =  0
C
C              Connectivity
C
               IM = -1
               IF(I.EQ.1) THEN
                  IM = 0
               ELSE IF(I.EQ.(NS-1)) THEN
                  IM = -2
               END IF
CSYMD
C              prise en compte de l'axe de symetrie
C
               IF (ISYM.EQ.1) THEN
                  IF ((KF.EQ.4.AND.I.EQ.(NS-1)).OR.
     .                (KF.EQ.2.AND.I.EQ.1)) THEN
                        IM = -1
                  END IF
               END IF
CMYSD
C
C              MII connectivity matrix for elt. MELT
C
               DO JETA = 1,MNOD
                  DO IXI = 1,MNOD
                     IGCON(MELT,IXI,JETA) = ICUMUL(KF) +(J+JM+JETA-2)*NS
     .                                                 +(I+IM+IXI-1)
CSYMD
                      IF (ISYM.EQ.1) THEN
                        IF (KF.EQ.1.AND.
     .                      J.EQ.(NM-1).AND.JETA.EQ.MNOD) THEN
                           IGCON(MELT,IXI,JETA) = ICUMUL(KF)
     .                                    +(J+JM+JETA-4)*NS+(I+IM+IXI-1)
                        END IF
                        IF (KF.EQ.5.AND.
     .                      J.EQ.1.AND.JETA.EQ.1) THEN
                           IGCON(MELT,IXI,JETA) = ICUMUL(KF)
     .                                     +(J+JM+JETA)*NS+(I+IM+IXI-1)
                        END IF
                        IF (KF.EQ.4.AND.
     .                      I.EQ.(NS-1).AND.IXI.EQ.MNOD) THEN
                           IGCON(MELT,IXI,JETA) = ICUMUL(KF)
     .                                   +(J+JM+JETA-2)*NS+(I+IM+IXI-3)
                        END IF
                        IF (KF.EQ.2.AND.
     .                      I.EQ.1.AND.IXI.EQ.1) THEN
                           IGCON(MELT,IXI,JETA) = ICUMUL(KF)
     .                                    +(J+JM+JETA-2)*NS+(I+IM+IXI+1)
                        END IF
                      END IF
CMYSD
                  END DO
               END DO
C
C              Initial chio for xi,eta -> mu transformation
C
               XICON(MELT,1) = -IM*TWTHI - ONE
               XICON(MELT,2) = -JM*TWTHI - ONE
C
C              4-node connectivity matrix for elt. MELT
C
               LCON = 0
               DO JETA = 1,2
                  I1 = 1
                  I2 = 2
                  I3 = 1
                  IF(JETA.EQ.2) THEN
                     I1 = 2
                     I2 = 1
                     I3 = -1
                  END IF
                  DO IXI = I1,I2,I3
                     LCON = LCON + 1
                     ILCON(MELT,LCON) = ICUMUL(KF)+ (J+JETA-2)*NS
     .                                            + (I+IXI-1)
                  END DO
               END DO
            END DO
         END DO
         IELEF(KF,2) = MELT
         NELEF(KF)   = IELEF(KF,2) - IELEF(KF,1) + 1
C
C        SLIDING connectivity matrix for elt. MELT
C
         DO J=1,NM
            JM = -2
            IF(J.EQ.1) THEN
               JM = 0
            ELSE IF(J.EQ.2) THEN
               JM = -1
            ELSE IF(J.EQ.(NM-1)) THEN
               JM = -3
            ELSE IF(J.EQ.NM) THEN
               JM = -4
            END IF
CSYMD
C           prise en compte de l'axe de symetrie
C
            IF (ISYM.EQ.1) THEN
               IF ((KF.EQ.1.AND.J.EQ.(NM-1)).OR.
     .             (KF.EQ.1.AND.J.EQ.NM).OR.
     .             (KF.EQ.5.AND.J.EQ.1).OR.
     .             (KF.EQ.5.AND.J.EQ.2)) THEN
                     JM = -2
               END IF
            END IF
CMYSD
C
            DO I=1,NS
               IM   = -2
               IF(I.EQ.1) THEN
                  IM = 0
               ELSE IF(I.EQ.2) THEN
                  IM = -1
               ELSE IF(I.EQ.(NS-1)) THEN
                  IM = -3
               ELSE IF(I.EQ.NS) THEN
                  IM = -4
               END IF
CSYMD
C              prise en compte de l'axe de symetrie
C
               IF (ISYM.EQ.1) THEN
                  IF ((KF.EQ.4.AND.I.EQ.(NS-1)).OR.
     .                (KF.EQ.4.AND.I.EQ.NS).OR.
     .                (KF.EQ.2.AND.I.EQ.1).OR.
     .                (KF.EQ.2.AND.I.EQ.2)) THEN
                         IM = -2
                  END IF
               END IF
CMYSD
C
C              Coordinate of node in 5x5 grid
C
	       IND = ICUMUL(KF) + (J-1)*NS + I
               IXIET(1,IND) = 1 - IM
               IXIET(2,IND) = 1 - JM
C
               IXE = 0
               DO JETA = 1,NDM
                  DO IXI = 1,NDM
		     IXE = IXE + 1
                     ISLCON(IXE,IND) = ICUMUL(KF) + (J+JM+JETA-2)*NS
     .                                             + (I+IM+IXI -1)
CSYMD
                         IF (ISYM.EQ.1) THEN
                        IF (KF.EQ.1.AND.
     .                      J.EQ.NM.AND.JETA.EQ.NDM) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA-6)*NS + (I+IM+IXI -1)
                        END IF
                        IF (KF.EQ.1.AND.
     .                      J.EQ.NM.AND.JETA.EQ.(NDM-1)) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA-4)*NS + (I+IM+IXI -1)
                        END IF
                        IF (KF.EQ.1.AND.
     .                      J.EQ.(NM-1).AND.JETA.EQ.NDM) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA-4)*NS + (I+IM+IXI -1)
                        END IF
                        IF (KF.EQ.5.AND.
     .                      J.EQ.1.AND.JETA.EQ.1) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA+2)*NS + (I+IM+IXI -1)
                        END IF
                        IF (KF.EQ.5.AND.
     .                      J.EQ.1.AND.JETA.EQ.2) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA)*NS + (I+IM+IXI -1)
                        END IF
                        IF (KF.EQ.5.AND.
     .                      J.EQ.2.AND.JETA.EQ.1) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA)*NS + (I+IM+IXI -1)
                        END IF
                        IF (KF.EQ.4.AND.
     .                      I.EQ.NS.AND.IXI.EQ.NDM) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA-2)*NS + (I+IM+IXI -5)
                        END IF
                        IF (KF.EQ.4.AND.
     .                      I.EQ.NS.AND.IXI.EQ.(NDM-1)) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA-2)*NS + (I+IM+IXI -3)
                        END IF
                        IF (KF.EQ.4.AND.
     .                      I.EQ.(NS-1).AND.IXI.EQ.NDM) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA-2)*NS + (I+IM+IXI -3)
                        END IF
                        IF (KF.EQ.2.AND.
     .                      I.EQ.1.AND.IXI.EQ.1) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA-2)*NS + (I+IM+IXI +3)
                        END IF
                        IF (KF.EQ.2.AND.
     .                      I.EQ.1.AND.IXI.EQ.2) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA-2)*NS + (I+IM+IXI +1)
                        END IF
                        IF (KF.EQ.2.AND.
     .                      I.EQ.2.AND.IXI.EQ.1) THEN
                           ISLCON(IXE,IND) = ICUMUL(KF)
     .                            + (J+JM+JETA-2)*NS + (I+IM+IXI +1)
                        END IF
                     END IF
CMYSD
                  END DO
               END DO
            END DO
         END DO
      END DO
C
      NOM = MNODE
      MOM = MELT
C
C.....Find double/triple nodes ( bottom has been skipped from node generation
C     with image method)
C
      IDN2 = 0
      IDN3 = 0
C
      DO IB=1,NOM
         XI         = XYZNOD(IB,1)
         YI         = XYZNOD(IB,2)
         ZI         = XYZNOD(IB,3)
         LXYZ(IB,1) = 0
         LXYZ(IB,2) = 0
C
         JDN = 0
         JB  = 1
         DO WHILE(JB.LE.NOM)
            IF((DABS(XI-XYZNOD(JB,1)).LE.EPS).AND.
     .         (DABS(YI-XYZNOD(JB,2)).LE.EPS).AND.
     .         (DABS(ZI-XYZNOD(JB,3)).LE.EPS).AND.(JB.NE.IB) ) THEN
               JDN = JDN + 1
	       IF((JDN.EQ.1).OR.(JDN.EQ.2)) THEN
                  LXYZ(IB,JDN) = JB
	       END IF
            END IF
	   JB = JB + 1
         END DO
         IF(JDN.EQ.1) IDN2 = IDN2 + 1
         IF(JDN.EQ.2) IDN3 = IDN3 + 1
      END DO
      NDBNOD = IDN2/2
      NTPNOD = IDN3/3
C
C.....Initialize AP on k boundaries
C
      DO K=2,5
         IF(IBCOND(K).EQ.3.AND.ISTART.EQ.0) THEN
	    DO J=1,MY+1
	       UPNEW(K-1,J) = ZERO
	       DUPDT(K-1,J) = ZERO
	    END DO
	 END IF
      END DO
C
C.....Mark corner elements for adaptive integration
C
C      DO IE=1,MOM
C         DO J=1,MNOD
C            L = ILCON(IE,J)
C            IF(LXYZ(L,1).NE.0) THEN
C               ISUB(IE) = NSUBC
C               IF(IPTYP.GE.9) THEN
C
C                 Landslide case
C
C                  ISUB(IE) = NSUBC + 1
C               END IF
C            END IF
C         END DO
C      END DO
C
C     Mark read elements for adaptive integration
C
      OPEN(UNIT=IO8,STATUS='UNKNOWN',FILE='input/subdiv.dat2')
      READ(IO8,*) ISUBDI
      IF (ISUBDI.GT.0) THEN
         DO I=1,ISUBDI
            READ(IO8,*)X1,X2
            READ(IO8,*)Y1,Y2
            READ(IO8,*)Z1,Z2
            WRITE(IO6,*)X1,X2
            WRITE(IO6,*)Y1,Y2
            WRITE(IO6,*)Z1,Z2
            DO IE=1,MOM
               DO J=1,MNOD
               L = ILCON(IE,J)
               IF(XYZNOD(L,1).GT.X1.AND.XYZNOD(L,1).LT.X2.AND.
     .            XYZNOD(L,2).GT.Y1.AND.XYZNOD(L,2).LT.Y2.AND.
     .            XYZNOD(L,3).GT.Z1.AND.XYZNOD(L,3).LT.Z2) THEN
                  ISUB(IE) = NSUBC
                  WRITE(IO6,*) 'Subdivided element : ',IE
               END IF
               END DO
            END DO
         END DO
      END IF
      CLOSE(IO8)
C
C......Read x,y,z,phi,phin from earlier results and regrid initial
C      data if IGRID=1
C
      IF(ISTART.EQ.1) THEN
         open(22,file='output/restart2.dat')
C
C        Old values of surface characteristics. If different from new
C        ones, regridding will occur
C
 1666	    format(3(i7,1x),2(E24.16,1x),i7)    
         READ(22,1666) NOMP,NXP,NYP,TSTART,DT,ILOOP0
	 write(*,*) 'Restart, NOMP,NXP,NYP,TSTART,DT,ILOOP'
         write(*,*) NOMP,NXP,NYP,TSTART,DT,ILOOP

         write(*,*)'ISTART detected'
         write(*,*)'TSTART',TSTART

         WRITE(IO6,*) 'Old NOMP,NXP,NYP : ',NOMP,NXP,NYP
         WRITE(IO6,*) 'Initial : i,x(i),y(i),z(i),phi(i),phin(i)'
C
2001   format(i7,1x,22(E24.16,1x))
         DO I=1,NOMP
c            READ(22,*) IVOID,XYZNDR(I,1),XYZNDR(I,2),XYZNDR(I,3),
c     .                  PHIR(I),PHINR(I)
c            WRITE(IO6,2000) I,XYZNDR(I,1),XYZNDR(I,2),XYZNDR(I,3),
c     .                      PHIR(I),PHINR(I)
            READ(22,2001) IVOID,XYZNDR(I,1),XYZNDR(I,2),XYZNDR(I,3),
     &                  PHIR(I),PHINR(I),phitr(i),phitnr(i),
     &			  phisr(i),phimr(i),phissr(i),phimmr(i),
     &			  phinsr(i),phinmr(i),phitsr(i),phitmr(i),
     &			  phismr(i),phinnr(i),
     &			  presr(i),dprdtr(i),uvwr(1,i),uvwr(2,i),
     &			  uvwr(3,i),
     &			  duvwdtr(1,i),duvwdtr(2,i),duvwdtr(3,i)
            WRITE(IO6,2000) I,XYZNDR(I,1),XYZNDR(I,2),XYZNDR(I,3),
     &                      PHIR(I),PHINR(I)
         END DO
C
	 IF(IGRID.EQ.0) THEN
C
C           No regridding, same mesh
C
c-----------------------------------------------
c	old version
c----------------------------------------------
c            DO I=1,NOM
c               DO K=1,3
c	          XYZNOD(I,K) = XYZNDR(I,K)
c	       END DO
c               PHI(I)  = PHIR(I)
c               PHIN(I) = PHINR(I)
c            END DO
c-----------------------------------------------
c	new version, XB
c----------------------------------------------
            DO I=1,NOM
               DO K=1,3
	          XYZNOD(I,K) = XYZNDR(I,K)
		  uvw(i,k)=uvwr(i,k)
		  duvwdt(i,k)=duvwdtr(i,k)
	       END DO
               PHI(I)  = PHIR(I)
               PHIN(I) = PHINR(I)
               phit(i) = phitr(i)
	       phitn(i)= phitnr(i)
	       phis(i) = phisr(i)
	       phim(i) = phimr(i)
	       phiss(i)= phissr(i)
	       phimm(i)= phimmr(i)
	       phins(i)= phinsr(i)
	       phinm(i)= phinmr(i)
	       phits(i)= phitsr(i)
	       phitm(i)= phitmr(i)
	       phism(i)= phismr(i)
	       phinn(i)= phinnr(i)
	       pres(i) = presr(i)
	       dprdt(i)= dprdtr(i)
	   END DO
	 ELSE
C
CSYM        fix near zero y-values to zero to get all points on only one
C           side from the axis (otherwise regridding is not possible)
C
            IF (ISYM.EQ.1) THEN
               DO I=1,NOMP
                  IF (ABS(XYZNDR(I,2)).LT.EPS) THEN
                     XYZNDR(I,2)=ZERO
                  END IF
               END DO
            END IF
CMYS
C
C           Regridding, new mesh
C
c	old version
c            CALL REGRID(PHI,PHIN,IGRID,XMIN,DXMIN,DYMIN)
c	new version, XB
            CALL REGRID(IGRID,XMIN,DXMIN,DYMIN)
C           -----------
	 END IF
C
C        Initialization of AP boundaries
C
CSYM
         KFMAX=5
         IF(ISYM.EQ.1) KFMAX=KFMAX-1      
	 DO K=2,KFMAX
	    IF(IBCOND(K).EQ.3) THEN
	       IF(K.EQ.2.OR.K.EQ.4) NK = MY+1
	       IF(K.EQ.3.OR.K.EQ.5) NK = MX+1
	       DO J=1,NK
	          READ(22,*) KF,UPNEW(KF-1,J),DUPDT(KF-1,J)
	       END DO
	    END IF
	 END DO
      END IF
C
C.....Generate internal points ! CHANGE for reading specified pts, gages
C
      IF(IFIELD) THEN
         IF(NXI.NE.0.AND.NYI.NE.0) THEN
C
C           Generate constant step grid of internal pts
C
	    IF(NZI.GT.0) THEN
C              Interpolate z from z_bot to z_max
	       INTZED = 0
	    ELSE IF(NZI.LT.0) THEN
C              Interpolate z from z_bot to z_free
	       INTZED = 1
	    END IF
	    NZI = IABS(NZI)
C
	    DELNOD(1) = ZL0/(NXI)
            DELNOD(2) = W0/(NYI)
C
            JOI = 0
	    IG  = 0
            DO J=1,NYI
               YINT = -W02 + (DFLOAT(J) - HALF)*
     .                DELNOD(2)
               DO I=1,NXI
                  XINT = X0 + (DFLOAT(I) - HALF)*DELNOD(1)
C
C                 Gages (x,y) coordinates
C
                  IF(NGA.EQ.0) THEN
                     IG = IG + 1
	             XYZGAG(IG,1) = XINT
		     XYZGAG(IG,2) = YINT
	             XYZBOT(IG,1) = XINT
		     XYZBOT(IG,2) = YINT
		  END IF
C
C                 Case IBOT=4
C
CSYM
                  IF(ISYM.NE.1.AND.IBOT.EQ.4) THEN
	           IF(XINT.LT.(X0 + D0)) THEN
	             ZB  = -H0
 	           ELSE
	             ZU  = SLOPE*(XINT - D0 - X0)
                     ZB = ZU/(DCOSH(ZK*YINT)**2) - H0
	           END IF
                  ELSE
                     ZB  = -H0
                  END IF
C
		  IF (INTZED.EQ.0) THEN
                     DELNOD(3) = (ZMAX - ZB)/DFLOAT(NZI)
                     DO K=1,NZI
                        JOI = JOI + 1
		        FIELD(1,JOI) = XINT
		        FIELD(2,JOI) = YINT
                        FIELD(3,JOI) = ZB + (DFLOAT(K) - HALF)*DELNOD(3)
                        IREP(JOI) = 1
                     END DO
		  ELSE IF(INTZED.EQ.1) THEN
C                    Pending known values for free surface, the z value is z=0
                     DELNOD(3) = ZERO
                     DO K=1,NZI
                        JOI = JOI + 1
		        FIELD(1,JOI) = XINT
		        FIELD(2,JOI) = YINT
                        FIELD(3,JOI) = ZERO
                     END DO
		  END IF
               END DO
            END DO
            NOI = JOI
	    NGA = IG
            WRITE(*,*) ' NXI,NYI,NZI,NOI,NGA : ',NXI,NYI,NZI,NOI,NGA
            WRITE(*,*) 'internal pts generated :',NOI
            WRITE(*,*) 'surf./bot.  gages generated :',NGA,INTZED
            open(11,file='initgrid.dat')
            do JOI=1,NOI
            write(11,*)(FIELD(J,JOI),J=1,3)
            enddo
            close(11)
	 ELSEIF(NXI.EQ.0.AND.NYI.EQ.0.AND.NZI.NE.0) THEN
C
C           interior points only between gages: done in postpr
C
	    DO IG=1,NGA
	       JOI1 = (IG-1)*NZI + 1
	       JOI2 = JOI1 + NZI - 1
	       DO JOI=JOI1,JOI2
                  FIELD(1,JOI) = XYZGAG(IG,1)
                  FIELD(2,JOI) = XYZGAG(IG,2)
	       END DO
	    END DO
	    INTZED=1
C              Interpolate z from z_bot to z_free
	 ELSE
C
C           Read internal pts from file 'intern.dat'
C
            OPEN(UNIT=IO63,STATUS='OLD',FILE='intern.dat2')
	    DO JOI=1,NOI
               READ(IO63,*,END=100) (FIELD(J,JOI),J=1,3)
	    END DO
  100       WRITE(IO6,*) 'internal pts from file intern.dat2 :',NOI
	    DO JOI=1,NOI
               WRITE(IO6,*) (FIELD(J,JOI),J=1,3)
	    END DO
            WRITE(*,*) 'internal pts from file intern.dat2 :',NOI
            CLOSE(IO63)
         END IF
Cfait dans postprocess.f
C	 DO JOI=1,NOI
C            WRITE(IO60,2005) (FIELD(J,JOI),J=1,3)
C	 END DO
      END IF
C
C.....Print data
C
C      WRITE(IO16,*)'Bottom :'
C        DO I=1,NX
C	    XB = X0 + DELNOD(1)*(I-1)
C            DO J=1,NY
C	          YB = -W02 + DELNOD(2)*(J-1)
C               WRITE(IO16,2040) XB,YB,BOT(I,J)
C            END DO
C        END DO


c--------------------------------------------
c	stupid line !!!
c	I'll comment, XB
c      IF (0.EQ.0) THEN
c--------------------------------------------

         WRITE(IO6,*) 'Grid::Grid node coordinates in,x(in),y(in),
     .z(in) :'
         DO I=1,NOM
            WRITE(IO6,2000) I,(XYZNOD(I,J),J=1,3)
         END DO
         WRITE(IO6,*) 'Grid::Grid 4-node element data ie,x(ie),y(ie),
     .z(ie) :'
         DO I=1,MOM
            WRITE(IO6,2010) I,IFACE(I),INTG(I),(ILCON(I,J),J=1,MNOD),
     .                 XICON(I,1),XICON(I,2)
         END DO
         WRITE(IO6,'(A)') 'Grid::'
         DO K=1,MOM
            WRITE(IO6,2020) K,IFACE(K),((IGCON(K,I,J),I=1,MNOD),
     .            J=1,MNOD)
         END DO
         WRITE(IO6,'(A)') 'Grid::'
         DO I=1,NOM
            WRITE(IO6,2030) I,(ISLCON(J,I),J=1,NDM2),IXIET(1,I),
     .            IXIET(2,I)
         END DO
CSYM...
         KFMAX=6
         IF (ISYM.EQ.1) THEN
            KFMAX=KFMAX-1
         END IF
         IF (IBOT.EQ.10) THEN
            KFMAX=KFMAX-1
         END IF
         WRITE(IO6,'(A)') 'Grid::Boundary limits'
         DO KF=1,KFMAX
            WRITE(IO6,2020) KF,INODF(KF,1),INODF(KF,2),NNODF(KF),
     .                    IELEF(KF,1),IELEF(KF,2),NELEF(KF)
         END DO
      
C      Write double and triple nodes
         WRITE(IO6,'(A)') 'Grid::Double nodes'
         DO I=1,NOM
            IF((LXYZ(I,1).NE.0).AND.(LXYZ(I,2).EQ.0)) THEN
               WRITE(IO6,2020) I,LXYZ(I,1),LXYZ(I,2)
            END IF
         END DO
         WRITE(IO6,'(A)') 'Grid::Triple nodes'
         DO I=1,NOM
            IF((LXYZ(I,1).NE.0).AND.(LXYZ(I,2).NE.0)) THEN
               WRITE(IO6,2020) I,LXYZ(I,1),LXYZ(I,2)
            END IF
         END DO
         WRITE(IO6,'(A)') 'Grid::Subdivided nodes'
         DO IE=1,MOM
            IF(ISUB(IE).NE.0) THEN
               WRITE(IO6,2020) IE,ISUB(IE)
            END IF
         END DO
c--------------------------------------------
c	end of the stupid if !!!
c	I'll comment, XB
c      END IF
c--------------------------------------------

C-------------------------------------------------------------------------------
      WRITE(*,*)   ' === Grid generated === '
      WRITE(IO6,*) ' === Grid generated === '
C-------------------------------------------------------------------------------
      RETURN
C-------------------------------------------------------------------------------
 2000 FORMAT(I5,5F10.3)
 2005 FORMAT(4E16.8)
 2010 FORMAT(7I5,2F10.5)
 2020 FORMAT(18I5)
 2030 FORMAT(30I5)
 2040 FORMAT(I5,3E16.8)
C-------------------------------------------------------------------------------
      END




C-----------------------------------------------------------------------
      SUBROUTINE ADAPTX(X1,X2,DX,MX,IGRID,XMIN,DXMIN)
C-----------------------------------------------------------------------
C
C     defines a vector DX of MX spatial steps between X1 and X2
C
C     XMIN is the location of the minimum spatial step DXMIN
C     IGRID is used as a flag for different irregular regridding techniques
C
C     CF2005
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
C
      DIMENSION DX(MX),X(MX)
C
      IF (IGRID.EQ.2.OR.IGRID.EQ.3) THEN
	 RMX=REAL(MX)
	 B=(8.0*(XMIN-X1)-RMX**3*DXMIN)/(4.0-RMX**2)
	 AM=RMX**3*DXMIN-RMX**2*B
	 AP=8.0*(X2-XMIN)-4.0*B
C
C	 WRITE(*,*)'subroutine adaptx',RMX,B,AM,AP
C
         X(1)=XMIN+AM*(1.0/RMX-0.5)**3
     .       +B*(1.0/RMX-0.5)
         DX(1)=X(1)-X1
         DO I=2,MX/2
	    X(I)=XMIN+AM*(REAL(I)/RMX-0.5)**3
     .       +B*(REAL(I)/RMX-0.5)
	    DX(I)=X(I)-X(I-1)
C
C	 WRITE(*,*)'subroutine adaptx', I,X(I),DX(I)
C
	 END DO
	 MX2=MX/2+1
         DO I=MX2,MX
	    X(I)=XMIN+AP*(REAL(I)/RMX-0.5)**3
     .       +B*(REAL(I)/RMX-0.5)
	    DX(I)=X(I)-X(I-1)
C
C	 WRITE(*,*)'subroutine adaptx', I,X(I),DX(I)
C
	 END DO
      ELSE
         DO I=1,MX
	    DX(I)=(X2-X1)/REAL(MX)
	 END DO
      END IF
      RETURN
      END
C
C-----------------------------------------------------------------------
      SUBROUTINE ADAPTY(Y,X,J,X1,X2,Y1,Y2,IGRID,XMIN,DYMIN)
C-----------------------------------------------------------------------
C
C     defines a value Y for X
C
C     (XMIN,0) is the location of the minimum spatial step DYMIN
C     
C     IGRID is used as a flag for different irregular regridding techniques
C
C     CF2005
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
C
      COMMON/DATGEN/  ZL0,W0,H0,X0,D0,MX,MY,MZ,NX,NY,NZ,NSUBC,
     .                NINTR,NOM,IDUM2
C
      IF (IGRID.EQ.3) THEN
	 RMY=REAL(MY)
         C=4.0*(-W0+RMY*DYMIN)/(RMY**2-2.0*RMY)
	 IF(J.LE.MY/2) THEN
            RJ=REAL(J)
         ELSE
            RJ=REAL(ABS(MY+2-J))
         END IF
         YMIN=(RMY/2.0-RJ+1)*(C*RMY/2.0-DYMIN)
     .         -C*(RMY/2.0-RJ+1)*(RMY/2.0-RJ+2)/2.0
     .         -C*(RJ-1)*(RMY/2.0-RJ+1)
         IF(J.LE.MY/2) THEN
	    B=YMIN
         ELSE
	    B=-YMIN
         END IF
	 AM=(Y1-B)/(X1-XMIN)**2
	 AP=(Y2-B)/(X2-XMIN)**2
         IF(X.LE.XMIN) THEN
            Y=AM*(X-XMIN)**2+B
         ELSE
            Y=AP*(X-XMIN)**2+B
         END IF
C         IF (J.GT.MY/2) THEN
C            Y=-Y
C         END IF
      ELSE
	 Y=Y1+(X-X1)*(Y2-Y1)/(X2-X1)
      END IF
      RETURN
      END
C
C-----------------------------------------------------------------------
c      SUBROUTINE REGRID(PHI,PHIN,IGRID,XMIN,DXMIN,DYMIN)

c	New Version, XB
      SUBROUTINE REGRID(IGRID,XMIN,DXMIN,DYMIN)
C-----------------------------------------------------------------------
C
C     Regrid existing results from previous computations into a new mesh
C     by interpolating in a MII grid. (xi,et) are found using Newton-
C     Raphson iterations. Each regridded point is marked by 1 in IREG.
C
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      LOGICAL*4 INIE
C
      COMMON/DATGEN/  ZL0,W0,H0,X0,D0,MX,MY,MZ,NX,NY,NZ,NSUBC,
     .                NINTR,NOM,IDUM2
c      COMMON /SYSTEM/ DUM2(31+8*MYMAX),IO6,IFLAG,IDUM4(3)
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /MAILLE/ XYZNOD(NOMM,3),DUM3(MOMM*2+18),
     .                IDUM5(MOMM*MNOD*MNOD+MOMM*7),
     .                LXYZ(NOMM,2),IDUM6(66),INODF(6,2),IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,IDUM8(3)
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),DUM4(13)
c----------------------------------------------------------------
c	old lines
c
c      COMMON /REGRD/  XYZNDR(NOMM,3),PHIR(NOMM),PHINR(NOMM),
c     .                XICONR(MOMM,2),IGCONR(NOMM,MNOD,MNOD),
c     .                ILCONR(MOMM,4),IREG(NOMM),NOMP,NXP,NYP
C
c      DIMENSION PHI(NOM),PHIN(NOM),FSFXI(NDM),FSDXI(NDM),
c----------------------------------------------------------------
c	New lines, XB
      DIMENSION FSFXI(NDM),FSDXI(NDM),
     &          FSFET(NDM),FSDET(NDM),ZMAT(2,2),ZP(2),DX(MX)

c----------------------------------------------------------------
c	New lines, XB
      include 'regrid.h' 	
      include 'result.h'
c----------------------------------------------------------------
C
      DATA ONE/1.D0/,TWTHI/0.6666666666666667D0/,ZERO/0.D0/,ITEMAX/20/,
     .     BIG/1.D10/,EPS/1.D-06/,OTHREE/0.3333333333333333D0/
C
      SAVE
C     SAVE /DATGEN/,/SYSTEM/,MAILLE/,/DELEM1/,/DELEM2/,/FUNCTN/,/REGRID/
C
C.....New grid on the free surface whenever moving boundaries are used (particular implementation as a test)
C
      IF (IBCOND(2).NE.0.OR.IBCOND(4).NE.0.OR.
     .    IBCOND(3).NE.0.OR.IBCOND(5).NE.0) THEN
         DO I=1,MX
	    DX(I)=ZERO
	 END DO
	 DO J=1,NY
           IND1=1+(J-1)*NX
           IND1P=1+(J-1)*NXP
	   X1=XYZNDR(IND1P,1)
	   Y1=XYZNDR(IND1P,2)
           IND2=IND1+MX
           IND2P=IND1P+NXP-1
	   X2=XYZNDR(IND2P,1)
	   Y2=XYZNDR(IND2P,2)
	   XYZNOD(IND1,1)=X1
	   XYZNOD(IND1,2)=Y1
	   XYZNOD(IND2,1)=X2
	   XYZNOD(IND2,2)=Y2
           DO I=2,MX
	     IND=IND1+I-1
	     CALL ADAPTX(X1,X2,DX,MX,IGRID,XMIN,DXMIN)
C            -----------
	     XYZNOD(IND,1)=XYZNOD(IND-1,1)+DX(I-1)
             CALL ADAPTY(Y,XYZNOD(IND,1),J,X1,X2,Y1,Y2,IGRID,XMIN,DYMIN)
C            ----------- 
	     XYZNOD(IND,2)=Y
	   END DO
         END DO
         DO KF=2,5
	   IF (IBCOND(KF).EQ.2) THEN
	   IB=INODF(KF,1)
           DO J=1,NY
	     NB=IB+J-1
	     NS=NB+MZ*(MY+1)
             DO K=1,NZ
	       IND = IB + (K-1)*(MY+1) + J - 1
	       DO KK=1,3
                  XYZNOD(IND,KK) = XYZNOD(NB,KK) + (K-1)*
     .                (XYZNOD(LXYZ(NS,1),KK) - XYZNOD(NB,KK))/MZ
               END DO
	     END DO
           END DO
	   END IF
	 END DO
      END IF
C
C.....Initialization
C
      DO INEW=INODF(1,1),INODF(1,2)
         IREG(INEW) = 0
      END DO
C
C.....Element connectivity for both cubic sliding MII and rectangular
C     4-node isoparametric elements for old surface nodes.
C     Adjustments at surface extremities are made.
C
      MELT  = 0
      NNODE = MNOD
C
      DO J=1,NYP-1
         JM = -1
         IF(J.EQ.1) THEN
            JM = 0
         ELSE IF(J.EQ.(NYP-1)) THEN
            JM = -2
	 END IF
C
         DO I=1,NXP-1
C
C           Current element Connectivity
C
            IM = -1
            IF(I.EQ.1) THEN
               IM = 0
            ELSE IF(I.EQ.(NXP-1)) THEN
               IM = -2
            END IF
	    MELT = MELT + 1
C
C           MII connectivity matrix for elt. MELT
C
            DO JETA = 1,MNOD
               DO IXI = 1,MNOD
                  IGCONR(MELT,IXI,JETA) = (J + JM + JETA - 2)*NXP
     .                                  + (I + IM + IXI - 1)
               END DO
            END DO
C
C           Initial chio for xi,eta -> mu transformation
C
            XICONR(MELT,1) = -IM*TWTHI - ONE
            XICONR(MELT,2) = -JM*TWTHI - ONE
C
C           4-node connectivity matrix for elt. MELT
C
            LCON = 0
            DO JETA = 1,2
               I1 = 1
               I2 = 2
               I3 = 1
               IF(JETA.EQ.2) THEN
                  I1 = 2
                  I2 = 1
                  I3 = -1
               END IF
	       DO IXI = I1,I2,I3
                  LCON = LCON + 1
                  ILCONR(MELT,LCON) = (J + JETA - 2)*NXP
     .                              + (I + IXI - 1)
               END DO
            END DO
         END DO
      END DO
C
      MOM1  = MELT
C
      WRITE(IO6,*) 'Connectivities for old nodes : '
      DO I=1,MOM1
         WRITE(IO6,2010) I,(ILCONR(I,J),J=1,NNODE),XICONR(I,1),
     .                                             XICONR(I,2)
      END DO
      DO K=1,MOM1
         WRITE(IO6,2020) K,((IGCONR(K,I,J),I=1,NNODE),J=1,NNODE)
      END DO
C
C.....Find which element contains new nodes and regrid z,phi,phin
C
      NOMSA = NOM
      NOM   = 2
      IFLAG = 1
C
      DO IE=1,MOM1
C
C        4-node and MII local connectivity
C
         ILOC = 0
C
         DO J=1,NNODE
            NODE2(J)    = ILCONR(IE,J)
            XYZLO2(J,1) = XYZNDR(NODE2(J),1)
            XYZLO2(J,2) = XYZNDR(NODE2(J),2)
C
            DO I=1,NNODE
               ILOC = ILOC + 1
               NODE1(ILOC)    = IGCONR(IE,I,J)
               XYZLO1(ILOC,1) = XYZNDR(NODE1(ILOC),1)
               XYZLO1(ILOC,2) = XYZNDR(NODE1(ILOC),2)
               XYZLO1(ILOC,3) = XYZNDR(NODE1(ILOC),3)
            END DO
         END DO
C
         EFJXI = XICONR(IE,1)
         EFJET = XICONR(IE,2)
C
         DO INEW=INODF(1,1),INODF(1,2)
            XN = XYZNOD(INEW,1)
            YN = XYZNOD(INEW,2)
C
C           Check if xn,yn is within element IE (4-nodes) (x,y)_j
C
            INIE = .FALSE.
	    IF(IREG(INEW).EQ.0) THEN
               CALL POLYIN2(INIE,XN,YN)
C              -----------
            END IF
	    IF(INIE) THEN
              WRITE(IO6,*) IE,INEW,XN,YN
C
C              Regrid xn,yn with MII element IE, ie find z,phi,phin
C              using Newton-Raphson method
C
C              Estimate of xio,etao by bilinear interpolation
C
               XI0   = ZERO
	       ET0   = ZERO
	       ZP(1) = BIG
	       ZP(2) = BIG
	       ITER  = 0
C
	       DO WHILE((DABS(ZP(1)).GT.EPS.OR.DABS(ZP(2)).GT.EPS).
     .                  AND.ITER.LE.ITEMAX)
C
C                 One dimensional 4-node shape functions
C
                  CHI = (XI0 + ONE)*OTHREE + EFJXI
                  CALL SHAPFN(CHI,FSFXI,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDXI,NNODE)
C                 -----------
                  CHI = (ET0 + ONE)*OTHREE + EFJET
                  CALL SHAPFN(CHI,FSFET,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDET,NNODE)
C                 -----------
C
C                 Build N-R system with bidim. shape functions
C
                  ZMAT(1,1) = ZERO
		  ZMAT(1,2) = ZERO
		  ZMAT(2,1) = ZERO
		  ZMAT(2,2) = ZERO
		  ZP(1)     = -XN
		  ZP(2)     = -YN
                  ILOC = 0
C
                  DO J=1,NNODE
                     DO I=1,NNODE
                        ILOC = ILOC + 1
C
                        FI(ILOC)   = FSFXI(I)*FSFET(J)
                        DFIX(ILOC) = FSDXI(I)*FSFET(J)*OTHREE
                        DFIE(ILOC) = FSFXI(I)*FSDET(J)*OTHREE
C
			ZMAT(1,1)  = ZMAT(1,1)-DFIX(ILOC)*XYZLO1(ILOC,1)
			ZMAT(1,2)  = ZMAT(1,2)-DFIE(ILOC)*XYZLO1(ILOC,1)
			ZMAT(2,1)  = ZMAT(2,1)-DFIX(ILOC)*XYZLO1(ILOC,2)
			ZMAT(2,2)  = ZMAT(2,2)-DFIE(ILOC)*XYZLO1(ILOC,2)
			ZP(1)      = ZP(1) + FI(ILOC)*XYZLO1(ILOC,1)
			ZP(2)      = ZP(2) + FI(ILOC)*XYZLO1(ILOC,2)
                     END DO
                  END DO
C
C                 Solve N-R system
C
                  CALL SOLVE(ZMAT,ZP,2)
C                 ----------
		  ITER = ITER + 1
		  XI0  = XI0 + ZP(1)
		  ET0  = ET0 + ZP(2)
                  WRITE(IO6,*) ITER,XI0,ET0
               END DO
C
C              Calculate regridded point parameters for converged xi,et
C
               CHI = (XI0 + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSFXI,NNODE)
C              -----------
               CHI = (ET0 + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSFET,NNODE)
C              -----------
               PHI(INEW)      = ZERO
               PHIN(INEW)     = ZERO
c----------------------------------------
c	New lines, XB
c----------------------------------------
               phit (INEW) = ZERO
	       phitn(INEW) = ZERO
	       phis (INEW) = ZERO
	       phim (INEW) = ZERO
	       phiss(INEW) = ZERO
	       phimm(INEW) = ZERO
	       phins(INEW) = ZERO
	       phinm(INEW) = ZERO
	       phits(INEW) = ZERO
	       phitm(INEW) = ZERO
	       phism(INEW) = ZERO
	       phinn(INEW) = ZERO
	       pres (INEW) = ZERO
	       dprdt(INEW) = ZERO
c----------------------------------------
	       XYZNOD(INEW,3) = ZERO
               ILOC           = 0
C
               DO J=1,NNODE
                  DO I=1,NNODE
                     ILOC           = ILOC + 1
                     FI(ILOC)       = FSFXI(I)*FSFET(J)
                     PHI(INEW)      = PHI(INEW) +
     .                                FI(ILOC)*PHIR(NODE1(ILOC))
                     PHIN(INEW)     = PHIN(INEW) +
     .                                FI(ILOC)*PHINR(NODE1(ILOC))
                     XYZNOD(INEW,3) = XYZNOD(INEW,3) +
     .                                FI(ILOC)*XYZLO1(ILOC,3)
c----------------------------------------
c	             New lines, XB
c----------------------------------------
                     phit (INEW) = phit (INEW)+
     &                                FI(ILOC)*phitr(NODE1(ILOC))
	             phitn(INEW) = phitn(INEW)+
     &                                FI(ILOC)*phitnr(NODE1(ILOC))		     
	             phis (INEW) = phis (INEW)+
     &                                FI(ILOC)*phisr(NODE1(ILOC))		     
	             phim (INEW) = phim (INEW)+
     &                                FI(ILOC)*phimr(NODE1(ILOC))		     
	             phiss(INEW) = phiss(INEW)+
     &                                FI(ILOC)*phissr(NODE1(ILOC))		     
	             phimm(INEW) = phimm(INEW)+
     &                                FI(ILOC)*phimmr(NODE1(ILOC))		     
	             phins(INEW) = phins(INEW)+
     &                                FI(ILOC)*phinsr(NODE1(ILOC))		     
	             phinm(INEW) = phinm(INEW)+
     &                                FI(ILOC)*phinmr(NODE1(ILOC))		    
	             phits(INEW) = phits(INEW)+
     &                                FI(ILOC)*phitsr(NODE1(ILOC))		     
	             phitm(INEW) = phitm(INEW)+
     &                                FI(ILOC)*phitmr(NODE1(ILOC))		     
	             phism(INEW) = phism(INEW)+
     &                                FI(ILOC)*phismr(NODE1(ILOC))		     
	             phinn(INEW) = phinn(INEW)+
     &                                FI(ILOC)*phinnr(NODE1(ILOC))		     
	             pres (INEW) = pres (INEW)+
     &                                FI(ILOC)*presr(NODE1(ILOC))		     
	             dprdt(INEW) = dprdt(INEW)+
     &                                FI(ILOC)*dprdtr(NODE1(ILOC))
	             uvw(INEW,1) = uvw(INEW,1)+
     &                                FI(ILOC)*uvwr(NODE1(ILOC),1)
	             uvw(INEW,2) = uvw(INEW,2)+
     &                                FI(ILOC)*uvwr(NODE1(ILOC),2)
	             uvw(INEW,3) = uvw(INEW,3)+
     &                                FI(ILOC)*uvwr(NODE1(ILOC),3)
	             duvwdt(INEW,1) = duvwdt(INEW,1)+
     &                                FI(ILOC)*duvwdtr(NODE1(ILOC),1)
	             duvwdt(INEW,2) = duvwdt(INEW,2)+
     &                                FI(ILOC)*duvwdtr(NODE1(ILOC),2)
	             duvwdt(INEW,3) = duvwdt(INEW,3)+
     &                                FI(ILOC)*duvwdtr(NODE1(ILOC),3)
     		     
                  END DO
               END DO
C
	       IREG(INEW) = 1
	    END IF
	 END DO
      END DO
C
C.....Find new lateral node coordinates, assuming vertical lateral
C     boundaries
C
      DO KF=2,5
      IF (IBCOND(KF).NE.2) THEN
         CALL IMPLAT(KF,PHIN)
C        -----------
      END IF
      END DO
C
C.....Restore dimension for SOLVE
C
      NOM = NOMSA
C
C.....Print regriddedd node coordinates
C
      WRITE(IO6,*) 'Regridded nodes : I,X,Y,Z,PHI,PHIN :'
      DO I=1,NOM
         WRITE(IO6,2000) I,(XYZNOD(I,J),J=1,3),PHI(I),PHIN(I)
      END DO
C
      RETURN
C
 2000 FORMAT(I5,5F13.4)
 2010 FORMAT(5I5,2F10.5)
 2020 FORMAT(17I5)
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE POLYIN(INIE,X,Y)
C-----------------------------------------------------------------------
C
C     Determines if the point (X,Y) is inside the polygon whose vertices
C     are the NNODE points whose coordinates are in XYZ(NNODE,1) and XYZ(,2).
C     Returns with a value 1 for inside and 0 for outside in SIDEIN.
C
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NDM=5,NNODE=4)

      LOGICAL*4 INIE
C
      COMMON /DELEM2/ XYZ(NDM,3),IDUM(NDM)
C
      DATA ZERO/0.D0/,EPS/1.D-06/
C
      ICOUNT = 0
C
C.....Case of (x,y) being one vertex
C
      DO I=1,NNODE
	 DIST2 = (XYZ(I,1) - X)**2 + (XYZ(I,2) - Y)**2
	 IF(DIST2.LT.EPS) THEN
	    INIE = .TRUE.
	    RETURN
	 END IF
      END DO
C
C.....Other cases
C
      DO I=1,NNODE-1
         IF ((XYZ(I,1) - X)*(XYZ(I+1,1) - X).LE.ZERO) THEN
            YOFPOLY = (XYZ(I+1,2) - XYZ(I,2))/(XYZ(I+1,1) - XYZ(I,1))*
     .                (X - XYZ(I,1)) + XYZ(I,2)
            IF (DABS(Y-YOFPOLY).LT.EPS) THEN
	       INIE = .TRUE.
	       RETURN
	    ELSE IF (Y.GT.YOFPOLY) THEN
	        ICOUNT = ICOUNT + 1
            END IF
         ELSE IF (DABS((XYZ(I,1) - X)*(XYZ(I+1,1) - X)).LT.EPS) THEN
            XOFPOLY = (XYZ(I+1,1) - XYZ(I,1))/(XYZ(I+1,2) - XYZ(I,2))*
     .                (Y - XYZ(I,2)) + XYZ(I,1)
            IF (DABS(X-XOFPOLY).LT.EPS) THEN
	       INIE = .TRUE.
	       RETURN
            END IF
         END IF
      END DO
C
      IF ((XYZ(NNODE,1) - X)*(XYZ(1,1) - X).LE.ZERO) THEN
         YOFPOLY = (XYZ(1,2) - XYZ(NNODE,2))/(XYZ(1,1) - XYZ(NNODE,1))*
     .             (X - XYZ(NNODE,1)) + XYZ(NNODE,2)
         IF (DABS(Y-YOFPOLY).LT.EPS) THEN
	    INIE = .TRUE.
	    RETURN
         ELSE IF (Y.GT.YOFPOLY) THEN
            ICOUNT = ICOUNT + 1
         END IF
      ELSE IF (DABS((XYZ(NNODE,1) - X)*(XYZ(1,1) - X)).LT.EPS) THEN
         XOFPOLY = (XYZ(1,1) - XYZ(NNODE,1))/(XYZ(1,2) - XYZ(NNODE,2))*
     .             (Y - XYZ(NNODE,2)) + XYZ(NNODE,1)
         IF (DABS(X-XOFPOLY).LT.EPS) THEN
	    INIE = .TRUE.
	    RETURN
         END IF
      END IF
C
      SIDEIN = DFLOAT(MOD(ICOUNT,2))
C
      IF(SIDEIN.GT.ZERO) THEN
         INIE = .TRUE.
      END IF
C
      RETURN
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE POLYIN2(INIE,X,Y)
C-----------------------------------------------------------------------
C
C     Determines if the point (X,Y) is inside the polygon whose vertices
C     are the MNOD points whose coordinates are in XYZ(MNOD,1) and XYZ(,2).
C     Returns with a value 1 for inside and 0 for outside in SIDEIN.
C
C     polyin bugs with Nec sx5 (IDRIS)
C     polyin2 not 'exhaustive' but better adapted
C     C.Fochesato, CMLA, 2003
C
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NDM=5,NNODE=4)

      LOGICAL INIE
C
      COMMON /DELEM2/ XYZ(NDM,3),IDUM(NDM)
C
      SAVE
C
      DATA ZERO/0./,EPS/1.E-06/
C
      ICOUNT = 0
C
C.....Case of (x,y) being one vertex
C
      DO I=1,NNODE
	 DIST2 = (XYZ(I,1) - X)**2 + (XYZ(I,2) - Y)**2
	 IF(DIST2.LT.EPS) THEN
	    INIE = .TRUE.
	    RETURN
	 END IF
      END DO
C
C.....Other cases excepted degenerated quadrilaterals
C
      IF (ABS(XYZ(4,2)-XYZ(1,2)).GE.EPS
     .    .AND.ABS(XYZ(3,2)-XYZ(2,2)).GE.EPS
     .    .AND.ABS(XYZ(2,1)-XYZ(1,1)).GE.EPS
     .    .AND.ABS(XYZ(3,1)-XYZ(4,1)).GE.EPS) THEN
        XOFPOLY1=(XYZ(4,1)-XYZ(1,1))/(XYZ(4,2)-XYZ(1,2))
     .         *(Y-XYZ(1,2))+XYZ(1,1)
        XOFPOLY2=(XYZ(3,1)-XYZ(2,1))/(XYZ(3,2)-XYZ(2,2))
     .         *(Y-XYZ(2,2))+XYZ(2,1)
        YOFPOLY1=(XYZ(2,2)-XYZ(1,2))/(XYZ(2,1)-XYZ(1,1))
     .         *(X-XYZ(1,1))+XYZ(1,2)
        YOFPOLY2=(XYZ(3,2)-XYZ(4,2))/(XYZ(3,1)-XYZ(4,1))
     .         *(X-XYZ(4,1))+XYZ(4,2)
      ELSE IF (ABS(XYZ(4,1)-XYZ(1,1)).GE.EPS
     .    .AND.ABS(XYZ(3,1)-XYZ(2,1)).GE.EPS
     .    .AND.ABS(XYZ(2,2)-XYZ(1,2)).GE.EPS
     .    .AND.ABS(XYZ(3,2)-XYZ(4,2)).GE.EPS) THEN
        YOFPOLY1=(XYZ(4,2)-XYZ(1,2))/(XYZ(4,1)-XYZ(1,1))
     .         *(X-XYZ(1,1))+XYZ(1,2)
        YOFPOLY2=(XYZ(3,2)-XYZ(2,2))/(XYZ(3,1)-XYZ(2,1))
     .         *(X-XYZ(2,1))+XYZ(2,2)
        XOFPOLY1=(XYZ(2,1)-XYZ(1,1))/(XYZ(2,2)-XYZ(1,2))
     .         *(Y-XYZ(1,2))+XYZ(1,1)
        XOFPOLY2=(XYZ(3,1)-XYZ(4,1))/(XYZ(3,2)-XYZ(4,2))
     .         *(Y-XYZ(4,2))+XYZ(4,1)
      END IF
C
      IF ((X.GE.XOFPOLY1.AND.X.LE.XOFPOLY2)
     .   .AND.(Y.GE.YOFPOLY1.AND.Y.LE.YOFPOLY2)) THEN
         INIE=.TRUE.
         RETURN
      END IF
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE DTSTEP(IO99,UVW)
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

C
      COMMON/DATGEN/  DUM1(5),IDUM1(8),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(MOMM*2+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7+2*NOMM),
     .                ICUMUL(6),NDST(6,2),IDUM3(60),IBCOND(6)
c      COMMON /SYSTEM/ DUM3(24),TIME,DUM4(6+8*MYMAX),IDUM4(4),ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOUNDC/ DT,DT2,DT22,DUM5(5),GE,DE1,DUM6(4),IDV
C
      DIMENSION UVW(3,NOMM)
C
      DATA HALF/.5D0/,BIG/1.D10/,FFETH/0.04D0/,CMAX/0.5D0/,
     .     COPT/0.5D0/,ONE/1.D0/,ZERO/0.D0/
C
C.....Save commons and local variables to enable static commons
C
      SAVE
C     SAVE /DATGEN/,/MAILLE/,/BOUNDC/,/SYSTEM/
C
C.....Shift time vector for computing at next time step
C
      TIME = TIME + DT
C
C.....Fix time step by varying time step according to max nde motion
C
      RMIN = BIG
C
      DO KF=1,5
         IF(IBCOND(KF).EQ.1) THEN
C
C           Updated Dirichlet boundary
C
            NS = NDST(KF,1)
            NM = NDST(KF,2)
C
C           Calculate min distance between free Dir. bd. nodes at t
C
            DO J=2,NM
	       DO I=2,NS
	          IND   = ICUMUL(KF) + (J-1)*NS + I
		  INDM1 = IND - 1
		  INDP1 = IND - NS
		  DIS1  = ZERO
		  DIS2  = ZERO
		  DO K=1,3
		     DIS1 = DIS1 + (XYZNOD(IND,K) - XYZNOD(INDM1,K))**2
		     DIS2 = DIS2 + (XYZNOD(IND,K) - XYZNOD(INDP1,K))**2
		  END DO
		  DIS1 = DSQRT(DIS1)
		  DIS2 = DSQRT(DIS2)
C
                  IF(DIS1.LT.RMIN) THEN
                     RMIN = DIS1
                  END IF
                  IF(DIS2.LT.RMIN) THEN
                     RMIN = DIS2
                  END IF
               END DO
            END DO
         END IF
      END DO
C
C.....Select new time step such that max node motion stays constant
C
      IF(ILOOP.EQ.1) THEN
C
         DT0 = DT
C
C        Initial Courant number
C
         C0 = DT0*DSQRT(GE*DE1)/RMIN
C
C        Courant number <= 0.5 for stablity reasons
C
c         IF(C0.GT.CMAX) THEN
c            C0 = CMAX
c         END IF
C
C        Protection by min dt in [dt0/50]
C
         DTMIN = FFETH*DT0
C
         WRITE(IO99,*) 'DT0,C0,DTMIN :',DT0,C0,DTMIN
         WRITE(IO99,*) 'ILOOP,CINC,CNEW,RMIN,DTC'
      END IF
C
C     New courant number so courant number tends to COPT by tanh law
C     implemented for regridding programs
C
C      CINC = (TANH(DFLOAT(ILOOP-5)*HALF) + ONE)*HALF*(COPT-C0)
      CINC = ZERO
C      CNEW = C0 + CINC
      CNEW=COPT
C
C     New time step based on constant Courant number
C
      DTC = RMIN*CNEW/DSQRT(GE*DE1)
C
C     Use of max. velocity instead of long wave speed
C
C      VELOMAX=ZERO
C      DO I=1,NOM
C         VELO=DSQRT(UVW(1,I)*UVW(1,I)+UVW(2,I)*UVW(2,I)
C     .                               +UVW(3,I)*UVW(3,I))
C         IF (ABS(VELO).GT.VELOMAX) THEN
C	    VELOMAX=ABS(VELO)
C	 END IF
C      END DO
C      DTC = RMIN*CNEW/VELOMAX
C
      IF(DTC.LT.DTMIN) THEN
         DTC = DTMIN
      END IF
C
      CT = DTC/RMIN
C
      WRITE(99,*) ILOOP,CINC,CNEW,RMIN,DTC
C
C.....New time updating data
C
      DT   = DTC
      DT2  = DT*HALF
      DT22 = DT2*DT
C
      RETURN
C
      END
C-----------------------------------------------------------------------
      REAL*8 FUNCTION DACOSH(X)
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      DATA ONE/1.D0/,TWO/2.D0/,EPS/1.D-12/,NMAX/20/
C
      Y0  = DLOG(TWO*X)
      ERR = ONE
      N   = 1
      DO WHILE((N.LE.NMAX).AND.(ERR.GT.EPS))
         DY  = (DCOSH(Y0)-X)/DSINH(Y0)
	 ERR = DABS((DY)/Y0)
	 Y0  = Y0 - DY
	 N   = N + 1
      END DO
      DACOSH = Y0
C
      RETURN
      END
C----------------------------------------------------------------------
      SUBROUTINE CELES(OMEGA,CEL,ZKWAVE)
C----------------------------------------------------------------------
C0  CELESF    CELES
C1  Purpose   CELES computes the wave celerity c using the linear dispersion
C1            relation : k tanh(k*d) = (omega)**2 / ge  in the form
C1            c = co tanh(k*h), with co=g T**2/2 pi
C2  Method    Newton-Raphson iteration method with relative error EPS
C2            Computations assume SI, i.e., MKS units but not necessary.
C2            Uses : x = k*h; k=2 pi/L
C2                   x(n+1)   = x(n) - F(x(n))/DF(x(n))
C2                   F(x(n))  = x(n) - D/tanh(x(n))
C2                   DF(x(n)) = 1 + D/sinh(x(n))**2
C2            Number of iterations is limited to ITERM=50
C3  CALL ARG. OMEGA : Wave pulsation (rad s-1)
C3            H0    : Depth of the sea at the radiation boundary NR2
C3            CEL   : Wave celerity
C3            GE    : Acceleration of the gravity
CE  ERRORS    01=The number of iterations is too large
C9  80-00     S. Grilli, Ocean Engng. Dept., Univ. of Rhode Island
CL
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
C
      CHARACTER *8 TEXTE
C
      COMMON /BOUNDC/ DUM1(8),GE,DUM2,DE,DUM3(3),IDUM1
      COMMON /DATGEN/ DUM4(2),H0,DUM5(2),IDUM2(10)
C
      DATA TEXTE/'CELES 01'/,EPS/1.D-06/,HALF/0.5D0/,
     .     PI/3.1415926535898D0/,TWO/2.D0/,ONE/1.D0/,ITERM/50/
C
      SAVE
C     SAVE /BOUNDC/
C
      D     = OMEGA*OMEGA * DE / GE
      ITER  = 0
      ERR   = ONE
C
C.....Initial guess for nondimensional solution X
C
      IF (D.GE.ONE) THEN
         X0 = D
      ELSE
         X0 = DSQRT(D)
      END IF
C
C.....Solution using Newton-Raphson method
C
      DO WHILE((ERR.GT.EPS).AND.(ITER.LE.ITERM))
         F    = X0 - D/DTANH(X0)
         DF   = ONE + D/DSINH(X0)**2
         X1   = X0 - F/DF
         ERR  = DABS((X1-X0)/X0)
         X0   = X1
         ITER = ITER + 1
      END DO
C
      IF (ITER.GT.ITERM) THEN
         CALL ERRORS (TEXTE)
C        -----------
      ELSE
         CEL     = OMEGA*DE / X1
	 ZKWAVE  = OMEGA/CEL
      END IF
C
      RETURN
C
      END
C


C----------------------------------------------------------------------
      SUBROUTINE ERRORS(TEXTE)
C---------------------------------------------------------------------
      CHARACTER*8 TEXTE
      WRITE(6,10)TEXTE
      WRITE(*,10)TEXTE
      STOP
   10 FORMAT(1H ,'PROGRAM STOP DUE TO ERROR  : ',A8)
      END


C-----------------------------------------------------------------------
c
c	automatic regridding 
c	f77 style, no change in the number of points
c	the mesh calulus come from gener
c
c	X Barthelemy, 05/10
C-----------------------------------------------------------------------
	  
       SUBROUTINE autoregrid3(Mainloop,switchautoregrid,iprint,
     &              ISTART,IGRID,TSTART,TIME,iloop)
       IMPLICIT REAL*8 (A-H,O-Z)
       logical Mainloop,switchautoregrid
       integer iprint,ISTART,IGRID

       switchautoregrid=.false.
       Mainloop=.true.
       if ((mod(ILOOP,3).eq.0).and.(TIME.NE.TSTART)) then
       switchautoregrid=.true.
       Mainloop=.false.
       ISTART =1
       IGRID=1
       iprint=1
       endif
       return
       end

       SUBROUTINE autoregrid4(Mainloop,switchautoregrid,iprint,
     &              ISTART,IGRID,INICOULOMB,IFREECOULOMB,iloop0,iloop)
       IMPLICIT REAL*8 (A-H,O-Z)
       logical Mainloop,switchautoregrid
       integer iprint,ISTART,IGRID
       COMMON /FMAPAR/ CUBE,TETHA,NLEVELS,IFFT,MP,IPFMA,IEPMTA,
     .                NPROC,IDIRECT,IBALANCED,IVERBOSE,IPMON
c     .                ,INICOULOMB,IFREECOULOMB
     
	save
       
       if ((.not.(Mainloop)).and.(switchautoregrid)) then
       ISTART =1
       IGRID=1
       INICOULOMB =  0
       IFREECOULOMB=0
       Mainloop=.true.
       iloop0=0
       iloop=0
       IBEMANALYSIS=1
       CALL CALLPMTA(CUBE,NLEVELS,IFFT,MP,IPFMA,IEPMTA,THETA,
C          --------
     .              NPROC,IDIRECT,IBALANCED,IVERBOSE,IPMON,
     .              NOM,MOM,INICOULOMB,IFREECOULOMB)
       INICOULOMB =  1
       IFREECOULOMB=0
     
       endif
       return
       end
