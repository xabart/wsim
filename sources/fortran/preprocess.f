C-------------------------------------------------------------------------------
C
C                       PRE-PROCESSING
C
C-------------------------------------------------------------------------------
C###############################################################################
C Author:  Christophe Kassiotis    					                   #
C          LMT - Cachan        |     Institute of Scientific Computing         #
C          ENS - Cachan        |     Technische Universitaet Braunschweig      #
C          Cachan, France      |     Braunschweig, Germany                     #
C  Email:  kassiotis@lmt.ens-cachan.fr                                         #
C                                                                              #
C  Copyright (c) 2007. All rights reserved. No warranty. No                    #
C  liability.                                                                  #
C                                                                              #
C  input
C  output
C     .            mx,my,mz,nintr,nsubc,almaxb,almaxi,
C     .            zl0,w0,h0,x0,d0,ibot,slope,zk,
C     .            ibcond,
C     .            ilmax,iptyp,nbs,iprint,tprint,
C     .            dt,tstart,tmax,rho,cpress,cprest,ge,tdamp,tdown
C     .            de(1),de(2),omega,v0h,infie,noi,nfr,nht,tolmax,
C     .            xldamp,dnu0,tresh,sutens,npoly,nxi,nyi,nzi,nga,
C     .            isym,ipost,ivect,istartis,igrid,ifield,
C     .            xmin,dxmin,dymin,
C     .            xyzgag,
C     .            cube,nlevels,ifft,mp,
C     .            ipfma,iepmta,teta,nproc,
C     .            idirect,ibalanced,iverbose,ipmon,
C     .            xyzbot,
C     .            nh,ifreq,ap,op,sp,zkwave,distf,tfoc,alphaf,
C     .            wmidth,delfr,dalpha 
C                                                                              #
C                                                                              #
C###############################################################################
      subroutine preprocess(
     .            almaxi,
     .            ilmax,iprint,tprint,
     .            infie,nht,
     .            istart,igrid,
     .            xmin,dxmin,dymin
     .           )

C-------------------------------------------------------------------------------
      implicit none

C-------------------------------------------------------------------------------
      include 'param.inc'

      include 'datgen.h' ! zl0,w0,h0,x0,d0,mx,my,mz,nx,ny,nz,nsubc,nintr,nom,mom
      include 'maille.h' ! xyznod,xicon,xstart,igcon,iface,isub,intg,lxyz,icumul
c                          ndst,idst,jslim,nelef,nnodf,ielef,inodf,ibcond
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdamp,tdown,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      include 'boundc.h' ! dt,dt2,dt22,tstart,tmax,rho,cpress,cprest,ge,de,
c                          omega,v0h,tolmax,nbs
      include 'interf.h' ! field,lisub,irep,nxi,nyi,nzi,noi,ifield
      include 'dissip.h' ! xdamp,xldamp,dnu0,tresh,sutens,coef,coefx,islop,
c                          npoly,iflab,ibreak,itens
      include 'paddle.h' ! ap,op,sp,zkwave,distf,tfoc,alphaf,wmidth,os,ost,ostt,
c                          damp,dampt,damptt,delfr,nfr,nh,ifreq
      include 'cgage.h'  ! zyzgag,uvwg,phitb,phig,phing,phib,phinb,xyzbot,uvwb,
c                          igag,nga,intzed
      include 'fmapar.h' ! cube,teta,nlevels,ifft,mp,ipfma,iepmta,nproc,idirect,
c                          ibalanced,iverbose,ipmon,inicoulomb,ifreecoulomb
      include 'botfct.h' ! bot,slope,zk,ibot
      include 'subdi.h'  ! almaxb,lsub(nomn)
      include 'option.h' ! isym,ipost,ivect

      integer ilmax,iprint,infie,nht,istart,igrid

      real*8  almaxi,tprint,xmin,dxmin,dymin

      real*8  one,half,two,pi,pdeg,pi2,zero,dalpha,
     .        cel0,zk0,zwl0,zlwave,twave,om,delom
      integer io,k,ig,ifr,ih
      character*8 texte

      data    io/5/,one/1.0/,half/0.5/,texte/'preproce'/,two/2.0/,
     .        pi/3.1415926535898d0/,pdeg/180.d0/,zero/0.d0/

      pi2 = two*pi

C-------------------------------------------------------------------------------
C.....Problem general input files
C-------------------------------------------------------------------------------

c      OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/input.dat2')
c
c      READ(IO,*) ILOOP0,MX,MY,MZ,NINTR,NSUBC,ALMAXB,ALMAXI
c      READ(IO,*) ZL0,W0,H0,X0,D0,IBOT,SLOPE,ZK
c      READ(IO,*) (IBCOND(K),K=1,6)
c      READ(IO,*) ILMAX,IPTYP,NBS,IPRINT,TPRINT
c      READ(IO,*) DT,TSTART,TMAX,RHO,CPRESS,CPREST,GE,TDAMP,TDOWN
c      READ(IO,*) DE(1),DE(2),OMEGA,V0H,INFIE,NOI,NFR,NHT,TOLMAX
c      READ(IO,*) XLDAMP,DNU0,TRESH,SUTENS,NPOLY,NXI,NYI,NZI,NGA
c      READ(IO,*) ISYM,IPOST,IVECT
c      READ(IO,*) ISTART,IGRID
c      IF(IGRID.GT.1) THEN
c         READ(IO,*) XMIN,DXMIN,DYMIN
c      ELSE
cC        empty line
c         READ(IO,*)
c      END IF
c      IF(NGA.GT.0) THEN
c         DO IG=1,NGA
c            READ(IO,*) XYZGAG(IG,1),XYZGAG(IG,2)
c	 END DO
c      END IF
c
c      CLOSE(IO)

c       print*, "init preprocess", IPTYP,NHT, infie    	
c	print*,'1.1'

       call readparamdat(
     .	ilmax,iprint,infie,nht,istart,igrid,almaxi,tprint,xmin,
     .  dxmin,
     .  dymin,dalpha,cel0,zk0,zwl0,zlwave,twave,om,delom,k,ig,ifr,ih		
c     .            almaxi,
c     .            ilmax,iprint,tprint,
c     .            infie,nht,
c     .            istart,igrid,
c     .            xmin,dxmin,dymin
     .           )
c       print*, "apres readparamdat",IPTYP,NHT, infie     	
c       print*, "avant test",IPTYP,NHT,NINTR , infie  	

C NINTR must be an odd integer
c Xavier: 10/02/2010: see l10 nwt.f NINTR even integer?
c the formula seems so.

c      if (floor(nintr*half)-half*floor(nintr*one).LT.0) then
c         call errors(texte)
c      end if
	if (mod(NINTR,2).eq.1) call errors(texte)

c       print*, "apres test",IPTYP,NHT,NINTR , infie  	

C Flag for interior points
      ifield = .false.
      if (infie.eq.1) then
         ifield = .true.
      end if

C-------------------------------------------------------------------------------
C.....New input file for FMA parameters, CF03
C-------------------------------------------------------------------------------

c      OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/fmadata.dat2')
c
c      READ(IO,*) CUBE,NLEVELS,IFFT,MP
c      READ(IO,*) IPFMA,IEPMTA,TETA,NPROC
c      READ(IO,*) IDIRECT,IBALANCED,IVERBOSE,IPMON
c
c      CLOSE(IO)
      
c      call readfmdata(
c     .            CUBE,NLEVELS,IFFT,MP,
c     .            IPFMA,IEPMTA,TETA,NPROC,
c     .            IDIRECT,IBALANCED,IVERBOSE,IPMON
c     .           )
c	print*,'1.2'
      call readfmdata(
     .	ilmax,iprint,infie,nht,istart,igrid,almaxi,tprint,xmin,
     .  dxmin,
     .  dymin,dalpha,cel0,zk0,zwl0,zlwave,twave,om,delom,k,ig,ifr,ih		
     .  )
c	print*,'1.3'

C-------------------------------------------------------------------------------
C     Read additional data for surface gages for IPOST=1 (VOF)
C-------------------------------------------------------------------------------

c      IF (IPOST.EQ.1) THEN
c         IF(NGA.GT.0) THEN
c            OPEN(UNIT=IO,STATUS='OLD',FILE='input/gage.dat2')
c	       DO IG=1,NGA
c	          READ(IO,*) XYZGAG(IG,1),XYZGAG(IG,2)
c	          XYZBOT(IG,1) = XYZGAG(IG,1)
c	          XYZBOT(IG,2) = XYZGAG(IG,2)
c	       END DO
c            CLOSE(IO)
c         END IF
c      END IF

c      IF (IPOST.EQ.1) THEN
c	      call readgagedata(XYZBOT,XYZGAG,IPOST,NGA)
c      END IF

      IF (IPOST.EQ.1) call readgagedata()
      

C-------------------------------------------------------------------------------
C     Read additional data for paddle wavemaker
C-------------------------------------------------------------------------------
      call SurfaceAndPaddleData(
     .	ilmax,iprint,infie,nht,istart,igrid,almaxi,tprint,xmin,
     .  dxmin,
     .  dymin,dalpha,cel0,zk0,zwl0,zlwave,twave,om,delom,k,ig,ifr,ih		
     .  )
c	print*,'1.4'

c      IF ((IPTYP.EQ.1).OR.(IPTYP.EQ.7).OR.(IPTYP.EQ.8)) THEN
c         IF(NHT.EQ.0) THEN
cC-------------------------------------------------------------------------------
cC           One wave
cC-------------------------------------------------------------------------------
c            AP(1,1) = ONE
c            OP(1) = OMEGA
c            SP(1) = ZERO
c            NH    = 1
c         ELSE
cC-------------------------------------------------------------------------------
cC        Random wave data in "paddle.data"
cC-------------------------------------------------------------------------------
c	       IF(IPTYP.EQ.1) THEN
c            OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/sinewaves.dat')
c               DO IFR=1,NFR
c                  DO IH=1,NHT
c                     READ(IO,*) AP(IFR,IH),OP(IFR),SP(IH)
c                     OP(IFR) = OP(IFR)*PI*TWO
c                  END DO
c               END DO
c	       ELSE IF((IPTYP.EQ.8).OR.(IPTYP.EQ.7)) THEN
cC-------------------------------------------------------------------------------
cC              Store wave focusing viarocio@yahoo.com.mxdata with SP = thetan
cC              NOTE: Adjusting AP to be smaller for larger angle is more real
cC                    Also using directional spreading of omega is possible
cC                    IFREQ=0 => no spreading, omega=cst
cC                    IFREQ=1 => spreading for frequency focusing
cC-------------------------------------------------------------------------------
c             OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/paddle.dat2')
c              READ(IO,*) DISTF,TFOC,ALPHAF,WMIDTH,IFREQ,DELFR
c	          DALPHA = TWO*ALPHAF/(NHT-1)
cC-------------------------------------------------------------------------------
cC           Linear theory results
cC-------------------------------------------------------------------------------
c               CALL CELES(OMEGA,CEL0,ZK0)
c
c               ZWL0 = CEL0*pi2/OMEGA
c               DO IFR=1,NFR
c                  IF(IFREQ.EQ.0) THEN
c                     DO IH=1,NHT
c 		              AP(IFR,IH) = ONE
c		              SP(IH) = ((IH-1)*DALPHA-ALPHAF)*PI/PDEG
c                     END DO
c                     OP(IFR) = OMEGA
c   	                ZKWAVE(IFR) = ZK0
c		        ELSE IF(IFREQ.EQ.1.AND.NFR.EQ.NHT) THEN
c		           IH = IFR
c                     AP(IFR,IH) = ONE
c		           SP(IH) = ((IH-1)*DALPHA - ALPHAF)*PI/PDEG
c		           ZLWAVE      = ZWL0/DCOS(SP(IH))
c		           ZKWAVE(IFR) = PI2/ZLWAVE
c		           TWAVE       = DSQRT(PI2*ZLWAVE/(GE*
c     .                                   DTANH(ZKWAVE(IFR)*H0)))
c                     OP(IFR)     = PI2/TWAVE
c                  ELSE IF(IFREQ.EQ.1.AND.NFR.NE.NHT) THEN
cC-------------------------------------------------------------------------------
cC                 Case with varying frequencies, given uniformly
cC                 between OMEGA-DELFR/2 and OMEGA+DELFR/2
cC                 Given amplitude V0H is the linear sum,
cC                 used in PADSIN : here initiated to ONE
cC                 -> for comparisons with Hong etal
cC-------------------------------------------------------------------------------
c                     OM = OMEGA*PI2
c                     DELOM = DELFR*PI2
c                     OP(IFR) = OM+DELOM*(REAL(IFR-1)/REAL(NFR-1)-HALF)
c                     CALL CELES(OP(IFR),CEL0,ZK0)
c                     ZKWAVE(IFR) = ZK0
c                     DO IH=1,NHT
c		              SP(IH) = ((IH-1)*DALPHA - ALPHAF)*PI/PDEG
c                        AP(IFR,IH) = ONE/REAL(NFR*NHT)
c	                END DO
c		        END IF
c               END DO
c	       END IF
c            CLOSE(IO)
c            NH = NHT
c         END IF
c      END IF

      write(*,*)   ' === Preprocess done === '
      write(io6,*) ' === Preprocess done === '

C-------------------------------------------------------------------------------
      return
C-------------------------------------------------------------------------------
      end
C-------------------------------------------------------------------------------

C-------------------------------------------------------------------------------
C
C                       INITIALIZATION TIME
C
C-------------------------------------------------------------------------------
C###############################################################################
C Author:  Christophe Kassiotis                                                #
C          LMT - Cachan        |     Institute of Scientific Computing         #
C          ENS - Cachan        |     Technische Universitaet Braunschweig      #
C          Cachan, France      |     Braunschweig, Germany                     #
C  Email:  kassiotis@lmt.ens-cachan.fr                                         #
C                                                                              #
C  Copyright (c) 2007. All rights reserved. No warranty. No                    #
C  liability.                                                                  #
C                                                                              #
C  purpose : init, generally to zero, desired values.                          #
C                                                                              #
C  input  : istart,tstart,dt                                                   #
C  output : time,timep,dt2,dt22,pres(),dprdt(),phit(),phitn(),uvw(,),duvwdt(,) #
C                                                                              #
C                                                                              #
C###############################################################################
      subroutine init(timep,istart)

C-------------------------------------------------------------------------------
      implicit none

C-------------------------------------------------------------------------------
      include 'param.inc'

C-------------------------------------------------------------------------------
      include 'datgen.h' ! zl0,w0,h0,x0,d0,mx,my,mz,nx,ny,nz,nsubc,nintr,nom,mom
      include 'boundc.h' ! dt,dt2,dt22,tstart,tmax,rho,cpress,cprest,ge,de,
c                          omega,v0h,tolmax,nbs
      include 'result.h' ! pg(nomm),phi(nomm),phin(nomm),phit(nomm),phitn(nomm),
c                          phis(nomm),phim(nomm),phiss(nomm),phimm(nomm),
c                          phins(nomm),phinm(nomm),phits(nomm),phitm(nomm),
c                          phism(nomm),phinn(nomm),
c                          pres(nomn),dprdt(nomm),uvw(3,nomm),duvwdt uvw(3,nomm)
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
C-------------------------------------------------------------------------------

      real*8    timep,half,zero
      integer*4 istart,i,k

      DATA half/0.5D0/,zero/0.D0/
C-------------------------------------------------------------------------------

      TIME  = TSTART
      TIMEP = TSTART
      DT2   = DT*HALF 
      DT22  = DT2*DT 

c      DO I=1,NOM 
c         PRES(I)  = ZERO 
c         DPRDT(I) = ZERO 
c         PHIT(I)  = ZERO
c         PHITN(I) = ZERO
c         IF(ISTART.EQ.0) THEN
c            PHI(I)  = ZERO
c            PHIN(I) = ZERO
c         END IF
c         DO K=1,3
c            UVW(I,K)    = ZERO
c            DUVWDT(I,K) = ZERO
c         END DO
c      END DO

c-------------------------------------------
c	New Restart Function, XB
c-------------------------------------------
      IF(ISTART.EQ.0) THEN
         DO I=1,NOM 
	 PRES(I)  = ZERO 
	 DPRDT(I) = ZERO 
	 PHIT(I)  = ZERO
	 PHITN(I) = ZERO
	 PHI(I)  = ZERO
	 PHIN(I) = ZERO
         END DO

      DO I=1,NOM 
         DO K=1,3
c            UVW(I,K)    = ZERO
c            DUVWDT(I,K) = ZERO
c  Bug
            UVW(K,I)    = ZERO
            DUVWDT(K,I) = ZERO
         END DO
      END DO
      END IF

      write(*,*)   ' === Init done === '
      write(io6,*) ' === Init done === '
C-------------------------------------------------------------------------------
      return
C-------------------------------------------------------------------------------
      end
C-------------------------------------------------------------------------------

C-------------------------------------------------------------------------------
C
C                       READ PARAM
C
C-------------------------------------------------------------------------------
C###############################################################################
C Author:  Christophe Kassiotis                                                #
C          LMT - Cachan        |     Institute of Scientific Computing         #
C          ENS - Cachan        |     Technische Universitaet Braunschweig      #
C          Cachan, France      |     Braunschweig, Germany                     #
C  Email:  kassiotis@lmt.ens-cachan.fr                                         #
C                                                                              #
C  Copyright (c) 2007. All rights reserved. No warranty. No                    #
C  liability.                                                                  #
C                                                                              #
C  purpose : init, generally to zero, desired values.                          #
C                                                                              #
C  input  : istart,tstart,dt                                                   #
C  output : time,timep,dt2,dt22,pres(),dprdt(),phit(),phitn(),uvw(,),duvwdt(,) #
C                                                                              #
C                                                                              #
C###############################################################################
      subroutine readparam(io6)

C-------------------------------------------------------------------------------
      implicit none
      include 'param.h'

C-------------------------------------------------------------------------------
      integer*4 io,io6
      character*20 comment     

      data io/5/
      

      open(unit=io,file='input/param.dat2',
     .     status='old',form='formatted')
      read(io,*) comment
      read(io,*) comment
      read(io,*) comment   
      read(io,*) nxm,nym,nzm
      read(io,*) comment
      read(io,*) mnod,noim,nfmax,nhmax,mymax,ndm,nintm,ngam
      read(io,*) comment
      read(io,*) mpm
      close(io)

      nomm = 2*( nxm*nym + nxm*nzm + nym*nzm )
      momm = nomm - 4 * (nxm + nym + nzm) + 6
      mnodm = mnod*mnod
      ndm2 = ndm*ndm
      lsavef = nintm*nintm*( 7 + 3*mnodm )

      write(io6,*) ' === Parameters === '
      write(io6,2001) nxm,nym,nzm,nomm,momm
      write(io6,2002) mnod,noim,nfmax,nhmax,mymax,ndm,nintm,ngam
      write(io6,2003) mnodm,ndm2,lsavef,mpm

C-------------------------------------------------------------------------------
2001  format('nxm:',i4,4x,'nym:',i4,4x,'nzm:',i4,4x,
     .       'nomm:',i4,4x,'momm:',i4,4x)
2002  format('mnod:',i4,4x,'noim:',i4,4x,'nfmax:',i4,4x,'nhmax:',i4,4x,
     .       'mymax:',i4,4x,'ndm:',i4,4x,'initm:',i4,4x,'ngam:',i4,4x)
2003  format('mnodm:',i4,4x,'ndm2:',i4,4x,'lsavef:',i4,4x,
     .       'npm:',i4,4x)

C-------------------------------------------------------------------------------
      write(*,*)   ' === Read parameters done === '
      write(io6,*) ' === Read parameters done === '

C-------------------------------------------------------------------------------
      end
C###############################################################################

      subroutine readparamdat(
     .	ilmax,iprint,infie,nht,istart,igrid,almaxi,tprint,xmin,
     .  dxmin,
     .  dymin,dalpha,cel0,zk0,zwl0,zlwave,twave,om,delom,k,ig,ifr,ih		
c     .            almaxi,
c     .            ilmax,iprint,tprint,
c     .            infie,nht,
c     .            istart,igrid,
c     .            xmin,dxmin,dymin
     .           )

C-------------------------------------------------------------------------------
      implicit none

C-------------------------------------------------------------------------------
      include 'param.inc'

      include 'datgen.h' ! zl0,w0,h0,x0,d0,mx,my,mz,nx,ny,nz,nsubc,nintr,nom,mom
      include 'maille.h' ! xyznod,xicon,xstart,igcon,iface,isub,intg,lxyz,icumul
c                          ndst,idst,jslim,nelef,nnodf,ielef,inodf,ibcond
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdamp,tdown,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      include 'boundc.h' ! dt,dt2,dt22,tstart,tmax,rho,cpress,cprest,ge,de,
c                          omega,v0h,tolmax,nbs
      include 'interf.h' ! field,lisub,irep,nxi,nyi,nzi,noi,ifield
      include 'dissip.h' ! xdamp,xldamp,dnu0,tresh,sutens,coef,coefx,islop,
c                          npoly,iflab,ibreak,itens
      include 'paddle.h' ! ap,op,sp,zkwave,distf,tfoc,alphaf,wmidth,os,ost,ostt,
c                          damp,dampt,damptt,delfr,nfr,nh,ifreq
      include 'cgage.h'  ! zyzgag,uvwg,phitb,phig,phing,phib,phinb,xyzbot,uvwb,
c                          igag,nga,intzed
      include 'fmapar.h' ! cube,teta,nlevels,ifft,mp,ipfma,iepmta,nproc,idirect,
c                          ibalanced,iverbose,ipmon,inicoulomb,ifreecoulomb
      include 'botfct.h' ! bot,slope,zk,ibot
      include 'subdi.h'  ! almaxb,lsub(nomn)
      include 'option.h' ! isym,ipost,ivect

      integer ilmax,iprint,infie,nht,istart,igrid

      real*8  almaxi,tprint,xmin,dxmin,dymin

      real*8  one,half,two,pi,pdeg,pi2,zero,dalpha,
     .        cel0,zk0,zwl0,zlwave,twave,om,delom
      integer io,k,ig,ifr,ih
      character*8 texte

      data    io/5/,one/1.0/,half/0.5/,texte/'preproce'/,two/2.0/,
     .        pi/3.1415926535898d0/,pdeg/180.d0/,zero/0.d0/

      pi2 = two*pi

C-------------------------------------------------------------------------------
C.....Problem general input files
C-------------------------------------------------------------------------------
      OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/input.dat2')

      READ(IO,*) ILOOP0,MX,MY,MZ,NINTR,NSUBC,ALMAXB,ALMAXI
      READ(IO,*) ZL0,W0,H0,X0,D0,IBOT,SLOPE,ZK
      READ(IO,*) (IBCOND(K),K=1,6)
      READ(IO,*) ILMAX,IPTYP,NBS,IPRINT,TPRINT
      READ(IO,*) DT,TSTART,TMAX,RHO,CPRESS,CPREST,GE,TDAMP,TDOWN
      READ(IO,*) DE(1),DE(2),OMEGA,V0H,INFIE,NOI,NFR,NHT,TOLMAX
      READ(IO,*) XLDAMP,DNU0,TRESH,SUTENS,NPOLY,NXI,NYI,NZI,NGA
      READ(IO,*) ISYM,IPOST,IVECT
      READ(IO,*) ISTART,IGRID
      IF(IGRID.GT.1) THEN
         READ(IO,*) XMIN,DXMIN,DYMIN
      ELSE
C        empty line
         READ(IO,*)
      END IF
      IF(NGA.GT.0) THEN
         DO IG=1,NGA
            READ(IO,*) XYZGAG(IG,1),XYZGAG(IG,2)
	 END DO
      END IF

      CLOSE(IO)
      return
      end
C###############################################################################
C###############################################################################

C-------------------------------------------------------------------------------
C.....New input file for FMA parameters, CF03
C-------------------------------------------------------------------------------
c      subroutine readfmdata(
c     .            CUBE,NLEVELS,IFFT,MP,
c     .            IPFMA,IEPMTA,TETA,NPROC,
c     .            IDIRECT,IBALANCED,IVERBOSE,IPMON
c     .           )
      subroutine readfmdata(
     .	ilmax,iprint,infie,nht,istart,igrid,almaxi,tprint,xmin,
     .  dxmin,
     .  dymin,dalpha,cel0,zk0,zwl0,zlwave,twave,om,delom,k,ig,ifr,ih		
     .  )
C-------------------------------------------------------------------------------
      implicit none

C-------------------------------------------------------------------------------
      include 'param.inc'

      include 'datgen.h' ! zl0,w0,h0,x0,d0,mx,my,mz,nx,ny,nz,nsubc,nintr,nom,mom
      include 'maille.h' ! xyznod,xicon,xstart,igcon,iface,isub,intg,lxyz,icumul
c                          ndst,idst,jslim,nelef,nnodf,ielef,inodf,ibcond
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdamp,tdown,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      include 'boundc.h' ! dt,dt2,dt22,tstart,tmax,rho,cpress,cprest,ge,de,
c                          omega,v0h,tolmax,nbs
      include 'interf.h' ! field,lisub,irep,nxi,nyi,nzi,noi,ifield
      include 'dissip.h' ! xdamp,xldamp,dnu0,tresh,sutens,coef,coefx,islop,
c                          npoly,iflab,ibreak,itens
      include 'paddle.h' ! ap,op,sp,zkwave,distf,tfoc,alphaf,wmidth,os,ost,ostt,
c                          damp,dampt,damptt,delfr,nfr,nh,ifreq
      include 'cgage.h'  ! zyzgag,uvwg,phitb,phig,phing,phib,phinb,xyzbot,uvwb,
c                          igag,nga,intzed
      include 'fmapar.h' ! cube,teta,nlevels,ifft,mp,ipfma,iepmta,nproc,idirect,
c                          ibalanced,iverbose,ipmon,inicoulomb,ifreecoulomb
      include 'botfct.h' ! bot,slope,zk,ibot
      include 'subdi.h'  ! almaxb,lsub(nomn)
      include 'option.h' ! isym,ipost,ivect

      integer ilmax,iprint,infie,nht,istart,igrid

      real*8  almaxi,tprint,xmin,dxmin,dymin

      real*8  one,half,two,pi,pdeg,pi2,zero,dalpha,
     .        cel0,zk0,zwl0,zlwave,twave,om,delom
      integer io,k,ig,ifr,ih
      character*8 texte

      data    io/5/,one/1.0/,half/0.5/,texte/'preproce'/,two/2.0/,
     .        pi/3.1415926535898d0/,pdeg/180.d0/,zero/0.d0/

      pi2 = two*pi
      OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/fmadata.dat2')

      READ(IO,*) CUBE,NLEVELS,IFFT,MP
      READ(IO,*) IPFMA,IEPMTA,TETA,NPROC
      READ(IO,*) IDIRECT,IBALANCED,IVERBOSE,IPMON

      CLOSE(IO)
      return
      end
C###############################################################################
C###############################################################################
      
C-------------------------------------------------------------------------------
C     Read additional data for surface gages for IPOST=1 (VOF)
C-------------------------------------------------------------------------------
c      subroutine readgagedata(XYZBOT,XYZGAG,IPOST,NGA)
      subroutine readgagedata()

C-------------------------------------------------------------------------------
      implicit none

C-------------------------------------------------------------------------------
      include 'param.inc'

      include 'datgen.h' ! zl0,w0,h0,x0,d0,mx,my,mz,nx,ny,nz,nsubc,nintr,nom,mom
      include 'maille.h' ! xyznod,xicon,xstart,igcon,iface,isub,intg,lxyz,icumul
c                          ndst,idst,jslim,nelef,nnodf,ielef,inodf,ibcond
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdamp,tdown,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      include 'boundc.h' ! dt,dt2,dt22,tstart,tmax,rho,cpress,cprest,ge,de,
c                          omega,v0h,tolmax,nbs
      include 'interf.h' ! field,lisub,irep,nxi,nyi,nzi,noi,ifield
      include 'dissip.h' ! xdamp,xldamp,dnu0,tresh,sutens,coef,coefx,islop,
c                          npoly,iflab,ibreak,itens
      include 'paddle.h' ! ap,op,sp,zkwave,distf,tfoc,alphaf,wmidth,os,ost,ostt,
c                          damp,dampt,damptt,delfr,nfr,nh,ifreq
      include 'cgage.h'  ! zyzgag,uvwg,phitb,phig,phing,phib,phinb,xyzbot,uvwb,
c                          igag,nga,intzed
      include 'fmapar.h' ! cube,teta,nlevels,ifft,mp,ipfma,iepmta,nproc,idirect,
c                          ibalanced,iverbose,ipmon,inicoulomb,ifreecoulomb
      include 'botfct.h' ! bot,slope,zk,ibot
      include 'subdi.h'  ! almaxb,lsub(nomn)
      include 'option.h' ! isym,ipost,ivect

      integer ilmax,iprint,infie,nht,istart,igrid

      real*8  almaxi,tprint,xmin,dxmin,dymin

      real*8  one,half,two,pi,pdeg,pi2,zero,dalpha,
     .        cel0,zk0,zwl0,zlwave,twave,om,delom
      integer io,k,ig,ifr,ih
      character*8 texte

      data    io/5/,one/1.0/,half/0.5/,texte/'preproce'/,two/2.0/,
     .        pi/3.1415926535898d0/,pdeg/180.d0/,zero/0.d0/

      pi2 = two*pi
      
c      IF (IPOST.EQ.1) THEN
c         IF(NGA.GT.0) THEN
            OPEN(UNIT=IO,STATUS='OLD',FILE='input/gage.dat2')
	       DO IG=1,NGA
	          READ(IO,*) XYZGAG(IG,1),XYZGAG(IG,2)
	          XYZBOT(IG,1) = XYZGAG(IG,1)
	          XYZBOT(IG,2) = XYZGAG(IG,2)
	       END DO
            CLOSE(IO)
c         END IF
c      END IF
      return
      end
C###############################################################################
C###############################################################################

      subroutine SurfaceAndPaddleData(
     .	ilmax,iprint,infie,nht,istart,igrid,almaxi,tprint,xmin,
     .  dxmin,
     .  dymin,dalpha,cel0,zk0,zwl0,zlwave,twave,om,delom,k,ig,ifr,ih		
     .  )

C-------------------------------------------------------------------------------
      implicit none

C-------------------------------------------------------------------------------
      include 'param.inc'

      include 'datgen.h' ! zl0,w0,h0,x0,d0,mx,my,mz,nx,ny,nz,nsubc,nintr,nom,mom
      include 'maille.h' ! xyznod,xicon,xstart,igcon,iface,isub,intg,lxyz,icumul
c                          ndst,idst,jslim,nelef,nnodf,ielef,inodf,ibcond
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdamp,tdown,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      include 'boundc.h' ! dt,dt2,dt22,tstart,tmax,rho,cpress,cprest,ge,de,
c                          omega,v0h,tolmax,nbs
      include 'interf.h' ! field,lisub,irep,nxi,nyi,nzi,noi,ifield
      include 'dissip.h' ! xdamp,xldamp,dnu0,tresh,sutens,coef,coefx,islop,
c                          npoly,iflab,ibreak,itens
      include 'paddle.h' ! ap,op,sp,zkwave,distf,tfoc,alphaf,wmidth,os,ost,ostt,
c                          damp,dampt,damptt,delfr,nfr,nh,ifreq
      include 'cgage.h'  ! zyzgag,uvwg,phitb,phig,phing,phib,phinb,xyzbot,uvwb,
c                          igag,nga,intzed
      include 'fmapar.h' ! cube,teta,nlevels,ifft,mp,ipfma,iepmta,nproc,idirect,
c                          ibalanced,iverbose,ipmon,inicoulomb,ifreecoulomb
      include 'botfct.h' ! bot,slope,zk,ibot
      include 'subdi.h'  ! almaxb,lsub(nomn)
      include 'option.h' ! isym,ipost,ivect
      include 'paddle2.h' ! 

      integer ilmax,iprint,infie,nht,istart,igrid

      real*8  almaxi,tprint,xmin,dxmin,dymin

      real*8  one,half,two,pi,pdeg,pi2,zero,dalpha,
     .        cel0,zk0,zwl0,zlwave,twave,om,delom
      integer io,k,ig,ifr,ih
      character*8 texte

      data    io/5/,one/1.0/,half/0.5/,texte/'preproce'/,two/2.0/,
     .        pi/3.1415926535898d0/,pdeg/180.d0/,zero/0.d0/
      pi2 = two*pi

C-------------------------------------------------------------------------------
C     Read additional data for paddle wavemaker
C-------------------------------------------------------------------------------
c       print*, "dans la routine", IPTYP,NHT  
       

c###########################################################################       
c
c	IPTYP=1 means paddle(s) moves with a sine motion.
c	NHT represents the number of paddles.
c	IFR is the number of frequencies.
c	Warning: SPECIAL PATCH: if NHT=0 the paddle make une sine movement and doesn't read the file
c		the amplitude is 1.
c
c	Ap amplitude for a given paddle.
c	OP pulsation omega for a given paddle.
c	SP relative phase for a given paddle.
c
c###########################################################################       

       if (IPTYP.EQ.1) then
            
	    OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/sinepaddles.dat')
c	nfr is the number of frequencies
	    DO IFR=1,NFR
               AP(1,1) = ONE
               OP(1) = OMEGA
               SP(1) = ZERO
               NH    = 1
c	if nht=0 special patch
               DO IH=1,NHT
                  READ(IO,*) AP(IFR,IH),OP(IFR),SP(IH)
c                  OP(IFR) = OP(IFR)*PI*TWO
               END DO
            END DO
	endif
c	print*,'ap',(AP(1,ih),ih=1,NHT)
c	print*,'op',(OP(ih),ih=1,NHT)
c	print*,'sp',(SP(ih),ih=1,NHT)
        CLOSE(IO)

c###########################################################################       
c
c	IPTYP=7 or 8 11means snake paddle.
c	read the data from input/paddle.dat2
c	DISTF distance between the snakepaddle and the focalisation point
c	TFOC time of the focalisation
c	ALPHAF boundaries of the angle in the geometric focalisation
c		between [-ALPHAF,ALPHAF] with NHT angular component
c		in degrees, not radians
c	WMIDTH ? unused?
c	IFREQ type of directional focusing 0= monochromatic, 1=frequency focusing
c	DELFR frequency spectrum given uniformly between OMEGA-DELFR/2 and OMEGA+DELFR/2
c
c###########################################################################       
C-------------------------------------------------------------------------------
C              Store wave focusing data with SP = thetan
C              NOTE: Adjusting AP to be smaller for larger angle is more real
C                    Also using directional spreading of omega is possible
C                    IFREQ=0 => no spreading, omega=cst
C                    IFREQ=1 => spreading for frequency focusing
C-------------------------------------------------------------------------------

	IF((IPTYP.EQ.8).OR.(IPTYP.EQ.7).OR.(IPTYP.EQ.11)) THEN
            OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/paddle.dat2')
            READ(IO,*) DISTF,TFOC,ALPHAF,WMIDTH,IFREQ,DELFR
            READ(IO,*) Xconv,Yconv,Iconv,Icurved,INN
	    close(io)
c	DALPHA angle between 2 consecutive paddles
	    DALPHA = 0.d0
	    if (NHT .gt. 1) DALPHA = TWO*ALPHAF/(NHT-1)

C-------------------------------------------------------------------------------
C           calculus of the celerity using the dispersion relation in the linear theory
C-------------------------------------------------------------------------------
            CALL CELES(OMEGA,CEL0,ZK0)

            ZWL0 = CEL0*pi2/OMEGA
	    
c 	XB: loop inversion

c	no freq speading
	    OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/sinepaddles.dat')

            IF(IFREQ.EQ.0) THEN
            	DO IFR=1,NFR
                   DO IH=1,NHT
c 		      AP(IFR,IH) = ONE
c		      SP(IH) = ((IH-1)*DALPHA-ALPHAF)*PI/PDEG
                      READ(IO,*) AP(IFR,IH),OP(IFR),SP(IH)
                   END DO
c                   OP(IFR) = OMEGA
   	           ZKWAVE(IFR) = ZK0
		enddo

c	freq spreading

	    else
C-------------------------------------------------------------------------------
C                 Case with varying frequencies, given uniformly
C                 between OMEGA-DELFR/2 and OMEGA+DELFR/2
C                 Given amplitude V0H is the linear sum,
C                 used in PADSIN : here initiated to ONE
C                 -> for comparisons with Hong etal
C-------------------------------------------------------------------------------

c	XB: loop inversion on the frequencies

c	if the number of the differents frequencies is equal to the number of paddles 

		IF(NFR.EQ.NHT) THEN
            	   DO IFR=1,NFR
		      IH = IFR
                      AP(IFR,IH) = ONE
		      SP(IH) = ((IH-1)*DALPHA - ALPHAF)*PI/PDEG
		      ZLWAVE      = ZWL0/DCOS(SP(IH))
		      ZKWAVE(IFR) = PI2/ZLWAVE
		      TWAVE       = DSQRT(PI2*ZLWAVE/(GE*
     .                               DTANH(ZKWAVE(IFR)*H0)))
                      OP(IFR)     = PI2/TWAVE
		   enddo
		endif     

c	if the number of the differents frequencies is NOT equal to the number of paddles 
                IF(NFR.NE.NHT) THEN
            	   DO IFR=1,NFR
                      OM = OMEGA*PI2
                      DELOM = DELFR*PI2
                      OP(IFR) = OM+DELOM*(REAL(IFR-1)/REAL(NFR-1)-HALF)
                      CALL CELES(OP(IFR),CEL0,ZK0)
                      ZKWAVE(IFR) = ZK0
                      DO IH=1,NHT
		         SP(IH) = ((IH-1)*DALPHA - ALPHAF)*PI/PDEG
                         AP(IFR,IH) = ONE/REAL(NFR*NHT)
	              END DO
		   enddo
		END IF


	    endif
            CLOSE(IO)
            NH = NHT
	endif    
c###########################################################################       
          	
c       IF ((IPTYP.EQ.1).OR.(IPTYP.EQ.7).OR.(IPTYP.EQ.8)) THEN
c         IF(NHT.EQ.0) THEN
cC-------------------------------------------------------------------------------
cC           One wave
cC-------------------------------------------------------------------------------
c            AP(1,1) = ONE
c            OP(1) = OMEGA
c            SP(1) = ZERO
c            NH    = 1
c         ELSE
C-------------------------------------------------------------------------------
C        Random wave data in "paddle.data"
C-------------------------------------------------------------------------------
c	       IF(IPTYP.EQ.1) THEN
c            OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/sinewaves.dat')
c               DO IFR=1,NFR
c                  DO IH=1,NHT
c                     READ(IO,*) AP(IFR,IH),OP(IFR),SP(IH)
c                     OP(IFR) = OP(IFR)*PI*TWO
c                  END DO
c               END DO
c	       ELSE IF((IPTYP.EQ.8).OR.(IPTYP.EQ.7)) THEN
cC-------------------------------------------------------------------------------
cC              Store wave focusing data with SP = thetan
cC              NOTE: Adjusting AP to be smaller for larger angle is more real
cC                    Also using directional spreading of omega is possible
cC                    IFREQ=0 => no spreading, omega=cst
cC                    IFREQ=1 => spreading for frequency focusing
cC-------------------------------------------------------------------------------
c             OPEN(UNIT=IO,STATUS='UNKNOWN',FILE='input/paddle.dat2')
c              READ(IO,*) DISTF,TFOC,ALPHAF,WMIDTH,IFREQ,DELFR
c	          DALPHA = TWO*ALPHAF/(NHT-1)
cC-------------------------------------------------------------------------------
cC           Linear theory results
cC-------------------------------------------------------------------------------
c               CALL CELES(OMEGA,CEL0,ZK0)
c
c               ZWL0 = CEL0*pi2/OMEGA
c               DO IFR=1,NFR
c                  IF(IFREQ.EQ.0) THEN
c                     DO IH=1,NHT
c 		              AP(IFR,IH) = ONE
c		              SP(IH) = ((IH-1)*DALPHA-ALPHAF)*PI/PDEG
c                     END DO
c                     OP(IFR) = OMEGA
c   	                ZKWAVE(IFR) = ZK0
c		        ELSE IF(IFREQ.EQ.1.AND.NFR.EQ.NHT) THEN
c		           IH = IFR
c                     AP(IFR,IH) = ONE
c		           SP(IH) = ((IH-1)*DALPHA - ALPHAF)*PI/PDEG
c		           ZLWAVE      = ZWL0/DCOS(SP(IH))
c		           ZKWAVE(IFR) = PI2/ZLWAVE
c		           TWAVE       = DSQRT(PI2*ZLWAVE/(GE*
c     .                                   DTANH(ZKWAVE(IFR)*H0)))
c                     OP(IFR)     = PI2/TWAVE
c                  ELSE IF(IFREQ.EQ.1.AND.NFR.NE.NHT) THEN
cC-------------------------------------------------------------------------------
cC                 Case with varying frequencies, given uniformly
cC                 between OMEGA-DELFR/2 and OMEGA+DELFR/2
cC                 Given amplitude V0H is the linear sum,
cC                 used in PADSIN : here initiated to ONE
cC                 -> for comparisons with Hong etal
cC-------------------------------------------------------------------------------
c                     OM = OMEGA*PI2
c                     DELOM = DELFR*PI2
c                     OP(IFR) = OM+DELOM*(REAL(IFR-1)/REAL(NFR-1)-HALF)
c                     CALL CELES(OP(IFR),CEL0,ZK0)
c                     ZKWAVE(IFR) = ZK0
c                     DO IH=1,NHT
c		              SP(IH) = ((IH-1)*DALPHA - ALPHAF)*PI/PDEG
c                        AP(IFR,IH) = ONE/REAL(NFR*NHT)
c	                END DO
c		        END IF
c               END DO
c	       END IF
c            CLOSE(IO)
c            NH = NHT
c         END IF
c       END IF
       return
       end

	
