C
C                       GAGE ROUTINES FOR INTERIOR POINTS
C
C-----------------------------------------------------------------------
      SUBROUTINE GAGE(PHI,PHIN)
C-----------------------------------------------------------------------
C
C     Find surface fields at specified wave gages
C     by interpolating in a MII grid. (xi,et) are found using Newton-
C     Raphson iterations.
C
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

      LOGICAL*4 INIE
C
      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
c      COMMON /SYSTEM/ DUM2(24),TIME,DUM3(6+8*MYMAX),IO6,IFLAG,IDUM4(3)
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),DUM4(18),
     .                IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),IDUM5(2*MOMM+2*NOMM+6),NDST(6,2),
     .                IDST(6,3),JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),
     .                INODF(6,2),IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,IDUM8(3)
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),DUM5(13)
      COMMON /CGAGE/  XYZGAG(NGAM,3),UVWG(NGAM,3),PHITB(NGAM),
     .                PHIG(NGAM),PHING(NGAM),PHIB(NGAM),PHINB(NGAM),
     .                XYZBOT(NGAM,3),UVWB(NGAM,3),IGAG(NGAM),NGA,INTZED
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      DIMENSION PHI(NOM),PHIN(NOM),FSFXI(NDM),FSDXI(NDM),
     .          FSFET(NDM),FSDET(NDM),ZMAT(2,2),ZP(2)
C
      DATA ONE/1.D0/,TWTHI/0.6666666666666667D0/,ZERO/0.D0/,ITEMAX/20/,
     .     BIG/1.D10/,EPS/1.D-06/,OTHREE/0.3333333333333333D0/
C
C     SAVE
C     SAVE /DATGEN/,/SYSTEM/,MAILLE/,/DELEM1/,/DELEM2/,/FUNCTN/,/REGRID/
C
C.....Initialization
C
      NNODE = MNOD
      DO IG=1,NGA
         IGAG(IG) = 0
      END DO
C
C.....Find which element contains new nodes and regrid z,phi,phin
C
      NOMSA = NOM
      NOM   = 2
      IFLAG = 1
C
      DO IE=1,NELEF(1)
C
C        4-node and MII local connectivity
C
         ILOC = 0
C
         DO J=1,NNODE
            NODE2(J)    = ILCON(IE,J)
            XYZLO2(J,1) = XYZNOD(NODE2(J),1)
            XYZLO2(J,2) = XYZNOD(NODE2(J),2)
C
            DO I=1,NNODE
               ILOC = ILOC + 1
               NODE1(ILOC)    = IGCON(IE,I,J)
               XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
               XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
               XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYM
	       IF (ISYM.EQ.1) THEN
                 NSIDE = IFACE(IE)
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.6.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYS
            END DO
         END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
         DO IG=1,NGA
            XG = XYZGAG(IG,1)
            YG = XYZGAG(IG,2)
C
C           Check if xn,yn is within element IE (4-nodes) (x,y)_j
C
            INIE = .FALSE.
	    IF(IGAG(IG).EQ.0) THEN
               CALL POLYIN2(INIE,XG,YG)
C              -----------
            END IF
	    IF(INIE) THEN
               WRITE(IO6,*) IE,IG,XG,YG
C
C              Regrid xn,yn with MII element IE, ie find z,phi,phin
C              using Newton-Raphson method
C
C              Estimate of xio,etao by bilinear interpolation
C
               XI0   = ZERO
	       ET0   = ZERO
	       ZP(1) = BIG
	       ZP(2) = BIG
	       ITER  = 0
C
	       DO WHILE((DABS(ZP(1)).GT.EPS.OR.DABS(ZP(2)).GT.EPS).
     .                  AND.ITER.LE.ITEMAX)
C
C                 One dimensional 4-node shape functions
C
                  CHI = (XI0 + ONE)*OTHREE + EFJXI
                  CALL SHAPFN(CHI,FSFXI,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDXI,NNODE)
C                 -----------
                  CHI = (ET0 + ONE)*OTHREE + EFJET
                  CALL SHAPFN(CHI,FSFET,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDET,NNODE)
C                 -----------
C
C                 Build N-R system with bidim. shape functions
C
                  ZMAT(1,1) = ZERO
		  ZMAT(1,2) = ZERO
		  ZMAT(2,1) = ZERO
		  ZMAT(2,2) = ZERO
		  ZP(1)     = -XG
		  ZP(2)     = -YG
                  ILOC = 0
C
                  DO J=1,NNODE
                     DO I=1,NNODE
                        ILOC = ILOC + 1
C
                        FI(ILOC)   = FSFXI(I)*FSFET(J)
                        DFIX(ILOC) = FSDXI(I)*FSFET(J)*OTHREE
                        DFIE(ILOC) = FSFXI(I)*FSDET(J)*OTHREE
C
			ZMAT(1,1)  = ZMAT(1,1)-DFIX(ILOC)*XYZLO1(ILOC,1)
			ZMAT(1,2)  = ZMAT(1,2)-DFIE(ILOC)*XYZLO1(ILOC,1)
			ZMAT(2,1)  = ZMAT(2,1)-DFIX(ILOC)*XYZLO1(ILOC,2)
			ZMAT(2,2)  = ZMAT(2,2)-DFIE(ILOC)*XYZLO1(ILOC,2)
			ZP(1)      = ZP(1) + FI(ILOC)*XYZLO1(ILOC,1)
			ZP(2)      = ZP(2) + FI(ILOC)*XYZLO1(ILOC,2)
                     END DO
                  END DO
C
C                 Solve N-R system
C
                  CALL SOLVE(ZMAT,ZP,2)
C                 ----------
		  ITER = ITER + 1
		  XI0  = XI0 + ZP(1)
		  ET0  = ET0 + ZP(2)
               END DO
C
C              Calculate gage parameters for converged (xi,et)
C
               CHI = (XI0 + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSFXI,NNODE)
C              -----------
               CHI = (ET0 + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSFET,NNODE)
C              -----------
               PHIG(IG)      = ZERO
               PHING(IG)     = ZERO
	       XYZGAG(IG,3)  = ZERO
               ILOC          = 0
C
               DO J=1,NNODE
                  DO I=1,NNODE
                     ILOC         = ILOC + 1
                     FI(ILOC)     = FSFXI(I)*FSFET(J)
                     PHIG(IG)     = PHIG(IG) +
     .                                FI(ILOC)*PHI(NODE1(ILOC))
                     PHING(IG)    = PHING(IG) +
     .                                FI(ILOC)*PHIN(NODE1(ILOC))
                     XYZGAG(IG,3) = XYZGAG(IG,3) +
     .                                FI(ILOC)*XYZLO1(ILOC,3)
                  END DO
               END DO
C
	       IGAG(IG) = 1
	    END IF
	 END DO
      END DO
C
C.....Restore dimension for SOLVE
C
      NOM = NOMSA
C
C.....Write gage coordinates and values
C
      WRITE(IO6,*) 'Gages : I,X,Y,Z,PHI,PHIN :'
      DO IG=1,NGA
         WRITE(IO6,2000) IG,(XYZGAG(IG,J),J=1,3),PHIG(IG),PHING(IG)
         WRITE(39+IG,2005) TIME,(XYZGAG(IG,J),J=1,3),PHIG(IG),PHING(IG)
      END DO
C
      RETURN
C
 2000 FORMAT(I5,5D16.8)
 2005 FORMAT(6E16.8)
 2010 FORMAT(5I5,2F10.5)
 2020 FORMAT(17I5)
C
      END
C-------------------------------------------------------------------------
      SUBROUTINE DISTPI2(IO1)
C-------------------------------------------------------------------------
C
C     Determine the interior points among those occupying the whole domain
C     -Note accelerate POLYIN for interior points with same (x,y) !
C     -Do surface and bottom intersections like in regrid
C
C-------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

      LOGICAL*4 INIE
C
      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,NX,NY,NZ,IDUM1(2),NOM,IDUM2
c      COMMON /SYSTEM/ DUM11(24),TIME,TDAMP,DUM12(5+8*MYMAX),IO6,IFLAGS,
c     .                IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),DUM3(18),
     .                IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IDUM5(MOMM*2),INTG(MOMM),LXYZ(NOMM,2),
     .                IDUM6(54),IELEF(6,2),INODF(6,2),IDUM7(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,IDUM8(2)
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /FUNCTN/ DUM4(3*MNODM+9),ZNP(3),ZJAC
      COMMON /INTERF/ FIELD(11,NOIM),LISUB(NOIM),IREP(NOIM),
     .                NXI,NYI,NZI,NOI,IFIELD
      REAL*8  FATINT(LSAVEF)
C
      DIMENSION XCIEB(3),XCIEF(3),XCIEBP(3),XCIEFP(3),ZNPP(3)
C
      DATA BIG/1.D+10/,EPS/1.D-03/,QUART/0.25D0/,ZERO/0.D0/
C
C     InitialisationS
C
      NNODE = MNOD
      IXYP  = 0
      IEFP  = 0
      IEBP  = 0
      XJP   = BIG
      YJP   = BIG
      ICST  = 7+3*NNODE*NNODE
C
C     Loop over the interior points
C
      DO JOI=1,NOI
C
C        First considered as exterior points
C
         IREP(JOI) = 0
C
C        Internal point horiz. coordinates
C
         XJ = FIELD(1,JOI)
	 YJ = FIELD(2,JOI)
C
         IF((DABS(XJ-XJP).LT.EPS).AND.(DABS(YJ-YJP).LT.EPS)) THEN
	    IXYP = 1
	    IEF  = IEFP
	    IEB  = IEBP
C
	    DO J=1,3
	       ZNP(J)   = ZNPP(J)
               XCIEF(J) = XCIEFP(J)
               XCIEB(J) = XCIEBP(J)
	    END DO
	 END IF
C
         IF(IXYP.EQ.0) THEN
C
C           Loop over the elements on the free surface
C
            INIE = .FALSE.
            IE   = IELEF(1,1)
            DO WHILE((IE.LE.IELEF(1,2)).AND.(.NOT.INIE))
               DO J=1,NNODE
                  NODE2(J)    = ILCON(IE,J)
                  XYZLO2(J,1) = XYZNOD(NODE2(J),1)
                  XYZLO2(J,2) = XYZNOD(NODE2(J),2)
                  XYZLO2(J,3) = XYZNOD(NODE2(J),3)
               END DO
C
               CALL POLYIN2(INIE,XJ,YJ)
C              -----------
               IE = IE + 1
	    END DO
C
C           If the interior point is inside the element
C           There could be several elements satisfying the condition
C
            IEF  = 0
	    IEFP = 0
            IF(INIE) THEN
               OPEN(UNIT=IO1,STATUS='UNKNOWN',FORM='UNFORMATTED',
     .                                        FILE='output/save')
  	          IEF  = IE - 1
	          IEFP = IEF

               NINTR  = INTG(IEF)
               NINTR2 = NINTR*NINTR
               LREC   = NINTR2*ICST

               READ(IO1,REC=IEF) (FATINT(I),I=1,LREC)
C-------------------------------------------------------------------------------
C              Central Gauss point of the element
C-------------------------------------------------------------------------------
               IF(MOD(NINTR2,2).EQ.0) THEN
                  NIN2 = NINTR2/2
               ELSE
                  NIN2 = (NINTR2 + 1)/2
               END IF
C-------------------------------------------------------------------------------
C              Normal vector for the center of the element
C-------------------------------------------------------------------------------
               IPCOR = NIN2*ICST
	          DO J=1,3
                  ZNP(J)  = FATINT(IPCOR+4+J)
	             ZNPP(J) = ZNP(J)
	          END DO
C-------------------------------------------------------------------------------
C              Coordinates of the element center
C-------------------------------------------------------------------------------
               DO J=1,3
                  XCIEF(J) = ZERO
                  DO I=1,NNODE
                     XCIEF(J) = XCIEF(J) + XYZLO2(I,J)
                  END DO
                  XCIEF(J)  = QUART*XCIEF(J)
		        XCIEFP(J) = XCIEF(J)
               END DO
               CLOSE(IO1)
	       END IF
	 END IF

	 IF(IEF.NE.0) THEN
C-------------------------------------------------------------------------------
C           Distance interior point-element center
C-------------------------------------------------------------------------------
            DISTA = ZERO
            DO J=1,3
               DISTA = DISTA + (FIELD(J,JOI) - XCIEF(J))**2
            END DO
            DISTA = DSQRT(DISTA)
C
C           Scalar product to determine the orientation
C
            DISTCI = ZERO
            DO J=1,3
               COMPOS = (FIELD(J,JOI) - XCIEF(J))/DISTA
               DISTCI = DISTCI + COMPOS*ZNP(J)
            END DO
C
C           Flag for the interior points
C           If the scalar product is negative --> interior point 1
C           ________________________ positive --> exterior point 0
C
            IF(DISTCI.LT.-EPS) THEN
               IREP(JOI) = 1
            ELSE
               IREP(JOI) = 0
            END IF
         END IF
C
C        Loop over the elements on the bottom
C
         IF(IXYP.EQ.0) THEN
            INIE = .FALSE.
            IE = IELEF(6,1)
            DO WHILE((IE.LE.IELEF(6,2)).AND.(.NOT.INIE))
               DO J=1,NNODE
                  NODE2(J)    = ILCON(IE,J)
                  XYZLO2(J,1) = XYZNOD(NODE2(J),1)
                  XYZLO2(J,2) = XYZNOD(NODE2(J),2)
                  XYZLO2(J,3) = XYZNOD(NODE2(J),3)
               END DO
C
               CALL POLYIN2(INIE,XJ,YJ)
C              -----------
               IE = IE + 1
	    END DO
C
C           If the interior point is inside the element
C           Assume the bottom surface is single-valued
C
            IEB  = 0
	    IEBP = 0
            IF(INIE) THEN
	       IEB  = IE - 1
	       IEBP = IEB
C
C              Coordinates of the element center
C
               DO J=1,3
                  XCIEB(J) = ZERO
                  DO I=1,NNODE
                     XCIEB(J) = XCIEB(J) + XYZLO2(I,J)
                  END DO
                  XCIEB(J)  = QUART*XCIEB(J)
		  XCIEBP(J) = XCIEB(J)
               END DO
	    END IF
	 END IF

	 IF(IEB.NE.0) THEN
C
C           Interior point if not below the bottom surface
C
            IF(FIELD(3,JOI).LE.XCIEB(3)) THEN
	       IREP(JOI) = 0
	    END IF
	 END IF
      END DO
C
      RETURN
C
 2000 FORMAT(I5,5F10.5)
 2010 FORMAT(5I5,2F10.5)
 2020 FORMAT(17I5)
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE GAGEVOF(UVW,PHI,PHIN,IO71)
C-----------------------------------------------------------------------
C
C     Find surface fields (z,u,v,w) at specified wave gages
C     by interpolating in a MII grid. (xi,et) are found using Newton-
C     Raphson iterations.
C
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

      LOGICAL*4 INIE
C
      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),DUM4(18),
     .                IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),IDUM5(2*MOMM+2*NOMM+42),NELEF(6),
     .                NNODF(6),IELEF(6,2),INODF(6,2),IDUM6(6)
c      COMMON /SYSTEM/ DUM11(24),TIME,TDAMP,DUM12(5+8*MYMAX),IO6,IFLAGS,
c     .                IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,IDUM8(3)
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),DUM5(9),
     .                ZN(3),ZJAC
      COMMON /TANGET/ SMNVEC(3,3)
      COMMON /CGAGE/  XYZGAG(NGAM,3),UVWG(NGAM,3),PHITB(NGAM),
     .                PHIG(NGAM),PHING(NGAM),PHIB(NGAM),PHINB(NGAM),
     .                XYZBOT(NGAM,3),UVWB(NGAM,3),IGAG(NGAM),NGA,INTZED
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION UVW(3,NOM),PHI(NOM),PHIN(NOM),FSFXI(NDM),FSDXI(NDM),
     .          FSFET(NDM),FSDET(NDM),ZMAT(2,2),ZP(2)
C
      DATA ONE/1.D0/,TWTHI/0.6666666666666667D0/,ZERO/0.D0/,ITEMAX/20/,
     .     BIG/1.D10/,EPS/1.D-06/,OTHREE/0.3333333333333333D0/
C
C     SAVE
C     SAVE /DATGEN/,/SYSTEM/,MAILLE/,/DELEM1/,/DELEM2/,/FUNCTN/,/REGRID/
C
C.....Initialization
C
      NNODE = MNOD
      DO IG=1,NGA
         IGAG(IG) = 0
      END DO
C
C.....Find which element contains new nodes and regrid z,phi,phin
C
      NOMSA = NOM
      NOM   = 2
      IFLAG = 1
C
      DO IE=1,NELEF(1)
C
C        4-node and MII local connectivity
C
         ILOC = 0
C
         DO J=1,NNODE
            NODE2(J)    = ILCON(IE,J)
            XYZLO2(J,1) = XYZNOD(NODE2(J),1)
            XYZLO2(J,2) = XYZNOD(NODE2(J),2)
C
            DO I=1,NNODE
               ILOC = ILOC + 1
               NODE1(ILOC)    = IGCON(IE,I,J)
               XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
               XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
               XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYM
	       IF (ISYM.EQ.1) THEN
                 NSIDE = IFACE(IE)
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.6.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYS
            END DO
         END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
         DO IG=1,NGA
            XG = XYZGAG(IG,1)
            YG = XYZGAG(IG,2)
C
C           Check if xn,yn is within element IE (4-nodes) (x,y)_j
C
            INIE = .FALSE.
	    IF(IGAG(IG).EQ.0) THEN
               CALL POLYIN2(INIE,XG,YG)
C              -----------
            END IF
	    IF(INIE) THEN
C               WRITE(IO6,*) IE,IG,XG,YG
C
C              Regrid xn,yn with MII element IE, ie find z,phi,phin
C              using Newton-Raphson method
C
C              Estimate of xio,etao by bilinear interpolation
C
               XI0   = ZERO
	       ET0   = ZERO
	       ZP(1) = BIG
	       ZP(2) = BIG
	       ITER  = 0
C
	       DO WHILE((DABS(ZP(1)).GT.EPS.OR.DABS(ZP(2)).GT.EPS).
     .                  AND.ITER.LE.ITEMAX)
C
C                 One dimensional 4-node shape functions
C
                  CHI = (XI0 + ONE)*OTHREE + EFJXI
                  CALL SHAPFN(CHI,FSFXI,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDXI,NNODE)
C                 -----------
                  CHI = (ET0 + ONE)*OTHREE + EFJET
                  CALL SHAPFN(CHI,FSFET,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDET,NNODE)
C                 -----------
C
C                 Build N-R system with bidim. shape functions
C
                  ZMAT(1,1) = ZERO
		  ZMAT(1,2) = ZERO
		  ZMAT(2,1) = ZERO
		  ZMAT(2,2) = ZERO
		  ZP(1)     = -XG
		  ZP(2)     = -YG
                  ILOC = 0
C
                  DO J=1,NNODE
                     DO I=1,NNODE
                        ILOC = ILOC + 1
C
                        FI(ILOC)   = FSFXI(I)*FSFET(J)
                        DFIX(ILOC) = FSDXI(I)*FSFET(J)*OTHREE
                        DFIE(ILOC) = FSFXI(I)*FSDET(J)*OTHREE
C
			ZMAT(1,1)  = ZMAT(1,1)-DFIX(ILOC)*XYZLO1(ILOC,1)
			ZMAT(1,2)  = ZMAT(1,2)-DFIE(ILOC)*XYZLO1(ILOC,1)
			ZMAT(2,1)  = ZMAT(2,1)-DFIX(ILOC)*XYZLO1(ILOC,2)
			ZMAT(2,2)  = ZMAT(2,2)-DFIE(ILOC)*XYZLO1(ILOC,2)
			ZP(1)      = ZP(1) + FI(ILOC)*XYZLO1(ILOC,1)
			ZP(2)      = ZP(2) + FI(ILOC)*XYZLO1(ILOC,2)
                     END DO
                  END DO
C
C                 Solve N-R system
C
                  CALL SOLVE(ZMAT,ZP,2)
C                 ----------
		  ITER = ITER + 1
		  XI0  = XI0 + ZP(1)
		  ET0  = ET0 + ZP(2)
C                  WRITE(IO6,*) ITER,XI0,ET0
               END DO
C
C              Calculate gage parameters for converged (xi,et)
C
               CHI = (XI0 + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSFXI,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSDXI,NNODE)
C              -----------
               CHI = (ET0 + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSFET,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSDET,NNODE)
C              -----------
C
               PHIG(IG)      = ZERO
               PHING(IG)     = ZERO
	       XYZGAG(IG,3)  = ZERO
	       PHIS          = ZERO
	       PHIM          = ZERO
C              DO K=1,3
C                 UVWG(IG,K) = ZERO
C	       END DO
               ILOC          = 0
C
               DO J=1,NNODE
                  DO I=1,NNODE
                     ILOC         = ILOC + 1
                     FI(ILOC)     = FSFXI(I)*FSFET(J)
                     XYZGAG(IG,3) = XYZGAG(IG,3) +
     .                              FI(ILOC)*XYZLO1(ILOC,3)
                     PHIG(IG)     = PHIG(IG) +
     .                              FI(ILOC)*PHI(NODE1(ILOC))
                     PHING(IG)    = PHING(IG) +
     .                              FI(ILOC)*PHIN(NODE1(ILOC))
C
                     DFIX(ILOC)   = FSDXI(I)*FSFET(J)*OTHREE
                     DFIE(ILOC)   = FSFXI(I)*FSDET(J)*OTHREE
		     PHIS         = PHIS + DFIX(ILOC)*PHI(NODE1(ILOC))
		     PHIM         = PHIM + DFIE(ILOC)*PHI(NODE1(ILOC))
C                    DO K=1,3
C                       UVWG(IG,K) = UVWG(IG,K) +
C     .                               FI(ILOC)*UVW(K,NODE1(ILOC))
C                    END DO
		  END DO
	       END DO
C
C              Compute tangential unit vectors s and m at converged point
C              CARAC2 uses Fi,DFIX and DFIE previously computed
C
               CALL CARAC2
C              -----------
               DVS = ZERO
	       DVM = ZERO
	       DO K=1,3
	          DVS = DVS + SMNVEC(1,K)*SMNVEC(1,K)
	          DVM = DVM + SMNVEC(2,K)*SMNVEC(2,K)
	       END DO
	       DVS = DSQRT(DVS)
	       DVM = DSQRT(DVM)
	       DO K=1,3
	          SMNVEC(1,K) = SMNVEC(1,K)/DVS
		  SMNVEC(2,K) = SMNVEC(2,K)/DVM
	       END DO
C
C              Compute tangential velocities at converged point
C
	       PHIS = PHIS/DVS
	       PHIM = PHIM/DVM
C
C              Cartesian velocities (u,v,w)
C
	       DO K=1,3
	          UVWG(IG,K) = PHIS*SMNVEC(1,K) + PHIM*SMNVEC(2,K)
     .                         + PHING(IG)*ZN(K)
               END DO
C
	       IGAG(IG) = 1
	    END IF
	 END DO
      END DO
C
C.....Restore dimension for SOLVE
C
      NOM = NOMSA
C
C.....Write gage coordinates and values
C
C      WRITE(IO6,*) 'Surface gages : I,X,Y,Z,U,V,W :'
      DO IG=1,NGA
C         WRITE(IO6,2000) IG,(XYZGAG(IG,J),J=1,3),(UVWG(IG,K),K=1,3)
         WRITE(IO71,2005) (XYZGAG(IG,J),J=1,3),(UVWG(IG,K),K=1,3)
      END DO
C
      RETURN
C
 2000 FORMAT(I5,6D16.8)
 2005 FORMAT(6E16.8)
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE BOTTOM(UVW,PHI,PHIN,PHIT,IO72)
C-----------------------------------------------------------------------
C
C     Find surface fields (z,u,v,w) at specified wave gages
C     by interpolating in a MII grid. (xi,et) are found using Newton-
C     Raphson iterations.
C
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

      LOGICAL*4 INIE
C
      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON /BOUNDC/ DUM6(5),RHO,DUM7(2),GE,DUM8(5),IDUM9
c      COMMON /SYSTEM/ DUM9(24),TIME,TDAMP,DUM10(5+8*MYMAX),IO6,IFLAGS,
c     .                IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),DUM4(18),
     .                IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),IDUM5(2*MOMM+2*NOMM+42),NELEF(6),
     .                NNODF(6),IELEF(6,2),INODF(6,2),IDUM6(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,IDUM8(3)
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),DUM5(9),
     .                ZN(3),ZJAC
      COMMON /TANGET/ SMNVEC(3,3)
      COMMON /CGAGE/  XYZGAG(NGAM,3),UVWG(NGAM,3),PHITB(NGAM),
     .                PHIG(NGAM),PHING(NGAM),PHIB(NGAM),PHINB(NGAM),
     .                XYZBOT(NGAM,3),UVWB(NGAM,3),IGAG(NGAM),NGA,INTZED
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION UVW(3,NOM),PHI(NOM),PHIN(NOM),PHIT(NOM),FSFXI(NDM),
     .          FSDXI(NDM),FSFET(NDM),FSDET(NDM),ZMAT(2,2),ZP(2)
C
      DATA ONE/1.D0/,TWTHI/0.6666666666666667D0/,ZERO/0.D0/,ITEMAX/20/,
     .     BIG/1.D10/,EPS/1.D-06/,OTHREE/0.3333333333333333D0/,
     .     HALF/0.5D0/
C
C     SAVE
C     SAVE /DATGEN/,/SYSTEM/,MAILLE/,/DELEM1/,/DELEM2/,/FUNCTN/,/REGRID/
C
C.....Initialization
C
      NNODE = MNOD
      DO IG=1,NGA
         IGAG(IG) = 0
      END DO
C
C.....Find which element contains new nodes and regrid z,phi,phin
C
      NOMSA = NOM
      NOM   = 2
      IFLAG = 1
C
      DO IE=MOM-NELEF(6),MOM
C
C        4-node and MII local connectivity
C
         ILOC = 0
C
         DO J=1,NNODE
            NODE2(J)    = ILCON(IE,J)
            XYZLO2(J,1) = XYZNOD(NODE2(J),1)
            XYZLO2(J,2) = XYZNOD(NODE2(J),2)
C
            DO I=1,NNODE
               ILOC = ILOC + 1
               NODE1(ILOC)    = IGCON(IE,I,J)
               XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
               XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
               XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYM
	       IF (ISYM.EQ.1) THEN
                 NSIDE = IFACE(IE)
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.6.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYS
            END DO
         END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
         DO IG=1,NGA
            XG = XYZBOT(IG,1)
            YG = XYZBOT(IG,2)
C
C           Check if xn,yn is within element IE (4-nodes) (x,y)_j
C
            INIE = .FALSE.
	    IF(IGAG(IG).EQ.0) THEN
               CALL POLYIN2(INIE,XG,YG)
C              -----------
            END IF
	    IF((INIE).AND.(IE.NE.MOM-NELEF(6))) THEN
C
C              Regrid xn,yn with MII element IE, ie find z,phi,phin
C              using Newton-Raphson method
C
C              Estimate of xio,etao by bilinear interpolation
C
               XI0   = ZERO
	       ET0   = ZERO
	       ZP(1) = BIG
	       ZP(2) = BIG
	       ITER  = 0
C
	       DO WHILE((DABS(ZP(1)).GT.EPS.OR.DABS(ZP(2)).GT.EPS).
     .                  AND.ITER.LE.ITEMAX)
C
C                 One dimensional 4-node shape functions
C
                  CHI = (XI0 + ONE)*OTHREE + EFJXI
                  CALL SHAPFN(CHI,FSFXI,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDXI,NNODE)
C                 -----------
                  CHI = (ET0 + ONE)*OTHREE + EFJET
                  CALL SHAPFN(CHI,FSFET,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDET,NNODE)
C                 -----------
C
C                 Build N-R system with bidim. shape functions
C
                  ZMAT(1,1) = ZERO
		  ZMAT(1,2) = ZERO
		  ZMAT(2,1) = ZERO
		  ZMAT(2,2) = ZERO
		  ZP(1)     = -XG
		  ZP(2)     = -YG
                  ILOC = 0
C
                  DO J=1,NNODE
                     DO I=1,NNODE
                        ILOC = ILOC + 1
C
                        FI(ILOC)   = FSFXI(I)*FSFET(J)
                        DFIX(ILOC) = FSDXI(I)*FSFET(J)*OTHREE
                        DFIE(ILOC) = FSFXI(I)*FSDET(J)*OTHREE
C
			ZMAT(1,1)  = ZMAT(1,1)-DFIX(ILOC)*XYZLO1(ILOC,1)
			ZMAT(1,2)  = ZMAT(1,2)-DFIE(ILOC)*XYZLO1(ILOC,1)
			ZMAT(2,1)  = ZMAT(2,1)-DFIX(ILOC)*XYZLO1(ILOC,2)
			ZMAT(2,2)  = ZMAT(2,2)-DFIE(ILOC)*XYZLO1(ILOC,2)
			ZP(1)      = ZP(1) + FI(ILOC)*XYZLO1(ILOC,1)
			ZP(2)      = ZP(2) + FI(ILOC)*XYZLO1(ILOC,2)
                     END DO
                  END DO
C
C                 Solve N-R system
C
                  CALL SOLVE(ZMAT,ZP,2)
C                 ----------
		  ITER = ITER + 1
		  XI0  = XI0 + ZP(1)
		  ET0  = ET0 + ZP(2)
C                  WRITE(IO6,*) ITER,XI0,ET0
               END DO
C
C              Calculate gage parameters for converged (xi,et)
C
               CHI = (XI0 + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSFXI,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSDXI,NNODE)
C              -----------
               CHI = (ET0 + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSFET,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSDET,NNODE)
C              -----------
	       XYZBOT(IG,3)  = ZERO
               PHIB(IG)      = ZERO
               PHINB(IG)     = ZERO
	       PHITB(IG)     = ZERO
	       PHIS          = ZERO
	       PHIM          = ZERO
C              DO K=1,3
C                 UVWB(IG,K) = ZERO
C	       END DO
               ILOC          = 0
C
               DO J=1,NNODE
                  DO I=1,NNODE
                     ILOC         = ILOC + 1
                     FI(ILOC)     = FSFXI(I)*FSFET(J)
                     XYZBOT(IG,3) = XYZBOT(IG,3) +
     .                              FI(ILOC)*XYZLO1(ILOC,3)
                     PHIB(IG)     = PHIB(IG) +
     .                              FI(ILOC)*PHI(NODE1(ILOC))
                     PHINB(IG)    = PHINB(IG) +
     .                              FI(ILOC)*PHIN(NODE1(ILOC))

                     PHITB(IG)    = PHITB(IG) +
     .                              FI(ILOC)*PHIT(NODE1(ILOC))
C
                     DFIX(ILOC)   = FSDXI(I)*FSFET(J)*OTHREE
                     DFIE(ILOC)   = FSFXI(I)*FSDET(J)*OTHREE
		     PHIS         = PHIS + DFIX(ILOC)*PHI(NODE1(ILOC))
		     PHIM         = PHIM + DFIE(ILOC)*PHI(NODE1(ILOC))
C		     DO K=1,3
C                       UVWB(IG,K) = UVWB(IG,K) +
C     .                               FI(ILOC)*UVW(K,NODE1(ILOC))
C                    END DO
                  END DO
               END DO
C
C              Compute tangential unit vectors s and m at converged point
C              CARAC2 uses Fi,DFIX and DFIE previously computed
C
               CALL CARAC2
C              -----------
               DVS = ZERO
	       DVM = ZERO
	       DO K=1,3
	          DVS = DVS + SMNVEC(1,K)*SMNVEC(1,K)
	          DVM = DVM + SMNVEC(2,K)*SMNVEC(2,K)
	       END DO
	       DVS = DSQRT(DVS)
	       DVM = DSQRT(DVM)
	       DO K=1,3
	          SMNVEC(1,K) = SMNVEC(1,K)/DVS
		  SMNVEC(2,K) = SMNVEC(2,K)/DVM
	       END DO
C
C              Compute tangential velocities at converged point
C
	       PHIS = PHIS/DVS
	       PHIM = PHIM/DVM
C
C              Cartesian velocities (u,v,w)
C
	       DO K=1,3
	          UVWB(IG,K) = PHIS*SMNVEC(1,K) + PHIM*SMNVEC(2,K)
     .                         + PHINB(IG)*ZN(K)
               END DO
C
	       IGAG(IG) = 1
	    END IF
	 END DO
      END DO
C
C.....Restore dimension for SOLVE
C
      NOM = NOMSA
C
C.....Write gage coordinates and values
C
      WRITE(IO6,*) 'Bottom gages : I,X,Y,Z,U,V,W :'
      DO IG=1,NGA
         PBOT = -RHO*(PHITB(IG) + GE*XYZBOT(IG,3) +
     .        HALF*(UVWB(IG,1)**2 + UVWB(IG,2)**2 + UVWB(IG,3)**2))
         WRITE(IO6,2000) IG,(XYZBOT(IG,J),J=1,3),(UVWB(IG,K),K=1,3),
     .                   PBOT
         WRITE(IO72,2005) (XYZBOT(IG,J),J=1,3),(UVWB(IG,K),K=1,3),
     .                   PBOT
      END DO
C
      RETURN
C
 2000 FORMAT(I5,7D16.8)
 2005 FORMAT(7E16.8)
C
      END
C
      SUBROUTINE GAGE_FB(PHI,PHIN,IO71)
C-----------------------------------------------------------------------
C
C     Find surface fields at specified wave gages
C     by interpolating in a MII grid. (xi,et) are found using Newton-
C     Raphson iterations.
C
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

      LOGICAL*4 INIE
C
      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
c      COMMON /SYSTEM/ DUM2(24),TIME,DUM3(6+8*MYMAX),IO6,IFLAG,IDUM4(3)
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),DUM4(18),
     .                IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),IDUM5(2*MOMM+2*NOMM+6),NDST(6,2),
     .                IDST(6,3),JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),
     .                INODF(6,2),IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,IDUM8(3)
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),DUM5(13)
      COMMON /CGAGE/  XYZGAG(NGAM,3),UVWG(NGAM,3),PHITB(NGAM),
     .                PHIG(NGAM),PHING(NGAM),PHIB(NGAM),PHINB(NGAM),
     .                XYZBOT(NGAM,3),UVWB(NGAM,3),IGAG(NGAM),NGA,INTZED
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION PHI(NOM),PHIN(NOM),FSFXI(NDM),FSDXI(NDM),
     .          FSFET(NDM),FSDET(NDM),ZMAT(2,2),ZP(2)
C
      DATA ONE/1.D0/,TWTHI/0.6666666666666667D0/,ZERO/0.D0/,ITEMAX/20/,
     .     BIG/1.D10/,EPS/1.D-06/,OTHREE/0.3333333333333333D0/
C
C     SAVE
C     SAVE /DATGEN/,/SYSTEM/,MAILLE/,/DELEM1/,/DELEM2/,/FUNCTN/,/REGRID/
C
C.....Initialization
C
      NNODE = MNOD
      DO IG=1,NGA
         IGAG(IG) = 0
      END DO
C
C.....Find which element contains new nodes and regrid z,phi,phin
C
      NOMSA = NOM
      NOM   = 2
      IFLAG = 1
C
      DO IE=1,NELEF(1)
C
C        4-node and MII local connectivity
C
         ILOC = 0
C
         DO J=1,NNODE
            NODE2(J)    = ILCON(IE,J)
            XYZLO2(J,1) = XYZNOD(NODE2(J),1)
            XYZLO2(J,2) = XYZNOD(NODE2(J),2)
C
            DO I=1,NNODE
               ILOC = ILOC + 1
               NODE1(ILOC)    = IGCON(IE,I,J)
               XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
               XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
               XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYM
	       IF (ISYM.EQ.1) THEN
                 NSIDE = IFACE(IE)
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.6.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYS
            END DO
         END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
         DO IG=1,NGA
            XG = XYZGAG(IG,1)
            YG = XYZGAG(IG,2)
C
C           Check if xn,yn is within element IE (4-nodes) (x,y)_j
C
            INIE = .FALSE.
	    IF(IGAG(IG).EQ.0) THEN
               CALL POLYIN2(INIE,XG,YG)
C              -----------
            END IF
	    IF(INIE) THEN
               WRITE(IO6,*) IE,IG,XG,YG
C
C              Regrid xn,yn with MII element IE, ie find z,phi,phin
C              using Newton-Raphson method
C
C              Estimate of xio,etao by bilinear interpolation
C
               XI0   = ZERO
	       ET0   = ZERO
	       ZP(1) = BIG
	       ZP(2) = BIG
	       ITER  = 0
C
	       DO WHILE((DABS(ZP(1)).GT.EPS.OR.DABS(ZP(2)).GT.EPS).
     .                  AND.ITER.LE.ITEMAX)
C
C                 One dimensional 4-node shape functions
C
                  CHI = (XI0 + ONE)*OTHREE + EFJXI
                  CALL SHAPFN(CHI,FSFXI,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDXI,NNODE)
C                 -----------
                  CHI = (ET0 + ONE)*OTHREE + EFJET
                  CALL SHAPFN(CHI,FSFET,NNODE)
C                 -----------
                  CALL SHAPD1(CHI,FSDET,NNODE)
C                 -----------
C
C                 Build N-R system with bidim. shape functions
C
                  ZMAT(1,1) = ZERO
		  ZMAT(1,2) = ZERO
		  ZMAT(2,1) = ZERO
		  ZMAT(2,2) = ZERO
		  ZP(1)     = -XG
		  ZP(2)     = -YG
                  ILOC = 0
C
                  DO J=1,NNODE
                     DO I=1,NNODE
                        ILOC = ILOC + 1
C
                        FI(ILOC)   = FSFXI(I)*FSFET(J)
                        DFIX(ILOC) = FSDXI(I)*FSFET(J)*OTHREE
                        DFIE(ILOC) = FSFXI(I)*FSDET(J)*OTHREE
C
			ZMAT(1,1)  = ZMAT(1,1)-DFIX(ILOC)*XYZLO1(ILOC,1)
			ZMAT(1,2)  = ZMAT(1,2)-DFIE(ILOC)*XYZLO1(ILOC,1)
			ZMAT(2,1)  = ZMAT(2,1)-DFIX(ILOC)*XYZLO1(ILOC,2)
			ZMAT(2,2)  = ZMAT(2,2)-DFIE(ILOC)*XYZLO1(ILOC,2)
			ZP(1)      = ZP(1) + FI(ILOC)*XYZLO1(ILOC,1)
			ZP(2)      = ZP(2) + FI(ILOC)*XYZLO1(ILOC,2)
                     END DO
                  END DO
C
C                 Solve N-R system
C
                  CALL SOLVE(ZMAT,ZP,2)
C                 ----------
		  ITER = ITER + 1
		  XI0  = XI0 + ZP(1)
		  ET0  = ET0 + ZP(2)
               END DO
C
C              Calculate gage parameters for converged (xi,et)
C
               CHI = (XI0 + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSFXI,NNODE)
C              -----------
               CHI = (ET0 + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSFET,NNODE)
C              -----------
	       XYZGAG(IG,3)  = ZERO
               ILOC          = 0
C
               DO J=1,NNODE
                  DO I=1,NNODE
                     ILOC         = ILOC + 1
                     FI(ILOC)     = FSFXI(I)*FSFET(J)
                     XYZGAG(IG,3) = XYZGAG(IG,3) +
     .                                FI(ILOC)*XYZLO1(ILOC,3)
                  END DO
               END DO
C
	       IGAG(IG) = 1
	    END IF
	 END DO
      END DO
C
C.....Restore dimension for SOLVE
C
      NOM = NOMSA
C
C.....Write gage coordinates and values
C
      WRITE(IO6,*) 'Gages : I,X,Y,Z,PHI,PHIN :'
      WRITE(IO71,2005) TIME,(XYZGAG(IG,3),IG=1,NGA)
C
      RETURN
C
 2000 FORMAT(I5,5D16.8)
 2005 FORMAT(25E16.8)
 2010 FORMAT(5I5,2F10.5)
 2020 FORMAT(17I5)
C
      END

