C----------------------------------------------------------------------------
      SUBROUTINE LEGEN(Pol,XVAL,MP)
C----------------------------------------------------------------------------                                              \
C0  LEGEN   LEGEN
C1  PURPOSE    Compute the Legendre polynomial
C2  C.Fochesato, CMLA, 2003
C----------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      DIMENSION Pol(MPM+1,MPM+1)
C
      DATA ZERO/0.D0/,ONE/1.D0/,TWO/2.D0/
C
      DO N=1,MPM+1
         DO M=1,MPM+1
            Pol(N,M) = ZERO
         END DO
      END DO
C
      ZNEGTERM = ONE
      ODDFACT = ONE
      ZNEXTODD = ONE
      SQROOT = DSQRT(ONE - XVAL*XVAL)
      SQRTTERM = ONE
      DO LI=0,MP
         Pol(LI+1,LI+1) = ZNEGTERM*ODDFACT*SQRTTERM
         ZNEGTERM = -ZNEGTERM
         ODDFACT = ODDFACT*ZNEXTODD
         ZNEXTODD = ZNEXTODD+TWO
         SQRTTERM = SQRTTERM*SQROOT
         IF (LI.LT.MP) THEN
            Pol(LI+2,LI+1) = XVAL * DFLOAT(2*LI+1) * Pol(LI+1,LI+1)
            DO LJ=LI+2,MP
               Pol(LJ+1,LI+1) = (XVAL*DFLOAT(2*LJ-1)*Pol(LJ,LI+1) -
     .                 DFLOAT(LJ+LI-1)*Pol(LJ-1,LI+1))/DFLOAT(LJ-LI)
            END DO
         END IF
      END DO
      RETURN
C
      END
C----------------------------------------------------------------------------
      SUBROUTINE LEGEN2(Pol,XVAL,MP)
C----------------------------------------------------------------------------                                              \
C0  LEGENDRE   LEGEN2
C1  PURPOSE    Compute the modified Legendre polynomial in the sense :
C1             LEGEN2=LEGEN*sqrt(1-XVAL^2)
C2  C.Fochesato, CMLA, 2003
C----------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      DIMENSION Pol(MPM+1,MPM+1)
C
      DATA ZERO/0.D0/,ONE/1.D0/,TWO/2.D0/
C
      DO N=1,MPM+1
         DO M=1,MPM+1
            Pol(N,M) = ZERO
         END DO
      END DO
C
      ZNEGTERM = ONE
      ODDFACT = ONE
      ZNEXTODD = ONE
      SQROOT = DSQRT(ONE - XVAL*XVAL)
      SQRTTERM = ONE
      DO LI=0,MP
         IF (LI.EQ.0) THEN
            Pol(1,1)=ZNEGTERM*ODDFACT
         ELSE
            Pol(LI+1,LI+1) = ZNEGTERM*ODDFACT*SQRTTERM
            SQRTTERM = SQRTTERM*SQROOT
         END IF
         ZNEGTERM = -ZNEGTERM
         ODDFACT = ODDFACT*ZNEXTODD
         ZNEXTODD = ZNEXTODD+TWO
         IF (LI.LT.MP) THEN
            Pol(LI+2,LI+1) = XVAL * DFLOAT(2*LI+1) * Pol(LI+1,LI+1)
            DO LJ=LI+2,MP
               Pol(LJ+1,LI+1) = (XVAL*DFLOAT(2*LJ-1)*Pol(LJ,LI+1) -
     .                 DFLOAT(LJ+LI-1)*Pol(LJ-1,LI+1))/DFLOAT(LJ-LI)
            END DO
         END IF
      END DO
      RETURN
C
      END
