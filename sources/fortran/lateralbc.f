C
C                        LATERAL BOUNDARY CONDITIONS
C
C-------------------------------------------------------------------------------
      SUBROUTINE UPLABC(UBC,QBC,PHI,PHIS,PHIM,PHINS,PHINM,PHISS,
     .                  PHIMM)
C-------------------------------------------------------------------------------
C0  UPLABCF   UPLABC
C1  PURPOSE   Update boundary conditions or b.c. and geometry (IFLAGV =
C1            (2/1)), on the lateral Neuman boundaries according to
C1            IPTYP (IBCOND(K)=0,2,3 for K=2,3,4,5).
C2  CALL UPLABC(UBC,QBC,PHI,PHIS,PHIM,PHINS,PHINM,PHISS,PHIMM)
C3  CALL ARG. PHIN(NOM),PHISS,PHIS= Are used in advection corr. of paddl
C3            ILOOP               = Loop in temporal iterations
C3            IPTYP               = 1: sine paddle, 2: stream fct. wave
C3                                  3,4: piston wave paddle
C3                                  0 : Wave potential on the free surf.
C3                                = 8 wave focusing with flap wavemaker
C3                                = 7 wave focusing with flap wavemaker, ECN wavemaker geometry
C3                                = 9 Underwater landslide with IBOT = 5,6 (symmetry)
C3                                = 10 Underwater slump with IBOT = 5,6 (symmetry)
C3            UBC,QBC(NOM)        = u,du/dn imposed on D<N boundaries
C3            IBCOND(6)           = 0: impermeable boundary, 1: Dirichl.
C3                                  bound., 2: wavemaker bd. (IPTYP=1-4)
C3                                  3 : absorbing piston
C3            IFLAGV              = (1/2) update B.C.+geo or B.C. only
C3
C4  RET. ARG. XYZNOD(NOM,3),QBC(NOM)
C6  INT. CALL PADSIN,STRFCT,PADSOL,ABSPIS,IMPLAT,ERRORS
C10 Feb. 99   S. Grilli at INLN
CLUPLATBC SUB. WHICH UPDATES LATERAL BOUNDARY GEOMETRY AND B.C.
C-------------------------------------------------------------------------------
      IMPLICIT  REAL*8 (A-H,O-Z)
      INCLUDE 'param.inc'

C-------------------------------------------------------------------------------
      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*5),
     .                IDUM3(78+2*NOMM+2*MOMM),IBCOND(6)
c      COMMON /SYSTEM/ DUM3(31+8*MYMAX),IDUM4(2),IFLAGV,IPTYP,ILOOP
      COMMON /OPTION/ ISYM,IPOST,IVECT
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      include 'paddle2.h'
C-------------------------------------------------------------------------------
      DIMENSION UBC(NOM),QBC(NOM),PHI(NOM),PHIS(NOM),PHISS(NOM),
     .          PHIM(NOM),PHIMM(NOM),PHINS(NOM),PHINM(NOM)
C-------------------------------------------------------------------------------
       SAVE

c###############################################################################
c###############################################################################


C-------------------------------------------------------------------------------
c	Taking in account the use of the symetry
C-------------------------------------------------------------------------------

      KMAX=5
      IF (ISYM.EQ.1) THEN
         KMAX=KMAX-1
      END IF


c###############################################################################
c	New version  less 'if'
c###############################################################################

c      left and right side	
       do K=2,4,2
       	
C-------------------------------------------------------------------------------
C           Wavemaker boundary for wave generation (Phin, Phitn imposed)
C                 Sinusoidally moved plane paddle hinged on the bottom
C                 Initially TANH tapered sinus. wave (or superposition)
C-------------------------------------------------------------------------------
          IF((IBCOND(K).EQ.2).and.(IPTYP.EQ.8))  
     &       CALL PADSIN(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
     
C-------------------------------------------------------------------------------
C                 Sinusoidally moved plane paddle hinged at approx. mid-depth
C                 LMF-ECN wavemaker geometry
C                 Initially linear tapered sinus. wave (or superposition)
C-------------------------------------------------------------------------------
          IF((IBCOND(K).EQ.2).and.(IPTYP.EQ.7))  
c     &       CALL PADSIN_FB(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,
c     &                 PHINM)
     &       CALL PADSIN_WRL(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,
     &                 PHINM)

C-------------------------------------------------------------------------------
C           Wavemaker boundary for wave generation (Phin, Phitn imposed)
C                 Sinusoidally moved plane paddle hinged on the bottom
C                 Initially TANH tapered sinus. wave (or superposition)
C-------------------------------------------------------------------------------
          IF((IBCOND(K).EQ.2).and.(IPTYP.EQ.11))  
     &    CALL PADSIN_WRL_type2(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
     

C-------------------------------------------------------------------------------
C                 monochramtical wave
C-------------------------------------------------------------------------------

          IF((IBCOND(K).EQ.2).and.(IPTYP.EQ.1))  
     &       CALL PADSIN_MONO(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,
     &                 PHINM)

     
C-------------------------------------------------------------------------------
C              Stream function wave theory generated wave
C-------------------------------------------------------------------------------
          IF((IBCOND(K).EQ.2).and.(IPTYP.EQ.2))  
     &       CALL STRFCT(K,QBC)


C-------------------------------------------------------------------------------
C              Solit. or cnoidal wves  by a piston w-m (Goring method)
C-------------------------------------------------------------------------------
          IF((IBCOND(K).EQ.2).and.(IPTYP.EQ.3.OR.IPTYP.EQ.4))  
     &       CALL PADSOL(K,QBC,PHISS,PHIMM)

cC-------------------------------------------------------------------------------
cC           Absorbing pistons
cC-------------------------------------------------------------------------------
          IF(IBCOND(K).EQ.3) !THEN
     &       CALL ABSPIS(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
     
C-------------------------------------------------------------------------------
C           Impermeable boundary (Phin=Phitn=0) : plane and equi-distant
C           stretched nodes (IBCOND=0)
C-------------------------------------------------------------------------------
          IF((IBCOND(K).EQ.0).and.(IFLAGV.EQ.1))  
     &       CALL IMPLAT(K,QBC)

       enddo
c###############################################################################
c      front and may be rear(ISYM.EQ.0) side	
       do K=3,KMAX,2
C-------------------------------------------------------------------------------
C           Impermeable boundary (Phin=Phitn=0) : plane and equi-distant
C           stretched nodes (IBCOND=0)
C-------------------------------------------------------------------------------
          IF((IBCOND(K).EQ.0).and.(IFLAGV.EQ.1))  
     &       CALL IMPLAT(K,QBC)

       enddo
cC-------------------------------------------------------------------------------
      RETURN
cC-------------------------------------------------------------------------------
      END
cC-------------------------------------------------------------------------------




C-------------------------------------------------------------------------------
      SUBROUTINE UPBOTC(QBC,PHIS,PHIM,PHIN,PHISS,PHIMM,PHINS,PHINM)
C-------------------------------------------------------------------------------
C0  UPBOTCF   UPBOTC
C1  PURPOSE   Update boundary conditions and geometry (with FLAGV = 1)
C1            on the bottom boundary, according to the value of
C1            IPTYP and for IBCOND(6)=0,4 and IBCOND(K)=0,2 for K=2,..,5.
C2  CALL UPBOTC(QBC,PHIS,PHIM,PHIN,PHISS,PHIMM,PHINS,PHINM,PHINN)
C3  CALL ARG. XYZNOD(NOM)         = Coordinates of discretization nodes
C3            QBC(NOM)            = du/dn imposed on Neuman boundaries
C3            IPTYP               = 1: sine paddle, 2: stream fct. wave
C3                                  3,4: solit. cnoid. wave paddle
C3                                  0 : Wave potential on the free surf.
C3            NELEF(6),NNODF(6)   = Nbs. of elts/nodes for each face
C3            IELEF(6,2),INODF()  = Begin/endind elts/nodes Nbs. for face
C3            IBCOND(6)           = 0/4 : fixed/sliding bottom
C3            IBCOND(K)           = 1: Dirichlet boundary
C3                                  2,3: wavemaker (IPTYP1-4) or AP bd.
C3                                  => stretching of NBS bottom nodes
C3                                  0: nothing moves on the bottom
C3            NBS                 = Number of stretched intervals
C3            IFLAGV              = (1/2) update B.C.+geo or B.C. only
C3
C4  RET. ARG. XYZNOD(NOM),QBC(NOM)
C6  INT. CALL STRBOT,ERRORS
CE  ERRORS    01 = Too many stretched intervals (NBS too large)
CE            02 = Bottom is not Neuman impermeable !
C9  Feb. 99   S. GRILLI, INLN
CLUPBOTC SUB. WHICH UPDATES BOTTOM GEOMETRY AND BOUNDARY CONDITION
C-------------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'
      PARAMETER (NSLMAX=800)
      CHARACTER*8 TEXTE1,TEXTE2
C-------------------------------------------------------------------------------
      COMMON /DATGEN/ ZL0,W0,H0,X0,D0,MX,MY,MZ,NX,NY,IDUM1(3),
     .                NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),DUM2(2*MOMM),XSTART(6,3),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*4),IFACE(MOMM),
     .                IDUM3(2*NOMM+2*MOMM),ICUMUL(6),
     .                IDUM4(30),JSLIM(6),NELEF(6),NNODF(6),
     .                IELEF(6,2),INODF(6,2),IBCOND(6)
c      COMMON /SYSTEM/ DUM3(24),TIME,DUM4(6+8*MYMAX),IO6,IDUM5,IFLAGV,
c     .                IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /TPLID/ VS(NOMM,3),VM(NOMM,3),VN(NOMM,3),DVS(NOMM),
     .               DVM(NOMM),DVN(NOMM),XSS(NOMM,3),XMM(NOMM,3),
     .               XSM(NOMM,3),DUM5(3*NOMM),
     .               XKA(NOMM)
      COMMON /BOUNDC/ DT,DUM6(13),NBS
      COMMON /BOTFCT/ BOT(NXM,NYM),DUM7(2),IBOT
      COMMON /LNDSLD/ THETA,BSLIDE,WSLIDE,TSLIDE,DSLIDE,ZL2,RHOL,GAMMA,
     .                CM,CD,ZEPS,ZMU,ZMUC,ZAL,UT,A0,S0,T0,WLEGTH,
     .                ZKX,ZKY,STHET,CTHET,TTHET,COSHEP,COSHMU,
     .                XINIT,TSLMAX,S,ST,STT,XBEGIN,XEND,XCENTR,XI0,
     .                SDELTT,DELTAS,ZKS,TSLOPE,DELTAT,AINIT,TANHXI,
     .                ZKRAMP,UINIT,TANHTI,SINIT,COSHTI,RATIO2
      COMMON /SLKINE/ TSL(NSLMAX),SSL(NSLMAX),USL(NSLMAX),ASL(NSLMAX),
     .                NSTART,NSL
      COMMON /OPTION/ ISYM,IPOST,IVECT
C-------------------------------------------------------------------------------
      DIMENSION QBC(NOM),PHIS(NOM),PHIM(NOM),PHIN(NOM),PHISS(NOM),
     .          PHIMM(NOM),PHINS(NOM),PHINM(NOM)
C-------------------------------------------------------------------------------
      DATA ZERO/0.D0/,TEXTE1/'UPBOTC01'/,TEXTE2/'UPBOTC02'/,
     .     HALF/0.5D0/,ONE/1.D0/,EPS/1.D-08/,NMAX/10/,ZK/10.D0/,
     .     TWO/2.D0/,FRACA/0.05D0/,XIRAMP/3.D0/,FOUR/4.D0/
C-------------------------------------------------------------------------------
       SAVE

C-------------------------------------------------------------------------------
C     Function
C-------------------------------------------------------------------------------
      UDASH(TEMPS) = HALF*AINIT*DLOG(DCOSH(ZKRAMP*TEMPS -
     .               XIRAMP)/DCOSH(XIRAMP))/(ZKRAMP*TANHXI)
C-------------------------------------------------------------------------------
C.....Updating of bottom boundary if Neuman condition is imposed
C-------------------------------------------------------------------------------
      IFOND=6
      KMAX=5
      IF (ISYM.EQ.1) THEN
         IFOND=5
         KMAX=4
      END IF
CSYM...
      IF((IBCOND(IFOND).EQ.0).OR.(IBCOND(IFOND).EQ.4)) THEN
         DO K=2,KMAX
            IF((IBCOND(K).EQ.2.AND.IPTYP.GT.1).OR.(IBCOND(K).EQ.3))THEN
C-------------------------------------------------------------------------------
C              Wavemaker boundary or AP boundary
C-------------------------------------------------------------------------------
               IF(((K.EQ.2.OR.K.EQ.4).AND.NBS.GT.MX).OR.
     .            ((K.EQ.3.OR.K.EQ.5).AND.NBS.GT.MY)) THEN
                  CALL ERRORS(TEXTE1)
C                 -----------
               ELSE
C-------------------------------------------------------------------------------
C                 Changes (x,y) coordinates of botom nodes for lateral
C                 moving boundaries
C                 Note : CHANGE ZL0 AND/OR W0 WHEN LATERAL
C                        BOUNDARIES MOVE (AP,WM...)
C-------------------------------------------------------------------------------
                  CALL STRBOT(K)
C                 -----------
               END IF
            END IF
         END DO
C-------------------------------------------------------------------------------
C        Impermeable boundary condition (Phin,Phitn=0, IFLAGV=1,2)
CSYM
C         IF((IPTYP.LT.9).OR.(IPTYP.EQ.9)) THEN
         IF((IPTYP.LE.9).OR.(IPTYP.EQ.11)) THEN
            DO IK=INODF(IFOND,1),INODF(IFOND,2)
               QBC(IK) = ZERO
            END DO
	 ELSE IF ( ((IBOT.EQ.5).OR.(IBOT.EQ.6)).AND.
     .             (IBCOND(IFOND).EQ.4) ) THEN
CMYS
C           Landslide/slump motion and kinematic BC
C-------------------------------------------------------------------------------
            IF(IFLAGV.EQ.1) THEN
C-------------------------------------------------------------------------------
C              Change geometry and set phin
C-------------------------------------------------------------------------------
               IF(IPTYP.NE.12) THEN
	             TIMEI = FRACA*T0
	             IF(TIME.LE.TIMEI) THEN
C-------------------------------------------------------------------------------
C                    Ramp up of initial acceleration
C-------------------------------------------------------------------------------
                     STT = HALF*(ONE + DTANH(ZKRAMP*TIME - XIRAMP)/
     .                        TANHXI)*AINIT
                     STP = ST
                     ST  = HALF*AINIT*TIME + UDASH(TIME)
	                S   = S + HALF*(STP + ST)*DT
	             ELSE IF(TIME.LE.(TSLOPE - DELTAT)) THEN
                     TERM = TIME/T0
                     S    = S0 * DLOG(DCOSH(TERM)/COSHTI) + SINIT
                     ST   = UT * (DTANH(TERM) - TANHTI) + UINIT
                     STT  = A0 /(DCOSH(TERM)**2)
		        ELSE
C-------------------------------------------------------------------------------
C                    Ramp down of S to (1-epst)*SSLOPE over time DELTAT
C-------------------------------------------------------------------------------
		           EXPT = DEXP(-ZKS*(TIME-(TSLOPE - DELTAT)))
		           S    = SDELTT + DELTAS*(ONE - EXPT)
		           ST   = ZKS*DELTAS*EXPT
		           STT  = -(ZKS**2)*DELTAS*EXPT
		        END IF
               ELSE
C-------------------------------------------------------------------------------
C                 Linearly interpolate s,u,a from file slikinem.dat data
C-------------------------------------------------------------------------------
                  NK = NSTART
                  DO WHILE(TIME.GE.TSL(NK))
	                NK = NK + 1
	             END DO
		        NK = NK - 1
C-------------------------------------------------------------------------------
C                 No more kinematics data --> stop calculation
C-------------------------------------------------------------------------------
	             NSTART = NK
		        IF(NSTART.GT.NSL) CALL ERRORS('NSTA>NSL')
	                PROP   = (TIME-TSL(NK))/(TSL(NK+1)-TSL(NK))
	                S      = SSL(NK) + (SSL(NK+1) - SSL(NK))*PROP
	                ST     = USL(NK) + (USL(NK+1) - USL(NK))*PROP
	                STT    = ASL(NK) + (ASL(NK+1) - ASL(NK))*PROP
	                WRITE (*,*) S,ST,STT
	             END IF
C-------------------------------------------------------------------------------
                  WRITE (IO6,*) 't =            ',TIME
                  WRITE (IO6,*) 'S =            ',S
                  WRITE (IO6,*) 'dS/dt =        ',ST
      	          WRITE (IO6,*) 'd2S/dt2 =      ',STT
	          WRITE (97,*) TIME,S,ST,STT
C-------------------------------------------------------------------------------
C              Instantaneous location of slide axis on slope
C              and slide extension along slope
C-------------------------------------------------------------------------------
	             XCENTR = XINIT - S*CTHET
	             XBEGIN = XCENTR - XI0*CTHET
	             XEND   = XCENTR + XI0*CTHET
                  WRITE(IO6,*) 'XCENTR,XI0,XBEGIN,XEND',XCENTR,XI0,
     .                       XBEGIN,XEND
C-------------------------------------------------------------------------------
C              Bottom elevation updating (x,y)=cst in slide area
CSYM
	             DO I=1,NX
	                XB = XYZNOD(ICUMUL(IFOND)+I,1)
C-------------------------------------------------------------------------------
  	                IF( (XB.GT.(X0+D0)).AND.(XB.LT.(X0+ZL0-ZL2)) )
     .               THEN
	                   IF((XB.LE.XBEGIN).OR.(XB.GE.XEND)) THEN
C-------------------------------------------------------------------------------
C                       Plane slope in front and behind slide
C-------------------------------------------------------------------------------
		                 ZOFX  = TTHET*(XB - D0 - X0) - H0
		                 DO J=1,NY
		                    BOT(I,J) = ZOFX
		                 END DO
	                   ELSE
C-------------------------------------------------------------------------------
C                       Double sech^2 slide
C-------------------------------------------------------------------------------
		                 XI   = (XB - XCENTR)/CTHET
		                 ZOFX = TTHET*(XB - X0 - D0) - H0
		                 DO J=1,NY
	                         YB     = XYZNOD(NOM - (J-1)*NX,2)
		                    DXI0   = EPS
		                    DXIERR = ONE
		                    N = 1
		                    DO WHILE((DXIERR.GT.EPS).AND.
     &				     (N.LE.NMAX))
		                       IF(ZEPS.NE.ZMU) THEN
C-------------------------------------------------------------------------------
C                                Quasi-2D and square footprint
C-------------------------------------------------------------------------------
		                 DXI1 = TSLMAX*(ONE/DCOSH(ZKX*
     .                                           (XI+DXI0))**2 - ZEPS)*
     .                                (ONE/DCOSH(ZKY*YB)**2-ZMUC)*TTHET
                              ELSE
C-------------------------------------------------------------------------------
C                                3D with elliptical footprint in (r,phi) axes
C-------------------------------------------------------------------------------
                                 RPDR = DSQRT((XI+DXI0)**2 + YB**2)
			         CPHI = (XI+DXI0)/RPDR
			         SPHI = YB/RPDR
			         R0   = XI0/DSQRT(CPHI*CPHI +
     .                                            RATIO2*SPHI*SPHI)
                                 IF(RPDR.GE.R0) THEN
C-------------------------------------------------------------------------------
C                                   No slide outside of footprint
C-------------------------------------------------------------------------------
			            DXI1 = ZERO
			         ELSE
                                    ZKR  = COSHEP/R0
                                    DXI1 = TSLMAX*(ONE/DCOSH(
     .                                         ZKR*RPDR)**2 -ZEPS)*TTHET
                                 END IF
			      END IF
C-------------------------------------------------------------------------------
                              DXIERR = DABS((DXI1-DXI0)/DXI0)
			      DXI0   = DXI1
			      N      = N + 1
		           END DO
		           BOT(I,J) = ZOFX + DXI0/STHET
		        END DO
	             END IF
	          END IF
	       END DO
C-------------------------------------------------------------------------------
C              Updating of bottom node z-geometry
CSYM
	       IK = ICUMUL(IFOND)
	       DO J=1,NY
		  DO I=1,NX
		     IK = IK + 1
		     XYZNOD(IK,3) = BOT(I,NY-J+1)
		  END DO
               END DO
C-------------------------------------------------------------------------------
C              Recompute sliding derivat. on the bottom for new geometry
CSYM
               CALL SLIDING(IFOND)
C              ------------
C-------------------------------------------------------------------------------
C              Updating PHIN for bottom nodes
CSYM
               DO IK=INODF(IFOND,1),INODF(IFOND,2)
	          XB = XYZNOD(IK,1)
                  IF((XB.LT.XBEGIN).OR.(XB.GT.XEND)) THEN
C-------------------------------------------------------------------------------
C                    Non-slide fixed nodes
C-------------------------------------------------------------------------------
                     QBC(IK) = ZERO
		        ELSE
	                IF(ZEPS.NE.ZMU) THEN
C-------------------------------------------------------------------------------
C                       Quasi-2D and square footprint
C                       Slide moving nodes phin=u_l . n
C-------------------------------------------------------------------------------
		              QBC(IK) = -ST*
     &			         (VN(IK,1)*CTHET+VN(IK,3)*STHET)
		           ELSE
C-------------------------------------------------------------------------------
C                       3D with elliptical footprint in (r,phi) axes
C-------------------------------------------------------------------------------
	                   YB   = XYZNOD(IK,2)
		              XI   = (XB - XCENTR)/CTHET
                        RPDR = DSQRT(XI**2 + YB**2)
	                   CPHI = XI/RPDR
	                   SPHI = YB/RPDR
		              R0   = XI0/
     &			             DSQRT(CPHI*CPHI + RATIO2*SPHI*SPHI)
                        IF(RPDR.GT.R0) THEN
                           QBC(IK) = ZERO
			         ELSE
		                 QBC(IK) = -ST*
     &				      (VN(IK,1)*CTHET+VN(IK,3)*STHET)
			         END IF
		           END IF
		        END IF
	          END DO
            ELSE IF(IFLAGV.EQ.2) THEN
C-------------------------------------------------------------------------------
C              Updating PHITN for bottom nodes
CSYM
               DO IK=INODF(IFOND,1),INODF(IFOND,2)
	          XB = XYZNOD(IK,1)
	          IF((XB.LT.XBEGIN).OR.(XB.GT.XEND)) THEN
C-------------------------------------------------------------------------------
C                    Non-slide fixed nodes
C-------------------------------------------------------------------------------
                     QBC(IK) = ZERO
		  ELSE
	             IF(ZEPS.EQ.ZMU) THEN
C-------------------------------------------------------------------------------
C                       3D with elliptical footprint in (r,phi) axes
C-------------------------------------------------------------------------------
	                YB   = XYZNOD(IK,2)
		        XI   = (XB - XCENTR)/CTHET
                        RPDR = DSQRT(XI**2 + YB**2)
	                CPHI = XI/RPDR
	                SPHI = YB/RPDR
		        R0   = XI0/DSQRT(CPHI*CPHI + RATIO2*SPHI*SPHI)
		     END IF
                     IF((ZEPS.EQ.ZMU).AND.(RPDR.GT.R0)) THEN
                        QBC(IK) = ZERO
		     ELSE
C-------------------------------------------------------------------------------
C                       Slide moving nodes phitn=d(u_l . n)/dt -
C                       (u_l . s)phins -(u_l . m)phinm - (u_l . n)phinn
C                       Note: s- m- fields have been calculated in DUDTPR
C-------------------------------------------------------------------------------
C                       Curvature and torsion terms for IK
C-------------------------------------------------------------------------------
                        CSSS = ZERO
	                CMMM = ZERO
	                CSMS = ZERO
	                CSMM = ZERO
                        CSSN = ZERO
	                CMMN = ZERO
                        DO K=1,3
                           CSSS = CSSS + XSS(IK,K)*VS(IK,K)
	                   CMMM = CMMM + XMM(IK,K)*VM(IK,K)
	                   CSMS = CSMS + XSM(IK,K)*VS(IK,K)
	                   CSMM = CSMM + XSM(IK,K)*VM(IK,K)
                           CSSN = CSSN + XSS(IK,K)*VN(IK,K)
	                   CMMN = CMMN + XMM(IK,K)*VN(IK,K)
	                END DO
                        CSSS = CSSS/DVS(IK)**2
	                CMMM = CMMM/DVM(IK)**2
	                CSMS = CSMS/(DVS(IK)*DVM(IK))
	                CSMM = CSMM/(DVS(IK)*DVM(IK))
                        CSSN = CSSN/DVS(IK)**2
	                CMMN = CMMN/DVM(IK)**2
C-------------------------------------------------------------------------------
                        PHINN = -( PHISS(IK) + PHIMM(IK) + PHIS(IK)*
     .                           (CSMM-CSSS) + PHIM(IK)*(CSMS - CMMM) -
     .                            PHIN(IK)*(CSSN + CMMN) )
C-------------------------------------------------------------------------------
                        QBC(IK) = -STT*(VN(IK,1)*CTHET+VN(IK,3)*STHET)+
     .                    ST*((VS(IK,1)*CTHET+VS(IK,3)*STHET)*PHINS(IK)+
     .                        (VM(IK,1)*CTHET+VM(IK,3)*STHET)*PHINM(IK)+
     .                        (VN(IK,1)*CTHET+VN(IK,3)*STHET)*PHINN)
                     END IF
		  END IF
	       END DO
	    END IF
	 END IF
      ELSE
         CALL ERRORS(TEXTE2)
C        -----------
      END IF
C-------------------------------------------------------------------------------
      RETURN
C-------------------------------------------------------------------------------
      END
C-------------------------------------------------------------------------------






C----------------------------------------------------------------------
      SUBROUTINE STRBOT(K)
C----------------------------------------------------------------------
C0  STRBOTF   STRBOT
C1  PURPOSE   Stretching of the bottom geometry over NBS intervals
C1            starting at lateral boundary K(2,5). Bottom is supposed
C1            horizontal over the NBS intervals.
C2  CALL STRBOT(K)
C3  CALL ARG. XYZNOD(NOMM,3)      = Coordin. of nofes for current step
C3            INODF(6,2)          = Begin/endind nodes Nbs. for face K
C3            NBS                 = Number of stretched intervals
C3
C4  RET. ARG. XYZNOD
C9  Feb 99   S. GRILLI, INLN
CLSTRBOT SUB. WHICH STRETCHES HORIZ. BOTTOM GEOM. OVER NBS INTERVALS
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM1(5),MX,MY,IDUM1(8)
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM3(66),INODF(6,2),IBCOND(6)
      COMMON /BOUNDC/ DUM4(14),NBS
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
       SAVE
C     SAVE DATGEN/,/MAILLE/,/BOUNDC/
C
C     Stretching on the bottom of the NBS+1 nodes close to the K bound.
C     The bottom is considered horizontal over the NBS intervals.
C
C     Need to account for changes in y or x due to snake WM or AP REAL*8ignment
C
CSYM...
      IFOND=6
      IF (ISYM.EQ.1) THEN
        IFOND=5
      END IF
C
      IF(K.EQ.2.OR.K.EQ.4) THEN
         IST  = 3 - K
CSYM...
	 IB   = INODF(IFOND,K/2)
         IBF  = IB + IST*(NBS + MY*(MX+1))
	 DO J=1,(MY+1)
	    IBS  = IB  + IST*(J-1)*(MX+1)
	    IBB  = IBS + IST*NBS
	    XEND = XYZNOD(LXYZ(IBS,1),1)
	    YEND = XYZNOD(LXYZ(IBS,1),2)
	    XBOT = XYZNOD(IBB,1)
	    YBOT = XYZNOD(IBB,2)
	    ZM   = (YBOT-YEND)/(XBOT-XEND)
            DLIB = (XYZNOD(IBF,1) - XEND)/NBS
            DO I=1,NBS
	       IND = IBS + IST*(I-1)
               XYZNOD(IND,1) = XEND + (I-1)*DLIB
	       XYZNOD(IND,2) = YEND + ZM*(XYZNOD(IND,1) - XEND)
	    END DO
         END DO
      ELSE IF(K.EQ.3.OR.K.EQ.5) THEN
         IST  = K - 4
CSYM...
	 IB   = INODF(IFOND,(7-K)/2)
         IBF  = IB + IST*((NBS+1)*(MX+1) - 1)
	 DO I=1,(MX+1)
	    IBS  = IB  + IST*(I - 1)
	    IBB  = IBS + IST*NBS*(MX+1)
	    XEND = XYZNOD(LXYZ(IBS,1),1)
	    YEND = XYZNOD(LXYZ(IBS,1),2)
	    XBOT = XYZNOD(IBB,1)
	    YBOT = XYZNOD(IBB,2)
	    ZM   = (XBOT-XEND)/(YBOT-YEND)
            DLIB = (XYZNOD(IBF,2) - YEND)/NBS
            DO J=1,NBS
	       IND = IBS + IST*(J-1)*(MX+1)
               XYZNOD(IND,2) = YEND + (J-1)*DLIB
	       XYZNOD(IND,1) = XEND + ZM*(XYZNOD(IND,2) - YEND)
	    END DO
         END DO
      END IF
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE IMPLAT(KF,QBC)
C----------------------------------------------------------------------
C0  IMPLATF   IMPLAT
C1  PURPOSE   Update boundary conditions and geometry (IFLAGV = 1)
C1            (2/1)), on the lateral Neuman impermeable boundaries for
C1            IBCOND(KF)=0, KF=2,3,4,5.
C1            Surface elevation cannot be multiple-valued !!
C1            Assumes at most 2 moving boundaries and 2 fixed staggered
C2  CALL IMPLAT(K,QBC)
C3  CALL ARG. XYZNOD()            = Coordin. of nofes for current step
C3            QBC(NOM)            = du/dn imposed on Neuman boundaries
C3
C4  RET. ARG. XYZNOD,QBC(NOM)
C9  Feb. 99   S. GRILLI, INLN
CLIMPLAT SUB. WHICH UPDATES LATERAL IMPERMEA. BOUNDARY GEOMETRY AND B.C.
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

c      COMMON /SYSTEM/ DUM0(31+8*MYMAX),IO6,IDUM0(2),IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON/DATGEN/  ZL0,W0,H0,X0,D0,MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM),XSTART(6,3),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM3(66),INODF(6,2),IBCOND(6)
      COMMON /BOUNDC/ DUM3(14),NBS
      COMMON /OPTION/ ISYM,IPOST,IVECT
      COMMON /BOTFCT/ BOT(NXM,NYM),SLOPE,ZK,IBOT
C
      DIMENSION QBC(NOM)
C
      DATA ZERO/0.D0/
C
       SAVE
C     SAVE /MAILLE/,/DATGEN/
C
C     Impermeable boundary (Phin=Phitn=0) : plane and equi-distant
C     stretched nodes (IBCOND=0). Updated z is kept on free surface.
C
C     ! NU1,NU2 x,y coordinates adjustments only work for vertical
C     ! boundaries. Other eqs. should be used for sloping boundaries
C     ! Flap wavemaker on K=2,4 has been programmed
C
CSYM...
      IF (ISYM.EQ.0) THEN
C
         IF(KF.EQ.2.OR.KF.EQ.4) THEN
            IB  = INODF(KF,1)
            NU1 = IB + MZ*(MY+1)
            NU2 = NU1 + MY
C
C        moving boundary on k=3 or 5 => rediscretize y on bottom
CBOT
            IF(IBCOND(3).NE.0.AND.IBOT.NE.10) THEN
               IB0    = IB  + (4-KF)*MY/2
               IBF    = IB0 + (KF-3)*NBS
               DELNDY = (XYZNOD(IBF,2) - XYZNOD(LXYZ(IB0,1),2))/NBS
               DO I=1,NBS+1
                  NB  = IB0 + (KF-3)*(I-1)
                  XYZNOD(NB,2) = XYZNOD(LXYZ(IB0,1),2) + DELNDY*(I-1)
               END DO
            END IF
CBOT
            IF(IBCOND(5).NE.0.AND.IBOT.NE.10) THEN
               IB0    = IB  + (KF-2)*MY/2
               IBF    = IB0 + (3-KF)*NBS
               DELNDY = (XYZNOD(LXYZ(IB0,1),2) - XYZNOD(IBF,2))/NBS
               DO I=1,NBS+1
                  NB  = IB0 + (3-KF)*(I-1)
                  XYZNOD(NB,2) = XYZNOD(LXYZ(IB0,1),2) - DELNDY*(I-1)
               END DO
            END IF
C
C        Fix free surface x in corners
C
c         XYZNOD(NU1,1) = XYZNOD(IB,1)
c         XYZNOD(NU2,1) = XYZNOD(IB+MX,1)
C
C        Lateral fixed boundaries
C
            IF(KF.EQ.2.AND.IBCOND(5).EQ.0) THEN
               XYZNOD(LXYZ(NU1,1),2) = XYZNOD(NU1,2)
            END IF
            IF(KF.EQ.2.AND.IBCOND(3).EQ.0) THEN
               XYZNOD(LXYZ(NU2,1),2) = XYZNOD(NU2,2)
            END IF
            IF(KF.EQ.4.AND.IBCOND(3).EQ.0) THEN
               XYZNOD(LXYZ(NU1,1),2) = XYZNOD(NU1,2)
            END IF
            IF(KF.EQ.4.AND.IBCOND(5).EQ.0) THEN
               XYZNOD(LXYZ(NU2,1),2) = XYZNOD(NU2,2)
            END IF
C
C        Regrid KF
C
         DO I=1,MY+1
            NB  = IB + I - 1
            NU  = NB + MZ*(MY+1)
            XYZNOD(LXYZ(NU,1),1) = XYZNOD(NB,1)
C
            DO J=1,MZ+1
               IND = NB + (J-1)*(MY+1)
               DO K=1,3
                  XYZNOD(IND,K) = XYZNOD(NB,K) + (J-1)*
     .                         (XYZNOD(LXYZ(NU,1),K) - XYZNOD(NB,K))/MZ
               END DO
               QBC(IND) = ZERO
            END DO
         END DO
      ELSE IF(KF.EQ.3.OR.KF.EQ.5) THEN
C        lateral boundaries
         IB  = INODF(KF,1)
         NU1 = IB + MZ*(MX+1)
         NU2 = NU1 + MX
C
         IF(IBCOND(2).NE.0) THEN
            IF(((IPTYP.NE.1).AND.(IPTYP.NE.8).AND.(IPTYP.NE.7)
     .	     .AND.(IPTYP.NE.11)).OR.
     .          (IBCOND(2).NE.2)) THEN
C
C              moving boundary on k=2 => rediscretize x on bottom
C
CBOT
               IF (IBOT.NE.10) THEN
                  IBP    = IB  + (KF-3)*MX/2
                  IBF    = IBP + (4-KF)*NBS
                  DELNDX = (XYZNOD(IBF,1)-XYZNOD(LXYZ(IBP,1),1))/NBS
                  DO I=1,NBS+1
                     NB  = IBP + (4-KF)*(I - 1)
                     XYZNOD(NB,1) = XYZNOD(LXYZ(IBP,1),1)+DELNDX*(I-1)
                  END DO
              END IF
C
C              Fix free surface x in corners
C
c               XYZNOD(NU1,1) = XYZNOD(IB,1)
c               XYZNOD(NU2,1) = XYZNOD(IB+MX,1)
            ELSE
C
C              Flap wavemaker => rediscr. x surface
C
               IBF0   = IB   + MZ*(MX+1) + (KF-3)*MX/2
               IBF    = IBF0 + (4-KF)*NBS
               DELNDX = (XYZNOD(IBF,1) - XYZNOD(LXYZ(IBF0,1),1))/NBS
               DO I=1,NBS+1
                  NB  = IBF0 + (4-KF)*(I - 1)
                  XYZNOD(NB,1) = XYZNOD(LXYZ(IBF0,1),1) + DELNDX*(I-1)
               END DO
C           
C              Flap wavemaker => rediscr. vertical line
C
               NB0 = IB + (KF-3)*MX/2
               DO J=1,MZ+1
                  NB = NB0 + (J-1)*(MX+1)
                  XYZNOD(NB,1) = XYZNOD(LXYZ(NB,1),1)
                  XYZNOD(NB,2) = XYZNOD(LXYZ(NB,1),2)
                  XYZNOD(NB,3) = XYZNOD(LXYZ(NB,1),3)
                  QBC(NB) = ZERO
               END DO

            END IF
         END IF
         IF(IBCOND(4).NE.0) THEN
            IF(((IPTYP.NE.1).AND.(IPTYP.NE.8).AND.(IPTYP.NE.7)
     .	    .AND.(IPTYP.NE.11)).OR.
     .          (IBCOND(4).NE.2)) THEN
C
C              moving boundary on k=4 => rediscretize x on bottom
C
CBOT
               IF (IBOT.NE.10) THEN
                  IBP    = IB  + (5-KF)*MX/2
                  IBF    = IBP + (KF-4)*NBS
                  DELNDX = (XYZNOD(LXYZ(IBP,1),1)-XYZNOD(IBF,1))/NBS
                  DO I=1,NBS+1
                     NB  = IBP + (KF-4)*(I - 1)
                     XYZNOD(NB,1) = XYZNOD(LXYZ(IBP,1),1)-DELNDX*(I-1)
                  END DO
              END IF 
C
C              Fix free surface x in corners
C
c               XYZNOD(NU1,1) = XYZNOD(IB,1)
c               XYZNOD(NU2,1) = XYZNOD(IB+MX,1)
            ELSE
C
C              Flap wavemaker => rediscr. x surface
C
               IBF0   = IB   + MZ*(MX+1) + (5-KF)*MX/2
               IBF    = IBF0 + (KF-4)*NBS
               DELNDX = (XYZNOD(LXYZ(IBF0,1),1) - XYZNOD(IBF,1))/NBS
               DO I=1,NBS+1
                  NB  = IBF0 + (KF-4)*(I - 1)
                  XYZNOD(NB,1) = XYZNOD(LXYZ(IBF0,1),1) - DELNDX*(I-1)
               END DO
C           
C              Flap wavemaker => rediscr. vertical line
C
               NB0 = IB + (5-KF)*MX/2
               DO J=1,MZ+1
                  NB = NB0 + (J-1)*(MX+1)
                  XYZNOD(NB,1) = XYZNOD(LXYZ(NB,1),1)
                  XYZNOD(NB,2) = XYZNOD(LXYZ(NB,1),2)
                  XYZNOD(NB,3) = XYZNOD(LXYZ(NB,1),3)
                  QBC(NB) = ZERO
               END DO
            END IF
         END IF
C
C        Lateral fixed boundaries
C
	 IF(KF.EQ.3.AND.IBCOND(2).EQ.0) THEN
	    XYZNOD(LXYZ(NU1,1),1) = XYZNOD(NU1,1)
	 END IF
	 IF(KF.EQ.3.AND.IBCOND(4).EQ.0) THEN
	    XYZNOD(LXYZ(NU2,1),1) = XYZNOD(NU2,1)
	 END IF
	 IF(KF.EQ.5.AND.IBCOND(4).EQ.0) THEN
	    XYZNOD(LXYZ(NU1,1),1) = XYZNOD(NU1,1)
	 END IF
	 IF(KF.EQ.5.AND.IBCOND(2).EQ.0) THEN
	    XYZNOD(LXYZ(NU2,1),1) = XYZNOD(NU2,1)
	 END IF
C
C        Regrid KF using straight lines from bottom to surface
C
c Is that a bug? that's why the absorbing piston corners with the free surface and the sidewalls don't update! 
C         DO I=2,MX
         DO I=1,MX+1
	    NB = IB + I - 1
	    NU = NB + MZ*(MX+1)
	    XYZNOD(LXYZ(NU,1),2) = XYZNOD(NB,2)
C
   	    DO J=1,MZ+1
	       IND = NB + (J-1)*(MX+1)
	       DO K=1,3
                  XYZNOD(IND,K) = XYZNOD(NB,K) + (J-1)*
     .                         (XYZNOD(LXYZ(NU,1),K) - XYZNOD(NB,K))/MZ
               END DO
               QBC(IND) = ZERO
	    END DO
         END DO
      END IF
CSYM
      ELSEIF (ISYM.EQ.1) THEN

       IF(KF.EQ.2.OR.KF.EQ.4) THEN
	 IB  = INODF(KF,1)
	 NU1 = IB + MZ*(MY+1)
	 NU2 = NU1 + MY
C	moving boundary on 3, not accepted for symetry:

        IF (IBCOND(3).NE.0) THEN

           CALL ERRORS('IMPLAT00')

        END IF

C       fix free surface x in corners

	XYZNOD(NU1,1)=XYZNOD(IB,1)
	XYZNOD(NU2,1)=XYZNOD(IB+MX,1)

C       lateral fixed boundary conditions

         IF (KF.EQ.2) THEN

           XYZNOD(LXYZ(NU1,1),2)=XYZNOD(NU1,2)
           XYZNOD(LXYZ(NU2,1),2)=XYZNOD(NU2,2)

         END IF

         IF (KF.EQ.4) THEN

           XYZNOD(LXYZ(NU1,1),2)=XYZNOD(NU1,2)
           XYZNOD(LXYZ(NU2,1),2)=XYZNOD(NU2,2)

         END IF

C      Regrid KF

         DO I=1,MY+1
            NB=IB+I-1
            NU=NB+MZ*(MY+1)
            XYZNOD(LXYZ(NU,1),1)=XYZNOD(NB,1)
             DO J=1,MZ+1
              IND=NB+(J-1)*(MY+1)
               DO K=1,3
               XYZNOD(IND,K)=XYZNOD(NB,K)+(J-1)*
     .      (XYZNOD(LXYZ(NU,1),K)-XYZNOD(NB,K))/MZ
               END DO
              QBC(IND)=ZERO
            END DO
         END DO

       ELSEIF (KF.EQ.3) THEN

         IB=INODF(KF,1)
         NU1=IB+MZ*(MX+1)
         NU2=NU1+MX
C
C        moving boundary on k=2 or 4 => rediscretize x on bottom
C
	 IF(IBCOND(2).NE.0) THEN
	    IF(((IPTYP.NE.1).AND.(IPTYP.NE.8).AND.(IPTYP.NE.7)
     .	    .AND.(IPTYP.NE.11)).OR.
     .          (IBCOND(2).NE.2)) THEN
C
C              moving boundary on k=2 => rediscretize x on bottom
C
CBOT
              IF (IBOT.NE.10) THEN
                IBP    = IB  + (KF-3)*MX/2
                IBF    = IBP + (4-KF)*NBS
                DELNDX = (XYZNOD(IBF,1)-XYZNOD(LXYZ(IBP,1),1))/NBS
	        DO I=1,NBS+1
	          NB  = IBP + (4-KF)*(I - 1)
                  XYZNOD(NB,1) = XYZNOD(LXYZ(IBP,1),1)+DELNDX*(I-1)
	        END DO
              END IF
C
C              Fix free surface x in corners
C
c               XYZNOD(NU1,1) = XYZNOD(IB,1)
c               XYZNOD(NU2,1) = XYZNOD(IB+MX,1)
	    ELSE
C
C              Flap wavemaker => rediscr. x surface
C
               IBF0   = IB   + MZ*(MX+1) + (KF-3)*MX/2
               IBF    = IBF0 + (4-KF)*NBS
               DELNDX = (XYZNOD(IBF,1) - XYZNOD(LXYZ(IBF0,1),1))/NBS
	       DO I=1,NBS+1
	          NB  = IBF0 + (4-KF)*(I - 1)
                  XYZNOD(NB,1) = XYZNOD(LXYZ(IBF0,1),1) + DELNDX*(I-1)
	       END DO
	    END IF
	 END IF
	 IF(IBCOND(4).NE.0) THEN
	    IF(((IPTYP.NE.1).AND.(IPTYP.NE.8).AND.(IPTYP.NE.7)
     .	    .AND.(IPTYP.NE.11)).OR.
     .          (IBCOND(4).NE.2)) THEN
C
C              moving boundary on k=4 => rediscretize x on bottom
C
CBOT
              IF (IBOT.NE.10) THEN
	        IBP    = IB  + (5-KF)*MX/2
                IBF    = IBP + (KF-4)*NBS
                DELNDX = (XYZNOD(LXYZ(IBP,1),1)-XYZNOD(IBF,1))/NBS
	        DO I=1,NBS+1
	          NB  = IBP + (KF-4)*(I - 1)
                  XYZNOD(NB,1) = XYZNOD(LXYZ(IBP,1),1)-DELNDX*(I-1)
	        END DO
              END IF
C
C              Fix free surface x in corners
C
c               XYZNOD(NU1,1) = XYZNOD(IB,1)
c               XYZNOD(NU2,1) = XYZNOD(IB+MX,1)
	    ELSE
C
C              Flap wavemaker => rediscr. x surface
C
               IBF0   = IB   + MZ*(MX+1) + (5-KF)*MX/2
               IBF    = IBF0 + (KF-4)*NBS
               DELNDX = (XYZNOD(LXYZ(IBF0,1),1) - XYZNOD(IBF,1))/NBS
	       DO I=1,NBS+1
	          NB  = IBF0 + (KF-4)*(I - 1)
                  XYZNOD(NB,1) = XYZNOD(LXYZ(IBF0,1),1) - DELNDX*(I-1)
	       END DO
	    END IF
	 END IF
C
C        Fix free surface x in corners
C
         XYZNOD(NU1,1) = XYZNOD(IB,1)
	 XYZNOD(NU2,1) = XYZNOD(IB+MX,1)
C
C        Lateral fixed boundaries
C
	 IF(KF.EQ.3.AND.IBCOND(2).EQ.0) THEN
	    XYZNOD(LXYZ(NU1,1),1) = XYZNOD(NU1,1)
	 END IF
	 IF(KF.EQ.3.AND.IBCOND(4).EQ.0) THEN
	    XYZNOD(LXYZ(NU2,1),1) = XYZNOD(NU2,1)
	 END IF
C
C        Regrid KF
C
         DO I=1,MX+1
	    NB = IB + I - 1
	    NU = NB + MZ*(MX+1)
	    XYZNOD(LXYZ(NU,1),2) = XYZNOD(NB,2)
C
   	    DO J=1,MZ+1
	       IND = NB + (J-1)*(MX+1)
	       DO K=1,3
                  XYZNOD(IND,K) = XYZNOD(NB,K) + (J-1)*
     .                         (XYZNOD(LXYZ(NU,1),K) - XYZNOD(NB,K))/MZ
               END DO
               QBC(IND) = ZERO
	    END DO
         END DO
      END IF
      END IF
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE ABSPIS(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C----------------------------------------------------------------------
C0  ABSPISF   ABSPIS
C1  PURPOSE   Update boundary conditions and geometry (IFLAGV = 1)
C1            (2/1)), on the lateral absorbing pistons (AP) for
C1            IBCOND(K)=3, K=2,3,4,5.
C1            Assumes at most 2 moving AP boundaries and 2 fixed staggered
C2  CALL ABSPIS(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C3  CALL ARG. XYZNOD()           = Coordin. of nofes for current step
C3            QBC(NOM)            = du/dn imposed on Neuman boundaries
C3            IFLAGV              = (1/2) update B.C.+geo or B.C. only
C3
C4  RET. ARG. XYZNOD(),QBC(NOM)
C9  Feb. 99/Aug. 00   S. GRILLI, INLN/URI
CLABSPIS SUB. WHICH UPDATES LATERAL ABSORBING PISTON GEOMETRY AND B.C.
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'
 
      COMMON/DATGEN/  ZL0,W0,H0,X0,D0,MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM),XSTART(6,3),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM3(66),INODF(6,2),IBCOND(6)
c      COMMON/SYSTEM/  DUM3(27),UPNEW(4,MYMAX),DUPDT(4,MYMAX),
c     .                UB(4),IO6,IFLAGS,IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOUNDC/ DT,DUM4,DT22,DUM5(11),IDV
      COMMON /LNDSLD/ DUM6(5),ZL2,DUM7(42)
      COMMON /BOTFCT/ DUM8(NXM,NYM),DUM9(2),IBOT
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION QBC(NOM),PHIS(NOM),PHIM(NOM),PHISS(NOM),PHIMM(NOM),
     .          PHINS(NOM),PHINM(NOM),DELXY(MYMAX)
      DATA HALF/0.5D0/,ZERO/0.D0/
C
       SAVE
C     SAVE /MAILLE/,/SYSTEM/,DATGEN/,/BOUNDC/
!!! Adjust x,y coordinates of bottom nodes along AP
C
C     Impermeable boundary (Phin=Phitn=0) : plane and equi-distant
C     stretched nodes (IBCOND=0). Updated z is kept on free surface.
C
      WRITE(IO6,*) 'AP on K = ',K
      WRITE(IO6,*) 'IFLAGV = ',IFLAGV
      IF(K.EQ.2.OR.K.EQ.4) THEN
         IST  = K - 3
	 IB   = INODF(K,1)
	 NK   = MY+1
	 NU1  = IB + MZ*NK
	 NU2  = NU1 + MY
C
	 IF(IFLAGV.EQ.1) THEN
C
C           Horizontal updating of snake absorbing piston
C
	    DO I=1,NK
	       DELXY(I) = UPNEW(K-1,I)*DT + DUPDT(K-1,I)*DT22
	    END DO
C
C           Change domain xo and length lo
C
	    IF(K.EQ.2) THEN
               X0  = X0  + DELXY(1)
	       D0  = D0  - DELXY(NK/2)
               ZL0 = ZL0 - DELXY(NK/2)
               XSTART(1,1) = X0
               XSTART(2,1) = X0
               XSTART(3,1) = X0
               XSTART(6,1) = X0
	    ELSE IF(K.EQ.4) THEN
               ZL0 = ZL0 + DELXY(NK/2)
	       ZL2 = ZL2 + DELXY(NK/2)
	    END IF
            XSTART(4,1) = X0 + ZL0
            XSTART(5,1) = X0 + ZL0
CSYM
            IF (ISYM.EQ.1) THEN
                XSTART(5,1)=X0
            END IF
CMYS
	    WRITE(IO6,*) 'DELX,X0,ZL0',DELXY(NK/2),X0,ZL0
C
C           Compatibility with lateral fixed boundaries
CSYM
	   IF (ISYM.EQ.0) THEN

	    IF(K.EQ.2.AND.IBCOND(5).EQ.0) THEN
	       XYZNOD(NU1,2)         = XYZNOD(LXYZ(NU1,2),2)
	       XYZNOD(LXYZ(NU1,1),2) = XYZNOD(NU1,2)
	    END IF
	    IF(K.EQ.2.AND.IBCOND(3).EQ.0) THEN
	       XYZNOD(NU2,2)         = XYZNOD(LXYZ(NU2,2),2)
	       XYZNOD(LXYZ(NU2,1),2) = XYZNOD(NU2,2)
	    END IF
	    IF(K.EQ.4.AND.IBCOND(3).EQ.0) THEN
	       XYZNOD(NU1,2)         = XYZNOD(LXYZ(NU1,2),2)
	       XYZNOD(LXYZ(NU1,1),2) = XYZNOD(NU1,2)
	    END IF
	    IF(K.EQ.4.AND.IBCOND(5).EQ.0) THEN
	       XYZNOD(NU2,2)         = XYZNOD(LXYZ(NU2,2),2)
	       XYZNOD(LXYZ(NU2,1),2) = XYZNOD(NU2,2)
	    END IF
C
	   ELSE IF (ISYM.EQ.1) THEN
C
	    IF(K.EQ.2) THEN
	      XYZNOD(NU1,2)=ZERO
	      XYZNOD(LXYZ(NU1,1),2) = XYZNOD(NU1,2)
	    END IF
	    IF(K.EQ.2.AND.IBCOND(3).EQ.0) THEN
	       XYZNOD(NU2,2)         = XYZNOD(LXYZ(NU2,2),2)
	       XYZNOD(LXYZ(NU2,1),2) = XYZNOD(NU2,2)
	    END IF
	    IF(K.EQ.4.AND.IBCOND(3).EQ.0) THEN
	       XYZNOD(NU1,2)         = XYZNOD(LXYZ(NU1,2),2)
	       XYZNOD(LXYZ(NU1,1),2) = XYZNOD(NU1,2)
	    END IF
	    IF(K.EQ.4) THEN
	       XYZNOD(NU2,2)=ZERO
	       XYZNOD(LXYZ(NU2,1),2) = XYZNOD(NU2,2)
	    END IF

	   END IF
CMYS
	 END IF
C
         DO I=1,NK
	    IF(IFLAGV.EQ.1) THEN
	       NB  = IB + I - 1
	       NU  = NB + MZ*NK
C
C              x-change on AP, bottom, and FS
C
               XYZNOD(NB,1) = XYZNOD(NB,1) + DELXY(I)
CBOT
	       IF(LXYZ(NB,1).NE.0.AND.IBOT.NE.10) THEN
	          XYZNOD(LXYZ(NB,1),1) = XYZNOD(NB,1)
	       END IF
	       IF(LXYZ(NB,2).NE.0.AND.IBOT.NE.10) THEN
   	          XYZNOD(LXYZ(NB,2),1) = XYZNOD(NB,1)
	       END IF
	       XYZNOD(NU,1)         = XYZNOD(NB,1)
	       XYZNOD(LXYZ(NU,1),1) = XYZNOD(NB,1)
C
C              y-change is as FS node except on the sides
C
	       IF((I.GT.1).AND.(I.LT.NK)) THEN
	          YP           = XYZNOD(LXYZ(NU,1),2)
	          XYZNOD(NB,2) = YP
	          XYZNOD(NU,2) = YP
	       ELSE
	          YP                   = XYZNOD(NU,2)
                  XYZNOD(LXYZ(NU,1),2) = YP
               END IF
C
	       DO J=1,MZ+1
	          IND = NB + (J-1)*NK
		  XYZNOD(IND,1) = XYZNOD(NB,1)
	          DO L=2,3
                     XYZNOD(IND,L) = XYZNOD(NB,L) + (J-1)*
     .                         (XYZNOD(LXYZ(NU,1),L) - XYZNOD(NB,L))/MZ
                  END DO
	          QBC(IND) = IST*UPNEW(K-1,I)
C           x-surf. = x flap
C
C	    XYZNOD(LXYZ(NU,1),1) = XYZNOD(NS,1)
		  
	       END DO
	    ELSE
	       DO J=1,MZ+1
	          IND = IB + (J-1)*NK + I - 1
c Bug on IST*UPNEW ?
	          QBC(IND) = IST*(DUPDT(K-1,I) + UPNEW(K-1,I)*
     .                                       (PHISS(IND) + PHIMM(IND)))
     .                     -(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
c	          QBC(IND) = IST*((DUPDT(K-1,I) + UPNEW(K-1,I)*
c     .                                       (PHISS(IND) + PHIMM(IND)))
c     .                     -(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND)))
c	          QBC(IND) = IST*(DUPDT(K-1,I) + UPNEW(K-1,I)*
c     .                                       (PHISS(IND) + PHIMM(IND))
c     .                     -(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND)))
	       END DO
	    END IF
         END DO
      ELSE IF(K.EQ.3.OR.K.EQ.5) THEN
         IST  = K - 4
	 IB   = INODF(K,1)
	 NK   = MX+1
	 NU1  = IB + MZ*NK
	 NU2  = NU1 + MX
C
	 IF(IFLAGV.EQ.1) THEN
C
C           Horizontal updating of snake absorbing piston
C
	    DO I=1,NK
	       DELXY(I) = UPNEW(K-1,I)*DT + DUPDT(K-1,I)*DT22
	    END DO
C
C           Change domain wo
C
	    IF(K.EQ.3) THEN
	       W0 = W0 - DELXY(1)
	    ELSE IF(K.EQ.5) THEN
               W0 = W0 + DELXY(1)
	    END IF
	    W02 = HALF*W0
            IF((IBOT.EQ.6).OR.(ISYM.EQ.1)) W02 = W0
            XSTART(1,2) = -W02
            XSTART(2,2) =  W02
            IF((IBOT.EQ.6).OR.(ISYM.EQ.1)) XSTART(2,2) = ZERO
            XSTART(3,2) = -W02
            XSTART(4,2) = -W02
            XSTART(5,2) = -W02
            IF((IBOT.EQ.6).OR.(ISYM.EQ.1)) XSTART(5,2) = ZERO
            XSTART(6,2) =  W02
            IF((IBOT.EQ.6).OR.(ISYM.EQ.1)) XSTART(6,2) = ZERO
C
C           Compatibility with lateral fixed boundaries
C
   	    IF(K.EQ.3.AND.IBCOND(2).EQ.0) THEN
	       XYZNOD(NU1,1)         = XYZNOD(LXYZ(NU1,2),1)
	       XYZNOD(LXYZ(NU1,1),1) = XYZNOD(NU1,1)
	    END IF
	    IF(K.EQ.3.AND.IBCOND(4).EQ.0) THEN
	       XYZNOD(NU2,1)         = XYZNOD(LXYZ(NU2,2),1)
	       XYZNOD(LXYZ(NU2,1),1) = XYZNOD(NU2,1)
	    END IF
	    IF(K.EQ.5.AND.IBCOND(4).EQ.0) THEN
	       XYZNOD(NU1,1)         = XYZNOD(LXYZ(NU1,2),1)
	       XYZNOD(LXYZ(NU1,1),1) = XYZNOD(NU1,1)
	    END IF
	    IF(K.EQ.5.AND.IBCOND(2).EQ.0) THEN
	       XYZNOD(NU2,1)         = XYZNOD(LXYZ(NU2,2),1)
	       XYZNOD(LXYZ(NU2,1),1) = XYZNOD(NU2,1)
	    END IF
	 END IF
C
         DO I=1,NK
            IF(IFLAGV.EQ.1) THEN
	       NB  = IB + I - 1
	       NU  = NB + MZ*NK
C
C              x-change on AP, bottom, and FS
C
               XYZNOD(NB,2) = XYZNOD(NB,2) + DELXY(I)
CBOT
	       IF(LXYZ(NB,1).NE.0.AND.IBOT.NE.10) THEN
	          XYZNOD(LXYZ(NB,1),2) = XYZNOD(NB,2)
	       END IF
	       IF(LXYZ(NB,2).NE.0.AND.IBOT.NE.10) THEN
   	          XYZNOD(LXYZ(NB,2),2) = XYZNOD(NB,2)
	       END IF
	       XYZNOD(NU,2)         = XYZNOD(NB,2)
	       XYZNOD(LXYZ(NU,1),2) = XYZNOD(NB,2)
C
C              x-change is as FS node except on the sides
C
	       IF((I.GT.1).AND.(I.LT.NK)) THEN
	          XP           = XYZNOD(LXYZ(NU,1),1)
	          XYZNOD(NB,2) = XP
	          XYZNOD(NU,2) = XP
	       ELSE
	          XP                   = XYZNOD(NU,1)
                  XYZNOD(LXYZ(NU,1),1) = XP
               END IF
C
	       DO J=1,MZ+1
	          IND = IB + (J-1)*(MX+1) + I - 1
		  XYZNOD(IND,2) = XYZNOD(NB,2)
	          DO L=1,3
		     IF(L.NE.2) THEN
                        XYZNOD(IND,L) = XYZNOD(NB,L) + (J-1)*
     .                         (XYZNOD(LXYZ(NU,1),L) - XYZNOD(NB,L))/MZ
                     END IF
                  END DO
	          QBC(IND) = IST*UPNEW(K-1,I)
	       END DO
	    ELSE
	       DO J=1,MZ+1
	          IND = IB + (J-1)*(MY+1) + I - 1
c Bug on IST? should be IST*UPNEW
	          QBC(IND) = IST*(DUPDT(K-1,I) + UPNEW(K-1,I)*
     .                                       (PHISS(IND) + PHIMM(IND)))
     .                     -(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
c	          QBC(IND) = IST*((DUPDT(K-1,I) + UPNEW(K-1,I)*
c     .                                       (PHISS(IND) + PHIMM(IND)))
c     .                     -(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND)))
c	          QBC(IND) = IST*(DUPDT(K-1,I) + UPNEW(K-1,I)*
c     .                                       (PHISS(IND) + PHIMM(IND))
c     .                     -(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND)))
               END DO
	    END IF
         END DO
      END IF
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE PADSIN(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C----------------------------------------------------------------------
C0  PADSINF   PADSIN
C1  PURPOSE   Simulation of a paddle wavemaker oscillating on the
C1            lateral boundary K(2,4). Both geometry and B.C. are updat.
C1            or only the B.C. (IFLAGV=1,2). The paddle motion is damped
C1            according to D(t,Tdamp) (1 if Tdamp <eps). The motion is :
C1            for z=0 :
C1
C1            Xp(t)=TU*S(i=1,NH) [Ap(i)*cos(Op(i)*t+Sp(i)+pi/2))*D(t)]
C1            Up(t)=d Xp(t)/dt, d Up(t)/dt= d2Xp(t)/dt2
C1            D(t) =(tanh(mu*to)+tanh(mu*(t-to)))/(1+tanh(mu*to)
C1            mu=2.3025/to
C1
C1            Xf the motion, up,upt the velocity and acceleration (z=0)
C1
C2  CALL PADSIN(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C3  CALL ARG. XYZNOD()            = Coordin. of nofes for current step
C3            PHIS(NOM),PHISS     = Phis and Phiss imposed on boundaries
C3            PHIM(NOM),PHIMM     = Phim and Phimm imposed on boundaries
C3            PHINS(NOM),PHINM    = Phins and Phinm imposed on boundaries
C3            QBC(NOM)            = du/dn imposed on Neuman boundaries
C3            K                   = Considered later. boundary (2,or 4)
C3            TIME                = Current time
C3            TUNE                = Tune the overall motion >0
C3            DE(2)               = Depth at K=2,4
C3            TDAMP=2*T0          = 1% Damping time (0: no,>0: yes)
C3            IFLAGV              = (1,2) update geom.+B.C. or B.C. only
C3            IPTYP               = 1 sine or sume of sine waves
C3                                = 8 wave focusing at (0,DF)
C3
C4  RET. ARG. XYZNOD(NOM,3),QBC(NOM)  on K
C9  Feb. 99   S. GRILLI, INLN
CLPADSIN SUB. WHICH SIMULATE MOTION AND KINEMATICS OF PADDLE WAVEMAKER
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM0(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM3(66),INODF(6,2),IBCOND(6)
c      COMMON/SYSTEM/  DUM3(24),TIME,TDAMP,TDOWN,DUM31(4+8*MYMAX),IDU(2),
c     .                IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOUNDC/ DUM4(9),DE(2),OMEGA,TUNE,DUM6,IDW
      COMMON /PADDLE/ AP(NFMAX,NHMAX),OP(NFMAX),SP(NHMAX),ZKWAVE(NFMAX),
     .                DISTF,TFOC,ALPHAF,WMIDTH,OS(MYMAX),OST(MYMAX),
     .                OSTT(MYMAX),DAMP,DAMPT,DAMPTT,DELFR,NFR,NH,IFREQ
C
      DIMENSION QBC(NOM),PHIS(NOM),PHIM(NOM),PHISS(NOM),PHIMM(NOM),
     .          PHINS(NOM),PHINM(NOM)
C
      DATA ONE/1.D0/,TWO/2.D0/,ZERO/0.D0/,EPS/1.D-08/,
     .     HALF/0.5D0/,CD/2.3025D0/,FOUR/4.D0/,PI/3.1415926535898D0/
C
       SAVE
C     SAVE /MAILLE/,/SYSTEM/,DATGEN/,/BOUNDC/,/PADDLE/
C
c#############################################################################
c	inputs: potential en curvilinear coordinates (s,n,m)
c	outputs: UBC=velocity, QBC=du/dn
c
c	DEC: local depth a the boundary K
c	IB: first point of the boundary K in the node table?
c

      DEC = DE(K/2)
      IST = 3 - K
      IB  = INODF(K,1)
C 
C
C.....Store parameters at beginning of time loop
C
      IF((IFLAGV.EQ.1).OR.(ILOOP.EQ.1)) THEN
C
C     ...Current damping
C
         T0     = HALF*TDAMP
         DAMP   = ZERO
         DAMPT  = ZERO
         DAMPTT = ZERO
C
         IF((T0.GT.EPS).AND.(TIME.LT.TDOWN+2*T0)) THEN
            ZMU  = CD/T0
            TA   = ZMU*(TIME-T0)
            TB   = ZMU*T0
            TC   = ZMU*(T0+TDOWN-TIME)
            THA  = DTANH(TA)
            THB  = DTANH(TB)
            THC  = DTANH(TC)
            DDEN = ONE + THB
            CH2  = (DCOSH(TA)**2)*DDEN
            CH3  = (DCOSH(TC)**2)*DDEN
C           
            DAMP1= (THB + THA)/DDEN 
            DAMP2= (THB + THC)/DDEN
C
            DAMP   = DAMP1*DAMP2
            DAMPT  =  ZMU/CH2*DAMP2 - DAMP1*ZMU/CH3
            DAMPTT = -ZMU*ZMU*TWO*(THA/CH2*DAMP2 + 1/(CH2*CH3) +
     1      THC/CH3*DAMP1)
C
C           Changement du signe
            DAMP=-DAMP
            DAMPT=-DAMPT
            DAMPTT=-DAMPTT
C          
C            Write(*,*) 'TIME=',TIME   
         END IF
C
C     ...Current motion at z=0 without damping
C
         DO I=1,MY+1
            OS(I)   = ZERO
            OST(I)  = ZERO
            OSTT(I) = ZERO
         END DO
C
         DO IFR=1,NFR
          OPTI = OP(IFR)*(TIME-TFOC-0.018/TWO*TIME*TIME)
	  IF(NFR.EQ.NH.AND.IFREQ.EQ.1) THEN
	    NH2 = 1
	    IH = IFR
	  ELSE
	    NH2=NH
	  END IF
          DO IH=1,NH2
	    IF(IPTYP.EQ.1) THEN
	       THETAN = ZERO
	       PHASE  = SP(IH) + HALF*PI
	    ELSE IF(IPTYP.EQ.8) THEN
	       THETAN = SP(IH)
	       PHASE  = ZKWAVE(IFR)*DISTF*DCOS(THETAN) + HALF*PI
	    END IF
            AMPL = AP(IFR,IH)*TUNE
C
            CO   = DCOS(PHASE + OPTI)
            SI   = DSIN(PHASE + OPTI)
C
C           y of free surface is used on wm. for vertical snake lines
C
            DO I=1,MY+1
	       NS  = IB + MZ*(MY+1) + I - 1
C
	       IF((I.GT.1).AND.(I.LE.MY)) THEN
	          YP = XYZNOD(LXYZ(NS,1),2)
	       ELSE
	          YP = XYZNOD(NS,2)
	       END IF
	       ZKY = ZKWAVE(IFR)*DSIN(THETAN)
	       CY  = DCOS(ZKY*YP)
	       SY  = DSIN(ZKY*YP)
C
               OS(I)   = OS(I)   + AMPL*(CY*CO + SY*SI)
               OST(I)  = OST(I)  + AMPL*OP(IFR)*(-1.0*CY*SI + 
     1              SY*CO)*(ONE - 0.018*TIME)
               OSTT(I) = OSTT(I) - AMPL*OP(IFR)*OP(IFR)*(CY*CO + SY*SI)
     1          *(ONE - 0.018*TIME) + (-CY*SI + SY*CO)*(-0.018)
C
	    END DO 
          END DO
         END DO
      END IF
C
C.....Updating boundary conditions and/or geometry (IFLAG=1,2)
C     Assumed to be on boundary K=2 or 4
C
      IF(IFLAGV.EQ.1) THEN
         DO I=1,MY+1
	    NB  = IB + I - 1
	    NS  = NB + MZ*(MY+1)
C
C           Current motion at z=0
C
            XP  = OS(I)*DAMP
            UP  = OST(I)*DAMP + OS(I)*DAMPT
            PL  = DEC*DEC + XP*XP
            DPL = DSQRT(PL)
            TT  =-DEC*UP/PL
C
C           Node coord. updating : x(0)=XP; z-free surface is forced
C           on top; and y-free surface on top and bottom of flap wm.
C
	    IF((I.GT.1).AND.(I.LE.MY)) THEN
	       YP = XYZNOD(LXYZ(NS,1),2)
	       XYZNOD(NB,2) = YP
	       XYZNOD(NS,2) = YP
	    ELSE
	       YP = XYZNOD(NS,2)
               XYZNOD(LXYZ(NS,1),2) = YP
            END IF
	    ZFS = XYZNOD(LXYZ(NS,1),3)
	    XYZNOD(NS,3) = ZFS
	    XYZNOD(NS,1) = XP*(ONE + ZFS/DEC)
C
	    DO J=1,MZ+1
	       IND = IB + (J-1)*(MY+1) + I - 1
	       DO KK=1,3
                  XYZNOD(IND,KK) = XYZNOD(NB,KK) + (J-1)*
     .                           (XYZNOD(NS,KK) - XYZNOD(NB,KK))/MZ
               END DO
	       RG       = (ONE + XYZNOD(IND,3)/DEC)*DPL
               QBC(IND) = QBC(IND) + RG*TT*IST
	    END DO
C
C           x-surf. = x flap
C
	    XYZNOD(LXYZ(NS,1),1) = XYZNOD(NS,1)
         END DO
      ELSE
         DO I=1,MY+1
	    NB  = IB + I - 1
C
C           Current motion at z=0
C
            XP  = OS(I)*DAMP
            UP  = OST(I)*DAMP + OS(I)*DAMPT
            UPT = OSTT(I)*DAMP + TWO*OST(I)*DAMPT + OS(I)*DAMPTT
            PL  = DEC*DEC + XP*XP
            DPL = DSQRT(PL)
C
            TT  =  -DEC*UP/PL
C ajout Up je pense qu'il y avait une erreur
            TTT = -(DEC*UPT + TWO*UP*XP*TT)/PL
C old line      TTT = -(DEC*UPT + TWO*XP*TT)/PL
	    WRITE(97,*) TIME,I,XP,UP,UPT,TT,TTT
C
	    DO J=1,MZ+1
	       IND      = IB + (J-1)*(MY+1) + I - 1
	       RG       = (ONE + XYZNOD(IND,3)/DEC)*DPL
               QBC(IND) = RG*IST*(TTT + TT*(PHISS(IND) + PHIMM(IND)))
     .                  -IST*(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
            END DO
         END DO
      END IF
      write(142,*) TIME, XP, UP
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE PADFILE(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C----------------------------------------------------------------------
C     PADFILE: boundary condition simulating a paddle whose angular
C     motion is 
C     given by a file
C     
C     Author: Raphael Poncet, CMLA, ENS Cachan
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      REAL EXPDATA(90001)
      
      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM0(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .     IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .     IDUM3(66),INODF(6,2),IBCOND(6)
c      COMMON/SYSTEM/  DUM3(24),TIME,TDAMP,TDOWN,DUM31(4+8*MYMAX),IDU(2),
c     .     IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOUNDC/ DUM4(9),DE(2),OMEGA,TUNE,DUM6,IDW
      COMMON /PADDLE/ AP(NFMAX,NHMAX),OP(NFMAX),SP(NHMAX),ZKWAVE(NFMAX),
     .     DISTF,TFOC,ALPHAF,WMIDTH,OS(MYMAX),OST(MYMAX),
     .     OSTT(MYMAX),DAMP,DAMPT,DAMPTT,DELFR,NFR,NH,IFREQ
C     
      DIMENSION QBC(NOM),PHIS(NOM),PHIM(NOM),PHISS(NOM),PHIMM(NOM),
     .     PHINS(NOM),PHINM(NOM)
C     
      DATA ONE/1.D0/,TWO/2.D0/,ZERO/0.D0/,EPS/1.D-08/,
     .     HALF/0.5D0/,CD/2.3025D0/,FOUR/4.D0/,PI/3.1415926535898D0/
C     
      SAVE
C     SAVE /MAILLE/,/SYSTEM/,DATGEN/,/BOUNDC/,/PADDLE/
C     

C load experimental data - file length is hardcoded
      
      OPEN( unit=50, file="theta.txt" )
      
      DO I = 1, 90001
         READ(50, *) X, EXPDATA(I)
      END DO

      CLOSE(50)

      
      DEC = DE(K/2)
      IST = 3 - K
      IB  = INODF(K,1)
C     
C     
C.....Store parameters at beginning of time loop
C     
      IF((IFLAGV.EQ.1).OR.(ILOOP.EQ.1)) THEN
C     
C     ...Current damping
C     
         T0     = HALF*TDAMP
         DAMP   = ZERO
         DAMPT  = ZERO
         DAMPTT = ZERO
C     

C     
C     ...Current motion at z=0 without damping
C     
         DO I=1,MY+1
            OS(I)   = ZERO
            OST(I)  = ZERO
            OSTT(I) = ZERO
         END DO
C     
         DO IFR=1,NFR
            OPTI = OP(IFR)*(TIME-TFOC-0.018/TWO*TIME*TIME)
            IF(NFR.EQ.NH.AND.IFREQ.EQ.1) THEN
               NH2 = 1
               IH = IFR
            ELSE
               NH2=NH
            END IF
            DO IH=1,NH2
               IF(IPTYP.EQ.1) THEN
                  THETAN = ZERO
                  PHASE  = SP(IH) + HALF*PI
               ELSE IF(IPTYP.EQ.8) THEN
                  THETAN = SP(IH)
                  PHASE  = ZKWAVE(IFR)*DISTF*DCOS(THETAN) + HALF*PI
               END IF
               AMPL = AP(IFR,IH)*TUNE
C     
               CO   = DCOS(PHASE + OPTI)
               SI   = DSIN(PHASE + OPTI)
C     
C     y of free surface is used on wm. for vertical snake lines
C     
               DO I=1,MY+1
                  NS  = IB + MZ*(MY+1) + I - 1
C     
                  IF((I.GT.1).AND.(I.LE.MY)) THEN
                     YP = XYZNOD(LXYZ(NS,1),2)
                  ELSE
                     YP = XYZNOD(NS,2)
                  END IF
                  ZKY = ZKWAVE(IFR)*DSIN(THETAN)
                  CY  = DCOS(ZKY*YP)
                  SY  = DSIN(ZKY*YP)
C     
                  OS(I)   = OS(I)   + AMPL*(CY*CO + SY*SI)
                  OST(I)  = OST(I)  + AMPL*OP(IFR)*(-1.0*CY*SI + 
     1                 SY*CO)*(ONE - 0.018*TIME)
                  OSTT(I) = OSTT(I) - AMPL*OP(IFR)*OP(IFR)*(CY*CO + SY
     $                 *SI)*(ONE - 0.018*TIME) + (-CY*SI + SY*CO)*(
     $                 -0.018)
C     
               END DO 
            END DO
         END DO
      END IF
C     
C.....Updating boundary conditions and/or geometry (IFLAG=1,2)
C     Assumed to be on boundary K=2 or 4
C     
      IF(IFLAGV.EQ.1) THEN
         DO I=1,MY+1
	    NB  = IB + I - 1
	    NS  = NB + MZ*(MY+1)
C     
C     Current motion at z=0
C     
            XP  = OS(I)*DAMP
            UP  = OST(I)*DAMP + OS(I)*DAMPT
            PL  = DEC*DEC + XP*XP
            DPL = DSQRT(PL)
            TT  =-DEC*UP/PL
C     
C     Node coord. updating : x(0)=XP; z-free surface is forced
C     on top; and y-free surface on top and bottom of flap wm.
C     
	    IF((I.GT.1).AND.(I.LE.MY)) THEN
	       YP = XYZNOD(LXYZ(NS,1),2)
	       XYZNOD(NB,2) = YP
	       XYZNOD(NS,2) = YP
	    ELSE
	       YP = XYZNOD(NS,2)
               XYZNOD(LXYZ(NS,1),2) = YP
            END IF
	    ZFS = XYZNOD(LXYZ(NS,1),3)
	    XYZNOD(NS,3) = ZFS
	    XYZNOD(NS,1) = XP*(ONE + ZFS/DEC)
C     
	    DO J=1,MZ+1
	       IND = IB + (J-1)*(MY+1) + I - 1
	       DO KK=1,3
                  XYZNOD(IND,KK) = XYZNOD(NB,KK) + (J-1)*
     .                 (XYZNOD(NS,KK) - XYZNOD(NB,KK))/MZ
               END DO
	       RG       = (ONE + XYZNOD(IND,3)/DEC)*DPL
               QBC(IND) = QBC(IND) + RG*TT*IST
	    END DO
C     
C     x-surf. = x flap
C     
	    XYZNOD(LXYZ(NS,1),1) = XYZNOD(NS,1)
         END DO
      ELSE
         DO I=1,MY+1
	    NB  = IB + I - 1
C     
C     Current motion at z=0
C     
            XP  = OS(I)*DAMP
            UP  = OST(I)*DAMP + OS(I)*DAMPT
            UPT = OSTT(I)*DAMP + TWO*OST(I)*DAMPT + OS(I)*DAMPTT
            PL  = DEC*DEC + XP*XP
            DPL = DSQRT(PL)
C     
            TT  =  -DEC*UP/PL
C     ajout Up je pense qu'il y avait une erreur
            TTT = -(DEC*UPT + TWO*UP*XP*TT)/PL
C     old line      TTT = -(DEC*UPT + TWO*XP*TT)/PL
	    WRITE(97,*) TIME,I,XP,UP,UPT,TT,TTT
C     
	    DO J=1,MZ+1
	       IND      = IB + (J-1)*(MY+1) + I - 1
	       RG       = (ONE + XYZNOD(IND,3)/DEC)*DPL
               QBC(IND) = RG*IST*(TTT + TT*(PHISS(IND) + PHIMM(IND)))
     .              -IST*(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
            END DO
         END DO
      END IF
      write(142,*) TIME, XP, UP
C     
      RETURN
C     
      END
C----------------------------------------------------------------------
      SUBROUTINE PADSIN_FB(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C----------------------------------------------------------------------
C0  PADSINF   PADSIN
C1  PURPOSE   Simulation of a paddle wavemaker oscillating on the
C1            lateral boundary K(2,4). Both geometry and B.C. are updat.
C1            or only the B.C. (IFLAGV=1,2). The paddle motion is damped
C1            according to D(t,Tdamp) (1 if Tdamp <eps). The motion is :
C1            for z=0 :
C1
C1            Xp(t)=TU*S(i=1,NH) [Ap(i)*cos(Op(i)*t+Sp(i)+pi/2))*D(t)]
C1            Up(t)=d Xp(t)/dt, d Up(t)/dt= d2Xp(t)/dt2
C1            D(t) =(tanh(mu*to)+tanh(mu*(t-to)))/(1+tanh(mu*to)
C1            mu=2.3025/to
C1
C1            Xf the motion, up,upt the velocity and acceleration (z=0)
C1
C2  CALL PADSIN(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C3  CALL ARG. XYZNOD()            = Coordin. of nofes for current step
C3            PHIS(NOM),PHISS     = Phis and Phiss imposed on boundaries
C3            PHIM(NOM),PHIMM     = Phim and Phimm imposed on boundaries
C3            PHINS(NOM),PHINM    = Phins and Phinm imposed on boundaries
C3            QBC(NOM)            = du/dn imposed on Neuman boundaries
C3            K                   = Considered later. boundary (2,or 4)
C3            TIME                = Current time
C3            TUNE                = Tune the overall motion >0
C3            DE(2)               = Depth at K=2,4
C3            TDAMP=2*T0          = 1% Damping time (0: no,>0: yes)
C3            IFLAGV              = (1,2) update geom.+B.C. or B.C. only
C3            IPTYP               = 7 wave focusing at (0,DF), ECN wavemaker geometry
C3
C4  RET. ARG. XYZNOD(NOM,3),QBC(NOM)  on K
C9  Feb. 99   S. GRILLI, INLN
C9  Jun. 09   F. BONNEFOY ECN
CLPADSIN SUB. WHICH SIMULATE MOTION AND KINEMATICS OF PADDLE WAVEMAKER
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
C
      INCLUDE 'param.inc'
C
      COMMON/DATGEN/  DUM0(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM3(66),INODF(6,2),IBCOND(6)
c      COMMON/SYSTEM/  DUM3(24),TIME,TDAMP,TDOWN,DUM31(4+8*MYMAX),IDU(2),
c     .                IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOUNDC/ DUM4(9),DE(2),OMEGA,TUNE,DUM6,IDW
      COMMON /PADDLE/ AP(NFMAX,NHMAX),OP(NFMAX),SP(NHMAX),ZKWAVE(NFMAX),
     .                DISTF,TFOC,ALPHAF,WMIDTH,OS(MYMAX),OST(MYMAX),
     .                OSTT(MYMAX),DAMP,DAMPT,DAMPTT,DELFR,NFR,NH,IFREQ
C
      DIMENSION QBC(NOM),PHIS(NOM),PHIM(NOM),PHISS(NOM),PHIMM(NOM),
     .          PHINS(NOM),PHINM(NOM)
C
      DATA ONE/1.D0/,TWO/2.D0/,ZERO/0.D0/,EPS/1.D-08/,
     .     HALF/0.5D0/,CD/2.3025D0/,FOUR/4.D0/,PI/3.1415926535898D0/
C
       SAVE
C     SAVE /MAILLE/,/SYSTEM/,DATGEN/,/BOUNDC/,/PADDLE/
C
C      WRITE(*,'(A,I1)') 'PADSIN_FB: IFLAGV = ', IFLAGV
      DEC = DE(K/2)
      IST = 3 - K
      IB  = INODF(K,1)
C
C.....Store parameters at beginning of time loop
C
      IF((IFLAGV.EQ.1).OR.(ILOOP.EQ.1)) THEN
C     ...ECN wave basin damping (linear, 3 sec.)
         T0     = 4.202D0
         DAMP   = ONE
         DAMPT  = ZERO
         DAMPTT = ZERO
C
         IF((TIME.LT.T0)) THEN
            ZMU    = ONE / T0
            DAMP   = ZMU * TIME
            DAMPT  = ZMU
            DAMPTT = ZERO
         END IF
C         WRITE(*,'(A,5(ES12.4,1X))') 'PADSIN_FB: Time ramp ',TIME, T0, 
C     .    DAMP, DAMPT, DAMPTT
C
C     ...Current motion at z=0 without damping
C
         DO I=1,MY+1
            OS(I)   = ZERO
            OST(I)  = ZERO
            OSTT(I) = ZERO
         END DO
C
         DO IFR=1,NFR
            OPTI = OP(IFR)*(TIME-TFOC)
            IF(NFR.EQ.NH.AND.IFREQ.EQ.1) THEN
               NH2 = 1
               IH = IFR
            ELSE
               NH2=NH
            END IF
            DO IH=1,NH2
C              IPTYP.EQ.7
               THETAN = SP(IH)
               YF     = 0.085D0
               PHASE  = ZKWAVE(IFR) * (DISTF*DCOS(THETAN) + 
     1         YF*DSIN(THETAN)) + HALF*PI
        
               AMPL = AP(IFR,IH)*TUNE
               CO   = DCOS(PHASE + OPTI)
               SI   = DSIN(PHASE + OPTI)
C
C           y of free surface is used on wm. for vertical snake lines
C
               DO I=1,MY+1
                  NS  = IB + MZ*(MY+1) + I - 1
C
                  IF((I.GT.1).AND.(I.LE.MY)) THEN
                     YP = XYZNOD(LXYZ(NS,1),2)
                  ELSE
                     YP = XYZNOD(NS,2)
                  END IF
                  ZKY = ZKWAVE(IFR)*DSIN(THETAN)
                  CY  = DCOS(ZKY*YP)
                  SY  = DSIN(ZKY*YP)
C
                  OS(I)   = OS(I)  +AMPL*(CY*CO + SY*SI)
                  OST(I)  = OST(I) +AMPL*OP(IFR)*(-CY*SI + SY*CO)
                  OSTT(I) = OSTT(I)-AMPL*OP(IFR)*OP(IFR)*(CY*CO+SY*SI)
               END DO
            END DO
         END DO
      END IF
C
C.....Updating boundary conditions and/or geometry (IFLAG=1,2)
C     Assumed to be on boundary K=2 or 4
C
      HINGE = 0.4294D0
      IF(IFLAGV.EQ.1) THEN
         DO I=1,MY+1
            NB = IB + I - 1
            NS = NB + MZ*(MY+1)
C
C           Current motion at z=0
C              wavemaker is a flap whose hinge is located at distance
C              HINGE from the bottom
C
            XP  = OS(I)*DAMP
            UP  = OST(I)*DAMP + OS(I)*DAMPT
            PL    = (DEC-HINGE)*(DEC-HINGE) + XP*XP
            DPL   = DSQRT(PL)
            TT    = -(DEC-HINGE)*UP/PL
C
C           Node coord. updating : x(0)=XP; z-free surface is forced
C           on top; and y-free surface on top and bottom of flap wm.
C
            IF((I.GT.1).AND.(I.LE.MY)) THEN
               YP = XYZNOD(LXYZ(NS,1),2)
               XYZNOD(NB,2) = YP
               XYZNOD(NS,2) = YP
            ELSE
               YP = XYZNOD(NS,2)
               XYZNOD(LXYZ(NS,1),2) = YP
            END IF
            ZFS = XYZNOD(LXYZ(NS,1),3)
            XYZNOD(NS,3) = ZFS
            XYZNOD(NS,1) = XP*(ONE + ZFS/(DEC-HINGE))
C         Bottom node
            XYZNOD(NB,1) = ZERO
            QBC(NB)      = QBC(NB) + ZERO
C         Hinge node
C            JHINGE = 2
            JHINGE = 3
            IHINGE = IB + (JHINGE-1)*(MY+1) + I - 1
            XYZNOD(IHINGE,3) = - DEC + HINGE
            XYZNOD(IHINGE,2) = XYZNOD(NB,2)
            XYZNOD(IHINGE,1) = ZERO
            QBC(IHINGE)      = QBC(IHINGE) + ZERO
C         Nodes below hinge
            DO J=1,JHINGE
               IND = IB + (J-1)*(MY+1) + I - 1
               XYZNOD(IND,1) = ZERO
               XYZNOD(IND,2) = XYZNOD(NB,2)
               XYZNOD(IND,3) = XYZNOD(NB,3) + (J-1)*
     1         (XYZNOD(IHINGE,3) - XYZNOD(NB,3))/(JHINGE - ONE)
               QBC(IND) = QBC(IND) + ZERO
            END DO
C         Nodes above hinge
            DO J=JHINGE,MZ+1
               IND = IB + (J-1)*(MY+1) + I - 1
               XYZNOD(IND,3) = XYZNOD(IHINGE,3) + (J-JHINGE)*
     1         (XYZNOD(NS,3) - XYZNOD(IHINGE,3))/(MZ+1-JHINGE)
               XYZNOD(IND,2) = XYZNOD(NB,2)
               XYZNOD(IND,1) = XP * (ONE + XYZNOD(IND,3)/(DEC-HINGE))
               RG       = (ONE + XYZNOD(IND,3)/(DEC-HINGE))*DPL
               QBC(IND) = QBC(IND) + RG*TT*IST
            END DO
C
C           x-surf. = x flap
C
            XYZNOD(LXYZ(NS,1),1) = XYZNOD(NS,1)
         END DO
C         WRITE(*,'(A)') 'PADSIN_FB: Writing wmk.dat'
C         OPEN(12345,FILE='wmk.dat',STATUS='UNKNOWN')
C         WRITE(12345,'(A)') 'VARIABLES="Iy","Iz","x","y","z","Q"'
C         DO I=1,MY+1
C            DO J=1,MZ+1
C               IND = IB + (J-1)*(MY+1) + I - 1
C               WRITE(12345,'(2(I6,1X),4(ES12.3,1X))') I, J, 
C     .          XYZNOD(IND,1), XYZNOD(IND,2), XYZNOD(IND,3), QBC(IND)
C            END DO
C         END DO
C         CLOSE(12345)
C         WRITE(*,'(A)') 'PADSIN_FB: Writing wmk.dat: done'
      ELSE
         DO I=1,MY+1
            NB = IB + I - 1
C
C           Current motion at z=0
C
            XP  = OS(I)*DAMP
            UP  = OST(I)*DAMP + OS(I)*DAMPT
            UPT = OSTT(I)*DAMP + TWO*OST(I)*DAMPT + OS(I)*DAMPTT
            PL  = (DEC-HINGE) * (DEC-HINGE) + XP*XP
            DPL = DSQRT(PL)
C
            TT  =  -(DEC-HINGE)*UP/PL
            TTT = -((DEC-HINGE)*UPT + TWO*UP*XP*TT)/PL
            WRITE(97,*) TIME,I,XP,UP,UPT,TT,TTT
C
C         Nodes below hinge
            QBC(NB) = ZERO
C            J       = 2
            J       = 3
            IHINGE  = IB + (J-1)*(MY+1) + I - 1
C            QBC(IHINGE) = ZERO
            DO J=1,3
               IND = IB + (J-1)*(MY+1) + I - 1
               QBC(IND) = ZERO
            END DO
C         Nodes above hinge
            DO J=3,MZ+1
               IND = IB + (J-1)*(MY+1) + I - 1
               RG       = (ONE + XYZNOD(IND,3)/(DEC-HINGE))*DPL
               QBC(IND) = RG*IST*(TTT + TT*(PHISS(IND) + PHIMM(IND)))
     1                  -IST*(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
            END DO
         END DO
C         WRITE(*,'(A)') 'PADSIN_FB: Writing flux.dat'
C         OPEN(12345,FILE='flux.dat',STATUS='UNKNOWN')
C         WRITE(12345,'(A)') 'VARIABLES="Iy","Iz","Q"'
C         DO I=1,MY+1
C            DO J=1,MZ+1
C               IND = IB + (J-1)*(MY+1) + I - 1
C               WRITE(12345,'(2(I6,1X),1(ES12.3,1X))') I, J, QBC(IND)
C            END DO
C         END DO
C         CLOSE(12345)
C         WRITE(*,'(A)') 'PADSIN_FB: Writing flux.dat: done'
      END IF
C
      write(142,*) TIME, XP, UP, UPT
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE PADSOL(K,QBC,PHISS,PHIMM)
C----------------------------------------------------------------------
C0  PADSOLF   PADSOL
C1  PURPOSE   Simulation of a piston wavemaker motion according to
C1            Goring's first order KdV wave solution on the
C1            lateral boundary K(2,4). Both geometry and B.C. are updat.
C1            or only the B.C. (IFLAGV=1,2).
C1            Xp the motion, Up,dUp/dt, the velocity and accel. with :
C1            Phin =Up*(K-4)
C1            Phitn=(K-4)*( dUp/dt - Up*(Phiss + Phimm) )
C1            When Omega=0 (or T=infty), solitary waves are generated,
C1            and when Omega different from 0 (or T=2pi/omeg)),cnoidal
C1            waves (H,T,d) are generated, starting for eta=up=0.
C1
C2  CALL PADSOL(K,QBC,PHISS,PHIMM)
C3  CALL ARG. XYZNOD()            = Coordin. of nofes for current step
C3            PHIS(NOM),PHISS     = Phis and Phiss imposed on boundaries
C3            PHIM(NOM),PHIMM     = Phim and Phimm imposed on boundaries
C3            PHINS(NOM),PHINM    = Phins and Phinm imposed on boundaries
C3            QBC(NOM)            = du/dn imposed on Neuman boundaries
C3            K                   = Considered later. boundary (2,or 4)
C3            TIME                = Current time
C3            GE                  = Acceleration of gravity
C3            H                   = Solitary wave amplitude
C3            DE(2)               = Depth at K=2,4
C3            IFLAGV              = (1,2) update geom.+B.C. or B.C. only
C3            ILOOP               = Loop in temporal iterations
C3
C4  RET. ARG. XYZNOD(NOM,3),QBC(NOM)  on K
C6  INT. CALL SOLITA, CNIDAL
C9  Feb. 99   S. GRILLI, INLN
CLPADSOL SUB. WHICH SIMULATE MOTION AND KIN. OF GORING PISTON WAVEMAKER
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM3(66),INODF(6,2),IBCOND(6)
c      COMMON/SYSTEM/  DUM3(24),TIME,TDAMP,DUM31(5+8*MYMAX),IDU(2),
c     .                IFLAGV,IDV,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOUNDC/ DUM4(8),GE,DE(2),OMEGA,H,DUM6,IDW
c	Old line
c      COMMON /PATTLE/ XP,UP,DUPDT
c	New line, XB
      COMMON /PATTLE/ XI,XIT,XITT
C
      DIMENSION QBC(NOM),PHISS(NOM),PHIMM(NOM)
C
      DATA TWO/2.D0/,EPS/1.D-03/,PI/3.1415926535898D0/
C
       SAVE
C     SAVE /MAILLE/,/SYSTEM/,DATGEN/,/BOUNDC/,/PATTLE/
C
      DEC = DE(K/2)
      IST = K - 3
      IB  = INODF(K,1)
C
      IF(IFLAGV.EQ.1) THEN
         XP = XYZNOD(IB,1)
C
         IF(OMEGA.LT.EPS) THEN
C
C           Generation of a solitary wave
C
            CALL SOLITA(TIME,GE,DEC,H)
C           -----------
         ELSE
C
C           Generation of a cnoidal wave
C
            T = TWO*PI/OMEGA
C
            CALL CNIDAL(TIME,GE,DEC,T,H,ILOOP)
C           -----------
         END IF
C
C        Geometry and phin updating
C
         DO I=1,MY+1
	    NB  = IB + I - 1
	    NU  = IB + MZ*(MY+1) + I - 1
	    ZUN = XYZNOD(LXYZ(NU,1),3)
	    ZK  = (ZUN - XYZNOD(NB,3))/(XYZNOD(NU,3) - XYZNOD(NB,3))
	    DO J=1,MZ+1
	       IND = IB + (J-1)*(MY+1) + I - 1
	       DO K=1,3
                  XYZNOD(IND,K) = XYZNOD(NB,K) + (J-1)*
     .                           (XYZNOD(NU,K) - XYZNOD(NB,K))*ZK/MZ
               END DO
c	Old line
c               QBC(IND) = UP*IST
c	New line, XB
               QBC(IND) = XIT*IST

	    END DO
         END DO
      ELSE
C
C        phitn updating
C
         DO I=1,MY+1
	    DO J=1,MZ+1
	       IND = IB + (J-1)*(MY+1) + I - 1
c	Old line
c               QBC(IND) = IST*(DUPDT - UP*(PHISS(IND) + PHIMM(IND)))
c	New line, XB
               QBC(IND) = IST*(XITT - XIT*(PHISS(IND) + PHIMM(IND)))
	    END DO
         END DO
      END IF
C
      RETURN
C
      END
C
C----------------------------------------------------------------------
      SUBROUTINE SOLITA(T,GE,DE,H)
C----------------------------------------------------------------------
C0  SOLITAF   SOLITA
C1  PURPOSE   Compute values for Goring's first order solution : motion
C1            Xi, velocity Xit and acceleration (Lagrangian) Xitt, at t.
C1            The equations are solved iteratively by Newton's method.
C1            XIP,F3 and F4 are the dimensionless equiv. of Xi,Xit,Xitt.
C1
C2  CALL SOLITA(T,GE,DE,H)
C3  CALL ARG. T                   = Current time
C3            GE                  = Acceleration of gravity
C3            DE                  = Depth at the piston wavemaker
C3            H                   = Solitary wave amplitude
C3
C4  RET. ARG. XI,XIT,XITT
C9  87-89     S. GRILLI, Civil Engng., Univ. of Delaware (Copyright)
CLSOLITA SUB. WHICH COMPUTES GORING SOLUTION AT TIME T
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      COMMON /PATTLE/ XI,XIT,XITT
C
      DATA ONE/1.D0/,TWO/2.D0/,THREE/3.D0/,TPE/3.8D0/,NMAX/50/,
     .     EPS/1.D-06/
C
       SAVE
C     SAVE /PATTLE/
C
C.....Formula functions
C
      ARGU(XIC) = ZK*(CEL*TN - (XIC+ZL))
C
      F1(ARG) = ZH*(DTANH(ARG)-DTANH(-ZK*ZL))/ZK
      F2(ARG) = ONE + ZH/(DCOSH(ARG)**2)
      F3(ARG) = ZH*CEL/(ZH+DCOSH(ARG)**2)
      F4(ARG) = -TWO*ZH*CEL*CEL*ZK*DSINH(ARG)*(DCOSH(ARG)/
     .           (ZH+DCOSH(ARG)**2) )**3
C
C.....Dimensionless variables
C
      ZH  = H/DE
      TN  = T*DSQRT(GE/DE)
      CEL = DSQRT(ONE+ZH)
      XIP = XI/DE
      IF(DABS(XIP).LT.EPS) THEN
         XIP = EPS
      END IF
C
C.....Constants
C
      ZK = DSQRT(THREE*ZH)/TWO
      ZL = TPE/ZK
C
C.....Computation of XI(t) by the Newton's method
C
      N = 0
      TEST = ONE
C
      DO WHILE(TEST.GT.EPS.AND.N.LT.NMAX)
         DXI = (XIP-F1(ARGU(XIP)))/F2(ARGU(XIP))
         XIP = XIP - DXI
         N   = N + 1
         IF(DABS(XIP).LT.EPS) THEN
            TEST = DABS(DXI)
         ELSE
            TEST = DABS(DXI/XIP)
         END IF
      END DO
C
C.....Resulting variables
C
      XI   = XIP*DE
      XIT  = F3(ARGU(XIP))*DE*DSQRT(GE/DE)
      XITT = F4(ARGU(XIP))*GE
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE CNIDAL(TIME,GE,DE,T,H,ILOOP)
C----------------------------------------------------------------------
C0  CNIDALF   CNIDAL
C1  PURPOSE   Generation of cnoidal waves by a piston wavemaker motion,
C1            according to Goring's first order KdV wave solution.
C1            At first loop (ILOOP=1), cnoidal wave characteristics are
C1            calculated for (H,T,d) as m,L,c,U,K,E. Then for time t,
C1            the motion xp, velocity Up and acceleration dUp/dt are
C1            calculated. The method is accurate for U=20-850, or 1-m=
C1            0.226 to 2.19D-10, and for L/d>20 and H/d<0.3.
C1
C2  CALL CNIDAL(TIME,GE,DE,T,H,ILOOP)
C3  CALL ARG. TIME                = Current time
C3            GE                  = Acceleration of gravity
C3            H                   = Cnoidal wave amplitude
C3            T                   = Cnoidal wave period
C3            DE                  = Depth at piston wavemaker
C3            ILOOP               = Loop in temporal iterations
C4  RET. ARG. M=m: parameter of elliptic integrals
C4            MP=1-m: complementary parameter of elliptic integrals
C4            WH,WT=Dimensionless wave height and period.
C4            WL,WC,WU=Dimensionless wave length, celerity, Ursell param
C4            THE0,XP0,T0=Initial characteristics for paddle
C4            AM,BM=Wave coefficients for celerity and trough
C4  RET. ARG. X(NOM),Z(NOM),QBC(NOM)   on K
C6  INT. CALL CHARAC, TRAJEC
C9  Oct. 90   S. GRILLI, Civil Engng., Univ. of Delaware (Copyright)
CLCNIDAL SUB. WHICH SIMULATE MOTION AND KIN. OF CNOIDAL WAVES
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NMAX=100)

      REAL*8 MP,M,KK
C
      COMMON /GATAB/ A(NMAX),B(NMAX),C(NMAX),KK,EE,MP,N,IFLAG
      COMMON /CNOWA/ WH,WT,WC,WL,WU,M,AM,BM,XP0,T0
      COMMON /PATTLE/ XI,XIT,XITT
C
       SAVE
C     SAVE /GATAB/,/CNOWA/,/PATTLE/
C
      IFLAG = 0
C
C.....Dimensionless time
C
      TT  = TIME*DSQRT(GE/DE)
C
C.....Caracter. for cnoidal wave (H,T,d) and print results on FI6
C
      IF(ILOOP.EQ.1) THEN
         WH = H/DE
         WT = T*DSQRT(GE/DE)
         CALL CHARAC
C        -----------
C
         WRITE(6,2000) WH,WT,WL,WU,EE,KK,WC,BM*WH,XP0,T0,M,MP
      END IF
C
C.....Computes paddle trajectory for time T & velocity and acceleration
C
      XP = XI*DE+XP0
C
      CALL TRAJEC(TT+T0,XP,XPT,XPTT)
C     -----------
C
      XI = (XP-XP0)*DE
      XIT = XPT*DSQRT(GE*DE)
      XITT= XPTT*GE
C
      RETURN
C
 2000 FORMAT(//15X,'THE CNOIDAL WAVE PARAMETERS ARE:'//
     .       10X,'H/D                              ',F12.4/
     .       10X,'T/SQRT(D/G)                      ',F12.2/
     .       10X,'L/H0                             ',F12.2/
     .       10X,'U   (H*L**2/D**3)                ',F12.2/
     .       10X,'E(m)                             ',F12.4/
     .       10X,'K(m)                             ',F12.4/
     .       10X,'C/SQRT(G*D)                      ',F12.4/
     .       10X,'Eta2/D                           ',F12.4/
     .       10X,'XP0/D                            ',E12.6/
     .       10X,'TO/SQRT(D/G)                     ',F12.4/
     .       10X,'ELLIPTIC PARAMETER m             ',E12.6/
     .       10X,'COMPLEMENTARY PARAMETER  1-m     ',E12.6)
C
      END
C----------------------------------------------------------------------
      SUBROUTINE CHARAC
C----------------------------------------------------------------------
C0  CHARACF   CHARAC
C1  PURPOSE   This subroutine computes the complementary elliptic
C1            parameter, given the wave height and period, for
C1            cnoidal waves.
C2  CALL      CALL CHARAC
C3  CALL ARG. IFLAG=(0/1) tables of geom.-arith. mean do or don't exist
C3            WH,WT=Dimensionless wave height and period.
C4  RET. ARG. M=m: parameter of elliptic integrals
C4            MP=1-m: complementary parameter of elliptic integrals
C4            WL,WC,WU=Dimensionless wave length, celerity, Ursell param
C4            THE0,XP0,T0=Initial characteristics for paddle
C4            AM,BM=Wave coefficients for celerity and trough
C6  INT. CALL ERRORS
CE  ERRORS    01 = Results do not converge for these characteristics
C9  Oct. 90   J. Kirby, S. Grilli, Civil Engng. Univ. of Delaware (Copyr
CLCHARAC SUB. COMPUTING CHARACTERISTICS OF CNOIDAL WAVES FROM (H,T)
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NMAX=100)

      REAL*8 MP,M,KK,MP01
      CHARACTER*8 TEXTE1
C
      EXTERNAL ACN,EINC
C
      COMMON /GATAB/ DUM(3*NMAX),KK,EE,MP,IDUM(2)
      COMMON /CNOWA/ WH,WT,WC,WL,WU,M,AM,BM,XP0,T0
C
      DATA ONE/1.D0/,THREE/3.D0/,SEIZE/16.D0/,ZERO/0.D0/,
     .     TWO/2.D0/,P001/0.001D0/,TENTH/0.1D0/,TEXTE1/'CHARAC01'/
C
       SAVE
C     SAVE /GATAB/,/CNOWA/
C
      CONST = THREE*WH*WT*WT/SEIZE
C
C.....Find superior root for m
C
      MP  = P001
      I   = 0
      GHT = ZERO
C
      DO WHILE(GHT.LT.ZERO.AND.I.LT.15)
         MP = MP*TENTH
         I = I+1
         CALL FCTS(CONST,GHT,DUMM)
C        ---------
      END DO
C
      MP01 = MP
C
      CALL NEWTON(CONST,MP01)
C     -----------
C
C.....Computes final elliptic integrals K(m), E(m) for MP
C
      CALL AGM
C     --------
C
C.....Compute parameters of the wave train
C
      M   = ONE - MP
      AM  = (ONE+MP-THREE*EE/KK)/M
      BM  = (MP-EE/KK)/M
C
      WCA = ONE + WH*AM
C
      IF(WCA.LT.ZERO) THEN
         CALL ERRORS(TEXTE1)
C        -----------
      ELSE
         WC = DSQRT(WCA)
      END IF
      WL = WC*WT
      WU = WH*WL*WL
C
C.....Compute initial phase angle for zero elevation and velocity
C
      THE0 = TWO*KK - ACN(DSQRT(-BM))
C
C     Check initial zero stroke and calculate phase shift lambda
C
      XP0 = (WL*WH/(TWO*KK))*(BM*THE0+(EINC(THE0)-MP*THE0)/M)
      T0 = (THE0*WL/(TWO*KK) + XP0)/WC
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE NEWTON(CONST,MP0)
C----------------------------------------------------------------------
C0  NEWTONF   NEWTON
C1  PURPOSE   This subroutine computes the zeros of GHT function using
C1            discrete Newton's method. Part of the m-derivatives of GHT
C1            are only known by finite difference, namely : dK/dm, dE/dm
C2  CALL      CALL NEWTON(CONST,MP0)
C3  CALL ARG. MP0=1-mo: estimate of comp. par. of elliptic integrals
C3            CONST= (3*H*T**2)/16 constant
C3            WH,WT=Dimensionless wave height and period
C3            EPSI=Accuracy in Newton's iterations
C3            IMAX=Max number of iterations
C4  RET. ARG. Root MP and tables A,B,C
C4            KK,EE=First and 2nd kind complete ellip. integrals K(m),E(
CE  ERRORS    01=THE NUMBER OF ITERATIONS IS TOO LARGE
C6  INT. CALL ERRORS,FCTS
C9  Oct. 90   J. Kirby, S. Grilli, Civil Engng. Univ. of Delaware (Copyr
CLNEWTON SUB. COMPUTING COMPLEM. PAR. MP OF CNOIDAL WAVE (H,T)
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NMAX=100)

      CHARACTER*8 TEXTE
      REAL*8 MP,MPSAVE,MP0,MPM1,KK
C
      COMMON /GATAB/ DUM1(3*NMAX),KK,EE,MP,IDUM1(2)
      COMMON /CNOWA/ WH,WT,DUM2(8)
C
      DATA EPS1/1.D-08/, ONE/1.D0/, IMAX/200/,
     .     EPS2/1.D-06/,TEXTE/'NEWTON01'/,P9/0.9D0/,HALF/0.5D0/,
     .     EPS3/1.D-20/,EPS4/5.D-06/,ZERO/0.D0/,DP10/1.D+10/
C
       SAVE
C     SAVE /GATAB/,/CNOWA/
C
C.....Initialization
C
      MP     = MP0
      MPSAVE = MP0
      EPSAVE = DP10
      EPSI   = EPS1
      I      = 0
C
   10    CALL FCTS(CONST,GHT,DGHT)
C        ---------
C
         MPM1 = MP
C
         IF(DABS(DGHT).GT.EPS3) THEN
            DMP = GHT/DGHT
         ELSE
            DMP= ONE
            MP = MP*P9
         END IF
         ACCU = DABS(DMP/MPM1)
C
C........Keep best result, in case of divergence
C
         IF(ACCU.LT.EPSAVE) THEN
            EPSAVE = ACCU
            MPSAVE = MP
         END IF
C
C........Convergence test
C
         IF(ACCU.LT.EPSI.OR.I.GT.IMAX) GOTO 20
C
            MP = MPM1-DMP
C
            I = I+1
C
C.......... Try prevent oscillations
C
            IF(I.GT.15.OR.MPM1.LT.EPS4) THEN
               MP = HALF*(MP+MPM1)
               EPSI = EPS2
            END IF
C
            J = 0
            DMPP = DMP
   15       IF(MP.LE.EPS3) THEN
               DMPP = DMPP*HALF
               MP = MPM1-DMPP
               J = J+1
               IF(J.LT.10) GOTO 15
            END IF
            IF(MP.LT.ZERO) THEN
               MP = EPS3
            END IF
            IF((ONE-MP).LT.EPSI) THEN
               MP= MPM1+(ONE-MPM1)*P9
            END IF
C
            GOTO 10
C
   20 MP = MPSAVE
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE FCTS(CONST,GHT,DGHT)
C----------------------------------------------------------------------
C0  FCTSF     FCTS
C1  PURPOSE   This subroutine computes functions GHT(m)=0 and dGHT/dm,
C1            at m, to find the parameter m of cnoidal wave (H,T)
C2  CALL      FCTS(CONST,GHT,DGHT)
C3  CALL ARG. MP=1-m: complementary parameter of elliptic integrals
C3            IFLAG=(0/1) tables of geom.=arith. mean do or don't exist
C3            WH,WT:Dimensionless wave height and period of cnoidal wave
C3            CONST=3 *(WH*WT**2)/16 constant
C4  RET. ARG. GHT, DGHT
C6  INT. CALL AGM,DKE
C9  Oct. 90   J. Kirby, S. Grilli, Civil Engng. Univ. of Delaware (Copyr
CLFCTS SUB. COMPUTING ZERO FCT OF (H,T) FOR CNOIDAL WAVE
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NMAX=100)

      REAL*8 MP,KK,M,KK2
C
      COMMON /GATAB/ DUM1(3*NMAX),KK,EE,MP,IDUM(2)
      COMMON /CNOWA/ WH,WT,DUM2(8)
C
      DATA ONE/1.D0/, THREE/3.D0/, TWO/2.D0/
C
       SAVE
C     SAVE /GATAB/,/CNOWA/
C
C.....Calculate centered derivatives for K(m),E(m)
C
      CALL DKE(DKKM,DEEM)
C     --------
C
C.....Initialize geom.-arit. tables and K(m), E(m)
C
      CALL AGM
C     --------
C
      KK2 = KK*KK
      M   = ONE-MP
      AM  = (ONE+MP-THREE*EE/KK)/M
      DMA = (ONE-THREE*(KK*DEEM-DKKM*EE)/KK2 + AM)/M
C
      GHT = M - CONST*(ONE+WH*DABS(AM))/KK2
      DGHT= -ONE-CONST*(WH*DMA-(TWO*DKKM/KK)*(ONE+WH*AM))/KK2
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE TRAJEC(T,XP,XPT,XPTT)
C----------------------------------------------------------------------
C0  TRAJECF   TRAJEC
C1  PURPOSE   This subroutine computes the motion, velocity and acceler.
C1            of a piston wavemaker for generating a cnoidal wave
C1            (H,T,L,C). The initial stroke is 0 for chi=chi0 and the
C1            stroke varies between 0 and 2*xpmax. Velocity and wave
C1            elevation are 0 at the initial time 0 and stroke.
C2  CALL      CALL TRAJEC(T,XP,XPT,XPTT)
C3  CALL ARG. M,MP=1-m: compl. param. and compl. par. of ellip. integral
C3            IFLAG=(0/1) tables of geom.=arith. mean do or don't exist
C3            WH,WT,WL,WC=Wave characteristics
C3            BM=trough wave characteristics
C3            T=Time of paddle motion
C4  RET. ARG. XP,XPT,XPTT=Paddle motion, vel. and acc. at time T
C6  INT. CALL ERRORS
CE  ERR. 01 = THE NUMBER OF ITERATIONS IS TOO LARGE
C9  Oct. 90   J. Kirby, S. Grilli, Civil Engng. Univ. of Delaware (Copyr
CLTRAJEC SUB. COMPUTING PADDLE TRAJECTORY, VEL., ACCEL. FOR CNOIDAL WVES
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NMAX=100)

      CHARACTER*8 TEXTE
      REAL*8 MP,M,KK,KK2
C
      COMMON /GATAB/ DUM(3*NMAX),KK,EE,MP,IDUM(2)
      COMMON /CNOWA/ WH,WT,WC,WL,DUM2,M,DUM3,BM,DUM4(2)
C
      EXTERNAL EINC,CN
C
      DATA EPS1/1.D-07/,ONE/1.D0/,IMAX/50/,TWO/2.D0/,TEXTE/'TRAJEC01'/,
     .     EPS2/1.D-05/,P99/0.99D0/,FOUR/4.D0/,HALF/0.5D0/,ZERO/0.D0/,
     .     DP10/1.D+10/
C
       SAVE
C     SAVE /GATAB/,/CNOWA/
C
C.....Initialization
C
      KK2 = TWO*KK
      TT = T
      IF(T.GE.WT) THEN
         IS = INT(T/WT)
         TT =  T - IS*WT
      END IF
      CST = KK2*(WC*TT)/WL
C
      THET = CST-KK2*XP/WL
C
      I = 0
      EPSI = EPS1
      EPSAVE = DP10
      THETS = THET
C
C.....Newton iterations
C
   10    FC = THET - CST + (WH/M)*(EINC(THET)-EE*THET/KK)
         DFC= ONE + WH*(BM+CN(THET)**2)
C
         IF(DABS(DFC).GT.EPSI) THEN
            DTHET = FC/DFC
            THETP = THET
         ELSE
            DTHET = ONE
            THETP = THET
            THET  = DTHET*P99
         END IF
C
         I = I+1
         ACCU = DABS(DTHET/THET)
         IF(ACCU.LT.EPSAVE) THEN
            EPSAVE = ACCU
            THETS = THET
         END IF
C
         IF(ACCU.LT.EPSI.OR.I.GT.IMAX) GOTO 20
C
            THET  = THET - DTHET
C
C           Help preventing oscillations
C
            IF(THET.LT.ZERO) THEN
               THET = EPS1
            END IF
C
            IF(I.GT.10) THEN
               THET = HALF*(THET+THETP)
               EPSI = EPS2
            END IF
C
            GOTO 10
C
   20 THET = THETS
C
C.....Dimensionless paddle motion
C
      XP = (CST-THET)*WL/(TWO*KK)
C
C.....Dimensionless velocity and acceleration
C
      CNCHI= CN(THET)
      SNCHI= DSQRT(ONE-CNCHI*CNCHI)
      DNCHI= DSQRT(ONE-M*SNCHI*SNCHI)
C
      ETA = WH*(BM+CNCHI*CNCHI)
C
      XPT = WC*ETA/(ONE+ETA)
      XPTT=-(FOUR*WH*WC*WC/WL)*KK*(CNCHI*SNCHI*DNCHI/(ONE+ETA)**3)
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE AGM
C----------------------------------------------------------------------
C0  AGMF      AGM
C1  PURPOSE   This subroutine computes the arithmetic-geometric mean
C1            table used to compute elliptic integrals and elliptic
C1            functions. First and 2nd. kind complete integrals are then
C1            calculated for the given complementary parameter 1-m.
C1            Computations are based on Abramowitz & Stegun p598, eq. 17
C2  CALL      CALL AGM
C3  CALL ARG. MP=1-m: complementary parameter of elliptic integrals
C3            IFLAG=(0/1) tables of geom.=arith. mean do or don't exist
C3            EPSI=Accuracy in the geom.-arit. mean calculation
C4  RET. ARG. A(NMAX),B,C :Tables of geom.-arith. mean
C4            KK,EE=First and 2nd kind complete ellip. integrals K(m),E(
CE  ERRORS    01=THE NUMBER OF ITERATIONS IS TOO LARGE
C6  INT. CALL ERRORS
C9  Oct. 90   J. Kirby, S. Grilli, Civil Engng. Univ. of Delaware (Copyr
CLAGM SUB. COMPUTING GEOMETRIC ARITHMETIC MEAN TABLES FOR ELLIPTIC FCTS
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      PARAMETER (NMAX=100)

      CHARACTER*8 TEXTE
      REAL*8 MP,KK
C
      COMMON /GATAB/ A(NMAX),B(NMAX),C(NMAX),KK,EE,MP,N,IFLAG
C
      DATA EPSI/1.D-010/, ONE/1.D0/, PI/3.1415926535898D0/, HALF/0.5D0/,
     .     ZERO/0.D0/, TWO/2.D0/, TEXTE/'AGM01   '/
C
       SAVE
C     SAVE /GATAB/
C
C.....Initialization
C
      A(1) = ONE
      B(1) = DSQRT(MP)
      C(1) = DSQRT(ONE-MP)
      N=1
C
C.....Computation of tables by recurrence
C
10    IF(C(N).GT.EPSI) THEN
         N = N+1
         IF(N.GE.NMAX) CALL ERRORS(TEXTE)
C                      -----------
         AA = A(N-1)
         BB = B(N-1)
         A(N) = HALF*(AA+BB)
         B(N) = DSQRT(AA*BB)
         C(N) = HALF*(AA-BB)
C
         GOTO 10
      END IF
C
C.....Tables are complete. Calculate K(m) and E(m) by summation formulae
C
      KK = HALF*PI/A(N)
C
      SUM=ZERO
C
      DO I=1,N
        SUM=SUM+(TWO**(I-1))*C(I)**2
      END DO
C
      EE=KK*(ONE-HALF*SUM)
C
C.....Flag of table existance
C
      IFLAG=1
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE DKE(DKKM,DEEM)
C----------------------------------------------------------------------
C0  DKEF      DKE
C1  PURPOSE   This subroutine computes the m-derivatives of K(m),E(m)
C2  CALL      CALL DKE(DKKM,DEEM)
C3  CALL ARG. MP=1-m: complementary parameter of elliptic integrals
C3            IFLAG=(0/1) tables of geom.-arith. mean do or don't exist
C4  RET. ARG. DKKM,DEEM=m-derivatives of first and 2nd kind compl. ell.
C6  INT. CALL AGM
C9  Oct. 90   J. Kirby, S. Grilli, Civil Engng. Univ. of Delaware (Copyr
CLDKE SUB. COMPUTING M-DERIVATIVES OF K AND E
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NMAX=100)

      REAL*8 MP,KK,KK1,KK2,MPI,MP1,MP2
C
      COMMON /GATAB/ DUM(3*NMAX),KK,EE,MP,IDUM(2)
C
      DATA ONE/1.D0/, DP6/1.D-06/
C
       SAVE
C     SAVE /GATAB/
C
C.....Initialization
C
      IF(MP.GT.DP6) THEN
         MP1 = MP-DP6
         IFC1 = 1
      ELSE
         MP1 = MP
         IFC1 = 0
      END IF
C
      IF((ONE-MP).GT.DP6) THEN
         MP2 = MP+DP6
         IFC2 = 1
      ELSE
         MP2 = MP
         IFC2 = 0
      END IF
C
      DTM = (IFC1+IFC2)*DP6
C
C.....Calculation of difference functions
C
      MPI= MP
C
      MP = MP1
      CALL AGM
C     --------
      KK1 = KK
      EE1 = EE
C
      MP = MP2
      CALL AGM
C     --------
      KK2 = KK
      EE2 = EE
C
      DKKM = (KK2-KK1)/DTM
      DEEM = (EE2-EE1)/DTM
C
      MP=MPI
C
      RETURN
C
      END
C----------------------------------------------------------------------
      FUNCTION CN(U)
C----------------------------------------------------------------------
C0  CNF      CN
C1  PURPOSE   This subroutine computes the jacobi elliptic cosine of
C1            the argument u, cn(u,m), with parameter m. If m'=1-m is le
C1            than 5.D-06, an hyperbolic approximation is used to improv
C1            the numerical stability.
C1            Computations are based on Abramowitz & Stegun p571, eq. 16
C1            and p574 eq. 16.15.2.
C1            Notice : tables of geom.-arith. fcts are assumed to exist.
C1            Hence, AGM routine must have been called. This is checked
C1            with IFLAG=1.
C1
C2  CALL      CALL CN(U)
C3  CALL ARG. MP=1-m: complementary parameter of elliptic integrals
C3            IFLAG=(0/1) tables of geom.=arith. mean do or don't exist
C3            EPSHYP=Limit on 1-m for hyperbolic approximation
C4  RET. ARG. CN
C6  INT. CALL AGM
C9  Oct. 90   J. Kirby, S. Grilli, Civil Engng. Univ. of Delaware (Copyr
CLCN SUB. COMPUTING JACOBI ELLIPTIC COSINE FCT.
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NMAX=100)

      REAL*8 MP,KK,KK2,KK4
C
      COMMON /GATAB/ A(NMAX),B(NMAX),C(NMAX),KK,DUM,MP,N,IFLAG
C
      DATA EPSHYP/5.D-06/, ONE/1.D0/, EPSI/1.D-10/, HALF/0.5D0/,
     .     ZERO/0.D0/,FOURTH/0.25D0/,TWO/2.D0/
C
       SAVE
C     SAVE /GATAB/
C
C.....Creates geom.-arit. mean tables when they don't exist
C
      IF(IFLAG.EQ.0) THEN
         CALL AGM
C        --------
      END IF
C
      KK2 = TWO*KK
      KK4 = TWO*KK2
C
      SIGN=ONE
      IF(U.LT.ZERO) SIGN=-ONE
C
C.....Initialization of phi (series 17.2.3)
C
      W = U
      SIGN = ONE
      IF(W.LT.ZERO) THEN
         SIGN = -ONE
      END IF
      W = W*SIGN
C
C     Periodicity conditions
C     Remove any multiple of 4K. Symmetry K, 2K
C
      JS = 1
C
      IF(W.GE.KK4) THEN
         IS = INT(W/KK4)
         W =  W - IS*KK4
      END IF
      IF(W.GT.KK2) THEN
         W = KK4-W
      END IF
      IF(W.GT.KK) THEN
         W = KK2-W
         JS = -1
      END IF
C
C.....W is now between 0 and K
C
      IF(MP.LT.EPSHYP) THEN
C
C........Hyperbolic approximation of cn
C
         TANHU = DTANH(W)
         SECHU = DSQRT(ONE-TANHU*TANHU)
C
         IF(SECHU.GT.EPSI) THEN
            COSHU = ONE/SECHU
         ELSE
            COSHU = ZERO
         END IF
C
         SINHU = TANHU*COSHU
C
         CN = SECHU*(ONE-FOURTH*MP*(SINHU*COSHU-W)*TANHU)
C
      ELSE
C
C........Summation formula based on geom.-arit. mean
C
         PHN = (TWO**(N-1))*A(N)*W
C
         DO I=1,N-1
            J = N-I+1
            Y = C(J)/A(J)*DSIN(PHN)
            ASINY = DASIN(Y)
            PHN = HALF*(PHN+ASINY)
         END DO
C
         CN = DCOS(PHN)
C
      END IF
C
C.....Rescaling cn(u,m)
C
      CN = JS*CN*SIGN
C
      IF(CN.GT.ONE) CN=  ONE
      IF(CN.LT.-ONE)CN= -ONE
C
      RETURN
C
      END
C----------------------------------------------------------------------
      FUNCTION EINC(U)
C----------------------------------------------------------------------
C0  EINCF     EINC
C1  PURPOSE   This subroutine computes the 2nd kind complete elliptic
C1            integral of the argument u, E(u,m), with parameter m.
C1            Computations are based on Abramowitz & Stegun p599, eq. 17
C1            with Landen 17.5.2.
C1            Notice : tables of geom.-arith. fcts are assumed to exist.
C1            Hence, AGM routine must have been called. This is checked
C1            with IFLAG=1.
C1
C2  CALL      CALL EINC(U)
C3  CALL ARG. MP=1-m: complementary parameter of elliptic integrals
C3            IFLAG=(0/1) tables of geom.=arith. mean do or don't exist
C4  RET. ARG. EINC
C6  INT. CALL AGM
C9  Oct. 90   J. Kirby, S. Grilli, Civil Engng. Univ. of Delaware (Copyr
CLEINC SUB. COMPUTING INCOMPLETE ELLIPTIC INTEGRAL OF 2ND KIND
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NMAX=100)

      REAL*8 MP,KK,KK2
C
      EXTERNAL CN
C
      COMMON /GATAB/ A(NMAX),B(NMAX),C(NMAX),KK,EE,MP,N,IFLAG
C
      DIMENSION PH(NMAX)
C
      DATA ONE/1.D0/, ZERO/0.D0/, TWO/2.D0/,EPSI/1.D-06/,
     .     PI/3.1415926535898D0/
C
       SAVE
C     SAVE /GATAB/
C
C.....Creates geom.-arit. mean tables when they don't exist
C
      IF(IFLAG.EQ.0) THEN
         CALL AGM
C        --------
      END IF
C
      KK2 = TWO*KK
      EE2 = TWO*EE
C
C.....Initialization of phi (series 17.2.3)
C
      W = U
      SIGN = ONE
      IF(W.LT.ZERO) THEN
         SIGN = -ONE
      END IF
      W = W*SIGN
C
C     Periodicity conditions
C     Remove any multiple of 2K. Symmetry K, 2K
C
      IS = 0
      JS = 1
C
      IF(W.GE.KK2) THEN
         IS = INT(W/KK2)
         W =  W - IS*KK2
      END IF
      IF(W.GT.KK) THEN
         W = KK2-W
         JS = -1
         IS = IS+1
      END IF
      IF(DABS(W-KK2).LT.EPSI) THEN
         W = ZERO
      END IF
C
C     Now W is between 0 and K. Hence, Phi is between 0 and PI/2
C
      PH(1) = DACOS(CN(W))
C
C.....Series computation
C
      DO I=1,N-1
         Y = (B(I)/A(I))*DSIN(PH(I))/DCOS(PH(I))
         PH(I+1) = PH(I)+DATAN(Y)
C
C........The function PH(I)/(2**(I-1)*A(I)) must be monotoneously
C        increasing. So IF  WIP1 < WI  , PI is added to PH(I+1) (see
C        figure 17.4 p593).
C
         WI  = PH(I)/(TWO**(I-1) * A(I))
         WIP = PH(I+1)/(TWO**I * A(I+1))
C
   10    IF((WIP-WI).LE.ZERO) THEN
            PH(I+1) = PH(I+1) + PI
            WIP = PH(I+1)/(TWO**I * A(I+1))
            GOTO 10
         END IF
C
      END DO
C
C.....FIEI is the first incomplete elliptic integral
C
      FIEI= PH(N)/(TWO**(N-1) * A(N))
C
      IF(MP.GT.EPSI) THEN
         SUM = ZERO
C
         DO I=2,N
           SUM = SUM+C(I)*DSIN(PH(I))
         END DO
C
         EINC = EE/KK*FIEI+SUM
C
      ELSE
C
C........Sine limit of E(u,m) when m=1 (17.4.25)
C
         EINC = DSIN(PH(1))
      END IF
C
C.....Linear approximation near origin
C
      IF(DABS(PH(1)).LT.EPSI) THEN
         EINC = PH(1)
      END IF
C
C.....Final rescaling of E(u,m)
C
      EINC = SIGN*(JS*EINC + IS*EE2)
C
      RETURN
C
      END
C----------------------------------------------------------------------
      FUNCTION ACN(U)
C----------------------------------------------------------------------
C0  ACNF      ACN
C1  PURPOSE   This subroutine computes the inverse jacobi elliptic cosin
C1            the argument u, acn(u,m), with parameter m.
C1            Computations are based on Abramowitz & Stegun
C1            Notice : tables of geom.-arith. fcts are assumed to exit.
C1            Hence, AGM routine must have been called. This is checked
C1            with IFLAG=1.
C1
C2  CALL      CALL ACN(U)
C3  CALL ARG. MP=1-m: complementary parameter of elliptic integrals
C3            IFLAG=(0/1) tables of geom.=arith. mean do or don't exist
C4  RET. ARG. ACN
C6  INT. CALL AGM
C9  Oct. 90   J. Kirby, S. Grilli, Civil Engng. Univ. of Delaware (Copyr
CLACN SUB. COMPUTING INVERSE JACOBI ELLIPTIC COSINE FCT.
C----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (NMAX=100)

      REAL*8 MP
C
      COMMON /GATAB/ A(NMAX),B(NMAX),C(NMAX),DUM1(2),MP,N,IFLAG
C
      DATA ZERO/0.D0/, EPSI/1.D-10/,TWO/2.D0/,PI/3.1415926535898D0/,
     .     ONE/1.D0/
C
       SAVE
C     SAVE /GATAB/
C
C.....Creates geom.-arit. mean tables when they don't exist
C
      IF(IFLAG.EQ.0) THEN
         CALL AGM
C        --------
      END IF
C
C.....Initialization
C
      PHI = DACOS(U)
C
      IF(DABS(PHI).GT.EPSI.AND.MP.GT.EPSI) THEN
C
         DO I=1,N-1
            Y = (B(I)/A(I))*DSIN(PHI)/DCOS(PHI)
            PHIP = PHI+DATAN(Y)
C
C...........The function PHI/(2.**(I-1)*A(I)) must be monotoneously
C           increasing. So IF  WIP1 < WI  , PI is added to PH(I+1) (see
C           figure 17.4 p593).
C
            WI  = PHI /(TWO**(I-1) * A(I))
            WIP = PHIP/(TWO**I * A(I+1))
C
   10       IF((WIP-WI).LE.ZERO) THEN
               PHIP = PHIP + PI
               WIP = PHIP/(TWO**I * A(I+1))
               GOTO 10
            END IF
            PHI = PHIP
         END DO
C
         ACN = PHI/(TWO**(N-1) * A(N))
      ELSE
         IF(MP.LE.EPSI) THEN
C
C...........Hyperbolic approximation => sech**-1 (4.6.24)
C
            ONU = ONE/DABS(U)
            ACN = DLOG(ONU+DSQRT(ONU*ONU-ONE))
         END IF
         IF(DABS(PHI).LT.EPSI) THEN
C
C...........Solution close to origin
C
            ACN = DABS(PHI)
         END IF
      END IF
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE STRFCT(K,QBC)
C----------------------------------------------------------------------
      IMPLICIT REAL*8(A-H,O-Z)


      INCLUDE 'param.inc'
      PARAMETER (NRMAX=30,MAXT=50,MAXN=61,ITMAX=20)

      EXTERNAL FSTRMT
C
      COMMON/DATGEN/  DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM3(66),INODF(6,2),IBCOND(6)
c      COMMON/SYSTEM/  DUM3(24),TIME,TDAMP,DUM31(1+8*MYMAX),UBK(4),IO6,
c     .                IDU,IFLAGV,IDV,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOUNDC/ DT,DUM4,DT22,DUM5(2),RHO,CPRESS,CPREST,GE,
     .                DE(2),OMEGA,H,DUM6,IDW
      COMMON /STRCOE/ WL,DUN1((2*MAXN+1)*MAXT-1),IDD(2),DUN2,WT,DPT,
     .                UBB,THETAM(MAXN),ETAM(MAXN),DUN4,WH,DUN5
C
c	Old line
c      DIMENSION QBC(NOM),UP(NRMAX),DUPDT(NRMAX),VP(NRMAX),DUPDZ(NRMAX)
c	New line, XB
      DIMENSION QBC(NOM),UP(NRMAX),DDUPDT(NRMAX),VP(NRMAX),DUPDZ(NRMAX)
C
      DATA EPS/1.0D-10/,ONE/1.D0/,PI/3.1415926535898D0/,TWO/2.D0/,
     .     ZERO/0.D0/
C
       SAVE
C     SAVE /DATGEN/,/MAILLE/,/BOUNDC/,/SYSTEM/,/STRCOE/
C
      NDY = MZ + 1
      DEC = DE(K/2)
      IST = K - 3
      IB  = INODF(K,1)
C
C.....Calculates stream function coefficients x(n) and characteristics
C
      IF(ILOOP.EQ.1.AND.IFLAGV.EQ.2) THEN
C
C        Wave characteristics (T,H,h,g,U)
C
         WT   = TWO*PI/OMEGA
         WH   = H
         DPT  = DEC
         G    = GE
c	Old line
c	 UDE  = UBK(K-1)
c	New line, XB
	 UDE  = UB(K-1)
         UBB   = UDE
         US   = UBB
C
C        First free surf. node initialization
C
         XP   = XYZNOD(IB,1)
C
         WRITE(IO6,2000)
C
C        Zero mass transport stream function generation
C
         IF (DABS(UDE).LT.EPS) THEN
C
            IT    = 0
            TMASS = ONE
            USP   = ONE
            DUS   = ONE
C
C           Lagrangian wave period
C
            WTT   = WT
C
            DO WHILE (DABS(DUS).GT.EPS.AND.IT.LE.ITMAX)
               UBB    = US
               IT    = IT + 1
c               TMASS = FSTRMT(US,G,WTT,IO6)
               TMASS = FSTRMT(US,G,WTT)
C                      ------
C
C              Calculate depth and period averaged current correction
C
               US  = US - TMASS/(DPT*WTT)
               DUS = (US - USP)/USP
               USP = US
C
               WRITE(IO6,2010) IT,TMASS*RHO,US,WTT
C
            END DO
C
            UBB = US
         END IF
C
C        Regular stream function generation and last correction for
C        zero mass transport
C
         CALL STRECO(IO6,US,G,THETA0)
C        -----------
         WC = WL/WT
         WK = TWO*PI/WL
      END IF
C
C.....Local phase angle with correction THETA0 for initial shift
C
      THETAT = (XP - WC*TIME)*WK + THETA0
C
      IF(IFLAGV.EQ.1) THEN
C
C     ...Geometry updating on the free surface intersection with K                                                                           C
C        Correct first node free surface values
C
         DO I=1,MY+1
	    NB  = IB + I - 1
	    NU  = IB + MZ*(MY+1) + I - 1
	    ETA = XYZNOD(LXYZ(NU,1),3)
	    XYZNOD(LXYZ(NU,1),1) = XP
C
C           Calculate velocity and acceleration for t=TIME
C
            CALL STRETA(NDY,ETA,UP,DDUPDT,THETAT,VP,DUPDZ)
C           -----------
	    ZK  = (ETA - XYZNOD(NB,3))/(XYZNOD(NU,3) - XYZNOD(NB,3))
	    DO J=MZ+1,1,-1
	       IND = IB + (J-1)*(MY+1) + I - 1
	       XYZNOD(IND,1) = XP
	       DO K=2,3
                  XYZNOD(IND,K) = XYZNOD(NB,K) + (J-1)*
     .                           (XYZNOD(NU,K) - XYZNOD(NB,K))*ZK/MZ
               END DO
               QBC(IND) = UP(MZ-J+2)*IST
	    END DO
         END DO
      ELSE
C
C     ...Geometry and phin updating
C
         UPM    = ZERO
         DUDTOM = ZERO
C
         DO I=1,MY+1
	    NB  = IB + I - 1
	    NU  = IB + MZ*(MY+1) + I - 1
	    ETA = XYZNOD(LXYZ(NU,1),3)
C
C           Calculate velocity and acceleration for t=TIME
C
            CALL STRETA(NDY,ETA,UP,DDUPDT,THETAT,VP,DUPDZ)
C           -----------
            DUPDX  = -DDUPDT(1)/WC
            DUDTOT =  DDUPDT(1) + UP(1)*DUPDX + VP(1)*DUPDZ(1)
	    UPM    =  UPM + UP(1)
	    DUDTOM =  DUDTOM + DUDTOT
C
            DO J=MZ+1,1,-1
	       IND = IB + (J-1)*(MY+1) + I - 1
               QBC(IND) = DDUPDT(MZ-J+2)*IST
	    END DO
         END DO
C
C     ...Get paddle XP displacement by Taylor expansion with free surface
C        velocity and acceleration for next FS updating
C
         XP = XP + (UPM*DT + DUDTOM*DT22)/DFLOAT(MY+1)
      END IF
C
      RETURN
C
 2000 FORMAT(///'  Stream function analysis'/
     .          '  ========================'///)
 2010 FORMAT(1H ,'Iter. =',I5,' Mass transp. = ',E15.8,
     .           ' Avg. curr. = ',E18.12,' Lagr. T = ',F10.6)
C
      END
C
C---------------------------------------------------------------
C      REAL*8 FUNCTION FSTRMT(US,G,WTT,IO6)
      REAL*8 FUNCTION FSTRMT(US,G,WTT)
C---------------------------------------------------------------
C
C     Given a value of UU (current velocity) FSTRMT returns
C     the value of mass transport integrated over the depth
C     (lateral boundary nodes) and over 31 pts in the time dim.
C
C---------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      include 'param.inc'
C
      PARAMETER (NSIMP=31,NRMAX=30,MAXN=61,MAXT=50)

      COMMON /STRCOE/ WL,DUN1((2*MAXN+1)*MAXT-1),IDD(2),DUN2,WT,DPT,
     .                UBB,THETAM(MAXN),ETAM(MAXN),DUN4,WH,DUN5
c      COMMON /SYSTEM/ DUM1(24),TIME,DUM2(6+8*MYMAX),IDU(5)
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
C
      DIMENSION PM(NSIMP),UP(NRMAX),DDUPDT(NRMAX),DUPDZ(NRMAX),
     .          THETAP(NSIMP),ETAP(NSIMP),ETAUP(NSIMP),VP(NRMAX)
C
      DATA TWO/2.0D0/,PI/3.1415926535898D0/,ZERO/0.D0/,EIGHT/8.D0/
C
       SAVE
C     SAVE /STRCOE/,/SYSTEM/
C
      XPP = ZERO
C
C.....Streamfunction coefficients for wave with US=UB
C
      CALL STRECO(IO6,US,G,THETA0)
C     -----------
C
C.....Dummy value of TL to ensure TIME values greater than TDAMP
C
      TL    = EIGHT*WT
      TINC  = WTT/DFLOAT(NSIMP-1)
      TBACK = TIME
      WC    = WL/WT
      ZKL   = TWO*PI/WL
C
C.....Simpson's integration over time for time average over WT to calculate
C     total mass transport (/RHO) over WT
C
C     Complete waveform with L/2 coefficients
C
      DO IN=1,NSIMP
         THETAP(NSIMP-IN+1) = -THETAM(IN)
         ETAP(NSIMP-IN+1)   =  ETAM(IN)
      END DO
C
C     Get time series of PM(IT)= \int_{-h}^{\eta} u dz
C
      DO IT = 1,NSIMP
         TIME = TL + DFLOAT(IT-1)* TINC
         XP   = XPP
C
C     ...Phase angle with REAL*8 time to get ETA
C
         THETAT = THETA0 + ZKL*(XP-WC*(TIME-TL))
C
         IF (THETAT.LT.-PI) THEN
            THETAT = -THETAT - TWO*PI
         ELSE IF (THETAT.GT.ZERO) THEN
            THETAT = -THETAT
         END IF
C
C        Polynomial nterpolation to get ETA(THETAT)
C
         CALL POLINT(THETAP,ETAP,NSIMP,THETAT,ETA,EDUM)
C        -----------
c
C     ...Calculates elevation, velocity UP and acceleration DUPDT for
C         t=TIME, at (NSIMP-2) depth from bottom to surface
C
         THETAT = THETA0 + ZKL*(XP-WC*TIME)
C
         CALL STRETA(NSIMP-2,ETA,UP,DDUPDT,THETAT,VP,DUPDZ)
C        -----------
C
C     ...Performs simpsons integrals, over Z for \int u dz with
C        (NSIMP-2) segments, at time IME
C
         ZINC = (ETA + DPT)/DFLOAT(NSIMP-3)
C
         CALL INTSIM(UP,ZINC,NSIMP-2,PM(IT))
C        -----------
C
C     ...Get paddle XP displacement by Taylor expansion with free surface
C        velocity and acceleration
C
         DUPDX  = -DDUPDT(1)/WC
         DUDTOT =  DDUPDT(1) + UP(1)*DUPDX + VP(1)*DUPDZ(1)
         XPP    =  XP + UP(1)*TINC + DUDTOT*TINC*TINC/TWO
C
C     ...Second integral contribution to mass transport
C
         ETAUP(IT) = (DPT + ETA)*UP(1)
C
      END DO
C
C     Time integration by Simpson's method for NSIMP intervals
C     Get TMASS = MASS TRANSPORT/RHO
C
      CALL INTSIM(PM,TINC,NSIMP,VQM)
C     -----------
      CALL INTSIM(ETAUP,TINC,NSIMP,VUM)
C     -----------
C
      TIME   = TBACK
      WTT    = WT*(1 + XP/WL)
C
      FSTRMT = DPT*XP - VUM + VQM
C     ------
C
      RETURN
C
      END
C
C-----------------------------------------------------------------------
      SUBROUTINE STRETA(NDY,ETAS,UP,DDUPDT,THETAT,VP,DUPDZ)
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      include 'param.inc'

      PARAMETER (NT=5,NRMAX=30,MAXT=50,MAXN=61)
C
      DIMENSION UP(NRMAX),DDUPDT(NRMAX),U(NRMAX),DUDT(NRMAX),VP(NRMAX),
     .          DUPDZ(NRMAX),V(NRMAX),DUDZ(NRMAX)
C
      COMMON /STRCOE/ WL,DUN1((2*MAXN+1)*MAXT-1),IDU(2),DUN2,WT,
     .                DUN3(2*MAXN+3),WH,DUN4
c      COMMON /SYSTEM/ DUM1(24),TIME,TDAMP,DUM2(5+8*MYMAX),IDV(5)
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
C
      DATA HALF/0.5D0/,ONE/1.D0/,ZERO/0.D0/,FOUR/4.D0/,EPS/1.D-08/,
     .     CD/2.3025D0/
C
       SAVE
C     SAVE /STRCOE/,/SYSTEM/
C
C.....Current damping
C
      T0   = HALF*TDAMP
      D    = ONE
      DT   = ZERO
C
      IF(TIME.LT.FOUR*T0.AND.T0.GT.EPS) THEN
         ZMU = CD/T0
         TA  = ZMU*(TIME-T0)
         TB  = ZMU*T0
         THA = DTANH(TA)
         THB = DTANH(TB)
         DDEN= ONE+THB
         CH2 = DCOSH(TA)**2 *DDEN
C
         D   = (THB+THA)/DDEN
         DT  = ZMU/CH2
      END IF
C
C.....Current motion along vertical line
C                                                                                                                             C
      CALL STREVE(NDY,THETAT,ETAS,U,DUDT,V,DUDZ)
C     -----------
C
C.....Include damping function
C
      DO I=1,NDY
         UP(I)    = U(I)*D
         VP(I)    = V(I)*D
         DDUPDT(I) = DUDT(I)*D + U(I)*DT
         DUPDZ(I) = DUDZ(I)*D
      END DO
C
      RETURN
C
      END
C-------------------------------------------------------------
      SUBROUTINE STREVE(NDY,THETAT,ETAS,U,DUDT,V,DUDZ)
C-------------------------------------------------------------
C
C  Calculate internal quantities for the streamfunction generation
C                                                                                                                                            C-------------------------------------------------------------
C-------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER(MAXT=50,MAXN=61,NRMAX=30)

      COMMON /STRCOE/ X(MAXT),DUN1(2*MAXN*MAXT),NN,IDU,DUN2,WT,DPT,
     .                UO,DUN3(2*MAXN+2),OMEGA
      DIMENSION U(NRMAX),DUDT(NRMAX),V(NRMAX),DUDZ(NRMAX)
C
      DATA PI2/6.2831853071795D0/,ZERO/0.0/
C
       SAVE
C     SAVE /STRCOE/
C
      WL   = X(1)
      CC   = WL/WT
      CON  = PI2/WL
      TOTH = DPT + ETAS
      DY   = TOTH/(NDY-1)
      ELEV = TOTH + DY
C
C.....Loop over internal NDY nodes at phase THETAT
C
      DO IK=1,NDY
         U(IK)    = UO   + OMEGA*ELEV
         V(IK)    = ZERO
         DUDZ(IK) = ZERO
         DUDT(IK) = ZERO
         ELEV     = ELEV - DY
C
C     ...Sum over order
C
         DO N=2,NN
            XN    = N-1
            ZN    = CON*XN
            COEF1 = DCOS(THETAT*XN)*X(N)
            COEF2 = DSIN(THETAT*XN)*X(N)
            CH    = DCOSH(ZN*ELEV)
            SH    = DSINH(ZN*ELEV)
            U(IK)     = U(IK)     - ZN*CH*COEF1
            V(IK)     = V(IK)     - ZN*SH*COEF2
            DUDZ(IK)  = DUDZ(IK)  - ZN*ZN*COEF1*SH
            DUDT(IK)  = DUDT(IK)  + ZN*ZN*COEF2*CH
         END DO
C
C     ...Velocity and local acceleration (du/dt=-c du/dx)
C
         DUDT(IK) = -CC*DUDT(IK)
      END DO
C
      RETURN
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE STRECO(IO6,US,G,THETA0)
C-----------------------------------------------------------------------
C
C     Generation of parameters X(n) for the stream function solution
C     Based on algorithm by Robert A. Dalrymple, University of Delaware
C                      Dean, R.G.,Streamfunction Representation
C                      of Nonlinear Ocean Waves, J.Geophysical
C                      Research, Vol. 70,4561-4572.
C
C                      Dalrymple, R.A., A Finite Amplitude Water
C                      Wave on a Linear Shear Current,
C                      J. Geophysical Research, Vol. 79, 4498-4504
C
C    Program Input :   Wave Height H, Period T and Water Depth DPT
C                      Linear Current: Surface US and Bottom UB Velocity
C                      Gravity G, th. ordre NORDER, iterations KMAX
C                      Damping DAMP, Nb. nodes on surf. NTHTS
C                      Number of points on lat. boundary NDY
C
C    Feb. 1994 by S. Grilli, University of Rhode Island
C
C    ERR: 01 : No convergence of the stream fct. solution. Check data
C
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (MAXT=50,MAXN=61)

      INTEGER*4 AUTOFF
C
      COMMON /STRCOE/ X,XF1,XF2,NN,NTHTS,QBAR,T,DPT,UBB,THETA,ETA,
     .                DTHETA,H,OMEGA
      DIMENSION X(MAXT),XF1(MAXN,MAXT),XF2(MAXN,MAXT)
      DIMENSION THETA(MAXN),ETA(MAXN)
C
      DATA PI/3.1415926535898D0/,ZERO/0.D0/,TWO/2.0D0/,ONE/1.0D0/,
     .     C1/0.1705D0/,C2/4.575D0/,EPS/1.D-06/,EPS1/0.01D0/,KMAX/20/,
     .     NORDER/20/,AUTOFF/1/,NTHTS/31/,C3/.01465D0/,C4/0.3D0/,
     .     HALF/0.5D0/,C5/.75D0/
C
       SAVE
C     SAVE /STRCOE/
C
C.....Begin analysis, check and initialize data
C                                                                                                                                            C     Vorticity
C
      OMEGA  = (US-UBB)/DPT
C
C     Order of theory used
C
      NN   = NORDER+1
      NNP1 = NN+1
      IFL  = 0
C
C     Set damping to minimum damping for numer. stability
C
      DAMP = C4
      IF(DABS(DAMP).LE.EPS) THEN
         DAMP = HALF
         IF(NN.GT.18) THEN
            DAMP = C4
         END IF
      END IF
C
C     Relative water depth
C
      WL0  = G*T*T / (TWO*PI)
      DBT2 = DPT/WL0
C
      IF (DBT2.LT.C3.AND.DAMP.GT.C4) THEN
         DAMP = C4
      END IF
C
C     Set phase angle vector
C
      XN       = NTHTS-1
      DTHETA   = PI/XN
      THETA(1) = ZERO
      DO I=2,NTHTS
         THETA(I) = THETA(I-1) + DTHETA
      END DO
C
C     Determine breaking index
C
      HBT2T = C1*DTANH(C2*DBT2)
      HBT2  = H/WL0
      TT    = HBT2/HBT2T
      IF (TT.GT.C5.AND.DAMP.GT.C4) THEN
         DAMP = C4
      END IF
      IF((HBT2-HBT2T).LE.EPS1) THEN
         IF(DABS(HBT2-HBT2T).LE.EPS1) THEN
            WRITE(IO6,2010)
            WRITE(IO6,2020)
            WRITE(IO6,2010)
         END IF
      ELSE
         WRITE(IO6,2010)
         HB = HBT2T*WL0
         WRITE(IO6,2030) HB
         WRITE(IO6,2010)
      END IF
C
C.....Main computing loop
C     Return index is initialized for loop to occur once
C
      IRET = 1
C
      DO WHILE(IRET.EQ.1)
C
C        Approximate wavelength in X(1)
C        Approximate ....       in X(2)
C
         SIG2G = ((TWO*PI/T)**2)/G
         XK    = SIG2G/DSQRT(DTANH(SIG2G*DPT))
         X(1)  = TWO*PI/XK
         X(1)  = X(1) + 0.2*X(1)*(HBT2/HBT2T)**2
         X(2)  =-X(1) * H/(T*TWO*DSINH(TWO*PI*(DPT+H/TWO)/X(1)))

C
C        Initialize stream function coefficients
C
         DO N=3,NN
            X(N) = ZERO
         END DO
C
         X(NNP1) = ZERO
C
C     ...Calculate stream function coefficients
C
         IRET = 0
         CALL STREAM(KMAX,DAMP,AUTOFF,IRET,IO6,G)
C        -----------
      END DO
C
C ....Results for converged results only
C
      IF (IRET.EQ.0) THEN
C
C     ...Calculate profile shift THETA0 for initial elev.=0
C        linear interpolation to start with
C
         WL = X(1)
         I=1
         DO WHILE(ETA(I).GE.ZERO)
            I=I+1
         END DO
         DX = WL*(THETA(I) - THETA(I-1))/(PI*TWO)
         RA = ETA(I-1)/(-ETA(I))
         DTX= RA*DX/(ONE+RA)
         THETA0 =  (THETA(I-1) + DTX*(PI*TWO)/WL)
C
         WRITE(IO6,2050) H,T,DPT,WL,WL/T
C
      ELSE IF(IRET.EQ.2.OR.IRET.EQ.3) THEN
C
C     ...Failed computation or singular matrix.
C
         CALL ERRORS('STREAM01')
C        -----------
      END IF
C
      RETURN
C
 2010 FORMAT(' ******************************************************')
 2020 FORMAT(' **** Wave parameters are within the accuracy of the ',
     .       'breaking indicator ***')
 2030 FORMAT(' **** Wave height exceeds breaking wave height of',
     .        F8.3,' ****')
 2050 FORMAT(/' Wave characteristics'/' --------------------'/
     .         ' Wave height = ',F8.4,' , Period     = ',F11.7,
     .         ' , Water depth = ',F8.4,/' Wavelength  = ',F11.7,
     .         ' , Celerity    = ',F11.7//)
C
      END
C-------------------------------------------------------------
      SUBROUTINE STREAM(KMAX,DAMP,AUTOFF,IRET,IO6,G)
C-------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (MAXT=50,MAXN=61)

      REAL*8 MSL
      INTEGER*4 AUTOFF
C
      COMMON /STRCOE/ X,XF1,XF2,NN,NTHTS,QBAR,T,DPT,UU,THETA,
     .                ETA,DTHETA,H,OMEGA
      DIMENSION X(MAXT),XF1(MAXN,MAXT),XF2(MAXN,MAXT)
      DIMENSION THETA(MAXN),ETA(MAXN)
C
      DATA ZERO/0.0D0/,EPS/1.D-06/,EPS1/0.01D0/
C
       SAVE
C     SAVE /STRCOE/
C
      IEND = 0
      DO I=1,NTHTS
         ETA(I) = ZERO
      END DO
      DO I=1,NTHTS
         DO N=2,NN
            XN = N-1
            XF1(I,N) = DCOS(THETA(I)*XN)
            XF2(I,N) = DSIN(THETA(I)*XN)
         END DO
      END DO
C
      KMAXP1 = KMAX + 1
      L      = 1
      IEND   = 0
      DO KI=1,KMAXP1
         IF (KI.EQ.KMAXP1) THEN
            L=2
         END IF
         CALL FSCALC(L,ERRORH,KMAX,IO6)
C        -----------
         IF (KI.EQ.KMAXP1) THEN
            IEND = 1
         END IF
         IKI = KI
         CALL CFF(DAMP,IEND,MSL,ERRORQ,IKI,IO6,G,KS)
C        --------
         TOTERR = DABS(ERRORH) + DABS(MSL) + DABS(ERRORQ)
C
         IF(KS.EQ.1) THEN
            IRET = 3
            RETURN
         END IF
         IF (AUTOFF.NE.1) THEN
            IF (TOTERR.LE.EPS1) THEN
               RETURN
            END IF
         ELSE
            IF (KI.EQ.1) THEN
               E1 = TOTERR
            END IF
            IF (TOTERR.GT.E1) THEN
               DAMP = DAMP - 0.1D0
               IF (DAMP.GT.EPS) THEN
                  WRITE(IO6,2000)
                  IRET = 1
                  RETURN
               ELSE
                  WRITE(IO6,2010)
                  IRET = 2
                  RETURN
               END IF
            END IF
         END IF
      END DO
C
      RETURN
C
 2000 FORMAT('          Solution diverging---Program is restarting with
     1       damping')
 2010 FORMAT(//' *****************************************'/' **********
     1Convergence impossible *****'/' *********************************
     2*************'/'                      Check input variables   ')
C
      END
C---------------------------------------------------------------------
      SUBROUTINE FSCALC(L,ERRORH,KMAX,IO6)
C---------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (MAXT=50,MAXN=61)

      COMMON /STRCOE/ X,XF1,XF2,NN,NTHTS,QBAR,T,DPT,UU,THETA,
     .                ETA,DTHETA,H,OMEGA
      DIMENSION X(MAXT),XF1(MAXN,MAXT),XF2(MAXN,MAXT)
      DIMENSION THETA(MAXN),ETA(MAXN)
C
       SAVE
C     SAVE /STRCOE/
C
      IEND = 0
      J    = 0
C
      DO I=1,NTHTS
         CALL FSI(I,ETA(I),N,IEND)
C        --------
         IF (N.GT.J) THEN
            J = N
         END IF
      END DO
      IF(IEND.NE.0) THEN
         WRITE(IO6,2000) KMAX
      END IF
C
      XH     = ETA(1) - ETA(NTHTS)
      ERRORH = H      - XH
      IF(L.NE.1) THEN
         WRITE(IO6,2010) XH,ERRORH
C        WRITE(IO6,2020)(THETA(I),ETA(I),I=1,NTHTS)
      END IF
C
      RETURN
C
 2000 FORMAT(I6,' iterations on eta failed to converge')
 2010 FORMAT(' Wave height = ',F8.4,'  , Error =',1PE10.3//)
 2020 FORMAT(' Thetas and calculated etas : '/
     .       ' ----------------------------'/(6(1X,F8.2,1X,F8.4)))
C
      END
C
C---------------------------------------------------------------------
      SUBROUTINE CFF(DAMP,IEND,MSL,ERRORQ,KI,IO6,G,KS)
C---------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      PARAMETER (MAXT=50,MAXN=61,MAXS=53,MAXU=2809)

      REAL*8 MSL
C
      COMMON /STRCOE/ X,XF1,XF2,NN,NTHTS,QBAR,T,DPT,UO,THETA,
     .                ETA,DTHETA,H,OMEGA
      DIMENSION X(MAXT),XF1(MAXN,MAXT),XF2(MAXN,MAXT)
      DIMENSION ETA(MAXN),DCDX(MAXS),DETADX(MAXN,MAXS),DUDX(MAXS),
     .          DVDX(MAXS),DQDX(MAXN,MAXS),Q(MAXN),B(MAXU),D(MAXS),
     .          ERROR(MAXN),THETA(MAXN),DQBAR(MAXS)
C
      DATA PI/3.1415926535898D0/,PI2/6.2831853071795D0/,ZERO/0.0D0/,
     .     ONE/1.0D0/,TWO/2.0D0/,THREE/3.0D0/,FOUR/4.0D0/
C
       SAVE
C     SAVE /STRCOE/
C
      CON     = PI2/X(1)
      NTHTM1  = NTHTS - 1
      QBAR    = ZERO
      NNP1    = NN + 1
      NNP3    = NN + 3
      DI      = DTHETA/(THREE*PI)
      XNTHTS  = NTHTS
      DCDX(1) = ONE/T
C
      DO N=2,NNP1
         DCDX(N) = ZERO
      END DO
C
      DO I=1,NTHTS
         U       = ZERO
         V       = ZERO
         DUDX(1) = ZERO
         DVDX(1) = ZERO
         DVDETA  = ZERO
         DUDETA  = ZERO
         DETADX(I,1) = ETA(I)/T
         TOTH = DPT + ETA(I)
         UU   = UO  + OMEGA*TOTH
C
         DO N=2,NN
            XN    = N - 1
            ZN    = CON*XN
            COEF1 = XF1(I,N)*X(N)
            COEF2 = XF2(I,N)*X(N)
            SH    = DSINH(ZN*TOTH)
            CH    = DCOSH(ZN*TOTH)
            U     = U-ZN*CH*COEF1
            V     = V-ZN*COEF2*SH
            DETADX(I,1) = DETADX(I,1)-ZN*TOTH/X(1)*CH*COEF1
            DVDX(N)     =-ZN*SH*XF2(I,N)
            DVDETA      = DVDETA-ZN*ZN*CH*COEF2
            DETADX(I,N) = SH*XF1(I,N)
            DUDX(1)     = DUDX(1)+(ZN*ZN*TOTH/X(1)*SH+ZN/X(1)*CH)*COEF1
            DUDX(N)     =-ZN*CH*XF1(I,N)
            DUDETA      = DUDETA-ZN*ZN*SH*COEF1
            DVDX(1)     = DVDX(1)+(ZN*ZN*TOTH/X(1)*CH+ZN/X(1)*SH)*COEF2
         END DO
C
         CC = U + UU - X(1)/T
         DQDETA         = ONE
         DETADX(I,NNP1) =-ONE
         DUDX(NNP1)     = ZERO
         DVDX(NNP1)     = ZERO
         DQDU           = CC/G
         DQDC           =-DQDU
         DQDV           = V/G
C
         DO N=1,NNP1
            DETADX(I,N) = DETADX(I,N)/CC
         END DO
         DO N=1,NNP1
            DQDX(I,N) = DQDETA*DETADX(I,N) + DQDU*(DUDX(N)+DUDETA
     .                * DETADX(I,N))+DQDV*(DVDX(N)+DVDETA*DETADX(I,N))
     .                + DQDC*DCDX(N)
         END DO
         Q(I) = ETA(I) + ONE/(TWO*G)*(CC*CC+V*V)
      END DO
C
      DO I=2,NTHTM1,2
         QBAR = QBAR + Q(I-1) + FOUR*Q(I)+Q(I+1)
      END DO
      QBAR = QBAR*DI
      DO N=1,NNP1
         DQBAR(N) = ZERO
         DO I=2,NTHTM1,2
            DQBAR(N) = DQBAR(N) + DQDX(I-1,N)
     .               + FOUR*DQDX(I,N) + DQDX(I+1,N)
         END DO
         DQBAR(N) = DQBAR(N)*DI
      END DO
C
      ERRORQ = ZERO
      DO I=1,NTHTS
         ERROR(I) = Q(I) - QBAR
      END DO
      DO I=2,NTHTM1,2
         ERRORQ = ERRORQ + ERROR(I-1)**2 + FOUR*ERROR(I)**2
     .          + ERROR(I+1)**2
      END DO
C
      ERRORQ = DSQRT(ERRORQ*DI)
C
      NNP2 = NN+2
      K    = 0
C
C.....Define an NNP3 X NNP1 array for B(K)
C
      DO N=1,NNP1
         DO M=1,NNP3
            K=K+1
            B(K) = ZERO
            IF(M.NE.NNP2.AND.M.NE.NNP3) THEN
               DO I=2,NTHTM1,2
                  B(K) = B(K) + (DQDX(I-1,N) - DQBAR(N))
     .                 * (DQDX(I-1,M)-DQBAR(M))
     .                 + FOUR*(DQDX(I,N)-DQBAR(N))*(DQDX(I,M)-DQBAR(M))
     .                 + (DQDX(I+1,N)-DQBAR(N))*(DQDX(I+1,M)-DQBAR(M))
               END DO
               B(K) = B(K)*DTHETA/THREE
            END IF
            IF(M.EQ.NNP2) THEN
               DO I=2,NTHTM1,2
                  B(K) = B(K) + DETADX(I-1,N) + FOUR*DETADX(I,N)
     .                        + DETADX(I+1,N)
               END DO
               B(K) = B(K)*DTHETA/THREE
            ELSE IF(M.EQ.NNP3) THEN
               B(K) = DETADX(1,N)-DETADX(NTHTS,N)
            END IF
         END DO
      END DO
C
C.....LAMBDA1 coefficients
C
      DO M=1,NNP3
         D(M) = ZERO
         K    = K + 1
         B(K) = ZERO
         IF(M.NE.NNP2.AND.M.NE.NNP3) THEN
            DO I=2,NTHTM1,2
               B(K) = B(K) + DETADX(I-1,M) + FOUR*DETADX(I,M)
     .              + DETADX(I+1,M)
            END DO
            B(K) = B(K)*DTHETA/THREE
            DO I=2,NTHTM1,2
               D(M) = D(M) + (QBAR-Q(I-1))*(DQDX(I-1,M)-DQBAR(M))
     .              + FOUR*(QBAR-Q(I))*(DQDX(I,M)-DQBAR(M))
     .              + (QBAR-Q(I+1))*(DQDX(I+1,M)-DQBAR(M))
            END DO
            D(M) = D(M)*DTHETA/THREE
         END IF
         IF (M.EQ.NNP2) THEN
            DO I=2,NTHTM1,2
               D(M) = D(M) + ETA(I-1) + FOUR*ETA(I)+ETA(I+1)
            END DO
            D(M) = D(M)*DTHETA/THREE
            MSL  = D(M)/PI
            IF (IEND.EQ.1) THEN
               WRITE(IO6,2000) MSL
               RETURN
            END IF
            D(M) = -D(M)
         ELSE IF (M.EQ.NNP3) THEN
            D(M) = H + ETA(NTHTS) - ETA(1)
         END IF
      END DO
C
C.....LAMBDA2 coefficients
C
      DO M=1,NNP3
         K = K+1
         B(K) = ZERO
         IF (M.LT.NNP2) THEN
            B(K) = DETADX(1,M) - DETADX(NTHTS,M)
         END IF
      END DO
      CALL SIMQ(B,D,NNP3,KS)
C     ---------
      IF (KS.EQ.1) THEN
         WRITE(IO6,2010)
      END IF
      DO N=1,NNP1
         X(N) = X(N) + D(N)*DAMP
      END DO
C
      RETURN
C
 2000 FORMAT(' MSL error          = ',1PE10.3)
 2010 FORMAT('          CFF matrix is singular')
C
      END
C---------------------------------------------------------------
      SUBROUTINE FUNC(ET,Y,YP,I)
C---------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      PARAMETER (MAXT=50,MAXN=61)

      COMMON /STRCOE/ X,XF1,XF2,NN,IDU,QBAR,T,DPT,UU,
     .                DUN2(2*MAXN+2),OMEGA
      DIMENSION X(MAXT),XF1(MAXN,MAXT),XF2(MAXN,MAXT)
C
      DATA PI2/6.2831853071795D0/,HALF/0.5D0/
C
       SAVE
C     SAVE /STRCOE/
C
      CON  = PI2/X(1)
      NNP1 = NN+1
      C    = X(1)/T - UU
      ELEV = DPT + ET
      Y    = X(NNP1) - C*ET + OMEGA*ELEV*ELEV*HALF
      YP   = -C + OMEGA*ELEV
      DO N=2,NN
         ZN = CON*DFLOAT(N-1)
         Y  = Y - X(N)*DSINH(ZN*ELEV)*XF1(I,N)
         YP = YP- X(N)*DCOSH(ZN*ELEV)*XF1(I,N)*ZN
      END DO
C
      RETURN
C
      END
C----------------------------------------------------------------
      SUBROUTINE FSI(I,X,N,IEND)
C----------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      PARAMETER (M=20)
C
      DATA EPSIL/0.001D0/,ONE/1.D0/,TWO/2.D0/,
     .     THREE/3.D0/
C
       SAVE
C
      XPP = X
      CALL FUNC(XPP,YPP,YDPP,I)
C     ---------
      XP = XPP-YPP/YDPP
C
      DO N=1,M
         CALL FUNC(XP,YP,YDP,I)
C        ---------
         D = XP-XPP
         A = (TWO*YDP + YDPP - THREE*(YP-YPP)/D)/D
         U = YP/YDP
         X = XP - U*(ONE + U*A/YDP)
C
         IF (DABS((X-XP)/X).GT.EPSIL) THEN
            XPP = XP
            XP  = X
            YPP = YP
            YDPP= YDP
         ELSE
            RETURN
         END IF
      END DO
C
      IEND = 1
C
      RETURN
C
      END
C---------------------------------------------------------------
      SUBROUTINE SIMQ(A,B,N,KS)
C---------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      PARAMETER (MAXS=53,MAXU=2809)
C
      DIMENSION A(MAXU),B(MAXS)
C
      DATA ZERO/0.0D0/
C
       SAVE
C
C.....Forward solution
C
      TOL = ZERO
      KS  = 0
      JJ  = -N
      DO J=1,N
         JY   = J+1
         JJ   = JJ+N+1
         BIGA = ZERO
         IT   = JJ-J
         DO I=J,N
C
C        ...Search for maximum coefficient in column
C
            IJ = IT+I
            IF(DABS(BIGA).LT.DABS(A(IJ))) THEN
               BIGA = A(IJ)
               IMAX = I
            END IF
         END DO
C
C     ...Test for pivot less than tolerance (singular matrix)
C
         IF(DABS(BIGA).LE.TOL) THEN
            KS=1
            RETURN
         ELSE
C
C        ...Interchange rows if necessary
C
            I1 = J+N*(J-2)
            IT = IMAX-J
            DO K=J,N
               I1   = I1+N
               I2   = I1+IT
               SAVE = A(I1)
               A(I1)= A(I2)
               A(I2)= SAVE
C
C           ...Divide equation by leading coefficient
C
               A(I1)= A(I1)/BIGA
            END DO
            SAVE    = B(IMAX)
            B(IMAX) = B(J)
            B(J)    = SAVE/BIGA
C
C        ...Eliminate next variable
C
            IF(J.NE.N) THEN
               IQS = N*(J-1)
               DO IX=JY,N
                  IXJ = IQS+IX
                  IT  = J-IX
                  DO JX=JY,N
                     IXJX = N*(JX-1)+IX
                     JJX  = IXJX+IT
                     A(IXJX) = A(IXJX) - (A(IXJ)*A(JJX))
                  END DO
                  B(IX) = B(IX) - (B(J)*A(IXJ))
                END DO
            END IF
         END IF
      END DO
C
C.....Backward solution
C
      NY = N-1
      IT = N*N
      DO JJ=1,NY
         IA = IT-JJ
         IB = N-JJ
         IC = N
         DO K=1,JJ
            B(IB) = B(IB) - A(IA)*B(IC)
            IA = IA-N
            IC = IC-1
         END DO
      END DO
C
      RETURN
C
      END
C
C-----------------------------------------------------------------
      SUBROUTINE POLINT(XX,YY,NN,X,Y,DY)
C-----------------------------------------------------------------
C
C     Polynomial interpolation subroutine fron Numerical Recipes
C     Given XA(N) and YA(N), POLINT returns Y corr. to X with
C     error DY by "Neville's Algorithm"
C
C     Modified to restrict polynomial order to 10
C     by selecting 5 pts on either side adjoining Y
C
C-----------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      PARAMETER (NMAX=200,NOPT=10)
C
      DIMENSION XX(NN),YY(NN),C(NMAX),D(NMAX),XA(NOPT),YA(NOPT)
C
       SAVE
C
C     Modification to restrict order of interpolating polynomial to 10
C
      NS  = 1
      DIF = DABS(X-XX(1))
C
C     First locate the closest entry in the table
C
      DO I = 1,NN
         DIFT = DABS(X - XX(I))
         IF (DIFT.LT.DIF) THEN
            NS  = I
            DIF = DIFT
         END IF
      END DO
      NHALF = NOPT/2
C
C     generate new XA and YA vectors
C
      IF (NS.GT.NHALF.AND.NS.LT.NN-NHALF) THEN
         IL = NS-NHALF
         IR = NS+NHALF-1
      ELSE IF (NS.LE.NHALF) THEN
         IL = 1
         IR = NOPT
      ELSE
         IL = NN-NOPT+1
         IR = NN
      END IF
C
      DO I = IL,IR
         XA(I-IL+1) = XX(I)
         YA(I-IL+1) = YY(I)
      END DO
      N = NOPT
C
      NS  = 1
      DIF = DABS(X-XA(1))
C
C     First locate the closest entry in the table
C
      DO I = 1,N
         DIFT = DABS(X - XA(I))
         IF (DIFT.LT.DIF) THEN
            NS  = I
            DIF = DIFT
         END IF
C
         C(I) = YA(I)
         D(I) = YA(I)
      END DO
C
C     Approximate Y
C
      Y  = YA(NS)
      NS = NS-1
C
      DO M = 1,N-1
         DO I = 1,N-M
            HO  = XA(I)-X
            HP  = XA(I+M)-X
            W   = C(I+1)-D(I)
            DEN = HO-HP
C
C           Error in case of identical abscissas
C
            IF (DEN.EQ.0.) CALL ERRORS('POLINT01')
C                          -----------
            DEN = W/DEN
            D(I)= HP*DEN
            C(I)= HO*DEN
         END DO
C
         IF (2*NS.LT.N-M) THEN
            DY = C(NS+1)
         ELSE
            DY = D(NS)
            NS = NS-1
         END IF
C
         Y = Y+DY
      END DO
C
      RETURN
C
      END
C--------------------------------------------------------------------
      SUBROUTINE INTSIM(DI,DH,NSIMP,TI)
C--------------------------------------------------------------------
C
C     Computes the Simpson's integral of function DI over NSIMP
C     fn. evaluation points at DH increments and returns the
C     integrated value in TI
C
C--------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      DIMENSION DI(NSIMP)
C
      DATA ZERO/0.D0/,TWO/2.D0/,THIRD/0.3333333333333333D0/,
     .     FOUR/4.D0/
       SAVE
C
      TI1 = ZERO
      TI2 = ZERO
C
      DO I = 2,NSIMP-1,2
         TI1 = TI1 + DI(I)
      END DO
C
      DO I = 3,NSIMP-2,2
         TI2 = TI2 + DI(I)
      END DO
C
      TI = DI(1) + DI(NSIMP) + TWO*TI2 + FOUR*TI1
      TI = TI*DH*THIRD
C
      RETURN
C
      END
C
C--------------------------------------------------------------------
      SUBROUTINE EVAL_EXP(T, EXPDATA, RESULT)
C--------------------------------------------------------------------
C     
C     Author : Raphael Poncet 06/2009
C     Given a sequence of experimental data in array EXPDATA, evaluate 
C     the linear interpolation at time t.
C     
C     WARNING : the time sampling and number of experimental points 
C     are hardcoded
C
C     
C--------------------------------------------------------------------
      DOUBLE PRECISION T
      DOUBLE PRECISION EXPDATA(90001)

      DOUBLE PRECISION DT
      DOUBLE PRECISION RESULT
      INTEGER I

      DT = 2.D-3

C     Find the closest experimental data points and interpolate
C     linearly between them
      I = INT(T/DT)

      IF ( I.GT.90002 ) THEN
         RESULT = 0.D0
      ELSE
         RESULT = EXPDATA(I) + (T/DT-DBLE(I))*(EXPDATA(I+1)-EXPDATA(I))
      END IF

      RETURN 
      END SUBROUTINE EVAL_EXP

C--------------------------------------------------------------------
      SUBROUTINE EVAL_DER_EXP(T, EXPDATA, RESULT)
C--------------------------------------------------------------------
C     
C     Author : Raphael Poncet 06/2009
C     Given a sequence of experimental data in array EXPDATA, evaluate 
C     its derivative at time t.
C     
C     WARNING : the time sampling and number of experimental points 
C     are hardcoded
C     
C     
C--------------------------------------------------------------------
      DOUBLE PRECISION T
      DOUBLE PRECISION EXPDATA(90001)

      DOUBLE PRECISION DT
      DOUBLE PRECISION RESULT
      INTEGER I

      DT = 2.D-3

C     Find the closest experimental data points and interpolate
C     linearly between them
      I = INT(T/DT)

      IF ( I.GT.90002 ) THEN
         RESULT = 0.D0
      ELSE
         RESULT = (EXPDATA(I+1)-EXPDATA(I))/DT
      END IF

      RETURN 
      END SUBROUTINE EVAL_DER_EXP



C----------------------------------------------------------------------
      SUBROUTINE PADSIN_WRL(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C----------------------------------------------------------------------
C0  PADSINF   PADSIN of the WRL
C1  PURPOSE   Simulation of a paddle wavemaker oscillating on the
C1            lateral boundary K(2,4). Both geometry and B.C. are updat.
C1            or only the B.C. (IFLAGV=1,2). The paddle motion is damped
C1            according to D(t,Tdamp) (1 if Tdamp <eps). The motion is :
C1            for z=0 :
C1
C1            Xp(t)=-0.25 Ap (tanh((4wpt)/N Pi)+1)(1-tanh(4(wpt-2 Pi N)/N Pi)) sin (wp(t-0.018t**2))
C1            Up(t)=d Xp(t)/dt, d Up(t)/dt= d2Xp(t)/dt2
C1            D(t) =
C1            
C1	There is no damping function as everythings is included in the description of Xp
C1
C1            Xf the motion, up,upt the velocity and acceleration (z=0)
C1
C2  CALL PADSIN(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C3  CALL ARG. XYZNOD()            = Coordin. of nofes for current step
C3            PHIS(NOM),PHISS     = Phis and Phiss imposed on boundaries
C3            PHIM(NOM),PHIMM     = Phim and Phimm imposed on boundaries
C3            PHINS(NOM),PHINM    = Phins and Phinm imposed on boundaries
C3            QBC(NOM)            = du/dn imposed on Neuman boundaries
C3            K                   = Considered later. boundary (2,or 4)
C3            TIME                = Current time
C3            TUNE                = Tune the overall motion >0
C3            DE(2)               = Depth at K=2,4
C3            TDAMP=2*T0          = 1% Damping time (0: no,>0: yes)
C3            IFLAGV              = (1,2) update geom.+B.C. or B.C. only
C3            IPTYP               = 1 sine or sume of sine waves
C3                                = 8 wave focusing at (0,DF)
C3
C4  RET. ARG. XYZNOD(NOM,3),QBC(NOM)  on K
C9  Feb. 99   S. GRILLI, INLN
CLPADSIN SUB. WHICH SIMULATE MOTION AND KINEMATICS OF PADDLE WAVEMAKER
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM0(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      include 'maille.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM+18),
c     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
c     .                IDUM3(66),INODF(6,2),IBCOND(6)
c      COMMON/SYSTEM/  DUM3(24),TIME,TDAMP,TDOWN,DUM31(4+8*MYMAX),IDU(2),
c     .                IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOUNDC/ DUM4(9),DE(2),OMEGA,TUNE,DUM6,IDW
c      COMMON /PADDLE/ AP(NFMAX,NHMAX),OP(NFMAX),SP(NHMAX),ZKWAVE(NFMAX),
c     .                DISTF,TFOC,ALPHAF,WMIDTH,OS(MYMAX),OST(MYMAX),
c     .                OSTT(MYMAX),DAMP,DAMPT,DAMPTT,DELFR,NFR,NH,IFREQ
C
      DIMENSION QBC(NOM),PHIS(NOM),PHIM(NOM),PHISS(NOM),PHIMM(NOM),
     .          PHINS(NOM),PHINM(NOM)
C
C
C       SAVE
      include 'paddle2.h'
      include 'paddle.h'
      logical Converge,CurvedPaddle
      Integer NN

      DATA ONE/1.D0/,TWO/2.D0/,ZERO/0.D0/,EPS/1.D-08/,
     .     HALF/0.5D0/,CD/2.3025D0/,FOUR/4.D0/,PI/3.1415926535898D0/
C     SAVE /MAILLE/,/SYSTEM/,DATGEN/,/BOUNDC/,/PADDLE/
C
c#############################################################################
c	inputs: potential en curvilinear coordinates (s,n,m)
c	outputs: UBC=velocity, QBC=du/dn
c
c	DEC: local depth a the boundary K
c	IB: first point of the boundary K in the node table
c       NN number of wave in the paddle motion

       
      DEC = DE(K/2)
      IST = 3 - K
      IB  = INODF(K,1)

      Converge=.false.
      CurvedPaddle=.false.
      Coeft2=0.018d0
      Tdec=-5.d0
      NN=INN

      if (Iconv.eq.1) Converge=.true.
      if (Icurved.eq.1) CurvedPaddle=.true.
C      print*,'Icurved',ILOOP,Iconv,Converge,Icurved,CurvedPaddle
C      print*,'Icurved2',Xconv, Yconv

      IF((IFLAGV.EQ.1).OR.(ILOOP.EQ.1)) THEN

C 
C
C.....Store parameters at beginning of time loop
C


C
C     ...Current motion at z=0 without damping
C
c--------------------------------------------------------
c	debug
113     FORMAT(5(G21.14,1X))

C(omega,t,coeft2,N,Pi)
C------------------------------------------------------------
c	Construction of the paddle motion
c	Double loops :
c			- first on frequencies: IFR fom 1 to NFR
c			- then on the directions NH
c

         DO IFR=1,NFR
c-------------------------------------------------------------
c	Construction of the 'carrier' frequency
c	opti=wp((t-tfoc)-coeft2 * 0.5 * t**2)
c	 
c          OPTI = OP(IFR)*(TIME-TFOC-0.018/TWO*TIME*TIME)
c          OPTI = OP(IFR)*(TIME-TFOC-Coeft2/TWO*TIME*TIME)
          OPTI = OP(IFR)
	  
	  ZKK=0.d0
	  if (Converge) call CELES(OPTI,CEL,ZKK)
!          print*,'celes',OPTI,CEL,ZKK

c------------------------------------------------
C        WRL wave basin damping (tanh )
c------------------------------------------------
            DAMP   = PaddleDAMP(OPTI,TIME+Tdec,coeft2,NN,PI)
            DAMPT  = PaddleDAMPT(OPTI,TIME+Tdec,coeft2,NN,PI)
            DAMPTT = PaddleDAMPTT(OPTI,TIME+Tdec,coeft2,NN,PI)

c	print*,'dans padsinWRL, AMPL, omega,time, time+Tdec, coeft2'
c	write(*,113) AMPL,OPTI,TIME,TIME+Tdec,coeft2  
c--------------------------------------------------------
         DO I=1,MY+1
               OS(I)   = 0.d0
               OST(I)  = 0.d0
               OSTT(I) = 0.d0
         END DO
	  
	  IF(NFR.EQ.NH.AND.IFREQ.EQ.1) THEN
	    NH2 = 1
	    IH = IFR
	  ELSE
	    NH2=NH
	  END IF
          DO IH=1,NH2

c-------------------------------------------------------------
c	Amplitude
c
c            AMPL = AP(IFR,IH)*TUNE
            AMPL = AP(IFR,IH)
c            AMPL = -0.025d0*0.0005d0
		

c-------------------------------------------------------------
c	phase 2D
c
	    PHASE=0.d0!HALF*PI
c            CO   = DCOS(PHASE + OPTI)
c            SI   = DSIN(PHASE + OPTI)
c-------------------------------------------------------------
c	relative phase 2D  when more than one wavemaker
	    THETAN = ZERO
	    PPhase = 0.d0
C
C           y of free surface is used on wm. for vertical snake lines
C
c-----------------------------------------------------------------
c	points of both the free surface and the wave maker

            DO I=1,MY+1
c	Ns= indice of the free surface point, IB=INODF(K,1)
	       NS  = IB + MZ*(MY+1) + I - 1
C
	       IF((I.GT.1).AND.(I.LE.MY)) THEN
c	If the points are on the boundaries (I=1 or I=MY+1): the corners
	          YP = XYZNOD(LXYZ(NS,1),2)
	       ELSE
c	else inner points
	          YP = XYZNOD(NS,2)
	       END IF
c	       ZKY = ZKWAVE(IFR)*DSIN(THETAN)
c	       CY  = DCOS(ZKY*YP)
c	       SY  = DSIN(ZKY*YP)


	       if (Converge) then
	         THETAN=datan2(YP-Yconv,Xconv)
		 PPhase=ZKK*YP*dsin(THETAN)+ZKK*(Xconv*dcos(THETAN)+
     &	                Yconv*dsin(THETAN))
               endif
C
c               OS(I)   = OS(I)   + AMPL*(CY*CO + SY*SI)
c               OST(I)  = OST(I)  + AMPL*OP(IFR)*(-1.0*CY*SI + 
c     1              SY*CO)*(ONE - 0.018*TIME)
c               OSTT(I) = OSTT(I) - AMPL*OP(IFR)*OP(IFR)*(CY*CO + SY*SI)
c     1          *(ONE - 0.018*TIME) + (-CY*SI + SY*CO)*(-0.018)
c		print*,'WRL PADDLE',AMPL,OPTI,TIME,coeft2,PI
              OS(I)   = AMPL*PaddleOS(OPTI,TIME+Tdec,coeft2,NN,PPhase)
              OST(I)  = AMPL*PaddleOST(OPTI,TIME+Tdec,coeft2,NN,PPhase)
              OSTT(I) = AMPL*PaddleOSTT(OPTI,TIME+Tdec,coeft2,NN,PPhase)
!	      write(*,2003) 'OP',OPTI,TIME+Tdec,coeft2,NN,PPhase
c       print*,'zk y sin ',I,ZKK*YP*dsin(THETAN)
c       print*,'k xc cos k yc sin',I,ZKK*Xconv*dcos(THETAN),
c     &      ZKK*Yconv*dsin(THETAN)
c       print*,'theta, phase',I,THETAN,PPhase
!       write(*,2001) I,YP,THETAN,ZKK*YP*dsin(THETAN),
!     &       ZKK*Xconv*dcos(THETAN),ZKK*Yconv*dsin(THETAN),
!     &      PPhase,OS(I),OST(I),OSTT(I)
        
2001  format(i4,1x,9(G21.14,1X))     
2003  format(a2,1x,3(G21.14,1X),i3,1x,G21.14)     
!       print*,'HAHAHA',I,OS(I),OST(I),OSTT(I)

	    END DO 
          END DO
         END DO
      END IF
C
C.....Updating boundary conditions and/or geometry (IFLAG=1,2)
C     Assumed to be on boundary K=2 or 4
C
	
	
      IF(IFLAGV.EQ.1) THEN
         DO I=1,MY+1
c	node at the bottom	 
	    NB  = IB + I - 1
c	node at the free surface	 
	    NS  = NB + MZ*(MY+1)
C
C           Current motion at z=0
C
c	XP paddle position = oscillation*damp
            XP  = OS(I)*DAMP
c	UP paddle velocity 
            UP  = OST(I)*DAMP + OS(I)*DAMPT
C
C           Node coord. updating : x(0)=XP; z-free surface is forced
C           on top; and y-free surface on top and bottom of flap wm.
C
	    IF((I.GT.1).AND.(I.LE.MY)) THEN
	       YP = XYZNOD(LXYZ(NS,1),2)
	       XYZNOD(NB,2) = YP
	       XYZNOD(NS,2) = YP
	    ELSE
	       YP = XYZNOD(NS,2)
               XYZNOD(LXYZ(NS,1),2) = YP
            END IF
c------------------------------------------------------------------------------------------
c	for external lines, force the points to be on the boundaries
c	inner points, update paddle position point at the free surface(knowing it is linear) 
c		and passing thru the hinge at the bottom and Xp. so Xs(free surface)=XP(paddle)*(1+eta/Dec)	    
	    ZFS = XYZNOD(LXYZ(NS,1),3)
	    XYZNOD(NS,3) = ZFS
	    XYZNOD(NS,1) = XP*(ONE + ZFS/DEC)

c if the paddle is curved, i know the equation implying the system is 
c    connected at z=0:
c    z<0 x= XP/(2*DEC**2)*((z+DEC)**3-3*DEC*((z+DEC)**2)
c    z>0 x= XP/(2*DEC**2)*(DEC**3-3*DEC**2*((z+DEC))
	    if (CurvedPaddle) then
	      if (ZFS.gt. 0.d0) then
	        XYZNOD(NS,1) = XP/(2.d0*(DEC**2.d0))*
     &    (DEC**3.d0 - 3.d0*DEC*(ZFS+DEC)**2.d0)
	      else
	        XYZNOD(NS,1) = XP/(2.d0*(DEC**2.d0))*
     &    ((ZFS+DEC)**3.d0 - 3.d0*(ZFS+DEC)*DEC**2.d0)
	      endif
	    endif
	      
c------------------------------------------------------------------------------------------
C
	    DO J=1,MZ+1
	       IND = IB + (J-1)*(MY+1) + I - 1

	       if (CurvedPaddle) then
                DO KK=2,3
                  XYZNOD(IND,KK) = XYZNOD(NB,KK) + (J-1)*
     .                           (XYZNOD(NS,KK) - XYZNOD(NB,KK))/MZ
                 END DO
		 ZZZ=XYZNOD(IND,3)
	         if (ZZZ.gt. 0.d0) then
	           ZZZ=XYZNOD(IND,3)+DEC
	           XXX = -XP/(2.d0*DEC**3.d0)*(DEC**3.d0 - 
     &              3.d0*ZZZ*DEC**2.d0)
                   XYZNOD(IND,1) = XXX 
		   
	           RG =dsqrt(ZZZ**2.d0 + XXX**2.d0)
                   dRGdt=1.d0/RG*(  XP/(2.d0*DEC**3.d0)*
     &    ((DEC**3.d0 - 3.d0*ZZZ*DEC**2.d0)**2.d0)*UP/(2.d0*DEC**3.d0))
                   omeg=-3.d0*XP/(2.d0*DEC)
                   DomegDt=-3.d0*UP/(2.d0*DEC)
                   TT=DomegDt
     
	         else 
	           ZZZ=XYZNOD(IND,3)+DEC
	           XXX = -XP/(2.d0*DEC**3.d0)*(ZZZ**3.d0 - 
     &	            3.d0*DEC*ZZZ**2.d0)
                   XYZNOD(IND,1) = XXX
	           if (J.eq.1) then
		     dRGdt=0.d0
		     omeg=0.d0
		     DomegDt=0.d0
		   else
	           RG =dsqrt(ZZZ**2.d0 + XXX**2.d0)
                   dRGdt=1.d0/RG*(  XP/(2.d0*DEC**3.d0)*
     &     ((ZZZ**3.d0 - 3.d0*DEC*ZZZ**2.d0)**2.d0)*UP/(2.d0*DEC**3.d0))
                   omeg=(3.d0*XP/(2.d0*DEC))*(ZZZ**2.d0-2.d0*DEC*ZZZ)
                   DomegDt=(3.d0*UP/(2.d0*DEC))*(ZZZ**2.d0-2.d0*DEC*ZZZ)
                   TT=DomegDt
		   endif
		     
	         endif
	       else !linear paddle
                 DO KK=1,3
                  XYZNOD(IND,KK) = XYZNOD(NB,KK) + (J-1)*
     .                           (XYZNOD(NS,KK) - XYZNOD(NB,KK))/MZ
                 END DO
c	DPL distance between (XP,y,0) and the axis of rotation.
                 PL  = DEC*DEC + XP*XP
                 DPL = DSQRT(PL)
c	TT D omega/Dt
                 TT  =-DEC*UP/PL
	         RG  = (ONE + XYZNOD(IND,3)/DEC)*DPL
c	       print*,'linear',RG
	       endif
C 	IST direction of the normal
c	RG= YP

c
c BOUNDARY CONDITION= D phi /Dn = R Dtheta/Dt Etheta	    

              QBC(IND) = QBC(IND) + RG*TT*IST
!	      if (j.eq.5)
!     &	      write(*,2002) 'QBS',i,RG,TT,QBC(IND)
2002  format(a3,1x,i4,1x,3(G21.14,1X))     
	    END DO
C
C           x-surf. = x flap
C
	    XYZNOD(LXYZ(NS,1),1) = XYZNOD(NS,1)
         END DO
      ELSE

         DO I=1,MY+1
	    NB  = IB + I - 1
C
C           Current motion at z=0
C
c	XP paddle position = oscillation*damp
            XP  = OS(I)*DAMP
c	UP paddle velocity 
            UP  = OST(I)*DAMP + OS(I)*DAMPT
            UPT = OSTT(I)*DAMP + TWO*OST(I)*DAMPT + OS(I)*DAMPTT
C
	    DO J=1,MZ+1
	       IND      = IB + (J-1)*(MY+1) + I - 1
	       if (CurvedPaddle) then
		 ZZZ=XYZNOD(IND,3)
	         if (ZZZ.gt. 0.d0) then
	           ZZZ=XYZNOD(IND,3)+DEC
	           XXX = -XP/(2.d0*DEC**3.d0)*(DEC**3.d0 - 
     &              3.d0*ZZZ*DEC**2.d0)
		   
	           RG =dsqrt(ZZZ**2.d0 + XXX**2.d0)
                   dRGdt=1.d0/RG*(  XP/(2.d0*DEC**3.d0)*
     &     ((DEC**3.d0 - 3.d0*ZZZ*DEC**2.d0)**2.d0)*UP/(2.d0*DEC**3.d0))
                   omeg=-3.d0*XP/(2.d0*DEC)
                   DomegDt=-3.d0*UP/(2.d0*DEC)
                   D2omegDt2=-3.d0*UPT/(2.d0*DEC)
                   TT=DomegDt
		   TTT=D2omegDt2
!            print*,'7778'
	         else
!            print*,'8778'
	           ZZZ=XYZNOD(IND,3)+DEC
	           XXX = -XP/(2.d0*DEC**3.d0)*(ZZZ**3.d0 - 
     &	            3.d0*DEC*ZZZ**2.d0)
!            print*,'8777'
	           if (J.eq.1) then
		     dRGdt=0.d0
		     omeg=0.d0
		     DomegDt=0.d0
		     D2omegDt2=0.d0
		   else  
	           RG =dsqrt(ZZZ**2.d0 + XXX**2.d0)
                   dRGdt=1.d0/RG*(  XP/(2.d0*DEC**3.d0)*
     &     ((ZZZ**3.d0 - 3.d0*DEC*ZZZ**2.d0)**2.d0)*UP/(2.d0*DEC**3.d0))
                   omeg=(3.d0*XP/(2.d0*DEC))*(ZZZ**2.d0-2.d0*DEC*ZZZ)
                   DomegDt=(3.d0*UP/(2.d0*DEC))*(ZZZ**2.d0-2.d0*DEC*ZZZ)
                   D2omegDt2=(3.d0*UPT/(2.d0*DEC))*
     &        (ZZZ**2.d0-2.d0*DEC*ZZZ)
                   endif
                   TT=DomegDt
		   TTT=D2omegDt2
!            print*,'8888'
		 endif 
!       print*,'8888'
	       
	       
	       else !linear paddle
c	DPL distance between (XP,y,0) and the axis of rotation.
                   PL  = DEC*DEC + XP*XP
                   DPL = DSQRT(PL)
                   TT  =  -DEC*UP/PL
C ajout Up je pense qu'il y avait une erreur
                   TTT = -(DEC*UPT + TWO*UP*XP*TT)/PL
C old line      TTT = -(DEC*UPT + TWO*XP*TT)/PL


	       RG       = (ONE + XYZNOD(IND,3)/DEC)*DPL
	       endif
	       WRITE(97,112) TIME,I,XP,UP,UPT,TT,TTT
c	       RG       = (ONE + XYZNOD(IND,3)/DEC)*DPL
C 	IST direction of the normal
c	RG= YP

!	      if (j.eq.5)
!     &	      write(*,2000) 'PHI',i,RG,TT,TTT,PHISS(IND),PHIMM(IND),
!     & PHIS(IND),PHINS(IND),PHIM(IND),PHINM(IND)
               QBC(IND) = RG*IST*(TTT + TT*(PHISS(IND) + PHIMM(IND)))
     .                  -(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
c----------------------------
c	Old line, may be an error on second line, no normal vector on last component
c----------------------------------------
c               QBC(IND) = RG*IST*(TTT + TT*(PHISS(IND) + PHIMM(IND)))
c     .                  -IST*(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
!       print*,'9999'
!	      if (j.eq.5)
!     &	      write(*,*) 'QBS2',i,QBC(IND)

               write(142,111) TIME,I,J,RG, XP, UP, UPT,TT,TTT,QBC(IND)
           END DO
         END DO
      END IF
111    FORMAT((G21.14,1X),2(i3,1X),7(G21.14,1X))
112    FORMAT(G21.14,1X,i5,1X,5(G21.14,1X))
2000   FORMAT(a3,1x,i2,1x,9(G21.14,1X))
C
      RETURN
C
      END
C----------------------------------------------------------------------
C----------------------------------------------------------------------
c	WRL Paddle motion     
c	
c	PaddleOS    oscillatory part of the paddle motion 
c	PaddleOST   first time derivative of the oscillatory part
c	PaddleOSTT  second time derivative of the oscillatory part
c	PaddleDAMP    damping part of the paddle motion 
c	PaddleDAMPT   first time derivative of the damping part
c	PaddleDAMPTT  second time derivative of the damping part
C----------------------------------------------------------------------
C----------------------------------------------------------------------
	function PaddleOS(omega,t,coeft2,N,PPhase)
	IMPLICIT REAL*8 (A-H,O-Z)

	phase=PPhase+0.5d0 * 3.1415926535898D0
	CCC= omega *(t- 0.5D0*coeft2*t**2) + phase
	PaddleOS=dsin(CCC)
	return
	end

	function PaddleOST(omega,t,coeft2,N,PPhase)
	IMPLICIT REAL*8 (A-H,O-Z)

	phase=PPhase+0.5d0 * 3.1415926535898D0
	CCC= omega *(t- 0.5D0*coeft2*t**2) + phase
	PaddleOST=omega*(1-coeft2*t)*dcos(CCC)
c	PaddleOST=omega*(1-coeft2)*dcos(CCC)
	return
	end

	function PaddleOSTT(omega,t,coeft2,N,PPhase)
	IMPLICIT REAL*8 (A-H,O-Z)

	phase=PPhase+0.5d0 * 3.1415926535898D0
	CCC= omega *(t- 0.5D0*coeft2*t**2) + phase
       PaddleOSTT=-omega*coeft2*dcos(CCC)-omega*omega*(1-coeft2*t)*
     &      (1-coeft2*t)*dsin(CCC)
c	PaddleOSTT=-omega*coeft2*dcos(CCC)-omega*omega*(1-coeft2)*
c     &      (1-coeft2)*dsin(CCC)
	return
	end

	function PaddleDAMP(omega,t,coeft2,N,Pi)
	IMPLICIT REAL*8 (A-H,O-Z)
	
	AAA= (4*(-2*N*Pi + omega*t))/(N *Pi)
	BBB= (4*omega*t)/(N*Pi)
	PaddleDAMP=(1 + dtanh(BBB))*(1 - dtanh(AAA))
	return
	end

	function PaddleDAMPT(omega,t,coeft2,N,Pi)
	IMPLICIT REAL*8 (A-H,O-Z)
	
	AAA= (4*(-2*N*Pi + omega*t))/(N *Pi)
	BBB= (4*omega*t)/(N*Pi)
	DDD= (4.d0*omega) / (N*Pi)
	
	CAAA=1.d0/dcosh(AAA)
	CBBB=1.d0/dcosh(BBB)
	
	PaddleDAMPT=DDD*(-(CAAA*CAAA)*(1 + dtanh(BBB)) 
     &	             + (CBBB*CBBB)*(1 - dtanh(AAA)))
	return
	end

	function PaddleDAMPTT(omega,t,coeft2,N,Pi)
	IMPLICIT REAL*8 (A-H,O-Z)
	
	AAA= (4*(-2*N*Pi + omega*t))/(N *Pi)
	BBB= (4*omega*t)/(N*Pi)
	EEE= (32.d0*omega*omega) / (N*N*Pi*Pi)
	SBBB2=1.d0/(dcosh(BBB)*dcosh(BBB))
	SAAA2=1.d0/(dcosh(AAA)*dcosh(AAA))
	TAAA=dtanh(AAA)
	TBBB=dtanh(BBB)
	
	PaddleDAMPTT=EEE*(-SBBB2*SAAA2-SBBB2*TBBB*(1.d0-TAAA)
     &	              +SAAA2*(1.d0+TBBB)*TAAA)
	return
	end

C----------------------------------------------------------------------
C----------------------------------------------------------------------
c	SINE Paddle motion     
c	
c	PaddleOS    oscillatory part of the paddle motion 
c	PaddleOST   first time derivative of the oscillatory part
c	PaddleOSTT  second time derivative of the oscillatory part
c	PaddleDAMP    damping part of the paddle motion 
c	PaddleDAMPT   first time derivative of the damping part
c	PaddleDAMPTT  second time derivative of the damping part
C----------------------------------------------------------------------
C----------------------------------------------------------------------
	function PaddleOSSIN(omega,t,coeft2,N,Pi)
	IMPLICIT REAL*8 (A-H,O-Z)

	phase=0.5d0 * 3.1415926535898D0
	CCC= omega *t + phase
	PaddleOSSIN=dsin(CCC)
	return
	end

	function PaddleOSTSIN(omega,t,coeft2,N,Pi)
	IMPLICIT REAL*8 (A-H,O-Z)

	phase=0.5d0 * 3.1415926535898D0
	CCC= omega *t + phase
	PaddleOSTSIN=omega*dcos(CCC)
	return
	end

	function PaddleOSTTSIN(omega,t,coeft2,N,Pi)
	IMPLICIT REAL*8 (A-H,O-Z)

	phase=0.5d0 * 3.1415926535898D0
	CCC= omega *t + phase
	PaddleOSTTSIN=-omega*omega*dsin(CCC)
	return
	end

	function PaddleDAMPSIN(omega,t,coeft2,N,Pi)
	IMPLICIT REAL*8 (A-H,O-Z)
	
	BBB= (4*omega*t)/(N*Pi)
	PaddleDAMPSIN=dtanh(BBB)
	return
	end

	function PaddleDAMPTSIN(omega,t,coeft2,N,Pi)
	IMPLICIT REAL*8 (A-H,O-Z)
	
	BBB= (4*omega*t)/(N*Pi)
	DDD= (4.d0*omega) / (N*Pi)
	
	CBBB=1.d0/dcosh(BBB)
	
	PaddleDAMPTSIN=DDD*(CBBB*CBBB)
	return
	end

	function PaddleDAMPTTSIN(omega,t,coeft2,N,Pi)
	IMPLICIT REAL*8 (A-H,O-Z)
	
	BBB= (4*omega*t)/(N*Pi)
	DDD= (4.d0*omega) / (N*Pi)
	EEE= DDD*DDD
	SBBB2=1.d0/(dcosh(BBB)*dcosh(BBB))
	TBBB=dtanh(BBB)
	
	PaddleDAMPTTSIN=-2.d0*EEE*(-SBBB2*TBBB)

	return
	end

C----------------------------------------------------------------------
C----------------------------------------------------------------------
c	SINE type 2 Paddle motion     
c	
c	PaddleOS    oscillatory part of the paddle motion 
c	PaddleOST   first time derivative of the oscillatory part
c	PaddleOSTT  second time derivative of the oscillatory part
c	PaddleDAMP    damping part of the paddle motion 
c	PaddleDAMPT   first time derivative of the damping part
c	PaddleDAMPTT  second time derivative of the damping part
C----------------------------------------------------------------------
C----------------------------------------------------------------------
	function PaddleOSSIN_type2(omega1,omega2,t,A1,A2,Phase1,Phase2)
	IMPLICIT REAL*8 (A-H,O-Z)

	CCC= omega1 *t + Phase1
	CCC2=omega2*t+Phase2
	PaddleOSSIN_type2=A1*dcos(CCC)+A2*dcos(CCC2)
	return
	end

	function PaddleOSTSIN_type2(omega1,omega2,t,A1,A2,Phase1,Phase2)
	IMPLICIT REAL*8 (A-H,O-Z)

	CCC= omega1 *t + Phase1
	CCC2=omega2*t+Phase2
	PaddleOSTSIN_type2=-omega1*A1*dsin(CCC)-A2*omega2*dsin(CCC2)
	return
	end

	function PaddleOSTTSIN_type2(omega1,omega2,t,A1,A2,Phase1,Phase2)
	IMPLICIT REAL*8 (A-H,O-Z)

	CCC= omega1 *t + Phase1
	CCC2=omega2*t+Phase2
	PaddleOSTTSIN_type2=-omega1*omega1*A1*dcos(CCC)
     &    -omega2*omega2*A2*dcos(CCC2)
	return
	end

	function PaddleDAMPSIN_type2(t,Threshold)
	IMPLICIT REAL*8 (A-H,O-Z)
	
	if (t .lt. 0.d0) then
	  PaddleDAMPSIN_type2=0.d0
	else if ((t .ge. 0.d0) .and. (t .lt. Threshold)) then
	   PaddleDAMPSIN_type2=t/Threshold
	else if (t .ge. Threshold)  then 
	PaddleDAMPSIN_type2=1.d0
	endif
	return
	end

	function PaddleDAMPTSIN_type2(t,Threshold)
	IMPLICIT REAL*8 (A-H,O-Z)
	
	PaddleDAMPTSIN_type2=0.d0
	if ((t .ge. 0.d0) .and. (t .lt. Threshold)) then
	   PaddleDAMPTSIN_type2=1.d0/Threshold
	endif
	return
	end

	function PaddleDAMPTTSIN_type2(t,Threshold)
	IMPLICIT REAL*8 (A-H,O-Z)
	
	PaddleDAMPTTSIN_type2=0.d0

	return
	end
       
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C----------------------------------------------------------------------
      SUBROUTINE PADSIN_MONO(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C----------------------------------------------------------------------
C0  PADSINF   PADSIN of the WRL
C1  PURPOSE   Simulation of a paddle wavemaker oscillating on the
C1            lateral boundary K(2,4). Both geometry and B.C. are updat.
C1            or only the B.C. (IFLAGV=1,2). The paddle motion is damped
C1            according to D(t,Tdamp) (1 if Tdamp <eps). The motion is :
C1            for z=0 :
C1
C1            Xp(t)=-0.25 Ap (tanh((4wpt)/N Pi)+1)(1-tanh(4(wpt-2 Pi N)/N Pi)) sin (wp(t-0.018t**2))
C1            Up(t)=d Xp(t)/dt, d Up(t)/dt= d2Xp(t)/dt2
C1            D(t) =
C1            
C1	There is no damping function as everythings is included in the description of Xp
C1
C1            Xf the motion, up,upt the velocity and acceleration (z=0)
C1
C2  CALL PADSIN(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C3  CALL ARG. XYZNOD()            = Coordin. of nofes for current step
C3            PHIS(NOM),PHISS     = Phis and Phiss imposed on boundaries
C3            PHIM(NOM),PHIMM     = Phim and Phimm imposed on boundaries
C3            PHINS(NOM),PHINM    = Phins and Phinm imposed on boundaries
C3            QBC(NOM)            = du/dn imposed on Neuman boundaries
C3            K                   = Considered later. boundary (2,or 4)
C3            TIME                = Current time
C3            TUNE                = Tune the overall motion >0
C3            DE(2)               = Depth at K=2,4
C3            TDAMP=2*T0          = 1% Damping time (0: no,>0: yes)
C3            IFLAGV              = (1,2) update geom.+B.C. or B.C. only
C3            IPTYP               = 1 sine or sume of sine waves
C3                                = 8 wave focusing at (0,DF)
C3
C4  RET. ARG. XYZNOD(NOM,3),QBC(NOM)  on K
C9  Feb. 99   S. GRILLI, INLN
CLPADSIN SUB. WHICH SIMULATE MOTION AND KINEMATICS OF PADDLE WAVEMAKER
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM0(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM3(66),INODF(6,2),IBCOND(6)
c      COMMON/SYSTEM/  DUM3(24),TIME,TDAMP,TDOWN,DUM31(4+8*MYMAX),IDU(2),
c     .                IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOUNDC/ DUM4(9),DE(2),OMEGA,TUNE,DUM6,IDW
      COMMON /PADDLE/ AP(NFMAX,NHMAX),OP(NFMAX),SP(NHMAX),ZKWAVE(NFMAX),
     .                DISTF,TFOC,ALPHAF,WMIDTH,OS(MYMAX),OST(MYMAX),
     .                OSTT(MYMAX),DAMP,DAMPT,DAMPTT,DELFR,NFR,NH,IFREQ
C
      DIMENSION QBC(NOM),PHIS(NOM),PHIM(NOM),PHISS(NOM),PHIMM(NOM),
     .          PHINS(NOM),PHINM(NOM)
C
      DATA ONE/1.D0/,TWO/2.D0/,ZERO/0.D0/,EPS/1.D-08/,
     .     HALF/0.5D0/,CD/2.3025D0/,FOUR/4.D0/,PI/3.1415926535898D0/
C
       SAVE
C     SAVE /MAILLE/,/SYSTEM/,DATGEN/,/BOUNDC/,/PADDLE/
C
c#############################################################################
c	inputs: potential en curvilinear coordinates (s,n,m)
c	outputs: UBC=velocity, QBC=du/dn
c
c	DEC: local depth a the boundary K
c	IB: first point of the boundary K in the node table
c

      DEC = DE(K/2)
      IST = 3 - K
      IB  = INODF(K,1)
C
C.....Store parameters at beginning of time loop
C
      IF((IFLAGV.EQ.1).OR.(ILOOP.EQ.1)) THEN

C 
C
C.....Store parameters at beginning of time loop
C
	  Coeft2=0.d0
!          OPTI = OP(IFR)
!	  OPTI = 8.2d0
	  Tdec=0.d0
	  NNN= 10
c------------------------------------------------
C        WRL wave basin damping (tanh )
c------------------------------------------------
            DAMP   = PaddleDAMPSIN(OPTI,TIME+Tdec,coeft2,NNN,PI)
            DAMPT  = PaddleDAMPTSIN(OPTI,TIME+Tdec,coeft2,NNN,PI)
            DAMPTT = PaddleDAMPTTSIN(OPTI,TIME+Tdec,coeft2,NNN,PI)


C
C     ...Current motion at z=0 without damping
C
c--------------------------------------------------------
c	debug
113     FORMAT(5(G21.14,1X))
         DO IFR=1,NFR
          OPTI = OP(IFR)
	  enddo

c	print*,'dans padsinWRL, AMPL, omega,time, time+Tdec, coeft2'
c	write(*,113) AMPL,OPTI,TIME,TIME+Tdec,coeft2  
c--------------------------------------------------------
         DO I=1,MY+1
               OS(I)   = 0.d0
               OST(I)  = 0.d0
               OSTT(I) = 0.d0
         END DO


C(omega,t,coeft2,N,Pi)
C------------------------------------------------------------
c	Construction of the paddle motion
c	Double loops :
c			- first on frequencies: IFR fom 1 to NFR
c			- then on the directions NH
c

         DO IFR=1,NFR
c-------------------------------------------------------------
c	Construction of the 'carrier' frequency
c	opti=wp((t-tfoc)-coeft2 * 0.5 * t**2)
c	 
c          OPTI = OP(IFR)*(TIME-TFOC-0.018/TWO*TIME*TIME)
c          OPTI = OP(IFR)*(TIME-TFOC-Coeft2/TWO*TIME*TIME)
          OPTI = OP(IFR)
	  
	  
	  IF(NFR.EQ.NH.AND.IFREQ.EQ.1) THEN
	    NH2 = 1
	    IH = IFR
	  ELSE
	    NH2=NH
	  END IF
          DO IH=1,NH2

c-------------------------------------------------------------
c	Amplitude
c
c            AMPL = AP(IFR,IH)*TUNE
            AMPL = AP(IFR,IH)
c            AMPL = -0.025d0*0.0005d0
		

c-------------------------------------------------------------
c	phase 2D
c
	    PHASE=0.d0!HALF*PI
            CO   = DCOS(PHASE + OPTI)
            SI   = DSIN(PHASE + OPTI)
c-------------------------------------------------------------
c	relative phase 2D  when more than one wavemaker
	    THETAN = ZERO
C
C           y of free surface is used on wm. for vertical snake lines
C
c-----------------------------------------------------------------
c	points of both the free surface and the wave maker

            DO I=1,MY+1
c	Ns= indice of the free surface point, IB=INODF(K,1)
	       NS  = IB + MZ*(MY+1) + I - 1
C
	       IF((I.GT.1).AND.(I.LE.MY)) THEN
c	If the points are on the boundaries (I=1 or I=MY+1): the corners
	          YP = XYZNOD(LXYZ(NS,1),2)
	       ELSE
c	else inner points
	          YP = XYZNOD(NS,2)
	       END IF
	       ZKY = ZKWAVE(IFR)*DSIN(THETAN)
c	       CY  = DCOS(ZKY*YP)
c	       SY  = DSIN(ZKY*YP)
C
c               OS(I)   = OS(I)   + AMPL*(CY*CO + SY*SI)
c               OST(I)  = OST(I)  + AMPL*OP(IFR)*(-1.0*CY*SI + 
c     1              SY*CO)*(ONE - 0.018*TIME)
c               OSTT(I) = OSTT(I) - AMPL*OP(IFR)*OP(IFR)*(CY*CO + SY*SI)
c     1          *(ONE - 0.018*TIME) + (-CY*SI + SY*CO)*(-0.018)
c		print*,'WRL PADDLE',AMPL,OPTI,TIME,coeft2,PI
               OS(I)   = AMPL*PaddleOSSIN(OPTI,TIME+Tdec,coeft2,NNN,PI)
               OST(I)  = AMPL*PaddleOSTSIN(OPTI,TIME+Tdec,coeft2,NNN,PI)
               OSTT(I) = AMPL
     &      	       *PaddleOSTTSIN(OPTI,TIME+Tdec,coeft2,NNN,PI)
	    END DO 
          END DO
         END DO
      END IF
C
C.....Updating boundary conditions and/or geometry (IFLAG=1,2)
C     Assumed to be on boundary K=2 or 4
C
	
	
      IF(IFLAGV.EQ.1) THEN
         DO I=1,MY+1
c	node at the bottom	 
	    NB  = IB + I - 1
c	node at the free surface	 
	    NS  = NB + MZ*(MY+1)
C
C           Current motion at z=0
C
c	XP paddle position = oscillation*damp
            XP  = OS(I)*DAMP
c	UP paddle velocity 
            UP  = OST(I)*DAMP + OS(I)*DAMPT
            PL  = DEC*DEC + XP*XP
c	DPL distance between (XP,y,0) and the axis of rotation.
            DPL = DSQRT(PL)
c	TT D omega/Dt
            TT  =-DEC*UP/PL
C
C           Node coord. updating : x(0)=XP; z-free surface is forced
C           on top; and y-free surface on top and bottom of flap wm.
C
	    IF((I.GT.1).AND.(I.LE.MY)) THEN
	       YP = XYZNOD(LXYZ(NS,1),2)
	       XYZNOD(NB,2) = YP
	       XYZNOD(NS,2) = YP
	    ELSE
	       YP = XYZNOD(NS,2)
               XYZNOD(LXYZ(NS,1),2) = YP
            END IF
c------------------------------------------------------------------------------------------
c	for external lines, force the points to be on the boundaries
c	inner points, update paddle position point at the free surface(knowing it is linear) 
c		and passing thru the hinge at the bottom and Xp. so Xs(free surface)=XP(paddle)*(1+eta/Dec)	    
	    ZFS = XYZNOD(LXYZ(NS,1),3)
	    XYZNOD(NS,3) = ZFS
	    XYZNOD(NS,1) = XP*(ONE + ZFS/DEC)
c------------------------------------------------------------------------------------------
C
	    DO J=1,MZ+1
	       IND = IB + (J-1)*(MY+1) + I - 1
	       DO KK=1,3
                  XYZNOD(IND,KK) = XYZNOD(NB,KK) + (J-1)*
     .                           (XYZNOD(NS,KK) - XYZNOD(NB,KK))/MZ
               END DO
C 	IST direction of the normal
c	RG= YP
	       RG       = (ONE + XYZNOD(IND,3)/DEC)*DPL
               QBC(IND) = QBC(IND) + RG*TT*IST
	    END DO
C
C           x-surf. = x flap
C
	    XYZNOD(LXYZ(NS,1),1) = XYZNOD(NS,1)
         END DO
      ELSE
         DO I=1,MY+1
	    NB  = IB + I - 1
C
C           Current motion at z=0
C
c	XP paddle position = oscillation*damp
            XP  = OS(I)*DAMP
c	UP paddle velocity 
            UP  = OST(I)*DAMP + OS(I)*DAMPT
            UPT = OSTT(I)*DAMP + TWO*OST(I)*DAMPT + OS(I)*DAMPTT
c	DPL distance between (XP,y,0) and the axis of rotation.
            PL  = DEC*DEC + XP*XP
            DPL = DSQRT(PL)
C
            TT  =  -DEC*UP/PL
C ajout Up je pense qu'il y avait une erreur
            TTT = -(DEC*UPT + TWO*UP*XP*TT)/PL
C old line      TTT = -(DEC*UPT + TWO*XP*TT)/PL
	    WRITE(97,112) TIME,I,XP,UP,UPT,TT,TTT
C
	    DO J=1,MZ+1
	       IND      = IB + (J-1)*(MY+1) + I - 1
	       RG       = (ONE + XYZNOD(IND,3)/DEC)*DPL
C 	IST direction of the normal
c	RG= YP
c               QBC(IND) = RG*IST*(TTT + TT*(PHISS(IND) + PHIMM(IND)))
c     .                  -(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
c----------------------------
c	Old line, may be an error on second line, no normal vector on last component
c----------------------------------------
               QBC(IND) = RG*IST*(TTT + TT*(PHISS(IND) + PHIMM(IND)))
     .                  -IST*(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
               write(142,111) TIME,I,J,RG, XP, UP, UPT,TT,TTT,QBC(IND)
           END DO
         END DO
      END IF
111    FORMAT((G21.14,1X),2(i3,1X),7(G21.14,1X))
112    FORMAT(G21.14,1X,i5,1X,5(G21.14,1X))
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE PADSIN_WRL_type2
     &      (K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C----------------------------------------------------------------------
C0  PADSINF   PADSIN of the WRL type2
C1  PURPOSE   Simulation of a paddle wavemaker oscillating on the
C1            lateral boundary K(2,4). Both geometry and B.C. are updat.
C1            or only the B.C. (IFLAGV=1,2). The paddle motion is damped
C1            according to D(t,Tdamp) (1 if Tdamp <eps). The motion is :
C1            for z=0 :
C1
C1            Xp(t)=A0 (cos(k0t)+eps cos((N+1/N)k0t-Phase)
C1            Up(t)=d Xp(t)/dt, d Up(t)/dt= d2Xp(t)/dt2
C1            D(t) =
C1            
C1	There is no damping function as everythings is included in the description of Xp
C1
C1            Xf the motion, up,upt the velocity and acceleration (z=0)
C1
C2  CALL PADSIN(K,QBC,PHIS,PHISS,PHIM,PHIMM,PHINS,PHINM)
C3  CALL ARG. XYZNOD()            = Coordin. of nofes for current step
C3            PHIS(NOM),PHISS     = Phis and Phiss imposed on boundaries
C3            PHIM(NOM),PHIMM     = Phim and Phimm imposed on boundaries
C3            PHINS(NOM),PHINM    = Phins and Phinm imposed on boundaries
C3            QBC(NOM)            = du/dn imposed on Neuman boundaries
C3            K                   = Considered later. boundary (2,or 4)
C3            TIME                = Current time
C3            TUNE                = Tune the overall motion >0
C3            DE(2)               = Depth at K=2,4
C3            TDAMP=2*T0          = 1% Damping time (0: no,>0: yes)
C3            IFLAGV              = (1,2) update geom.+B.C. or B.C. only
C3            IPTYP               = 1 sine or sume of sine waves
C3                                = 8 wave focusing at (0,DF)
C3
C4  RET. ARG. XYZNOD(NOM,3),QBC(NOM)  on K
C9  Feb. 99   S. GRILLI, INLN
CLPADSIN SUB. WHICH SIMULATE MOTION AND KINEMATICS OF PADDLE WAVEMAKER
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON/DATGEN/  DUM0(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      include 'maille.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c      COMMON/MAILLE/  XYZNOD(NOMM,3),DUM2(2*MOMM+18),
c     .                IDUM2(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
c     .                IDUM3(66),INODF(6,2),IBCOND(6)
c      COMMON/SYSTEM/  DUM3(24),TIME,TDAMP,TDOWN,DUM31(4+8*MYMAX),IDU(2),
c     .                IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /BOUNDC/ DUM4(9),DE(2),OMEGA,TUNE,DUM6,IDW
c      COMMON /PADDLE/ AP(NFMAX,NHMAX),OP(NFMAX),SP(NHMAX),ZKWAVE(NFMAX),
c     .                DISTF,TFOC,ALPHAF,WMIDTH,OS(MYMAX),OST(MYMAX),
c     .                OSTT(MYMAX),DAMP,DAMPT,DAMPTT,DELFR,NFR,NH,IFREQ
C
      DIMENSION QBC(NOM),PHIS(NOM),PHIM(NOM),PHISS(NOM),PHIMM(NOM),
     .          PHINS(NOM),PHINM(NOM)
C
C
C       SAVE
      include 'paddle2.h'
      include 'paddle.h'
      logical Converge,CurvedPaddle
      Integer NN

      DATA ONE/1.D0/,TWO/2.D0/,ZERO/0.D0/,EPS/1.D-08/,
     .     HALF/0.5D0/,CD/2.3025D0/,FOUR/4.D0/,PI/3.1415926535898D0/
C     SAVE /MAILLE/,/SYSTEM/,DATGEN/,/BOUNDC/,/PADDLE/
C
c#############################################################################
c	inputs: potential en curvilinear coordinates (s,n,m)
c	outputs: UBC=velocity, QBC=du/dn
c
c	DEC: local depth a the boundary K
c	IB: first point of the boundary K in the node table
c       NN number of wave in the paddle motion

       
      DEC = DE(K/2)
      IST = 3 - K
      IB  = INODF(K,1)

      Converge=.false.
      CurvedPaddle=.false.
      Tdec=-0.1d0
      Threshold=15.d0
      Phase2=-PI/18.d0
      
      if (Iconv.eq.1) Converge=.true.
      if (Icurved.eq.1) CurvedPaddle=.true.

      IF((IFLAGV.EQ.1).OR.(ILOOP.EQ.1)) THEN


113     FORMAT(5(G21.14,1X))


c         DO IFR=1,NFR
c-------------------------------------------------------------
c	Construction of the 'carrier' frequency
	  omega1=OP(1)
	  omega2=OP(2)
	  
	  ZKK1=0.d0
	  ZKK2=0.d0
	  if (Converge) call CELES(omega1,CEL,ZKK1)
	  if (Converge) call CELES(omega2,CEL,ZKK2)

c------------------------------------------------
C        WRL wave basin damping 
c------------------------------------------------
            DAMP   = 
     &    PaddleDAMPSIN_type2(TIME+Tdec,Threshold)
            DAMPT  = 
     &    PaddleDAMPTSIN_type2(TIME+Tdec,Threshold)
            DAMPTT = 
     &    PaddleDAMPTTSIN_type2(TIME+Tdec,Threshold)
c--------------------------------------------------------
         DO I=1,MY+1
               OS(I)   = 0.d0
               OST(I)  = 0.d0
               OSTT(I) = 0.d0
         END DO
	  
c          DO IH=1,NH2

c-------------------------------------------------------------
c	Amplitude
c
	    A1=AP(1,1)
	    A2=AP(2,1)
c-------------------------------------------------------------
c	phase 2D
c
	    PHASE=0.d0!HALF*PI
c-------------------------------------------------------------
c	relative phase 2D  when more than one wavemaker
	    THETAN = ZERO
	    PPhase = 0.d0
C
C           y of free surface is used on wm. for vertical snake lines
C
c-----------------------------------------------------------------
c	points of both the free surface and the wave maker

            DO I=1,MY+1
c	Ns= indice of the free surface point, IB=INODF(K,1)
	       NS  = IB + MZ*(MY+1) + I - 1
C
	       IF((I.GT.1).AND.(I.LE.MY)) THEN
c	If the points are on the boundaries (I=1 or I=MY+1): the corners
	          YP = XYZNOD(LXYZ(NS,1),2)
	       ELSE
c	else inner points
	          YP = XYZNOD(NS,2)
	       END IF
c	       ZKY = ZKWAVE(IFR)*DSIN(THETAN)
c	       CY  = DCOS(ZKY*YP)
c	       SY  = DSIN(ZKY*YP)

               PPhase1=0.d0
               PPhase2=0.d0
	       if (Converge) then
	         THETAN=datan2(YP-Yconv,Xconv)
		 dcosTHETAN=dcos(THETAN)
		 dsinTHETAN=dsin(THETAN)
		 PPhase1=ZKK1*YP*dsinTHETAN+ZKK1*(Xconv*dcosTHETAN+
     &	                Yconv*dsinTHETAN)
		 PPhase2=ZKK2*YP*dsinTHETAN+ZKK2*(Xconv*dcosTHETAN+
     &	                Yconv*dsinTHETAN)
               endif
              OS(I)   = 
     &     PaddleOSSIN_type2(
     &   omega1,omega2,TIME+Tdec,A1,A2,PPhase1,PPhase2+Phase2)
              OST(I)  = 
     &     PaddleOSTSIN_type2(
     &   omega1,omega2,TIME+Tdec,A1,A2,PPhase1,PPhase2+Phase2)
              OSTT(I) = 
     &     PaddleOSTTSIN_type2(
     &   omega1,omega2,TIME+Tdec,A1,A2,PPhase1,PPhase2+Phase2)
2001  format(i4,1x,9(G21.14,1X))     
2003  format(a2,1x,3(G21.14,1X),i3,1x,G21.14)     

	    END DO 
c          END DO
c         END DO
      END IF
C
C.....Updating boundary conditions and/or geometry (IFLAG=1,2)
C     Assumed to be on boundary K=2 or 4
C
	
	
      IF(IFLAGV.EQ.1) THEN
         DO I=1,MY+1
c	node at the bottom	 
	    NB  = IB + I - 1
c	node at the free surface	 
	    NS  = NB + MZ*(MY+1)
C
C           Current motion at z=0
C
c	XP paddle position = oscillation*damp
            XP  = OS(I)*DAMP
c	UP paddle velocity 
            UP  = OST(I)*DAMP + OS(I)*DAMPT
C
C           Node coord. updating : x(0)=XP; z-free surface is forced
C           on top; and y-free surface on top and bottom of flap wm.
C
	    IF((I.GT.1).AND.(I.LE.MY)) THEN
	       YP = XYZNOD(LXYZ(NS,1),2)
	       XYZNOD(NB,2) = YP
	       XYZNOD(NS,2) = YP
	    ELSE
	       YP = XYZNOD(NS,2)
               XYZNOD(LXYZ(NS,1),2) = YP
            END IF
c------------------------------------------------------------------------------------------
c	for external lines, force the points to be on the boundaries
c	inner points, update paddle position point at the free surface(knowing it is linear) 
c		and passing thru the hinge at the bottom and Xp. so Xs(free surface)=XP(paddle)*(1+eta/Dec)	    
	    ZFS = XYZNOD(LXYZ(NS,1),3)
	    XYZNOD(NS,3) = ZFS
	    XYZNOD(NS,1) = XP*(ONE + ZFS/DEC)

c if the paddle is curved, i know the equation implying the system is 
c    connected at z=0:
c    z<0 x= XP/(2*DEC**2)*((z+DEC)**3-3*DEC*((z+DEC)**2)
c    z>0 x= XP/(2*DEC**2)*(DEC**3-3*DEC**2*((z+DEC))
	    if (CurvedPaddle) then
	      if (ZFS.gt. 0.d0) then
	        XYZNOD(NS,1) = XP/(2.d0*(DEC**2.d0))*
     &    (DEC**3.d0 - 3.d0*DEC*(ZFS+DEC)**2.d0)
	      else
	        XYZNOD(NS,1) = XP/(2.d0*(DEC**2.d0))*
     &    ((ZFS+DEC)**3.d0 - 3.d0*(ZFS+DEC)*DEC**2.d0)
	      endif
	    endif
	      
c------------------------------------------------------------------------------------------
C
	    DO J=1,MZ+1
	       IND = IB + (J-1)*(MY+1) + I - 1

	       if (CurvedPaddle) then
                DO KK=2,3
                  XYZNOD(IND,KK) = XYZNOD(NB,KK) + (J-1)*
     .                           (XYZNOD(NS,KK) - XYZNOD(NB,KK))/MZ
                 END DO
		 ZZZ=XYZNOD(IND,3)
	         if (ZZZ.gt. 0.d0) then
	           ZZZ=XYZNOD(IND,3)+DEC
	           XXX = -XP/(2.d0*DEC**3.d0)*(DEC**3.d0 - 
     &              3.d0*ZZZ*DEC**2.d0)
                   XYZNOD(IND,1) = XXX 
		   
	           RG =dsqrt(ZZZ**2.d0 + XXX**2.d0)
                   dRGdt=1.d0/RG*(  XP/(2.d0*DEC**3.d0)*
     &    ((DEC**3.d0 - 3.d0*ZZZ*DEC**2.d0)**2.d0)*UP/(2.d0*DEC**3.d0))
                   omeg=-3.d0*XP/(2.d0*DEC)
                   DomegDt=-3.d0*UP/(2.d0*DEC)
                   TT=DomegDt
     
	         else 
	           ZZZ=XYZNOD(IND,3)+DEC
	           XXX = -XP/(2.d0*DEC**3.d0)*(ZZZ**3.d0 - 
     &	            3.d0*DEC*ZZZ**2.d0)
                   XYZNOD(IND,1) = XXX
	           if (J.eq.1) then
		     dRGdt=0.d0
		     omeg=0.d0
		     DomegDt=0.d0
		   else
	           RG =dsqrt(ZZZ**2.d0 + XXX**2.d0)
                   dRGdt=1.d0/RG*(  XP/(2.d0*DEC**3.d0)*
     &     ((ZZZ**3.d0 - 3.d0*DEC*ZZZ**2.d0)**2.d0)*UP/(2.d0*DEC**3.d0))
                   omeg=(3.d0*XP/(2.d0*DEC))*(ZZZ**2.d0-2.d0*DEC*ZZZ)
                   DomegDt=(3.d0*UP/(2.d0*DEC))*(ZZZ**2.d0-2.d0*DEC*ZZZ)
                   TT=DomegDt
		   endif
		     
	         endif
	       else !linear paddle
                 DO KK=1,3
                  XYZNOD(IND,KK) = XYZNOD(NB,KK) + (J-1)*
     .                           (XYZNOD(NS,KK) - XYZNOD(NB,KK))/MZ
                 END DO
c	DPL distance between (XP,y,0) and the axis of rotation.
                 PL  = DEC*DEC + XP*XP
                 DPL = DSQRT(PL)
c	TT D omega/Dt
                 TT  =-DEC*UP/PL
	         RG  = (ONE + XYZNOD(IND,3)/DEC)*DPL
c	       print*,'linear',RG
	       endif
C 	IST direction of the normal
c	RG= YP

c
c BOUNDARY CONDITION= D phi /Dn = R Dtheta/Dt Etheta	    

              QBC(IND) = QBC(IND) + RG*TT*IST
2002  format(a3,1x,i4,1x,3(G21.14,1X))     
	    END DO
C
C           x-surf. = x flap
C
	    XYZNOD(LXYZ(NS,1),1) = XYZNOD(NS,1)
         END DO
      ELSE

         DO I=1,MY+1
	    NB  = IB + I - 1
C
C           Current motion at z=0
C
c	XP paddle position = oscillation*damp
            XP  = OS(I)*DAMP
c	UP paddle velocity 
            UP  = OST(I)*DAMP + OS(I)*DAMPT
            UPT = OSTT(I)*DAMP + TWO*OST(I)*DAMPT + OS(I)*DAMPTT
C
	    DO J=1,MZ+1
	       IND      = IB + (J-1)*(MY+1) + I - 1
	       if (CurvedPaddle) then
		 ZZZ=XYZNOD(IND,3)
	         if (ZZZ.gt. 0.d0) then
	           ZZZ=XYZNOD(IND,3)+DEC
	           XXX = -XP/(2.d0*DEC**3.d0)*(DEC**3.d0 - 
     &              3.d0*ZZZ*DEC**2.d0)
		   
	           RG =dsqrt(ZZZ**2.d0 + XXX**2.d0)
                   dRGdt=1.d0/RG*(  XP/(2.d0*DEC**3.d0)*
     &     ((DEC**3.d0 - 3.d0*ZZZ*DEC**2.d0)**2.d0)*UP/(2.d0*DEC**3.d0))
                   omeg=-3.d0*XP/(2.d0*DEC)
                   DomegDt=-3.d0*UP/(2.d0*DEC)
                   D2omegDt2=-3.d0*UPT/(2.d0*DEC)
                   TT=DomegDt
		   TTT=D2omegDt2
!            print*,'7778'
	         else
!            print*,'8778'
	           ZZZ=XYZNOD(IND,3)+DEC
	           XXX = -XP/(2.d0*DEC**3.d0)*(ZZZ**3.d0 - 
     &	            3.d0*DEC*ZZZ**2.d0)
!            print*,'8777'
	           if (J.eq.1) then
		     dRGdt=0.d0
		     omeg=0.d0
		     DomegDt=0.d0
		     D2omegDt2=0.d0
		   else  
	           RG =dsqrt(ZZZ**2.d0 + XXX**2.d0)
                   dRGdt=1.d0/RG*(  XP/(2.d0*DEC**3.d0)*
     &     ((ZZZ**3.d0 - 3.d0*DEC*ZZZ**2.d0)**2.d0)*UP/(2.d0*DEC**3.d0))
                   omeg=(3.d0*XP/(2.d0*DEC))*(ZZZ**2.d0-2.d0*DEC*ZZZ)
                   DomegDt=(3.d0*UP/(2.d0*DEC))*(ZZZ**2.d0-2.d0*DEC*ZZZ)
                   D2omegDt2=(3.d0*UPT/(2.d0*DEC))*
     &        (ZZZ**2.d0-2.d0*DEC*ZZZ)
                   endif
                   TT=DomegDt
		   TTT=D2omegDt2
!            print*,'8888'
		 endif 
!       print*,'8888'
	       
	       
	       else !linear paddle
c	DPL distance between (XP,y,0) and the axis of rotation.
                   PL  = DEC*DEC + XP*XP
                   DPL = DSQRT(PL)
                   TT  =  -DEC*UP/PL
C ajout Up je pense qu'il y avait une erreur
                   TTT = -(DEC*UPT + TWO*UP*XP*TT)/PL
C old line      TTT = -(DEC*UPT + TWO*XP*TT)/PL


	       RG       = (ONE + XYZNOD(IND,3)/DEC)*DPL
	       endif
	       WRITE(97,112) TIME,I,XP,UP,UPT,TT,TTT
c	       RG       = (ONE + XYZNOD(IND,3)/DEC)*DPL
C 	IST direction of the normal
c	RG= YP

!	      if (j.eq.5)
!     &	      write(*,2000) 'PHI',i,RG,TT,TTT,PHISS(IND),PHIMM(IND),
!     & PHIS(IND),PHINS(IND),PHIM(IND),PHINM(IND)
               QBC(IND) = RG*IST*(TTT + TT*(PHISS(IND) + PHIMM(IND)))
     .                  -(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
c----------------------------
c	Old line, may be an error on second line, no normal vector on last component
c----------------------------------------
c               QBC(IND) = RG*IST*(TTT + TT*(PHISS(IND) + PHIMM(IND)))
c     .                  -IST*(PHIS(IND)*PHINS(IND)+PHIM(IND)*PHINM(IND))
!       print*,'9999'
!	      if (j.eq.5)
!     &	      write(*,*) 'QBS2',i,QBC(IND)

               write(142,111) TIME,I,J,RG, XP, UP, UPT,TT,TTT,QBC(IND)
           END DO
         END DO
      END IF
111    FORMAT((G21.14,1X),2(i3,1X),7(G21.14,1X))
112    FORMAT(G21.14,1X,i5,1X,5(G21.14,1X))
2000   FORMAT(a3,1x,i2,1x,9(G21.14,1X))
C
      RETURN
C
      END
