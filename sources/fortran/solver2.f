C
C                LINEAR ITERATIVE SYSTEM SOLVER WITH PMTA
C                FAST MULTIPOLE ALGORITHM
C
C--------------------------------------------------------------------
      SUBROUTINE SOLVER2(TB,B,NOM)
C--------------------------------------------------------------------
C0  SOLVERF   SOLVER2
C1  PURPOSE   Compute the solution Xj of linear system Aij * Xj = Bi
C1            by the GMRES algorithm (general full nonsym. matrix)
C1            combined with the Fast Multipole Algorithm
C1            with parameterized SSOR preconditioner
C2  CALL      CALL SOLVER2(TB,B,NOM)
C3  CALL ARG. TB(NOM)             = initial condition for iterations
C3            B(NOM)              = system load vector
C4  RET. ARG. B(NOM)
C9  Oct. 99   P. GUYENNE, INLN
C9            adapted by Christophe Fochesato, CMLA, 2003
CLSOLVER SUB. COMPUTES SOLUTION OF LINEAR SYSTEM FOR THE 3D-BEM
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'
C
      PARAMETER (NSTART=100)

      COMMON /DATGEN/ DUM1(5),IDUM1(9),MOM
c      COMMON /SYSTEM/ DUM(39),IO6,IFLAG,IDW(3)
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
C
C.....New common to share variables with PMTA
C       Q : charge vector
C       SOL : matrix/vector product
C       IBEMANLYSIS : flag for calling a new BEM analysis (when new geometry)
C       IRLRIGID : flag for Dirichlet or Neumann coefficients, CF03
C
      COMMON /RMPREC/ RM(NOMM),SPA(NOMM)
C
      DIMENSION TB(NOM),B(NOM)
      DIMENSION H(NSTART+1,NSTART)
C
      SAVE
C
c      ITER = 100
c      TOL  = 1.D-6
      ITER = 10000
      TOL  = 1.D-12
C
C     no more assembly matrix, but preconditioning matrix SPA, CF03
C
      CALL GMRESFMA(SPA,TB,B,H,NSTART,ITER,TOL,NOM,INFO)
C     -------------
C
      IF(INFO.NE.0) THEN
         WRITE(99,*) ' '
         WRITE(99,*) ' NON ZERO INFO IN GMRES = ', INFO
         WRITE(99,*) ' MAXITER = ', ITER
         WRITE(99,*) ' RESIDUAL = ', TOL
         WRITE(99,*) ' '
         WRITE(*,*) ' NON ZERO INFO IN GMRES = ', INFO
         WRITE(*,*) ' MAXITER = ', ITER
         WRITE(*,*) ' RESIDUAL = ', TOL
         CALL ERRORS('INFO/=0 ')
      ELSE
         WRITE(99,*) ' '
         WRITE(99,*) ' MAXITER = ', ITER
         WRITE(99,*) ' RESIDUAL = ', TOL
         WRITE(99,*) ' '
         WRITE(*,*) ' MAXITER = ', ITER
         WRITE(*,*) ' RESIDUAL = ', TOL
      END IF
C
      DO I=1,NOM
         B(I) = TB(I)
      END DO
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE GMRESFMA(SPAD,X,B,H,NREST,MAXITER,TOL,N,INFO)
C----------------------------------------------------------------------
C     ITERATIVE TEMPLATE ROUTINE --> GMRESFMA
C
C     GMRESFMA SOLVES THE UNSYMMETRIC LINEAR SYSTEM A X = B USING
C     THE GENERALIZED MINIMAL RESIDUAL METHOD WITH THE FAST MULTIPOLE
C     ALGORITHM
C
C     GMRES FOLLOWS THE ALGORITHM DESCRIBED ON P.20 OF THE SIAM
C     TEMPLATES BOOK
C
C     EACH MATRIX/VECTOR PRODUCT IS REPLACED BY A CALL TO PMTA
C     WITH SOME SPECIFIC TREATMENT FOR BEM
C
C     THE RETURN VALUE INFO INDICATES CONVERGENCE WITHIN MAXITER
C     ITERATIONS (INFO = 0) OR ... NO CONVERGENCE WITHIN (INPUT)
C     MAXITER ITERATIONS (INFO = 1)
C
C     NREST IS THE NUMBER OF ITERATIONS BEFORE RESTARTING
C
C     UPON SUCCESSFUL RETURN, OUTPUT ARGUMENTS HAVE THE FOLLOWING
C     VALUES
C
C     X       --> APPROXIMATE SOLUTION TO A X = B
C     MAXITER --> NUMBER OF ITERATIONS BEFORE TOLERANCE IS REACHED
C     TOL     --> RESIDUAL AFTER THE FINAL ITERATION
C
C     adapted for FMA + BEM by Christophe Fochesato, CMLA, 2003
C----------------------------------------------------------------------
C
C--------- REM:passer NOMM en parametre plutot que d'utiliser NSIZE
      IMPLICIT NONE
C
      INTEGER NREST,MAXITER,N,INFO
      REAL*8 X(*),B(*),TOL
C
C     Preconditioning matrix SPA
C

      REAL*8 SPAD(*),AX(N),H(NREST+1,*)
C
C bug correction Xavier
c      INTEGER I,II,J,JJ,K,NITER,NSIZE
c      PARAMETER (NITER=100,NSIZE=20000)
      INTEGER I,II,J,JJ,K,NITER
      PARAMETER (NITER=100)
      INCLUDE 'param.inc'
      REAL*8 ZERO,ONE,RESID,NORMB,BETA
      PARAMETER (ZERO=0.0,ONE=1.0)
      REAL*8 S(NITER+1),CS(NITER+1),SN(NITER+1)
      REAL*8 R(NSIZE),W(NSIZE),Z(NSIZE),V(NITER+1,NSIZE)
      REAL*8 NORM,DOT
C
      CALL SPARPREC(B,Z,SPAD,N)
C      PRECON(B,Z,SPA,N)
C     -----------
C
      NORMB = NORM(Z,N)
C
      DO I=1,N
         Z(I) = B(I)
      END DO
C
C
C     Subroutine which calls the Fast Multipole Algorithm
C
      CALL PMTAPRO(AX,X,N)
C     ------------
C
      DO I=1,N
         Z(I) = Z(I) - AX(I)
      END DO
C
      CALL SPARPREC(Z,R,SPAD,N)
C      PRECON(Z,R,SPA,N)
C     -----------
C
      BETA = NORM(R,N)
C
      IF(NORMB.EQ.ZERO) NORMB = ONE
C
      RESID = BETA/NORMB
      IF(RESID.LE.TOL) THEN
         TOL     = RESID
         MAXITER = 0
         INFO    = 0
         RETURN
      END IF
C
      J = 1
C
      DO WHILE(J.LE.MAXITER)
C
         DO I=1,N
            V(1,I) = R(I)*(ONE/BETA)
         END DO
         S(1) = BETA
         DO I=2,NREST+1
            S(I) = ZERO
         END DO
C
         I = 1
C
         DO WHILE(I.LE.NREST.AND.J.LE.MAXITER)
C
            DO II=1,N
               Z(II) = V(I,II)
            END DO
C
C
C           Subroutine which calls the Fast Multipole Algorithm
C
            CALL PMTAPRO(AX,Z,N)
C           ------------
C
            DO II=1,N
               Z(II) = AX(II)
            END DO
C
            CALL SPARPREC(Z,W,SPAD,N)
C                PRECON(Z,W,SPA,N)
C           -----------
C
            DO K=1,I
               H(K,I) = DOT(W,V,K,NREST+1,N)
               DO II=1,N
                  W(II) = W(II) - H(K,I)*V(K,II)
               END DO
            END DO
C
            H(I+1,I) = NORM(W,N)
C
            DO II=1,N
               V(I+1,II) = W(II)*(ONE/H(I+1,I))
            END DO
C
            DO K=1,I-1
               CALL APPLANROT(H(K,I),H(K+1,I),CS(K),SN(K))
C              --------------
            END DO
C
            CALL GENPLANROT(H(I,I),H(I+1,I),CS(I),SN(I))
C           ---------------
C
            CALL APPLANROT(H(I,I),H(I+1,I),CS(I),SN(I))
C           --------------
C
            CALL APPLANROT(S(I),S(I+1),CS(I),SN(I))
C           --------------
C
            RESID = ABS(S(I+1))/NORMB
            IF(RESID.LT.TOL) THEN
               CALL UPDATE(X,I,H,S,V,NREST+1,N)
C              -----------
C
               TOL     = RESID
               MAXITER = J
               INFO    = 0
               RETURN
            END IF
C
            I = I + 1
            J = J + 1
C
         END DO
C
         CALL UPDATE(X,NREST,H,S,V,NREST+1,N)
C        -----------
C
         DO II=1,N
            Z(II) = B(II)
         END DO
C
C
C        Subroutine which calls the Fast Multipole Algorithm
C
         CALL PMTAPRO(AX,X,N)
C        ------------
C
         DO II=1,N
            Z(II) = Z(II) - AX(II)
         END DO
C
         CALL SPARPREC(Z,R,SPAD,N)
C             PRECON(Z,R,SPA,N)
C        -----------
C
         BETA  = NORM(R,N)
         RESID = BETA/NORMB
C
         IF(RESID.LT.TOL) THEN
            TOL     = RESID
            MAXITER = J
            INFO    = 0
            RETURN
         END IF
C
      END DO
C
      TOL  = RESID
      INFO = 1
C
      RETURN
C
      END
C----------------------------------------------------------------------
      REAL*8 FUNCTION NORM(VEC,N)
C----------------------------------------------------------------------
      IMPLICIT NONE
C
      INTEGER N
      REAL*8 VEC(*)
      INTEGER I
      REAL*8 TEMP
C
      TEMP = 0.0
      DO I=1,N
         TEMP = TEMP + VEC(I)*VEC(I)
      END DO
C
      NORM = DSQRT(TEMP)
C
      RETURN
C
      END
C----------------------------------------------------------------------
      REAL*8 FUNCTION DOT(VEC,MAT,IND,M,N)
C----------------------------------------------------------------------
      IMPLICIT NONE
C
      INTEGER IND,M,N
      REAL*8 VEC(*),MAT(M,*)
      INTEGER I
      REAL*8 TEMP
C
      TEMP = 0.0
      DO I=1,N
         TEMP = TEMP + VEC(I)*MAT(IND,I)
      END DO
C
      DOT = TEMP
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE GENPLANROT(DX,DY,CS,SN)
C----------------------------------------------------------------------
      IMPLICIT NONE
C
      REAL*8 DX,DY,CS,SN
      REAL*8 ZERO,ONE,TEMP
      PARAMETER (ZERO=0.0,ONE=1.0)
C
      TEMP = ZERO
C
      IF(DY.EQ.ZERO) THEN
         CS = ONE
         SN = ZERO
      ELSEIF(ABS(DY).GT.ABS(DX)) THEN
         TEMP = DX/DY
         SN   = ONE/DSQRT(ONE + TEMP*TEMP)
         CS   = TEMP*SN
      ELSE
         TEMP = DY/DX
         CS   = ONE/DSQRT(ONE + TEMP*TEMP)
         SN   = TEMP*CS
      ENDIF
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE APPLANROT(DX,DY,CS,SN)
C----------------------------------------------------------------------
      IMPLICIT NONE
C
      REAL*8 DX,DY,CS,SN
      REAL*8 TEMP
C
      TEMP = CS*DX + SN*DY
      DY   = CS*DY - SN*DX
      DX   = TEMP
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE UPDATE(VEC_X,K,MAT_H,VEC_S,MAT_V,M,N)
C----------------------------------------------------------------------
      IMPLICIT NONE
C
      INTEGER K,M,N
      REAL*8 VEC_X(*),VEC_S(*)
      REAL*8 MAT_H(M,*),MAT_V(M,*)
      INTEGER I,J,NP
      PARAMETER (NP=200)
      REAL*8 VEC_Y(NP)
C
      DO I=1,K
         VEC_Y(I) = VEC_S(I)
      END DO
C
      DO I=K,1,-1
         VEC_Y(I) = VEC_Y(I)/MAT_H(I,I)
         DO J=I-1,1,-1
            VEC_Y(J) = VEC_Y(J) - VEC_Y(I)*MAT_H(J,I)
         END DO
      END DO
C
      DO I=1,N
         DO J=1,K
            VEC_X(I) = VEC_X(I) + VEC_Y(J)*MAT_V(J,I)
         END DO
      END DO
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE PRECON(VEC_X,VEC_Y,MAT,N)
C----------------------------------------------------------------------
C
      IMPLICIT NONE
C
      INTEGER N
      REAL*8 VEC_X(*),VEC_Y(*),MAT(N,*)
      INTEGER I,J
      REAL*8 EPS,RELAX,TEMP
      PARAMETER (EPS=1.E-10,RELAX=0.6)
C
C     classical SSOR preconditioning with parameter RELAX
C
      TEMP = 2.0 - RELAX
C
      DO I=1,N
         VEC_Y(I) = TEMP*VEC_X(I)
      END DO
C
      TEMP = 1.0/RELAX
C
      DO I=1,N
         IF(ABS(MAT(I,I)).LE.EPS) THEN
            WRITE(99,*) ' SINGULAR PRECON '
            STOP
         ELSE
            VEC_Y(I) = RELAX*VEC_Y(I)/MAT(I,I)
            DO J=I+1,N
               VEC_Y(J) = VEC_Y(J) - VEC_Y(I)*MAT(J,I)
            END DO
         END IF
      END DO
C
      DO I=1,N
         VEC_Y(I) = TEMP*VEC_Y(I)*MAT(I,I)
      END DO
C
      DO I=N,1,-1
         IF(ABS(MAT(I,I)).LE.EPS) THEN
            WRITE(99,*) ' SINGULAR PRECON '
            STOP
         ELSE
            VEC_Y(I) = RELAX*VEC_Y(I)/MAT(I,I)
            DO J=I-1,1,-1
               VEC_Y(J) = VEC_Y(J) - VEC_Y(I)*MAT(J,I)
            END DO
         END IF
      END DO
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE SPARPREC(VEC_X,VEC_Y,MATD,N)
C----------------------------------------------------------------------
C
      IMPLICIT NONE
C
      INTEGER N
      REAL*8 VEC_X(*),VEC_Y(*)
      REAL*8 MATD(*)
      INTEGER I,J,K
      REAL*8 EPS,RELAX,TEMP
      PARAMETER (EPS=1.D-10,RELAX=0.6D0)
C
C     diagonal preconditioning
C
      DO I=1,N
	 IF (DABS(MATD(I)).GT.EPS) THEN
             VEC_Y(I)=VEC_X(I)/MATD(I)
	 ELSE
            WRITE(99,*) ' SINGULAR PRECON ',I,MATD(I)
            STOP
         END IF
      END DO
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE PMTAPRO(AX,X,N)
C----------------------------------------------------------------------
C
C     PMTAPRO calls the C subroutine CALLPMTA with the adequate options
C     for the matrix/vector product (for Boundary Integral Equation),
C     makes the correction on the result for the rigid mode technique,
C     and imposes double nodes compatibility
C
C     Christophe Fochesato, CMLA, 2003
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'


      COMMON /DATGEN/ DUM1(5),IDUM1(9),MOM
      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
      COMMON /CHARGE/ Q(NOMM),SOL(NOMM),IBEMANALYSIS,IRLRIGID
      COMMON /RMPREC/ RM(NOMM),SPA(NOMM)
      COMMON /FMAPAR/ CUBE,THETA,NLEVELS,IFFT,MP,IPFMA,IEPMTA,
     .                NPROC,IDIRECT,IBALANCED,IVERBOSE,IPMON,
     .                INICOULOMB,IFREECOULOMB
	DATA ZERO/0.D0/
      SAVE
C
      DIMENSION AX(N),X(N)
C
      DO I=1,N
	 Q(I) = X(I)
	 SOL(I)=ZERO
      END DO
C
      CALL CALLPMTA(CUBE,NLEVELS,IFFT,MP,IPFMA,IEPMTA,THETA,
C          --------
     .                NPROC,IDIRECT,IBALANCED,IVERBOSE,IPMON,
     .                N,MOM,INICOULOMB,IFREECOULOMB)
C
C.....Rigid mode technique adapted
C
      DO I=1,N
	 AX(I) = SOL(I)
      END DO
      DO I=1,N
         IF (INCOND(I).NE.1) THEN
	    AX(I) = AX(I) - Q(I)*RM(I)
	 END IF
      END DO
C
C.....Imposing double nodes compatibility to AX with ZMAX
C
      CALL DBNODAX(AX,Q)
C     ------------
C      write(*,*) 'produit with fma : '
C      do I=1,N
C           write(*,*) AX(I),I
C      end do
C
      RETURN
C
      END
