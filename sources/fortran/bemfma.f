C
C                      BEM ANALYSIS FOR MULTIPOLE COEFFICIENTS
C
C-----------------------------------------------------------------------
      SUBROUTINE BEMKFMA(ZQXIE,ZQYIE,ZUXIE,ZUYIE,NNIE,
     .                   OX,OY,OZ,NIE,IELTS,MP,ZMYCSTV)
C-----------------------------------------------------------------------
C0  BEMKF     BEMKFMA
C1  PURPOSE   Compute the complex multipole ZQ/UXIE+iZQ/UYIE, performs
C1            integrations through a BEM analysis
C1
C3  CALL ARG. XYZNOD(NOM)         = Coordinates of discretization nodes
C3            XICON(MOM,2)        = xo,eta for MII elements
C3            IGCON(MOM,4,4)      = MII elts connectivity
C3            ILCON(MOM,4)        = 4-node elt. connectivity
C3            IFACE(MOM)          = Face code for each element
C3            ISUB(MOMM)          = Max subdivisions for elts.
C3            INTG(MOMM)          = Nb. of integ. points per elt.
C3            LSUB(NOM)           = Subdivision status for element IE
C3            LXYZI(NOM,2)        = Addresses of double/triple nodes
C3            JSLIM(6)            = Record limits for saving in /SAVEF/
C3            NELEF(6),NNODF(6)   = Nbs. of elts/nodes for each face
C3            IELEF(6,2),INODF()  = Begin/ending node/elemt Nbs. for face
C3            IBCOND(6)           = Boundary condition type for each face
C3            NOM,MOM             = Discr. nb. of nodes and elements
C3            NOMM,MOMM           = Discr. max nb. of nodes and elts.
C3            NNODM               = Discr. max nb. of nodes per element
C3            ALMAX [0,PI/2]      = Limit angle for adapt. integration
C3            XYZLO1(NNODM,3)     = XYZ-coordinates of element IE MII nodes
C3            NODE1(NNODE)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = XYZ-coordinates of element IE 4 nodes
C3            NODE2(NNODE)        = Nodes of 4-node element IE
C3            NSUBM               = Max nb. of subdivisions for IE (0-8)
C3            NINTR               = Nb. of regular and singular int. pts
C3            ETARP()             = Regular integr. points
C3            WEIGTR()            = Regular integr. weights
C3            INCOND(NOMM)        = Boundary condition types for all nodes
C3            ZQXIE,ZQYIE,ZUXIE,ZUYIE(NNIE*(MP+1)*(MP+1))
C3                                = Multipoles
C3            NNIE                = Number of nodes related to the elements
C3                                  of the current cell
C3            INFACE(NOMM)        = Face types for all nodes
C3            OX,OY,OZ            = Center of current cell
C3            NIE                 = Nb of elts in the current cell
C3            IELTS(NIE)          = Elts numbers of the current cell
C3            MP                  = Nb of multipoles
C3            ZMYCSTV((MP+1)*(MP+1)) = Precomputed cst for multipoles
C4
C4  RET. ARG. ZQXIE,ZQYIE,ZUXIE,ZUYIE,JSLIM
C6
C6  INT.CALL  GAUSSP,BILFMA,BIDFMA,ERRORS
C6            SHAPFN,SHAPD1
CE  ERRORS    01= Insufficient length of saving buffer
C10 Jan. 99   S. Grilli at INLN
C10           adapted for FMA, C. Fochesato, CMLA, 2003
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
C
      CHARACTER*8 TEXTE

      INCLUDE 'param.inc'
C
      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),
     .                DUM3(18),IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),IDUM2(36),JSLIM(6),NELEF(6),
     .                NNODF(6),IELEF(6,2),INODF(6,2),IBCOND(6)
      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/  ETARP(NINTM),WEIGTR(NINTM),PHIP12(NINTM),
     .                PHIP23(NINTM),RMIP12(NINTM),RMIP23(NINTM),
     .                RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .                XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .                ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /FSCUB/ FN0XI(NDM,NINTM),FN0ET(NDM,NINTM),
     .               FN1XI(NDM,NINTM),FN1ET(NDM,NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),
     .               DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),DUM7(7*NINTM*NINTM)
      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
     .               FIS23(MNODM,NINTM,NINTM,MNOD),
     .               DFIX12(MNODM,NINTM,NINTM,MNOD),
     .               DFIX23(MNODM,NINTM,NINTM,MNOD),
     .               DFIE12(MNODM,NINTM,NINTM,MNOD),
     .               DFIE23(MNODM,NINTM,NINTM,MNOD)
c      COMMON /SYSTEM/ DUM8(39),IO6,IFLAGS,IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
C.....a common is added for some variables which need to be initialized
C     outside the multiple calls to BEMK2 and BEMKFMA, CF03
C
      COMMON /BEMINI/ EFJXIP,EFJETP,JSAVE,NINTPR,NSIDEP
C
      DIMENSION FSF(NDM),FSD(NDM),FSFX12(NDM),FSFE12(NDM),
     .          FSDX12(NDM),FSDE12(NDM),FSFX23(NDM),FSFE23(NDM),
     .          FSDX23(NDM),FSDE23(NDM),XELT(MNOD,2)
C
      DIMENSION ZMYCSTV((MP+1)*(MP+1)),IELTS(NIE),ZMYCST((MP+1),(MP+1)),
     .          ZINTQX(MNODM,(MP+1),(MP+1)),ZINTQY(MNODM,(MP+1),(MP+1)),
     .          ZINTUX(MNODM,(MP+1),(MP+1)),ZINTUY(MNODM,(MP+1),(MP+1))
      DIMENSION ZQXIE(NNIE*(MP+1)*(MP+1)),
     .          ZQYIE(NNIE*(MP+1)*(MP+1)),
     .          ZUXIE(NNIE*(MP+1)*(MP+1)),
     .          ZUYIE(NNIE*(MP+1)*(MP+1))
C
C.....Global numbers of nodes related to integrated elements
C
      DIMENSION JGCOEF(NNIE)
C
      SAVE
C
      DATA TEXTE/'BEMK01'/,ZERO/0.D0/,ONE/1.D0/,TWO/2.D0/,THREE/3.D0/,
     .     PIS8/0.3926990816987241D0/,PIS2/1.570796326794896D0/,
     .     OTHREE/0.3333333333333333D0/,S3/0.3333333333333333D0/,
     .     XELT/-1.D0,1.D0,1.D0,-1.D0,-1.D0,-1.D0,1.D0,1.D0/,
     .     HALF/0.5D0/
C
C.....definition of 2-dimensional ZMYCST from 1-dim ZMYCSTV
C
      DO N=0,MP
         DO M=0,MP
            ZMYCST(N+1,M+1) = ZMYCSTV(N*(MP+1)+M+1)
         END DO
      END DO
      DO J=1,NNIE
         DO N=0,MP
            DO M=0,MP
	       K=((J-1)*(MP+1)+N)*(MP+1)+M+1
	       ZQXIE(K) = ZERO
	       ZQYIE(K) = ZERO
	       ZUXIE(K) = ZERO
	       ZUYIE(K) = ZERO
            END DO
	 END DO
      END DO
      DO J=1,NNIE
         JGCOEF(J)=0
      END DO
      INDJG = 0
C
      NNODE  = MNOD
C
C=====Loop on the elements, local computations, assembling
C     but for one cell
C
      DO INDIE=1,NIE
         IE = IELTS(INDIE)
C
C     ...Local values assigned to element IE
C
C NINTR: a bug observed with NINTR=INTG(IE), and -2 is sufficient (CF2005)
         NINTR = INTG(IE)-2
         NSUBM = ISUB(IE)
         NSIDE = IFACE(IE)
C
C        4-node connectivity in NODE1(NDM)
C
         DO J=1,NNODE
            NODE2(J)         = ILCON(IE,J)
            XYZLO2(J,1)      = XYZNOD(NODE2(J),1)
            XYZLO2(J,2)      = XYZNOD(NODE2(J),2)
            XYZLO2(J,3)      = XYZNOD(NODE2(J),3)
            INFACE(NODE2(J)) = IFACE(IE)
         END DO
C
C        Assign NOD1E(NDM) and NNODE for correct assembly of ZK(U/Q)L
C        i.e., considering MII elements
C
         ILOC = 0
         DO J=1,NNODE
            DO I=1,NNODE
               ILOC = ILOC + 1
               NODE1(ILOC)    = IGCON(IE,I,J)
               XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
               XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
               XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYM
	       IF (ISYM.EQ.1) THEN
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.6.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYS
               INCOND(NODE1(ILOC)) = IBCOND(IFACE(IE))
            END DO
         END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
C     ...Filling up of the common INTEG when the elements have different
C        node or integration points numbers (accelerator)
C
         IRECAL = 1
         IF (NINTR.NE.NINTPR) THEN
C
C           Regular Gauss points
C
            CALL GAUSSP
C           -----------
C
            IRECAL = 0
         END IF
C
C        MII cubic 4-node shape functions
C        Compute shape functions and their derivatives for
C        cubic element on the boundary and store in common FSCUB
C
         IF(EFJXI.NE.EFJXIP.OR.NINTR.NE.NINTPR) THEN
C
C           XI direction
C
            IRECAL = 0
            DO IP=1,NINTR
               CHI = (ETARP(IP) + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0XI(J,IP) = FSF(J)
                  FN1XI(J,IP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
         IF(EFJET.NE.EFJETP.OR.NINTR.NE.NINTPR) THEN
C
C           ETA direction
C
            IRECAL = 0
            DO JP=1,NINTR
               CHI = (ETARP(JP) + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0ET(J,JP) = FSF(J)
                  FN1ET(J,JP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
C
C        Calculates bidim shape functions at all reg/sing integ. points
C
         IF(IRECAL.EQ.0) THEN
            ILOC = 0
            DO J=1,NNODE
               DO I=1,NNODE
                  ILOC = ILOC + 1
                  DO IP=1,NINTR
                     DO JP=1,NINTR
C
C                       Shape functions at all regular integ. points
C
                        FIRIP(ILOC,IP,JP)  = FN0XI(I,IP)*FN0ET(J,JP)
                        DFIXIP(ILOC,IP,JP) = FN1XI(I,IP)*FN0ET(J,JP)
                        DFIETP(ILOC,IP,JP) = FN0XI(I,IP)*FN1ET(J,JP)
                     END DO
                  END DO
               END DO
            END DO
C
         END IF
C
C     ...Computation of integrations, save geometry and shape
C        functions in common SAVEF, beginning at JSAVE (change for
C        no subdivisions or subdivision integrals)
C
         CALL BILFMA(ZINTQX,ZINTQY,ZINTUX,ZINTUY,IE,OX,OY,OZ,MP,ZMYCST)
C        -----------
C
         NINTPR = NINTR
         NSIDEP = NSIDE
         EFJXIP = EFJXI
         EFJETP = EFJET
C
C     ...Assembling and storage for all the nodes of the
C        current cell:all the constituting nodes of the
C        elements
C
	 DO J=1,MNODM
	    JG=NODE1(J)
	    CALL ISINVECT(ISIN,JG,JGCOEF,INDJG)
C           -------------
	    IF (ISIN.GT.0) THEN
	      INDJGK=ISIN
	    ELSE
	      INDJG=INDJG+1
	      JGCOEF(INDJG)=JG
	      INDJGK=INDJG
	    END IF
            DO N=0,MP
               DO M=0,MP
	          K=((INDJGK-1)*(MP+1)+N)*(MP+1)+M+1
	          ZQXIE(K) = ZQXIE(K) + ZINTQX(J,N+1,M+1)
	          ZQYIE(K) = ZQYIE(K) + ZINTQY(J,N+1,M+1)
	          ZUXIE(K) = ZUXIE(K) + ZINTUX(J,N+1,M+1)
	          ZUYIE(K) = ZUYIE(K) + ZINTUY(J,N+1,M+1)
               END DO
            END DO
         END DO
      END DO
C
      RETURN
 2000 FORMAT(3I5,6F10.5)
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE BEMKSYM(ZQXIE,ZQYIE,ZUXIE,ZUYIE,
     .                   ZQXIES,ZQYIES,ZUXIES,ZUYIES,NNIE,
     .                   OX,OY,OZ,NIE,IELTS,MP,ZMYCSTV)
C-----------------------------------------------------------------------
C0  BEMKF     BEMKSYM
C1  PURPOSE   Compute the complex multipole ZQ/UXIE+iZQ/UYIE and its symmetric,
C1            performs integrations through a BEM analysis
C1
C3  CALL ARG. XYZNOD(NOM)         = Coordinates of discretization nodes
C3            XICON(MOM,2)        = xo,eta for MII elements
C3            IGCON(MOM,4,4)      = MII elts connectivity
C3            ILCON(MOM,4)        = 4-node elt. connectivity
C3            IFACE(MOM)          = Face code for each element
C3            ISUB(MOMM)          = Max subdivisions for elts.
C3            INTG(MOMM)          = Nb. of integ. points per elt.
C3            LSUB(NOM)           = Subdivision status for element IE
C3            LXYZI(NOM,2)        = Addresses of double/triple nodes
C3            JSLIM(6)            = Record limits for saving in /SAVEF/
C3            NELEF(6),NNODF(6)   = Nbs. of elts/nodes for each face
C3            IELEF(6,2),INODF()  = Begin/ending node/elemt Nbs. for face
C3            IBCOND(6)           = Boundary condition type for each face
C3            NOM,MOM             = Discr. nb. of nodes and elements
C3            NOMM,MOMM           = Discr. max nb. of nodes and elts.
C3            NNODM               = Discr. max nb. of nodes per element
C3            ALMAX [0,PI/2]      = Limit angle for adapt. integration
C3            XYZLO1(NNODM,3)     = XYZ-coordinates of element IE MII nodes
C3            NODE1(NNODE)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = XYZ-coordinates of element IE 4 nodes
C3            NODE2(NNODE)        = Nodes of 4-node element IE
C3            NSUBM               = Max nb. of subdivisions for IE (0-8)
C3            NINTR               = Nb. of regular and singular int. pts
C3            ETARP()             = Regular integr. points
C3            WEIGTR()            = Regular integr. weights
C3            INCOND(NOMM)        = Boundary condition types for all nodes
C3            ZQXIE,ZQYIE,ZUXIE,ZUYIE(NNIE*(MP+1)*(MP+1))
C3                                = Multipoles
C3            NNIE                = Number of nodes related to the elements
C3                                  of the current cell
C3            INFACE(NOMM)        = Face types for all nodes
C3            OX,OY,OZ            = Center of current cell
C3            NIE                 = Nb of elts in the current cell
C3            IELTS(NIE)          = Elts numbers of the current cell
C3            MP                  = Nb of multipoles
C3            ZMYCSTV((MP+1)*(MP+1)) = Precomputed cst for multipoles
C4
C4  RET. ARG. ZQXIE,ZQYIE,ZUXIE,ZUYIE,ZQXIES,ZQYIES,ZUXIES,ZUYIES,JSLIM
C6
C6  INT.CALL  GAUSSP,BILFMA,BIDFMA,ERRORS
C6            SHAPFN,SHAPD1
CE  ERRORS    01= Insufficient length of saving buffer
C10 Jan. 99   S. Grilli at INLN
C10           adapted for FMA, C. Fochesato, CMLA, 2003
C-----------------------------------------------------------------------

      IMPLICIT REAL*8 (A-H,O-Z)
C
      CHARACTER*8 TEXTE

      INCLUDE 'param.inc'

      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),
     .                DUM3(18),IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),IDUM2(36),JSLIM(6),NELEF(6),
     .                NNODF(6),IELEF(6,2),INODF(6,2),IBCOND(6)
      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/  ETARP(NINTM),WEIGTR(NINTM),PHIP12(NINTM),
     .                PHIP23(NINTM),RMIP12(NINTM),RMIP23(NINTM),
     .                RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .                XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .                ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /FSCUB/ FN0XI(NDM,NINTM),FN0ET(NDM,NINTM),
     .               FN1XI(NDM,NINTM),FN1ET(NDM,NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),
     .               DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),DUM7(7*NINTM*NINTM)
      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
     .               FIS23(MNODM,NINTM,NINTM,MNOD),
     .               DFIX12(MNODM,NINTM,NINTM,MNOD),
     .               DFIX23(MNODM,NINTM,NINTM,MNOD),
     .               DFIE12(MNODM,NINTM,NINTM,MNOD),
     .               DFIE23(MNODM,NINTM,NINTM,MNOD)
c      COMMON /SYSTEM/ DUM8(39),IO6,IFLAGS,IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
C.....a common is added for some variables which need to be initialized
C     outside the multiple calls to BEMK2 and BEMKFMA, CF03
C
      COMMON /BEMINI/ EFJXIP,EFJETP,JSAVE,NINTPR,NSIDEP
C
      DIMENSION FSF(NDM),FSD(NDM),FSFX12(NDM),FSFE12(NDM),
     .          FSDX12(NDM),FSDE12(NDM),FSFX23(NDM),FSFE23(NDM),
     .          FSDX23(NDM),FSDE23(NDM),XELT(MNOD,2)
C
      DIMENSION ZMYCSTV(MP+1*MP+1),IELTS(NIE),ZMYCST(MP+1,MP+1),
     .          ZINTQX(MNODM,MP+1,MP+1),ZINTQY(MNODM,MP+1,MP+1),
     .          ZINTUX(MNODM,MP+1,MP+1),ZINTUY(MNODM,MP+1,MP+1),
     .          ZINTQXS(MNODM,MP+1,MP+1),ZINTQYS(MNODM,MP+1,MP+1),
     .          ZINTUXS(MNODM,MP+1,MP+1),ZINTUYS(MNODM,MP+1,MP+1)
      DIMENSION ZQXIE(NNIE*(MP+1)*(MP+1)),
     .          ZQYIE(NNIE*(MP+1)*(MP+1)),
     .          ZUXIE(NNIE*(MP+1)*(MP+1)),
     .          ZUYIE(NNIE*(MP+1)*(MP+1)),
     .          ZQXIES(NNIE*(MP+1)*(MP+1)),
     .          ZQYIES(NNIE*(MP+1)*(MP+1)),
     .          ZUXIES(NNIE*(MP+1)*(MP+1)),
     .          ZUYIES(NNIE*(MP+1)*(MP+1))
C
C.....Global numbers of nodes related to integrated elements
C
      DIMENSION JGCOEF(NNIE)
C
      SAVE
C
      DATA TEXTE/'BEMK01'/,ZERO/0.D0/,ONE/1.D0/,TWO/2.D0/,THREE/3.D0/,
     .     PIS8/0.3926990816987241D0/,PIS2/1.570796326794896D0/,
     .     OTHREE/0.3333333333333333D0/,S3/0.3333333333333333D0/,
     .     XELT/-1.D0,1.D0,1.D0,-1.D0,-1.D0,-1.D0,1.D0,1.D0/,
     .     HALF/0.5D0/
C
C.....definition of 2-dimensional ZMYCST from 1-dim ZMYCSTV
C
      DO N=0,MP
         DO M=0,MP
            ZMYCST(N+1,M+1) = ZMYCSTV(N*(MP+1)+M+1)
         END DO
      END DO
      DO J=1,NNIE
         DO N=0,MP
            DO M=0,MP
	       K=((J-1)*(MP+1)+N)*(MP+1)+M+1
	       ZQXIE(K) = ZERO
	       ZQYIE(K) = ZERO
	       ZUXIE(K) = ZERO
	       ZUYIE(K) = ZERO
	       ZQXIES(K) = ZERO
	       ZQYIES(K) = ZERO
	       ZUXIES(K) = ZERO
	       ZUYIES(K) = ZERO
            END DO
	 END DO
      END DO
      DO J=1,NNIE
         JGCOEF(J)=0
      END DO
      INDJG = 0
C
      NNODE  = MNOD
C
C=====Loop on the elements, local computations, assembling
C     but for one cell
C
      DO INDIE=1,NIE
         IE = IELTS(INDIE)
C
C     ...Local values assigned to element IE
C
C NINTR: a bug observed with NINTR=INTG(IE), and -2 is sufficient (CF2005)
         NINTR = INTG(IE)-2
         NSUBM = ISUB(IE)
         NSIDE = IFACE(IE)
C
C        4-node connectivity in NODE1(NDM)
C
         DO J=1,NNODE
            NODE2(J)         = ILCON(IE,J)
            XYZLO2(J,1)      = XYZNOD(NODE2(J),1)
            XYZLO2(J,2)      = XYZNOD(NODE2(J),2)
            XYZLO2(J,3)      = XYZNOD(NODE2(J),3)
            INFACE(NODE2(J)) = IFACE(IE)
         END DO
C
C        Assign NOD1E(NDM) and NNODE for correct assembly of ZK(U/Q)L
C        i.e., considering MII elements
C
         ILOC = 0
         DO J=1,NNODE
            DO I=1,NNODE
               ILOC = ILOC + 1
               NODE1(ILOC)    = IGCON(IE,I,J)
               XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
               XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
               XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYM
	       IF (ISYM.EQ.1) THEN
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.6.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYS
               INCOND(NODE1(ILOC)) = IBCOND(IFACE(IE))
            END DO
         END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
C     ...Filling up of the common INTEG when the elements have different
C        node or integration points numbers (accelerator)
C
         IRECAL = 1
         IF (NINTR.NE.NINTPR) THEN
C
C           Regular Gauss points
C
            CALL GAUSSP
C           -----------
C
            IRECAL = 0
         END IF
C
C        MII cubic 4-node shape functions
C        Compute shape functions and their derivatives for
C        cubic element on the boundary and store in common FSCUB
C
         IF(EFJXI.NE.EFJXIP.OR.NINTR.NE.NINTPR) THEN
C
C           XI direction
C
            IRECAL = 0
            DO IP=1,NINTR
               CHI = (ETARP(IP) + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0XI(J,IP) = FSF(J)
                  FN1XI(J,IP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
         IF(EFJET.NE.EFJETP.OR.NINTR.NE.NINTPR) THEN
C
C           ETA direction
C
            IRECAL = 0
            DO JP=1,NINTR
               CHI = (ETARP(JP) + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0ET(J,JP) = FSF(J)
                  FN1ET(J,JP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
C
C        Calculates bidim shape functions at all reg/sing integ. points
C
         IF(IRECAL.EQ.0) THEN
            ILOC = 0
            DO J=1,NNODE
               DO I=1,NNODE
                  ILOC = ILOC + 1
                  DO IP=1,NINTR
                     DO JP=1,NINTR
C
C                       Shape functions at all regular integ. points
C
                        FIRIP(ILOC,IP,JP)  = FN0XI(I,IP)*FN0ET(J,JP)
                        DFIXIP(ILOC,IP,JP) = FN1XI(I,IP)*FN0ET(J,JP)
                        DFIETP(ILOC,IP,JP) = FN0XI(I,IP)*FN1ET(J,JP)
                     END DO
                  END DO
               END DO
            END DO
C
         END IF
C
C     ...Computation of integrations, save geometry and shape
C        functions in common SAVEF, beginning at JSAVE (change for
C        no subdivisions or subdivision integrals)
C
         CALL BILSYM(ZINTQX,ZINTQY,ZINTUX,ZINTUY,
C        -----------
     .               ZINTQXS,ZINTQYS,ZINTUXS,ZINTUYS,
     .               IE,OX,OY,OZ,MP,ZMYCST)
C
         NINTPR = NINTR
         NSIDEP = NSIDE
         EFJXIP = EFJXI
         EFJETP = EFJET
C
C     ...Assembling and storage for all the nodes of the
C        current cell:all the constituting nodes of the
C        elements
C
	 DO J=1,MNODM
	    JG=NODE1(J)
	    CALL ISINVECT(ISIN,JG,JGCOEF,INDJG)
C           -------------
	    IF (ISIN.GT.0) THEN
	      INDJGK=ISIN
	    ELSE
	      INDJG=INDJG+1
	      JGCOEF(INDJG)=JG
	      INDJGK=INDJG
	    END IF
            DO N=0,MP
               DO M=0,MP
	          K=((INDJGK-1)*(MP+1)+N)*(MP+1)+M+1
	          ZQXIE(K) = ZQXIE(K) + ZINTQX(J,N+1,M+1)
	          ZQYIE(K) = ZQYIE(K) + ZINTQY(J,N+1,M+1)
	          ZUXIE(K) = ZUXIE(K) + ZINTUX(J,N+1,M+1)
	          ZUYIE(K) = ZUYIE(K) + ZINTUY(J,N+1,M+1)
	          ZQXIES(K) = ZQXIES(K) + ZINTQXS(J,N+1,M+1)
	          ZQYIES(K) = ZQYIES(K) + ZINTQYS(J,N+1,M+1)
	          ZUXIES(K) = ZUXIES(K) + ZINTUXS(J,N+1,M+1)
	          ZUYIES(K) = ZUYIES(K) + ZINTUYS(J,N+1,M+1)
               END DO
            END DO
         END DO
      END DO
C
      RETURN
 2000 FORMAT(3I5,6F10.5)
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE BEMKSYM2(ZQXIE,ZQYIE,ZUXIE,ZUYIE,
     .                   ZQXIES,ZQYIES,ZUXIES,ZUYIES,
     .                   ZQXIES2,ZQYIES2,ZUXIES2,ZUYIES2,
     .                   ZQXIES3,ZQYIES3,ZUXIES3,ZUYIES3,
     .                   NNIE,OX,OY,OZ,NIE,IELTS,MP,ZMYCSTV)
C-----------------------------------------------------------------------
C0  BEMKF     BEMKSYM2
C1  PURPOSE   Compute the complex multipole ZQ/UXIE+iZQ/UYIE and its symmetric(y+flat bottom),
C1            performs integrations through a BEM analysis
C1
C3  CALL ARG. XYZNOD(NOM)         = Coordinates of discretization nodes
C3            XICON(MOM,2)        = xo,eta for MII elements
C3            IGCON(MOM,4,4)      = MII elts connectivity
C3            ILCON(MOM,4)        = 4-node elt. connectivity
C3            IFACE(MOM)          = Face code for each element
C3            ISUB(MOMM)          = Max subdivisions for elts.
C3            INTG(MOMM)          = Nb. of integ. points per elt.
C3            LSUB(NOM)           = Subdivision status for element IE
C3            LXYZI(NOM,2)        = Addresses of double/triple nodes
C3            JSLIM(6)            = Record limits for saving in /SAVEF/
C3            NELEF(6),NNODF(6)   = Nbs. of elts/nodes for each face
C3            IELEF(6,2),INODF()  = Begin/ending node/elemt Nbs. for face
C3            IBCOND(6)           = Boundary condition type for each face
C3            NOM,MOM             = Discr. nb. of nodes and elements
C3            NOMM,MOMM           = Discr. max nb. of nodes and elts.
C3            NNODM               = Discr. max nb. of nodes per element
C3            ALMAX [0,PI/2]      = Limit angle for adapt. integration
C3            XYZLO1(NNODM,3)     = XYZ-coordinates of element IE MII nodes
C3            NODE1(NNODE)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = XYZ-coordinates of element IE 4 nodes
C3            NODE2(NNODE)        = Nodes of 4-node element IE
C3            NSUBM               = Max nb. of subdivisions for IE (0-8)
C3            NINTR               = Nb. of regular and singular int. pts
C3            ETARP()             = Regular integr. points
C3            WEIGTR()            = Regular integr. weights
C3            INCOND(NOMM)        = Boundary condition types for all nodes
C3            ZQXIE,ZQYIE,ZUXIE,ZUYIE(NNIE*(MP+1)*(MP+1))
C3                                = Multipoles
C3            NNIE                = Number of nodes related to the elements
C3                                  of the current cell
C3            INFACE(NOMM)        = Face types for all nodes
C3            OX,OY,OZ            = Center of current cell
C3            NIE                 = Nb of elts in the current cell
C3            IELTS(NIE)          = Elts numbers of the current cell
C3            MP                  = Nb of multipoles
C3            ZMYCSTV((MP+1)*(MP+1)) = Precomputed cst for multipoles
C4
C4  RET. ARG. ZQXIE,ZQYIE,ZUXIE,ZUYIE,ZQXIES,ZQYIES,ZUXIES,ZUYIES,JSLIM
C6
C6  INT.CALL  GAUSSP,BILFMA,BIDFMA,ERRORS
C6            SHAPFN,SHAPD1
CE  ERRORS    01= Insufficient length of saving buffer
C10 Jan. 99   S. Grilli at INLN
C10           adapted for FMA, C. Fochesato, CMLA, 2003
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
C
      CHARACTER*8 TEXTE
C
      INCLUDE 'param.inc'
      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),
     .                DUM3(18),IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),IDUM2(36),JSLIM(6),NELEF(6),
     .                NNODF(6),IELEF(6,2),INODF(6,2),IBCOND(6)
      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/  ETARP(NINTM),WEIGTR(NINTM),PHIP12(NINTM),
     .                PHIP23(NINTM),RMIP12(NINTM),RMIP23(NINTM),
     .                RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .                XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .                ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /FSCUB/ FN0XI(NDM,NINTM),FN0ET(NDM,NINTM),
     .               FN1XI(NDM,NINTM),FN1ET(NDM,NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),
     .               DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),DUM7(7*NINTM*NINTM)
      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
     .               FIS23(MNODM,NINTM,NINTM,MNOD),
     .               DFIX12(MNODM,NINTM,NINTM,MNOD),
     .               DFIX23(MNODM,NINTM,NINTM,MNOD),
     .               DFIE12(MNODM,NINTM,NINTM,MNOD),
     .               DFIE23(MNODM,NINTM,NINTM,MNOD)
c      COMMON /SYSTEM/ DUM8(39),IO6,IFLAGS,IFLAGV,IPTYP,ILOOP
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
C.....a common is added for some variables which need to be initialized
C     outside the multiple calls to BEMK2 and BEMKFMA, CF03
C
      COMMON /BEMINI/ EFJXIP,EFJETP,JSAVE,NINTPR,NSIDEP
C
      DIMENSION FSF(NDM),FSD(NDM),FSFX12(NDM),FSFE12(NDM),
     .          FSDX12(NDM),FSDE12(NDM),FSFX23(NDM),FSFE23(NDM),
     .          FSDX23(NDM),FSDE23(NDM),XELT(MNOD,2)
C
      DIMENSION ZMYCSTV(MP+1*MP+1),IELTS(NIE),ZMYCST(MP+1,MP+1),
     .          ZINTQX(MNODM,MP+1,MP+1),ZINTQY(MNODM,MP+1,MP+1),
     .          ZINTUX(MNODM,MP+1,MP+1),ZINTUY(MNODM,MP+1,MP+1),
     .          ZINTQXS(MNODM,MP+1,MP+1),ZINTQYS(MNODM,MP+1,MP+1),
     .          ZINTUXS(MNODM,MP+1,MP+1),ZINTUYS(MNODM,MP+1,MP+1),
     .          ZINTQXS2(MNODM,MP+1,MP+1),ZINTQYS2(MNODM,MP+1,MP+1),
     .          ZINTUXS2(MNODM,MP+1,MP+1),ZINTUYS2(MNODM,MP+1,MP+1),
     .          ZINTQXS3(MNODM,MP+1,MP+1),ZINTQYS3(MNODM,MP+1,MP+1),
     .          ZINTUXS3(MNODM,MP+1,MP+1),ZINTUYS3(MNODM,MP+1,MP+1)
      DIMENSION ZQXIE(NNIE*(MP+1)*(MP+1)),
     .          ZQYIE(NNIE*(MP+1)*(MP+1)),
     .          ZUXIE(NNIE*(MP+1)*(MP+1)),
     .          ZUYIE(NNIE*(MP+1)*(MP+1)),
     .          ZQXIES(NNIE*(MP+1)*(MP+1)),
     .          ZQYIES(NNIE*(MP+1)*(MP+1)),
     .          ZUXIES(NNIE*(MP+1)*(MP+1)),
     .          ZUYIES(NNIE*(MP+1)*(MP+1)),
     .          ZQXIES2(NNIE*(MP+1)*(MP+1)),
     .          ZQYIES2(NNIE*(MP+1)*(MP+1)),
     .          ZUXIES2(NNIE*(MP+1)*(MP+1)),
     .          ZUYIES2(NNIE*(MP+1)*(MP+1)),
     .          ZQXIES3(NNIE*(MP+1)*(MP+1)),
     .          ZQYIES3(NNIE*(MP+1)*(MP+1)),
     .          ZUXIES3(NNIE*(MP+1)*(MP+1)),
     .          ZUYIES3(NNIE*(MP+1)*(MP+1))
C
C.....Global numbers of nodes related to integrated elements
C
      DIMENSION JGCOEF(NNIE)
C
      SAVE
C
      DATA TEXTE/'BEMK01'/,ZERO/0.D0/,ONE/1.D0/,TWO/2.D0/,THREE/3.D0/,
     .     PIS8/0.3926990816987241D0/,PIS2/1.570796326794896D0/,
     .     OTHREE/0.3333333333333333D0/,S3/0.3333333333333333D0/,
     .     XELT/-1.D0,1.D0,1.D0,-1.D0,-1.D0,-1.D0,1.D0,1.D0/,
     .     HALF/0.5D0/
C
C.....definition of 2-dimensional ZMYCST from 1-dim ZMYCSTV
C
      DO N=0,MP
         DO M=0,MP
            ZMYCST(N+1,M+1) = ZMYCSTV(N*(MP+1)+M+1)
         END DO
      END DO
      DO J=1,NNIE
         DO N=0,MP
            DO M=0,MP
	       K=((J-1)*(MP+1)+N)*(MP+1)+M+1
	       ZQXIE(K) = ZERO
	       ZQYIE(K) = ZERO
	       ZUXIE(K) = ZERO
	       ZUYIE(K) = ZERO
	       ZQXIES(K) = ZERO
	       ZQYIES(K) = ZERO
	       ZUXIES(K) = ZERO
	       ZUYIES(K) = ZERO
	       ZQXIES2(K) = ZERO
	       ZQYIES2(K) = ZERO
	       ZUXIES2(K) = ZERO
	       ZUYIES2(K) = ZERO
	       ZQXIES3(K) = ZERO
	       ZQYIES3(K) = ZERO
	       ZUXIES3(K) = ZERO
	       ZUYIES3(K) = ZERO
            END DO
	 END DO
      END DO
      DO J=1,NNIE
         JGCOEF(J)=0
      END DO
      INDJG = 0
C
      NNODE  = MNOD
C
C=====Loop on the elements, local computations, assembling
C     but for one cell
C
      DO INDIE=1,NIE
         IE = IELTS(INDIE)
C
C     ...Local values assigned to element IE
C
C NINTR: a bug observed with NINTR=INTG(IE), and -2 is sufficient (CF2005)
         NINTR = INTG(IE)-2
         NSUBM = ISUB(IE)
         NSIDE = IFACE(IE)
C
C        4-node connectivity in NODE1(NDM)
C
         DO J=1,NNODE
            NODE2(J)         = ILCON(IE,J)
            XYZLO2(J,1)      = XYZNOD(NODE2(J),1)
            XYZLO2(J,2)      = XYZNOD(NODE2(J),2)
            XYZLO2(J,3)      = XYZNOD(NODE2(J),3)
            INFACE(NODE2(J)) = IFACE(IE)
         END DO
C
C        Assign NOD1E(NDM) and NNODE for correct assembly of ZK(U/Q)L
C        i.e., considering MII elements
C
         ILOC = 0
         DO J=1,NNODE
            DO I=1,NNODE
               ILOC = ILOC + 1
               NODE1(ILOC)    = IGCON(IE,I,J)
               XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
               XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
               XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
CSYM
	       IF (ISYM.EQ.1) THEN
                 IF (NSIDE.EQ.1.AND.
     .             IE.GT.(IELEF(NSIDE,2)-MX).AND.
     .             J.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.6.AND.
     .             IE.LT.(IELEF(NSIDE,1)+MX).AND.
     .             J.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.4.AND.
     .             MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
     .             I.EQ.NNODE) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
                 IF (NSIDE.EQ.2.AND.
     .             MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
     .             I.EQ.1) THEN
                  XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
                 END IF
               END IF
CMYS
               INCOND(NODE1(ILOC)) = IBCOND(IFACE(IE))
            END DO
         END DO
C
         EFJXI = XICON(IE,1)
         EFJET = XICON(IE,2)
C
C     ...Filling up of the common INTEG when the elements have different
C        node or integration points numbers (accelerator)
C
         IRECAL = 1
         IF (NINTR.NE.NINTPR) THEN
C
C           Regular Gauss points
C
            CALL GAUSSP
C           -----------
C
            IRECAL = 0
         END IF
C
C        MII cubic 4-node shape functions
C        Compute shape functions and their derivatives for
C        cubic element on the boundary and store in common FSCUB
C
         IF(EFJXI.NE.EFJXIP.OR.NINTR.NE.NINTPR) THEN
C
C           XI direction
C
            IRECAL = 0
            DO IP=1,NINTR
               CHI = (ETARP(IP) + ONE)*OTHREE + EFJXI
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0XI(J,IP) = FSF(J)
                  FN1XI(J,IP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
         IF(EFJET.NE.EFJETP.OR.NINTR.NE.NINTPR) THEN
C
C           ETA direction
C
            IRECAL = 0
            DO JP=1,NINTR
               CHI = (ETARP(JP) + ONE)*OTHREE + EFJET
               CALL SHAPFN(CHI,FSF,NNODE)
C              -----------
               CALL SHAPD1(CHI,FSD,NNODE)
C              -----------
               DO J=1,NNODE
                  FN0ET(J,JP) = FSF(J)
                  FN1ET(J,JP) = OTHREE*FSD(J)
               END DO
            END DO
         END IF
C
C        Calculates bidim shape functions at all reg/sing integ. points
C
         IF(IRECAL.EQ.0) THEN
            ILOC = 0
            DO J=1,NNODE
               DO I=1,NNODE
                  ILOC = ILOC + 1
                  DO IP=1,NINTR
                     DO JP=1,NINTR
C
C                       Shape functions at all regular integ. points
C
                        FIRIP(ILOC,IP,JP)  = FN0XI(I,IP)*FN0ET(J,JP)
                        DFIXIP(ILOC,IP,JP) = FN1XI(I,IP)*FN0ET(J,JP)
                        DFIETP(ILOC,IP,JP) = FN0XI(I,IP)*FN1ET(J,JP)
                     END DO
                  END DO
               END DO
            END DO
C
         END IF
C
C     ...Computation of integrations, save geometry and shape
C        functions in common SAVEF, beginning at JSAVE (change for
C        no subdivisions or subdivision integrals)
C
         CALL BILSYM2(ZINTQX,ZINTQY,ZINTUX,ZINTUY,
C        -----------
     .               ZINTQXS,ZINTQYS,ZINTUXS,ZINTUYS,
     .               ZINTQXS2,ZINTQYS2,ZINTUXS2,ZINTUYS2,
     .               ZINTQXS3,ZINTQYS3,ZINTUXS3,ZINTUYS3,
     .               IE,OX,OY,OZ,MP,ZMYCST)
C
         NINTPR = NINTR
         NSIDEP = NSIDE
         EFJXIP = EFJXI
         EFJETP = EFJET
C
C     ...Assembling and storage for all the nodes of the
C        current cell:all the constituting nodes of the
C        elements
C
	 DO J=1,MNODM
	    JG=NODE1(J)
	    CALL ISINVECT(ISIN,JG,JGCOEF,INDJG)
C           -------------
	    IF (ISIN.GT.0) THEN
	      INDJGK=ISIN
	    ELSE
	      INDJG=INDJG+1
	      JGCOEF(INDJG)=JG
	      INDJGK=INDJG
	    END IF
            DO N=0,MP
               DO M=0,MP
	          K=((INDJGK-1)*(MP+1)+N)*(MP+1)+M+1
	          ZQXIE(K) = ZQXIE(K) + ZINTQX(J,N+1,M+1)
	          ZQYIE(K) = ZQYIE(K) + ZINTQY(J,N+1,M+1)
	          ZUXIE(K) = ZUXIE(K) + ZINTUX(J,N+1,M+1)
	          ZUYIE(K) = ZUYIE(K) + ZINTUY(J,N+1,M+1)
	          ZQXIES(K) = ZQXIES(K) + ZINTQXS(J,N+1,M+1)
	          ZQYIES(K) = ZQYIES(K) + ZINTQYS(J,N+1,M+1)
	          ZUXIES(K) = ZUXIES(K) + ZINTUXS(J,N+1,M+1)
	          ZUYIES(K) = ZUYIES(K) + ZINTUYS(J,N+1,M+1)
	          ZQXIES2(K) = ZQXIES2(K) + ZINTQXS2(J,N+1,M+1)
	          ZQYIES2(K) = ZQYIES2(K) + ZINTQYS2(J,N+1,M+1)
	          ZUXIES2(K) = ZUXIES2(K) + ZINTUXS2(J,N+1,M+1)
	          ZUYIES2(K) = ZUYIES2(K) + ZINTUYS2(J,N+1,M+1)
	          ZQXIES3(K) = ZQXIES3(K) + ZINTQXS3(J,N+1,M+1)
	          ZQYIES3(K) = ZQYIES3(K) + ZINTQYS3(J,N+1,M+1)
	          ZUXIES3(K) = ZUXIES3(K) + ZINTUXS3(J,N+1,M+1)
	          ZUYIES3(K) = ZUYIES3(K) + ZINTUYS3(J,N+1,M+1)
               END DO
            END DO
         END DO
      END DO
C
      RETURN
 2000 FORMAT(3I5,6F10.5)
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE ISINVECT(ISIN,I,IVECT,N)
C-----------------------------------------------------------------------
C
C     puts ISIN to 0 if I belongs to the vector  IVECT of length N
C     and to the first index where I is encountered in IVECT otherwise
C
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION IVECT(N)
C
      ISIN = 0
      IF(N.GT.0) THEN
        J=1
        DO WHILE (I.NE.IVECT(J).AND.J.LT.N)
          J=J+1
        END DO
        IF (J.LT.N) THEN
           ISIN = J
        ELSE
           IF (I.EQ.IVECT(N)) THEN
              ISIN = N
	   END IF
        END IF
      END IF
C
      RETURN
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE BILFMA(ZINTQX,ZINTQY,ZINTUX,ZINTUY,
     .                                       IE,OX,OY,OZ,MP,ZMYCST)
C-----------------------------------------------------------------------
C1  BILFMAF   BILFMA
C1  PURPOSE   Compute the integrations for the multipoles
C2  CALL      CALL BILFMA(ZINTQX,ZINTQY,ZINTUX,ZINTUY)
C3  CALL ARG. XYZNOD(NOMM,3)      = Coordinates of discretization nodes
C3            LXYZ(NOM)           = Addresses of possible double nodes
C3            NOM,NOMM            = Number of nodes and max nb. of nodes
C3            NINTR               = Nb. of reg. and sing. int. pts of IE
C3            WEIGTR              = Regular integr. weights
C3            IP,JP               = current integration point of IE
C3            XYZLO1(MNODM,3)     = MII nodes for IE
C3            NODE1(NNODM)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = 4-nodes for IE
C3            NODE2(NNODM)        = Nodes of 4-node element IE
C3            FIRIP(NNODM,NINTM,.)= Reg. shape fcts. and   )  At all
C3            DFIXIP(),DXIETP()   = their derivatives      )  points IP,JP
C3            XYZPR(NINTM,.,3)    = All integr. point coordinates
C3            ZNPR(NINTM,.,3)     = All integr. point normal vector
C3            ZJACPR(NINTM,NINTM) = All integr. point jacobian
C3            FIS12(),FIS23()     = Sing. shape fcts. and  )  At all
C3            DFIX12(),DFIX23()   = their derivatives      )  points IP,JP
C3            DFIE12(),DFIE23()   = their derivatives      )  and 4-nodes
C3            FI(NNODM)           = Shape functions and    )  At point
C3            DFIX(NNODM),DFIE()  = their derivatives      )  IP,JP
C3            XYZP(3)             = Current integr. point coordinates
C3            ZNP(3)              = Current integr. point normal vector
C3            ZJACP               = Current integr. point jacobian
C3            IE                  = Current elt
C3            OX,OY,OZ            = Center of current cell
C3            MP                  = Nb of multipoles
C3            ZMYCST(MP,MP)        = Precomputed cst for multipoles
C4  RET. ARG. ZINTX,ZINTY, for element IE
C6  INT.CALL  CARAC2,SAVGEO
C9  Jan. 99   S. GRILLI, INLN
C9            adapetd for FMA, C.Fochesato, CMLA, 2003
CLBILMAT SUB. WHICH COMPUTES THE MATRIX Kul AND Kql OF IE FOR THE 3D-BEM
C-----------------------------------------------------------------------

      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

C
      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*4),IFACE(MOMM),
     .                ISUB(MOMM),INTG(MOMM),LXYZ(NOMM,2),IDUM3(36),
     .                JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),INODF(6,2),
     .                IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/ DUM3(NINTM),WEIGTR(NINTM),DUM4(4*NINTM),
     .               DUM5(2*NINTM*NINTM),DUM6(4*NINTM*NINTM*MNOD)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM7(6),
     .                ZNP(3),ZJACP
      COMMON /FSCUB/ DUM8(4*NDM*NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),XYZPR(NINTM,NINTM,3),
     .               ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
C
      DIMENSION ZINTQX(MNODM,MP+1,MP+1),ZINTQY(MNODM,MP+1,MP+1),
     .          ZINTUX(MNODM,MP+1,MP+1),ZINTUY(MNODM,MP+1,MP+1),
     .		ZMYCST(MP+1,MP+1),XX(3)
      DIMENSION ZLEG1(MP+1,MP+1)
      DIMENSION ZLEG2(MP+1,MP+1)
C
      SAVE
C
      DATA ZERO/0.D0/,S64/64.D0/,PII4/0.7957747154594768D-01/,
     .     ONE/1.D0/,TWO/2.D0/
C
C.....Initialisation
C
      NNODM = NNODE*NNODE
      DO N=0,MP
         DO M=0,MP
            DO J=1,NNODM
               ZINTQX(J,N+1,M+1) = ZERO
               ZINTQY(J,N+1,M+1) = ZERO
               ZINTUX(J,N+1,M+1) = ZERO
               ZINTUY(J,N+1,M+1) = ZERO
            END DO
         END DO
      END DO
C
C.....Regular integration
C
      DO IP=1,NINTR
         DO JP=1,NINTR
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
               DFIX(J) = DFIXIP(J,IP,JP)
               DFIE(J) = DFIETP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Save geometry in common INTEG
C
            DO J=1,3
               XYZPR(IP,JP,J) = XYZP(J)
               ZNPR(IP,JP,J)  = ZNP(J)
            END DO
            ZJACPR(IP,JP) = ZJACP
C
C           Final Gauss Jacobian
C
            ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
            ZJACP = PII4*ZJACP
C
C	    distance from origin to current integ. pt.
C	    in both cartesian and spherical coord.
C
            XOX = XYZP(1) - OX
            XOY = XYZP(2) - OY
            XOZ = XYZP(3) - OZ
            ZOR = DSQRT(XOX*XOX+XOY*XOY+XOZ*XOZ)
            ZOA = DACOS(XOZ/ZOR)
            ZOB = DATAN2(XOY,XOX)
C
C           Dirichlet condition
C
	    COA=DCOS(ZOA)
	    COB=DCOS(ZOB)
	    SIA=DSIN(ZOA)
	    SIB=DSIN(ZOB)
	    COSIZN1=COB*SIA*ZNP(1)+SIB*SIA*ZNP(2)+COA*ZNP(3)
	    COSIZN2=COB*COA*ZNP(1)+SIB*COA*ZNP(2)-SIA*ZNP(3)
	    COSIZN3=-SIB*ZNP(1)+COB*ZNP(2)
C
	    DO N=1,MPM+1
               DO M=1,MPM+1
	          ZLEG1(N,M) = ZERO
                  ZLEG2(N,M) = ZERO
               END DO
            END DO
C
C 	    compute the Legendre polynomials
C
	    CALL LEGEN(ZLEG1,COA,MP)
C           ----------
C
C           compute modified Legendre polynomials such that ZLEG2=ZLEG1*SIA
C
	    CALL LEGEN2(ZLEG2,COA,MP)
C           -----------
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      RTERM=ONE
	      DO N=1,MP
	        ZINTQX(J,N+1,1)=ZINTQX(J,N+1,1)+(
     .          DFLOAT(N)*RTERM*ZLEG1(N+1,1)*COSIZN1
     .         +RTERM*ZLEG1(N+1,2)*COSIZN2
     .		                                )*ZJACP*FI(J)
	        DO M=1,N-1
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG2(N+1,M+1)
     .					*ZJACP*FI(J)
	         ZINTQX(J,N+1,M+1)=ZINTQX(J,N+1,M+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(M)*ZOB)*COSIZN1
     .         +DFLOAT(M)*TEMP*COA*DCOS(DFLOAT(M)*ZOB)*COSIZN2
     .         +RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DCOS(DFLOAT(M)*ZOB)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DSIN(DFLOAT(M)*ZOB)*COSIZN3
	         ZINTQY(J,N+1,M+1)=ZINTQY(J,N+1,M+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(M)*ZOB)*COSIZN1
     .         -DFLOAT(M)*TEMP*COA*DSIN(DFLOAT(M)*ZOB)*COSIZN2
     .         -RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DSIN(DFLOAT(M)*ZOB)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DCOS(DFLOAT(M)*ZOB)*COSIZN3
  	        END DO
	        TEMP=RTERM*ZMYCST(N+1,N+1)*ZLEG2(N+1,N+1)
     .					*ZJACP*FI(J)
	        ZINTQX(J,N+1,N+1)=ZINTQX(J,N+1,N+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(N)*ZOB)*COSIZN1
     .         +DFLOAT(N)*TEMP*COA*DCOS(DFLOAT(N)*ZOB)*COSIZN2
     .         -DFLOAT(N)*TEMP*DSIN(DFLOAT(N)*ZOB)*COSIZN3
	        ZINTQY(J,N+1,N+1)=ZINTQY(J,N+1,N+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(N)*ZOB)*COSIZN1
     .         -DFLOAT(N)*TEMP*COA*DSIN(DFLOAT(N)*ZOB)*COSIZN2
     .         -DFLOAT(N)*TEMP*DCOS(DFLOAT(N)*ZOB)*COSIZN3
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
C           Neumann condition
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      ZINTUX(J,1,1)=ZINTUX(J,1,1)+ZJACP*FI(J)
	      RTERM=ZOR
	      DO N=1,MP
	        ZINTUX(J,N+1,1)=ZINTUX(J,N+1,1)
     .                         +RTERM*ZLEG1(N+1,1)*ZJACP*FI(J)
	        DO M=1,N
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG1(N+1,M+1)*ZJACP*FI(J)
	         ZINTUX(J,N+1,M+1)=ZINTUX(J,N+1,M+1)
     .                            +TEMP*DCOS(DFLOAT(M)*ZOB)
	         ZINTUY(J,N+1,M+1)=ZINTUY(J,N+1,M+1)
     .                            -TEMP*DSIN(DFLOAT(M)*ZOB)
  	        END DO
	        RTERM=RTERM*ZOR
	      END DO
            END DO
         END DO
      END DO
C
C.....Save for later use of geometric and intrinsic data
C
C      CALL SAVGEO
C     -----------
C
      RETURN
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE BILSYM(ZINTQX,ZINTQY,ZINTUX,ZINTUY,
     .                  ZINTQXS,ZINTQYS,ZINTUXS,ZINTUYS,
     .                  IE,OX,OY,OZ,MP,ZMYCST)
C-----------------------------------------------------------------------
C1  BILFMAF   BILSYM
C1  PURPOSE   Compute the integrations for the multipoles
C1            and the symmetrical in y or flat bottom
C2  CALL      CALL BILFMA(ZINTQX,ZINTQY,ZINTUX,ZINTUY,
C2     .                  ZINTQX,ZINTQY,ZINTUX,ZINTUY,
C2     .                  IE,OX,OY,OZ,MP,ZMYCST)
C3  CALL ARG. XYZNOD(NOMM,3)      = Coordinates of discretization nodes
C3            LXYZ(NOM)           = Addresses of possible double nodes
C3            NOM,NOMM            = Number of nodes and max nb. of nodes
C3            NINTR               = Nb. of reg. and sing. int. pts of IE
C3            WEIGTR              = Regular integr. weights
C3            IP,JP               = current integration point of IE
C3            XYZLO1(MNODM,3)     = MII nodes for IE
C3            NODE1(NNODM)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = 4-nodes for IE
C3            NODE2(NNODM)        = Nodes of 4-node element IE
C3            FIRIP(NNODM,NINTM,.)= Reg. shape fcts. and   )  At all
C3            DFIXIP(),DXIETP()   = their derivatives      )  points IP,JP
C3            XYZPR(NINTM,.,3)    = All integr. point coordinates
C3            ZNPR(NINTM,.,3)     = All integr. point normal vector
C3            ZJACPR(NINTM,NINTM) = All integr. point jacobian
C3            FIS12(),FIS23()     = Sing. shape fcts. and  )  At all
C3            DFIX12(),DFIX23()   = their derivatives      )  points IP,JP
C3            DFIE12(),DFIE23()   = their derivatives      )  and 4-nodes
C3            FI(NNODM)           = Shape functions and    )  At point
C3            DFIX(NNODM),DFIE()  = their derivatives      )  IP,JP
C3            XYZP(3)             = Current integr. point coordinates
C3            ZNP(3)              = Current integr. point normal vector
C3            ZJACP               = Current integr. point jacobian
C3            IE                  = Current elt
C3            OX,OY,OZ            = Center of current cell
C3            MP                  = Nb of multipoles
C3            ZMYCST(MP,MP)        = Precomputed cst for multipoles
C4  RET. ARG. ZINTX,ZINTY, for element IE
C6  INT.CALL  CARAC2,SAVGEO
C9  Jan. 99   S. GRILLI, INLN
C9            adapetd for FMA, C.Fochesato, CMLA, 2004
CLBILMAT SUB. WHICH COMPUTES THE MATRIX Kul AND Kql OF IE FOR THE 3D-BEM
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'
C
      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*4),IFACE(MOMM),
     .                ISUB(MOMM),INTG(MOMM),LXYZ(NOMM,2),IDUM3(36),
     .                JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),INODF(6,2),
     .                IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/ DUM3(NINTM),WEIGTR(NINTM),DUM4(4*NINTM),
     .               DUM5(2*NINTM*NINTM),DUM6(4*NINTM*NINTM*MNOD)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM7(6),
     .                ZNP(3),ZJACP
      COMMON /FSCUB/ DUM8(4*NDM*NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),XYZPR(NINTM,NINTM,3),
     .               ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
      COMMON /OPTION/ ISYM,IPOST,IVECT
      COMMON /BOTFCT/ DUMBOT(NXM*NYM+2),IBOT
C
      DIMENSION ZINTQX(MNODM,MP+1,MP+1),ZINTQY(MNODM,MP+1,MP+1),
     .          ZINTUX(MNODM,MP+1,MP+1),ZINTUY(MNODM,MP+1,MP+1),
     .		ZINTQXS(MNODM,MP+1,MP+1),ZINTQYS(MNODM,MP+1,MP+1),
     .          ZINTUXS(MNODM,MP+1,MP+1),ZINTUYS(MNODM,MP+1,MP+1),
     .		ZMYCST(MP+1,MP+1),XX(3)
      DIMENSION ZLEG1(MP+1,MP+1)
      DIMENSION ZLEG2(MP+1,MP+1)
C
      SAVE
C
      DATA ZERO/0.D0/,S64/64.D0/,PII4/0.7957747154594768D-01/,
     .     ONE/1.D0/,TWO/2.D0/
C
C.....Initialisation
C
      NNODM = NNODE*NNODE
      DO N=0,MP
         DO M=0,MP
            DO J=1,NNODM
               ZINTQX(J,N+1,M+1) = ZERO
               ZINTQY(J,N+1,M+1) = ZERO
               ZINTUX(J,N+1,M+1) = ZERO
               ZINTUY(J,N+1,M+1) = ZERO
               ZINTQXS(J,N+1,M+1) = ZERO
               ZINTQYS(J,N+1,M+1) = ZERO
               ZINTUXS(J,N+1,M+1) = ZERO
               ZINTUYS(J,N+1,M+1) = ZERO
            END DO
         END DO
      END DO
C
C.....Regular integration
C
      DO IP=1,NINTR
         DO JP=1,NINTR
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
               DFIX(J) = DFIXIP(J,IP,JP)
               DFIE(J) = DFIETP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Save geometry in common INTEG
C
            DO J=1,3
               XYZPR(IP,JP,J) = XYZP(J)
               ZNPR(IP,JP,J)  = ZNP(J)
            END DO
            ZJACPR(IP,JP) = ZJACP
C
C           Final Gauss Jacobian
C
            ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
            ZJACP = PII4*ZJACP
C
C	    distance from origin to current integ. pt.
C	    in both cartesian and spherical coord.
C
            XOX = XYZP(1) - OX
            XOY = XYZP(2) - OY
            XOZ = XYZP(3) - OZ
            ZOR = DSQRT(XOX*XOX+XOY*XOY+XOZ*XOZ)
            ZOA = DACOS(XOZ/ZOR)
            ZOB = DATAN2(XOY,XOX)
C
C           Dirichlet condition
C
	    COA=DCOS(ZOA)
	    COB=DCOS(ZOB)
	    SIA=DSIN(ZOA)
	    SIB=DSIN(ZOB)
	    COSIZN1=COB*SIA*ZNP(1)+SIB*SIA*ZNP(2)+COA*ZNP(3)
	    COSIZN2=COB*COA*ZNP(1)+SIB*COA*ZNP(2)-SIA*ZNP(3)
	    COSIZN3=-SIB*ZNP(1)+COB*ZNP(2)
C
	    DO N=1,MPM+1
               DO M=1,MPM+1
	          ZLEG1(N,M) = ZERO
                  ZLEG2(N,M) = ZERO
               END DO
            END DO
C
C 	    compute the Legendre polynomials
C
	    CALL LEGEN(ZLEG1,COA,MP)
C           ----------
C
C           compute modified Legendre polynomials such that ZLEG2=ZLEG1*SIA
C
	    CALL LEGEN2(ZLEG2,COA,MP)
C           -----------
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      RTERM=ONE
	      DO N=1,MP
	        ZINTQX(J,N+1,1)=ZINTQX(J,N+1,1)+(
     .          DFLOAT(N)*RTERM*ZLEG1(N+1,1)*COSIZN1
     .         +RTERM*ZLEG1(N+1,2)*COSIZN2
     .		                                )*ZJACP*FI(J)
	        DO M=1,N-1
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG2(N+1,M+1)
     .					*ZJACP*FI(J)
	         ZINTQX(J,N+1,M+1)=ZINTQX(J,N+1,M+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(M)*ZOB)*COSIZN1
     .         +DFLOAT(M)*TEMP*COA*DCOS(DFLOAT(M)*ZOB)*COSIZN2
     .         +RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DCOS(DFLOAT(M)*ZOB)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DSIN(DFLOAT(M)*ZOB)*COSIZN3
	         ZINTQY(J,N+1,M+1)=ZINTQY(J,N+1,M+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(M)*ZOB)*COSIZN1
     .         -DFLOAT(M)*TEMP*COA*DSIN(DFLOAT(M)*ZOB)*COSIZN2
     .         -RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DSIN(DFLOAT(M)*ZOB)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DCOS(DFLOAT(M)*ZOB)*COSIZN3
  	        END DO
	        TEMP=RTERM*ZMYCST(N+1,N+1)*ZLEG2(N+1,N+1)
     .					*ZJACP*FI(J)
	        ZINTQX(J,N+1,N+1)=ZINTQX(J,N+1,N+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(N)*ZOB)*COSIZN1
     .         +DFLOAT(N)*TEMP*COA*DCOS(DFLOAT(N)*ZOB)*COSIZN2
     .         -DFLOAT(N)*TEMP*DSIN(DFLOAT(N)*ZOB)*COSIZN3
	        ZINTQY(J,N+1,N+1)=ZINTQY(J,N+1,N+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(N)*ZOB)*COSIZN1
     .         -DFLOAT(N)*TEMP*COA*DSIN(DFLOAT(N)*ZOB)*COSIZN2
     .         -DFLOAT(N)*TEMP*DCOS(DFLOAT(N)*ZOB)*COSIZN3
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
C           Neumann condition
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      ZINTUX(J,1,1)=ZINTUX(J,1,1)+ZJACP*FI(J)
	      RTERM=ZOR
	      DO N=1,MP
	        ZINTUX(J,N+1,1)=ZINTUX(J,N+1,1)
     .                         +RTERM*ZLEG1(N+1,1)*ZJACP*FI(J)
	        DO M=1,N
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG1(N+1,M+1)*ZJACP*FI(J)
	         ZINTUX(J,N+1,M+1)=ZINTUX(J,N+1,M+1)
     .                            +TEMP*DCOS(DFLOAT(M)*ZOB)
	         ZINTUY(J,N+1,M+1)=ZINTUY(J,N+1,M+1)
     .                            -TEMP*DSIN(DFLOAT(M)*ZOB)
  	        END DO
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
CSYM       and the symmetrical contribution
C
C
            IF (ISYM.EQ.1) THEN
	      ZOB = - ZOB
              COSIZN3 = - COSIZN3
            END IF
            IF (IBOT.EQ.10) THEN
  	      COA = - COA
	      COSIZN2 = -COSIZN2
C
	      DO N=1,MPM+1
                 DO M=1,MPM+1
	            IF (MOD(N+M,2).NE.0) THEN
	              ZLEG1(N,M) = -ZLEG1(N,M)
                      ZLEG2(N,M) = -ZLEG2(N,M)
		    END IF
                 END DO
              END DO
            END IF
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      RTERM=ONE
	      DO N=1,MP
	        ZINTQXS(J,N+1,1)=ZINTQXS(J,N+1,1)+(
     .          DFLOAT(N)*RTERM*ZLEG1(N+1,1)*COSIZN1
     .         +RTERM*ZLEG1(N+1,2)*COSIZN2
     .		                                )*ZJACP*FI(J)
	        DO M=1,N-1
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG2(N+1,M+1)
     .					*ZJACP*FI(J)
	         ZINTQXS(J,N+1,M+1)=ZINTQXS(J,N+1,M+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(M)*ZOB)*COSIZN1
     .         +DFLOAT(M)*TEMP*COA*DCOS(DFLOAT(M)*ZOB)*COSIZN2
     .         +RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DCOS(DFLOAT(M)*ZOB)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DSIN(DFLOAT(M)*ZOB)*COSIZN3
	         ZINTQYS(J,N+1,M+1)=ZINTQYS(J,N+1,M+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(M)*ZOB)*COSIZN1
     .         -DFLOAT(M)*TEMP*COA*DSIN(DFLOAT(M)*ZOB)*COSIZN2
     .         -RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DSIN(DFLOAT(M)*ZOB)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DCOS(DFLOAT(M)*ZOB)*COSIZN3
  	        END DO
	        TEMP=RTERM*ZMYCST(N+1,N+1)*ZLEG2(N+1,N+1)
     .					*ZJACP*FI(J)
	        ZINTQXS(J,N+1,N+1)=ZINTQXS(J,N+1,N+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(N)*ZOB)*COSIZN1
     .         +DFLOAT(N)*TEMP*COA*DCOS(DFLOAT(N)*ZOB)*COSIZN2
     .         -DFLOAT(N)*TEMP*DSIN(DFLOAT(N)*ZOB)*COSIZN3
	        ZINTQYS(J,N+1,N+1)=ZINTQYS(J,N+1,N+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(N)*ZOB)*COSIZN1
     .         -DFLOAT(N)*TEMP*COA*DSIN(DFLOAT(N)*ZOB)*COSIZN2
     .         -DFLOAT(N)*TEMP*DCOS(DFLOAT(N)*ZOB)*COSIZN3
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
C           Neumann condition
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      ZINTUXS(J,1,1)=ZINTUXS(J,1,1)+ZJACP*FI(J)
	      RTERM=ZOR
	      DO N=1,MP
	        ZINTUXS(J,N+1,1)=ZINTUXS(J,N+1,1)
     .                         +RTERM*ZLEG1(N+1,1)*ZJACP*FI(J)
	        DO M=1,N
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG1(N+1,M+1)*ZJACP*FI(J)
	         ZINTUXS(J,N+1,M+1)=ZINTUXS(J,N+1,M+1)
     .                            +TEMP*DCOS(DFLOAT(M)*ZOB)
	         ZINTUYS(J,N+1,M+1)=ZINTUYS(J,N+1,M+1)
     .                            -TEMP*DSIN(DFLOAT(M)*ZOB)
  	        END DO
	        RTERM=RTERM*ZOR
	      END DO
            END DO
         END DO
      END DO
C
C.....Save for later use of geometric and intrinsic data
C
C      CALL SAVGEO
C     -----------
C
      RETURN
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE BILSYM2(ZINTQX,ZINTQY,ZINTUX,ZINTUY,
     .                  ZINTQXS,ZINTQYS,ZINTUXS,ZINTUYS,
     .                  ZINTQXS2,ZINTQYS2,ZINTUXS2,ZINTUYS2,
     .                  ZINTQXS3,ZINTQYS3,ZINTUXS3,ZINTUYS3,
     .                  IE,OX,OY,OZ,MP,ZMYCST)
C-----------------------------------------------------------------------
C1  BILFMAF   BILSYM
C1  PURPOSE   Compute the integrations for the multipoles
C1            and the symmetrical in y + flat bottom
C2  CALL      CALL BILFMA(ZINTQX,ZINTQY,ZINTUX,ZINTUY,
C2     .                  ZINTQX,ZINTQY,ZINTUX,ZINTUY,
C2     .                  IE,OX,OY,OZ,MP,ZMYCST)
C3  CALL ARG. XYZNOD(NOMM,3)      = Coordinates of discretization nodes
C3            LXYZ(NOM)           = Addresses of possible double nodes
C3            NOM,NOMM            = Number of nodes and max nb. of nodes
C3            NINTR               = Nb. of reg. and sing. int. pts of IE
C3            WEIGTR              = Regular integr. weights
C3            IP,JP               = current integration point of IE
C3            XYZLO1(MNODM,3)     = MII nodes for IE
C3            NODE1(NNODM)        = Nodes of MII element IE
C3            XYZLO2(NNODE,3)     = 4-nodes for IE
C3            NODE2(NNODM)        = Nodes of 4-node element IE
C3            FIRIP(NNODM,NINTM,.)= Reg. shape fcts. and   )  At all
C3            DFIXIP(),DXIETP()   = their derivatives      )  points IP,JP
C3            XYZPR(NINTM,.,3)    = All integr. point coordinates
C3            ZNPR(NINTM,.,3)     = All integr. point normal vector
C3            ZJACPR(NINTM,NINTM) = All integr. point jacobian
C3            FIS12(),FIS23()     = Sing. shape fcts. and  )  At all
C3            DFIX12(),DFIX23()   = their derivatives      )  points IP,JP
C3            DFIE12(),DFIE23()   = their derivatives      )  and 4-nodes
C3            FI(NNODM)           = Shape functions and    )  At point
C3            DFIX(NNODM),DFIE()  = their derivatives      )  IP,JP
C3            XYZP(3)             = Current integr. point coordinates
C3            ZNP(3)              = Current integr. point normal vector
C3            ZJACP               = Current integr. point jacobian
C3            IE                  = Current elt
C3            OX,OY,OZ            = Center of current cell
C3            MP                  = Nb of multipoles
C3            ZMYCST(MP,MP)        = Precomputed cst for multipoles
C4  RET. ARG. ZINTX,ZINTY, for element IE
C6  INT.CALL  CARAC2,SAVGEO
C9  Jan. 99   S. GRILLI, INLN
C9            adapetd for FMA, C.Fochesato, CMLA, 2004
CLBILMAT SUB. WHICH COMPUTES THE MATRIX Kul AND Kql OF IE FOR THE 3D-BEM
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

      INCLUDE 'param.inc'

C
      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),DUM2(2*MOMM+18),
     .                IDUM2(MOMM*MNOD*MNOD+MOMM*4),IFACE(MOMM),
     .                ISUB(MOMM),INTG(MOMM),LXYZ(NOMM,2),IDUM3(36),
     .                JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),INODF(6,2),
     .                IBCOND(6)
      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
     .                NSIDE
      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
      COMMON /INTEG/ DUM3(NINTM),WEIGTR(NINTM),DUM4(4*NINTM),
     .               DUM5(2*NINTM*NINTM),DUM6(4*NINTM*NINTM*MNOD)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM7(6),
     .                ZNP(3),ZJACP
      COMMON /FSCUB/ DUM8(4*NDM*NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),XYZPR(NINTM,NINTM,3),
     .               ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION ZINTQX(MNODM,MP+1,MP+1),ZINTQY(MNODM,MP+1,MP+1),
     .          ZINTUX(MNODM,MP+1,MP+1),ZINTUY(MNODM,MP+1,MP+1),
     .		ZINTQXS(MNODM,MP+1,MP+1),ZINTQYS(MNODM,MP+1,MP+1),
     .          ZINTUXS(MNODM,MP+1,MP+1),ZINTUYS(MNODM,MP+1,MP+1),
     .		ZINTQXS2(MNODM,MP+1,MP+1),ZINTQYS2(MNODM,MP+1,MP+1),
     .          ZINTUXS2(MNODM,MP+1,MP+1),ZINTUYS2(MNODM,MP+1,MP+1),
     .		ZINTQXS3(MNODM,MP+1,MP+1),ZINTQYS3(MNODM,MP+1,MP+1),
     .          ZINTUXS3(MNODM,MP+1,MP+1),ZINTUYS3(MNODM,MP+1,MP+1),
     .		ZMYCST(MP+1,MP+1),XX(3)
      DIMENSION ZLEG1(MP+1,MP+1)
      DIMENSION ZLEG2(MP+1,MP+1)
C
      SAVE
C
      DATA ZERO/0.D0/,S64/64.D0/,PII4/0.7957747154594768D-01/,
     .     ONE/1.D0/,TWO/2.D0/
C
C.....Initialisation
C
      NNODM = NNODE*NNODE
      DO N=0,MP
         DO M=0,MP
            DO J=1,NNODM
               ZINTQX(J,N+1,M+1) = ZERO
               ZINTQY(J,N+1,M+1) = ZERO
               ZINTUX(J,N+1,M+1) = ZERO
               ZINTUY(J,N+1,M+1) = ZERO
               ZINTQXS(J,N+1,M+1) = ZERO
               ZINTQYS(J,N+1,M+1) = ZERO
               ZINTUXS(J,N+1,M+1) = ZERO
               ZINTUYS(J,N+1,M+1) = ZERO
               ZINTQXS2(J,N+1,M+1) = ZERO
               ZINTQYS2(J,N+1,M+1) = ZERO
               ZINTUXS2(J,N+1,M+1) = ZERO
               ZINTUYS2(J,N+1,M+1) = ZERO
               ZINTQXS3(J,N+1,M+1) = ZERO
               ZINTQYS3(J,N+1,M+1) = ZERO
               ZINTUXS3(J,N+1,M+1) = ZERO
               ZINTUYS3(J,N+1,M+1) = ZERO
            END DO
         END DO
      END DO
C
C.....Regular integration
C
      DO IP=1,NINTR
         DO JP=1,NINTR
C
C        ...Gauss integration point intrins. coord. and weight
C
            DO J=1,NNODM
               FI(J)   = FIRIP(J,IP,JP)
               DFIX(J) = DFIXIP(J,IP,JP)
               DFIE(J) = DFIETP(J,IP,JP)
            END DO
C
C        ...Gauss integration point cartes. coord., unit vectors and Jacob.
C
            CALL CARAC2
C           -----------
C
C           Save geometry in common INTEG
C
            DO J=1,3
               XYZPR(IP,JP,J) = XYZP(J)
               ZNPR(IP,JP,J)  = ZNP(J)
            END DO
            ZJACPR(IP,JP) = ZJACP
C
C           Final Gauss Jacobian
C
            ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
            ZJACP = PII4*ZJACP
C
C	    distance from origin to current integ. pt.
C	    in both cartesian and spherical coord.
C
            XOX = XYZP(1) - OX
            XOY = XYZP(2) - OY
            XOZ = XYZP(3) - OZ
            ZOR = DSQRT(XOX*XOX+XOY*XOY+XOZ*XOZ)
            ZOA = DACOS(XOZ/ZOR)
            ZOB = DATAN2(XOY,XOX)
C
C           Dirichlet condition
C
	    COA=DCOS(ZOA)
	    COB=DCOS(ZOB)
	    SIA=DSIN(ZOA)
	    SIB=DSIN(ZOB)
	    COSIZN1=COB*SIA*ZNP(1)+SIB*SIA*ZNP(2)+COA*ZNP(3)
	    COSIZN2=COB*COA*ZNP(1)+SIB*COA*ZNP(2)-SIA*ZNP(3)
	    COSIZN3=-SIB*ZNP(1)+COB*ZNP(2)
C
	    DO N=1,MPM+1
               DO M=1,MPM+1
	          ZLEG1(N,M) = ZERO
                  ZLEG2(N,M) = ZERO
               END DO
            END DO
C
C 	    compute the Legendre polynomials
C
	    CALL LEGEN(ZLEG1,COA,MP)
C           ----------
C
C           compute modified Legendre polynomials such that ZLEG2=ZLEG1*SIA
C
	    CALL LEGEN2(ZLEG2,COA,MP)
C           -----------
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      RTERM=ONE
	      DO N=1,MP
	        ZINTQX(J,N+1,1)=ZINTQX(J,N+1,1)+(
     .          DFLOAT(N)*RTERM*ZLEG1(N+1,1)*COSIZN1
     .         +RTERM*ZLEG1(N+1,2)*COSIZN2
     .		                                )*ZJACP*FI(J)
	        DO M=1,N-1
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG2(N+1,M+1)
     .					*ZJACP*FI(J)
	         ZINTQX(J,N+1,M+1)=ZINTQX(J,N+1,M+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(M)*ZOB)*COSIZN1
     .         +DFLOAT(M)*TEMP*COA*DCOS(DFLOAT(M)*ZOB)*COSIZN2
     .         +RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DCOS(DFLOAT(M)*ZOB)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DSIN(DFLOAT(M)*ZOB)*COSIZN3
	         ZINTQY(J,N+1,M+1)=ZINTQY(J,N+1,M+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(M)*ZOB)*COSIZN1
     .         -DFLOAT(M)*TEMP*COA*DSIN(DFLOAT(M)*ZOB)*COSIZN2
     .         -RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DSIN(DFLOAT(M)*ZOB)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DCOS(DFLOAT(M)*ZOB)*COSIZN3
  	        END DO
	        TEMP=RTERM*ZMYCST(N+1,N+1)*ZLEG2(N+1,N+1)
     .					*ZJACP*FI(J)
	        ZINTQX(J,N+1,N+1)=ZINTQX(J,N+1,N+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(N)*ZOB)*COSIZN1
     .         +DFLOAT(N)*TEMP*COA*DCOS(DFLOAT(N)*ZOB)*COSIZN2
     .         -DFLOAT(N)*TEMP*DSIN(DFLOAT(N)*ZOB)*COSIZN3
	        ZINTQY(J,N+1,N+1)=ZINTQY(J,N+1,N+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(N)*ZOB)*COSIZN1
     .         -DFLOAT(N)*TEMP*COA*DSIN(DFLOAT(N)*ZOB)*COSIZN2
     .         -DFLOAT(N)*TEMP*DCOS(DFLOAT(N)*ZOB)*COSIZN3
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
C           Neumann condition
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      ZINTUX(J,1,1)=ZINTUX(J,1,1)+ZJACP*FI(J)
	      RTERM=ZOR
	      DO N=1,MP
	        ZINTUX(J,N+1,1)=ZINTUX(J,N+1,1)
     .                         +RTERM*ZLEG1(N+1,1)*ZJACP*FI(J)
	        DO M=1,N
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG1(N+1,M+1)*ZJACP*FI(J)
	         ZINTUX(J,N+1,M+1)=ZINTUX(J,N+1,M+1)
     .                            +TEMP*DCOS(DFLOAT(M)*ZOB)
	         ZINTUY(J,N+1,M+1)=ZINTUY(J,N+1,M+1)
     .                            -TEMP*DSIN(DFLOAT(M)*ZOB)
  	        END DO
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
CSYM       and the symmetrical contribution in y
C
C
            ZOBS = - ZOB
            COSIZN3S = - COSIZN3
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      RTERM=ONE
	      DO N=1,MP
	        ZINTQXS(J,N+1,1)=ZINTQXS(J,N+1,1)+(
     .          DFLOAT(N)*RTERM*ZLEG1(N+1,1)*COSIZN1
     .         +RTERM*ZLEG1(N+1,2)*COSIZN2
     .		                                )*ZJACP*FI(J)
	        DO M=1,N-1
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG2(N+1,M+1)
     .					*ZJACP*FI(J)
	         ZINTQXS(J,N+1,M+1)=ZINTQXS(J,N+1,M+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(M)*ZOBS)*COSIZN1
     .         +DFLOAT(M)*TEMP*COA*DCOS(DFLOAT(M)*ZOBS)*COSIZN2
     .         +RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DCOS(DFLOAT(M)*ZOBS)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DSIN(DFLOAT(M)*ZOBS)*COSIZN3S
	         ZINTQYS(J,N+1,M+1)=ZINTQYS(J,N+1,M+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(M)*ZOBS)*COSIZN1
     .         -DFLOAT(M)*TEMP*COA*DSIN(DFLOAT(M)*ZOBS)*COSIZN2
     .         -RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DSIN(DFLOAT(M)*ZOBS)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DCOS(DFLOAT(M)*ZOBS)*COSIZN3S
  	        END DO
	        TEMP=RTERM*ZMYCST(N+1,N+1)*ZLEG2(N+1,N+1)
     .					*ZJACP*FI(J)
	        ZINTQXS(J,N+1,N+1)=ZINTQXS(J,N+1,N+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(N)*ZOBS)*COSIZN1
     .         +DFLOAT(N)*TEMP*COA*DCOS(DFLOAT(N)*ZOBS)*COSIZN2
     .         -DFLOAT(N)*TEMP*DSIN(DFLOAT(N)*ZOBS)*COSIZN3S
	        ZINTQYS(J,N+1,N+1)=ZINTQYS(J,N+1,N+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(N)*ZOBS)*COSIZN1
     .         -DFLOAT(N)*TEMP*COA*DSIN(DFLOAT(N)*ZOBS)*COSIZN2
     .         -DFLOAT(N)*TEMP*DCOS(DFLOAT(N)*ZOBS)*COSIZN3S
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
C           Neumann condition
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      ZINTUXS(J,1,1)=ZINTUXS(J,1,1)+ZJACP*FI(J)
	      RTERM=ZOR
	      DO N=1,MP
	        ZINTUXS(J,N+1,1)=ZINTUXS(J,N+1,1)
     .                         +RTERM*ZLEG1(N+1,1)*ZJACP*FI(J)
	        DO M=1,N
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG1(N+1,M+1)*ZJACP*FI(J)
	         ZINTUXS(J,N+1,M+1)=ZINTUXS(J,N+1,M+1)
     .                            +TEMP*DCOS(DFLOAT(M)*ZOBS)
	         ZINTUYS(J,N+1,M+1)=ZINTUYS(J,N+1,M+1)
     .                            -TEMP*DSIN(DFLOAT(M)*ZOBS)
  	        END DO
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
CSYM       and the symmetrical contribution for flat bottom
C
C
  	    COA = - COA
	    COSIZN2 = -COSIZN2
C
	    DO N=1,MPM+1
               DO M=1,MPM+1
	          IF (MOD(N+M,2).NE.0) THEN
	            ZLEG1(N,M) = -ZLEG1(N,M)
                    ZLEG2(N,M) = -ZLEG2(N,M)
		  END IF
               END DO
            END DO
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      RTERM=ONE
	      DO N=1,MP
	        ZINTQXS2(J,N+1,1)=ZINTQXS2(J,N+1,1)+(
     .          DFLOAT(N)*RTERM*ZLEG1(N+1,1)*COSIZN1
     .         +RTERM*ZLEG1(N+1,2)*COSIZN2
     .		                                )*ZJACP*FI(J)
	        DO M=1,N-1
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG2(N+1,M+1)
     .					*ZJACP*FI(J)
	         ZINTQXS2(J,N+1,M+1)=ZINTQXS2(J,N+1,M+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(M)*ZOB)*COSIZN1
     .         +DFLOAT(M)*TEMP*COA*DCOS(DFLOAT(M)*ZOB)*COSIZN2
     .         +RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DCOS(DFLOAT(M)*ZOB)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DSIN(DFLOAT(M)*ZOB)*COSIZN3
	         ZINTQYS2(J,N+1,M+1)=ZINTQYS2(J,N+1,M+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(M)*ZOB)*COSIZN1
     .         -DFLOAT(M)*TEMP*COA*DSIN(DFLOAT(M)*ZOB)*COSIZN2
     .         -RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DSIN(DFLOAT(M)*ZOB)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DCOS(DFLOAT(M)*ZOB)*COSIZN3
  	        END DO
	        TEMP=RTERM*ZMYCST(N+1,N+1)*ZLEG2(N+1,N+1)
     .					*ZJACP*FI(J)
	        ZINTQXS2(J,N+1,N+1)=ZINTQXS2(J,N+1,N+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(N)*ZOB)*COSIZN1
     .         +DFLOAT(N)*TEMP*COA*DCOS(DFLOAT(N)*ZOB)*COSIZN2
     .         -DFLOAT(N)*TEMP*DSIN(DFLOAT(N)*ZOB)*COSIZN3
	        ZINTQYS2(J,N+1,N+1)=ZINTQYS2(J,N+1,N+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(N)*ZOB)*COSIZN1
     .         -DFLOAT(N)*TEMP*COA*DSIN(DFLOAT(N)*ZOB)*COSIZN2
     .         -DFLOAT(N)*TEMP*DCOS(DFLOAT(N)*ZOB)*COSIZN3
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
C           Neumann condition
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      ZINTUXS2(J,1,1)=ZINTUXS2(J,1,1)+ZJACP*FI(J)
	      RTERM=ZOR
	      DO N=1,MP
	        ZINTUXS2(J,N+1,1)=ZINTUXS2(J,N+1,1)
     .                         +RTERM*ZLEG1(N+1,1)*ZJACP*FI(J)
	        DO M=1,N
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG1(N+1,M+1)*ZJACP*FI(J)
	         ZINTUXS2(J,N+1,M+1)=ZINTUXS2(J,N+1,M+1)
     .                            +TEMP*DCOS(DFLOAT(M)*ZOB)
	         ZINTUYS2(J,N+1,M+1)=ZINTUYS2(J,N+1,M+1)
     .                            -TEMP*DSIN(DFLOAT(M)*ZOB)
  	        END DO
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
CSYM       and the symmetrical contribution for flat bottom+y
C
C
C deja           ZOBS = - ZOB
C fait           COSIZN3S = - COSIZN3
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      RTERM=ONE
	      DO N=1,MP
	        ZINTQXS3(J,N+1,1)=ZINTQXS3(J,N+1,1)+(
     .          DFLOAT(N)*RTERM*ZLEG1(N+1,1)*COSIZN1
     .         +RTERM*ZLEG1(N+1,2)*COSIZN2
     .		                                )*ZJACP*FI(J)
	        DO M=1,N-1
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG2(N+1,M+1)
     .					*ZJACP*FI(J)
	         ZINTQXS3(J,N+1,M+1)=ZINTQXS3(J,N+1,M+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(M)*ZOBS)*COSIZN1
     .         +DFLOAT(M)*TEMP*COA*DCOS(DFLOAT(M)*ZOBS)*COSIZN2
     .         +RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DCOS(DFLOAT(M)*ZOBS)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DSIN(DFLOAT(M)*ZOBS)*COSIZN3S
	         ZINTQYS3(J,N+1,M+1)=ZINTQYS3(J,N+1,M+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(M)*ZOBS)*COSIZN1
     .         -DFLOAT(M)*TEMP*COA*DSIN(DFLOAT(M)*ZOBS)*COSIZN2
     .         -RTERM*DSQRT(DFLOAT((N-M)*(N+M+1)))
     .          *ZMYCST(N+1,M+2)*ZLEG1(N+1,M+2)*DSIN(DFLOAT(M)*ZOBS)
     .					*COSIZN2*ZJACP*FI(J)
     .             -DFLOAT(M)*TEMP*DCOS(DFLOAT(M)*ZOBS)*COSIZN3S
  	        END DO
	        TEMP=RTERM*ZMYCST(N+1,N+1)*ZLEG2(N+1,N+1)
     .					*ZJACP*FI(J)
	        ZINTQXS3(J,N+1,N+1)=ZINTQXS3(J,N+1,N+1)
     .         +DFLOAT(N)*TEMP*SIA*DCOS(DFLOAT(N)*ZOBS)*COSIZN1
     .         +DFLOAT(N)*TEMP*COA*DCOS(DFLOAT(N)*ZOBS)*COSIZN2
     .         -DFLOAT(N)*TEMP*DSIN(DFLOAT(N)*ZOBS)*COSIZN3S
	        ZINTQYS3(J,N+1,N+1)=ZINTQYS3(J,N+1,N+1)
     .         -DFLOAT(N)*TEMP*SIA*DSIN(DFLOAT(N)*ZOBS)*COSIZN1
     .         -DFLOAT(N)*TEMP*COA*DSIN(DFLOAT(N)*ZOBS)*COSIZN2
     .         -DFLOAT(N)*TEMP*DCOS(DFLOAT(N)*ZOBS)*COSIZN3S
	        RTERM=RTERM*ZOR
	      END DO
            END DO
C
C           Neumann condition
C
            DO J=1,NNODM
C
C  	      for n=0 and m=0, then n>0 and m=0
C	      and finally n>0 and m>0
C
	      ZINTUXS3(J,1,1)=ZINTUXS3(J,1,1)+ZJACP*FI(J)
	      RTERM=ZOR
	      DO N=1,MP
	        ZINTUXS3(J,N+1,1)=ZINTUXS3(J,N+1,1)
     .                         +RTERM*ZLEG1(N+1,1)*ZJACP*FI(J)
	        DO M=1,N
	         TEMP=RTERM*ZMYCST(N+1,M+1)*ZLEG1(N+1,M+1)*ZJACP*FI(J)
	         ZINTUXS3(J,N+1,M+1)=ZINTUXS3(J,N+1,M+1)
     .                            +TEMP*DCOS(DFLOAT(M)*ZOBS)
	         ZINTUYS3(J,N+1,M+1)=ZINTUYS3(J,N+1,M+1)
     .                            -TEMP*DSIN(DFLOAT(M)*ZOBS)
  	        END DO
	        RTERM=RTERM*ZOR
	      END DO
            END DO
         END DO
      END DO
C
C.....Save for later use of geometric and intrinsic data
C
C      CALL SAVGEO
C     -----------
C
      RETURN
C
      END
