c$$$C     
c$$$C     OLD BEM ANALYSIS FILE
c$$$C     BEMK,BILMAT,BIDMAT not used in this FMA version
c$$$C     
c$$$C-----------------------------------------------------------------------
c$$$      SUBROUTINE BEMK(ZKG,ZKUG,ZKQG,ZKUL,ZKQL,LSUB,ALMAX,IO1,IVECT)
c$$$C-----------------------------------------------------------------------
c$$$C     0  BEMKF     BEMK
c$$$C     1  PURPOSE   Compute all the Ku and Kq, peforms rigid mode on Kq,
c$$$C     1            assembles the system matrix K and impose double-node
c$$$C     1            compatibility to it, according to boundary conditions
c$$$C     ,
c$$$C     1            for the 3D-BEM, and introduce c into Kq
c$$$C     1
c$$$C     3  CALL ARG. XYZNOD(NOM)         = Coordinates of discretization
c$$$C     nodes
c$$$C     3            XICON(MOM,2)        = xo,eta for MII elements
c$$$C     3            IGCON(MOM,4,4)      = MII elts connectivity
c$$$C     3            ILCON(MOM,4)        = 4-node elt. connectivity
c$$$C     3            IFACE(MOM)          = Face code for each element
c$$$C     3            ISUB(MOMM)          = Max subdivisions for elts.
c$$$C     3            INTG(MOMM)          = Nb. of integ. points per elt.
c$$$C     3            LSUB(NOM)           = Subdivision status for element
c$$$C     IE
c$$$C     3            LXYZI(NOM,2)        = Addresses of double/triple
c$$$C     nodes
c$$$C     3            JSLIM(6)            = Record limits for saving in
c$$$C     /SAVEF/
c$$$C     3            NELEF(6),NNODF(6)   = Nbs. of elts/nodes for each
c$$$C     face
c$$$C     3            IELEF(6,2),INODF()  = Begin/ending node/elemt Nbs.
c$$$C     for face
c$$$C     3            IBCOND(6)           = Boundary condition type for
c$$$C     each face
c$$$C     3            ZKQL(NOM,NNODE)     = Local Kq matrix of element IE
c$$$C     3            ZKUL(NOM,NNODE)     = Local Ku matrix of element IE
c$$$C     3            ZKQG(NOM,NOM)       = Global Kq matrix
c$$$C     3            ZKUG(NOM,NOM)       = Global Ku matrix
c$$$C     3            ZKG (NOM,NOM)       = Global system matrix
c$$$C     3            NOM,MOM             = Discr. nb. of nodes and
c$$$C     elements
c$$$C     3            NOMM,MOMM           = Discr. max nb. of nodes and
c$$$C     elts.
c$$$C     3            NNODM               = Discr. max nb. of nodes per
c$$$C     element
c$$$C     3            ALMAX [0,PI/2]      = Limit angle for adapt.
c$$$C     integration
c$$$C     3            XYZLO1(NNODM,3)     = XYZ-coordinates of element IE
c$$$C     MII nodes
c$$$C     3            NODE1(NNODE)        = Nodes of MII element IE
c$$$C     3            XYZLO2(NNODE,3)     = XYZ-coordinates of element IE 4
c$$$C     nodes
c$$$C     3            NODE2(NNODE)        = Nodes of 4-node element IE
c$$$C     3            NSUBM               = Max nb. of subdivisions for IE
c$$$C     (0-8)
c$$$C     3            NINTR               = Nb. of regular and singular
c$$$C     int. pts
c$$$C     3            ETARP()             = Regular integr. points
c$$$C     3            WEIGTR()            = Regular integr. weights
c$$$C     3            ZMAX                = largest diag. element of ZKG
c$$$C     3            INCOND(NOMM)        = Boundary condition types for
c$$$C     all nodes
c$$$C     3            INFACE(NOMM)        = Face types for all nodes
c$$$C     4
c$$$C     4  RET. ARG. ZKG,ZKUG,ZKQG,FATINT,JSLIM,ZMAX
c$$$C     6
c$$$C     6  INT.CALL  GAUSSP,BILMAT,BISMAT,ASSEML,ASSEMK,DBNODK,INTRCI
c$$$C     ,ERRORS
c$$$C     6            SHAPFN,SHAPD1
c$$$C     E  ERRORS    01= Insufficient length of saving buffer
c$$$C     10 Jan. 99   S. Grilli at INLN
c$$$C-----------------------------------------------------------------------
c$$$C     
c$$$      include 'param.inc'
c$$$C     
c$$$      IMPLICIT REAL (A-H,O-Z)
c$$$C     
c$$$      CHARACTER*8 TEXTE
c$$$C     
c$$$      COMMON /DATGEN/ DUM1(5),MX,MY,MZ,IDUM1(5),NOM,MOM
c$$$      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),
c$$$     .     DUM3(18),IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
c$$$     .     IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
c$$$     .     LXYZ(NOMM,2),IDUM2(36),JSLIM(6),NELEF(6),
c$$$     .     NNODF(6),IELEF(6,2),INODF(6,2),IBCOND(6)
c$$$      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
c$$$      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
c$$$     .     NSIDE
c$$$      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
c$$$      COMMON /INTEG/  ETARP(NINTM),WEIGTR(NINTM),PHIP12(NINTM),
c$$$     .     PHIP23(NINTM),RMIP12(NINTM),RMIP23(NINTM),
c$$$     .     RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
c$$$     .     XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
c$$$     .     ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
c$$$      COMMON /FSCUB/ FN0XI(NDM,NINTM),FN0ET(NDM,NINTM),
c$$$     .     FN1XI(NDM,NINTM),FN1ET(NDM,NINTM),
c$$$     .     FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
c$$$     .     DFIETP(MNODM,NINTM,NINTM),DUM5(7*NINTM*NINTM)
c$$$      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
c$$$     .     FIS23(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIX12(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIX23(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIE12(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIE23(MNODM,NINTM,NINTM,MNOD)
c$$$      COMMON /SAVEF/ FATINT(LSAVEF)
c$$$C     
c$$$      DIMENSION LSUB(NOM),ZKUL(NOMM,MNODM),ZKQL(NOMM,MNODM),
c$$$     .     ZKG(NOMM,NOM),ZKUG(NOMM,NOM),ZKQG(NOMM,NOM),
c$$$     .     FSF(NDM),FSD(NDM),FSFX12(NDM),FSFE12(NDM),
c$$$     .     FSDX12(NDM),FSDE12(NDM),FSFX23(NDM),FSFE23(NDM),
c$$$     .     FSDX23(NDM),FSDE23(NDM),XELT(MNOD,2)
c$$$C     
c$$$      SAVE
c$$$C     
c$$$      DATA TEXTE/'BEMK01'/,ZERO/0.D0/,ONE/1.D0/,TWO/2.D0/,THREE/3.D0/,
c$$$     .     PIS8/0.3926990816987241D0/,PIS2/1.570796326794896D0/,
c$$$     .     OTHREE/0.3333333333333333D0/,S3/0.3333333333333333D0/,
c$$$     .     XELT/-1.D0,1.D0,1.D0,-1.D0,-1.D0,-1.D0,1.D0,1.D0/,
c$$$     .     HALF/0.5D0/
c$$$C     
c$$$C=====Initialize the system matrices, and adressing variables
c$$$C     
c$$$      DO J=1,NOM
c$$$         DO I=1,NOM
c$$$            ZKQG(I,J) = ZERO
c$$$            ZKUG(I,J) = ZERO
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$      JSAVE  = 0
c$$$      NINTPR = 0
c$$$      NSIDEP = 0
c$$$      EFJXIP = ZERO
c$$$      EFJETP = ZERO
c$$$      NNODE  = MNOD
c$$$C     
c$$$      REWIND(IO1)
c$$$C     
c$$$C=====Loop on the elements, local computations, assembling
c$$$C     
c$$$      DO IE=1,MOM
c$$$C     
c$$$C     ...Local values assigned to element IE
c$$$C     
c$$$         NINTR = INTG(IE)
c$$$         NSUBM = ISUB(IE)
c$$$         NSIDE = IFACE(IE)
c$$$C     
c$$$C     4-node connectivity in NODE1(NDM)
c$$$C     
c$$$         DO J=1,NNODE
c$$$            NODE2(J)         = ILCON(IE,J)
c$$$            XYZLO2(J,1)      = XYZNOD(NODE2(J),1)
c$$$            XYZLO2(J,2)      = XYZNOD(NODE2(J),2)
c$$$            XYZLO2(J,3)      = XYZNOD(NODE2(J),3)
c$$$            INCOND(NODE2(J)) = IBCOND(IFACE(IE))
c$$$            INFACE(NODE2(J)) = IFACE(IE)
c$$$         END DO
c$$$C     
c$$$C     Assign NOD1E(NDM) and NNODE for correct assembly of ZK(U/Q)L
c$$$C     i.e., considering MII elements
c$$$C     
c$$$         ILOC = 0
c$$$         DO J=1,NNODE
c$$$            DO I=1,NNODE
c$$$               ILOC = ILOC + 1
c$$$               NODE1(ILOC)    = IGCON(IE,I,J)
c$$$               XYZLO1(ILOC,1) = XYZNOD(NODE1(ILOC),1)
c$$$               XYZLO1(ILOC,2) = XYZNOD(NODE1(ILOC),2)
c$$$               XYZLO1(ILOC,3) = XYZNOD(NODE1(ILOC),3)
c$$$C     SYM
c$$$               IF (ISYM.EQ.1) THEN
c$$$                  IF (NSIDE.EQ.1.AND.
c$$$     .                 IE.GT.(IELEF(NSIDE,2)-MX).AND.
c$$$     .                 J.EQ.NNODE) THEN
c$$$                     XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
c$$$                  END IF
c$$$                  IF (NSIDE.EQ.6.AND.
c$$$     .                 IE.LT.(IELEF(NSIDE,1)+MX).AND.
c$$$     .                 J.EQ.1) THEN
c$$$                     XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
c$$$                  END IF
c$$$                  IF (NSIDE.EQ.4.AND.
c$$$     .                 MOD(IE-IELEF(NSIDE,1)+1,MY).EQ.0.AND.
c$$$     .                 I.EQ.NNODE) THEN
c$$$                     XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
c$$$                  END IF
c$$$                  IF (NSIDE.EQ.2.AND.
c$$$     .                 MOD(IE-IELEF(NSIDE,1),MY).EQ.0.AND.
c$$$     .                 I.EQ.1) THEN
c$$$                     XYZLO1(ILOC,2) = -XYZLO1(ILOC,2)
c$$$                  END IF
c$$$               END IF
c$$$C     MYS
c$$$            END DO
c$$$         END DO
c$$$C     
c$$$         EFJXI = XICON(IE,1)
c$$$         EFJET = XICON(IE,2)
c$$$C     
c$$$C     ...Filling up of the common INTEG when the elements have different
c$$$C     node or integration points numbers (accelerator)
c$$$C     
c$$$         IRECAL = 1
c$$$         IF (NINTR.NE.NINTPR) THEN
c$$$C     
c$$$C     Regular Gauss points
c$$$C     
c$$$            CALL GAUSSP
c$$$C     -----------
c$$$C     
c$$$C     Singular integrals characteristics
c$$$C     
c$$$            DO IP = 1,NINTR
c$$$               PHIP12(IP) = PIS8*(ONE   + ETARP(IP))
c$$$               PHIP23(IP) = PIS8*(THREE + ETARP(IP))
c$$$               RMIP12(IP) = TWO/DCOS(PHIP12(IP))
c$$$               RMIP23(IP) = TWO/DSIN(PHIP23(IP))
c$$$C     
c$$$               DO JP=1,NINTR
c$$$                  RIJP12(IP,JP) = HALF*RMIP12(IP)*(ONE + ETARP(JP))
c$$$                  RIJP23(IP,JP) = HALF*RMIP23(IP)*(ONE + ETARP(JP))
c$$$C     
c$$$                  DO I=1,NNODE
c$$$                     XI12(IP,JP,I)  = XELT(I,1) + RIJP12(IP,JP)*
c$$$     .                    DCOS(PHIP12(IP) + (I-1)*PIS2)
c$$$                     XI23(IP,JP,I)  = XELT(I,1) + RIJP23(IP,JP)*
c$$$     .                    DCOS(PHIP23(IP) + (I-1)*PIS2)
c$$$                     ET12(IP,JP,I)  = XELT(I,2) + RIJP12(IP,JP)*
c$$$     .                    DSIN(PHIP12(IP) + (I-1)*PIS2)
c$$$                     ET23(IP,JP,I)  = XELT(I,2) + RIJP23(IP,JP)*
c$$$     .                    DSIN(PHIP23(IP) + (I-1)*PIS2)
c$$$                  END DO
c$$$               END DO
c$$$            END DO
c$$$C     
c$$$            IRECAL = 0
c$$$         END IF
c$$$C     
c$$$C     MII cubic 4-node shape functions
c$$$C     Compute shape functions and their derivatives for
c$$$C     cubic element on the boundary and store in common FSCUB
c$$$C     
c$$$         IF(EFJXI.NE.EFJXIP.OR.NINTR.NE.NINTPR) THEN
c$$$C     
c$$$C     XI direction
c$$$C     
c$$$            IRECAL = 0
c$$$            DO IP=1,NINTR
c$$$               CHI = (ETARP(IP) + ONE)*OTHREE + EFJXI
c$$$               CALL SHAPFN(CHI,FSF,NNODE)
c$$$C     -----------
c$$$               CALL SHAPD1(CHI,FSD,NNODE)
c$$$C     -----------
c$$$               DO J=1,NNODE
c$$$                  FN0XI(J,IP) = FSF(J)
c$$$                  FN1XI(J,IP) = OTHREE*FSD(J)
c$$$               END DO
c$$$            END DO
c$$$         END IF
c$$$         IF(EFJET.NE.EFJETP.OR.NINTR.NE.NINTPR) THEN
c$$$C     
c$$$C     ETA direction
c$$$C     
c$$$            IRECAL = 0
c$$$            DO JP=1,NINTR
c$$$               CHI = (ETARP(JP) + ONE)*OTHREE + EFJET
c$$$               CALL SHAPFN(CHI,FSF,NNODE)
c$$$C     -----------
c$$$               CALL SHAPD1(CHI,FSD,NNODE)
c$$$C     -----------
c$$$               DO J=1,NNODE
c$$$                  FN0ET(J,JP) = FSF(J)
c$$$                  FN1ET(J,JP) = OTHREE*FSD(J)
c$$$               END DO
c$$$            END DO
c$$$         END IF
c$$$C     
c$$$C     Calculates bidim shape functions at all reg/sing integ. points
c$$$C     
c$$$         IF(IRECAL.EQ.0) THEN
c$$$            ILOC = 0
c$$$            DO J=1,NNODE
c$$$               DO I=1,NNODE
c$$$                  ILOC = ILOC + 1
c$$$                  DO IP=1,NINTR
c$$$                     DO JP=1,NINTR
c$$$C     
c$$$C     Shape functions at all regular integ. points
c$$$C     
c$$$                        FIRIP(ILOC,IP,JP)  = FN0XI(I,IP)*FN0ET(J,JP)
c$$$                        DFIXIP(ILOC,IP,JP) = FN1XI(I,IP)*FN0ET(J,JP)
c$$$                        DFIETP(ILOC,IP,JP) = FN0XI(I,IP)*FN1ET(J,JP)
c$$$                     END DO
c$$$                  END DO
c$$$               END DO
c$$$            END DO
c$$$C     
c$$$C     Shape functions at all singular integ. points
c$$$C     
c$$$            DO IP=1,NINTR
c$$$               DO JP=1,NINTR
c$$$                  DO L=1,NNODE
c$$$                     CHX12 = (XI12(IP,JP,L) + ONE)*OTHREE + EFJXI
c$$$                     CHX23 = (XI23(IP,JP,L) + ONE)*OTHREE + EFJXI
c$$$                     CHE12 = (ET12(IP,JP,L) + ONE)*OTHREE + EFJET
c$$$                     CHE23 = (ET23(IP,JP,L) + ONE)*OTHREE + EFJET
c$$$C     
c$$$                     CALL SHAPFN(CHX12,FSFX12,NNODE)
c$$$C     -----------
c$$$                     CALL SHAPD1(CHX12,FSDX12,NNODE)
c$$$C     -----------
c$$$                     CALL SHAPFN(CHE12,FSFE12,NNODE)
c$$$C     -----------
c$$$                     CALL SHAPD1(CHE12,FSDE12,NNODE)
c$$$C     -----------
c$$$                     CALL SHAPFN(CHX23,FSFX23,NNODE)
c$$$C     -----------
c$$$                     CALL SHAPD1(CHX23,FSDX23,NNODE)
c$$$C     -----------
c$$$                     CALL SHAPFN(CHE23,FSFE23,NNODE)
c$$$C     -----------
c$$$                     CALL SHAPD1(CHE23,FSDE23,NNODE)
c$$$C     -----------
c$$$                     ILOC = 0
c$$$                     DO J=1,NNODE
c$$$                        DO I=1,NNODE
c$$$                           ILOC = ILOC + 1
c$$$                           FIS12(ILOC,IP,JP,L)  = FSFX12(I)*FSFE12(J)
c$$$                           DFIX12(ILOC,IP,JP,L) = S3*FSDX12(I)*FSFE12(J)
c$$$                           DFIE12(ILOC,IP,JP,L) = S3*FSFX12(I)*FSDE12(J)
c$$$                           FIS23(ILOC,IP,JP,L)  = FSFX23(I)*FSFE23(J)
c$$$                           DFIX23(ILOC,IP,JP,L) = S3*FSDX23(I)*FSFE23(J)
c$$$                           DFIE23(ILOC,IP,JP,L) = S3*FSFX23(I)*FSDE23(J)
c$$$                        END DO
c$$$                     END DO
c$$$                  END DO
c$$$               END DO
c$$$            END DO
c$$$         END IF
c$$$C     
c$$$C     ...Computation of local matrices, save geometry and shape
c$$$C     functions in common SAVEF, beginning at JSAVE (change for
c$$$C     no subdivisions or subdivision integrals)
c$$$C     
c$$$         IF(NSUBM.EQ.0) THEN
c$$$            CALL BILMAT(ZKQL,ZKUL)
c$$$C     ----------
c$$$         ELSE
c$$$            CALL BIDMAT(ZKQL,ZKUL,LSUB,ALMAX,IE)
c$$$C     -----------
c$$$         END IF
c$$$C     
c$$$C     ...Global assembling
c$$$C     
c$$$         CALL ASSEML(ZKQG,ZKUG,ZKQL,ZKUL,NOMM)
c$$$C     -----------
c$$$C     
c$$$C     ...Update the address for saving in FATINT
c$$$C     
c$$$         IF(NSIDE.NE.NSIDEP) THEN
c$$$            JSLIM(NSIDE) = JSAVE
c$$$         END IF
c$$$C     
c$$$         LREC  = NINTR*NINTR*(7+3*NNODE*NNODE)
c$$$         JSAVE = JSAVE + LREC
c$$$C     
c$$$         DO I=1,LREC
c$$$            WRITE(IO1) FATINT(I)
c$$$         END DO
c$$$C     WRITE(IO1) (FATINT(I),I=1,LREC)
c$$$C     
c$$$         NINTPR = NINTR
c$$$         NSIDEP = NSIDE
c$$$         EFJXIP = EFJXI
c$$$         EFJETP = EFJET
c$$$      END DO
c$$$C     
c$$$C=====Application of the rigid mode technique to ZKQG
c$$$C     
c$$$      CALL INTRCI(ZKQG,NOMM)
c$$$C     -----------
c$$$C     
c$$$C==== Assembling of the general system matrix ZKG
c$$$C     
c$$$      CALL ASSEMK(ZKG,ZKUG,ZKQG)
c$$$C     -----------
c$$$C     
c$$$C==== Double node compatibility on the system matrix
c$$$C     
c$$$      CALL DBNODK(ZKG)
c$$$C     -----------
c$$$C     
c$$$      RETURN
c$$$C     
c$$$ 2000 FORMAT(3I5,6F10.5)
c$$$C     
c$$$      END
c$$$C-----------------------------------------------------------------------
c$$$      SUBROUTINE BILMAT(ZKQL,ZKUL)
c$$$C-----------------------------------------------------------------------
c$$$C     1  BILMATF   BILMAT
c$$$C     1  PURPOSE   Compute the local Kul and Kql to the elemt IE and
c$$$C     save
c$$$C     1            geometric and intrinsic data for the 3D-BEM (no elt.
c$$$C     subdi)
c$$$C     2  CALL      CALL BILMAT(ZKQL,ZKUL)
c$$$C     3  CALL ARG. XYZNOD(NOMM,3)      = Coordinates of discretization
c$$$C     nodes
c$$$C     3            LXYZ(NOM)           = Addresses of possible double
c$$$C     nodes
c$$$C     3            NOM,NOMM            = Number of nodes and max nb. of
c$$$C     nodes
c$$$C     3            ZKQL(NOM,NNODE)     = Local Kq matrix of element IE
c$$$C     3            ZKUL(NOM,NNODE)     = Local Ku matrix of element IE
c$$$C     3            NINTR               = Nb. of reg. and sing. int. pts
c$$$C     of IE
c$$$C     3            WEIGTR              = Regular integr. weights
c$$$C     3            IP,JP               = current integration point of IE
c$$$C     3            XYZLO1(MNODM,3)     = MII nodes for IE
c$$$C     3            NODE1(NNODM)        = Nodes of MII element IE
c$$$C     3            XYZLO2(NNODE,3)     = 4-nodes for IE
c$$$C     3            NODE2(NNODM)        = Nodes of 4-node element IE
c$$$C     3            FIRIP(NNODM,NINTM,.)= Reg. shape fcts. and   )  At
c$$$C     all
c$$$C     3            DFIXIP(),DXIETP()   = their derivatives      )
c$$$C     points IP,JP
c$$$C     3            XYZPR(NINTM,.,3)    = All integr. point coordinates
c$$$C     3            ZNPR(NINTM,.,3)     = All integr. point normal vector
c$$$C     3            ZJACPR(NINTM,NINTM) = All integr. point jacobian
c$$$C     3            FIS12(),FIS23()     = Sing. shape fcts. and  )  At
c$$$C     all
c$$$C     3            DFIX12(),DFIX23()   = their derivatives      )
c$$$C     points IP,JP
c$$$C     3            DFIE12(),DFIE23()   = their derivatives      )  and 4
c$$$C     -nodes
c$$$C     3            FI(NNODM)           = Shape functions and    )  At
c$$$C     point
c$$$C     3            DFIX(NNODM),DFIE()  = their derivatives      )  IP,JP
c$$$C     3            XYZP(3)             = Current integr. point
c$$$C     coordinates
c$$$C     3            ZNP(3)              = Current integr. point normal
c$$$C     vector
c$$$C     3            ZJACP               = Current integr. point jacobian
c$$$C     4  RET. ARG. ZKUL,ZKQL, for element IE
c$$$C     6  INT.CALL  CARAC2,SAVGEO
c$$$C     9  Jan. 99   S. GRILLI, INLN
c$$$C     LBILMAT SUB. WHICH COMPUTES THE MATRIX Kul AND Kql OF IE FOR THE
c$$$C     3D-BEM
c$$$C-----------------------------------------------------------------------
c$$$C     
c$$$      include 'param.inc'
c$$$C     
c$$$      IMPLICIT REAL (A-H,O-Z)
c$$$C     
c$$$      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,MOM
c$$$      COMMON /MAILLE/ XYZNOD(NOMM,3),DUM2(2*MOMM+18),
c$$$     .     IDUM2(MOMM*MNOD*MNOD+MOMM*4),IFACE(MOMM),
c$$$     .     ISUB(MOMM),INTG(MOMM),LXYZ(NOMM,2),IDUM3(36),
c$$$     .     JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),INODF(6,2),
c$$$     .     IBCOND(6)
c$$$      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
c$$$     .     NSIDE
c$$$      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
c$$$      COMMON /INTEG/ DUM3(NINTM),WEIGTR(NINTM),DUM4(2*NINTM),
c$$$     .     RMIP12(NINTM),RMIP23(NINTM),
c$$$     .     RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
c$$$     .     XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
c$$$     .     ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
c$$$      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM5(6),
c$$$     .     ZNP(3),ZJACP
c$$$      COMMON /FSCUB/ DUM6(4*NDM*NINTM),
c$$$     .     FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
c$$$     .     DFIETP(MNODM,NINTM,NINTM),XYZPR(NINTM,NINTM,3),
c$$$     .     ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
c$$$      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
c$$$     .     FIS23(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIX12(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIX23(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIE12(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIE23(MNODM,NINTM,NINTM,MNOD)
c$$$C     
c$$$      DIMENSION ZKUL(NOMM,MNODM),ZKQL(NOMM,MNODM),XX(3)
c$$$C     
c$$$      SAVE
c$$$C     
c$$$      DATA ZERO/0.D0/,S64/64.D0/,PII4/0.7957747154594768D-01/
c$$$C     
c$$$C.....Initialisation of local matrices
c$$$C     
c$$$      NNODM = NNODE*NNODE
c$$$      DO J=1,NNODM
c$$$         DO I=1,NOM
c$$$            ZKUL(I,J) = ZERO
c$$$            ZKQL(I,J) = ZERO
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$C.....Regular integration
c$$$C     
c$$$      DO IP=1,NINTR
c$$$         DO JP=1,NINTR
c$$$C     
c$$$C     ...Gauss integration point intrins. coord. and weight
c$$$C     
c$$$            DO J=1,NNODM
c$$$               FI(J)   = FIRIP(J,IP,JP)
c$$$               DFIX(J) = DFIXIP(J,IP,JP)
c$$$               DFIE(J) = DFIETP(J,IP,JP)
c$$$            END DO
c$$$C     
c$$$C     ...Gauss integration point cartes. coord., unit vectors and Jacob.
c$$$C     
c$$$            CALL CARAC2
c$$$C     -----------
c$$$C     
c$$$C     Save geometry in common INTEG
c$$$C     
c$$$            DO J=1,3
c$$$               XYZPR(IP,JP,J) = XYZP(J)
c$$$               ZNPR(IP,JP,J)  = ZNP(J)
c$$$            END DO
c$$$            ZJACPR(IP,JP) = ZJACP
c$$$C     
c$$$C     Final Gauss Jacobian
c$$$C     
c$$$            ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
c$$$            CST   = PII4*ZJACP
c$$$C     
c$$$C     ...Computation of ZKUL, ZKQL
c$$$C     
c$$$            DO L=1,NOM
c$$$               R     = ZERO
c$$$               DGNUM = ZERO
c$$$               DO J=1,3
c$$$                  XX(J) = XYZP(J) - XYZNOD(L,J)
c$$$                  DGNUM = DGNUM + XX(J)*ZNP(J)
c$$$                  R     = R + XX(J)*XX(J)
c$$$               END DO
c$$$               R  = DSQRT(R)
c$$$C     
c$$$C     Computation of Green's functions at (IP,JP)
c$$$C     
c$$$               GP    =  CST/R
c$$$               DGDNP = -CST*DGNUM/(R*R*R)
c$$$C     
c$$$C     Kernel transformation, in case of L on 4-node IE
c$$$C     
c$$$               DO I=1,NNODE
c$$$                  IF (L.EQ.NODE2(I)) THEN
c$$$                     GP    = ZERO
c$$$                     DGDNP = ZERO
c$$$                  END IF
c$$$               END DO
c$$$C     
c$$$C     Regular integrals
c$$$C     
c$$$               DO J=1,NNODM
c$$$                  ZKUL(L,J) = ZKUL(L,J) + FI(J)*GP
c$$$                  ZKQL(L,J) = ZKQL(L,J) + FI(J)*DGDNP
c$$$               END DO
c$$$            END DO
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$C.....Save for later use of geometric and intrinsic data
c$$$C     
c$$$      CALL SAVGEO
c$$$C     -----------
c$$$C     
c$$$C.....Singular integrals for ZKUL
c$$$C     
c$$$      DO IP=1,NINTR
c$$$         DO JP=1,NINTR
c$$$            DO I=1,NNODE
c$$$               L = NODE2(I)
c$$$               CS12 = RMIP12(IP)*WEIGTR(IP)*WEIGTR(JP)/
c$$$     .              S64
c$$$               CS23 = RMIP23(IP)*WEIGTR(IP)*WEIGTR(JP)/
c$$$     .              S64
c$$$C     
c$$$C     ...Gauss integration point intrins. coord. and weight for 12
c$$$C     unit vectors and Jacob.
c$$$C     
c$$$               DO J=1,NNODM
c$$$                  FI(J)   = FIS12(J,IP,JP,I)
c$$$                  DFIX(J) = DFIX12(J,IP,JP,I)
c$$$                  DFIE(J) = DFIE12(J,IP,JP,I)
c$$$               END DO
c$$$C     
c$$$               CALL CARAC2
c$$$C     -----------
c$$$C     
c$$$               R     = ZERO
c$$$               DGNUM = ZERO
c$$$               DO J=1,3
c$$$                  XX(J) = XYZP(J) - XYZNOD(L,J)
c$$$                  DGNUM = DGNUM + XX(J)*ZNP(J)
c$$$                  R     = R + XX(J)*XX(J)
c$$$               END DO
c$$$               R     = DSQRT(R)
c$$$               GP    = CS12*ZJACP*RIJP12(IP,JP)/R
c$$$               DGDNP = -CS12*ZJACP*RIJP12(IP,JP)*DGNUM/(R*R*R)
c$$$C     
c$$$               DO J=1,NNODM
c$$$                  ZKUL(L,J) = ZKUL(L,J) + FI(J)*GP
c$$$                  ZKQL(L,J) = ZKQL(L,J) + FI(J)*DGDNP
c$$$               END DO
c$$$C     
c$$$C     ...Gauss integration point intrins. coord. and weight for 23
c$$$C     unit vectors and Jacob.
c$$$C     
c$$$               DO J=1,NNODM
c$$$                  FI(J)   = FIS23(J,IP,JP,I)
c$$$                  DFIX(J) = DFIX23(J,IP,JP,I)
c$$$                  DFIE(J) = DFIE23(J,IP,JP,I)
c$$$               END DO
c$$$C     
c$$$               CALL CARAC2
c$$$C     -----------
c$$$C     
c$$$               R     = ZERO
c$$$               DGNUM = ZERO
c$$$               DO J=1,3
c$$$                  XX(J) = XYZP(J) - XYZNOD(L,J)
c$$$                  DGNUM = DGNUM + XX(J)*ZNP(J)
c$$$                  R     = R + XX(J)*XX(J)
c$$$               END DO
c$$$               R     = DSQRT(R)
c$$$               GP    = CS23*ZJACP*RIJP23(IP,JP)/R
c$$$               DGDNP = -CS23*ZJACP*RIJP23(IP,JP)*DGNUM/(R*R*R)
c$$$C     
c$$$               DO J=1,NNODM
c$$$                  ZKUL(L,J) = ZKUL(L,J) + FI(J)*GP
c$$$                  ZKQL(L,J) = ZKQL(L,J) + FI(J)*DGDNP
c$$$               END DO
c$$$            END DO
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$C.....Double/triple nodes, taking into account for the sing. and part.
c$$$C.....integ.
c$$$C     
c$$$      DO I=1,NNODE
c$$$         L = NODE2(I)
c$$$         DO K=1,2
c$$$            IF(LXYZ(L,K).NE.0) THEN
c$$$               DO J=1,NNODM
c$$$                  ZKUL(LXYZ(L,K),J) = ZKUL(L,J)
c$$$                  ZKQL(LXYZ(L,K),J) = ZKQL(L,J)
c$$$               END DO
c$$$            END IF
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$      RETURN
c$$$C     
c$$$      END
c$$$C-----------------------------------------------------------------------
c$$$      SUBROUTINE BIDMAT(ZKQL,ZKUL,LSUB,ALMAXB,IE)
c$$$C-----------------------------------------------------------------------
c$$$C     1  BIDMATF   BIDMAT
c$$$C     1  PURPOSE   Compute the local Kul and Kql to the elemt IE and
c$$$C     save
c$$$C     1            geometric and intrinsic data for the 3D-BEM (with
c$$$C     elt. subdi)
c$$$C     2  CALL      CALL BIDMAT(ZKQL,ZKUL,LSUB,ALMAX,IE)
c$$$C     3  CALL ARG. XYZNOD(NOMM,3)      = Coordinates of discretization
c$$$C     nodes
c$$$C     3            LXYZ(NOM)           = Addresses of possible double
c$$$C     nodes
c$$$C     3            LSUB(NOM)           = Subdivision vector for IE
c$$$C     3            ALMAXB              = Limit angle for subdivisions
c$$$C     3            NOM,NOMM            = Number of nodes and max nb. of
c$$$C     nodes
c$$$C     3            ZKQL(NOM,NNODE)     = Local Kq matrix of element IE
c$$$C     3            ZKUL(NOM,NNODE)     = Local Ku matrix of element IE
c$$$C     3            NINTR               = Nb. of reg. and sing. int. pts
c$$$C     of IE
c$$$C     3            WEIGTR              = Regular integr. weights
c$$$C     3            IP,JP               = current integration point of IE
c$$$C     3            XYZLO1(MNODM,3)     = MII nodes for IE
c$$$C     3            NODE1(NNODM)        = Nodes of MII element IE
c$$$C     3            XYZLO2(NNODE,3)     = 4-nodes for IE
c$$$C     3            NODE2(NNODM)        = Nodes of 4-node element IE
c$$$C     3            FIRIP(NNODM,NINTM,.)= Reg. shape fcts. and   )  At
c$$$C     all
c$$$C     3            DFIXIP(),DXIETP()   = their derivatives      )
c$$$C     points IP,JP
c$$$C     3            XYZPR(NINTM,.,3)    = All integr. point coordinates
c$$$C     3            ZNPR(NINTM,.,3)     = All integr. point normal vector
c$$$C     3            ZJACPR(NINTM,NINTM) = All integr. point jacobian
c$$$C     3            FIS12(),FIS23()     = Sing. shape fcts. and  )  At
c$$$C     all
c$$$C     3            DFIX12(),DFIX23()   = their derivatives      )
c$$$C     points IP,JP
c$$$C     3            DFIE12(),DFIE23()   = their derivatives      )  and 4
c$$$C     -nodes
c$$$C     3            FI(NNODM)           = Shape functions and    )  At
c$$$C     point
c$$$C     3            DFIX(NNODM),DFIE()  = their derivatives      )  IP,JP
c$$$C     3            XYZP(3)             = Current integr. point
c$$$C     coordinates
c$$$C     3            ZNP(3)              = Current integr. point normal
c$$$C     vector
c$$$C     3            ZJACP               = Current integr. point jacobian
c$$$C     4  RET. ARG. ZKUL,ZKQL, for element IE
c$$$C     6  INT.CALL  CARAC2,SAVGEO
c$$$C     9  Feb. 99   S. GRILLI, INLN
c$$$C     LBIDMAT SUB. WHICH COMPUTES THE MATRIX Kul AND Kql OF IE FOR THE
c$$$C     3D-BEM
c$$$C-----------------------------------------------------------------------
c$$$C     
c$$$      include 'param.inc'
c$$$C     
c$$$      IMPLICIT REAL (A-H,O-Z)
c$$$      LOGICAL IOUT
c$$$C     
c$$$      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,MOM
c$$$      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),DUM2(18),
c$$$     .     IDUM2(MOMM*MNOD*MNOD+MOMM*4),IFACE(MOMM),
c$$$     .     ISUB(MOMM),INTG(MOMM),LXYZ(NOMM,2),IDUM3(36),
c$$$     .     JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),INODF(6,2),
c$$$     .     IBCOND(6)
c$$$      COMMON /DELEM1/ XYZLO1(MNODM,3),NODE1(MNODM),NNODE,NINTR,NSUBM,
c$$$     .     NSIDE
c$$$      COMMON /DELEM2/ XYZLO2(NDM,3),NODE2(NDM)
c$$$      COMMON /INTEG/ ETARP(NINTM),WEIGTR(NINTM),DUM4(2*NINTM),
c$$$     .     RMIP12(NINTM),RMIP23(NINTM),
c$$$     .     RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
c$$$     .     XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
c$$$     .     ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
c$$$      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZP(3),DUM5(6),
c$$$     .     ZNP(3),ZJACP
c$$$      COMMON /FSCUB/ DUM6(4*NDM*NINTM),
c$$$     .     FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
c$$$     .     DFIETP(MNODM,NINTM,NINTM),XYZPR(NINTM,NINTM,3),
c$$$     .     ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
c$$$      COMMON /FSING/ FIS12(MNODM,NINTM,NINTM,MNOD),
c$$$     .     FIS23(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIX12(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIX23(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIE12(MNODM,NINTM,NINTM,MNOD),
c$$$     .     DFIE23(MNODM,NINTM,NINTM,MNOD)
c$$$C     
c$$$      DIMENSION ZKUL(NOMM,MNODM),ZKQL(NOMM,MNODM),LSUB(NOM)
c$$$      DIMENSION XX(3),DIAG(2),XCIE(3),VIE(3,3),V12(3),V13(3),
c$$$     .     FSFX(NDM),FSDX(NDM),FSFE(NDM),FSDE(NDM),DISND(MNOD)
c$$$C     
c$$$      SAVE
c$$$C     
c$$$      DATA ZERO/0.D0/,QUART/0.25D0/,HALF/0.5D0/,ONE/1.0D0/,
c$$$     .     TWO/2.D0/,THREE/3.D0/,FOUR/4.D0/,EIGHT/8.D0/,S64/64.D0/,
c$$$     .     PII4/0.7957747154594768D-01/,DLOG2I/3.321928094D0/,
c$$$     .     CRAT/0.4999D0/,OTHREE/0.3333333333333333D0/,EPS/1.D-08/
c$$$C     
c$$$C.....Initialisation of local matrices
c$$$C     
c$$$      NNODM = NNODE*NNODE
c$$$      DO J=1,NNODM
c$$$         DO I=1,NOM
c$$$            ZKUL(I,J) = ZERO
c$$$            ZKQL(I,J) = ZERO
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$C.....Computation of subdivisions for IE
c$$$C     
c$$$      EFJXI = XICON(IE,1)
c$$$      EFJET = XICON(IE,2)
c$$$      TANA  = DTAN(ALMAXB)
c$$$C     
c$$$C     Preparing for minimum distance computations
c$$$C     
c$$$      DO I=2,NNODE
c$$$         DO J=1,3
c$$$            VIE(I-1,J) = XYZLO2(I,J) - XYZLO2(1,J)
c$$$         END DO
c$$$      END DO
c$$$      V12(1) = VIE(1,2)*VIE(2,3) - VIE(2,2)*VIE(1,3)
c$$$      V12(2) = VIE(1,3)*VIE(2,1) - VIE(2,3)*VIE(1,1)
c$$$      V12(3) = VIE(1,1)*VIE(2,2) - VIE(2,1)*VIE(1,2)
c$$$      V13(1) = VIE(1,2)*VIE(3,3) - VIE(3,2)*VIE(1,3)
c$$$      V13(2) = VIE(1,3)*VIE(3,1) - VIE(3,3)*VIE(1,1)
c$$$      V13(3) = VIE(1,1)*VIE(3,2) - VIE(3,1)*VIE(1,2)
c$$$      ZNV12 = DSQRT(V12(1)*V12(1)+V12(2)*V12(2)+V12(3)*V12(3))
c$$$      ZNV13 = DSQRT(V13(1)*V13(1)+V13(2)*V13(2)+V13(3)*V13(3))
c$$$      DO J=1,3
c$$$         V12(J) = V12(J)/ZNV12
c$$$         V13(J) = V13(J)/ZNV13
c$$$      END DO
c$$$C     
c$$$C     Longest 1/2 diagonal chord in elt. IE
c$$$C     
c$$$      DO K=1,2
c$$$         DIAG(K) = ZERO
c$$$         DO J=1,3
c$$$            DIAG(K) = DIAG(K) + (XYZLO2(K+2,J) - XYZLO2(K,J))**2
c$$$         END DO
c$$$      END DO
c$$$      DIAM = HALF*DSQRT(AMAX1(DIAG(1),DIAG(2)))
c$$$C     
c$$$C     Coordinates of elt. IE center
c$$$C     
c$$$      DO J=1,3
c$$$         XCIE(J) = ZERO
c$$$         DO I=1,NNODE
c$$$            XCIE(J) = XCIE(J) + XYZLO2(I,J)
c$$$         END DO
c$$$         XCIE(J) = QUART*XCIE(J)
c$$$      END DO
c$$$C     
c$$$C     Loop over exterior points
c$$$C     
c$$$      DO ID=1,NOM
c$$$         IOUT = .TRUE.
c$$$         NSUB = 0
c$$$C     
c$$$C     Check if node belongs to element or is a double node
c$$$C     
c$$$         DO J=1,NNODE
c$$$            IF((ID.EQ.NODE2(J)).OR.(LXYZ(ID,1).EQ.NODE2(J)).OR.
c$$$     .           (LXYZ(ID,2).EQ.NODE2(J))) THEN
c$$$               IOUT =.FALSE.
c$$$            END IF
c$$$         END DO
c$$$C     
c$$$C     Subdivision analysis
c$$$C     
c$$$         IF(IOUT) THEN
c$$$C     
c$$$C     Distance of I from elt IE center plus minima d12, d13
c$$$C     
c$$$            DISTCI = ZERO
c$$$            D12    = ZERO
c$$$            D13    = ZERO
c$$$            DO J=1,3
c$$$               DISTCI = DISTCI + (XCIE(J) - XYZNOD(ID,J))**2
c$$$               D12    = D12 + (XYZLO2(1,J) - XYZNOD(ID,J))*V12(J)
c$$$               D13    = D13 + (XYZLO2(1,J) - XYZNOD(ID,J))*V13(J)
c$$$            END DO
c$$$            DISTCI = DSQRT(DISTCI)
c$$$            DMINPP = AMIN1(ABS(D12), ABS(D13))
c$$$C     
c$$$C     Distance of JOI from elt IE 4 nodes
c$$$C     
c$$$            DO I=1,NNODE
c$$$               DISND(I) = ZERO
c$$$               DO J=1,3
c$$$                  DISND(I) = DISND(I) + (XYZLO2(I,J)-XYZNOD(ID,J))**2
c$$$               END DO
c$$$               DISND(I) = DSQRT(DISND(I))
c$$$            END DO
c$$$            DISMI = AMIN1(DISND(1),DISND(2),DISND(3),DISND(4))
c$$$            DISDA = DIAM/DISTCI
c$$$C     
c$$$            DISTB = DSQRT(DISTCI*DISTCI + DMINPP*DMINPP)
c$$$            IF(DISTCI.GT.DISTB) THEN
c$$$               ANGLE = TWO*DATAN2(DIAM*DMINPP,DISTCI*DISTCI)
c$$$            ELSE
c$$$               IF(DMINPP.LT.EPS) THEN
c$$$                  ANGLE = ZERO
c$$$               ELSE
c$$$                  ANGLE = TWO*DATAN2(DIAM,DMINPP)
c$$$               END IF
c$$$            END IF
c$$$C     
c$$$            IF((ANGLE.GT.ALMAXB).AND.(DISDA.GT.ONE)) THEN
c$$$C     
c$$$C     Cas d'un angle obtu avec noeuds proche interieur elt
c$$$C     
c$$$               IF(DMINPP.LT.EPS) THEN
c$$$                  DISDA = DIAM/DISTCI
c$$$               ELSE
c$$$                  DISDA = DIAM/DMINPP
c$$$               END IF
c$$$               NSUB  = INT(LOG10(DISDA/TANA)*DLOG2I + CRAT)
c$$$            ELSE
c$$$               IF(DISMI.LT.DIAM) THEN
c$$$C     
c$$$C     Cas d'un angle aigu avec noeuds alignes en dehors elt
c$$$C     
c$$$                  DISDA = DIAM/(DISMI*TANA)
c$$$                  NSUB = 0
c$$$                  IF((DISDA.GT.TWO).AND.(DISDA.LT.FOUR)) THEN
c$$$                     NSUB = 1
c$$$                  ELSE IF((DISDA.GE.FOUR).AND.(DISDA.LT.EIGHT)) THEN
c$$$                     NSUB = 2
c$$$                  ELSE IF(DISDA.GE.EIGHT) THEN
c$$$                     NSUB = 3
c$$$                  END IF
c$$$               ELSE
c$$$                  NSUB = 0
c$$$               END IF
c$$$            END IF
c$$$C     
c$$$            IF(NSUB.GT.NSUBM) THEN
c$$$               NSUB = NSUBM
c$$$            END IF
c$$$         END IF
c$$$C     
c$$$         LSUB(ID) = NSUB
c$$$      END DO
c$$$C     
c$$$C.....Regular integration
c$$$C     
c$$$      DO IP=1,NINTR
c$$$         DO JP=1,NINTR
c$$$C     
c$$$C     ...Gauss integration point intrins. coord. and weight
c$$$C     
c$$$            DO J=1,NNODM
c$$$               FI(J)   = FIRIP(J,IP,JP)
c$$$               DFIX(J) = DFIXIP(J,IP,JP)
c$$$               DFIE(J) = DFIETP(J,IP,JP)
c$$$            END DO
c$$$C     
c$$$C     ...Gauss integration point cartes. coord., unit vectors and Jacob.
c$$$C     
c$$$            CALL CARAC2
c$$$C     -----------
c$$$C     
c$$$C     Save geometry in common INTEG
c$$$C     
c$$$            DO J=1,3
c$$$               XYZPR(IP,JP,J) = XYZP(J)
c$$$               ZNPR(IP,JP,J)  = ZNP(J)
c$$$            END DO
c$$$            ZJACPR(IP,JP) = ZJACP
c$$$C     
c$$$C     Final Gauss Jacobian
c$$$C     
c$$$            ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
c$$$C     
c$$$C     ...Computation of ZKUL, ZKQL
c$$$C     
c$$$            NSUBP = 0
c$$$            DO L=1,NOM
c$$$C     
c$$$C     Subdivisions for L
c$$$C     
c$$$               NSUB = LSUB(L)
c$$$               NSG  = 2**NSUB
c$$$               NSG2 = NSG*NSG
c$$$C     
c$$$               IF(NSUB.EQ.0) THEN
c$$$                  IF(NSUBP.NE.0) THEN
c$$$                     DO J=1,NNODM
c$$$                        FI(J)   = FIRIP(J,IP,JP)
c$$$                        DFIX(J) = DFIXIP(J,IP,JP)
c$$$                        DFIE(J) = DFIETP(J,IP,JP)
c$$$                     END DO
c$$$C     
c$$$                     DO J=1,3
c$$$                        XYZP(J) = XYZPR(IP,JP,J)
c$$$                        ZNP(J)  = ZNPR(IP,JP,J)
c$$$                     END DO
c$$$C     
c$$$                     ZJACP = ZJACPR(IP,JP)*WEIGTR(IP)*WEIGTR(JP)
c$$$                     NSUBP = 0
c$$$                  END IF
c$$$               ELSE
c$$$                  DSG   = TWO/FLOAT(NSG)
c$$$                  NSUBP = NSUB
c$$$               END IF
c$$$C     
c$$$               DO ISG=1,NSG
c$$$                  IF(NSUB.NE.0) THEN
c$$$                     XILI = TWO*(ISG-1)/FLOAT(NSG) - ONE
c$$$                     XIRI = XILI + DSG
c$$$                     XIM  = HALF*(XILI + XIRI)
c$$$                     XI   = ETARP(IP)/FLOAT(NSG) + XIM
c$$$                     CHI  = (XI + ONE)*OTHREE + EFJXI
c$$$                     CALL SHAPFN(CHI,FSFX,NNODE)
c$$$C     -----------
c$$$                     CALL SHAPD1(CHI,FSDX,NNODE)
c$$$C     -----------
c$$$                  END IF
c$$$                  DO JSG=1,NSG
c$$$                     IF(NSUB.NE.0) THEN
c$$$                        ETLI = TWO*(JSG-1)/FLOAT(NSG) - ONE
c$$$                        ETRI = ETLI + DSG
c$$$                        ETM  = HALF*(ETLI + ETRI)
c$$$                        ETA  = ETARP(JP)/FLOAT(NSG) + ETM
c$$$                        CHI  = (ETA + ONE)*OTHREE + EFJET
c$$$                        CALL SHAPFN(CHI,FSFE,NNODE)
c$$$C     -----------
c$$$                        CALL SHAPD1(CHI,FSDE,NNODE)
c$$$C     -----------
c$$$C     
c$$$C     Shape functions at points IP,JP,ISG,JSG
c$$$C     
c$$$                        ILOC = 0
c$$$                        DO J=1,NNODE
c$$$                           DO I=1,NNODE
c$$$                              ILOC = ILOC + 1
c$$$                              FI(ILOC)   = FSFX(I)*FSFE(J)
c$$$                              DFIX(ILOC) = OTHREE*FSDX(I)*FSFE(J)
c$$$                              DFIE(ILOC) = OTHREE*FSFX(I)*FSDE(J)
c$$$                           END DO
c$$$                        END DO
c$$$C     
c$$$C     Cord., unit vectors and Jacob.
c$$$C     
c$$$                        CALL CARAC2
c$$$C     -----------
c$$$                        ZJACP = ZJACP*WEIGTR(IP)*WEIGTR(JP)
c$$$                     END IF
c$$$C     
c$$$                     CST   = PII4*ZJACP/FLOAT(NSG2)
c$$$C     
c$$$                     R     = ZERO
c$$$                     DGNUM = ZERO
c$$$                     DO J=1,3
c$$$                        XX(J) = XYZP(J) - XYZNOD(L,J)
c$$$                        DGNUM = DGNUM + XX(J)*ZNP(J)
c$$$                        R     = R + XX(J)*XX(J)
c$$$                     END DO
c$$$                     R = DSQRT(R)
c$$$C     
c$$$C     Computation of Green's functions at (IP,JP)
c$$$C     
c$$$                     GP    =  CST/R
c$$$                     DGDNP = -CST*DGNUM/(R*R*R)
c$$$C     
c$$$C     Kernel transformation, in case of L on 4-node IE
c$$$C     
c$$$                     DO I=1,NNODE
c$$$                        IF (L.EQ.NODE2(I)) THEN
c$$$                           GP    = ZERO
c$$$                           DGDNP = ZERO
c$$$                        END IF
c$$$                     END DO
c$$$C     
c$$$C     Regular integrals
c$$$C     
c$$$                     DO J=1,NNODM
c$$$                        ZKUL(L,J) = ZKUL(L,J) + FI(J)*GP
c$$$                        ZKQL(L,J) = ZKQL(L,J) + FI(J)*DGDNP
c$$$                     END DO
c$$$                  END DO
c$$$               END DO
c$$$            END DO
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$C.....Save for later use of geometric and intrinsic data
c$$$C     
c$$$      CALL SAVGEO
c$$$C     -----------
c$$$C     
c$$$C.....Singular integrals for ZKUL,(ZKQL)
c$$$C     
c$$$      DO IP=1,NINTR
c$$$         DO JP=1,NINTR
c$$$            DO I=1,NNODE
c$$$               L = NODE2(I)
c$$$               CS12 = RMIP12(IP)*WEIGTR(IP)*WEIGTR(JP)/
c$$$     .              S64
c$$$               CS23 = RMIP23(IP)*WEIGTR(IP)*WEIGTR(JP)/
c$$$     .              S64
c$$$C     
c$$$C     ...Gauss integration point intrins. coord. and weight for 12
c$$$C     unit vectors and Jacob.
c$$$C     
c$$$               DO J=1,NNODM
c$$$                  FI(J)   = FIS12(J,IP,JP,I)
c$$$                  DFIX(J) = DFIX12(J,IP,JP,I)
c$$$                  DFIE(J) = DFIE12(J,IP,JP,I)
c$$$               END DO
c$$$C     
c$$$               CALL CARAC2
c$$$C     -----------
c$$$C     
c$$$               R     = ZERO
c$$$               DGNUM = ZERO
c$$$               DO J=1,3
c$$$                  XX(J) = XYZP(J) - XYZNOD(L,J)
c$$$                  DGNUM = DGNUM + XX(J)*ZNP(J)
c$$$                  R     = R + XX(J)*XX(J)
c$$$               END DO
c$$$               R     = DSQRT(R)
c$$$               GP    = CS12*ZJACP*RIJP12(IP,JP)/R
c$$$               DGDNP = -CS12*ZJACP*RIJP12(IP,JP)*DGNUM/(R*R*R)
c$$$C     
c$$$               DO J=1,NNODM
c$$$                  ZKUL(L,J) = ZKUL(L,J) + FI(J)*GP
c$$$                  ZKQL(L,J) = ZKQL(L,J) + FI(J)*DGDNP
c$$$               END DO
c$$$C     
c$$$C     ...Gauss integration point intrins. coord. and weight for 23
c$$$C     unit vectors and Jacob.
c$$$C     
c$$$               DO J=1,NNODM
c$$$                  FI(J)   = FIS23(J,IP,JP,I)
c$$$                  DFIX(J) = DFIX23(J,IP,JP,I)
c$$$                  DFIE(J) = DFIE23(J,IP,JP,I)
c$$$               END DO
c$$$C     
c$$$               CALL CARAC2
c$$$C     -----------
c$$$C     
c$$$               R     = ZERO
c$$$               DGNUM = ZERO
c$$$               DO J=1,3
c$$$                  XX(J) = XYZP(J) - XYZNOD(L,J)
c$$$                  DGNUM = DGNUM + XX(J)*ZNP(J)
c$$$                  R     = R + XX(J)*XX(J)
c$$$               END DO
c$$$               R     = DSQRT(R)
c$$$               GP    = CS23*ZJACP*RIJP23(IP,JP)/R
c$$$               DGDNP = -CS23*ZJACP*RIJP23(IP,JP)*DGNUM/(R*R*R)
c$$$C     
c$$$               DO J=1,NNODM
c$$$                  ZKUL(L,J) = ZKUL(L,J) + FI(J)*GP
c$$$                  ZKQL(L,J) = ZKQL(L,J) + FI(J)*DGDNP
c$$$               END DO
c$$$            END DO
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$C.....Double/triple nodes, taking into account for the sing. and part.
c$$$C.....integ.
c$$$C     
c$$$      DO I=1,NNODE
c$$$         L = NODE2(I)
c$$$         DO K=1,2
c$$$            IF(LXYZ(L,K).NE.0) THEN
c$$$               DO J=1,NNODM
c$$$                  ZKUL(LXYZ(L,K),J) = ZKUL(L,J)
c$$$                  ZKQL(LXYZ(L,K),J) = ZKQL(L,J)
c$$$               END DO
c$$$            END IF
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$      RETURN
c$$$C     
c$$$      END
c$$$C----------------------------------------------------------------------
c$$$      SUBROUTINE ASSEML(ZKQG,ZKUG,ZKQL,ZKUL,NOMM2)
c$$$C----------------------------------------------------------------------
c$$$C     0  ASSEMLF   ASSEML
c$$$C     1  PURPOSE   Assemble local Kul and Kql of the elemt IE in the
c$$$C     global
c$$$C     1            matrix Ku and Kq for the 3D-BEM
c$$$C     2  CALL      CALL ASSEML(ZKQG,ZKUG,ZKQL,ZKUL,NOMM)
c$$$C     3  CALL ARG. NOM                 = Discr. nb. of nodes
c$$$C     3            NOMM                = Discr. max nb. of nodes
c$$$C     3            ZKQG(NOM,NOM)       = Global Kq matrix
c$$$C     3            ZKUG(NOM,NOM)       = Global Ku matrix
c$$$C     3            NNODM               = MII elets. number of nodes
c$$$C     3            ZKQL(NOM,NNODM)     = Local Kq matrix of element IE
c$$$C     3            ZKUL(NOM,NNODM)     = Local Ku matrix of element IE
c$$$C     3            NODE1(NNODM)        = Nodes of MII element IE
c$$$C     4  RET. ARG. ZKUG,ZKQG
c$$$C     9  Jan. 99   S. GRILLI, INLN
c$$$C     LASSEML SUB. WHICH ASSEMBLES MATRIX Kul AND Kql OF IE FOR THE 3D
c$$$C     -BEM
c$$$C-----------------------------------------------------------------------
c$$$C     
c$$$      include 'param.inc'
c$$$C     
c$$$      IMPLICIT REAL (A-H,O-Z)
c$$$C     
c$$$      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
c$$$      COMMON /DELEM1/ DUM2(MNODM,3),NODE1(MNODM),NNODE,IDUM3(3)
c$$$C     
c$$$      DIMENSION ZKQG(NOMM2,NOM),ZKUG(NOMM2,NOM),ZKQL(NOMM2,MNODM),
c$$$     .     ZKUL(NOMM2,MNODM)
c$$$C     
c$$$      SAVE
c$$$C     
c$$$      NNODM = NNODE*NNODE
c$$$      DO J=1,NNODM
c$$$         K = NODE1(J)
c$$$         DO I=1,NOM
c$$$            ZKUG(I,K) = ZKUG(I,K) + ZKUL(I,J)
c$$$            ZKQG(I,K) = ZKQG(I,K) + ZKQL(I,J)
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$      RETURN
c$$$C     
c$$$      END
c$$$C----------------------------------------------------------------------
c$$$      SUBROUTINE INTRCI(ZKQG,NOMM)
c$$$C----------------------------------------------------------------------
c$$$C     1  INTRCIF   INTRCI
c$$$C     1  PURPOSE   Introduce c coefficients into Kq from the
c$$$C     1            "rigid mode" method computation.
c$$$C     2  CALL      CALL INTRCI(ZKQG,NOMM)
c$$$C     3  CALL ARG. NOM                 = Discr. nb. of nodes
c$$$C     3            NOMM                = Discr. max nb. of nodes
c$$$C     3            ZKQG(NOM,NOM)       = Global Kq matrix of element IE
c$$$C     4  RET. ARG. ZKQG
c$$$C     9  Jan. 99   S. GRILLI, INLN
c$$$C     LINTRCI SUB. WHICH INTRODUCES C INTO Kq FOR THE 3D-BEM
c$$$C-----------------------------------------------------------------------
c$$$C     
c$$$      IMPLICIT REAL (A-H,O-Z)
c$$$C     
c$$$      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
c$$$C     
c$$$      DIMENSION ZKQG(NOMM,NOM)
c$$$C     
c$$$      SAVE
c$$$C     
c$$$      DATA ZERO/0.D0/
c$$$C     
c$$$      DO I=1,NOM
c$$$         DIAG      = ZERO
c$$$         ZKQG(I,I) = ZERO
c$$$         DO J=1,NOM
c$$$            DIAG = DIAG - ZKQG(I,J)
c$$$         END DO
c$$$         ZKQG(I,I) = DIAG
c$$$      END DO
c$$$C     
c$$$      RETURN
c$$$C     
c$$$      END
c$$$
c$$$C----------------------------------------------------------------------
c$$$      SUBROUTINE ASSEMK(ZKG,ZKUG,ZKQG)
c$$$C----------------------------------------------------------------------
c$$$C     0  ASSEMKF   ASSEMK
c$$$C     1  PURPOSE   Assemble global Ku and Kq in the system matrix K for
c$$$C     the
c$$$C     1            3D-BEM(IBCOND(K)=1: u imposed, IBCOND(K)=<>1: q
c$$$C     imposed)
c$$$C     2  CALL      CALL ASSEMK(ZKG,ZKUG,ZKQG)
c$$$C     3  CALL ARG. NOM                 = Discr. nb. of nodes
c$$$C     3            NOMM                = Discr. max nb. of nodes
c$$$C     3            IBCOND(6)           = Type of B.C. (Dir. or Neum.)
c$$$C     3            INODF(6,2)          = Node limits of each boundary
c$$$C     3            ZKG(NOM,NOM)        = Global K system  matrix
c$$$C     3            ZKQG(NOM,NOM)       = Global Kq matrix
c$$$C     3            ZKUG(NOM,NOM)       = Global Ku matrix
c$$$C     4  RET. ARG. ZKG
c$$$C     9  Jan. 99   S. GRILLI, INLN
c$$$C     LASSEMK SUB. WHICH ASSEMBLES MATRIX Ku AND Kq INTO K FOR THE 3D
c$$$C     -BEM
c$$$C-----------------------------------------------------------------------
c$$$C     
c$$$      include 'param.inc'
c$$$C     
c$$$      IMPLICIT REAL (A-H,O-Z)
c$$$C     
c$$$      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
c$$$      COMMON /MAILLE/ DUM2(NOMM*3+MOMM*2+18),
c$$$     .     IDUM3(MOMM*MNOD*MNOD+MOMM*7+NOMM*2+66),
c$$$     .     INODF(6,2),IBCOND(6)
c$$$C     
c$$$      DIMENSION ZKG(NOMM,NOM),ZKUG(NOMM,NOM),ZKQG(NOMM,NOM)
c$$$C     
c$$$      SAVE
c$$$C     
c$$$C     Loop over the 6 different boundaries
c$$$C     
c$$$      DO K=1,6
c$$$         J1 = INODF(K,1)
c$$$         J2 = INODF(K,2)
c$$$C     
c$$$         IF(IBCOND(K).EQ.1) THEN
c$$$C     
c$$$C     Dirichlet boundary condition
c$$$C     
c$$$            DO J=J1,J2
c$$$               DO I=1,NOM
c$$$                  ZKG(I,J) = -ZKUG(I,J)
c$$$               END DO
c$$$            END DO
c$$$         ELSE
c$$$C     
c$$$C     Neuman boundary condition
c$$$C     
c$$$            DO J=J1,J2
c$$$               DO I=1,NOM
c$$$                  ZKG(I,J) = ZKQG(I,J)
c$$$               END DO
c$$$            END DO
c$$$         END IF
c$$$      END DO
c$$$C     
c$$$      RETURN
c$$$C     
c$$$      END
c$$$C----------------------------------------------------------------------
c$$$      SUBROUTINE DBNODK(ZKG)
c$$$C----------------------------------------------------------------------
c$$$C     0  DBNODKF   DBNODK
c$$$C     1  PURPOSE   Impose double node compatibility in the general ZKG
c$$$C     matrix
c$$$C     1            according what type of B.C. is imposed on the 6
c$$$C     boundaries
c$$$C     1            of 3D-BEM (IBCOND(K)=1: u imposed, IBCOND(K)=2: q
c$$$C     imposed)
c$$$C     2  CALL      CALL DBNODK(ZKG)
c$$$C     3  CALL ARG. NOM                 = Discr. nb. of nodes
c$$$C     3            NOMM                = Discr. max nb. of nodes
c$$$C     3            IBCOND(6)           = Type of B.C. (Dir. or Neum.)
c$$$C     3            ZKG(NOM,NOM)        = Global K system  matrix
c$$$C     3            ZKQG(NOM,NOM)       = Neuman system matrix for
c$$$C     dble.nodes
c$$$C     3            ZMAX                = Max diag. element of ZKG
c$$$C     4  RET. ARG. ZKG,ZMAX
c$$$C     9  Jan. 99   S. GRILLI, INLN
c$$$C     LDBNODK SUB. WHICH IMPOSES DOUBLE NODE COMPATIBILITY FOR THE 3D
c$$$C     -BEM
c$$$C-----------------------------------------------------------------------
c$$$C     
c$$$      include 'param.inc'
c$$$C     
c$$$      IMPLICIT REAL (A-H,O-Z)
c$$$C     
c$$$      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
c$$$      COMMON /MAILLE/ DUM2(NOMM*3+MOMM*2+18),
c$$$     .     IDUM3(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
c$$$     .     IDUM4(84)
c$$$      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
c$$$C     
c$$$      DIMENSION ZKG(NOMM,NOM)
c$$$C     
c$$$      SAVE
c$$$C     
c$$$      DATA ZERO/0.D0/
c$$$C     
c$$$C     Locate maximum diagonal term
c$$$C     
c$$$      ZMAX = ZERO
c$$$C     
c$$$      DO I=1,NOM
c$$$         IF(ABS(ZKG(I,I)).GT.ZMAX) ZMAX = ABS(ZKG(I,I))
c$$$      END DO
c$$$C     
c$$$C     Loop on double/triple nodes
c$$$C     
c$$$      DO I=1,NOM
c$$$C     
c$$$         ID = LXYZ(I,1)
c$$$         IF(ID.NE.0) THEN
c$$$            IFI = INFACE(I)
c$$$            IF(IFI.EQ.1) THEN
c$$$C     
c$$$C     Surface element with double/triple node
c$$$C     
c$$$               DO K=1,2
c$$$C     
c$$$C     D-N on the free surface : u(K)=u(1)
c$$$C     
c$$$                  IR = LXYZ(I,K)
c$$$                  IF(IR.NE.0) THEN
c$$$                     DO J=1,NOM
c$$$                        ZKG(IR,J) = ZERO
c$$$                     END DO
c$$$C     
c$$$                     ZKG(IR,IR) = ZMAX
c$$$                  END IF
c$$$               END DO
c$$$            ELSE
c$$$C     
c$$$C     Neuman faces
c$$$C     
c$$$               IT = LXYZ(I,2)
c$$$               IF(IT.EQ.0.AND.INFACE(ID).GT.IFI) THEN
c$$$C     
c$$$C     Double node not yet treated
c$$$C     
c$$$                  IR = ID
c$$$C     
c$$$C     N-N on the lat. bn. bottom : u(4)=u(K)
c$$$C     
c$$$                  DO J=1,NOM
c$$$                     ZKG(IR,J) = ZERO
c$$$                  END DO
c$$$C     
c$$$                  ZKG(IR,IR) =  ZMAX
c$$$                  ZKG(IR,I)  = -ZMAX
c$$$               ELSE IF(IT.NE.0.AND.IFI.EQ.6) THEN
c$$$C     
c$$$C     Triple node on bottom
c$$$C     N-N on the lat. bn. bottom : u(4)=u(K)
c$$$C     
c$$$                  DO K=1,2
c$$$                     IR = LXYZ(I,K)
c$$$                     DO J=1,NOM
c$$$                        ZKG(IR,J) = ZERO
c$$$                     END DO
c$$$C     
c$$$                     ZKG(IR,IR) =  ZMAX
c$$$                     ZKG(IR,I)  = -ZMAX
c$$$                  END DO
c$$$               END IF
c$$$            END IF
c$$$         END IF
c$$$      END DO
c$$$C     
c$$$      RETURN
c$$$C     
c$$$      END
c$$$C-----------------------------------------------------------------------
c$$$      SUBROUTINE SAVGEO
c$$$C-----------------------------------------------------------------------
c$$$C     1  SAVGEOF   SAVGEO
c$$$C     1  PURPOSE   Save geometry and shape functions of 2D element IE,
c$$$C     for inte-
c$$$C     1            gration points (IP,JP) in SAVEF
c$$$C     2  CALL      CALL SAVGEO(
c$$$C     3  CALL ARG. JSAVE               = Pointer of common SAVEF
c$$$C     3            IP,JP               = current integration point of IE
c$$$C     3            FIRIP(NNODM,IP,JP)  = Shape functions and      ) At
c$$$C     point
c$$$C     3            DFIXIP(),DFIETP()   = their deriv. (Spl. or not) IP
c$$$C     ,JP
c$$$C     3            WEIGTR(NINTR)       = All integr. point integr.
c$$$C     weight
c$$$C     3            XYZPR(IP,JP,3)      = All integr. point coordinates
c$$$C     3            ZNPR(IP,JP,3)       = All integr. point normal vector
c$$$C     3            ZJACPR(IP,JP)       = All integr. point jacobian
c$$$C     4  RET. ARG. FATINT(LSAVEF)  for element IE
c$$$C     9  Jan. 99    S. Grilli, INLN
c$$$C-----------------------------------------------------------------------
c$$$C     
c$$$      include 'param.inc'
c$$$C     
c$$$      IMPLICIT REAL (A-H,O-Z)
c$$$C     
c$$$      COMMON /DELEM1/ DUM1(3*MNODM),IDU1(MNODM),NNODE,NINTR,IDU2(2)
c$$$      COMMON /INTEG/ DUM2(NINTM),WEIGTR(NINTM),DUM3(4*NINTM),
c$$$     .     DUM4(2*NINTM*NINTM),DUM5(4*NINTM*NINTM*MNOD)
c$$$      COMMON /FSCUB/ DUM6(4*NDM*NINTM),
c$$$     .     FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
c$$$     .     DFIETP(MNODM,NINTM,NINTM),XYZPR(NINTM,NINTM,3),
c$$$     .     ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
c$$$      COMMON /SAVEF/ FATINT(LSAVEF)
c$$$C     
c$$$      SAVE
c$$$C     
c$$$C.....Common SAVEF saving
c$$$C     
c$$$      NNODM = NNODE*NNODE
c$$$      IPCOR = 0
c$$$C     
c$$$      DO IP=1,NINTR
c$$$         DO JP=1,NINTR
c$$$C     
c$$$            FATINT(IPCOR+1) = WEIGTR(IP)*WEIGTR(JP)*ZJACPR(IP,JP)
c$$$C     
c$$$            DO J=1,3
c$$$               FATINT(IPCOR+1+J) = XYZPR(IP,JP,J)
c$$$               FATINT(IPCOR+4+J) = ZNPR(IP,JP,J)
c$$$            END DO
c$$$C     
c$$$            DO I=1,NNODM
c$$$C     FATINT(IPCOR+7+I)   = FIRIP(I,IP,JP)
c$$$C     FATINT(IPCOR+7+2*I) = DFIXIP(I,IP,JP)
c$$$C     FATINT(IPCOR+7+3*I) = DFIETP(I,IP,JP)
c$$$               FATINT(IPCOR+7+3*I-2) = FIRIP(I,IP,JP)
c$$$               FATINT(IPCOR+7+3*I-1) = DFIXIP(I,IP,JP)
c$$$               FATINT(IPCOR+7+3*I)   = DFIETP(I,IP,JP)
c$$$            END DO
c$$$C     
c$$$            IPCOR = IPCOR + (7+3*NNODM)
c$$$         END DO
c$$$      END DO
c$$$C     
c$$$      RETURN
c$$$C     
c$$$      END
C-----------------------------------------------------------------------
      SUBROUTINE GAUSSP
C-----------------------------------------------------------------------
C     0  GAUSSPF   GAUSSP
C     1  PURPOSE   Computes Gauss points and weights for NINTR integ.
C     pts (2,10)
C     2  CALL      CALL GAUSSP
C     3  CALL ARG. NINTR               = Number of reg. or sing. int.
C     points
C     3            ETARP               = Integration points
C     3            WEIGTR       = Integration weight
C     6  INT.CALL  ERRORS
C     E  ERRORS    01= Incorrect number of integration points (2 To 10)
C     9
C     9  Jan. 99   S. Grilli, INLN
C-----------------------------------------------------------------------
C     

C     
      IMPLICIT REAL*8 (A-H,O-Z)

      include 'param.inc'
C     
      CHARACTER*8 TEXTE1
C     
      DIMENSION XIP(5,9),WIP(5,9)
C     
      COMMON /DELEM1/ DUMY(3*MNODM),IDV(MNODM+1),NINTR,IDU(2)
      COMMON /INTEG/ ETARP(NINTM),WEIGTR(NINTM),DUM1(4*NINTM),
     .     DUM2(2*NINTM*NINTM),DUM3(4*NINTM*NINTM*MNOD)
C     
      SAVE
C     
      DATA TEXTE1/'GAUSSP01'/,EPS/1.D-08/,TWO/2.D0/
C     
      DATA XIP/.577350269189626D0,4*0.D0,
     .     .774596669241483D0,4*0.D0,
     .     .861136311594053D0,.339981043584856D0,3*0.D0,
     .     .906179845938664D0,.538469310105683D0,3*0.D0,
     .     .932469514203152D0,.661209386466265D0,
     .     .238619186083197D0,2*0.D0,
     .     .949107912342759D0,.741531185599394D0,
     .     .405845151377397D0,2*0.D0,
     .     .960289856497536D0,.796666477413627D0,
     .     .525532409916329D0,.183434642495650D0,0.D0,
     .     .968160239507626D0,.836031107326636D0,
     .     .613371432700590D0,.324253423403809D0,0.D0,
     .     .973906528517172D0,.865063366688985D0,
     .     .679409568299024D0,.433395394129247D0,
     .     .148874338981631D0/
C     
      DATA WIP/1.D0,4*0.D0,
     .     .555555555555556D0,.888888888888889D0,3*0.D0,
     .     .347854845137454D0,.652145154862546D0,3*0.D0,
     .     .236926885056189D0,.478628670499366D0,
     .     .568888888888889D0,2*0.D0,
     .     .171324492379170D0,.360761573048139D0,
     .     .467913934572691D0,2*0.D0,
     .     .129484966168870D0,.279705391489277D0,
     .     .381830050505119D0,.417959183673469D0,0.D0,
     .     .101228536290376D0,.222381034453374D0,
     .     .313706645877887D0,.362683783378362D0,0.D0,
     .     .812743883615744D-01,.180648160694857D0,
     .     .260610696402935D0,.312347077040003D0,
     .     .330239355001260D0,
     .     .666713443086881D-01,.149451349150581D0,
     .     .219086362515982D0,.269266719309996D0,
     .     .295524224714753D0/
C     
C.....Check validity of general data
C     
      IF (NINTR.LT.2.OR.NINTR.GT.10) CALL ERRORS (TEXTE1)
C     -----------
C     
C.....Gauss quadrature, degree 3 to 19 (2 to 10 integration points)
C     int(f(x)*dx)=sum(f(xip)*wip), x in [-1,1]
C     
      NIN = NINTR/2
C     
      IF(ABS(FLOAT(NIN)-FLOAT(NINTR)/TWO).LE.EPS) THEN
         DO IP=1,NINTR
C     
C     Even number of integration points
C     
            IF(IP.LE.NIN) THEN
               K = -1
               M = IP
            ELSE
               K = 1
               M = IP-2*(IP-NIN)+1
            END IF
C     
            ETARP(IP)  = K*XIP(M,NINTR-1)
            WEIGTR(IP) = WIP(M,NINTR-1)
         END DO
      ELSE
         DO IP=1,NINTR
C     
C     Odd number of integration points
C     
            NIN = NIN + 1
            IF(IP.EQ.NIN) THEN
               K = 0
               M = NIN
            ELSE IF(IP.LT.NIN) THEN
               K = -1
               M = IP
            ELSE IF(IP.GT.NIN) THEN
               K = 1
               M = IP - 2*(IP-NIN)
            END IF
C     
            ETARP(IP)  = K*XIP(M,NINTR-1)
            WEIGTR(IP) = WIP(M,NINTR-1)
         END DO
      END IF
C     
      RETURN
C     
      END
C---------------------------------------------------------------------
      SUBROUTINE CARAC2
C---------------------------------------------------------------------
C     0  CARAC2F   CARAC2
C     1  PURPOSE   Compute the XYZip,ZNip, and JAC at the integration
C     1            point IP of the 2D element IE, NNODE in each
C     direction
C     2  CALL      CALL CARAC2
C     3  CALL ARG. NNODM               = Nb. of nodes of element IE
C     3            XYZLOC(NNODM)       = XYZ-coordinates of element IE
C     nodes
C     3            FI(NNODM)           = Shape functions and       )  At
C     point
C     3            DFIX(NNODM),DFIE()  = their derivatives, xi,eta )
C     IP
C     3            XIP,ZIP             = Current integr. point
C     coordinates
C     3            ZN(3)               = Current integr. point normal
C     vector
C     3            ZJAC                = Current integr. point jacobian
C     4  RET. ARG. XYZ(3),ZJAC,ZN(3)
C     9  Jan. 99   S. GRILLI, INLN
C     LCARAC2 SUB. WHICH COMPUTES CARACTERISTICS AT IP OF IE FOR THE 3D
C     -BEM
C-----------------------------------------------------------------------
C     

C     
      IMPLICIT REAL*8(A-H,O-Z)
      include 'param.inc'
      CHARACTER*8 TEXTE1
C     
      COMMON /DELEM1/ XYZLOC(MNODM,3),IDU(MNODM),NNODE,IDV(3)
      COMMON /FUNCTN/ FI(MNODM),DFIX(MNODM),DFIE(MNODM),XYZ(3),DUM(6),
     .     ZN(3),ZJAC
      COMMON /TANGET/ SMNVEC(3,3)
C     
      DATA ZERO/0.D0/,TEXTE1/'CARAC201'/

      SAVE
C     SAVE /DELEM1/,/FUNCTN/
C     
C     Jacobian matrix, tangential vectors, and IP coordinates
C     
      NNODM = NNODE*NNODE
      DO J=1,3
         DO I=1,2
            SMNVEC(I,J) = ZERO
         END DO
         XYZ(J) = ZERO
      END DO
C     
      DO I=1,NNODM
         DO J=1,3
            SMNVEC(1,J) = SMNVEC(1,J) + DFIX(I)*XYZLOC(I,J)
            SMNVEC(2,J) = SMNVEC(2,J) + DFIE(I)*XYZLOC(I,J)
            XYZ(J)      = XYZ(J)      + FI(I)*XYZLOC(I,J)
         END DO
      END DO
C     
C     Normal vector at IP
C     
      ZN(1) = SMNVEC(1,2)*SMNVEC(2,3) - SMNVEC(2,2)*SMNVEC(1,3)
      ZN(2) = SMNVEC(1,3)*SMNVEC(2,1) - SMNVEC(2,3)*SMNVEC(1,1)
      ZN(3) = SMNVEC(1,1)*SMNVEC(2,2) - SMNVEC(2,1)*SMNVEC(1,2)
      ZJAC   = DSQRT(ZN(1)*ZN(1) + ZN(2)*ZN(2) + ZN(3)*ZN(3))
      IF(ZJAC.LE.ZERO) CALL ERRORS(TEXTE1)
C     -----------

      ZN(1) = ZN(1)/ZJAC
      ZN(2) = ZN(2)/ZJAC
      ZN(3) = ZN(3)/ZJAC
C     
      RETURN
C     
      END
