        PROGRAM TANK
C-------------------------------------------------------------------------------
C
C     Input parameters in file input.dat :
C
C     MX,MY,MZ : Nb. of segment (elements in each direction)>= 4 !!
C         NX=MX+1,NY=MY+1,NZ=MZ+1 : Nb. of nodes in each direction
C         NOM = 2(NX*NY + NX*NZ + NY*NZ)
C         MOM = 2(MX*MY + MY*MZ + MX*MZ)
C     NINTR    : Nb. of reg. and sing. Gauss pts per direction (must be even)
C     NSUBC    : Max nb. of binary subdivisions for corner elements
C     ALMAXB,ALMAXI : angle criterion for adap. integ. on bound. and int.
C     ZL0      : Numerical wave tank length
C     W0       : Numerical wave tank width
C     H0       : Numerical wave tank ocean depth
C     X0       : Numerical wave tank leftward x-coord.
C     D0       : Numerical wave tank length of h=H0 part
C     IBOT  = 1 Flat bottom z=-H0 (image method is used)
C           = 2 Sloping bottom s=SLOPE in x-dir, starting at x=D0
C           = 3 Sloping bottom s=SLOPE in x-dir, starting at x=D0
C             with sech^2(ZK*y) taper scaled to z=-H0 at +-W0/2
C           = 4 Sloping bottom s=SLOPE in x-dir, starting at x=D0
C             with sech^2(ZK*y) to y = +-W0/2
C           = 5 Sloping bottom s=SLOPE in x-dir, starting at x=D0
C             till x=L0-ZL2
C             with sech^2(ZK*y) to y = +-W0/2 and sech^2(zk*xi) for
C             underwater landslide
C           = 6 same as 5 but symmetry for y=0
C           = 7 CK: Flat bottom z=-H0 + heavyside function
C           = 10 flat bottom at z=-H0, image method => both NOM and MOM
C             are adjusted since boundary 6 is skipped in generation of data
C
C     IBCOND(K) : BC for boundary K : 1 Dirichlet
C                                     0 Impermeable Neuman
C                                     2 Wavemaker with IPTYP
C                                     3 AP
C                                     4 slideing bottom
C     ILMAX    : Max number of loops
C     IPTYP = 0 Exact solitary wave H=V0H,eps=TOLMAX
C           = 1 Periodic sine wave TUNE=V0H,om=OMEGA,NHT=0
C           = 1 Sum of n period. sine waves TUNE=V0H,NHT=n,
C             READ paddle.dat2 (a,om,phase)
C           = 2 Solitary wave by pist. wm. H=V0H
C           = 3 Cnoidal wave by pist. wm. H=V0H,om=OMEGA
C           = 4 Zero-mass flux SFW H=V0H,om=OMEGA
C           = 8 wave focusing with flap wavemaker
C           = 7 wave focusing with flap wavemaker, ECN wavemaker geometry
C           = 9 Underwater landslide with IBOT = 5,6 (symmetry)
C           = 10 Underwater slump with IBOT = 5,6 (symmetry)
C           = 11 Class 2 wave focusing with flap wavemaker
C     ISTART : 0/1 Restart with earlier data in file restart.dat
C     IGRID  : 0/1 Regrid using earlier data in file restart.dat
C              2 Regrid with a non regular grid given by XMIN and DXMIN
C     NBS    : Number of stretched bottom segments in front of moving bds.
C     IPRINT : index for printing restart results (every IPRINT loops)
C     TPRINT : time for printing restart results (at time~TPRINT)
C     DT     : Initial time step
C     TSTART : Start time
C     TMAX   : ending time
C     RHO    : water density
C     CPRESS : initial atmospheric pressure
C     CPREST : D/Dt of atmospheric pressure
C     GE     : Gravitational acceleration
C     TDAMP  : Damping Nb. of periods for initial moption and IPTYP=1,4
C     TDOWN  : For t>tdown, the paddle amplitude start decreasing to 0
C     DE1,DE2 : extremity reference depths in x-direction
C     OMEGA,V0H : wave parameters, IPTYP=1,2,3,4
C     INFIE  : Interior points flag (0 no; 1 yes)
C     NOI    : Nb. of interior points
C     NHT    : Nb. of sine components for IPTYP=1
C     TOLMAX : Used for IPTYP=0, sol wave generation
C     XLDAMP,N0 : Absorbing beach length in x-dir from extremity and coef
C     TRESH  : front wave slope angle treshold for local absorption
C     SUTENS : surface tension (0 no)
C     NPOLY  : coef. for Nu variation iin AB
C     ISOLVE : (1/0) choice of direct/iterative GMRES solver
C     ISYM   : (0/1) Symmetry in y
C     IPOST  : (0/1/2) for choice of postprocessing subroutines (1:VOF,2:WM/AP-freak waves/tsunamis)
C     IVECT  : (0/1/2) for vectorized integration subroutines (1:BILMAT and BIDMAT concerned,2:only BILMAT)
C     IFREQ  : (0/1) directional focusing without or with freq. focusing
C     NFR    : number of frequency components for wavemaker
C
C     NXI,NYI,NZI : Number of internal points in each dir to be automatically generated,
C                   with NOI=NXI*NYI*NZI. If 0,0,0 read NOI points from intern.dat
C     NGA : Number of internal gages of coordinates (x,y) to be read from gage.dat.
C           If 0 and INFIE.EQ.1, generate NGA=NXI*NYI (x,y) coords identical to internal points
C
C     S. Grilli, 1/99, INLN, 6/02 LSEET
C
C     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA
C     by coupling with code PMTA.
C     parenthesis indicates not (yet) available options, which exits in PMTA
C
C     CUBE   : length of the level 0 cube, containing all the nodes
C     NLEVELS : number of levels in the hierarchcal subdivision
C     IFFT   : flag for fft subroutines use
C     MP     : number of multipoles
C     IPFMA+IEPMTA :(0 Multipole Treecode Algorithm)
C                    1 Fast Multipole Algorithm
C                   (2 Enhanced MTA)
C    (THETA  : Barnes & Hut Theta parameter)
C    (NPROC  : number of processors)
C    (IDIRECT : flag for direct calculations)
C    (IBALANCED : flag for parallel models)
C     IVERBOSE : flag for reporting on what PMTA is doing
C    (IPMON  : flag for pmon performance)
C     INICOULOMB : flag for calling InitializeCoulomb
C     IFREECOULOMB : flag for calling FreeCoulomb
C
C23456789012345678901234567890123456789012345678901234567890123456789012
C-------------------------------------------------------------------------------
C-------------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)

C-------------------------------------------------------------------------------
      INCLUDE 'param.inc'

C-------------------------------------------------------------------------------
      CHARACTER*8 TEXTE

C-------------------------------------------------------------------------------
c      include 'param.h' ! nxm,nym,nzm,nomm,momm,mnod,noim,nfmax,nhmax,mymax,
c                          ndm,nintm,ngam,mnodm,ndm2,lsavef,mpm
      include 'datgen.h' ! zl0,w0,h0,x0,d0,mx,my,mz,nx,ny,nz,nsubc,nintr,nom,mom
      include 'maille.h' ! xyznod,xicon,xstart,igcon,iface,isub,intg,lxyz,icumul
c                          jslim,nelef,nnodf,ielef,inodf,ibcond
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
      include 'boundc.h' ! dt,dt2,dt22,tstart,tmax,rho,cpress,cprest,ge,de,
c                          omega,v0h,tolmax,nbs
      include 'interf.h' ! field,lisub,irep,nxi,nyi,nzi,noi,ifield
      include 'dissip.h' ! xdamp,xldamp,dnu0,tresh,sutens,coef,coefx,islop,
c                          npoly,iflab,ibreak,itens
      include 'paddle.h' ! ap,op,sp,zkwave,distf,tfoc,alphaf,wmidth,os,ost,ostt,
c                          damp,dampt,damptt,delfr,nfr,nh,ifreq
      include 'botfct.h' ! bot,slope,zk,ibot
      include 'lndsld.h' ! theta,bslide,wslide,tslide,dslide,zl2,rhol,gamma,cm,
c                          cd,zeps,zmu,zmuc,zal,ut,a0,s0,t0,wlegth,zkx,zky,
c                          sthet,cthet,tthet,coshep,coshmu,xinit,tslamx,s,st,
c                          stt,xbegin,xend,xcentr,xi0,sdeltt,deltas,zks,tslope,
c                          deltat,ainit,tanhxi,zkramp,uinit,tanhtt,sinit,coshti,
c                          ratio2
      include 'cgage.h' !  zyzgag,uvwg,phitb,phig,phing,phib,phinb,xyzbot,uvwb,
c                          igag,nga,intzed
      include 'fmapar.h' ! cube,teta,nlevels,ifft,mp,ipfma,iepmta,nproc,idirect,
c                          ibalanced,iverbose,ipmon,inicoulomb,ifreecoulomb
      include 'subdi.h'  ! almaxb,lsub(nomn)
      include 'option.h' ! isym,ipost,ivect
      include 'result.h' ! pg(nomm),phi(nomm),phin(nomm),phit(nomm),phitn(nomm),
c                          phis(nomm),phim(nomm),phiss(nomm),phimm(nomm),
c                          phins(nomm),phinm(nomm),phits(nomm),phitm(nomm),
c                          phism(nomm),phinn(nomm),
c                          pres(nomn),dprdt(nomm),uvw(3,nomm),duvwdt uvw(3,nomm)
      include 'regrid.h' 	
      include 'paddle2.h' 	

	character(len=16) date
	logical switchautoregrid,Mainloop
C-------------------------------------------------------------------------------
      DATA ONE/1.D0/,ZERO/0.D0/,TWO/2.D0/,PI/3.1415926535898D0/,
     .     HALF/0.5D0/,IO5/5/,IO6/6/,IO10/10/,IO11/11/,
     .     IO15/15/,IO80/80/,IO99/99/,PDEG/180.D0/,
     .     IO60/60/,IO61/61/,IO62/62/,IO71/71/,IO72/72/,
     .     MOT/8/,IO16/16/,TEXTE/'NWTANK01'/,IO8/8/,IO63/63/

C-------------------------------------------------------------------------------
c      open and load the suffix of result files	
C-------------------------------------------------------------------------------
      OPEN(UNIT=666,STATUS='UNKNOWN',FILE=' suffix.outputfile')
      read(666,*) date

C-------------------------------------------------------------------------------
C.....Problem general input/output files
C-------------------------------------------------------------------------------
      OPEN(UNIT=IO5,STATUS='UNKNOWN',FILE='output/restart.dat.'//date)
      OPEN(UNIT=IO6,STATUS='UNKNOWN',FILE='output/listing.dat.'//date)

C-------------------------------------------------------------------------------
C     Files for results saving
C-------------------------------------------------------------------------------
      OPEN(UNIT=IO10,STATUS='UNKNOWN',FILE='output/ekpt.dat.'//date)
      OPEN(UNIT=IO11,STATUS='UNKNOWN',FILE='output/volu.dat.'//date)	 
      OPEN(UNIT=IO15,STATUS='UNKNOWN',FILE='output/final.dat.'//date)
      OPEN(UNIT=IO16,STATUS='UNKNOWN',FILE='output/bottom.dat.'//date)
      OPEN(UNIT=IO99,STATUS='UNKNOWN',FILE='output/tstep.dat.'//date)
      open(142,file='PaddleMotion.dat.'//date)
      open(97,file='NWTWRLPaddleMotion.dat.'//date)

	  
C-------------------------------------------------------------------------------
C     Input  general data
C-------------------------------------------------------------------------------
C      call readparam(io6)
c	print*,'1'
      call preprocess(almaxi,ilmax,iprint,tprint,
     .                infie,nht,istart,igrid,xmin,dxmin,dymin)

c------------------------------------------------------------------------------
c	autoregrid
c------------------------------------------------------------------------------
	switchautoregrid=.true.
	Mainloop=.true.
	do while (switchautoregrid) 
	switchautoregrid=.false.
!      	  print*,'start autoregrid'
!      	  print*,'-----------------------------------------------------'
!	  print*,'INIT : switchautoregrid,Mainloop,ILOOP,ILOOP1,
!     &	   ISTART,IGRID,INICOULOMB,IFREECOULOMB,iprint'
!	  print*,switchautoregrid,Mainloop,ILOOP,ILOOP1,
!     &	   ISTART,IGRID,INICOULOMB,IFREECOULOMB,iprint
!          print*,'cube,teta,nlevels,ifft,mp,ipfma,iepmta,nproc,idirect,
!     &                 ibalanced,iverbose,ipmon,inicoulomb,ifreecoulomb,
!     &		IBEMANALYSIS'
!          print*,cube,teta,nlevels,ifft,mp,ipfma,iepmta,nproc,idirect,
!     &                 ibalanced,iverbose,ipmon,inicoulomb,ifreecoulomb,
!     &		IBEMANALYSIS
!      	  print*,'-----------------------------------------------------'

C-------------------------------------------------------------------------------
C.....Generate problem BEM data
C-------------------------------------------------------------------------------
      ILOOP = 0
c      CALL GENER(PHI,PHIN,ISTART,IGRID,IO5,IO60,IO63,IO16,IO8,
c     .           XMIN,DXMIN,DYMIN)
c-------
c	correction for the new restart, XB
c-------
c	print*,'2'
      CALL GENER(ISTART,IGRID,IO5,IO60,IO63,IO16,IO8,
     .           XMIN,DXMIN,DYMIN,DT)

C-------------------------------------------------------------------------------
C.....General initializations
C-------------------------------------------------------------------------------
c	print*,'3'
      call init(timep,istart)

C-------------------------------------------------------------------------------
C     Beach (AB) and surface tension flags
C-------------------------------------------------------------------------------
      IF(XLDAMP.GT.ZERO) THEN
         IFLAB = .TRUE.
	    XDAMP = X0 + ZL0 - XLDAMP
      END IF

      IF (SUTENS.GT.ZERO) THEN
         ITENS = .TRUE.
      END IF

C-------------------------------------------------------------------------------
C     Selective absorption on front face
C-------------------------------------------------------------------------------
      IF(TRESH.GT.ZERO) THEN
         IBREAK = .TRUE.
C-------------------------------------------------------------------------------
C        Initialize absorption coefficient on free surface
C-------------------------------------------------------------------------------
	    DO IK=INODF(1,1),INODF(1,2)
	       COEF(IK)  = ZERO
	       COEFX(IK) = ZERO
	    END DO
      END IF

C-------------------------------------------------------------------------------
C.....Generate initial data for exact solitary wave
C-------------------------------------------------------------------------------
      IF(IPTYP.EQ.0.AND.ISTART.EQ.0) THEN
         WRITE(IO6,*) 'Solwave data :'
         iflr = 1 ! wave moves left to right'
c        iflr = 0 ! wave moves right to left'
         CALL SOLWA(PHI,PHIN,IO6,iflr)
C        ----------
         WRITE(*,*) ' === Solwave generated === '
      END IF
C              
      IF((IPTYP.EQ.1).or. (IPTYP.EQ.7) .or. (IPTYP.EQ.8)
     .   .or.  (IPTYP.EQ.11) )THEN
         write(*,*) 'paddle activated'
c         write(*,*)'amplitude',V0H
c         write(*,*)'omega',OMEGA
c         write(*,*)'TDOWN',TDOWN
      END IF 

      IF(IBCOND(4).EQ.3) THEN
         write(*,*)'absorbing piston activated'
      END IF

C------------------------------------------------------------------------
C.....Compute initial sliding derivat. on the 6 boundaries
C------------------------------------------------------------------------
c 	print*,'4'
      CALL SLIDING(1)
C     ------------
C-------------------------------------------------------------------------------
         write(*,*) 'nwtank: TMAX=', TMAX, ', ILMAX=', ILMAX
C-------------------------------------------------------------------------------
C.....Solution
C-------------------------------------------------------------------------------
      IFLAGS = 1

      DO WHILE((TIME.LE.TMAX).AND.(ILOOP.LT.ILMAX).and.Mainloop)
C-------------------------------------------------------------------------------
         ILOOP = ILOOP + 1
	 ILOOP1=ILOOP+ILOOP0
         WRITE(IO6,'(A,I5,A,ES12.4,A,ES12.4)') 'nwtank: Iloop: ',ILOOP1,
     .      ', Time : ',TIME,', DT: ',DT
         WRITE(*,'(A,I5,A,ES12.4,A,ES12.4)') 'nwtank: Iloop: ',ILOOP1,
     .      '  Time: ',TIME,'  DT: ',DT
C-------------------------------------------------------------------------------
C        Compute d/ds,d/dm,u,v,w on Dirichlet and moving boundaries
C-------------------------------------------------------------------------------
c 	print*,'9'
         CALL DUDTPR(PHI,PHIN,PHIS,PHIM,PHISS,PHIMM,PHISM,PHINS,
C        -----------
     .               PHINM,UVW)
C-------------------------------------------------------------------------------
C        Fix pressure on boundaries (2nd argument line is void here)
C        and consequently fix Phit on the Dirichlet boundaries
C-------------------------------------------------------------------------------
c 	print*,'10'
         CALL PREFIX(PRES,PHIT,PHIN,PHIS,PHIM)
C        -----------
C-------------------------------------------------------------------------------
C        Fix the Neuman conditions at time t (paddle, imperm,...) &
C        the domn. sources & Solve the system (Phit,Phitn) at time t
C        (performs a first BEM analysis for ILOOP=1 with IFLAGS=1)
C-------------------------------------------------------------------------------
         IFLAGV = 2
C-------------------------------------------------------------------------------
C        no more matrices in NOM*NOM, CF03
C-------------------------------------------------------------------------------
c         CALL RESOLFMA(PG,PHIT,PHITN,
C        -------------
c     .              PHI,PHIS,PHIM,PHISS,PHIMM,PHINS,PHINM)
c	New line, XB
c 	print*,'11'
         CALL RESOLFMA(PG,PHIT,PHITN,
C        -------------
     .              PHI,PHIS,PHIM,PHISS,PHIMM,PHINS,PHINM,PHIN,ISTART,
     &		switchautoregrid)
C-------------------------------------------------------------------------------
C        Fix other s-deriv., calc. du/dt,dv/dt,dw/dt
C-------------------------------------------------------------------------------
c 	print*,'12'
         CALL D2UTPR(PHIT,PHITS,PHITM,PHITN,PHIS,PHIM,PHIN,PHINS,PHINM,
C        -----------
     .               PHISS,PHIMM,PHISM,DUVWDT,PHINN)
C-------------------------------------------------------------------------------
C        Boundary fluxes and internal point calculations
C-------------------------------------------------------------------------------

c-------
c	correction for the new restart, XB
c-------
c         CALL POSTPROC(PHI,PHIN,PHIS,PHIM,PHIT,PHITN,UVW,DUVWDT,PRES,
c 	print*,'9'

!       call autoregrid3(Mainloop,switchautoregrid,iprint,
!     &              ISTART,IGRID,TSTART,TIME,iloop)
c	print*,'1'
         CALL POSTPROC(
C        -------------
     .                TIMEP,ALMAXI,
     .                IO10,IO11,IO15,IO60,IO61,IO62,IO71,IO72,
     .                IPRINT,TPRINT,ALMAXB,NHT,ILMAX,ISTART,IGRID,INFIE,
     .                IPOST,ILOOP,NOM,TIME,DT,ILOOP0)

C-------------------------------------------------------------------------------
C        Fix dp/dt on lateral and surface Dirichlet boundaries
C-------------------------------------------------------------------------------
c	print*,'2'
         CALL DPRFIX(DPRDT,PHIS,PHIM,PHIN,PHINS,PHINM,PHITN,PHINN)
C        -----------
C-------------------------------------------------------------------------------
C        Update Phi,x,z on all Dirichlet boundaries.
C        Thus new geometry of the Dirichlet boundaries
C-------------------------------------------------------------------------------
c 	print*,'3'
        CALL UPDTDB(PHI,PHIT,UVW,DUVWDT,DPRDT)
C        -----------
C-------------------------------------------------------------------------------
C        Fix next time step
C-------------------------------------------------------------------------------
c 	print*,'4'
        CALL DTSTEP(IO99,UVW)
C        -----------
C-------------------------------------------------------------------------------
C        Solving of the system (Phi, Phin) at time t+dt : new geometry
C        of the free surface and updating of the rest of the boundary
C        geometry and Neuman conditions (Paddle, imperm.,..), sources.
C        BEM analysis for the new boundary geometry  (IFLAGS=1).
C-------------------------------------------------------------------------------
         IFLAGV=1
         IFLAGS=1

C-------------------------------------------------------------------------------
C        no more matrices in NOM*NOM, CF03
C-------------------------------------------------------------------------------
c	print*,'5'
         CALL RESOLFMA(PG,PHI,PHIN,
C        -------------
     .              PHI,PHIS,PHIM,PHISS,PHIMM,PHINS,PHINM,PHIN,ISTART,
     &		switchautoregrid)
c	print*,'6'
C-------------------------------------------------------------------------------
C        Next system solving (Phit,Phitn) will be in the same geometry
C-------------------------------------------------------------------------------
         IFLAGS=2
	 ISTART=0
C-------------------------------------------------------------------------------
	Mainloop=.true.	
      END DO
!       call autoregrid4(Mainloop,switchautoregrid,iprint,
!     &              ISTART,IGRID,INICOULOMB,IFREECOULOMB,iloop0,iloop)
      enddo

C-------------------------------------------------------------------------------
C     free in PMTA, CF03
C
C     INICOULOMB = 0
C     IFREECOULOMB = 1
C	 IVERBOSE = 1
C         CALL CALLPMTA(CUBE,NLEVELS,IFFT,MP,IPFMA,IEPMTA,TETA,
C             --------
C     .              NPROC,IDIRECT,IBALANCED,IVERBOSE,IPMON,NOM,MOM,
C     .              INICOULOMB,IFREECOULOMB)
C-------------------------------------------------------------------------------
      CLOSE(IO5)
      CLOSE(IO6)
      CLOSE(IO10)
      CLOSE(IO11)
      CLOSE(IO15)
      CLOSE(IO17)
      CLOSE(IO99)
      close(142)
C-------------------------------------------------------------------------------
      STOP
C-------------------------------------------------------------------------------
 2000 FORMAT(I5,7F12.6)
 2020 FORMAT(8I5)
 2100 FORMAT(I5,11D14.6)
C-------------------------------------------------------------------------------
      END

