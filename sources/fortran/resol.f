C
C                 PROBLEM RESOLUTION AT CURRENT TIME
C                 FOR (PHI,PHIN) OR (PHIT,PHITN)
C                 WITH PMTA(FAST MULTIPOLE ALGORITHM)
C
C----------------------------------------------------------------------
c      SUBROUTINE RESOLFMA(PG,UBC,QBC,
c     .           PHI,PHIS,PHIM,PHISS,PHIMM,PHINS,PHINM)
c	New line, XB
      SUBROUTINE RESOLFMA(PG,UBC,QBC,
     .           PHI,PHIS,PHIM,PHISS,PHIMM,PHINS,PHINM,PHIN,ISTART,
     &		switchautoregrid)
C----------------------------------------------------------------------
C0  RESOLF    RESOLFMA
C1  PURPOSE   Update boundary conditions or b.c. and geometry (IFLAGV =
C1            (2/1)), performs BEM anal.(IFLAGS=1),dbe nde,sources,glob
C1            assembling and solving of one system (Phi or Phit).
C1            Use of the Fast Multipole Algorithm combined with GMRES
C2  CALL RESOL(PG,UBC,QBC,
C2             PHI,PHIT,PHIN,PHIS,PHIM,PHISS,PHIMM,PHISM)
C3  CALL ARG. XYZNOD(NOM)         = Coordinates of discretization nodes
C3            XICON(MOM,2)        = xo,eta for MII elements
C3            IGCON(MOM,4,4)      = MII elts connectivity
C3            ILCON(MOM,4)        = 4-node elt. connectivity
C3            IFACE(MOM)          = Face code for each element
C3            ISUB(MOMM)          = Max subdivisions for elts.
C3            INTG(MOMM)          = Nb. of integ. points per elt.
C3            LSUB(NOM)           = Subdivision status for element IE
C3            LXYZI(NOM,2)        = Addresses of double/triple nodes
C3            JSLIM(6)            = Record limits for saving in /SAVEF/
C3            NELEF(6),NNODF(6)   = Nbs. of elts/nodes for each face
C3            IELEF(6,2),INODF()  = Begin/ending node/elemt Nbs. for face
C3            IBCOND(6)           = 0: impermeable boundary, 1: Dirichl.
C3                                  bound., 2: wavemaker bd. (IPTYP=1-4)
C3                                  3 : absorbing piston
C3            NOM,MOM             = Discr. nb. of nodes and elements
C3            NOMM,MOMM           = Discr. max nb. of nodes and elts.
C3            NNODM               = Discr. max nb. of nodes per element
C3            IFLAGS,IFLAGV       = Flags for BEM and geometry updating
C3            IPTYP               = Type of problem/bc for wave generation
C3            ALMAX [0,PI/2]      = Limit angle for adapt. integration
C3            SINBF(4),COSBF(4)   = Direct. cosines of free s. bd.db-n
C3            SINBR(4),COSBR(4)   = Direct. cosines of radiat. bd.db-n
C3            VX(4),VZ(4)         = X,Z-compnt. of D-D dble node veloc.
C3            IABSP,UPNEW,DPHIN   = Absorbing piston parameters
C3            UB                  = Lateral uniform current for stfc.
C3            TIME,TDAMP          = Current time and damping time for wm
C3            ILOOP               = Current loop index
C3            IO6                 = Unit nb. for output file
C3
C4  RET. ARG. UBC(NOM),QBC(NOM)  (results for Phi,Phin or Phit,Phitn)
C6  INT. CALL UPLABC,UPBOTC,ASSEMP,DBNODP,SOLVER2,SORT,
C6            SLIDING,FSVELO,CALLPMTA
C10 Feb. 99   S. Grilli, INLN
C10
C10 adapted for Fast Multipole Algorihtm, C. Fochesato, 2003, CMLA
C10
CLRESOL SUB. WHICH SETS AND SOLVES BOTH SYSTEM FOR 3D-BEM NONLINEAR WAVE
C-------------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,MOM
      COMMON /MAILLE/ XYZNOD(NOMM,3),XICON(MOMM,2),DUM2(18),
     .                IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),IDUM2(36),JSLIM(6),NELEF(6),
     .                NNODF(6),IELEF(6,2),INODF(6,2),IBCOND(6)
c      COMMON/SYSTEM/  SINBR(4),SINBF(4),COSBR(4),COSBF(4),VX(4),
c     .                VZ(4),TIME,TDAMP,TDOWN,UPNEW(4,MYMAX),
c     .                DUPDT(4,MYMAX),UB(4),IO6,IFLAGS,IFLAGV,IPTYP,
c     .                ILOOP
      COMMON /BOTFCT/ DUMBOT(NXM*NYM+2),IBOT
      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
      include 'system.h' ! sinbr,sinbf,cosbr,cosbf,vx,vz,time,tdanmp,upnew,
c                          dupdt,ub,io6,iflags,iflagv,iptyp,iloop
C-------------------------------------------------------------------------------
C.....Two new commons, the first to share variables with PMTA
C       Q : charge vector
C       SOL : matrix/vector product
C       IBEMANALYSIS : flag for calling a new BEM analysis (when new geometry)
C       IRLRIGID : flag for Dirichlet or Neumann coefficients
C     the second to store the vector for rigid mode technique RM
C     and preconditioning matrix SPA, CF03
C-------------------------------------------------------------------------------
      COMMON /CHARGE/ Q(NOMM),SOL(NOMM),IBEMANALYSIS,IRLRIGID
      COMMON /RMPREC/ RM(NOMM),SPA(NOMM)
C-------------------------------------------------------------------------------
C.....a common is added for FMA parameters, CF03
C-------------------------------------------------------------------------------
      COMMON /FMAPAR/ CUBE,TETHA,NLEVELS,IFFT,MP,IPFMA,IEPMTA,
     .                NPROC,IDIRECT,IBALANCED,IVERBOSE,IPMON,
     .                INICOULOMB,IFREECOULOMB
C-------------------------------------------------------------------------------
C.....a common is added for some variables which need to be initialized
C     outside the multiple calls to BEMK2 and BEMKFMA, CF03
C-------------------------------------------------------------------------------
      COMMON /BEMINI/ EFJXIP,EFJETP,JSAVE,NINTPR,NSIDEP
      COMMON /OPTION/ ISYM,IPOST,IVECT
C-------------------------------------------------------------------------------
C.....No more matrices in NOM*NOM, CF03
C-------------------------------------------------------------------------------
      DIMENSION PG(NOM),
     .          UBC(NOM),QBC(NOM),PHI(NOM),PHIS(NOM),PHIM(NOM),
     .          PHISS(NOM),PHIMM(NOM),PHINS(NOM),PHINM(NOM),TX(NOMM)
C  Bug
      DIMENSION PHIN(NOM)
	logical switchautoregrid
C-------------------------------------------------------------------------------
      SAVE
C-------------------------------------------------------------------------------
      DATA ZERO/0.D0/,ONE/1.D0/
C-------------------------------------------------------------------------------
C.....Geometry (IFLAGV=1) and Boundary conditions updating (IFLAGV=1,2)
C     for the Neuman B. cond. cases.
C
C     Recording the previous solution as initial condition for use
C     in the iterative GMRES solver (only with SOLVER)
C-------------------------------------------------------------------------------
      CALL ENTR(TX,UBC,QBC)
C     ---------
C-------------------------------------------------------------------------------
C     Initialization with impermeable Neuman condition everywhere
C-------------------------------------------------------------------------------
      DO I=1,NOM
         QBC(I) = ZERO
      END DO
C-------------------------------------------------------------------------------
C     Updating
C-------------------------------------------------------------------------------
      CALL UPLABC(UBC,QBC,PHI,PHIS,PHIM,PHINS,PHINM,PHISS,
     .                  PHIMM)
C     -----------
C-------------------------------------------------------------------------------
      IF(IFLAGV.EQ.1.AND.IBOT.NE.10) THEN

         CALL UPBOTC(QBC,PHIS,PHIM,PHIN,PHISS,PHIMM,PHINS,PHINM)
C        -----------
      END IF
C-------------------------------------------------------------------------------
C.....Compute new sliding s-derivatives when geometry changes
C-------------------------------------------------------------------------------
      IF(IFLAGV.EQ.1) THEN
         CALL SLIDING(1)
C        ------------
      END IF
C-------------------------------------------------------------------------------
C.....Computation of the right hand side PG by FMA, CF03
C-------------------------------------------------------------------------------
c	Old line
      	IF (((ILOOP.EQ.1).or.(switchautoregrid)).AND.IFLAGS.EQ.1
     &      .AND.IFLAGV.EQ.2) THEN
c	New line, XB
c      IF (((ILOOP.EQ.1).or.(ISTART.eq.1)).AND.
c     &      IFLAGS.EQ.1.AND.IFLAGV.EQ.2) THEN
         INICOULOMB =  1
      ELSE
         INICOULOMB =  0
      END IF
      IF (IFLAGS.EQ.1) THEN
         IBEMANALYSIS = 1
C-------------------------------------------------------------------------------
C        Positions the file IO1 to its initial point
C
C         REWIND(IO1)
C
C        Initialization of variables needed in bemk2 and bemkfma to
C        do only once some computations
C-------------------------------------------------------------------------------
         JSAVE  = 0
         NINTPR = 0
         NSIDEP = 0
         EFJXIP = ZERO
         EFJETP = ZERO
C-------------------------------------------------------------------------------
         DO I=1,NOM
            SPA(I)=ZERO
         END DO
      ELSE
         IBEMANALYSIS = 0
      END IF
      IRLRIGID = 1
      IFREECOULOMB = 0
C-------------------------------------------------------------------------------
C.....Initialization of PG and Q with specified UBC,QBC
C     using ENTR with inversed arguments and PMTA mat*vec
C-------------------------------------------------------------------------------
      CALL ENTR(Q,QBC,UBC)
C     ---------
C-------------------------------------------------------------------------------
      DO I=1,NOM
         SOL(I) = ZERO
      END DO
C-------------------------------------------------------------------------------
C.....Solution of the matrix/vector product stored in SOL
C-------------------------------------------------------------------------------
      CALL CALLPMTA(CUBE,NLEVELS,IFFT,MP,IPFMA,IEPMTA,THETA,
C          --------
     .              NPROC,IDIRECT,IBALANCED,IVERBOSE,IPMON,
     .              NOM,MOM,INICOULOMB,IFREECOULOMB)
      DO I=1,NOM
         PG(I) = SOL(I)
      END DO
C-------------------------------------------------------------------------------
C.....Rigid mode technique adapted, vector RM in common for reuse
C-------------------------------------------------------------------------------
      IBEMANALYSIS = 0
      INICOULOMB =  0
      IRLRIGID = 3
C
      DO I=1,NOM
	 SOL(I)=ZERO
      END DO
      CALL CALLPMTA(CUBE,NLEVELS,IFFT,MP,IPFMA,IEPMTA,THETA,
C          --------
     .              NPROC,IDIRECT,IBALANCED,IVERBOSE,IPMON,
     .              NOM,MOM,INICOULOMB,IFREECOULOMB)
      DO I=1,NOM
	 RM(I) = SOL(I)
      END DO
      DO I=1,NOM
         IF (INCOND(I).EQ.1) THEN
	    PG(I) = PG(I) + UBC(I)*RM(I)
	 END IF
      END DO
C-------------------------------------------------------------------------------
C.....Define SPA, sparse assembly matrix for preconditioning
C-------------------------------------------------------------------------------
      IF(IFLAGS.EQ.1) THEN
         CALL DEFSPA()
C        -----------
      END IF
C-------------------------------------------------------------------------------
C.....Imposing double nodes compatibility to PG
C-------------------------------------------------------------------------------
      CALL DBNODP(PG,UBC)
C     -----------
C-------------------------------------------------------------------------------
C      write(*,*) '2nd membre with fma : 4'
C      do I=1,NOM
C           write(*,*) PG(I),I
C      end do
C      STOP
C
C.....no matrix for fma . TX=previous iteration, PG=second member.
C     Solution is calculated in Pg
C-------------------------------------------------------------------------------
      IRLRIGID = 2
      CALL SOLVER2(TX,PG,NOM)
C     -----------
c      write(*,*) 'resol with fma : '
c      DO I=1,NOM
c       write(*,*) PG(I),I
c      END DO
C-------------------------------------------------------------------------------
C.....Sorting the results in the original unknown vectors
C-------------------------------------------------------------------------------
      CALL SORT(PG,UBC,QBC)
C     ---------
C-------------------------------------------------------------------------------
      RETURN
C-------------------------------------------------------------------------------
 2100 FORMAT(I5,2E16.8)
      END
C------------------------------------------------------------------------------
      SUBROUTINE ENTR(PINT,UBC,QBC)
C------------------------------------------------------------------------------
C0  ENTRF     ENTR
C1  PURPOSE   Record the solution of the previous step for use in GMRES solver
C2  CALL      CALL ENTR(PINT,UBC,QBC)
C3  CALL ARG. PINT(NOM)           = Results of the previous BEM computation
C3            UBC(NOM),QBC(NOM)   = Contain Boundary conditions
C3            NOM                 = Discr. nb. of nodes
C4  RET. ARG. UBC(NOM)            = Nodal u over the boundary
C4            QBC(NOM)            = Nodal q over the boundary
C9  Jul. 99   P. GUYENNE, INLN
CLENTR SUB. WHICH RECORDS THE PREVIOUS RESULTS FOR USE IN GMRES SOLVER
C------------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
      COMMON /MAILLE/ DUM2(NOMM*3+MOMM*2+18),
     .                IDUM3(MOMM*MNOD*MNOD+MOMM*7+NOMM*2+66),
     .                INODF(6,2),IBCOND(6)
      COMMON /BOTFCT/ DUMBOT(NXM*NYM+2),IBOT
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION PINT(NOM),UBC(NOM),QBC(NOM)
C
      SAVE
C
C     Loop over the 6 boundaries
CSYM...
      KMAX = 6
      IF(IBOT.EQ.10) KMAX = KMAX-1
      IF(ISYM.EQ.1) KMAX = KMAX-1
C
      DO K=1,KMAX
         J1 = INODF(K,1)
         J2 = INODF(K,2)
C
         IF(IBCOND(K).EQ.1) THEN
C
C           Dirichlet boundary condition
C
            DO J=J1,J2
               PINT(J) = QBC(J)
            END DO
         ELSE
C
C           Neuman boundary condition
C
            DO J=J1,J2
               PINT(J) = UBC(J)
            END DO
         END IF
      END DO
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE DBNODP(PG,U)
C----------------------------------------------------------------------
C0  DBNODPF   DBNODP
C1  PURPOSE   Impose the compatibility conditions at the domain double
C1            nodes, for the R.H.S. PG only
C2  CALL      CALL DBNODP(PG,U)
C3  CALL ARG. PG(NOM)             = Global system vector
C3            U(NOM)              = Either Phi or Phit
C3            ZMAX                = Maximum diagonal term of Kg
C3
C4 RET. ARG.  PG(NOM) (modified)
C9  Jan. 99   S. GRILLI, INLN
CLDBNODP SUB. WHICH IMPOSE DOUBLE NODE COMPATIBILITY ON PG
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
      COMMON /MAILLE/ DUM2(NOMM*3+MOMM*2+18),
     .                IDUM3(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM4(84)
      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION U(NOM),PG(NOM)
C
      SAVE
C
      DATA ZERO/0.D0/
C
C     Loop on double/triple nodes
C
      DO I=1,NOM
C
         ID = LXYZ(I,1)
         IF(ID.NE.0) THEN
            IFI = INFACE(I)
            IF(IFI.EQ.1) THEN
C
C              Surface element with double/triple node
C
               DO K=1,2
C
C                 D-N on the free surface : u(K)=u(1)
C
                  IR = LXYZ(I,K)
                  IF(IR.NE.0) THEN
                     PG(IR) = ZMAX*U(I)
                  END IF
               END DO
            ELSE
C
C              Non-surface
C
               IT = LXYZ(I,2)
               IF(IT.EQ.0.AND.INFACE(ID).GT.IFI) THEN
C
C                 Double node not yet treated
C
                  IR = ID
C
C                 N-N on the lat. bn. bottom : u(4)=u(K)
C
                  PG(IR) = ZERO
CSYM
               ELSE IF((IT.NE.0.AND.IFI.EQ.6)
     .             .OR.(ISYM.EQ.1.AND.IT.NE.0.AND.IFI.EQ.5)) THEN
C
C                 Triple node on bottom
C                 N-N on the lat. bn. bottom : u(4)=u(K)
C
                  DO K=1,2
                     IR     = LXYZ(I,K)
                     PG(IR) = ZERO
                  END DO
               END IF
            END IF
         END IF
      END DO
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE SORT(PSOL,UBC,QBC)
C----------------------------------------------------------------------
C0  SORTF     SORT
C1  PURPOSE   Sort the unknowns after solving regarding their type
C2  CALL      CALL SORT(PSOL,UBC,QBC)
C3  CALL ARG. PSOL(NOM)           = Results of the BEM computation
C3            UBC(NOM),QBC(NOM)   = Contain Boundary conditions
C3            NOM                 = Discr. nb. of nodes
C4  RET. ARG. UBC(NOM)            = Nodal u over the boundary
C4            QBC(NOM)            = Nodal q over the boundary
C9  Jan. 99   S. GRILLI, INLN
CLSORT SUB. WHICH SORTS RESULTS OF THE 3D-BEM
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
      COMMON /MAILLE/ DUM2(NOMM*3+MOMM*2+18),
     .                IDUM3(MOMM*MNOD*MNOD+MOMM*7+NOMM*2+66),
     .                INODF(6,2),IBCOND(6)
      COMMON /OPTION/ ISYM,IPOST,IVECT
      COMMON /BOTFCT/ DUMBOT(NXM*NYM+2),IBOT
C
      DIMENSION PSOL(NOM),UBC(NOM),QBC(NOM)
C
      SAVE
C
C     Loop over the 6 boundaries
CSYM...
      KMAX = 6
      IF(IBOT.EQ.10) KMAX = KMAX-1
      IF(ISYM.EQ.1) KMAX = KMAX-1
C
      DO K=1,KMAX
         J1 = INODF(K,1)  
         J2 = INODF(K,2)  
C  
         IF(IBCOND(K).EQ.1) THEN  
C  
C           Dirichlet boundary condition  
C  
            DO J=J1,J2  
               QBC(J) = PSOL(J)
            END DO
         ELSE
C
C           Neuman boundary condition
C
            DO J=J1,J2  
               UBC(J) = PSOL(J)
            END DO
         END IF
      END DO
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE DBNODAX(PG,U)
C----------------------------------------------------------------------
C0  DBNODPF   DBNODAX
C1  PURPOSE   Impose the compatibility conditions at the domain double
C1            nodes, for the L.H.S. PG only. Adapted for FMA
C2  CALL      CALL DBNODP(PG,U)
C3  CALL ARG. PG(NOM)             = Global system vector
C3            U(NOM)              = Either Phi or Phit
C3            ZMAX                = Maximum diagonal term of Kg
C3
C4 RET. ARG.  PG(NOM) (modified)
C9  Jan. 99   S. GRILLI, INLN
CLDBNODP SUB. WHICH IMPOSE DOUBLE NODE COMPATIBILITY ON PG
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
      COMMON /MAILLE/ DUM2(NOMM*3+MOMM*2+18),
     .                IDUM3(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM4(84)
      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      DIMENSION U(NOM),PG(NOM)
C
      SAVE
C
      DATA ZERO/0.D0/
C
C     Loop on double/triple nodes
C
      DO I=1,NOM
C
         ID = LXYZ(I,1)
         IF(ID.NE.0) THEN
            IFI = INFACE(I)
            IF(IFI.EQ.1) THEN
C
C              Surface element with double/triple node
C
               DO K=1,2
C
C                 D-N on the free surface : u(K)=u(1)
C
                  IR = LXYZ(I,K)
                  IF(IR.NE.0) THEN
C                     PG(IR) = ZMAX*U(I)
                     PG(IR) = ZMAX*U(IR)
                  END IF
               END DO
            ELSE
C
C              Non-surface
C
               IT = LXYZ(I,2)
               IF(IT.EQ.0.AND.INFACE(ID).GT.IFI) THEN
C
C                 Double node not yet treated
C
C                 N-N on the lat. bn. bottom : u(4)=u(K)
C
                  PG(ID) = ZMAX*U(ID) - ZMAX*U(I)
CSYM
               ELSE IF((IT.NE.0.AND.IFI.EQ.6)
     .             .OR.(ISYM.EQ.1.AND.IT.NE.0.AND.IFI.EQ.5)) THEN
C
C                 Triple node on bottom
C
C                 N-N on the lat. bn. bottom : u(4)=u(K)
C
                  DO K=1,2
                     IR = LXYZ(I,K)
                     PG(IR) = ZMAX*(U(IR) - U(I))
                  END DO
               END IF
            END IF
         END IF
      END DO
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE DEFSPA()
C----------------------------------------------------------------------
C     DEFSPA define the (sparse approximation) matrix of the assembly
C     matrix needed for preconditioning GMRES
C
C     used for debugging, still useful ?
C
C     CF03
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      INCLUDE 'param.inc'

      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
      COMMON /MAILLE/ DUM2(NOMM*3+MOMM*2+18),
     .                IDUM3(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM4(84)
      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
      COMMON /RMPREC/ RM(NOMM),SPA(NOMM)
C
C
      SAVE
C
      DATA ZERO/0.D0/
C
C
C     initialization of spa, sparse approximation of A
C     spa=near field(A)
C     preliminary preconditioning with n*n matrix spa as before
C     SSOR preconditioner by the left
C     ...-> to be improved
C
         IWITHBEM=0
         IF (IWITHBEM.EQ.1) THEN
         ELSE
            DO I=1,NOM
	      IF (INCOND(I).NE.1) THEN
                 SPA(I) = SPA(I)-RM(I)
	      END IF
            END DO
C
            CALL DBNODSPA()
C           -----------
         END IF
C
      RETURN
C
      END
C----------------------------------------------------------------------
      SUBROUTINE DBNODSPA()
C----------------------------------------------------------------------
C0  DBNODKF   DBNODSPA
C1  PURPOSE   Impose double node compatibility in the sparse precond. matrix
C1            according what type of B.C. is imposed on the 6 boundaries
C1            of 3D-BEM (IBCOND(K)=1: u imposed, IBCOND(K)=2: q imposed)
C2  CALL      CALL DBNODK(ZKG)
C3  CALL ARG. NOM                 = Discr. nb. of nodes
C3            NOMM                = Discr. max nb. of nodes
C3            IBCOND(6)           = Type of B.C. (Dir. or Neum.)
C3            ZKG(NOM,NOM)        = Global K system  matrix
C3            ZKQG(NOM,NOM)       = Neuman system matrix for dble.nodes
C3            ZMAX                = Max diag. element of ZKG
C4  RET. ARG. ZKG,ZMAX
C9  Jan. 99   S. GRILLI, INLN
CLDBNODK SUB. WHICH IMPOSES DOUBLE NODE COMPATIBILITY FOR THE 3D-BEM
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)


      include 'param.inc'

      COMMON /DATGEN/ DUM1(5),IDUM1(8),NOM,IDUM2
      COMMON /MAILLE/ DUM2(NOMM*3+MOMM*2+18),
     .                IDUM3(MOMM*MNOD*MNOD+MOMM*7),LXYZ(NOMM,2),
     .                IDUM4(84)
      COMMON /DBNODE/ ZMAX,INCOND(NOMM),INFACE(NOMM)
      COMMON /RMPREC/ RM(NOMM),SPA(NOMM)
      COMMON /OPTION/ ISYM,IPOST,IVECT
C
      SAVE
C
      DATA ZERO/0.D0/,EPS/1.D-08/
C
C     Locate maximum diagonal term
C
      ZMAX = ZERO
C
      DO I=1,NOM
         IF(ABS(SPA(I)).GT.ZMAX) ZMAX = ABS(SPA(I))
      END DO
C
C     Loop on double/triple nodes
C
      DO I=1,NOM
C
         ID = LXYZ(I,1)
         IF(ID.NE.0) THEN
            IFI = INFACE(I)
            IF(IFI.EQ.1) THEN
C
C              Surface element with double/triple node
C
               DO K=1,2
C
C                 D-N on the free surface : u(K)=u(1)
C
                  IR = LXYZ(I,K)
                  IF(IR.NE.0) THEN
                     SPA(IR) = ZMAX
                  END IF
               END DO
            ELSE
C
C              Neuman faces
C
               IT = LXYZ(I,2)
               IF(IT.EQ.0.AND.INFACE(ID).GT.IFI) THEN
C
C                 Double node not yet treated
C
                  IR = ID
C
C                 N-N on the lat. bn. bottom : u(4)=u(K)
C
                  SPA(IR) =  ZMAX
CSYM
               ELSE IF((IT.NE.0.AND.IFI.EQ.6)
     .             .OR.(ISYM.EQ.1.AND.IT.NE.0.AND.IFI.EQ.5)) THEN
C
C                  Triple node on bottom
C                  N-N on the lat. bn. bottom : u(4)=u(K)
C
                   DO K=1,2
                      IR = LXYZ(I,K)
                      SPA(IR) =  ZMAX
                   END DO
                END IF
            END IF
         END IF
      END DO
C
      RETURN
C
      END
