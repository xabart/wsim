C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################

      REAL*8          AP(NFMAX,NHMAX),OP(NFMAX),SP(NHMAX),ZKWAVE(NFMAX),
     .                DISTF,TFOC,ALPHAF,WMIDTH,OS(MYMAX),OST(MYMAX),
     .                OSTT(MYMAX),DAMP,DAMPT,DAMPTT,DELFR
      INTEGER*4       NFR,NH,IFREQ
      COMMON /PADDLE/ AP,OP,SP,ZKWAVE,
     .                DISTF,TFOC,ALPHAF,WMIDTH,OS,OST,
     .                OSTT,DAMP,DAMPT,DAMPTT,DELFR,NFR,NH,IFREQ
 
