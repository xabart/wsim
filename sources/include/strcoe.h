C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################

      REAL*8          X(MAXT),XF1(MAXN,MAXT),XF2(MAXN,MAXT),
     .                QBAR,WT,DPT,UBB,THETA(MAXN),ETA(MAXN),
     .                DTHETA,WH,OMEGA2
      INTEGER         NN,NTHTS
      COMMON /STRCOE/ X,XF1,XF2,NN,NTHTS,QBAR,WT,DPT,UBB,THETA,ETA,
     .               DTHETA,WH,OMEGA2
