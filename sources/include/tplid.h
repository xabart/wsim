
C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################

      REAL*8         VS(NOMM,3),VM(NOMM,3),VN(NOMM,3),DVS(NOMM),
     .               DVM(NOMM),DVN(NOMM),XSS(NOMM,3),XMM(NOMM,3),
     .               XSM(NOMM,3),XKA(NOMM),
     .               VCSSN(NOMM),VCMMN(NOMM),VCSMN(NOMM)
      COMMON /TPLID/ VS,VM,VN,DVS,DVM,DVN,XSS,XMM,XSM,XKA,
     .               VCSSN,VCMMN,VCSMN
