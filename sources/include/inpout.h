C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################

      INTEGER*4       IO1,IO5,IO6,IO7,IO8,
     .     IO10,IO11,IO12,IO13,IO14,IO15,IO16,
     .     IO21,IO22,IO23,IO24,
     .     IO60,IO61,IO62,IO63,
     .     IO70,IO71,IO72,
     .     IO80,IO99
      DATA IO1/1/,IO5/5/,IO6/6/,IO7/7/,IO8/8/,
     .     IO10/10/,IO11/11/,IO12/12/,IO13/13/,
     .     IO14/14/,IO15/15/,IO16/16/,
     .     IO21/21/,IO22/22/,IO23/23/,IO24/24/,
     .     IO60/60/,IO61/61/,IO62/62/,IO63/63/,
     .     IO70/70/,IO71/71/,IO72/72/,
     .     IO80/80/,IO99/99/



