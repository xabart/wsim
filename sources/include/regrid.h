C###############################################################################
C#                                                                             #
C#     X Barthelemy, WRL 03/10                                                 #
C#                                                                             #
C###############################################################################

      REAL*8    XYZNDR(NOMM,3),XICONR(MOMM,2),
     &          PHIR(NOMM),PHINR(NOMM),PHITR(NOMM),PHITNR(NOMM),
     &          PHISR(NOMM),PHIMR(NOMM),PHISSR(NOMM),PHIMMR(NOMM),
     &          PHINSR(NOMM),PHINMR(NOMM),PHITSR(NOMM),PHITMR(NOMM),
     &          PHISMR(NOMM),PHINNR(NOMM),
     &          PRESR(NOMM),DPRDTR(NOMM),UVWR(3,NOMM),DUVWDTR(3,NOMM)
      integer   IGCONR(NOMM,MNOD,MNOD),
     .                ILCONR(MOMM,4),IREG(NOMM),NOMP,NXP,NYP
      COMMON /REGRD/  XYZNDR,XICONR,
     .          PHIR,PHINR,PHITR,PHITNR,PHISR,PHIMR,PHISSR,PHIMMR,PHINSR,
     .          PHINMR,PHITSR,PHITMR,PHISMR,PHINNR,
     .          PRESR,DPRDTR,UVWR,DUVWDTR,
     .                IGCONR,
     .                ILCONR,IREG,NOMP,NXP,NYP
