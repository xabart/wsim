C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################

      REAL*8          XYZNOD(NOMM,3),XICON(MOMM,2),XSTART(6,3)
      INTEGER*4       IGCON(MOMM,MNOD,MNOD),ILCON(MOMM,4),
     .                IFACE(MOMM),ISUB(MOMM),INTG(MOMM),
     .                LXYZ(NOMM,2),ICUMUL(6),NDST(6,2),IDST(6,3),
     .                JSLIM(6),NELEF(6),NNODF(6),IELEF(6,2),
     .                INODF(6,2),IBCOND(6)    
      COMMON /MAILLE/ XYZNOD,XICON,XSTART,
     .                IGCON,ILCON,
     .                IFACE,ISUB,INTG,
     .                LXYZ,ICUMUL,NDST,IDST,
     .                JSLIM,NELEF,NNODF,IELEF,
     .                INODF,IBCOND
