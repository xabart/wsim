C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################
   
      REAL*8         FN0XI(NDM,NINTM),FN0ET(NDM,NINTM),
     .               FN1XI(NDM,NINTM),FN1ET(NDM,NINTM),
     .               FIRIP(MNODM,NINTM,NINTM),DFIXIP(MNODM,NINTM,NINTM),
     .               DFIETP(MNODM,NINTM,NINTM),
     .               XYZPR(NINTM,NINTM,3),
     .               ZNPR(NINTM,NINTM,3),ZJACPR(NINTM,NINTM)
      COMMON /FSCUB/ FN0XI,FN0ET,FN1XI,FN1ET,FIRIP,DFIXIP,DFIETP,
     .               XYZPR,ZNPR,ZJACPR
