C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################

      REAL*8          SINBR(4),SINBF(4),COSBR(4),COSBF(4),VX(4),
     .                VZ(4),TIME,TDAMP,TDOWN,UPNEW(4,MYMAX),
     .                DUPDT(4,MYMAX),UB(4)
      INTEGER*4       IO6,IFLAGS,IFLAGV,IPTYP,ILOOP,ILOOP0,ILOOP1

      COMMON /SYSTEM/ SINBR,SINBF,COSBR,COSBF,VX,VZ,TIME,TDAMP,TDOWN,
     .                UPNEW,DUPDT,UB,IO6,IFLAGS,IFLAGV,IPTYP,ILOOP,
     &			ILOOP0,ILOOP1
