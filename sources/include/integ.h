C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################


      REAL*8          ETARP(NINTM),WEIGTR(NINTM),PHIP12(NINTM),
     .                PHIP23(NINTM),RMIP12(NINTM),RMIP23(NINTM),
     .                RIJP12(NINTM,NINTM),RIJP23(NINTM,NINTM),
     .                XI12(NINTM,NINTM,MNOD),XI23(NINTM,NINTM,MNOD),
     .                ET12(NINTM,NINTM,MNOD),ET23(NINTM,NINTM,MNOD)
      COMMON /INTEG/  ETARP,WEIGTR,PHIP12,
     .                PHIP23,RMIP12,RMIP23,
     .                RIJP12,RIJP23,
     .                XI12,XI23,
     .                ET12,ET23


