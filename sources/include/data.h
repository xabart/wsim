C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################

      REAL*8   ZERO,ONE,TWO,THREE,FOUR,FIVE,SIX,EIGHT,TEN,SEIZE,
     .         PI,PI2,PIS8,PIS2,PII4,
     .         HALF,QUART,TWTHI,OTHREE,P001,TENTH,S3,S64,BIG,
     .         PDEG,DLOG2I,CRAT,EPS,EPS2
      INTEGER  MOT,ITEMAX
      DATA ZERO/0.D0/,ONE/1.D0/,TWO/2.D0/,THREE/3.D0/,FOUR/4.D0/,
     .     FIVE/5.D0/,SIX/6.D0/,EIGHT/8.D0/,TEN/10.D0/,SEIZE/16.D0/,
     .     PI/3.1415926535898D0/,PI2/6.2831853071795D0/,
     .     PIS8/0.3926990816987241D0/,
     .     PIS2/1.570796326794896D0/,PII4/0.7957747154594768D-01/,
     .     HALF/0.5D0/,QUART/0.25D0/,TWTHI/0.6666666666666667D0/,
     .     TENTH/0.1D0/,P001/0.001D0/,
     .     OTHREE/0.3333333333333333D0/,S3/0.3333333333333333D0/,
     .     S64/64.D0/,BIG/1.D10/,
     .     MOT/8/,ITEMAX/20/,
     .     PDEG/180.D0/,DLOG2I/3.321928094D0/,CRAT/0.4999D0/,
     .     EPS/1.D-08/, EPS2/1.D-03/

