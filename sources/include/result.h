C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################

      REAL*8    PG(NOMM),
     .          PHI(NOMM),PHIN(NOMM),PHIT(NOMM),PHITN(NOMM),
     .          PHIS(NOMM),PHIM(NOMM),PHISS(NOMM),PHIMM(NOMM),
     .          PHINS(NOMM),PHINM(NOMM),PHITS(NOMM),PHITM(NOMM),
     .          PHISM(NOMM),PHINN(NOMM),
     .          PRES(NOMM),DPRDT(NOMM),UVW(3,NOMM),DUVWDT(3,NOMM)
      COMMON /RESULT/ PG,
     .          PHI,PHIN,PHIT,PHITN,PHIS,PHIM,PHISS,PHIMM,PHINS,
     .          PHINM,PHITS,PHITM,PHISM,PHINN,
     .          PRES,DPRDT,UVW,DUVWDT
