C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################

      REAL*8          THETA,BSLIDE,WSLIDE,TSLIDE,DSLIDE,ZL2,RHOL,GAMMA,
     .                CM,CD,ZEPS,ZMU,ZMUC,ZAL,UT,A0,S0,T0,WLEGTH,
     .                ZKX,ZKY,STHET,CTHET,TTHET,COSHEP,COSHMU,
     .                XINIT,TSLMAX,S,ST,STT,XBEGIN,XEND,XCENTR,XI0,
     .                SDELTT,DELTAS,ZKS,TSLOPE,DELTAT,AINIT,TANHXI,
     .                ZKRAMP,UINIT,TANHTI,SINIT,COSHTI,RATIO2
      COMMON /LNDSLD/ THETA,BSLIDE,WSLIDE,TSLIDE,DSLIDE,ZL2,RHOL,GAMMA,
     .                CM,CD,ZEPS,ZMU,ZMUC,ZAL,UT,A0,S0,T0,WLEGTH,
     .                ZKX,ZKY,STHET,CTHET,TTHET,COSHEP,COSHMU,
     .                XINIT,TSLMAX,S,ST,STT,XBEGIN,XEND,XCENTR,XI0,
     .                SDELTT,DELTAS,ZKS,TSLOPE,DELTAT,AINIT,TANHXI,
     .                ZKRAMP,UINIT,TANHTI,SINIT,COSHTI,RATIO2

