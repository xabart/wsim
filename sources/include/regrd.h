C###############################################################################
C#                                                                             #
C#     S. Grilli, 1/99, INLN, 6/02 LSEET                                       #
C#                                                                             #
C#     adapted for Fast Multipole Algorihtm (FMA), C. Fochesato, 2003, CMLA    #
C#     by coupling with code PMTA.                                             #
C#     parenthesis indicates not (yet) available options, which exits in PMTA  #
C#                                                                             #
C#     adapted to make a CTL component, C. Kassiotis, 2007                     #
C#									       #
C# Author:  Christophe Kassiotis                                               #
C#          LMT - Cachan        |     Institute of Scientific Computing        #
C#          ENS - Cachan        |     Technische Universitaet Braunschweig     #
C#          Cachan, France      |     Braunschweig, Germany                    #
C#  Email:  kassiotis@lmt.ens-cachan.fr                                        #
C#                                                                             #
C#  Copyright (c) 2007. All rights reserved. No warranty. No                   #
C#  liability.                                                                 #
C#                                                                             #
C###############################################################################
C     common to store the vector for rigid mode technique RM
C     and preconditioning matrix SPA, CF03
C###############################################################################

      REAL*8          XYZNDR(NOMM,3),PHIR(NOMM),PHINR(NOMM),
     .                XICONR(MOMM,2)
      INTEGER         IGCONR(NOMM,MNOD,MNOD),ILCONR(MOMM,4),
     .                IREG(NOMM),NOMP,NXP,NYP

      COMMON /REGRD/  XYZNDR,PHIR,PHINR,XICONR,
     .                IGCONR,ILCONR,IREG,NOMP,NXP,NYP




