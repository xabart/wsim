/*
 * PMTAfile.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAfile.c,v 1.3 1994/12/02 18:07:22 wrankin Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAfile.c,v $
 * Revision 1.3  1994/12/02  18:07:22  wrankin
 * changed particle generation to use the more common rand48() calls.
 *
 * Revision 1.2  1994/11/11  20:14:39  lambert
 * GenerateParticles redeclared so doesn't need to look at globals:
 * PTablePtr GenerateParticles(int NumParticles,int seed, double CubeLength)
 *
 * Forces and potentials in GenerateParticles don't have to be zeroed
 * in the particle table, as AdjustParticles does it anyways each
 * timestep. A particletable is now returned from the function,
 *
 * ReadParticles() never worked.  Fixed it and tested it.  It now
 * reads in Brookhaven PDB format files from given file descriptor.
 * It also computes a CubeLength that surrounds the given particles.
 * If this CubeLength used in PMTA, one particle will be right at
 * the edge of the big cube, which if don't use periodic boundary
 * conditions, particle may go outside of cell in first timestep.
 *
 * Thus user might want to increase Boxlength by a little to avoid
 * particle going outside cell.  Currently there is no method
 * to dynamically modify the Boxlength within PMTA.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 *
 */


#include "include/PMTA.h"

PTablePtr GenerateParticles(int NumParticles,int seed, double CubeLength){
    int i;
    double hCLx,hCLy,hCLz,max;
    PTablePtr ParticleTable;

    if(Verbose) 
        fprintf(Output,"Generating %d random particles...\n",NumParticles);

    srand48(Seed=seed);
    
    ParticleTable = (PTablePtr) malloc(NumParticles * sizeof(PTable));
    if(ParticleTable == NULL)
        ERROR("(GenerateParticles) malloc failure, SIZE:",NULL,
            NumParticles * sizeof(PTable));

    /*
     * NOTE: this function ensures that particles do not lie on the 
     * outer surfaces of the level 0 cube 
     */
    hCLx = CUBE_CTR_X - CubeLength/2;
    hCLy = CUBE_CTR_Y - CubeLength/2;
    hCLz = CUBE_CTR_Z - CubeLength/2;
    max = ((double) MAXINT) + 2;   
    for(i=0;i < NumParticles;i++){
        ParticleTable[i].p.x =  drand48() * CubeLength + hCLx;
        ParticleTable[i].p.y =  drand48() * CubeLength + hCLy;
        ParticleTable[i].p.z =  drand48() * CubeLength + hCLz;
        if ( drand48() >= 0.5 )
            ParticleTable[i].q = 1.0;
        else
            ParticleTable[i].q = -1.0;
    }
    return(ParticleTable);
    
}


PTablePtr ReadParticles(FILE *Input, int *NumPart, double *CubeLength){
    int particles,status,done;
    PTablePtr ParticleTable;
    char buffer[50];
    int NumParticles;
    double minx=CUBE_CTR_X,miny=CUBE_CTR_Y,minz=CUBE_CTR_Z,
	   maxx=CUBE_CTR_X,maxy=CUBE_CTR_Y,maxz=CUBE_CTR_X; /* Use these to find cubelength */
    
    if(Verbose) fprintf(Output,"Reading particle data...\n");

    ParticleTable = NULL;
    particles = PTABLE_INC;     /* particle table size */
    NumParticles = 0;           /* number of particles read from data file */
    done = FALSE;               /* have we reached the end-of-file? */


    ParticleTable = (PTablePtr) malloc(particles * sizeof(PTable));
    if(ParticleTable == NULL) 
	ERROR("(ReadParticles) malloc failure, SIZE:",NULL,
	      particles * sizeof(PTable));

    do {
        while(NumParticles < particles && !done){
            status = fscanf(Input,"%49s",buffer);
            if(status == EOF)
                done = TRUE;
            else if(strcmp(buffer,"ATOM") == 0){
                status = fscanf(Input,"%49s %49s %49s %49s %lg %lg %lg "
                    "%49s %49s %49s",buffer,buffer,buffer,buffer,
                    &ParticleTable[NumParticles].p.x,
                    &ParticleTable[NumParticles].p.y,
                    &ParticleTable[NumParticles].p.z,buffer,buffer,buffer);
                if(status != 10) 
                    done = TRUE;
                else {
                    ParticleTable[NumParticles].p.x += CUBE_CTR_X;
                    ParticleTable[NumParticles].p.y += CUBE_CTR_Y;
                    ParticleTable[NumParticles].p.z += CUBE_CTR_Z;
		    if(ParticleTable[NumParticles].p.x < minx) minx = ParticleTable[NumParticles].p.x;
		    if(ParticleTable[NumParticles].p.y < miny) miny = ParticleTable[NumParticles].p.y;		    
		    if(ParticleTable[NumParticles].p.z < minz) minz = ParticleTable[NumParticles].p.z;

    		    if(ParticleTable[NumParticles].p.x > maxx) maxx = ParticleTable[NumParticles].p.x;
		    if(ParticleTable[NumParticles].p.y > maxy) maxy = ParticleTable[NumParticles].p.y;		    
		    if(ParticleTable[NumParticles].p.z > maxz) maxz = ParticleTable[NumParticles].p.z;

                    /* charge is set randomly to + or - 1.0 */
                    if ( drand48() >= 0.5 )
                        ParticleTable[NumParticles].q = 1.0;
                    else
                        ParticleTable[NumParticles].q = -1.0;

		    /* Don't have to initialize forces/pots, they are zeroed
		       in AdjustParticles() */

                    NumParticles++;
                }
            }
        }
	if(!done) {
	    /*  extend table size by PTABLE_INC particles */
	    particles += PTABLE_INC; 
	    ParticleTable = (PTablePtr) realloc(ParticleTable,
						particles * sizeof(PTable));
	    if(ParticleTable == NULL) 
		ERROR("(ReadParticles) realloc failure, SIZE:",NULL,
		      particles * sizeof(PTable));
	}
    } while(!done);
	

    /* Resize table to exact number of particles */
    if(NumParticles == 0) ERROR("(ReadParticle) zero particles read",NULL,0);
    ParticleTable = (PTablePtr) realloc(ParticleTable,
        NumParticles * sizeof(PTable));
    if(ParticleTable == NULL) 
        ERROR("(ReadParticles) realloc failure, SIZE:",NULL,
            NumParticles * sizeof(PTable));

    *NumPart = NumParticles;
    *CubeLength = fabs(maxx-minx);
    if (*CubeLength < fabs(maxy-miny)) *CubeLength = fabs(maxy-miny);
    if (*CubeLength < fabs(maxz-minz)) *CubeLength = fabs(maxz-minz);    
    return(ParticleTable);
}

void PrintProblemInfo(){
    int i;

    fputc('\n',Output);
    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\nPMTA version 4.0\n\n");
    fprintf(Output,"Problem Information\n\n");

	if(EPMTA){
        fprintf(Output,"*** Executed Enhanced PMTA\n\n");
    } else if(PFMA){
        fprintf(Output,"*** Executed Fast Multipole Algorithm\n\n");
    }
    if(FFT){
        fprintf(Output,"*** Used FFT subroutines\n\n");
    }
	if(BALANCED){
		fprintf(Output,"*** Load Balancing M2L and Direct Computations\n\n");
	}

    fprintf(Output,"Cube Length = %g\n",CubeLength);
    if(!PFMA || EPMTA){
        fprintf(Output,"Theta = %-.10g\n",Theta);
    }
    fprintf(Output,"Number of Multipole Expansion Terms = %d\n",Mp+1);
    fprintf(Output,"Number of Oct Tree Levels = %d\n",NumLevels+1);
    fprintf(Output,"\nNumber of Particles = %d\n",NumParticles);
    fprintf(Output,"Random Seed = %d\n",Seed);
    fprintf(Output,"\nNumber of Processors = %d\n",NumProcessors);
#ifndef SERIAL
    fprintf(Output,"Processor Level = %d\n\n",ProcessorLevel);
#else
    fputc('\n',Output);
#endif

    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\n");

}

void PrintResults(void){
    int i,size;

    fputc('\n',Output);
    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\nForce and Potential Results\n\n");

    if(NumParticles > RESULTS_MAX){
        size = RESULTS_MAX;
    } else {
        size = NumParticles;
    }
    for(i=0;i < size;i++){
        fprintf(Output,"# %-6d, q=%g, p={%g, %g, %g}\n",
            i,ParticleTable[i].q,ParticleTable[i].p.x,
            ParticleTable[i].p.y,ParticleTable[i].p.z);
        fprintf(Output,"ap: v=%-.9g, f={%-.9g,%-.9g,%-.9g}\n",
            ParticleTable[i].v,ParticleTable[i].f.x,
            ParticleTable[i].f.y,ParticleTable[i].f.z);
        if(Direct){
            fprintf(Output,"dt: v=%-.9g, f={%-.9g,%-.9g,%-.9g}\n\n",
                ParticleTable[i].vdirect,
                ParticleTable[i].fdirect.x,ParticleTable[i].fdirect.y,
                ParticleTable[i].fdirect.z);
        } else {
            fputc('\n',Output);
        }
    }

    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\n");

}
