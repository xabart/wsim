/*
 * Prescribed values for BEM (= param.inc)
 *
 * C.Fochesato, CMLA, 2003
 */
#define NXM      41
#define NYM      5
#define NZM      11

#define ISYM     0
#define IBOT     1

#if (ISYM==0 && IBOT!=10)

#define NUMPART  2*(NXM*NYM+NXM*NZM+NYM*NZM)
#define NUMELEM  (NUMPART-4*(NXM+NYM+NZM)+6)

#else

/*
 *    moins de stockage si methode des images pour le fond
 */
#if (IBOT==10 && ISYM==0)
#define NUMPART  (2*(NXM*NYM+NXM*NZM+NYM*NZM)-(NXM*NYM))
#define NUMELEM  (2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))-((NXM-1)*(NYM-1)))
#endif

/*
 *    moins de stockage si methode des images pour la symetrie en y
 */
#if (ISYM==1 && IBOT!=10)
#define NUMPART  (2*(NXM*NYM+NXM*NZM+NYM*NZM)-(NXM*NZM))
#define NUMELEM  (2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))-((NXM-1)*(NZM-1)))
#endif

/*
 *    moins de stockage si methode des images pour la symetrie en y+pour le fond
 */
#if (ISYM==1 && IBOT==10)
#define NUMPART  (2*(NXM*NYM+NXM*NZM+NYM*NZM)-(NXM*NYM)-(NXM*NZM))
#define NUMELEM  (2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))-((NXM-1)*(NYM-1))-((NXM-1)*(NZM-1)))
#endif

#endif

#define NNOD      4
#define MNODM    MNOD*MNOD
/*
 * PMTA.h
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

/*
 * RCS Info: $Id: PMTA.h,v 1.4 1995/01/17 17:13:56 welliott Exp $
 *
 * Revision History:
 *
 * $Log: PMTA.h,v $
 * Revision 1.4  1995/01/17  17:13:56  welliott
 * Commented out random.h for KSR version (KSR has changed include files)
 *
 * Revision 1.3  1994/12/02  18:06:09  wrankin
 * added CLK_TCK definition and include <sys/times> for timing fixes
 *
 * Revision 1.2  1994/11/11  19:34:20  lambert
 * min(a,b) was system macro, so redefining it causes warnings with
 * some compilers.  Redefined to Min(a,b)
 *
 * No need to have Outliers as global variable, removed from PMTA.h.
 *
 * iTheta made global when library interface cleaned up.
 *
 * ParallelSetup and Parallel were incorrectly defined as returning
 * void POINTERS, so prototype fixed.  A couple other prototypes
 * cleaned up.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 * Modified by C.Fochesato, CMLA, 2003 : Bem structure definition
 */



#ifndef PMTA_HDR
#define PMTA_HDR

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <stdlib.h>
#include <values.h>             /* for MAXINT */
#ifndef SERIAL
/* #include <random.h>*/ /* KSR isn't the same */
#include <pthread.h>
#include <ksr/sqrt.h>
#include <ksr/ksr_timers.h>
#include <ksr/pmon.h>
#else
#include <sys/time.h>           /* for timing */
#include <sys/times.h>
#include <sys/resource.h>       /* for timing */
#endif

/*
 * Constant definitions
 */

#define TRUE            1
#define FALSE           0

#define LEVELS_MAX      9

#define PTABLE_INC      5000

#define ILIST_INC       16

#define RESULTS_MAX     20

#define DIR_X           1
#define DIR_Y           2
#define DIR_Z           4

#define CUBE_CTR_X      5.5 
#define CUBE_CTR_Y      0.5
#define CUBE_CTR_Z      0.0

#define PTHREAD_START   0
#define PTHREAD_END     1

#define TIME_NUM        11
#define TIME_USER       0
#define TIME_WALL       1

#define TIME_CS         0
#define TIME_PC         1
#define TIME_CPL        2
#define TIME_AP			3
#define TIME_CM         4
#define TIME_CF         5
#define TIME_CFD        6
#define TIME_CFM        7
#define TIME_CR         8
#define TIME_AM         9
#define TIME_DF         10

#define PMON_NUM        2

#define PMON_AM         0
#define PMON_DM         1

#define Min(a, b)       ((a) < (b) ? (a) : (b))

#ifndef CLK_TCK
#define CLK_TCK 60
#endif

/*
 * Structure declarations
 */

typedef struct vector {
    double  x,y,z;
} Vector;

typedef struct sphvector {
    double  r,a,b;
} SphVector;

typedef struct complex {
    double  x,y;
} Complex;

typedef Complex **Mtype;

typedef struct ptable {
    Vector p;                   /* position */
    int c;                      /* cell identifier */
    double q;                   /* charge */
    Vector fdirect;             /* exact force by direct method */
    double vdirect;             /* exact potential by direct method */
    Vector f;                   /* force */
    double v;                   /* potential */
} PTable;

typedef PTable *PTablePtr;

typedef struct etable {
    Vector p;                   /* position of the center*/
    int c;                      /* cell identifier */
} ETable;

typedef ETable *ETablePtr;

typedef struct particle {       /* particle list entry : */
    Vector p;                   /* position */
    double q;                   /* charge */
    Vector f;                   /* force */
    double v;                   /* potential */
    int id;                     /* particle identifier */
} Particle;

typedef Particle *ParticlePtr;

#ifndef SERIAL
typedef struct smallparticle {  /* small particle list entry : */
    Vector p;                   /* position */
    double q;                   /* charge */
} SmallParticle;

typedef SmallParticle *SmallParticlePtr;
#endif

typedef struct bem {            /* bem info */
    int nbelts;                 /* number of cell elements */
    int nnie;                   /* number of nodes contributing by the elements */
    int *ielts;                 /* array of cell elements indexes*/
    int *inodes;                /* array of cell nodes indexes*/
    int *jgcoef;                /* array of cell nodes contributing by the elements */
} Bem;

typedef double *Coefs;          /* coefficients of BEM contributions defined as double */

typedef struct cell {           /* cell table entry : */
#ifndef SERIAL
__align128
#endif
    Vector p;                   /* center of cell */
    Mtype m;                    /* multipole expansion of cell particles */
#if (ISYM==1 || IBOT==10)
    Mtype msym;                 /* multipole expansion for y-symmetrical of cell particles */
#if (ISYM==1 && IBOT==10)
    Mtype msym2;                 /* multipole expansion for y and flat bottom symmetrical of cell particles */
    Mtype msym3;                 /*  */
#endif
#endif
    Mtype l;                    /* local expansion of distant particles */
    int vl;                     /* is multipole expansion valid (non-zero)? */
    int n;                      /* number of cell particles */
    ParticlePtr pa;             /* array of cell particles */
#ifndef SERIAL
    SmallParticlePtr spa;       /* array of cell particles in small format */
#endif
    Bem mesh;                   /* mesh info for bem */
    int iddt;                   /* number of double direct interactions */
    int isdt;                   /* number of single direct interactions */
    int ime;                    /* number of expansion based interactions */
    int lvl;                    /* is local expansion valid (non-zero)? */
    int temp_iddt;              /* temporary variable in PMTAcell_setup.c */
    int temp_isdt;              /* temporary variable in PMTAcell_setup.c */
    int temp_ime;               /* temporary variable in PMTAcell_setup.c */
    Coefs coefqselfdirect;      /* BEM coefficients for the current cell */
    Coefs coefuselfdirect;      /* BEM coefficients for the current cell */
    int *iddirect;              /* array of double direct interactions */
    Coefs *coefqddirect1;       /* BEM coefficients for double direct interactions, 1st direction */
    Coefs *coefqddirect2;       /* BEM coefficients for double direct interactions, 2st direction */
    Coefs *coefuddirect1;       /* BEM coefficients for double direct interactions, 1st direction */
    Coefs *coefuddirect2;       /* BEM coefficients for double direct interactions, 2st direction */
    int *isdirect;              /* array of single direct interactions */
    Coefs *coefqsdirect;        /* BEM coefficients for single direct interactions */
    Coefs *coefusdirect;        /* BEM coefficients for single direct interactions */
    int *imultipole;            /* array of multipole interactions */
    Coefs coefqxmultipole;      /* BEM coefficients for multipole interactions */
    Coefs coefqymultipole;      /* BEM coefficients for multipole interactions */
    Coefs coefuxmultipole;      /* BEM coefficients for multipole interactions */
    Coefs coefuymultipole;      /* BEM coefficients for multipole interactions */
#if (ISYM==1 || IBOT==10)
    Coefs coefqxmultisym;      /* BEM coefficients for symmetrical multipole interactions */
    Coefs coefqymultisym;      /* BEM coefficients for symmetrical multipole interactions */
    Coefs coefuxmultisym;      /* BEM coefficients for symmetrical multipole interactions */
    Coefs coefuymultisym;      /* BEM coefficients for symmetrical multipole interactions */
#if (ISYM==1 && IBOT==10)
    Coefs coefqxmultisym2;      /* BEM coefficients for symmetrical multipole interactions */
    Coefs coefqymultisym2;      /* BEM coefficients for symmetrical multipole interactions */
    Coefs coefuxmultisym2;      /* BEM coefficients for symmetrical multipole interactions */
    Coefs coefuymultisym2;      /* BEM coefficients for symmetrical multipole interactions */
    Coefs coefqxmultisym3;      /* BEM coefficients for symmetrical multipole interactions */
    Coefs coefqymultisym3;      /* BEM coefficients for symmetrical multipole interactions */
    Coefs coefuxmultisym3;      /* BEM coefficients for symmetrical multipole interactions */
    Coefs coefuymultisym3;      /* BEM coefficients for symmetrical multipole interactions */
#endif
#endif
    double h;                   /* length of cell */
} Cell;

typedef Cell *CellPtr;

typedef struct pthread_work {   /* computational responsibility of processor */
    int ptstart;                /* start of particle load in ParticleTable */
    int ptend;                  /* end of particle load in ParticleTable */
    int etstart;                /* start of element load in ElementTable */
    int etend;                  /* end of element load in ElementTable */
    int **ct;                   /* cell load at each level of CellTable */
    int lb_ct_mstart;			/* M2L load balance: CellTable start */
    int lb_ct_mend;				/* M2L load balance: CellTable end */
    int lb_ct_dstart;			/* Direct load balance: CellTable start */
    int lb_ct_dend;				/* Direct load balance: CellTable end */
    Complex *mt1;               /* these variables are needed by a */
    Complex **ms;               /* specific pthread */
    double **mlegendre;
    Mtype transferf;
    double **transferb;
    double *finput;
    Mtype *FLocal_accum;
    char *Mm;                   /* pointers to the block of multipole */
    char *Ml;                   /* and local expansions per processor */
} PthreadWork;

typedef PthreadWork *PthreadWorkPtr;

#ifndef SERIAL
/*
 * Global variable declarations
 */

__shared extern int                 FlagInit;
__shared extern int                 Direct, Results, PFMA, EPMTA, COULOMB_INIT;
__shared extern int                 Verbose, Pmon, BALANCED;
__shared extern int                 Mp, NumLevels, NumParticles, NumElements;
__shared extern int                 Seed;
__shared extern double              CubeLength, OutVcutoff, OutFcutoff;
__shared extern int                 NumProcessors, ProcessorLevel;
__shared extern int					Power2Processors;
__shared extern int                 Power8[LEVELS_MAX+2];
__shared extern int                 LevelLocate[LEVELS_MAX+2];
__shared extern FILE                *Output;
__shared extern double              iTheta,Theta;
__shared extern double              **MYconstant;
__shared extern double              ****Ms2constant;
__shared extern double              ****Ms3constant1, ****Ms3constant2;
__shared extern CellPtr             CellTable;
__shared extern PTablePtr           ParticleTable;
__shared extern ETablePtr           ElementTable;
__shared extern SmallParticlePtr    SmallParticleTable;
__shared extern PthreadWorkPtr      PthreadWorkTable;
__shared extern processor_name_t    *ProcessorTable;
__shared extern pthread_t           *PthreadTable;
__shared extern pthread_barrier_t   PthreadBarrier;
__shared extern int                 **ILevelAnalysis;
__shared extern PMON_DATA           **PmonAnalysis;
__shared extern double              ***Timing;
__private extern int                PthreadNum;
__private extern int                PthreadPTStart, PthreadPTEnd;
__private extern int                PthreadETStart, PthreadETEnd;
__private extern int                **PthreadCT;
__private extern Complex            *Mt1;
__private extern Complex            **Ms;
__private extern double             **MLegendre;
__private extern int                *PthreadILA;
__private extern PMON_DATA          *PthreadPmon;
__private extern double             **PthreadTiming;

/*
 * FFT Version variables
 */
__shared extern int                 FFT;
__shared extern int                 nblocks, fftarray_m, fftsize;
__shared extern int                 *frowsz, *fincrement, *fblocksz;
__shared extern double              *fscale;
__shared extern double              scale;
__shared extern double              **A;
__shared extern double              **PFMA_s3_const1F;
__shared extern double              FCubeLength;
__private extern Mtype              transferf;
__private extern double             **transferb;
__private extern double             *finput;
__private extern Mtype              *FLocal_accum;

#else

/*
 * Global variable declarations for serial execution
 */

extern int                          FlagInit;
extern int                          Direct, Results, PFMA, EPMTA, COULOMB_INIT;
extern int                          Verbose, BALANCED;
extern int                          Mp, NumLevels, NumParticles, NumElements;
extern int                          Seed;
extern double                       CubeLength, OutVcutoff, OutFcutoff;
extern int                          NumProcessors, ProcessorLevel;
extern int							Power2Processors;
extern int                          Power8[LEVELS_MAX+2];
extern int                          LevelLocate[LEVELS_MAX+2];
extern FILE                         *Output;
extern double                       iTheta,Theta;
extern double                       **MYconstant;
extern double                       ****Ms2constant;
extern double                       ****Ms3constant1, ****Ms3constant2;
extern CellPtr                      CellTable;
extern PTablePtr                    ParticleTable;
extern ETablePtr                    ElementTable;
extern PthreadWorkPtr               PthreadWorkTable;
extern int                          **ILevelAnalysis;
extern double                       ***Timing;
extern int                          PthreadNum;
extern int                          PthreadPTStart, PthreadPTEnd;
extern int                          PthreadETStart, PthreadETEnd;
extern int                          **PthreadCT;
extern Complex                      *Mt1;
extern Complex                      **Ms;
extern double                       **MLegendre;
extern int                          *PthreadILA;
extern double                       **PthreadTiming;


/*
 * FFT Version variables
 */
extern int                          FFT;
extern int                          nblocks, fftarray_m, fftsize;
extern int                          *frowsz, *fincrement, *fblocksz;
extern double                       *fscale;
extern double                       scale;
extern double                       **A;
extern double                       **PFMA_s3_const1F;
extern double                       FCubeLength;
extern Mtype                        transferf;
extern double                       **transferb;
extern double                       *finput;
extern Mtype                        *FLocal_accum;
#endif

/*
 * Global function declarations
 */

void InitializeCoulomb(double,int,int,int,int,
           double,int,int,int,int,int,int,int);     /* PMTAcoulomb.c */
void Coulomb(void);                                 /* PMTAcoulomb.c */
void CoulombMultipoleBalance(void);                 /* PMTAbalance.c */
void CoulombDirectBalance(void);                    /* PMTAbalance.c */
void MultipoleSetup(void);                          /* PMTAmultipole_setup.c */
void CellTableSetup(void);                          /* PMTAcell_setup.c */
void CellsSetup(void);                              /* PMTAcell_setup.c */
void PFMAInteractLists(void);                       /* PMTApfma.c */
void PMTAInteractLists(void);	                    /* PMTAinteract.c */
void InteractAnalysis(void);                        /* PMTAanalysis.c */
PTablePtr GenerateParticles(int,int,double);        /* PMTAparticle.c */
PTablePtr ReadParticles(FILE *,int *,double *);     /* PMTAfile.c */
void BemParticles(int, double *, double *);         /* PMTA_BEM.c */
void BemElements(int, int, double *, int *);        /* PMTA_BEM.c */
void PrintProblemInfo();                            /* PMTAfile.c */
void ParallelSetup(void *);                         /* PMTAparallel.c */
void Parallel(void *);                              /* PMTAparallel.c */
void ParticleCells(void);                           /* PMTAparticle.c */
void ParticleCellsCoef(int);                        /* PMTAparticle.c */
void CellPlists(void);                              /* PMTAparticle.c */
void AdjustParticles(void);                         /* PMTAparticle.c */
void CellMultipoles(void);                          /* PMTAmultipole.c */
void MLocal(int, int);                              /* PMTAlocal.c */
void MShiftLocal(int, int);                         /* PMTAlocal.c */
void MForce(int);                                   /* PMTAlocal.c */
void CellForces(void);                              /* PMTAforce.c */
void BalancedCellForces(void);                      /* PMTAbalance.c */
void CellResults(void);                             /* PMTAparticle.c */
void CellParticleAnalysis(void);                    /* PMTAanalysis.c */
void DirectForces(void);                            /* PMTAforce.c */
void DirectWithinCell(int);                         /* PMTAforce.c */
void DirectDoubleCell(int, int);                    /* PMTAforce.c */
void DirectSingleCell(int, int);                    /* PMTAforce.c */
#if (ISYM==0 && IBOT!=10)
void bemkfma_(double *,double *,double *,double *,int *,
              double *,double *,double *,int *,int *,int *,double *);
#else
#if (ISYM==1 && IBOT==10)
void bemksym2_(double *,double *,double *,double *,double *,double *,double *,double *,
              double *,double *,double *,double *,double *,double *,double *,double *,
              int *,double *,double *,double *,int *,int *,int *,double *);
#else
void bemksym_(double *,double *,double *,double *,double *,double *,double *,double *,
              int *,double *,double *,double *,int *,int *,int *,double *);
#endif
#endif
void bemk2_(double *,double *,int *,int *,int *,int *,int *);
void ErrorAnalysis(int,double,double);              /* PMTAanalysis.c */
void TimingAnalysis(void);                          /* PMTAanalysis.c */
void PrintResults(void);                            /* PMTAfile.c */
void FreeCoulomb(void);                             /* PMTAcell_setup.c */
void PthreadStartTiming(int);                       /* PMTAparallel.c */
void PthreadStopTiming(int);                        /* PMTAparallel.c */
void ERROR(char *, char *, int);                    /* PMTAmain.c */
void SolutionBem(double *);                             /* PMTAfile.c */
void InitializeNewStep(void);                       /* PMTAnewstep.c */
void ParticleCellsNewStep(void);                    /* PMTAnewstep.c */
void isinvect_(int *,int *,int *,int *);
/*
 * FFT subroutines
 */

void loc_expF(int, int, Mtype *);                   /* PMTAlocal.c */
void FFTMultipole(int);                             /* PMTAlocal.c */
void IFFTLocal(int, Mtype *);                       /* PMTAlocal.c */

#endif
