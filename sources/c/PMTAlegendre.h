/*
 * PMTAlegendre.h
 *
 * Copyright (c) 1994 Jim Leathrum.
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

/*
 * RCS Info: $Id: PMTAlegendre.h,v 1.1 1994/10/08 04:34:10 wrankin Exp $
 *
 * Revision History:
 *
 * $Log: PMTAlegendre.h,v $
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 *
 */

#ifndef PMTA_LEGENDRE_HDR
#define PMTA_LEGENDRE_HDR

/*
 * Legendre() sets up the Legendre Polynomial
 * adapted from Numerical Recipes in C
 *
 * P is type Mtype, xval is type double, and p is type unsigned int
 * p is the number of terms in the multipole expansion
 */
#define Legendre(P,xval,p)                                              \
{                                                                       \
    int Li, Lj;                                                         \
    double negterm, oddfact, nextodd, sqroot, sqrtterm;                 \
                                                                        \
    negterm = 1.0;                                                      \
    oddfact = 1.0;                                                      \
    nextodd = 1.0;                                                      \
    sqroot = sqrt(1.0 - xval*xval);                                     \
    sqrtterm = 1.0;                                                     \
    for(Li=0;Li <= p;Li++){                                             \
        P[Li][Li] = negterm*oddfact*sqrtterm;                           \
        negterm *= -1.0;                                                \
        oddfact *= nextodd;                                             \
        nextodd += 2.0;                                                 \
        sqrtterm *= sqroot;                                             \
        if(Li < p){                                                     \
        P[Li+1][Li] = xval * (double)(2*Li+1) * P[Li][Li];              \
            for(Lj=Li+2;Lj <= p;Lj++){                                  \
                P[Lj][Li] = (xval*(double)(2*Lj-1)*P[Lj-1][Li] -        \
                    (double)(Lj+Li-1)*P[Lj-2][Li])/(double)(Lj-Li);     \
            }                                                           \
        }                                                               \
    }                                                                   \
}

#endif
