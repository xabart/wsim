/*
 * PMTAcoulomb.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *  
 */

static char rcsid[] = "$Id: PMTAcoulomb.c,v 1.2 1994/11/11 20:04:45 lambert Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAcoulomb.c,v $
 * Revision 1.2  1994/11/11  20:04:45  lambert
 * It used to be that a subset of PMTA's globals were set
 * in PMTAmain.c, and another subset were set by calling
 * InitializeCoulomb(). Now they are all set by calling
 * InitializeCoulomb(), making the interface much cleaner.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 *
*/


#include "include/PMTA.h"

#ifndef SERIAL
/*
 * Global variable definitions
 */

__shared int                Direct = FALSE, Results = FALSE, PFMA = FALSE;
__shared int				EPMTA = FALSE;
__shared int                Verbose = FALSE, Pmon = FALSE;
__shared int                FFT = FALSE, COULOMB_INIT = FALSE, BALANCED = FALSE;
__shared int                Mp = -1, NumLevels = -1, NumParticles = -1, NumElements = -1;
__shared int                Seed = 1;
__shared double             CubeLength = -1, OutVcutoff, OutFcutoff;
__shared int                NumProcessors = -1, ProcessorLevel;
__shared int				Power2Processors;
__shared int                Power8[LEVELS_MAX+2];
__shared int                LevelLocate[LEVELS_MAX+2];
__shared FILE               *Output; /*= stdout;*/
__shared double             iTheta,Theta=-1;
__shared double             **MYconstant;
__shared double             ****Ms2constant;
__shared double             ****Ms3constant1, ****Ms3constant2;
__shared CellPtr            CellTable;
__shared PTablePtr          ParticleTable;
__shared ETablePtr          ElementTable;
__shared SmallParticlePtr   SmallParticleTable;
__shared PthreadWorkPtr     PthreadWorkTable;
__shared processor_name_t   *ProcessorTable;
__shared pthread_t          *PthreadTable;
__shared pthread_barrier_t  PthreadBarrier;
__shared int                **ILevelAnalysis;
__shared PMON_DATA          **PmonAnalysis;
__shared double             ***Timing;
__private int               PthreadNum;
__private int               PthreadPTStart, PthreadPTEnd;
__private int               PthreadETStart, PthreadETEnd;
__private int               **PthreadCT;
__private Complex           *Mt1;
__private Complex           **Ms;
__private double            **MLegendre;
__private int               *PthreadILA;
__private PMON_DATA         *PthreadPmon;
__private double            **PthreadTiming;

/*
 * FFT Version variables
 */
__shared int                nblocks, fftarray_m, fftsize;
__shared int                *frowsz, *fincrement, *fblocksz;
__shared double             *fscale;
__shared double             scale;
__shared double             **A;
__shared double             **PFMA_s3_const1F;
__shared double             FCubeLength;
__private Mtype             transferf;
__private double            **transferb;
__private double            *finput;
__private Mtype             *FLocal_accum;

#else

/*
 * Global variable definitions
 */

int                         Direct = FALSE, Results = FALSE, PFMA = FALSE;
int							EPMTA = FALSE;
int                         Verbose = FALSE;
int                         FFT = FALSE, COULOMB_INIT = FALSE, BALANCED = FALSE;
int                         Mp = -1, NumLevels = -1, NumParticles = -1, NumElements = -1;
int                         Seed = 1;
double                      CubeLength = -1, OutVcutoff, OutFcutoff;
int                         NumProcessors = -1, ProcessorLevel;
int							Power2Processors;
int                         Power8[LEVELS_MAX+2];
int                         LevelLocate[LEVELS_MAX+2];
FILE                        *Output; /*= stdout;*/
double                      iTheta,Theta=-1;
double                      **MYconstant;
double                      ****Ms2constant;
double                      ****Ms3constant1, ****Ms3constant2;
CellPtr                     CellTable;
PTablePtr                   ParticleTable;
ETablePtr                   ElementTable;
PthreadWorkPtr              PthreadWorkTable;
int                         **ILevelAnalysis;
double                      ***Timing;
int                         PthreadNum;
int                         PthreadPTStart, PthreadPTEnd;
int                         PthreadETStart, PthreadETEnd;
int                         **PthreadCT;
Complex                     *Mt1;
Complex                     **Ms;
double                      **MLegendre;
int                         *PthreadILA;
double                      **PthreadTiming;

/*
 * FFT Version variables
 */
int                         nblocks, fftarray_m, fftsize;
int                         *frowsz, *fincrement, *fblocksz;
double                      *fscale;
double                      scale;
double                      **A;
double                      **PFMA_s3_const1F;
double                      FCubeLength;
Mtype                       transferf;
double                      **transferb;
double                      *finput;
Mtype                       *FLocal_accum;
#endif

void InitializeCoulomb(double cubelength, int treelevels, int fourier,
    int pterms, int algorithm, double theta, int processors,
    int direct, int particles, int elements, int balance, int verbose, int pmon){

    int i;
#ifndef SERIAL
    int power2,error,default_procsetsize;	
#endif
    Output=stdout;

    /*
     * check that the system has not already been initialized
     */
    if(COULOMB_INIT){
        ERROR("(InitializeCoulomb) repeated call to InitializeCoulomb() without FreeCoulomb()",
            NULL,0);
    } else {
        COULOMB_INIT = TRUE;
    }

    /*
     * check the validity of the input variables
     */
    if(cubelength <= 0) 
        ERROR("(InitializeCoulomb) invalid cubelength",NULL,0);
    CubeLength = cubelength;
    treelevels--;
    if(treelevels < 1 || treelevels > LEVELS_MAX)
        ERROR("(InitializeCoulomb) invalid treelevels",NULL,0);
    NumLevels = treelevels;
    if(fourier < 0 || fourier > 1)
        ERROR("(InitializeCoulomb) invalid fourier",NULL,0);
    FFT = fourier;
    pterms--;
    if(pterms < 0)
        ERROR("(InitializeCoulomb) invalid pterms",NULL,0);
    Mp = pterms;
    if(FFT && Mp != 3 && Mp != 7 && Mp != 11 && Mp != 15)
        ERROR("(InitializeCoulomb) invalid pterms for FFT",NULL,0);
    if(algorithm < 0 || algorithm > 2)
        ERROR("(InitializeCoulomb) invalid algorithm",NULL,0);
	switch(algorithm){
		case 1: PFMA = TRUE; break;
		case 2: PFMA = TRUE; EPMTA = TRUE; break;
	}
    Theta = theta;
    if((!PFMA || EPMTA) && Theta <= 0)  /* this check depends on the MAC */
        ERROR("(InitializeCoulomb) invalid Theta",NULL,0);
    if(!PFMA || EPMTA)
        iTheta = 1.0/Theta;
#ifndef SERIAL
    NumProcessors = processors;
    Pmon = pmon;
#else
    NumProcessors = 1;
	Power2Processors = 1;
#endif
    if(direct < 0 || direct > 1)
        ERROR("(InitializeCoulomb) invalid direct",NULL,0);
    Direct = direct;
    if(particles < 1)
        ERROR("(InitializeCoulomb) invalid particles",NULL,0);
    NumParticles = particles;
    if(elements < 1)
        ERROR("(InitializeCoulomb) invalid elements",NULL,0);
    NumElements = elements;

    Verbose = verbose;

    if(balance < 0 || balance > 1)
        ERROR("(InitializeCoulomb) invalid balance",NULL,0);
	BALANCED = balance;
#ifdef SERIAL
	BALANCED = FALSE;
#endif

    /*
     * setup ProcessorTable, PthreadWorkTable, ILevelAnalysis, Timing, 
     * and PmonAnalysis and calculate ProcessorLevel
     */
#ifndef SERIAL
    default_procsetsize = pthread_procsetinfo(NULL,0);
    if(default_procsetsize == -1) 
        ERROR("(InitializeCoulomb) pthread_procsetinfo failure",NULL,0);
    if(NumProcessors <= 0 || NumProcessors > default_procsetsize) 
        NumProcessors = default_procsetsize;
    for(i=NumProcessors,power2=-1;i > 0;power2++){ i /= 2; }
    Power2Processors = (int) pow(2.0,power2);
	if(!BALANCED){
		NumProcessors = Power2Processors;
	}

    ProcessorTable = (processor_name_t *) malloc(NumProcessors * 
        sizeof(processor_name_t));
    error = pthread_procsetinfo(ProcessorTable,NumProcessors);
    if(error == -1) 
        ERROR("(InitializeCoulomb) pthread_procsetinfo failure",NULL,0);
#endif

    PthreadWorkTable = (PthreadWorkPtr) malloc(NumProcessors * 
        sizeof(PthreadWork));
    if(PthreadWorkTable == NULL) 
        ERROR("(InitializeCoulomb) malloc failure",NULL,0);

    ILevelAnalysis = (int **) malloc(NumProcessors * sizeof(int *));
    if(ILevelAnalysis == NULL) 
        ERROR("(InitializeCoulomb) malloc failure",NULL,0);

    Timing = (double ***) malloc(NumProcessors * sizeof(double **));
    if(Timing == NULL) ERROR("(InitializeCoulomb) malloc failure",NULL,0);

#ifndef SERIAL
    PmonAnalysis = (PMON_DATA **) malloc(NumProcessors * sizeof(PMON_DATA *));
    if(PmonAnalysis == NULL) ERROR("(InitializeCoulomb) malloc failure",NULL,0);
#endif

    for(i=Power2Processors,ProcessorLevel=0;i > 1;ProcessorLevel++){ i /= 8; }

    /*
     * initialize MYconstant, Ms2constant and Ms3constant1
     */
    MultipoleSetup();

    /*
     * allocate space for CellTable and calculate Power8 and LevelLocate
     */
    CellTableSetup();

#ifndef SERIAL
    /*
     * create barrier
     */
    error = pthread_barrier_init(&PthreadBarrier,pthread_barrierattr_default,
        NumProcessors);
    if(error == -1) 
        ERROR("(InitializeCoulomb) pthread_barrier_init failure",NULL,0);

    /*
     * create pthreads 
     */
    PthreadTable = (pthread_t *) malloc(NumProcessors * sizeof(pthread_t));
    if(PthreadTable == NULL) 
        ERROR("(InitializeCoulomb) malloc failure",NULL,0);

    PthreadTable[0] = pthread_self();
    for(i=1;i < NumProcessors;i++){
        error = pthread_create(&PthreadTable[i],pthread_attr_default,
            ParallelSetup,(void *)i);
        if(error == -1) 
            ERROR("(InitializeCoulomb) pthread_create failure",NULL,0);
    }

    /* 
     * if we intend to compute the exact forces and potentials then
     * allocate space for SmallParticleTable which is used by 
     * DirectForces to calculate the exact solution
     */
    if(Direct){
        SmallParticleTable = (SmallParticlePtr) 
            malloc(NumParticles * sizeof(SmallParticle));
        if(SmallParticleTable == NULL) 
            ERROR("(InitializeCoulomb) malloc failure",NULL,0);
    }
#endif

    /*
     * perform parallel initialization
     */
    ParallelSetup((void *)0);

	/*
     * load balance the M2L work load 
     * copy the direct work load if not running in BALANCED mode
     */
	if(BALANCED){
		CoulombMultipoleBalance();
	} else {
		for(i=0;i < NumProcessors;i++){
			PthreadWorkTable[i].lb_ct_dstart = 
				PthreadWorkTable[i].ct[NumLevels][PTHREAD_START];
			PthreadWorkTable[i].lb_ct_dend = 
				PthreadWorkTable[i].ct[NumLevels][PTHREAD_END];
		}
	}

}

void Coulomb(void){
#ifndef SERIAL
    int i;
    int error;
#endif

    if(!COULOMB_INIT){
        ERROR("(Coulomb) InitializeCoulomb has not been called",NULL,0);
    }


    if(BALANCED){
	CoulombDirectBalance();
    }

#ifndef SERIAL
    PthreadTable[0] = pthread_self();
    for(i=1;i < NumProcessors;i++){
        error = pthread_create(&PthreadTable[i],pthread_attr_default,
            Parallel,(void *)i);
        if(error == -1) 
            ERROR("(Coulomb) pthread_create failure",NULL,0);
    }
#endif

    /*
     * execute in parallel
     */
    Parallel((void *)0);

}

void ERROR(char *msg, char *var, int num) {
    fprintf(Output,"COULOMB ERROR: %s",msg);
    if(var != NULL)
        fprintf(Output," %s\n",var);
    else if(num != 0)
        fprintf(Output," %d\n",num);
    else
        fprintf(Output,"\n");
    exit(1);
}
