/*
 *
 * !!!!!!!!!!!!!!!!!!!! to be changed as in PMTAforce.c if necessary !!!!!!!!!!!!!!!!!!!!!!
 *
 *
 * PMTAbalance.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAbalance.c,v 1.3 1995/01/17 17:14:55 welliott Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAbalance.c,v $
 * Revision 1.3  1995/01/17  17:14:55  welliott
 * Commented out all prefetch statements to prevent the program
 * from locking up. Locks up inconsistently in parallel mode on the
 * KSR when prefetches are included.
 *
 * Revision 1.2  1994/11/11  19:51:07  lambert
 * Several variables that were declared and never used were
 * removed.  Also, the variable error is only used under parallel,
 * so it was defined within the #ifndef SERIAL #endif blocks.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 *
*/


#include "include/PMTA.h"

/*
 * CoulombMultipoleBalance computes the balanced M2L load for
 * all processors.  Load balancing is achieved by dividing the work
 * equally between the processors.  This is a serial function.
 */
void CoulombMultipoleBalance(void){
	int i,proc;
	int last_cell,newlast_cell;
	int total_multipole;
	int frac_multipole;
	int sum_multipole;

	if(Verbose) fprintf(Output,"Computing the M2L load balance...\n");

	total_multipole = 0;
	for(i=(PFMA ? 9:LevelLocate[NumLevels]);i < LevelLocate[NumLevels+1];i++){
		total_multipole += CellTable[i].ime;
	}

	frac_multipole = total_multipole/NumProcessors;

	if(PFMA){
		newlast_cell = last_cell = 8;
	} else {
		newlast_cell = last_cell = LevelLocate[NumLevels] - 1;
	}
	frac_multipole -= total_multipole/(2*(LevelLocate[NumLevels+1] -
		(last_cell+1)));
	for(proc=0;proc < NumProcessors;proc++){
		sum_multipole = 0;
		while(sum_multipole < frac_multipole &&
			newlast_cell < (LevelLocate[NumLevels+1]-1)){
			newlast_cell++;
			sum_multipole += CellTable[newlast_cell].ime;
		}
		PthreadWorkTable[proc].lb_ct_mstart = last_cell + 1;
		PthreadWorkTable[proc].lb_ct_mend = newlast_cell + 1;
		last_cell = newlast_cell;
	}
	PthreadWorkTable[NumProcessors-1].lb_ct_mend = LevelLocate[NumLevels+1];

}

/*
 * CoulombDirectBalance computes the balanced Direct load for all
 * processors.  Load balancing is achieved by dividing the work
 * equally between the processors.  This is a serial function.
 */
void CoulombDirectBalance(void){
	int i,j,proc;
	int last_cell,newlast_cell;
	long total_direct;
	long frac_direct;
	long sum_direct;

	if(Verbose) fprintf(Output,"Computing the Direct load balance...\n");

	total_direct = 0;
	for(i=LevelLocate[NumLevels];i < LevelLocate[NumLevels+1];i++){
		for(j=0;j < CellTable[i].iddt;j++){
			total_direct += CellTable[i].n *
				CellTable[CellTable[i].iddirect[j]].n;
		}
		for(j=0;j < CellTable[i].isdt;j++){
			total_direct += CellTable[i].n *
				CellTable[CellTable[i].isdirect[j]].n;
		}
		total_direct += CellTable[i].n * CellTable[i].n;
	}

	frac_direct = total_direct/NumProcessors;

	newlast_cell = last_cell = LevelLocate[NumLevels] - 1;
	frac_direct -= total_direct/(2*(LevelLocate[NumLevels+1] -
		(last_cell+1)));
	for(proc=0;proc < NumProcessors;proc++){
		sum_direct = 0;
		while(sum_direct < frac_direct &&
			newlast_cell < (LevelLocate[NumLevels+1]-1)){
			newlast_cell++;
			for(j=0;j < CellTable[newlast_cell].iddt;j++){
				sum_direct += CellTable[newlast_cell].n *
					CellTable[CellTable[newlast_cell].iddirect[j]].n;
			}
			for(j=0;j < CellTable[newlast_cell].isdt;j++){
				sum_direct += CellTable[newlast_cell].n *
					CellTable[CellTable[newlast_cell].isdirect[j]].n;
			}
			sum_direct += CellTable[newlast_cell].n*CellTable[newlast_cell].n;
		}
		PthreadWorkTable[proc].lb_ct_dstart = last_cell + 1;
		PthreadWorkTable[proc].lb_ct_dend = newlast_cell + 1;
		last_cell = newlast_cell;
	}
	PthreadWorkTable[NumProcessors-1].lb_ct_dend = LevelLocate[NumLevels+1];

}


/*
 * BalancedCellForces computes the forces and potentials for all particles
 */
void BalancedCellForces(void){
    int i,j,k,level,max_cells,index,n,nbelts,interj;
    int *interact,*ielts,*inodes;
    Mtype ML;
    int fftb;
    double *scratch,*v;
#ifndef SERIAL
    int error;
    int prefetch_count,prefetch_local_count,prefetch_direct_place;
	ParticlePtr prefetch_pa;
    char *prefetch_pointer;
#endif

    PthreadStartTiming(TIME_CF);
    PthreadStartTiming(TIME_CFM);

    if(Verbose && PthreadNum == 0)
        fprintf(Output,"Calculating cell forces and potentials...\n");

#ifndef SERIAL
    if(FFT){
        prefetch_count = (int) ((4.0 * ((double) sizeof(double)) *
            ((double) (Mp+1)) * ((double) fftarray_m))/((double) 128) + 1.0);
    } else {
        prefetch_count = (int) ((((double) sizeof(double)) *
            ((double) (Mp+2)) * ((double) (Mp+1)))/((double) 128) + 1.0);
    }
    prefetch_local_count = (int) ((((double) sizeof(double)) *
    	((double) (Mp+2)) * ((double) (Mp+1)))/((double) 128) + 1.0);
#endif

    max_cells = PthreadWorkTable[PthreadNum].lb_ct_mend;
    for(i=PthreadWorkTable[PthreadNum].lb_ct_mstart;i < max_cells;i++){

#ifndef SERIAL
		/*
         * prefetch next local expansion
         */

		if(i < (max_cells-1)){
            prefetch_pointer = (char *) CellTable[i+1].l;
            for(k=0;k < prefetch_local_count;k++){
/*            	_pcsp(prefetch_pointer,"ex","bl"); */
                prefetch_pointer += 128;
            }
        }

#endif
    	/*
         * initialize local expansion
         */
        ML = CellTable[i].l;
        for(j=0;j <= Mp;j++){
        	for(k=0;k <= j;k++){
            	ML[j][k].x = 0.0;
                ML[j][k].y = 0.0;
            }
        }
       	CellTable[i].lvl = FALSE;

        if(CellTable[i].ime > 0){
        	/*
             * calculate local expansion using multipoles of cells
             * listed in the multipole interaction list
             */

            if(FFT){
            	scratch = &FLocal_accum[0][0][0].x;
                for (fftb=0; fftb < 2*(Mp+1)*fftarray_m; fftb++) {
                	*scratch++ = 0.0;
                    *scratch++ = 0.0;
                } /* for fftb */
            } /* if FFT */

            interact = CellTable[i].imultipole;
#ifndef SERIAL

            for(j=0;j < CellTable[i].ime;j++){
            	prefetch_pointer = (char *) CellTable[interact[j]].m;
                for(k=0;k < prefetch_count;k++){
/*                	_pcsp(prefetch_pointer,"ro","bl"); */
                    prefetch_pointer += 128;
                }
            }

#endif
            for(j=0;j < CellTable[i].ime;j++){
            	if(CellTable[interact[j]].vl){
                	if(FFT)
                    	loc_expF(i, interact[j], FLocal_accum);
                    else
                        MLocal(i,interact[j]);
                   	CellTable[i].lvl = TRUE;
                }
            }

            if(FFT)
            	IFFTLocal(i, FLocal_accum);

   		} /* if CellTable... */

    }

#ifndef SERIAL
 	if(PFMA){
    	error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    	if(error == -1) ERROR("(BalancedCellForces) pthread_barrier_checkout "
        	"failure",NULL,0);

      	error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
       	if(error == -1) ERROR("(BalancedCellForces) pthread_barrier_checkin "
        	"failure",NULL,0);
    }

	/*
	 * prefetch local expansions required by this processor for the
     * downward local-to-local pass
     */

    for(level=(PFMA ? 2:NumLevels);level < NumLevels;level++){
        max_cells = PthreadCT[level][PTHREAD_END];
        for(i=PthreadCT[level][PTHREAD_START];i < max_cells;i++){
            prefetch_pointer = (char *) CellTable[i].l;
            for(k=0;k < prefetch_local_count;k++){
/*            	_pcsp(prefetch_pointer,"ex","bl"); */
                prefetch_pointer += 128;
            }
        } /* for i */
    } /* for level */
#endif
    for(level=(PFMA ? 2:NumLevels);level < NumLevels;level++){
#ifndef SERIAL
        if(PFMA && level <= ProcessorLevel && level > 2){
            error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
            if(error == -1)
				ERROR("(BalancedCellForces) pthread_barrier_checkout "
                	"failure",NULL,0);

            error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
            if(error == -1)
				ERROR("(BalancedCellForces) pthread_barrier_checkin "
                	"failure",NULL,0);
        }
#endif
        max_cells = PthreadCT[level][PTHREAD_END];
        for(i=PthreadCT[level][PTHREAD_START];i < max_cells;i++){

            /*
             * translate parent's local expansion to local expansions
             * for each child
             */
            if(CellTable[i].lvl){
                index = ((i-LevelLocate[level]) << 3)+LevelLocate[level+1];
                for(j=0;j < 8;j++){
                    MShiftLocal(index + j, i);
                    CellTable[index + j].lvl = TRUE;
                }
            }
        } /* for i */
    } /* for level */

#ifndef SERIAL
   	error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(BalancedCellForces) pthread_barrier_checkout "
    	"failure",NULL,0);

    error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(BalancedCellForces) pthread_barrier_checkin "
        "failure",NULL,0);

	prefetch_direct_place = PthreadWorkTable[PthreadNum].lb_ct_dstart;
#endif
    PthreadStopTiming(TIME_CFM);
    max_cells = PthreadWorkTable[PthreadNum].lb_ct_dend;
    for(i=PthreadWorkTable[PthreadNum].lb_ct_dstart;i < max_cells;i++){
    	nbelts=CellTable[i].mesh.nbelts;
   	ielts=CellTable[i].mesh.ielts;
	n=CellTable[i].n;
	inodes=CellTable[i].mesh.inodes;

        /*if(CellTable[i].n == 0) continue;*/
        if(nbelts == 0) continue;

    	PthreadStartTiming(TIME_CFD);

#ifndef SERIAL
		/*
		 * prefetch particles which are required for direct interactions
		 */

		if(i < (max_cells-1)){
			for(j=0;j < CellTable[i+1].iddt;j++){
				if(CellTable[i+1].iddirect[j] > prefetch_direct_place){
					prefetch_direct_place = CellTable[i+1].iddirect[j];
					prefetch_pa = CellTable[prefetch_direct_place].pa;
					for(k=0;k < CellTable[prefetch_direct_place].n;k += 4){
/*						_pcsp(&prefetch_pa[k].p.x,"ex","bl"); */
					}
				}
			}
		}

		_gspwt(&CellTable[i].p.x);

#endif
        /*
         * calculate direct interactions within cell
         */
        /*DirectWithinCell(i);*/


        /*
         * calculate direct interactions with other cells which
         * are listed in the cell's direct double interaction list
         * these are cells which reside on the same processor
         */
        interact = CellTable[i].iddirect;
        for(j=0;j < CellTable[i].iddt;j++){
#ifndef SERIAL
	    	_gspwt(&CellTable[interact[j]].p.x);
#endif
            /*DirectDoubleCell(i,interact[j]);*/

#ifndef SERIAL
	    	_rsp(&CellTable[interact[j]].p.x);
#endif
        }

        /*
         * calculate direct interactions with other cells which
         * are listed in the cell's direct single interaction list
         * these are cells which reside on a different processor
         */
        interact = CellTable[i].isdirect;
        for(j=0;j < CellTable[i].isdt;j++){
            /*DirectSingleCell(i,interact[j]);*/

        }
    	PthreadStopTiming(TIME_CFD);
    	PthreadStartTiming(TIME_CFM);

#ifndef SERIAL
        /*
         * prefetch next local expansion
         */

        if(i < (max_cells-1)){
            prefetch_pointer = (char *) CellTable[i+1].l;
            for(k=0;k < prefetch_local_count;k++){
/*                _pcsp(prefetch_pointer,"ro","bl"); */
                prefetch_pointer += 128;
            }
        }

#endif

        /*
         * calculate force and potentials due to local expansion
         */
        MForce(i);
#ifndef SERIAL
	    _rsp(&CellTable[i].p.x);
#endif

    	PthreadStopTiming(TIME_CFM);
    }

    PthreadStopTiming(TIME_CF);

}
