#include "include/PMTA.h"

/*
 *
 * interfacing file between PMTA and BEM
 *
 */

#ifndef SERIAL
__shared PTablePtr          ParticleTable;
__shared ETablePtr          ElementTable;
#else
PTablePtr                   ParticleTable;
ETablePtr                   ElementTable;
#endif

void callpmta_(double *CubeLengthPtr,int *NumLevelsPtr,int *FFTPtr,int *MpPtr,int *PFMAPtr,
          int *EPMTAPtr,double *ThetaPtr,int *NumProcessorsPtr,int *DirectPtr,int *BALANCEDPtr,int *VerbosePtr,
	  int *PmonPtr, int *NumParticlesPtr, int *NumElementsPtr, int *FlagInitPtr, int *FlagFreePtr){

    double CubeLength=*CubeLengthPtr;
    int NumLevels=*NumLevelsPtr;
    int FFT=*FFTPtr;
    int Mp=*MpPtr;
    int PFMA=*PFMAPtr;
    int EPMTA=*EPMTAPtr;
    double Theta=*ThetaPtr;
    int NumProcessors=*NumProcessorsPtr;
    int Direct=*DirectPtr;
    int BALANCED=*BALANCEDPtr;
    int Verbose=*VerbosePtr;
    int Pmon=*PmonPtr;
    int NumParticles=*NumParticlesPtr;
    int NumElements=*NumElementsPtr;
    int FlagInit=*FlagInitPtr;
    int FlagFree=*FlagFreePtr;
/*    int NumParticles;
    int NumElements; */
/*    double CubeLength 		= -1.0;
    double Theta 		= -1;
    int Mp			= -1;
    int NumLevels		= -1;
    int NumParticles 		= -1;   default is to read PDB file from stdin*/
    int Seed 			= 1;
    int Outliers 		= 0;
    double OutVcutoff,OutFcutoff;
/*    int Verbose 		= FALSE;
    int Direct			= FALSE;*/
    int Results			= FALSE;
/*    int Pmon			= FALSE;
    int NumProcessors		= 1;
    int PFMA			= FALSE;
    int EPMTA			= FALSE;
    int FFT			= FALSE;
    int BALANCED		= FALSE;
*/

    int i,j;

    if(FlagFree==0){
    /* if not call for only freing variables */

    /* extern Fortran commons from BEM code */
    extern struct {
      double q[NUMPART];
      double sol[NUMPART];
      int ibemanalysis;
      int irightleftrigid;
    } charge_;
    extern struct {
      double xyznod[NUMPART*3];
      double xicon[NUMELEM*2];
      double xstart[6*3];
      int igcon[NUMELEM*MNOD*MNOD];
      int ilcon[NUMELEM*4];
      int iface[NUMELEM];
      int isub[NUMELEM];
      int intg[NUMELEM];
      int lxyz[NUMPART*2];
      int icumul[6];
      int ndst[6*2];
      int idst[6*3];
      int jslim[6];
      int nelef[6];
      int nnodf[6];
      int ielef[6*2];
      int inodf[6*2];
      int ibcond[6];
    } maille_;

    if(FlagInit){
    /* if first call, allocation of memory for particles and elements */

      ParticleTable = (PTablePtr) malloc(NumParticles * sizeof(PTable));
      ElementTable = (ETablePtr) malloc(NumElements * sizeof(ETable));
    }

    /*
     * Create ParticleTable
     */
    BemParticles(NumParticles,maille_.xyznod,charge_.q);

    /*
     * Create ElementTable
     */

    BemElements(NumParticles,NumElements,maille_.xyznod,maille_.ilcon);

    if(FlagInit){
     fprintf(stdout,"PMTA : %f,%d,%d,%d,%d,%f,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",      CubeLength,         /* Length of Cube                                      */
      NumLevels,          /* Number of tree levels: 3 -> 0,1,2 -> 8^2 leaves     */
      FFT,                /* FFT: 0=no 1=yes                                     */
      Mp,                 /* Number of terms, p                                  */
      PFMA+EPMTA,         /* Algorithm: 0:B&H 1:FMA 2: EPMTA (includes B&H & FMA)*/
      Theta,              /* B&H Theta  (0.5 - 4.0) required for Algorithm 0 & 2 */
      NumProcessors,      /* Number of Processors: 1 for serial                  */
      Direct,             /* O(n^2) direct computation 0=no 1=yes                */
      NumParticles,       /* NumParticles                                        */
      NumElements,       /* NumElements                                        */
      BALANCED,           /* Use load balancing: 0=no 1=yes                      */
      Verbose,            /* Verbose: 0=no 1=yes                                 */
      Pmon,               /* Pmon performance: 0=no 1=yes (ignored for serial    */
      FlagInit,
      FlagFree,
      charge_.ibemanalysis,
      charge_.irightleftrigid,NUMPART,NUMELEM
);
    }


    if(FlagInit){
    /*
     * if first call, initialize the system for the computation of Coulomb's force
     */
      InitializeCoulomb(
        CubeLength,         /* Length of Cube                                      */
        NumLevels,          /* Number of tree levels: 3 -> 0,1,2 -> 8^2 leaves     */
        FFT,                /* FFT: 0=no 1=yes                                     */
        Mp,                 /* Number of terms, p                                  */
        PFMA+EPMTA,         /* Algorithm: 0:B&H 1:FMA 2: EPMTA (includes B&H & FMA)*/
        Theta,              /* B&H Theta  (0.5 - 4.0) required for Algorithm 0 & 2 */
        NumProcessors,      /* Number of Processors: 1 for serial                  */
        Direct,             /* O(n^2) direct computation 0=no 1=yes                */
        NumParticles,       /* NumParticles                                        */
        NumElements,       /* NumElements                                        */
        BALANCED,           /* Use load balancing: 0=no 1=yes                      */
        Verbose,            /* Verbose: 0=no 1=yes                                 */
        Pmon               /* Pmon performance: 0=no 1=yes (ignored for serial    */
      );
    }

    if(!FlagInit && charge_.ibemanalysis){
    /* if not first call, updating data structures */
      InitializeNewStep();
    }

    /*
     * display the parameters for the simulation
     */
    if(FlagInit){
      PrintProblemInfo();
    }

    /*
     * compute Coulomb's force for one timestep
     */
    Coulomb();

    /*
     * perform analyses and print results
     */
    if(FlagInit){
      InteractAnalysis();
      CellParticleAnalysis();
      if(Direct)
        ErrorAnalysis(Outliers,OutVcutoff,OutFcutoff);
      TimingAnalysis();
    }

    /*
     * print particle potential and force results
     */
    if(Results)
      PrintResults();

    /*
     * fill the Fortran common charge_.q containing the solution
     */
    SolutionBem(charge_.sol);

    }
    else{
    /* free variables */
      free(ParticleTable);
      free(ElementTable);
      ParticleTable=NULL;
      ElementTable=NULL;
      FreeCoulomb();
    }

}

/*
 *  subroutine to create my own ParticleTable from Bem
 */
void BemParticles(int NumParticles, double *xyznod, double *field){
    int i;

    for(i=0; i< NumParticles; i++){
       ParticleTable[i].p.x = xyznod[i];
       ParticleTable[i].p.y = xyznod[NumParticles+i];
       ParticleTable[i].p.z = xyznod[2*NumParticles+i];
       ParticleTable[i].q = field[i];
    }

}

/*
 *  subroutine to create ElementTable from Bem
 */
void BemElements(int NumParticles,int NumElements,double *xyznod,int *ilcon){
    int i,i1,i2,i3,i4;

    for(i=0; i< NumElements; i++){
       i1=ilcon[i]-1;
       i2=ilcon[NumElements+i]-1;
       i3=ilcon[2*NumElements+i]-1;
       i4=ilcon[3*NumElements+i]-1;
       ElementTable[i].p.x = 0.25*(xyznod[i1]+xyznod[i2]
                                  +xyznod[i3]+xyznod[i4]);
       ElementTable[i].p.y = 0.25*(xyznod[NumParticles+i1]+xyznod[NumParticles+i2]
                                  +xyznod[NumParticles+i3]+xyznod[NumParticles+i4]);
       ElementTable[i].p.z = 0.25*(xyznod[2*NumParticles+i1]+xyznod[2*NumParticles+i2]
                                  +xyznod[2*NumParticles+i3]+xyznod[2*NumParticles+i4]);
    }

}

/*
 *  subroutine to give the solution to Bem
 */
void SolutionBem(double *sol){
    int i;

    for(i=0;i<NumParticles;i++){
       sol[i]=ParticleTable[i].v;
    }
}
