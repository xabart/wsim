/*
 * PMTAparticle.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAparticle.c,v 1.2 1994/11/11 20:50:40 lambert Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAparticle.c,v $
 * Revision 1.2  1994/11/11  20:50:40  lambert
 * Cleaned up ugly indenting.
 *
 * AdjustParticles() now zeroes forces/pots within particletable
 * before each time step.  It used to be that the new forces were
 * added to the old, which was closer to a bug than a feature.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 * adapted for BEM, C.Fochesato, CMLA, 2003
 * elements treated in the same way as particles
 */


#include "include/PMTA.h"

/*
 * ParticleCells calculates the cell identifiers of all the
 * particles in ParticleTable
 * and calculates the number of particles in each cell in the CellTable
 * it also allocates space in the CellTable for the particle and
 * small particle lists of each cell
 *
 * this function should be called at the beginning of a time-step to re-assign
 * particles to cells after particle positions have been updated
 */
void ParticleCells(void){
    int             i,j,bit,shift,x,y,z,locate;
    int             size_p,start,end;
    unsigned int    c;
    double          num,hCLx,hCLy,hCLz;
#ifndef SERIAL
    int error,size_sp,cell;
#endif

    PthreadStartTiming(TIME_PC);

    if(Verbose && PthreadNum == 0)
        fprintf(Output,"Assigning particles and elements to cells...\n");

    /*
     * set the number of particles and elements in each cell in the CellTable
     * to zero
     */
    start = PthreadCT[NumLevels][PTHREAD_START];
    end = PthreadCT[NumLevels][PTHREAD_END];
    for(i=start;i < end;i++){
        CellTable[i].n = 0;
        CellTable[i].mesh.nbelts = 0;
    }

    locate = LevelLocate[NumLevels];
    num = pow(2.0,(double) NumLevels);
    hCLx = CubeLength/2 - CUBE_CTR_X;
    hCLy = CubeLength/2 - CUBE_CTR_Y;
    hCLz = CubeLength/2 - CUBE_CTR_Z;
    for(i=PthreadPTStart;i < PthreadPTEnd;i++){
        x = (int) ((ParticleTable[i].p.x + hCLx)/CubeLength * num);
        if(x == num) x--;
        y = (int) ((ParticleTable[i].p.y + hCLy)/CubeLength * num);
        if(y == num) y--;
        z = (int) ((ParticleTable[i].p.z + hCLz)/CubeLength * num);
        if(z == num) z--;
        for(j=0,c=0,bit=1,shift=0;j < NumLevels;j++){
            c |= (bit & x) << shift;
            shift++;
            c |= (bit & y) << shift;
            shift++;
            c |= (bit & z) << shift;
            bit <<= 1;
        }
        c += locate;
        ParticleTable[i].c = c;
#ifdef SERIAL
        CellTable[c].n++;
#endif
    }
    for(i=PthreadETStart;i < PthreadETEnd;i++){
        x = (int) ((ElementTable[i].p.x + hCLx)/CubeLength * num);
        if(x == num) x--;
        y = (int) ((ElementTable[i].p.y + hCLy)/CubeLength * num);
        if(y == num) y--;
        z = (int) ((ElementTable[i].p.z + hCLz)/CubeLength * num);
        if(z == num) z--;
        for(j=0,c=0,bit=1,shift=0;j < NumLevels;j++){
            c |= (bit & x) << shift;
            shift++;
            c |= (bit & y) << shift;
            shift++;
            c |= (bit & z) << shift;
            bit <<= 1;
        }
        c += locate;
        ElementTable[i].c = c;
#ifdef SERIAL
        CellTable[c].mesh.nbelts++;
#endif
    }
#ifndef SERIAL
    error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkout failure",NULL,0);

    error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkin failure",NULL,0);

    /*
     * calculate the number of particles in each cell and the nodes indexes
     */
    for(i=0;i < NumParticles;i++){
        cell = ParticleTable[i].c;
        if(cell >= start && cell < end)
            CellTable[cell].n++;
    }
    /*
     * calculate the number of elements in each cell and the element indexes
     */
    for(i=0;i < NumElements;i++){
        cell = ElementTable[i].c;
        if(cell >= start && cell < end)
            CellTable[cell].mesh.nbelts++;
    }
#endif

    /*
     * allocate space for the particle lists
     */
    size_p = sizeof(Particle);
#ifndef SERIAL
    size_sp = sizeof(SmallParticle);
#endif
    for(i=start;i < end;i++){
        free(CellTable[i].pa);
#ifndef SERIAL
        free(CellTable[i].spa);
#endif
        if(CellTable[i].n == 0){
            CellTable[i].pa = NULL;
#ifndef SERIAL
            CellTable[i].spa = NULL;
#endif
        } else {
            CellTable[i].pa = (ParticlePtr) malloc(CellTable[i].n * size_p);
#ifndef SERIAL
            CellTable[i].spa = (SmallParticlePtr)
                malloc(CellTable[i].n * size_sp);
#endif
            if(CellTable[i].pa == NULL
#ifndef SERIAL
                || CellTable[i].spa == NULL
#endif
                )
                ERROR("(ParticleCells) malloc failure",NULL,0);
        }
    }
    /*
     * allocate space for the element lists
     */
    size_p = sizeof(int);
    for(i=start;i < end;i++){
        free(CellTable[i].mesh.ielts);
        free(CellTable[i].mesh.inodes);
        if(CellTable[i].mesh.nbelts == 0){
            CellTable[i].mesh.ielts = NULL;
        } else {
            CellTable[i].mesh.ielts = (int*) malloc(CellTable[i].mesh.nbelts * size_p);
            if(CellTable[i].mesh.ielts == NULL
                )
                ERROR("(ParticleCells) malloc failure",NULL,0);
        }
        if(CellTable[i].n == 0){
            CellTable[i].mesh.inodes = NULL;
        } else {
            CellTable[i].mesh.inodes = (int*) malloc(CellTable[i].n * size_p);
            if(CellTable[i].mesh.inodes == NULL)
                ERROR("(ParticleCells) malloc failure",NULL,0);
        }
    }

    PthreadStopTiming(TIME_PC);

}

/*
 * allocation for BEM associated structures
 */
void ParticleCellsCoef(int freecoef){
    int             i,j,nie,n,interj;
    int             start,end;
    int             nnie,j1,j2,indie,jg,isin,oldnnie,nn;

extern struct {
   double xyznod[NUMPART*3];
   double xicon[NUMELEM*2];
   double xstart[6*3];
   int igcon[NUMELEM*MNOD*MNOD];
   int ilcon[NUMELEM*4];
   int iface[NUMELEM];
   int isub[NUMELEM];
   int intg[NUMELEM];
   int lxyz[NUMPART*2];
   int icumul[6];
   int ndst[6*2];
   int idst[6*3];
   int jslim[6];
   int nelef[6];
   int nnodf[6];
   int ielef[6*2];
   int inodf[6*2];
   int ibcond[6];
} maille_;

    PthreadStartTiming(TIME_PC);

    if(Verbose && PthreadNum == 0)
        fprintf(Output,"Allocating coefs...\n");

    start = PthreadCT[NumLevels][PTHREAD_START];
    end = PthreadCT[NumLevels][PTHREAD_END];
    for(i=start;i < end;i++){
        nie=CellTable[i].mesh.nbelts;
        n=CellTable[i].n;

/*	if(freecoef){
	  free(CellTable[i].mesh.jgcoef);
	}*/
	oldnnie=0;
	if(freecoef){
	  oldnnie=CellTable[i].mesh.nnie;
	  for(j=0;j<oldnnie;j++){
            CellTable[i].mesh.jgcoef[j]=0;
	  }
	}
        nnie=0;
	if(nie>0){
        for(indie=1;indie<=nie;indie++){
          for(j2=1;j2<=MNOD;j2++){
            for(j1=1;j1<=MNOD;j1++){
              jg=maille_.igcon[((j2-1)*MNOD+(j1-1))*NUMELEM+CellTable[i].mesh.ielts[indie-1]-1];
	      if(nnie>oldnnie){
 	        nn=nnie;}
	      else{
	        nn=oldnnie;}
              isinvect_(&isin,&jg,CellTable[i].mesh.jgcoef,&nn);
	      if(isin==0){
	        nnie++;
		if(!freecoef){
		  if(nnie==1){
                    CellTable[i].mesh.jgcoef= (int *) malloc(nnie * sizeof(int));
		    if (CellTable[i].mesh.jgcoef==NULL)
                       ERROR("(ParticleCellsCoef) malloc failure",NULL,0);
		  }
		  else{
                    CellTable[i].mesh.jgcoef= (int *) realloc(CellTable[i].mesh.jgcoef,nnie * sizeof(int));
		    if (CellTable[i].mesh.jgcoef==NULL)
                       ERROR("(ParticleCellsCoef) malloc failure",NULL,0);
		  }
		}else{
                  if(nnie>oldnnie){
                    CellTable[i].mesh.jgcoef= (int *) realloc(CellTable[i].mesh.jgcoef,nnie * sizeof(int));
		    if (CellTable[i].mesh.jgcoef==NULL)
                       ERROR("(ParticleCellsCoef) malloc failure",NULL,0);
		  }
		}
                CellTable[i].mesh.jgcoef[nnie-1]=jg;
	      }
            }
          }
        }
        CellTable[i].mesh.nnie=nnie;
	}
    }

    for(i=start;i < end;i++){
        nie=CellTable[i].mesh.nbelts;
        n=CellTable[i].n;
        nnie=CellTable[i].mesh.nnie;
	if(freecoef){
	  free(CellTable[i].coefqselfdirect);
          free(CellTable[i].coefuselfdirect);
	  for(j=0;j<CellTable[i].iddt;j++){
	    free(CellTable[i].coefqddirect1[j]);
	    free(CellTable[i].coefuddirect1[j]);
	    free(CellTable[i].coefqddirect2[j]);
	    free(CellTable[i].coefuddirect2[j]);
	  }
	  for(j=0;j<CellTable[i].isdt;j++){
	    free(CellTable[i].coefqsdirect[j]);
	    free(CellTable[i].coefusdirect[j]);
	  }
	  free(CellTable[i].coefqxmultipole);
	  free(CellTable[i].coefqymultipole);
	  free(CellTable[i].coefuxmultipole);
	  free(CellTable[i].coefuymultipole);
#if (ISYM==1 || IBOT==10)
	  free(CellTable[i].coefqxmultisym);
	  free(CellTable[i].coefqymultisym);
	  free(CellTable[i].coefuxmultisym);
	  free(CellTable[i].coefuymultisym);
#endif
#if (ISYM==1 && IBOT==10)
	  free(CellTable[i].coefqxmultisym2);
	  free(CellTable[i].coefqymultisym2);
	  free(CellTable[i].coefuxmultisym2);
	  free(CellTable[i].coefuymultisym2);
	  free(CellTable[i].coefqxmultisym3);
	  free(CellTable[i].coefqymultisym3);
	  free(CellTable[i].coefuxmultisym3);
	  free(CellTable[i].coefuymultisym3);
#endif
	}

	if(n>0 && nie>0){
	  CellTable[i].coefqselfdirect = (double *) malloc(nnie*n * sizeof(double));
          CellTable[i].coefuselfdirect = (double *) malloc(nnie*n * sizeof(double));
	}
	else{
	  CellTable[i].coefqselfdirect = NULL;
          CellTable[i].coefuselfdirect = NULL;
	}
	for(j=0;j<CellTable[i].iddt;j++){
	  interj=CellTable[i].iddirect[j];
          if(CellTable[interj].n>0 && nie>0){
            CellTable[i].coefqddirect1[j] = (double *) malloc(
	    		nnie*CellTable[interj].n * sizeof(double));
            CellTable[i].coefuddirect1[j] = (double *) malloc(
	    		nnie*CellTable[interj].n * sizeof(double));
	  }
	  else{
            CellTable[i].coefqddirect1[j] = NULL;
            CellTable[i].coefuddirect1[j] = NULL;
	  }
          if(n>0 && CellTable[interj].mesh.nbelts>0){
            CellTable[i].coefqddirect2[j] = (double *) malloc(
	    		CellTable[interj].mesh.nnie*n * sizeof(double));
            CellTable[i].coefuddirect2[j] = (double *) malloc(
	    		CellTable[interj].mesh.nnie*n * sizeof(double));
	  }else{
            CellTable[i].coefqddirect2[j] = NULL;
            CellTable[i].coefuddirect2[j] = NULL;
	  }
	}
	for(j=0;j<CellTable[i].isdt;j++){
	  interj=CellTable[i].isdirect[j];
          if(CellTable[interj].n>0 && nie>0){
            CellTable[i].coefqsdirect[j] = (double *) malloc(
	    		nnie*CellTable[interj].n * sizeof(double));
            CellTable[i].coefusdirect[j] = (double *) malloc(
	    		nnie*CellTable[interj].n * sizeof(double));
	  }else{
            CellTable[i].coefqsdirect[j] = NULL;
            CellTable[i].coefusdirect[j] = NULL;
	  }
	}
	/* cf allocation alloc_mpe dans cellsetup */
	if(nie>0){
          CellTable[i].coefqxmultipole = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefqymultipole = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefuxmultipole = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefuymultipole = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
	}else{
          CellTable[i].coefqxmultipole = NULL;
          CellTable[i].coefqymultipole = NULL;
          CellTable[i].coefuxmultipole = NULL;
          CellTable[i].coefuymultipole = NULL;
	}
#if (ISYM==1 || IBOT==10)
	if(nie>0){
          CellTable[i].coefqxmultisym = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefqymultisym = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefuxmultisym = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefuymultisym = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
	}else{
          CellTable[i].coefqxmultisym = NULL;
          CellTable[i].coefqymultisym = NULL;
          CellTable[i].coefuxmultisym = NULL;
          CellTable[i].coefuymultisym = NULL;
	}
#endif
#if (ISYM==1 && IBOT==10)
	if(nie>0){
          CellTable[i].coefqxmultisym2 = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefqymultisym2 = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefuxmultisym2 = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefuymultisym2 = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
	}else{
          CellTable[i].coefqxmultisym2 = NULL;
          CellTable[i].coefqymultisym2 = NULL;
          CellTable[i].coefuxmultisym2 = NULL;
          CellTable[i].coefuymultisym2 = NULL;
	}
	if(nie>0){
          CellTable[i].coefqxmultisym3 = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefqymultisym3 = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefuxmultisym3 = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefuymultisym3 = (double *) malloc(nnie*(Mp+1)*(Mp+1) * sizeof(double));
	}else{
          CellTable[i].coefqxmultisym3 = NULL;
          CellTable[i].coefqymultisym3 = NULL;
          CellTable[i].coefuxmultisym3 = NULL;
          CellTable[i].coefuymultisym3 = NULL;
	}
#endif
    }

    PthreadStopTiming(TIME_PC);

}

/*
 * CellPlists inserts particles that are represented in ParticleTable into
 * their respective cell particle lists in CellTable
 */
void CellPlists(void){
    int                 i,cell,start,end;
    ParticlePtr         NewParticle;
#ifndef SERIAL
    SmallParticlePtr    NewSmallParticle;
#endif

    PthreadStartTiming(TIME_CPL);

    if(Verbose && PthreadNum == 0)
        fprintf(Output,"Inserting particles into the cell table...\n");

    /*
     * set the number of particles in each cell in the CellTable
     * to zero
     */
    start = PthreadCT[NumLevels][PTHREAD_START];
    end = PthreadCT[NumLevels][PTHREAD_END];
    for(i=start;i < end;i++){
        CellTable[i].n = 0;
	CellTable[i].mesh.nbelts = 0;
    }

    for(i=0;i < NumParticles;i++){
        cell = ParticleTable[i].c;
#ifndef SERIAL
        if(cell >= start && cell < end){

            NewSmallParticle = &CellTable[cell].spa[CellTable[cell].n];
#endif
	    NewParticle = &CellTable[cell].pa[CellTable[cell].n];

            NewParticle->p = ParticleTable[i].p;
            NewParticle->q = ParticleTable[i].q;

            NewParticle->f.x = NewParticle->f.y = NewParticle->f.z = 0;
            NewParticle->v = 0;
            NewParticle->id = i;
	    CellTable[cell].mesh.inodes[CellTable[cell].n]=i+1;
            CellTable[cell].n++;

#ifndef SERIAL
            NewSmallParticle->p = ParticleTable[i].p;
            NewSmallParticle->q = ParticleTable[i].q;
        }
#endif
    }
    /*
     * idem pour elements
     */
    for(i=0;i < NumElements;i++){
        cell = ElementTable[i].c;
        CellTable[cell].mesh.ielts[CellTable[cell].mesh.nbelts]=i+1;
	CellTable[cell].mesh.nbelts++;
    }

    PthreadStopTiming(TIME_CPL);

}

/*
 * AdjustParticles reads particle positions into the CellTable and
 * checks that the particles are located in the correct cells.
 * If there are particles which are incorrectly located, then insert
 * particles into the correct cells.
 */
void AdjustParticles(void){
    int i,j,k,max_cells;
    int bit,shift,x,y,z,locate;
    unsigned int c;
    double num,hCLx,hCLy,hCLz;
    ParticlePtr pa;
    Particle temp_particle;
#ifndef SERIAL
    SmallParticlePtr spa;
    SmallParticle temp_smallparticle;
#endif
    int *meshinodes,*meshielts;
    int temp_element,temp_inode;
    Vector pelt;

    if(Verbose && PthreadNum == 0)
	fprintf(Output,"Reading particle positions into CellTable "
		"and adjusting cells...\n");

    PthreadStartTiming(TIME_AP);

    locate = LevelLocate[NumLevels];
    num = pow(2.0,(double) NumLevels);
    hCLx = CubeLength/2 - CUBE_CTR_X;
    hCLy = CubeLength/2 - CUBE_CTR_Y;
    hCLz = CubeLength/2 - CUBE_CTR_Z;
    max_cells = PthreadCT[NumLevels][PTHREAD_END];
    for(i=PthreadCT[NumLevels][PTHREAD_START];i < max_cells;i++){
#ifndef SERIAL
	_gspwt(&CellTable[i].p.x);
	spa = CellTable[i].spa;
#endif
	pa = CellTable[i].pa;
	meshinodes = CellTable[i].mesh.inodes;
	for(j=0;j < CellTable[i].n;j++){

	    /* Have to zero out the forces and potential each timestep */
	    pa[j].f.x = 0;
	    pa[j].f.y = 0;
	    pa[j].f.z = 0;
	    pa[j].v = 0;

	    pa[j].p = ParticleTable[pa[j].id].p;
#ifndef SERIAL
	    spa[j].p = ParticleTable[pa[j].id].p;
#endif
	    x = (int) ((pa[j].p.x + hCLx)/CubeLength * num);
	    if(x == num) x--;
	    y = (int) ((pa[j].p.y + hCLy)/CubeLength * num);
	    if(y == num) y--;
	    z = (int) ((pa[j].p.z + hCLz)/CubeLength * num);
	    if(z == num) z--;
	    for(k=0,c=0,bit=1,shift=0;k < NumLevels;k++){
            	c |= (bit & x) << shift;
            	shift++;
            	c |= (bit & y) << shift;
            	shift++;
            	c |= (bit & z) << shift;
            	bit <<= 1;
	    }
	    c += locate;
	    if(c != i){
		ParticleTable[pa[j].id].c = c;
		CellTable[i].n--;
		temp_particle = pa[j];
		temp_inode = meshinodes[j];
		pa[j] = pa[CellTable[i].n];
		meshinodes[j] = meshinodes[CellTable[i].n];
#ifndef SERIAL
		temp_smallparticle = spa[j];
		spa[j] = spa[CellTable[i].n];
		_rsp(&CellTable[i].p.x);
		_gspwt(&CellTable[c].p.x);
#endif
		CellTable[c].n++;
		CellTable[c].pa = (ParticlePtr)
		    realloc(CellTable[c].pa,CellTable[c].n *
			    sizeof(Particle));
		CellTable[c].mesh.inodes = (int*)
		    realloc(CellTable[c].mesh.inodes,CellTable[c].n *
			    sizeof(int));
#ifndef SERIAL
		CellTable[c].spa = (SmallParticlePtr)
		    realloc(CellTable[c].spa,CellTable[c].n *
			    sizeof(SmallParticle));
#endif
		if(CellTable[c].pa == NULL || CellTable[c].mesh.inodes == NULL
#ifndef SERIAL
		   || CellTable[c].spa == NULL
#endif
		    ) ERROR("(AdjustParticles) realloc failure",NULL,0);
		CellTable[c].pa[CellTable[c].n-1] = temp_particle;
		CellTable[c].mesh.inodes[CellTable[c].n-1] = temp_inode;
#ifndef SERIAL
		CellTable[c].spa[CellTable[c].n-1] = temp_smallparticle;
		_rsp(&CellTable[c].p.x);
		_gspwt(&CellTable[i].p.x);
		spa = CellTable[i].spa;
#endif
		pa = CellTable[i].pa;
		meshinodes = CellTable[i].mesh.inodes;
		j--;
	    }
	}
	/* idem pour les elements*/
	meshielts = CellTable[i].mesh.ielts;
	for(j=0;j < CellTable[i].mesh.nbelts;j++){

	    pelt = ElementTable[meshielts[j]-1].p;
	    x = (int) ((pelt.x + hCLx)/CubeLength * num);
	    if(x == num) x--;
	    y = (int) ((pelt.y + hCLy)/CubeLength * num);
	    if(y == num) y--;
	    z = (int) ((pelt.z + hCLz)/CubeLength * num);
	    if(z == num) z--;
	    for(k=0,c=0,bit=1,shift=0;k < NumLevels;k++){
            	c |= (bit & x) << shift;
            	shift++;
            	c |= (bit & y) << shift;
            	shift++;
            	c |= (bit & z) << shift;
            	bit <<= 1;
	    }
	    c += locate;
	    if(c != i){
		ElementTable[meshielts[j]-1].c = c;
		CellTable[i].mesh.nbelts--;
		temp_element = meshielts[j];
		meshielts[j] = meshielts[CellTable[i].mesh.nbelts];
		CellTable[c].mesh.nbelts++;
		CellTable[c].mesh.ielts = (int*)
		    realloc(CellTable[c].mesh.ielts,CellTable[c].mesh.nbelts *
			    sizeof(int));
		if(CellTable[c].mesh.ielts == NULL
		    ) ERROR("(AdjustParticles) realloc failure",NULL,0);
		CellTable[c].mesh.ielts[CellTable[c].mesh.nbelts-1] = temp_element;
		meshielts = CellTable[i].mesh.ielts;
		j--;
	    }
	}

#ifndef SERIAL
	_rsp(&CellTable[i].p.x);
#endif
    }
    PthreadStopTiming(TIME_AP);
}

/*
 * CellResults records particle forces and potentials in ParticleTable
 */
void CellResults(void){
    int             i,j,max_cells,max_particles,particle;
    ParticlePtr     OldParticle;

    PthreadStartTiming(TIME_CR);

    if(Verbose && PthreadNum == 0)
        fprintf(Output,"Recording force and potential results in the "
            "particle table...\n");

    max_cells = PthreadWorkTable[PthreadNum].lb_ct_dend;
    for(i=PthreadWorkTable[PthreadNum].lb_ct_dstart;i < max_cells;i++){
        max_particles = CellTable[i].n;
        for(j=0;j < max_particles;j++){
            OldParticle = &CellTable[i].pa[j];
            particle = OldParticle->id;
            ParticleTable[particle].f = OldParticle->f;
            ParticleTable[particle].v = OldParticle->v;
        }
    }

    PthreadStopTiming(TIME_CR);

}
