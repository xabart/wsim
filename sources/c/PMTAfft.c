/*
 * PMTAfft.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAfft.c,v 1.2 1994/11/11 20:08:40 lambert Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAfft.c,v $
 * Revision 1.2  1994/11/11  20:08:40  lambert
 * A couple of variables that were declared but never used were
 * deletedd.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 *
 */


#include "include/PMTA.h"
#include "PMTAlegendre.h"
#include "PMTAfft.h"

/* subroutines for FFT-Version */


/*************************************************************************
**                                                                      **
**   LOC_EXP converts a multipole expansion in a distant box to         **
**   a local expansion in the box of interest.  The new local           **
**   expansion will describe the effect of the particles in the         **
**   distant box on the particles in the local box.                     **
**                                                                      **
**   Parameters:                                                        **
**      O:   multipole expansion to be converted                        **
**      L:   resulting local expansion                                  **
**      p:   number of terms in each expansion                          **
**      center:   center of the box for the multipole expansion         **
**         relative the the destination box centered at                 **
**         the origin                                                   **
**                                                                      **
*************************************************************************/
/* FFT-Version */
void loc_expF (int LocalCell, int DistantCell, Mtype *L)
{
   int m, n, bl, bm, b, inputloop, p;
   double *leg, temp, r;
   double negone, conjterm;
   double *inptr, *outptr, *transptr;
   double realtemp, imgtemp, tempr1, tempr2, tempi1, tempi2;
   double *startloop, *endloop;
   int increment;
   Mtype O;
   SphVector center;
   Vector x0;

    p = Mp;
    O = CellTable[DistantCell].m;

    /*
     * calculate the center of the distant cell in spherical
     * coordinates relative to the center of the local cell
     * centered at the origin
     */
    x0.x = CellTable[DistantCell].p.x - CellTable[LocalCell].p.x;
    x0.y = CellTable[DistantCell].p.y - CellTable[LocalCell].p.y;
    x0.z = CellTable[DistantCell].p.z - CellTable[LocalCell].p.z;
    center.r = sqrt(x0.x*x0.x + x0.y*x0.y + x0.z*x0.z);
    if(center.r < fabs(x0.z)) center.r = fabs(x0.z);
    center.a = acos(x0.z/center.r);
    if((x0.x == 0.0) && (x0.y == 0.0)) {
        center.b = 0.0;
    } else {
        center.b = atan2(x0.y,x0.x);
    }

/*
   start to compute the transfer fcn array by precomputing e^imb
   negone and conjterm handle the equality: Y[l][-m] = (-1)^m Y*[l][m]
*/
   Mt1[0].x = 1.0;
   Mt1[0].y = 0.0;
   negone = -1.0;
   conjterm = 1.0;
   for (m = 1; m <= p; m++) {
      Mt1[m].x =  negone * cos( (double) m * center.b);
      Mt1[m].y =  conjterm * sin( (double) m * center.b);
      negone *= -1.0;
      conjterm *= -1.0;
   }
/*
   compute the Legendre polynomial
   (my geometry definitions are the opposite of Jim's)
*/
  temp = cos( center.a);
  n = p;
  Legendre( MLegendre, temp, n);
  r = temp = -1.0/center.r;
/*
include a factor 1/(r^n) in the polynomial and warp with A[l][m]
*/
  for (n = 0; n <= p; n++) {
      leg = MLegendre[n];
      for (m = 0; m <= n; m++) {
         leg[m] *= (r * PFMA_s3_const1F[n][m]);
      }
      r *= temp;
   }
/*
   load the transfer function into the transfer fft array
   and do the fourier transform in the m direction

   transferf points to the location of the start of each input vector
   (reverse ordered with respect to l)

   transferb points to each block, with the following 
   protocol: block 0 -> l = 0 to 3
             block 1 -> l = 4 to 7, etc.
*/
   for (bl = 0; bl <= p; bl++) {
      transferf[bl][0].x = MLegendre[bl][0];
      transferf[bl][0].y = 0.0;   
      for (bm = 1; bm <= bl; bm++) {
         realtemp = MLegendre[bl][bm] * Mt1[bm].x;
         imgtemp = MLegendre[bl][bm] * Mt1[bm].y;
         transferf[bl][bm].x = realtemp;
         transferf[bl][bm].y = imgtemp;
      } /* for bm */
      for (bm = bl+1; bm < frowsz[bl]; bm++) {
         transferf[bl][bm].x = 0.0;
         transferf[bl][bm].y = 0.0;
      } /* for bm */
      finput = &transferf[bl][0].x;
      switch (frowsz[bl]) {
         case 4:
            FFTH8(finput);
            break;
         case 8:
            FFTH16(finput);
            break;
         case 16:
            FFTH32(finput);
            break;
         default:
            ERROR("(loc_expF) wrong FFT size", NULL, 0);
            break;
      } /* switch */
   } /* for bl */
/*
   do the fourier transform in the l (vertical) direction
   and do the matrix multiply
*/
for (b = 0; b < nblocks; b++)  {
   increment = fincrement[b];
   finput = transferb[b];
   outptr = &L[0][0][0].x;
   startloop = finput;
   endloop = finput + (fblocksz[b] << 1);
   FFTV8(finput, frowsz[b << 2]);
   for (inputloop = 0; inputloop < b+1; inputloop++) {
    inptr = &O[(b-inputloop) << 2][0].x;
    for (transptr = startloop; transptr < endloop; ) {
      tempr1 = *transptr++; tempi1 = *transptr++;   
      tempr2 = *inptr++; tempi2 = *inptr;
      inptr += increment;   
      realtemp = tempr1 * tempr2 - tempi1 * tempi2;
      imgtemp = tempr1 * tempi2 + tempi1 * tempr2;
      *outptr++ += realtemp;
      *outptr += imgtemp;
      outptr += increment;
    } /* for innerloop */
   } /* for inputloop */
} /* for b */
} /* loc_expF */
/* end FFT-Version */

void FFTMultipole(int Cell)
{
    int ffti, fftj, fftk, fftl, fftm;
    double *fftblock;

    for (fftl = 0; fftl <= Mp; fftl++) {
					for (fftm = 0; fftm <= fftl; fftm++) {
                        CellTable[Cell].m[fftl][fftm].x *= A[fftl][fftm];
                        CellTable[Cell].m[fftl][fftm].y *= A[fftl][fftm];
                    } /* for fftm */
                    for (fftm = fftl+1; fftm < fftarray_m; fftm++) {
                        CellTable[Cell].m[fftl][fftm].x = 0.0;
                        CellTable[Cell].m[fftl][fftm].y = 0.0;
                    } /* for fftm */
                } /* for fftl */
                for (ffti = 0; ffti < Mp+1; ffti += 4) {
                    for (fftj = 0; fftj < 4; fftj++) {
                        fftblock = &CellTable[Cell].m[ffti+fftj][0].x;
                        switch (Mp+1) {
                            case 4:
                                FFTH8(fftblock);
                                break;
                            case 8:
                                FFTH16(fftblock);
                                break;
                            case 12:
                            case 16:
                                FFTH32(fftblock);
                                break;
                            default:
                                ERROR("(CellMultipoles) wrong FFT size",
                                    NULL, 0);
                        } /* switch */
                    } /* for fftj */
					for (fftk=0; fftk < 4*2*fftarray_m; fftk++)
        				fftblock[fftk+2*fftarray_m] = (double) 0.0;
    				fftblock = &CellTable[Cell].m[ffti][0].x;
    				FFTV8(fftblock, fftarray_m);
                } /* for ffti */
} /* FFTMultipole */


void IFFTLocal(int LocalCell, Mtype *psiF)
{
    Mtype psi;
    int     bi, bl, b, lstart, lvalue, bm;
    double  *finput;
    double  negone, conjterm, bmterm;
    double  FFTBLOCK = 8.0;


  psi = CellTable[LocalCell].l;

  for (b=0; b < nblocks; b++) {
    finput = &psiF[b][0][0].x;
    IFFTV8(finput, fftarray_m);
    for (bi=0; bi < (int) FFTBLOCK; bi++) {
      switch (Mp+1) {
         case 4:
            IFFTH8(finput);
            break;
         case 8:
            IFFTH16(finput);
            break;
         case 12:
         case 16:
            IFFTH32(finput);
            break;
         default:
            ERROR("(IFFTLocal) wrong FFT size", NULL, 0);
            break;
      } /* switch */
      finput += 2 * fftarray_m;
    } /* for bi */
    if (b == 0) lstart = 4;
    else lstart = 1;
    for (bl = lstart; bl < (int) FFTBLOCK; bl++) {
        negone = 1.0;
        conjterm = 1.0;
        lvalue = b * (int) FFTBLOCK/2 - (int)FFTBLOCK/2 + bl;
        for (bm = 0; bm <= lvalue; bm++) {
            bmterm = (((lvalue + bm) & 1)? 1.0 : -1.0);
            bmterm *= A[lvalue][bm] * scale;
            psi[lvalue][bm].x +=
                FLocal_accum[b][(int)FFTBLOCK-bl-1][bm].x  * bmterm;
            psi[lvalue][bm].y +=
                FLocal_accum[b][(int)FFTBLOCK-bl-1][bm].y * bmterm;
            negone *= -1.0;
            conjterm *= -1.0;
        } /* for bm */
    } /* for bl */
  } /* for b */
} /* IFFTLocal */
