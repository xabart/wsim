/*
 * PMTAcell_setup.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAcell_setup.c,v 1.4 1995/01/17 17:15:57 welliott Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAcell_setup.c,v $
 * Revision 1.4  1995/01/17  17:15:57  welliott
 * Added code to deallocate multipole expansions allocated with
 * alloc_mpe() (which was added in previous revision).
 *
 * Revision 1.3  1994/12/12  16:42:46  welliott
 * Fixed the "p not equal to power of 2" problem. This bug caused
 * a core dump on certain platforms (eg Sun) because of the original
 * method of allocating the multipole expansions. Bug did not appear
 * on RS 6000. Fixed by adding more portable subroutine "alloc_mpe"
 * for cases where FFT is not used (must be power of two for FFT).
 *
 * Revision 1.2  1994/11/11  19:57:02  lambert
 * Several variables that were declared and never used were removed.
 *
 * Routine FreeCellTable() was deleted.  It didn't work correctly,
 * as it didn't free everything it was supposed to.
 * New routine FreeCoulomb written which frees all memory that
 * PMTA allocates.  I verified (serial) correctness of this
 * routine using mallocmap() on the suns, as well as asking
 * codecenter to find memory leaks, and both methods indicated
 * everything that PMTA allocates is correctly freed by this
 * routine.  NOTE: I don't understand the parallel allocation
 * and deallocation, so these routines must be carefully
 * verified on the parallel platforms.
 *
 * Now, one can potentially set up PMTA with a set of parameters,
 * compute electrostatics for several timesteps, and then
 * deallocate everything and set it up again with different
 * parameters.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 * Modified by C.Fochesato, CMLA, 2003 for Bem structure
*/


#include "include/PMTA.h"

/*
 * Local function declarations
 *
 */
Vector   CellCenter(int, int);

void CellTableSetup(void){
    int i,CellTableLength;

    if(Verbose) fprintf(Output,"Setting up the cell table...\n");

    /*
     * initialize Power8 which stores the powers of 8, and 
     * initialize LevelLocate which stores the array index of the first
     * cell at each level in the CellTable array
     */
    Power8[0] = 1;
    LevelLocate[0] = 0;
    for(i=1;i < (LEVELS_MAX+2);i++){
        Power8[i] = 8 * Power8[i-1];
        LevelLocate[i] = Power8[i-1] + LevelLocate[i-1];
    }

    CellTableLength = LevelLocate[NumLevels+1];

    CellTable = (CellPtr) malloc(CellTableLength * sizeof(Cell));
    if(CellTable == NULL) 
        ERROR("(CellTableSetup) malloc failure, SIZE:",NULL,
        CellTableLength * sizeof(Cell));
    
}

void CellsSetup(void){
    int     j,k,i,n,max_cells;
    int     expansion_size,expansion_count,expansion_start,mem_count;
    int     local_expansion_count;
    int     Fexpansion_size,Fmem_count;
    char    *Mm,*Ml;
    Complex *Mtemp;
    double  cube_length;
    int     fftj, fftb, fftm;
    double  FFTBLOCK = 8.0;
    Complex *scratch;
    int     blockindex, m_size;
#ifndef SERIAL
    int    error;
#endif
    
    PthreadStartTiming(TIME_CS);

    if(Verbose && PthreadNum == 0) 
        fprintf(Output,"Setting up multipole expansion structures and the\n"
            "interaction list for each cell...\n");

    /*
     * setup MLegendre for the Legendre Polynomial
     */
    MLegendre = (double **) malloc((Mp+1)*sizeof(MLegendre));
    if(MLegendre == NULL) ERROR("1(CellsSetup) malloc failure",NULL,0);
    for(j=0;j <= Mp;j++){
        MLegendre[j] = (double *) malloc((j+1)*sizeof(double));
        if(MLegendre[j] == NULL) 
            ERROR("2(CellsSetup) malloc failure",NULL,0);
    }

    /* 
     * setup Mt1 used to precompute e^(imb)
     */
    Mt1 = (Complex *) malloc((Mp + 1) * sizeof(Complex));
    if(Mt1 == NULL) ERROR("3(CellsSetup) malloc failure",NULL,0);

    /* 
     * setup Ms used for precomputing terms in the expansions
     */
    Ms = (Complex **) malloc((Mp+1) * sizeof(Ms));
    if(Ms == NULL) ERROR("4(CellsSetup) malloc failure",NULL,0);
    for(n=0;n <= Mp;n++){
        Ms[n] = (Complex *) malloc((n+1) * sizeof(Complex)); 
        if(Ms[n] == NULL) ERROR("5(CellsSetup) malloc failure",NULL,0);
    }

    /*
     * FFT Version
     * set up blank array to accumulate the FFT local expansions
     */
    if(FFT){
        FLocal_accum = (Mtype *) malloc(nblocks * sizeof(Mtype));
        for(fftb=0;fftb < nblocks;fftb++){
            FLocal_accum[fftb] = (Mtype) malloc((int)(FFTBLOCK) * 
                sizeof(Mtype));
        }
        scratch = (Complex *) malloc(nblocks * fftsize * sizeof(Complex));
        for(fftm=0;fftm < nblocks*fftsize;fftm++){
            scratch[fftm].x = 0.0;
            scratch[fftm].y = 0.0;
        } /* for fftm */

        for(fftb=0;fftb < nblocks;fftb++){
            for(fftm=0;fftm < (int) FFTBLOCK;fftm++){
                FLocal_accum[fftb][fftm] = &scratch[fftb*fftsize + 
                    fftm*fftarray_m];
            } /* for fftm */
        } /* for fftb */


        /*
         * set up blank array in which to calculate the M2L transfer function
         * in Fourier space
         */
        transferf = (Mtype) malloc ((Mp+1) * sizeof(Mtype));
        transferb = (double **) malloc (nblocks * sizeof(double *));
        for(fftb=0;fftb < nblocks;fftb++){
            blockindex = 4*(fftb+1) - 1;
            m_size = fblocksz[fftb] / (int) FFTBLOCK;
            transferf[blockindex] = (Complex *) malloc(fblocksz[fftb] *
                sizeof(Complex));
            transferb[fftb] = &transferf[blockindex][0].x;
            for(fftm=0;fftm < fblocksz[fftb];fftm++){
                transferf[blockindex][fftm].x = 0.0;
                transferf[blockindex][fftm].y = 0.0;
            } /* for fftm */
            for(fftm=1;fftm < (int) FFTBLOCK / 2;fftm++){
                transferf[blockindex - fftm] = 
                    &transferf[blockindex][fftm*m_size];
            } /* for fftm */
        } /* for fftb */


    } /* if FFT */

    
    /*
     * allocate space for m and l expansions 
     */
    for(i=0,expansion_size=0;i <= Mp;i++) expansion_size += i+1;
    expansion_size = expansion_size*sizeof(Complex) + (Mp+1)*sizeof(Mtype);
    expansion_start = (Mp+1)*sizeof(Mtype);
    if(FFT){
        Fexpansion_size = 2 * (Mp+1) * fftarray_m;
        Fexpansion_size = Fexpansion_size*sizeof(Complex)+(Mp+1)*sizeof(Mtype);
    }

    for(j=1,expansion_count=0;j <= NumLevels;j++){
        expansion_count += PthreadCT[j][PTHREAD_END] - 
            PthreadCT[j][PTHREAD_START];
    }
    mem_count = expansion_count * expansion_size;
    if(FFT){
        Fmem_count = expansion_count * Fexpansion_size;
        Mm = (char *) malloc(Fmem_count*sizeof(char));
/*    } else {
        Mm = (char *) malloc(mem_count*sizeof(char));
  }*/
    if(PFMA){
        Ml = (char *) malloc(mem_count*sizeof(char));
    } else {
        Ml = (char *) malloc((PthreadCT[NumLevels][PTHREAD_END] -
            PthreadCT[NumLevels][PTHREAD_START])*expansion_size*sizeof(char));
    }
/* if FFT bug fix */
/*    if((Mm == NULL || Ml == NULL) && PthreadNum < Power2Processors)
		ERROR("6(CellsSetup) malloc failure",NULL,0); */
    PthreadWorkTable[PthreadNum].Mm = Mm;
    PthreadWorkTable[PthreadNum].Ml = Ml;
}
    /*
     * calculate centers and lengths of cells, and allocate space
     * for cell multipole and local expansions 
     */
    expansion_count = 0;
    local_expansion_count = 0;
    cube_length = CubeLength/2;
    for(j=1;j <= NumLevels;j++){
        max_cells = PthreadCT[j][PTHREAD_END];
        PthreadILA[j] = 0;
        for(k=PthreadCT[j][PTHREAD_START];k < max_cells;k++){

            CellTable[k].p = CellCenter(j,k);
            CellTable[k].h = cube_length;

            CellTable[k].vl = FALSE;
            CellTable[k].n = 0;
            CellTable[k].pa = NULL;
#ifndef SERIAL
            CellTable[k].spa = NULL;
#endif
            CellTable[k].mesh.nbelts = 0;
            CellTable[k].mesh.nnie = 0;
            CellTable[k].mesh.jgcoef = NULL;
            CellTable[k].mesh.ielts = NULL;
            CellTable[k].mesh.inodes = NULL;
            CellTable[k].iddt = CellTable[k].isdt = CellTable[k].ime = 0;
            CellTable[k].iddirect = CellTable[k].isdirect = NULL;
            CellTable[k].imultipole = NULL;
            CellTable[k].coefqselfdirect = NULL;
            CellTable[k].coefuselfdirect = NULL;
            CellTable[k].coefqddirect1 = NULL;
            CellTable[k].coefuddirect1 = NULL;
            CellTable[k].coefqddirect2 = NULL;
            CellTable[k].coefuddirect2 = NULL;
            CellTable[k].coefqsdirect = NULL;
            CellTable[k].coefusdirect = NULL;
            CellTable[k].coefqxmultipole = NULL;
            CellTable[k].coefqymultipole = NULL;
            CellTable[k].coefuxmultipole = NULL;
            CellTable[k].coefuymultipole = NULL;
#if (ISYM==1 || IBOT==10)
            CellTable[k].coefqxmultisym = NULL;
            CellTable[k].coefqymultisym = NULL;
            CellTable[k].coefuxmultisym = NULL;
            CellTable[k].coefuymultisym = NULL;
#endif
#if (ISYM==1 && IBOT==10)
            CellTable[k].coefqxmultisym2 = NULL;
            CellTable[k].coefqymultisym2 = NULL;
            CellTable[k].coefuxmultisym2 = NULL;
            CellTable[k].coefuymultisym2 = NULL;
            CellTable[k].coefqxmultisym3 = NULL;
            CellTable[k].coefqymultisym3 = NULL;
            CellTable[k].coefuxmultisym3 = NULL;
            CellTable[k].coefuymultisym3 = NULL;
#endif

            /*
             * link cell m and l pointers to allocated space
             */
            if(FFT){
                Fmem_count = expansion_count * Fexpansion_size;
                CellTable[k].m = (Mtype) ((char *)Mm + Fmem_count);
                Fmem_count += expansion_start;
                Mtemp = (Complex *) ((char *)Mm + Fmem_count);
                for(i=0;i <= Mp;i += 4){
                    for(fftj=0;fftj < 4;fftj++){
                        CellTable[k].m[i+fftj] = Mtemp;
                        Mtemp += fftarray_m;
                    } /* for fftj */
                    for(fftj = 0;fftj < 4;fftj++){
                        Mtemp += fftarray_m; 
                    }
                } /* for i */
            } else {
                mem_count = expansion_count * expansion_size;
					 alloc_mpe(&CellTable[k].m, Mp+1);
#if (ISYM==1 || IBOT==10)
                mem_count = expansion_count * expansion_size;
					 alloc_mpe(&CellTable[k].msym, Mp+1);
#endif
#if (ISYM==1 && IBOT==10)
                mem_count = expansion_count * expansion_size;
					 alloc_mpe(&CellTable[k].msym2, Mp+1);
                mem_count = expansion_count * expansion_size;
					 alloc_mpe(&CellTable[k].msym3, Mp+1);
#endif
/*
                CellTable[k].m = (Mtype) ((char *)Mm + mem_count);
                mem_count += expansion_start;
                Mtemp = (Complex *) ((char *)Mm + mem_count);
                for(i=0;i <= Mp;i++){
                    CellTable[k].m[i] = Mtemp;
                    Mtemp += i+1;
                }
*/
            } /* else not FFT */
            expansion_count++;
            if(PFMA || j == NumLevels){ 
                mem_count = local_expansion_count * expansion_size;
					 alloc_mpe(&CellTable[k].l, Mp+1);
/*                CellTable[k].l = (Mtype) ((char *)Ml + mem_count);
                mem_count += expansion_start;
                Mtemp = (Complex *) ((char *)Ml + mem_count);
                for(i=0;i <= Mp;i++){
                    CellTable[k].l[i] = Mtemp;
                    Mtemp += i+1;
                }
*/
                local_expansion_count++;
            } else {
                CellTable[k].l = NULL;
            }
        }
        cube_length /= 2;
    }

#ifndef SERIAL
    error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkout failure",NULL,0);
 
    error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkin failure",NULL,0);
#endif

    /*
     * allocate space and calculate cell interaction lists
     */
    if(!PFMA || EPMTA){
		PMTAInteractLists();
	} else {
        PFMAInteractLists();
    }
     
    PthreadStopTiming(TIME_CS);

}

Vector CellCenter(int Level, int Cell){
    int i,mul;
    int x,y,z;
    double cube,cube_adjust;
    Vector c;

    mul = 1;
    x = y = z = 0;
    Cell -= LevelLocate[Level];
    for(i=0;i < Level;i++){
        x |= (Cell & 01) << i;
        Cell >>= 1;
        y |= (Cell & 01) << i;
        Cell >>= 1;
        z |= (Cell & 01) << i;
        Cell >>= 1;
        mul <<= 1;
    }

    cube = CubeLength/((double) mul);
    cube_adjust = (cube - CubeLength)/2;
    c.x = ((double)x)*cube + cube_adjust + CUBE_CTR_X;
    c.y = ((double)y)*cube + cube_adjust + CUBE_CTR_Y;
    c.z = ((double)z)*cube + cube_adjust + CUBE_CTR_Z;

    return c;
}

/* Free all variables allocated for PMTA */

/* I checked for memory leaks using mallocmap() on the suns, as well
   as under codecenter, and it seems that everything that is allocated
   is indeed freed by FreeCoulomb */
void FreeCoulomb(void){
    int i,j,k,n;


    for(i=1;i < LevelLocate[NumLevels+1];i++){
        free(CellTable[i].pa);
#ifndef SERIAL
        free(CellTable[i].spa);
#endif
        free(CellTable[i].mesh.ielts);
        free(CellTable[i].mesh.inodes);
	free(CellTable[i].mesh.jgcoef);
        free(CellTable[i].iddirect);  /* allocated in both PMTApfma:215 and */
        free(CellTable[i].isdirect);  /*    PMTAinteract.c:49 ?? */

	free(CellTable[i].imultipole);
/*	if(i >= LevelLocate[NumLevels]){*/
	  free(CellTable[i].coefqselfdirect);
	  free(CellTable[i].coefuselfdirect);
	  for(j=CellTable[i].iddt-1;j>=0;j--){
	    free(CellTable[i].coefqddirect1[j]);
	    free(CellTable[i].coefuddirect1[j]);
	    free(CellTable[i].coefqddirect2[j]);
	    free(CellTable[i].coefuddirect2[j]);
	  }
	  free(CellTable[i].coefqddirect1);
	  free(CellTable[i].coefuddirect1);
	  free(CellTable[i].coefqddirect2);
	  free(CellTable[i].coefuddirect2);
	  for(j=CellTable[i].isdt-1;j>=0;j--){
	    free(CellTable[i].coefqsdirect[j]);
	    free(CellTable[i].coefusdirect[j]);
	  }
	  free(CellTable[i].coefqsdirect);
	  free(CellTable[i].coefusdirect);
	  free(CellTable[i].coefqxmultipole);
	  free(CellTable[i].coefqymultipole);
	  free(CellTable[i].coefuxmultipole);
	  free(CellTable[i].coefuymultipole);
#if (ISYM==1 || IBOT==10)
	  free(CellTable[i].coefqxmultisym);
	  free(CellTable[i].coefqymultisym);
	  free(CellTable[i].coefuxmultisym);
	  free(CellTable[i].coefuymultisym);
#endif
#if (ISYM==1 && IBOT==10)
	  free(CellTable[i].coefqxmultisym2);
	  free(CellTable[i].coefqymultisym2);
	  free(CellTable[i].coefuxmultisym2);
	  free(CellTable[i].coefuymultisym2);
	  free(CellTable[i].coefqxmultisym3);
	  free(CellTable[i].coefqymultisym3);
	  free(CellTable[i].coefuxmultisym3);
	  free(CellTable[i].coefuymultisym3);
#endif
/*	}*/
		if (!FFT) {
			free(CellTable[i].m);
#if (ISYM==1 || IBOT==10)
			free(CellTable[i].msym);
#endif
#if (ISYM==1 && IBOT==10)
			free(CellTable[i].msym2);
			free(CellTable[i].msym3);
#endif
			free(CellTable[i].l);
		} /* if not FFT (otherwise handled elsewhere */
    }
#ifndef SERIAL
    free(ProcessorTable);
    free(PmonAnalysis);
    free(PthreadTable);
    if(Direct) {
	free(SmallParticleTable);
    }
#endif

    free(ILevelAnalysis);
    free(Timing);

    free(CellTable);


    for(n=2*Mp;n >= 0;n--){
        free(A[n]);
    }
    free(A);

    for(n=2*Mp;n >= 0;n--){
        free(MYconstant[n]);
    }
    free(MYconstant);


    if(FFT){
	free(PFMA_s3_const1F[0]);
	free(PFMA_s3_const1F);
	free(fscale);
	free(fincrement);
	free(frowsz);
	free(fblocksz);
    }

    /* wow this is hairy -- hope it's right */
    for(j=Mp;j >= 0;j--){
        for(k=j;k >= -j;k--){
            for(n=Mp - j;n >= 0 ;n--){
                free(Ms3constant2[j][k][n]-n);
            }
	    free(Ms3constant2[j][k]);
	}
	free(Ms3constant2[j]-j);
    }
    free(Ms3constant2);


    for(j=Mp;j >= 0;j--){
        for(k = j;k >= -j;k--){
            for(n=Mp;n >=0;n--){
		free(Ms3constant1[j][k][n]-n);
            }
            free(Ms3constant1[j][k]);
        }
        free(Ms3constant1[j]-j);
    }
    free(Ms3constant1);


    for(j=Mp;j >=0;j--){
        for(k=j;k >= -j;k--){
            for(n=j;n >= 0;n--){
                free(Ms2constant[j][k][n] - n);
            }
            free(Ms2constant[j][k]);
        }
	free(Ms2constant[j]-j);
    }
    free(Ms2constant);

    for(i=NumProcessors-1;i >=0 ;i--){
        if(FFT){
           free(PthreadWorkTable[i].Ml);
           free(PthreadWorkTable[i].Mm);
        }
	for(j=NumLevels+1; j >= 0; j--) {
	    free(PthreadWorkTable[i].ct[0]);
	}
	free(PthreadWorkTable[i].ct);
    }
    free(PthreadILA);

    for(i= TIME_NUM-1; i >= 0; i--){
	free(PthreadTiming[i]);
    }
    free(PthreadTiming);

    free(PthreadWorkTable);
#ifndef SERIAL
    free(PthreadPmon);
#endif

    if(FFT) {
        free(transferb);
        for(i=nblocks-1;i >= 0;i--){
            free(transferf[4*(i+1) - 1]);
        }
        free(transferf);

	free(FLocal_accum[0][0]); /* this frees that scratch space */
        for(i=nblocks-1;i >= 0 ;i--){
            free(FLocal_accum[i]);
        }
	free(FLocal_accum);
    }


    for(i=0; i<= Mp; i++){
	free(Ms[i]);
    }
    free(Ms);


    free(Mt1); 

    for(i=0; i<= Mp; i++){
	free(MLegendre[i]);
    }

    free(MLegendre);

    COULOMB_INIT = FALSE;
    
}

alloc_mpe(Mptr, p)
Complex ***Mptr;
int p;
{
   int n, m;
   double *scratch;
   Complex *scratchC;
   Complex **M;


   scratchC = (Complex *) malloc(((p * (p+1))/2) * sizeof(Complex));
   M = (Complex **) malloc( p * sizeof(Complex **));
   for (n = 0; n < p; n++) {
      M[n] = scratchC;
      scratchC += n+1;
   } /* for n */
   scratch = &M[0][0].x;
   for (n=0; n < (p * (p+1)); n++)
      scratch[n] = 0.0;
   *Mptr = M;

} /* alloc_mpe */

