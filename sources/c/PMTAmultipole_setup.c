/*
 * PMTAmultipole_setup.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAmultipole_setup.c,v 1.3 1994/12/09 17:35:56 welliott Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAmultipole_setup.c,v $
 * Revision 1.3  1994/12/09  17:35:56  welliott
 * Added "if (FFT)" statement around fft variable setup.
 * Fixes the "p=6" problem.  Bill Elliott
 *
 * Revision 1.2  1994/11/11  20:45:31  lambert
 *  A couple of vars were declared but never used -- deleted them.
 *
 * min() macro was redefined to Min() in PMTA.h, as it clashed
 * with system min().
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 *
 */


#include "include/PMTA.h"

/*
 * MultipoleSetup is adapted from the PFMA code written by Jim Leathrum
 */
void MultipoleSetup(void){
    int j,k,n,m,km,jn,np,mp;
    double term;
    double term1, term2;
    double *factorial;
    double FFTBLOCK = 8.0;
    int fftl, ffti, m_size, fftn, fftm;

    if(Verbose)
        fprintf(Output,"Setting up multipole expansion constants...\n");

    /* 
     * setup factorial 
     */
    factorial = (double *) malloc((4*Mp+1)*sizeof(double));
    if(factorial == NULL) ERROR("(MutipoleSetup) malloc failure",NULL,0);
    term = 1.0;
    for(n=0;n<=(4*Mp);term *= sqrt((double) (++n))){
        factorial[n] = (double) term;
    }

    /*
     * set up A
     *
     * FFT-Version: A gets a sign change that doesn't affect it with
     * respect to the original version
     */
    A = (double **) malloc((2*Mp + 1) * sizeof(factorial));
    if(A == NULL) ERROR("(MutipoleSetup) malloc failure",NULL,0);
    for(n=0;n <= 2*Mp;n++){
        A[n] = (double *) malloc((n + 1) * sizeof(double));
        if(A[n] == NULL) ERROR("(MutipoleSetup) malloc failure",NULL,0);
    }
    term1 = 1.0;
    for(n = 0;n <= 2*Mp;n++){
        term2 = term1;
        for(m = 0;m <= n;m++){
            A[n][m] = term2/(factorial[n-m] * factorial[n+m]);
            term2 *= -1.0;
        }
        term1 *= -1.0;
    }

/*
    term = 1.0;
    for(n=0;n <= 2*Mp;n++){
        for(m=0;m <= n;m++){
            A[n][m] = term/(factorial[n-m] * factorial[n+m]);
        }
        term *= -1.0;
    }
*/
    /*
     * setup MYconstant for the spherical harmonics in the Multipole
     * Expansions
     *                          (n - |m|)!
     * MYconstant[n][m] = sqrt(------------)
     *                          (n + |m|)!
     */
    MYconstant = (double **) malloc((2*Mp+1)*sizeof(double));
    if(MYconstant == NULL) ERROR("(MutipoleSetup) malloc failure",NULL,0);
    for(n=0;n<=(2*Mp);n++){
        MYconstant[n] = (double *) malloc((n+1)*sizeof(double));
        if(MYconstant[n] == NULL)
            ERROR("(MutipoleSetup) malloc failure",NULL,0);
    }
    for(n=0;n<=(2*Mp);n++){
        for(m=0;m<=n;m++){
            MYconstant[n][m] = factorial[n-m]/factorial[n+m];
        }
    }

    /*
     * setup Ms2constant
     *
     *                           J[m][k-m]*A[n][m]*A[j-n][k-m]*MYconstant[n][m]
     * Ms2constant[j][k][n][m] = ----------------------------------------------
     *                                              A[j][k]
     */
    Ms2constant = (double ****) malloc((Mp+1) * sizeof(Ms2constant));
    if(Ms2constant == NULL) ERROR("(MutipoleSetup) malloc failure",NULL,0);
    for(j=0;j <= Mp;j++){
        Ms2constant[j] = (double ***) malloc((2*j+1) * sizeof(Ms2constant));
        if(Ms2constant[j] == NULL) 
            ERROR("(MutipoleSetup) malloc failure",NULL,0);
        Ms2constant[j] += j;
        for(k=-j;k <= j;k++){
            Ms2constant[j][k] = (double **) malloc((j+1) * sizeof(Ms2constant));
            if(Ms2constant[j][k] == NULL) 
                ERROR("(MutipoleSetup) malloc failure",NULL,0);
            for(n=0;n <= j;n++){
                Ms2constant[j][k][n] = (double *) 
                    malloc((2*n+1) * sizeof(double));
                if(Ms2constant[j][k][n] == NULL) 
                    ERROR("(MutipoleSetup) malloc failure",NULL,0);
                Ms2constant[j][k][n] += n;
                for(m=-n;m <= n;m++){
                    km = k-m;
                    jn = j-n;
                    if(abs(km) <= jn){
                        Ms2constant[j][k][n][m] = (((m*km < 0) ? 
                            ((Min(abs(km),abs(m)) & 1) ? -1.0:1.0):1.0)
                            /* ^ J term */
                            * A[n][abs(m)]
                            * A[jn][abs(km)]
                            * MYconstant[n][abs(m)])/A[j][abs(k)];
                    } else {
                        Ms2constant[j][k][n][m] = 0.0; 
                    }
                }
            }
        }
    }

    /*
     * setup Ms3constant1
     *
     *                            J*A[n][m]*A[j][k]*MYconstant[n][m]
     * Ms3constant1[j][k][n][m] = ----------------------------------
     *                                       A[j+n][k+m]
     */
    Ms3constant1 = (double ****) malloc((Mp+1) * sizeof(Ms3constant1));
    if(Ms3constant1 == NULL) ERROR("(MutipoleSetup) malloc failure",NULL,0);
    for(j=0;j <= Mp;j++){
        Ms3constant1[j] = (double ***) malloc((2*j+1) * sizeof(Ms3constant1));
        if(Ms3constant1[j] == NULL) 
            ERROR("(MutipoleSetup) malloc failure",NULL,0);
        Ms3constant1[j] += j;
        for(k = -j;k <= j;k++){
            Ms3constant1[j][k] = (double **) 
                malloc((Mp+1) * sizeof(Ms3constant1));
            if(Ms3constant1[j][k] == NULL) 
                ERROR("(MutipoleSetup) malloc failure",NULL,0);
            for(n=0;n <= Mp;n++){
                /* if(j+n <= Mp) */{
                Ms3constant1[j][k][n] = (double *) 
                    malloc((2*n+1) * sizeof(double));
                if(Ms3constant1[j][k][n] == NULL) 
                    ERROR("(MutipoleSetup) malloc failure",NULL,0);
                Ms3constant1[j][k][n] += n;
                for(m=-n;m <= n;m++){
                    /*if(m-k <= Mp)*/{
                    /* funny handling of term needed for sequent */
                    term = ((m*k > 0) ? (((Min(abs(k),abs(m)))&1) 
                        ? -1.0:1.0):1.0);
                    term *= ((n & 1) ? -1.0:1.0);
                    Ms3constant1[j][k][n][m] =
                        term * A[n][abs(m)] * A[j][abs(k)] *
                        MYconstant[j+n][abs(m-k)]/A[j+n][abs(m-k)]; }
                }}
            }
        }
    }
    
    /*
     * setup Ms3constant2
     *
     *                            J*A[n][m]*A[j][k]*MYconstant[n][m]
     * Ms3constant2[j][k][n][m] = ----------------------------------
     *                                       A[j+n][k+m]
     */
    Ms3constant2 = (double ****) malloc((Mp+1)*sizeof(Ms3constant2));
    if(Ms3constant2 == NULL) ERROR("(MutipoleSetup) malloc failure",NULL,0);
    for(j=0;j <= Mp;j++){
        Ms3constant2[j] = (double ***) malloc((2*j+1)*sizeof(Ms3constant2));
        if(Ms3constant2[j] == NULL) 
            ERROR("(MutipoleSetup) malloc failure",NULL,0);
        Ms3constant2[j] += j;
        for(k=-j;k <= j;k++){
            Ms3constant2[j][k] = (double **) 
                malloc((Mp-j+1)*sizeof(Ms3constant2));
            if(Ms3constant2[j][k] == NULL) 
                ERROR("(MutipoleSetup) malloc failure",NULL,0);
            for(n=0;n <= Mp - j;n++){
                Ms3constant2[j][k][n] = (double *) 
                    malloc((2*n+1)*sizeof(double));
                if(Ms3constant2[j][k][n] == NULL)
                    ERROR("(MutipoleSetup) malloc failure",NULL,0);
                Ms3constant2[j][k][n] += n;
                for(m=-n;m <= n;m++){
                    np = n + j;
                    mp = m + k;
                    if((abs(np) <= Mp) && (abs(mp) <= Mp)){
                        term = (A[n][abs(m)] * A[j][abs(k)] *
                            MYconstant[n][abs(m)])/A[np][abs(mp)];
                        if(j & 1)
                            term = -term;
                        if(np & 1)
                            term = -term;
                        if(mp == 0){
                            if(k & 1)
                                term = -term; 
                        }
                        if((mp < 0) && (k > 0)){
                            if(k & 1)
                                term = -term; 
                        }
                        if((mp > 0) && (k < 0)){
                            if(k & 1)
                                term = -term; 
                        }
                        if((mp < 0) && (m > 0)){
                            if(m & 1)
                                term = -term; 
                        }
                        if((mp > 0) && (m < 0)){
                            if(m & 1)
                                term = -term; 
                        }
                        Ms3constant2[j][k][n][m] = term; 
                    }
                }
            }
        }
    }

/* FFT-Version */
/* various constants relating to the FFT arrays */

/*
   calculate number of blocks in the l direction and length of fft array
   in the m direction, allocate enough memory to hold the fft of the
   multipole expansions
 */
if (FFT) {
  nblocks = (int) (2.0/FFTBLOCK * (float) (Mp+1));
  fftarray_m = 1 << ((int) (log((double)(2*(Mp+1)-1))/log(2.0)));
  fftsize = fftarray_m * (int) FFTBLOCK;
  fblocksz = (int *) malloc(nblocks * sizeof(int));
  frowsz = (int *) malloc((Mp+1) * sizeof(int));
  fincrement = (int *) malloc(nblocks * sizeof(int));
  fscale = (double *) malloc((Mp+1) * sizeof(double));
  for (fftl = 0; fftl < Mp+1 >> 2; fftl++) {
      m_size = 1 << ((int) (log((double)(FFTBLOCK*(fftl+1)-1))/log(2.0)));
      for (ffti = 0; ffti < (int) FFTBLOCK / 2; ffti++) {
         frowsz[((int) FFTBLOCK / 2) * fftl+ffti] = m_size;
      } /* for ffti */
  } /* for fftl */
  for (fftl = 0; fftl < nblocks; fftl++) {
      fblocksz[fftl] = (int) FFTBLOCK * frowsz[4*(fftl+1) - 1];
   }
   for (fftl = 0; fftl < nblocks; fftl++) {
      fincrement[fftl] = ((frowsz[Mp] / frowsz[fftl << 2]) << 1) - 1;
   }
   for (fftl = 0; fftl < (Mp+1); fftl++) {
      fscale[fftl] = (double) (frowsz[Mp] / frowsz[fftl]);
   }

   scale = 0.5/(double)(fftarray_m * (int)FFTBLOCK); 
   /* only using half of array */

/* Set up constants for LOC_EXP FFT version */

      PFMA_s3_const1F = (double **) malloc((Mp+1) * sizeof(double **));
      PFMA_s3_const1F[0] = (double *) malloc((Mp+1)*(Mp+2)/2*sizeof(double));
      PFMA_s3_const1F[0][0]  = fscale[0] * MYconstant[0][0] / A[0][0];
      for (fftn = 1; fftn <= Mp; fftn++) {
          PFMA_s3_const1F[fftn] = &PFMA_s3_const1F[0][fftn*(fftn+1)/2];
          for (fftm=0; fftm <= fftn; fftm++)
               PFMA_s3_const1F[fftn][fftm] = fscale[fftn] * 
                                        MYconstant[fftn][fftm]/A[fftn][fftm];
      } /* for fftn */

} /* if FFT */
/* end FFT-Version */

/* FFT-Version: no longer need fact, still need A */

    free(factorial);

}
