/*
 * PMTApfma.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTApfma.c,v 1.1 1994/10/08 04:34:10 wrankin Exp $";

/*
 * Revision History:
 *
 * $Log: PMTApfma.c,v $
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 * adapted for BEM, C.Fochesato, CMLA, 2003
 */


#include "include/PMTA.h"

int CellId(int x, int y, int z, int level){
    int j,c,bit,shift;

    for(j=0,c=0,bit=1,shift=0;j < level;j++){
        c |= (bit & x) << shift;
        shift++;
        c |= (bit & y) << shift;
        shift++;
        c |= (bit & z) << shift;
        bit <<= 1;
    }
    c += LevelLocate[level];
    
    return c;

}

void PFMAInteractLists(void){
    int i,j,k,m,level,max_cells,cell,parent,ila_parent;
    int pl_max,cl_max,count,sdcount,temp;
    int cx,cy,cz;               /* cell coordinates */
    int px,py,pz;               /* parent cell coordinates */
    int tx,ty,tz;               /* temporary coordinates */

    for(level=1;level <= NumLevels;level++){
        max_cells = PthreadCT[level][PTHREAD_END];
        for(i=PthreadCT[level][PTHREAD_START];i < max_cells;i++){

            CellTable[i].imultipole = (int *) malloc(189 * sizeof(int));
            if(CellTable[i].imultipole == NULL)
                ERROR("(PFMAInteractLists) malloc failure",NULL,0);
            count = 0;
           
            /*
             * determine cell location
             */
            cl_max = 1;
            cx = cy = cz = 0;
            cell = i - LevelLocate[level];
            for(j=0;j < level;j++){
                cx |= (cell & 01) << j;
                cell >>= 1;
                cy |= (cell & 01) << j;
                cell >>= 1;
                cz |= (cell & 01) << j;
                cell >>= 1;
                cl_max <<= 1;
            }
            pl_max = cl_max >> 1;
            /*
             * determine location of parent cell
             */
            px = py = pz = 0;
            parent = (i - LevelLocate[level]) >> 3;
            for(j=0;j < (level-1);j++){
                px |= (parent & 01) << j;
                parent >>= 1;
                py |= (parent & 01) << j;
                parent >>= 1;
                pz |= (parent & 01) << j;
                parent >>= 1;
            }
            
            /*
             * interactions with upper surface at parent level
             */
            if((pz+2) < pl_max)
                for(j=px-2;j <= (px+2);j++)
                    if(j >= 0 && j < pl_max)
                        for(k=py-2;k <= (py+2);k++)
                            if(k >= 0 && k < pl_max)
                                CellTable[i].imultipole[count++] = 
                                    CellId(j,k,pz+2,level-1);
            /*
             * interactions with lower surface at parent level
             */
            if((pz-2) >= 0)
                for(j=px-2;j <= (px+2);j++)
                    if(j >= 0 && j < pl_max)
                        for(k=py-2;k <= (py+2);k++)
                            if(k >= 0 && k < pl_max)
                                CellTable[i].imultipole[count++] = 
                                    CellId(j,k,pz-2,level-1);
            /*
             * interactions with four side surfaces at parent level
             */
            if((px-2) >= 0)
                for(j=py-2;j <= (py+2);j++)
                    if(j >= 0 && j < pl_max)
                        for(k=pz-1;k <= (pz+1);k++)
                            if(k >= 0 && k < pl_max)
                                CellTable[i].imultipole[count++] = 
                                    CellId(px-2,j,k,level-1);   
            if((px+2) < pl_max)
                for(j=py-2;j <= (py+2);j++)
                    if(j >= 0 && j < pl_max)
                        for(k=pz-1;k <= (pz+1);k++)
                            if(k >= 0 && k < pl_max)
                                CellTable[i].imultipole[count++] = 
                                    CellId(px+2,j,k,level-1);   
            if((py-2) >= 0)
                for(j=px-1;j <= (px+1);j++)
                    if(j >= 0 && j < pl_max)
                        for(k=pz-1;k <= (pz+1);k++)
                            if(k >= 0 && k < pl_max)
                                CellTable[i].imultipole[count++] = 
                                    CellId(j,py-2,k,level-1);   
            if((py+2) < pl_max)
                for(j=px-1;j <= (px+1);j++)
                    if(j >= 0 && j < pl_max)
                        for(k=pz-1;k <= (pz+1);k++)
                            if(k >= 0 && k < pl_max)
                                CellTable[i].imultipole[count++] = 
                                    CellId(j,py+2,k,level-1);
                                    
            ila_parent = count;
            PthreadILA[level-1] += count;
            
            /*
             * interactions with three surfaces at cell level
             */ 
            if(cx & 01){
                tx = cx - 3;
            } else {
                tx = cx + 3;
            }
            if(tx >= 0 && tx < cl_max)
                for(j=cy-2;j <= (cy+2);j++)
                    if(j >= 0 && j < cl_max)
                        for(k=cz-2;k <= (cz+2);k++)
                            if(k >= 0 && k < cl_max)
                                CellTable[i].imultipole[count++] = 
                                    CellId(tx,j,k,level);
            if(cy & 01){
                ty = cy - 3;
            } else {
                ty = cy + 3;
            }
            if(ty >= 0 && ty < cl_max)
                for(j=cx-2;j <= (cx+2);j++)
                    if(j >= 0 && j < cl_max)
                        for(k=cz-2;k <= (cz+2);k++)
                            if(k >= 0 && k < cl_max)
                                CellTable[i].imultipole[count++] = 
                                    CellId(j,ty,k,level);
            if(cz & 01){
                tz = cz - 3;
            } else {
                tz = cz + 3;
            }
            if(tz >= 0 && tz < cl_max)
                for(j=cx-2;j <= (cx+2);j++)
                    if(j >= 0 && j < cl_max)
                        for(k=cy-2;k <= (cy+2);k++)
                            if(k >= 0 && k < cl_max)
                                CellTable[i].imultipole[count++] =
                                    CellId(j,k,tz,level);
                                    
            if(tx >= 0 && tx < cl_max && ty >= 0 && ty < cl_max)
                for(j=cz-2;j <= (cz+2);j++)
                    if(j >= 0 && j < cl_max)
                        CellTable[i].imultipole[count++] = 
                            CellId(tx,ty,j,level);
            if(ty >= 0 && ty < cl_max && tz >= 0 && tz < cl_max)
                for(j=cx-2;j <= (cx+2);j++)
                    if(j >= 0 && j < cl_max)
                        CellTable[i].imultipole[count++] = 
                            CellId(j,ty,tz,level);
            if(tx >= 0 && tx < cl_max && tz >= 0 && tz < cl_max)
                for(j=cy-2;j <= (cy+2);j++)
                    if(j >= 0 && j < cl_max)
                        CellTable[i].imultipole[count++] = 
                            CellId(tx,j,tz,level);
            
            if(tx >= 0 && tx < cl_max && ty >= 0 && ty < cl_max && 
                tz >= 0 && tz < cl_max)
                CellTable[i].imultipole[count++] = CellId(tx,ty,tz,level);
            
            PthreadILA[level] += count - ila_parent;
            
            CellTable[i].ime = count;
            if(count < 189){
                CellTable[i].imultipole = (int *)
                    realloc(CellTable[i].imultipole,count * sizeof(int));
                if(CellTable[i].imultipole == NULL && count > 0)
                    ERROR("(PFMAInteractLists) realloc failure",NULL,0);
            }

            if(level == NumLevels){
                CellTable[i].iddirect = (int *) malloc(124 * sizeof(int));
                CellTable[i].coefqddirect1 = (Coefs *) malloc(124 * sizeof(Coefs));
                CellTable[i].coefuddirect1 = (Coefs *) malloc(124 * sizeof(Coefs));
                CellTable[i].coefqddirect2 = (Coefs *) malloc(124 * sizeof(Coefs));
                CellTable[i].coefuddirect2 = (Coefs *) malloc(124 * sizeof(Coefs));
                CellTable[i].isdirect = (int *) malloc(124 * sizeof(int));
                CellTable[i].coefqsdirect = (Coefs *) malloc(124 * sizeof(Coefs));
                CellTable[i].coefusdirect = (Coefs *) malloc(124 * sizeof(Coefs));
                if(CellTable[i].coefqddirect1 == NULL ||
                     CellTable[i].coefuddirect1 == NULL ||
		     CellTable[i].coefqddirect2 == NULL ||
		     CellTable[i].coefuddirect2 == NULL ||
		     CellTable[i].iddirect == NULL ||
                    CellTable[i].coefqsdirect == NULL ||
		     CellTable[i].coefusdirect == NULL ||
		     CellTable[i].isdirect == NULL)
                    ERROR("(PFMAInteractLists) malloc failure",NULL,0);
                count = sdcount = 0;
                
                for(j=cx-2;j <= (cx+2);j++)
                    if(j >= 0 && j < cl_max)
                        for(k=cy-2;k <= (cy+2);k++)
                            if(k >= 0 && k < cl_max)
                                for(m=cz-2;m <= (cz+2);m++)
                                    if(m >= 0 && m < cl_max)
                                        if(!(j == cx && k == cy && m == cz)){
                                            temp = CellId(j,k,m,NumLevels);
                                            if(temp >= 
                                            PthreadCT[NumLevels][PTHREAD_START]
                                            && temp <
                                            PthreadCT[NumLevels][PTHREAD_END]){
                                            if(i < temp)
                                            CellTable[i].iddirect[count++] = 
                                                temp;
                                            } else if(BALANCED && i < temp){
                                            CellTable[i].iddirect[count++] = 
                                                temp;
					    					} else if(!BALANCED){
                                            CellTable[i].isdirect[sdcount++] = 
                                                temp;
                                            }
                                        }
                                    
                PthreadILA[NumLevels] += count + sdcount;
                
                CellTable[i].iddt = count;
                CellTable[i].isdt = sdcount;
                CellTable[i].iddirect = (int *)
                    realloc(CellTable[i].iddirect,count * sizeof(int));
                CellTable[i].coefqddirect1 = (Coefs *)
                    realloc(CellTable[i].coefqddirect1,count * sizeof(Coefs));
                CellTable[i].coefuddirect1 = (Coefs *)
                    realloc(CellTable[i].coefuddirect1,count * sizeof(Coefs));
                CellTable[i].coefqddirect2 = (Coefs *)
                    realloc(CellTable[i].coefqddirect2,count * sizeof(Coefs));
                CellTable[i].coefuddirect2 = (Coefs *)
                    realloc(CellTable[i].coefuddirect2,count * sizeof(Coefs));
                CellTable[i].isdirect = (int *)
                    realloc(CellTable[i].isdirect,sdcount * sizeof(int));
                CellTable[i].coefqsdirect = (Coefs *)
                    realloc(CellTable[i].coefqsdirect,sdcount * sizeof(Coefs));
                CellTable[i].coefusdirect = (Coefs *)
                    realloc(CellTable[i].coefusdirect,sdcount * sizeof(Coefs));
                if(((CellTable[i].coefqddirect1 == NULL ||
                     CellTable[i].coefuddirect1 == NULL ||
		     CellTable[i].coefqddirect2 == NULL ||
		     CellTable[i].coefuddirect2 == NULL ||
		     CellTable[i].iddirect == NULL) && count > 0) ||
                    ((CellTable[i].coefqsdirect == NULL ||
		     CellTable[i].coefusdirect == NULL ||
		     CellTable[i].isdirect == NULL) && sdcount > 0))
                    ERROR("(PFMAInteractLists) realloc failure",NULL,0);
            }
        }
    }

}
