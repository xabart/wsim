/*
 * PMTAparallel.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAparallel.c,v 1.3 1994/12/02 18:08:35 wrankin Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAparallel.c,v $
 * Revision 1.3  1994/12/02  18:08:35  wrankin
 * changed timing routines to use teh more common gettimeofday() and times()
 * system calls.
 *
 * Revision 1.2  1994/11/11  20:47:51  lambert
 * A couple of vars were declared but never used -- deleted them.
 *
 * Parallel() was incorrectly declared as returning void *, fixed it.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 * adpated for BEM, C.Fochesato, CMLA, 2003
 */


#include "include/PMTA.h"

void ParallelSetup(void *Arg){
    int     i,block_size,block_size2;

#ifndef SERIAL
    int error;
#endif
    PthreadNum = (int) Arg;

#ifndef SERIAL
    /*
     * bind pthread to processor
     */
    error = pthread_move(PthreadTable[PthreadNum],ProcessorTable[PthreadNum],
        TH_BOUND_PROCESSOR);
    if(error == -1) ERROR("(ParallelSetup) pthread_move failure",NULL,0);
#endif

    /*
     * calculate load responsibility in both the Particle,Element and Cell Tables
     */
    block_size = NumParticles/NumProcessors;
    PthreadPTStart = PthreadNum * block_size;
    PthreadPTEnd = PthreadPTStart + block_size;
    if(PthreadNum == (NumProcessors-1)) PthreadPTEnd = NumParticles;
    PthreadWorkTable[PthreadNum].ptstart = PthreadPTStart;
    PthreadWorkTable[PthreadNum].ptend = PthreadPTEnd;
    block_size2 = NumElements/NumProcessors;
    PthreadETStart = PthreadNum * block_size2;
    PthreadETEnd = PthreadETStart + block_size2;
    if(PthreadNum == (NumProcessors-1)) PthreadETEnd = NumElements;
    PthreadWorkTable[PthreadNum].etstart = PthreadETStart;
    PthreadWorkTable[PthreadNum].etend = PthreadETEnd;

    PthreadWorkTable[PthreadNum].ct = (int **) malloc((NumLevels+2) *
        sizeof(int *));
    if(PthreadWorkTable[PthreadNum].ct == NULL)
        ERROR("(ParallelSetup) malloc failure",NULL,0);
    for(i=0;i <= (NumLevels+1);i++){
    /*for(i=1;i <= (NumLevels);i++){*/
        PthreadWorkTable[PthreadNum].ct[i] = (int *) malloc(2*sizeof(int));
        if(PthreadWorkTable[PthreadNum].ct[i] == NULL)
            ERROR("(ParallelSetup) malloc failure",NULL,0);
    }
    PthreadCT = PthreadWorkTable[PthreadNum].ct;
    for(i=1;i <= NumLevels;i++){
		if(PthreadNum >= Power2Processors){
            PthreadCT[i][PTHREAD_START] = 0;
            PthreadCT[i][PTHREAD_END] = 0;
        } else if(Power2Processors < Power8[i]){
            block_size = Power8[i]/Power2Processors;
            PthreadCT[i][PTHREAD_START] = PthreadNum * block_size +
                LevelLocate[i];
            PthreadCT[i][PTHREAD_END] = PthreadCT[i][PTHREAD_START] +
                block_size;
        } else if(PthreadNum % (Power2Processors/Power8[i]) == 0){
            PthreadCT[i][PTHREAD_START] =
				(PthreadNum*Power8[i])/Power2Processors + LevelLocate[i];
            PthreadCT[i][PTHREAD_END] = PthreadCT[i][PTHREAD_START] + 1;
        } else {
            PthreadCT[i][PTHREAD_START] = 0;
            PthreadCT[i][PTHREAD_END] = 0;
        }
    }

    /*
     * allocate and link pthread interaction level analysis,
     * pthread timing and pthread pmon arrays to shared arrays
     */
    PthreadILA = (int *) malloc((NumLevels+1) * sizeof(int));
    if(PthreadILA == NULL) ERROR("(ParallelSetup) malloc failure",NULL,0);
    ILevelAnalysis[PthreadNum] = PthreadILA;

    PthreadTiming = (double **) malloc(TIME_NUM * sizeof(double *));
    if(PthreadTiming == NULL) ERROR("(ParallelSetup) malloc failure",NULL,0);
    for(i=0;i < TIME_NUM;i++){
        PthreadTiming[i] = (double *) malloc(2 * sizeof(double));
        if(PthreadTiming[i] == NULL)
            ERROR("(ParallelSetup) malloc failure",NULL,0);
        PthreadTiming[i][TIME_USER] = PthreadTiming[i][TIME_WALL] = 0;
    }
    Timing[PthreadNum] = PthreadTiming;

#ifndef SERIAL
    PthreadPmon = (PMON_DATA *) malloc(PMON_NUM * sizeof(PMON_DATA));
    if(PthreadPmon == NULL) ERROR("(ParallelSetup) malloc failure",NULL,0);
    PmonAnalysis[PthreadNum] = PthreadPmon;

    error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    if(error == -1)
        ERROR("(ParallelSetup) pthread_barrier_checkin failure",NULL,0);
#endif

    /*
     * create the CellTable:
     * calculate centers and lengths of cells, allocate space for cell
     * multipole and local expansions, allocate space and calculate cell
     * interaction lists
     */
    CellsSetup();

    /*
     * calculate cell identifiers for particles and allocate cell particle
     * lists
     */
    ParticleCells();

    /*
     * fill cell particle lists
     */
    CellPlists();

    /*
     * calculate nnie and allocate coefs 0=without freeing coefs
     */
    ParticleCellsCoef(0);

    PthreadWorkTable[PthreadNum].mt1 = Mt1;
    PthreadWorkTable[PthreadNum].ms = Ms;
    PthreadWorkTable[PthreadNum].mlegendre = MLegendre;
    PthreadWorkTable[PthreadNum].transferf = transferf;
    PthreadWorkTable[PthreadNum].transferb = transferb;
    PthreadWorkTable[PthreadNum].finput = finput;
    PthreadWorkTable[PthreadNum].FLocal_accum = FLocal_accum;

#ifndef SERIAL
    error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    if(error == -1)
        ERROR("(ParallelSetup) pthread_barrier_checkout failure",NULL,0);
#endif

}

void Parallel(void *Arg){
    int i;
#ifndef SERIAL
    int error;
#endif

    PthreadNum = (int) Arg;

#ifndef SERIAL
    /*
     * bind pthread to processor and determine load responsibilty
     * also link PthreadILA, PthreadTiming, and PthreadPmon with
     * global structures
     */
    error = pthread_move(PthreadTable[PthreadNum],ProcessorTable[PthreadNum],
        TH_BOUND_PROCESSOR);
    if(error == -1) ERROR("(Parallel) pthread_move failure",NULL,0);
    PthreadPTStart = PthreadWorkTable[PthreadNum].ptstart;
    PthreadPTEnd = PthreadWorkTable[PthreadNum].ptend;
    PthreadETStart = PthreadWorkTable[PthreadNum].etstart;
    PthreadETEnd = PthreadWorkTable[PthreadNum].etend;
    PthreadCT = PthreadWorkTable[PthreadNum].ct;
    PthreadILA = ILevelAnalysis[PthreadNum];
    PthreadTiming = Timing[PthreadNum];
    PthreadPmon = PmonAnalysis[PthreadNum];

    Mt1 = PthreadWorkTable[PthreadNum].mt1;
    Ms = PthreadWorkTable[PthreadNum].ms;
    MLegendre = PthreadWorkTable[PthreadNum].mlegendre;
    transferf = PthreadWorkTable[PthreadNum].transferf;
    transferb = PthreadWorkTable[PthreadNum].transferb;
    finput = PthreadWorkTable[PthreadNum].finput;
    FLocal_accum = PthreadWorkTable[PthreadNum].FLocal_accum;

    error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkin failure",NULL,0);
#endif
	
    /*
     * clear all timers except TIME_CS, TIME_PC and TIME_CPL at start 
     * of timestep
     */
    for(i=3;i < TIME_NUM;i++){
        PthreadTiming[i][TIME_USER] = PthreadTiming[i][TIME_WALL] = 0;
    }

    PthreadStartTiming(TIME_AM);
#ifndef SERIAL
    if(Pmon){
        error = pmon_delta(&PthreadPmon[PMON_AM]);
        if(error != 0) ERROR("(Parallel) pmon_delta failure",NULL,0);
    }
#endif

	/*
     * read particle positions into the CellTable and check that the 
     * particles are correctly located.  If particles are incorrectly 
     * located, then insert into appropriate cells
	 */
	AdjustParticles();

#ifndef SERIAL
    error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkout failure",NULL,0);

    error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkin failure",NULL,0);
#endif

    /*
     * calculate cell multipole expansions with elements instead particles
     */
    CellMultipoles();

#ifndef SERIAL
    error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkout failure",NULL,0);

    error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkin failure",NULL,0);
#endif

    /*
     * traverse interaction lists, calculate local expansions
     * and direct forces
     */
	if(BALANCED){
		BalancedCellForces();
#ifndef SERIAL
    	error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    	if(error == -1)
			ERROR("(Parallel) pthread_barrier_checkout failure",NULL,0);

    	error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    	if(error == -1)
			ERROR("(Parallel) pthread_barrier_checkin failure",NULL,0);
#endif
	} else {
    	CellForces();
	}

    /*
     * write particle force and potential results in ParticleTable
     */
    CellResults();

    PthreadStopTiming(TIME_AM);
#ifndef SERIAL
    if(Pmon){
        pmon_delta(&PthreadPmon[PMON_AM]);
        if(error != 0) ERROR("(Parallel) pmon_delta failure",NULL,0);
    }

    error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkout failure",NULL,0);

    error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkin failure",NULL,0);

    /*
     * calculate forces and potentials by the direct method
     */
    if(Pmon && Direct){
        pmon_delta(&PthreadPmon[PMON_DM]);
        if(error != 0) ERROR("(Parallel) pmon_delta failure",NULL,0);
    }
#endif

    if(Direct){
        DirectForces();
    }

#ifndef SERIAL
    if(Pmon && Direct){
        pmon_delta(&PthreadPmon[PMON_DM]);
        if(error != 0) ERROR("(Parallel) pmon_delta failure",NULL,0);
    }

    error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkout failure",NULL,0);
#endif

}


void PthreadStartTiming(int ThisFunction){

#ifdef SERIAL
    struct tms timebuf;
    struct timeval runstruct;
    double user_seconds, all_seconds;

    gettimeofday(&runstruct,0);
    all_seconds=(double)runstruct.tv_sec+((double)runstruct.tv_usec)/1000000.0;

    times(&timebuf);
    user_seconds = (double)(timebuf.tms_utime)/(double)CLK_TCK;
 
    PthreadTiming[ThisFunction][TIME_USER] -= user_seconds;
    PthreadTiming[ThisFunction][TIME_WALL] -= all_seconds;
#else
    PthreadTiming[ThisFunction][TIME_USER] -= user_seconds();
    PthreadTiming[ThisFunction][TIME_WALL] -= all_seconds();
#endif

}

void PthreadStopTiming(int ThisFunction){
#ifdef SERIAL
    struct tms timebuf;
    struct timeval runstruct;
    double user_seconds, all_seconds;

    gettimeofday(&runstruct,0);
    all_seconds=(double)runstruct.tv_sec+((double)runstruct.tv_usec/1000000);

    times(&timebuf);
    user_seconds = (double)(timebuf.tms_utime)/(double)CLK_TCK;

    PthreadTiming[ThisFunction][TIME_USER] += user_seconds;
    PthreadTiming[ThisFunction][TIME_WALL] += all_seconds;
#else
    PthreadTiming[ThisFunction][TIME_USER] += user_seconds();
    PthreadTiming[ThisFunction][TIME_WALL] += all_seconds();
#endif

}
