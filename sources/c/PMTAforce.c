/*
 * PMTAforce.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAforce.c,v 1.3 1995/01/17 17:17:54 welliott Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAforce.c,v $
 * Revision 1.3  1995/01/17  17:17:54  welliott
 * Commented out all prefetch statements to prevent the program
 * from locking up. Locks up inconsistently in parallel mode on the
 * KSR when prefetches are included.
 *
 * Revision 1.2  1994/11/11  20:23:40  lambert
 * Couple of variables that were declared and never used were deleted.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 * adapted for BEM, C.Fochesato, CMLA, 2003
 *
 */


#include "include/PMTA.h"

/*
 * CellForces computes the forces and potentials for all particles
 */
void CellForces(void){
    int i,j,k,level,max_cells,index;
    int n,nbelts,interj,nj,nbeltsj,jg,jj,kk;
    int *interact,*ielts,*inodes,*ieltsj,*inodesj;
    Mtype ML;
    int fftb;
    double *scratch;
#ifndef SERIAL
    int error;
    int prefetch_count;
    char *prefetch_pointer;
#endif

extern struct {
   double q[NUMPART];
   double sol[NUMPART];
   int ibemanalysis;
   int irightleftrigid;
} charge_;
extern struct {
   double zmax;
   int incond[NUMPART];
   int inface[NUMPART];
} dbnode_;

    PthreadStartTiming(TIME_CF);
    PthreadStartTiming(TIME_CFM);

    if(Verbose && PthreadNum == 0)
        fprintf(Output,"Calculating cell forces and potentials...\n");

#ifndef SERIAL
    if(FFT){
        prefetch_count = (int) ((4.0 * ((double) sizeof(double)) *
            ((double) (Mp+1)) * ((double) fftarray_m))/((double) 128) + 1.0);
    } else {
        prefetch_count = (int) ((((double) sizeof(double)) *
            ((double) (Mp+2)) * ((double) (Mp+1)))/((double) 128) + 1.0);
    }
#endif

    for(level=(PFMA ? 2:NumLevels);level <= NumLevels;level++){
        max_cells = PthreadCT[level][PTHREAD_END];
        for(i=PthreadCT[level][PTHREAD_START];i < max_cells;i++){

            /*
             * initialize local expansions
             */
            ML = CellTable[i].l;
            for(j=0;j <= Mp;j++){
                for(k=0;k <= j;k++){
                    ML[j][k].x = 0.0;
                    ML[j][k].y = 0.0;
                }
            }
            CellTable[i].lvl = FALSE;
        }
    }

    for(level=(PFMA ? 2:NumLevels);level < NumLevels;level++){
#ifndef SERIAL
        if(PFMA && level <= ProcessorLevel && level > 2){
            error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
            if(error == -1) ERROR("(CellForces) pthread_barrier_checkout "
                "failure",NULL,0);

            error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
            if(error == -1) ERROR("(CellForces) pthread_barrier_checkin "
                "failure",NULL,0);
        }
#endif
        max_cells = PthreadCT[level][PTHREAD_END];
        for(i=PthreadCT[level][PTHREAD_START];i < max_cells;i++){

			/*
			 * check that there are M2L interactions and that
			 * this cell contains particles-check the validity of
			 * the multipole expansion
			 */
            if(CellTable[i].ime > 0){
                /*
                 * calculate local expansion using multipoles of cells
                 * listed in the multipole interaction list
                 */

                if(FFT){
                    scratch = &FLocal_accum[0][0][0].x;
                    for (fftb=0; fftb < 2*(Mp+1)*fftarray_m; fftb++) {
                        *scratch++ = 0.0;
                        *scratch++ = 0.0;
                    } /* for fftb */
                } /* if FFT */

                interact = CellTable[i].imultipole;
#ifndef SERIAL

                for(j=0;j < CellTable[i].ime;j++){
                    prefetch_pointer = (char *) CellTable[interact[j]].m;
                    for(k=0;k < prefetch_count;k++){
/*                        _pcsp(prefetch_pointer,"ro","bl"); */
                        prefetch_pointer += 128;
                    }
                }

#endif
                for(j=0;j < CellTable[i].ime;j++){
                    if(CellTable[interact[j]].vl){
                        if(FFT)
                            loc_expF(i, interact[j], FLocal_accum);
                        else
                            MLocal(i,interact[j]);
                        CellTable[i].lvl = TRUE;
                    }
                }

                if(FFT)
                    IFFTLocal(i, FLocal_accum);

            } /* if CellTable... */

            /*
             * translate parent's local expansion to local expansions
             * for each child
             */
            if(CellTable[i].lvl){
                index = ((i-LevelLocate[level]) << 3)+LevelLocate[level+1];
                for(j=0;j < 8;j++){
                    MShiftLocal(index + j, i);
                    CellTable[index + j].lvl = TRUE;
                }
            }
        } /* for i */
    } /* for level */

#ifndef SERIAL
    if(PFMA && NumLevels <= ProcessorLevel && NumLevels > 2){
        error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
        if(error == -1) ERROR("(CellForces) pthread_barrier_checkout "
            "failure",NULL,0);

        error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
        if(error == -1) ERROR("(CellForces) pthread_barrier_checkin "
            "failure",NULL,0);
    }
#endif
    PthreadStopTiming(TIME_CFM);
    max_cells = PthreadCT[NumLevels][PTHREAD_END];
    for(i=PthreadCT[NumLevels][PTHREAD_START];i < max_cells;i++){

    	nbelts=CellTable[i].mesh.nbelts;
   	ielts=CellTable[i].mesh.ielts;
	n=CellTable[i].n;
	inodes=CellTable[i].mesh.inodes;
        if(nbelts == 0 && n == 0) continue;
        PthreadStartTiming(TIME_CFD);
        /*
         * calculate direct interactions within cell
	 *
	 * call to bem analysis, storage in coeffcients for reuse
         */

	if(n > 0 && nbelts > 0){
	  /*
	   * contributions of elements from cell j to nodes of cell j
	   */
	  if(charge_.ibemanalysis){
	    bemk2_(CellTable[i].coefqselfdirect,CellTable[i].coefuselfdirect,
	           &nbelts,ielts,&n,inodes,&(CellTable[i].mesh.nnie));
	  }
	  for(j=0;j<CellTable[i].mesh.nnie;j++){
	    jg=CellTable[i].mesh.jgcoef[j];
 	    for(k=0;k < n;k++){
		kk=j*n+k;
	        if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==1){
                  CellTable[i].pa[k].v+=-charge_.q[jg-1]*CellTable[i].coefqselfdirect[kk];
	        }
	        if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==1){
                  CellTable[i].pa[k].v+=charge_.q[jg-1]*CellTable[i].coefuselfdirect[kk];
	        }
	        if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==2){
                  CellTable[i].pa[k].v+=-charge_.q[jg-1]*CellTable[i].coefuselfdirect[kk];
	        }
	        if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==2){
                  CellTable[i].pa[k].v+=charge_.q[jg-1]*CellTable[i].coefqselfdirect[kk];
	        }
	        if(charge_.irightleftrigid==3){
                  CellTable[i].pa[k].v+=CellTable[i].coefqselfdirect[kk];
	        }
	    }
	  }
	}
        /*
         * calculate direct interactions with other cells which
         * are listed in the cell's direct double interaction list
         * these are cells which reside on the same processor
	 *
	 * call to bem analysis, storage in coeffcients for reuse
         */
        interact = CellTable[i].iddirect;
        for(j=0;j < CellTable[i].iddt;j++){
	    interj=interact[j];
	    nj=CellTable[interj].n;
	    nbeltsj=CellTable[interj].mesh.nbelts;

	    /*
	     * contributions of elements from cell j to nodes of cell interj
	     */
	    if(nj > 0 && nbelts > 0){
	      inodesj=CellTable[interj].mesh.inodes;
	      if(charge_.ibemanalysis){
	        bemk2_(CellTable[i].coefqddirect1[j],CellTable[i].coefuddirect1[j],
		       &nbelts,ielts,&nj,inodesj,&(CellTable[i].mesh.nnie));
	      }
	      for(jj=0;jj<CellTable[i].mesh.nnie;jj++){
	         jg=CellTable[i].mesh.jgcoef[jj];
	         for(k=0;k < nj;k++){
		    kk=jj*nj+k;
	            if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==1){
                      CellTable[interj].pa[k].v+=-charge_.q[jg-1]*CellTable[i].coefqddirect1[j][kk];
	            }
	            if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==1){
                      CellTable[interj].pa[k].v+=charge_.q[jg-1]*CellTable[i].coefuddirect1[j][kk];
	            }
	            if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==2){
                       CellTable[interj].pa[k].v+=-charge_.q[jg-1]*CellTable[i].coefuddirect1[j][kk];
	            }
	            if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==2){
                      CellTable[interj].pa[k].v+=charge_.q[jg-1]*CellTable[i].coefqddirect1[j][kk];
	            }
	            if(charge_.irightleftrigid==3){
                      CellTable[interj].pa[k].v+=CellTable[i].coefqddirect1[j][kk];
	            }
	         }
	      }
	    }

	    /*
	     * contributions of elements from cell interj to nodes of cell j
	     */
	    if(nbeltsj > 0 && n > 0){
	      ieltsj=CellTable[interj].mesh.ielts;
              if(charge_.ibemanalysis){
	        bemk2_(CellTable[i].coefqddirect2[j],CellTable[i].coefuddirect2[j],
		       &nbeltsj,ieltsj,&n,inodes,&(CellTable[interj].mesh.nnie));
	      }
	      for(jj=0;jj<CellTable[interj].mesh.nnie;jj++){
	         jg=CellTable[interj].mesh.jgcoef[jj];
	         for(k=0;k < n;k++){
		    kk=jj*n+k;
	            if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==1){
                      CellTable[i].pa[k].v+=-charge_.q[jg-1]*CellTable[i].coefqddirect2[j][kk];
	            }
	            if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==1){
                      CellTable[i].pa[k].v+=charge_.q[jg-1]*CellTable[i].coefuddirect2[j][kk];
	            }
	            if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==2){
                       CellTable[i].pa[k].v+=-charge_.q[jg-1]*CellTable[i].coefuddirect2[j][kk];
	            }
	            if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==2){
                      CellTable[i].pa[k].v+=charge_.q[jg-1]*CellTable[i].coefqddirect2[j][kk];
	            }
	            if(charge_.irightleftrigid==3){
                      CellTable[i].pa[k].v+=CellTable[i].coefqddirect2[j][kk];
	            }
	         }
	      }
	    }
        }

        /*
         * calculate direct interactions with other cells which
         * are listed in the cell's direct single interaction list
         * these are cells which reside on a different processor
	 *
	 * call to bem analysis, storage in coeffcients for reuse
         */
        interact = CellTable[i].isdirect;
        for(j=0;j < CellTable[i].isdt;j++){
            interj=interact[j];
	    nj=CellTable[interj].n;

	    /*
	     * contributions of elements from cell j to nodes of cell interj
	     */
	    if(nj > 0 && nbelts > 0){
	      if(charge_.ibemanalysis){
	        bemk2_(CellTable[i].coefqsdirect[j],CellTable[i].coefusdirect[j],
		       &nbelts,ielts,&nj,CellTable[interj].mesh.inodes,&(CellTable[i].mesh.nnie));
	      }
	      for(jj=0;jj<CellTable[i].mesh.nnie;jj++){
	         jg=CellTable[i].mesh.jgcoef[jj];
	         for(k=0;k < nj;k++){
		    kk=jj*nj+k;
	            if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==1){
                      CellTable[interj].pa[k].v+=-charge_.q[jg-1]*CellTable[i].coefqsdirect[j][kk];
	            }
	            if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==1){
                      CellTable[interj].pa[k].v+=charge_.q[jg-1]*CellTable[i].coefusdirect[j][kk];
	            }
	            if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==2){
                       CellTable[interj].pa[k].v+=-charge_.q[jg-1]*CellTable[i].coefusdirect[j][kk];
	            }
	            if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==2){
                      CellTable[interj].pa[k].v+=charge_.q[jg-1]*CellTable[i].coefqsdirect[j][kk];
	            }
	            if(charge_.irightleftrigid==3){
                      CellTable[interj].pa[k].v+=CellTable[i].coefqsdirect[j][kk];
	            }
	         }
	      }
	    }
        }

        PthreadStopTiming(TIME_CFD);
        PthreadStartTiming(TIME_CFM);

        /*
         * calculate local expansion using multipoles of cells
         * listed in the multipole interaction list
         */

        if(FFT){
            scratch = &FLocal_accum[0][0][0].x;
            for (fftb=0; fftb < 2*(Mp+1)*fftarray_m; fftb++) {
                *scratch++ = 0.0;
                *scratch++ = 0.0;
            } /* for fftb */
        } /* if FFT */

        interact = CellTable[i].imultipole;
#ifndef SERIAL

        for(j=0;j < CellTable[i].ime;j++){
            prefetch_pointer = (char *) CellTable[interact[j]].m;
            for(k=0;k < prefetch_count;k++){
/*                _pcsp(prefetch_pointer,"ro","bl"); */
                prefetch_pointer += 128;
            }
        }

#endif
        for(j=0;j < CellTable[i].ime;j++){
            if(CellTable[interact[j]].vl){
                if(FFT)
                    loc_expF(i, interact[j], FLocal_accum);
                else
                    MLocal(i,interact[j]);
            }
        }

        if(FFT)
            IFFTLocal(i, FLocal_accum);

        /*
         * calculate force and potentials due to local expansion
         */
        MForce(i);

        PthreadStopTiming(TIME_CFM);
    }

    PthreadStopTiming(TIME_CF);

}

void DirectWithinCell(int Cell){
    int i,j;
    double ir,dx,dy,dz;
    double ircubed;

    ParticlePtr cell_particles;

    cell_particles = CellTable[Cell].pa;
    for(i=0;i < (CellTable[Cell].n-1);i++){
        for(j=i+1;j < CellTable[Cell].n;j++){
            dx = cell_particles[j].p.x - cell_particles[i].p.x;
            dy = cell_particles[j].p.y - cell_particles[i].p.y;
            dz = cell_particles[j].p.z - cell_particles[i].p.z;
            ir = 1/sqrt(dx*dx + dy*dy + dz*dz);
            cell_particles[j].v += cell_particles[i].q * ir;
            cell_particles[i].v += cell_particles[j].q * ir;
            ircubed = cell_particles[i].q * cell_particles[j].q *ir*ir*ir;
            cell_particles[j].f.x += ircubed * dx;
            cell_particles[i].f.x -= ircubed * dx;
            cell_particles[j].f.y += ircubed * dy;
            cell_particles[i].f.y -= ircubed * dy;
            cell_particles[j].f.z += ircubed * dz;
            cell_particles[i].f.z -= ircubed * dz;
        }
    }

}

void DirectDoubleCell(int Cell1, int Cell2){
    int i,j;
    double ir,dx,dy,dz;
    double ircubed;
    ParticlePtr c_p1,c_p2;

    c_p1 = CellTable[Cell1].pa;
    c_p2 = CellTable[Cell2].pa;
    for(i=0;i < CellTable[Cell1].n;i++){
        for(j=0;j < CellTable[Cell2].n;j++){
            dx = c_p2[j].p.x - c_p1[i].p.x;
            dy = c_p2[j].p.y - c_p1[i].p.y;
            dz = c_p2[j].p.z - c_p1[i].p.z;
            ir = 1/sqrt(dx*dx + dy*dy + dz*dz);
            c_p2[j].v += c_p1[i].q * ir;
            c_p1[i].v += c_p2[j].q * ir;
            ircubed = c_p1[i].q * c_p2[j].q *ir*ir*ir;
            c_p2[j].f.x += ircubed * dx;
            c_p1[i].f.x -= ircubed * dx;
            c_p2[j].f.y += ircubed * dy;
            c_p1[i].f.y -= ircubed * dy;
            c_p2[j].f.z += ircubed * dz;
            c_p1[i].f.z -= ircubed * dz;
        }
    }

}

void DirectSingleCell(int LocalCell, int DistantCell){
    int i,j;
    double ir,dx,dy,dz;
    double ircubed;
    ParticlePtr c_p1;
#ifndef SERIAL
    SmallParticlePtr c_p2;
#else
    ParticlePtr c_p2;
#endif

    c_p1 = CellTable[LocalCell].pa;
#ifndef SERIAL

    c_p2 = CellTable[DistantCell].spa;
    for(j=0;j < CellTable[DistantCell].n;j += 4){
/*        _pcsp(&c_p2[j].p.x,"ro","bl"); */
    }

#else
    c_p2 = CellTable[DistantCell].pa;
#endif
    for(i=0;i < CellTable[LocalCell].n;i++){
        for(j=0;j < CellTable[DistantCell].n;j++){
            dx = c_p2[j].p.x - c_p1[i].p.x;
            dy = c_p2[j].p.y - c_p1[i].p.y;
            dz = c_p2[j].p.z - c_p1[i].p.z;
            ir = 1/sqrt(dx*dx + dy*dy + dz*dz);
            c_p1[i].v += c_p2[j].q * ir;
            ircubed = c_p1[i].q * c_p2[j].q *ir*ir*ir;
            c_p1[i].f.x -= ircubed * dx;
            c_p1[i].f.y -= ircubed * dy;
            c_p1[i].f.z -= ircubed * dz;
        }
    }

}

void DirectForces(void){
    int i,j;
    double ir,dx,dy,dz;
    double ircubed;
#ifndef SERIAL
    int error;
#endif
    
    PthreadStartTiming(TIME_DF);

    if(Verbose && PthreadNum == 0)
        fprintf(Output,"Calculating forces and potentials directly...\n");

#ifndef SERIAL
    /*
     * initialize SmallParticleTable
     */
    for(i=PthreadPTStart;i < PthreadPTEnd;i++){
        SmallParticleTable[i].p = ParticleTable[i].p;
        SmallParticleTable[i].q = ParticleTable[i].q;
    }   

    error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkout failure",NULL,0);

    error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkin failure",NULL,0);

    for(j=0;j < PthreadPTStart;j += 4){
/*        _pcsp(&SmallParticleTable[j].p.x,"ro","bl"); */
    }
    for(j=PthreadPTEnd;j < NumParticles;j += 4){
/*        _pcsp(&SmallParticleTable[j].p.x,"ro","bl"); */
    }

#endif
    for(i=PthreadPTStart;i < PthreadPTEnd;i++){
        ParticleTable[i].fdirect.x = ParticleTable[i].fdirect.y = 0;
        ParticleTable[i].fdirect.z = ParticleTable[i].vdirect = 0;
    }
    for(i=PthreadPTStart;i < PthreadPTEnd;i++){
#ifndef SERIAL
        for(j=0;j < PthreadPTStart;j++){
            dx = SmallParticleTable[j].p.x - ParticleTable[i].p.x;
            dy = SmallParticleTable[j].p.y - ParticleTable[i].p.y;
            dz = SmallParticleTable[j].p.z - ParticleTable[i].p.z;
            ir = 1/sqrt(dx*dx + dy*dy + dz*dz);
            ParticleTable[i].vdirect += SmallParticleTable[j].q * ir;
            ircubed = ParticleTable[i].q*SmallParticleTable[j].q *ir*ir*ir;
            ParticleTable[i].fdirect.x -= ircubed * dx;
            ParticleTable[i].fdirect.y -= ircubed * dy;
            ParticleTable[i].fdirect.z -= ircubed * dz;
        }
#endif
        for(j=i+1;j < PthreadPTEnd;j++){
            dx = ParticleTable[j].p.x - ParticleTable[i].p.x;
            dy = ParticleTable[j].p.y - ParticleTable[i].p.y;
            dz = ParticleTable[j].p.z - ParticleTable[i].p.z;
            ir = 1/sqrt(dx*dx + dy*dy + dz*dz);
            ParticleTable[j].vdirect += ParticleTable[i].q * ir;
            ParticleTable[i].vdirect += ParticleTable[j].q * ir;
            ircubed = ParticleTable[i].q*ParticleTable[j].q *ir*ir*ir;
            ParticleTable[j].fdirect.x += ircubed * dx;
            ParticleTable[i].fdirect.x -= ircubed * dx;
            ParticleTable[j].fdirect.y += ircubed * dy;
            ParticleTable[i].fdirect.y -= ircubed * dy;
            ParticleTable[j].fdirect.z += ircubed * dz;
            ParticleTable[i].fdirect.z -= ircubed * dz;
        }
#ifndef SERIAL
        for(j=PthreadPTEnd;j < NumParticles;j++){
            dx = SmallParticleTable[j].p.x - ParticleTable[i].p.x;
            dy = SmallParticleTable[j].p.y - ParticleTable[i].p.y;
            dz = SmallParticleTable[j].p.z - ParticleTable[i].p.z;
            ir = 1/sqrt(dx*dx + dy*dy + dz*dz);
            ParticleTable[i].vdirect += SmallParticleTable[j].q * ir;
            ircubed = ParticleTable[i].q*SmallParticleTable[j].q *ir*ir*ir;
            ParticleTable[i].fdirect.x -= ircubed * dx;
            ParticleTable[i].fdirect.y -= ircubed * dy;
            ParticleTable[i].fdirect.z -= ircubed * dz;
        }
#endif
    }

    PthreadStopTiming(TIME_DF);

}
