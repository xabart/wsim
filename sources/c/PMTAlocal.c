/*
 * PMTAlocal.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAlocal.c,v 1.1 1994/10/08 04:34:10 wrankin Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAlocal.c,v $
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 *
 */


#include "include/PMTA.h"
#include "PMTAlegendre.h"

/*
 * MLocal converts a multipole expansion in a distant cell to a local
 * expansion for the particles of interest
 * The new local expansion describes the effects of the particles in the
 * distant cell on the particles in the local cell
 *
 * the calculated local expansion is added to the existing local expansion
 *
 * Adapted from the PFMA code written by Jim Leathrum
 */
void MLocal(int LocalCell, int DistantCell){
    register int j,k,n,m,v,vc;
    Mtype O,ML;
    Vector x0;
    SphVector center;
    double r,temp,tempc;
    double Ox,Oy,tx,ty,s3x,s3y,s3xc,s3yc;
    double *leg;

#if (IBOT==10)
extern struct {
   double zl0;
   double w0;
   double h0;
   double x0;
   double d0;
   int mx;
   int my;
   int mz;
   int nx;
   int ny;
   int nz;
   int nsubc;
   int nintr;
   int nom;
   int mom;
} datgen_;
#endif

    /*
     * calculate the center of the distant cell in spherical
     * coordinates relative to the center of the local cell
     * centered at the origin
     */
    x0.x = CellTable[DistantCell].p.x - CellTable[LocalCell].p.x;
    x0.y = CellTable[DistantCell].p.y - CellTable[LocalCell].p.y;
    x0.z = CellTable[DistantCell].p.z - CellTable[LocalCell].p.z;
    center.r = sqrt(x0.x*x0.x + x0.y*x0.y + x0.z*x0.z);
    if(center.r < fabs(x0.z)) center.r = fabs(x0.z);
    center.a = acos(x0.z/center.r);
    if((x0.x == 0.0) && (x0.y == 0.0)) {
        center.b = 0.0;
    } else {
        center.b = atan2(x0.y,x0.x);
    }
    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin((double) m * center.b);
    }
    /*
     * compute the Legendre Polynomial
     */
    temp = cos(center.a);
    Legendre(MLegendre,temp,Mp);
    r = temp = 1.0/center.r;
    /*
     * include a factor 1/(r^n) in the polynomial
     */
    for(n=0;n <= Mp;n++){
        leg = MLegendre[n];
        for(m=0;m <= n;m++){
            leg[m] *= r;
        }
        r *= temp;
    }
    /*
     * compute the local expansion
     */
    O = CellTable[DistantCell].m;
    ML = CellTable[LocalCell].l;
    for(n=0;n <= Mp;n++){
        /* m == 0 and k == 0 */
        Ox = O[n][0].x;
        for(j=0;j <= (Mp-n);j++){
            ML[j][0].x += Ms3constant1[j][0][n][0] * MLegendre[j+n][0] * Ox;
        }
        /* m == 0 and k > 0 */
        for(k=1;k <= (Mp-n);k++){
            tx = Mt1[k].x;
            ty = -Mt1[k].y;
            s3x = Ox * tx;
            s3y = Ox * ty;
            for(j=k;j <= (Mp-n);j++){
                temp = Ms3constant1[j][k][n][0] * MLegendre[j+n][k];
                ML[j][k].x += temp * s3x;
                ML[j][k].y += temp * s3y;
            }
        }
        /* m > 0 */
        for(m=1;m <= n;m++){
            /* k == 0 */
            Ox = O[n][m].x;
            Oy = O[n][m].y;
            tx = Mt1[m].x;
            ty = Mt1[m].y;
            s3x = Ox * tx - Oy * ty;
            s3y = Ox * ty + Oy * tx;
            for(j=0;j <= (Mp-n);j++){
                temp = Ms3constant1[j][0][n][m] * MLegendre[j+n][m];
                tempc = Ms3constant1[j][0][n][-m] * MLegendre[j+n][m];
                ML[j][0].x += (temp + tempc) * s3x;
                ML[j][0].y += (temp - tempc) * s3y;
            }
            /* k > 0 */
            for(k=1;k <= (Mp-n);k++){
                v = m - k;
                vc = m + k;  /* actually -m - k */
                if(v >= 0){
                    tx = Mt1[v].x;
                    ty = Mt1[v].y;
                } else {
                    v = -v;
                    tx = Mt1[v].x;
                    ty = -Mt1[v].y;
                }
                s3x = Ox * tx - Oy * ty;
                s3y = Ox * ty + Oy * tx;
                tx = Mt1[vc].x;
                ty = -Mt1[vc].y;
                s3xc = Ox * tx + Oy * ty;
                s3yc = Ox * ty - Oy * tx;
                for(j=k;j <= (Mp-n);j++){
                    temp = Ms3constant1[j][k][n][m] * MLegendre[j+n][v];
                    tempc = Ms3constant1[j][k][n][-m] * MLegendre[j+n][vc];
                    ML[j][k].x += temp * s3x + tempc * s3xc;
                    ML[j][k].y += temp * s3y + tempc * s3yc;
                }
            }
        }
    }
#if (ISYM==1 && IBOT==10)

// symmetry in y

    /*
     * calculate the center of the distant cell in spherical
     * coordinates relative to the center of the local cell
     * centered at the origin
     */
    x0.x = CellTable[DistantCell].p.x - CellTable[LocalCell].p.x;
    x0.z = CellTable[DistantCell].p.z - CellTable[LocalCell].p.z;

    x0.y = - CellTable[DistantCell].p.y - CellTable[LocalCell].p.y;

    center.r = sqrt(x0.x*x0.x + x0.y*x0.y + x0.z*x0.z);
    if(center.r < fabs(x0.z)) center.r = fabs(x0.z);
    center.a = acos(x0.z/center.r);
    if((x0.x == 0.0) && (x0.y == 0.0)) {
        center.b = 0.0;
    } else {
        center.b = atan2(x0.y,x0.x);
    }
    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin((double) m * center.b);
    }
    /*
     * compute the Legendre Polynomial
     */
    temp = cos(center.a);
    Legendre(MLegendre,temp,Mp);
    r = temp = 1.0/center.r;
    /*
     * include a factor 1/(r^n) in the polynomial
     */
    for(n=0;n <= Mp;n++){
        leg = MLegendre[n];
        for(m=0;m <= n;m++){
            leg[m] *= r;
        }
        r *= temp;
    }
    /*
     * add the symmetrical contribution
     */
    O = CellTable[DistantCell].msym;
    ML = CellTable[LocalCell].l;
    for(n=0;n <= Mp;n++){
        /* m == 0 and k == 0 */
        Ox = O[n][0].x;
        for(j=0;j <= (Mp-n);j++){
            ML[j][0].x += Ms3constant1[j][0][n][0] * MLegendre[j+n][0] * Ox;
        }
        /* m == 0 and k > 0 */
        for(k=1;k <= (Mp-n);k++){
            tx = Mt1[k].x;
            ty = -Mt1[k].y;
            s3x = Ox * tx;
            s3y = Ox * ty;
            for(j=k;j <= (Mp-n);j++){
                temp = Ms3constant1[j][k][n][0] * MLegendre[j+n][k];
                ML[j][k].x += temp * s3x;
                ML[j][k].y += temp * s3y;
            }
        }
        /* m > 0 */
        for(m=1;m <= n;m++){
            /* k == 0 */
            Ox = O[n][m].x;
            Oy = O[n][m].y;
            tx = Mt1[m].x;
            ty = Mt1[m].y;
            s3x = Ox * tx - Oy * ty;
            s3y = Ox * ty + Oy * tx;
            for(j=0;j <= (Mp-n);j++){
                temp = Ms3constant1[j][0][n][m] * MLegendre[j+n][m];
                tempc = Ms3constant1[j][0][n][-m] * MLegendre[j+n][m];
                ML[j][0].x += (temp + tempc) * s3x;
                ML[j][0].y += (temp - tempc) * s3y;
            }
            /* k > 0 */
            for(k=1;k <= (Mp-n);k++){
                v = m - k;
                vc = m + k;  /* actually -m - k */
                if(v >= 0){
                    tx = Mt1[v].x;
                    ty = Mt1[v].y;
                } else {
                    v = -v;
                    tx = Mt1[v].x;
                    ty = -Mt1[v].y;
                }
                s3x = Ox * tx - Oy * ty;
                s3y = Ox * ty + Oy * tx;
                tx = Mt1[vc].x;
                ty = -Mt1[vc].y;
                s3xc = Ox * tx + Oy * ty;
                s3yc = Ox * ty - Oy * tx;
                for(j=k;j <= (Mp-n);j++){
                    temp = Ms3constant1[j][k][n][m] * MLegendre[j+n][v];
                    tempc = Ms3constant1[j][k][n][-m] * MLegendre[j+n][vc];
                    ML[j][k].x += temp * s3x + tempc * s3xc;
                    ML[j][k].y += temp * s3y + tempc * s3yc;
                }
            }
        }
    }
// symmetry flat bottom

    /*
     * calculate the center of the distant cell in spherical
     * coordinates relative to the center of the local cell
     * centered at the origin
     */
    x0.x = CellTable[DistantCell].p.x - CellTable[LocalCell].p.x;
    x0.y = CellTable[DistantCell].p.y - CellTable[LocalCell].p.y;

    x0.z = - CellTable[DistantCell].p.z -2*datgen_.h0 - CellTable[LocalCell].p.z;

    center.r = sqrt(x0.x*x0.x + x0.y*x0.y + x0.z*x0.z);
    if(center.r < fabs(x0.z)) center.r = fabs(x0.z);
    center.a = acos(x0.z/center.r);
    if((x0.x == 0.0) && (x0.y == 0.0)) {
        center.b = 0.0;
    } else {
        center.b = atan2(x0.y,x0.x);
    }
    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin((double) m * center.b);
    }
    /*
     * compute the Legendre Polynomial
     */
    temp = cos(center.a);
    Legendre(MLegendre,temp,Mp);
    r = temp = 1.0/center.r;
    /*
     * include a factor 1/(r^n) in the polynomial
     */
    for(n=0;n <= Mp;n++){
        leg = MLegendre[n];
        for(m=0;m <= n;m++){
            leg[m] *= r;
        }
        r *= temp;
    }
    /*
     * add the symmetrical contribution
     */
    O = CellTable[DistantCell].msym2;
    ML = CellTable[LocalCell].l;
    for(n=0;n <= Mp;n++){
        /* m == 0 and k == 0 */
        Ox = O[n][0].x;
        for(j=0;j <= (Mp-n);j++){
            ML[j][0].x += Ms3constant1[j][0][n][0] * MLegendre[j+n][0] * Ox;
        }
        /* m == 0 and k > 0 */
        for(k=1;k <= (Mp-n);k++){
            tx = Mt1[k].x;
            ty = -Mt1[k].y;
            s3x = Ox * tx;
            s3y = Ox * ty;
            for(j=k;j <= (Mp-n);j++){
                temp = Ms3constant1[j][k][n][0] * MLegendre[j+n][k];
                ML[j][k].x += temp * s3x;
                ML[j][k].y += temp * s3y;
            }
        }
        /* m > 0 */
        for(m=1;m <= n;m++){
            /* k == 0 */
            Ox = O[n][m].x;
            Oy = O[n][m].y;
            tx = Mt1[m].x;
            ty = Mt1[m].y;
            s3x = Ox * tx - Oy * ty;
            s3y = Ox * ty + Oy * tx;
            for(j=0;j <= (Mp-n);j++){
                temp = Ms3constant1[j][0][n][m] * MLegendre[j+n][m];
                tempc = Ms3constant1[j][0][n][-m] * MLegendre[j+n][m];
                ML[j][0].x += (temp + tempc) * s3x;
                ML[j][0].y += (temp - tempc) * s3y;
            }
            /* k > 0 */
            for(k=1;k <= (Mp-n);k++){
                v = m - k;
                vc = m + k;  /* actually -m - k */
                if(v >= 0){
                    tx = Mt1[v].x;
                    ty = Mt1[v].y;
                } else {
                    v = -v;
                    tx = Mt1[v].x;
                    ty = -Mt1[v].y;
                }
                s3x = Ox * tx - Oy * ty;
                s3y = Ox * ty + Oy * tx;
                tx = Mt1[vc].x;
                ty = -Mt1[vc].y;
                s3xc = Ox * tx + Oy * ty;
                s3yc = Ox * ty - Oy * tx;
                for(j=k;j <= (Mp-n);j++){
                    temp = Ms3constant1[j][k][n][m] * MLegendre[j+n][v];
                    tempc = Ms3constant1[j][k][n][-m] * MLegendre[j+n][vc];
                    ML[j][k].x += temp * s3x + tempc * s3xc;
                    ML[j][k].y += temp * s3y + tempc * s3yc;
                }
            }
        }
    }
// symmetry in y + flat bottom

    /*
     * calculate the center of the distant cell in spherical
     * coordinates relative to the center of the local cell
     * centered at the origin
     */
    x0.x = CellTable[DistantCell].p.x - CellTable[LocalCell].p.x;

    x0.y = - CellTable[DistantCell].p.y - CellTable[LocalCell].p.y;
    x0.z = - CellTable[DistantCell].p.z -2*datgen_.h0 - CellTable[LocalCell].p.z;

    center.r = sqrt(x0.x*x0.x + x0.y*x0.y + x0.z*x0.z);
    if(center.r < fabs(x0.z)) center.r = fabs(x0.z);
    center.a = acos(x0.z/center.r);
    if((x0.x == 0.0) && (x0.y == 0.0)) {
        center.b = 0.0;
    } else {
        center.b = atan2(x0.y,x0.x);
    }
    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin((double) m * center.b);
    }
    /*
     * compute the Legendre Polynomial
     */
    temp = cos(center.a);
    Legendre(MLegendre,temp,Mp);
    r = temp = 1.0/center.r;
    /*
     * include a factor 1/(r^n) in the polynomial
     */
    for(n=0;n <= Mp;n++){
        leg = MLegendre[n];
        for(m=0;m <= n;m++){
            leg[m] *= r;
        }
        r *= temp;
    }
    /*
     * add the symmetrical contribution
     */
    O = CellTable[DistantCell].msym3;
    ML = CellTable[LocalCell].l;
    for(n=0;n <= Mp;n++){
        /* m == 0 and k == 0 */
        Ox = O[n][0].x;
        for(j=0;j <= (Mp-n);j++){
            ML[j][0].x += Ms3constant1[j][0][n][0] * MLegendre[j+n][0] * Ox;
        }
        /* m == 0 and k > 0 */
        for(k=1;k <= (Mp-n);k++){
            tx = Mt1[k].x;
            ty = -Mt1[k].y;
            s3x = Ox * tx;
            s3y = Ox * ty;
            for(j=k;j <= (Mp-n);j++){
                temp = Ms3constant1[j][k][n][0] * MLegendre[j+n][k];
                ML[j][k].x += temp * s3x;
                ML[j][k].y += temp * s3y;
            }
        }
        /* m > 0 */
        for(m=1;m <= n;m++){
            /* k == 0 */
            Ox = O[n][m].x;
            Oy = O[n][m].y;
            tx = Mt1[m].x;
            ty = Mt1[m].y;
            s3x = Ox * tx - Oy * ty;
            s3y = Ox * ty + Oy * tx;
            for(j=0;j <= (Mp-n);j++){
                temp = Ms3constant1[j][0][n][m] * MLegendre[j+n][m];
                tempc = Ms3constant1[j][0][n][-m] * MLegendre[j+n][m];
                ML[j][0].x += (temp + tempc) * s3x;
                ML[j][0].y += (temp - tempc) * s3y;
            }
            /* k > 0 */
            for(k=1;k <= (Mp-n);k++){
                v = m - k;
                vc = m + k;  /* actually -m - k */
                if(v >= 0){
                    tx = Mt1[v].x;
                    ty = Mt1[v].y;
                } else {
                    v = -v;
                    tx = Mt1[v].x;
                    ty = -Mt1[v].y;
                }
                s3x = Ox * tx - Oy * ty;
                s3y = Ox * ty + Oy * tx;
                tx = Mt1[vc].x;
                ty = -Mt1[vc].y;
                s3xc = Ox * tx + Oy * ty;
                s3yc = Ox * ty - Oy * tx;
                for(j=k;j <= (Mp-n);j++){
                    temp = Ms3constant1[j][k][n][m] * MLegendre[j+n][v];
                    tempc = Ms3constant1[j][k][n][-m] * MLegendre[j+n][vc];
                    ML[j][k].x += temp * s3x + tempc * s3xc;
                    ML[j][k].y += temp * s3y + tempc * s3yc;
                }
            }
        }
    }
#else

#if (ISYM==1 || IBOT==10)
#if (ISYM==1)
    /*
     * calculate the center of the distant cell in spherical
     * coordinates relative to the center of the local cell
     * centered at the origin
     */
    x0.x = CellTable[DistantCell].p.x - CellTable[LocalCell].p.x;
    x0.z = CellTable[DistantCell].p.z - CellTable[LocalCell].p.z;

    x0.y = - CellTable[DistantCell].p.y - CellTable[LocalCell].p.y;
#endif

#if (IBOT==10)
    /*
     * calculate the center of the distant cell in spherical
     * coordinates relative to the center of the local cell
     * centered at the origin
     */
    x0.x = CellTable[DistantCell].p.x - CellTable[LocalCell].p.x;
    x0.y = CellTable[DistantCell].p.y - CellTable[LocalCell].p.y;

    x0.z = - CellTable[DistantCell].p.z -2*datgen_.h0 - CellTable[LocalCell].p.z;
#endif

    center.r = sqrt(x0.x*x0.x + x0.y*x0.y + x0.z*x0.z);
    if(center.r < fabs(x0.z)) center.r = fabs(x0.z);
    center.a = acos(x0.z/center.r);
    if((x0.x == 0.0) && (x0.y == 0.0)) {
        center.b = 0.0;
    } else {
        center.b = atan2(x0.y,x0.x);
    }
    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin((double) m * center.b);
    }
    /*
     * compute the Legendre Polynomial
     */
    temp = cos(center.a);
    Legendre(MLegendre,temp,Mp);
    r = temp = 1.0/center.r;
    /*
     * include a factor 1/(r^n) in the polynomial
     */
    for(n=0;n <= Mp;n++){
        leg = MLegendre[n];
        for(m=0;m <= n;m++){
            leg[m] *= r;
        }
        r *= temp;
    }
    /*
     * add the symmetrical contribution
     */
    O = CellTable[DistantCell].msym;
    ML = CellTable[LocalCell].l;
    for(n=0;n <= Mp;n++){
        /* m == 0 and k == 0 */
        Ox = O[n][0].x;
        for(j=0;j <= (Mp-n);j++){
            ML[j][0].x += Ms3constant1[j][0][n][0] * MLegendre[j+n][0] * Ox;
        }
        /* m == 0 and k > 0 */
        for(k=1;k <= (Mp-n);k++){
            tx = Mt1[k].x;
            ty = -Mt1[k].y;
            s3x = Ox * tx;
            s3y = Ox * ty;
            for(j=k;j <= (Mp-n);j++){
                temp = Ms3constant1[j][k][n][0] * MLegendre[j+n][k];
                ML[j][k].x += temp * s3x;
                ML[j][k].y += temp * s3y;
            }
        }
        /* m > 0 */
        for(m=1;m <= n;m++){
            /* k == 0 */
            Ox = O[n][m].x;
            Oy = O[n][m].y;
            tx = Mt1[m].x;
            ty = Mt1[m].y;
            s3x = Ox * tx - Oy * ty;
            s3y = Ox * ty + Oy * tx;
            for(j=0;j <= (Mp-n);j++){
                temp = Ms3constant1[j][0][n][m] * MLegendre[j+n][m];
                tempc = Ms3constant1[j][0][n][-m] * MLegendre[j+n][m];
                ML[j][0].x += (temp + tempc) * s3x;
                ML[j][0].y += (temp - tempc) * s3y;
            }
            /* k > 0 */
            for(k=1;k <= (Mp-n);k++){
                v = m - k;
                vc = m + k;  /* actually -m - k */
                if(v >= 0){
                    tx = Mt1[v].x;
                    ty = Mt1[v].y;
                } else {
                    v = -v;
                    tx = Mt1[v].x;
                    ty = -Mt1[v].y;
                }
                s3x = Ox * tx - Oy * ty;
                s3y = Ox * ty + Oy * tx;
                tx = Mt1[vc].x;
                ty = -Mt1[vc].y;
                s3xc = Ox * tx + Oy * ty;
                s3yc = Ox * ty - Oy * tx;
                for(j=k;j <= (Mp-n);j++){
                    temp = Ms3constant1[j][k][n][m] * MLegendre[j+n][v];
                    tempc = Ms3constant1[j][k][n][-m] * MLegendre[j+n][vc];
                    ML[j][k].x += temp * s3x + tempc * s3xc;
                    ML[j][k].y += temp * s3y + tempc * s3yc;
                }
            }
        }
    }
#endif

#endif
}

/*
 * MShiftLocal translates a parent's local expansion (O) to a local
 * expansion (L) for a child cell
 * the local expansion for the child cell includes the effect of all
 * particles distant to the parent cell
 * the translated parent local expansion is added to the existing child
 * local expansion
 *
 * Adapted from the PFMA code written by Jim Leathrum
 */
void MShiftLocal(int ChildCell, int ParentCell){
    int j,k,n,m,np,mp;
    double r,temp,Ox,Oy,s3x,s3y;
    double **s3_c;
    Complex *l;
    Mtype O,L;
    Vector x0;
    SphVector center;

    /*
     * calculate the center of the parent cell in spherical
     * coordinates relative to the center of the child cell
     * centered at the origin
     */
    x0.x = CellTable[ParentCell].p.x - CellTable[ChildCell].p.x;
    x0.y = CellTable[ParentCell].p.y - CellTable[ChildCell].p.y;
    x0.z = CellTable[ParentCell].p.z - CellTable[ChildCell].p.z;
    center.r = sqrt(x0.x*x0.x + x0.y*x0.y + x0.z*x0.z);
    if(center.r < fabs(x0.z)) center.r = fabs(x0.z);
    center.a = acos(x0.z/center.r);
    if((x0.x == 0.0) && (x0.y == 0.0)) {
        center.b = 0.0;
    } else {
        center.b = atan2(x0.y,x0.x);
    }

    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin((double) m * center.b);
    }
    /*
     * compute the Legendre Polynomial
     */
    temp = cos(center.a);
    Legendre(MLegendre,temp,Mp);
    /*
     * precomputed terms
     */
    r = 1.0;
    for(n=0;n <= Mp;n++){
        for(m=0;m <= n;m++){
            temp = r * MLegendre[n][m];
            Ms[n][m].x = temp * Mt1[m].x;
            Ms[n][m].y = temp * Mt1[m].y;
        }
        r *= center.r;
    }
    /*
     * shift the local expansion
     */
    O = CellTable[ParentCell].l;
    L = CellTable[ChildCell].l;
    for(j=0;j <= Mp;j++){
        for(k=0;k <= j;k++){
            l = &L[j][k];
            s3_c = Ms3constant2[j][k];
            for(n=0;n <= Mp - j;n++){
                np = n + j;
                if(np <= Mp){
                    /* m = 0 */
                    temp = s3_c[n][0];
                    Ox = O[np][k].x;
                    Oy = O[np][k].y;
                    s3x = Ms[n][0].x;
                    (*l).x += temp * Ox * s3x;
                    (*l).y += temp * Oy * s3x;
                    /* m > 0 */
                    for(m=1;m <= n;m++){
                        mp = m + k;
                        temp = s3_c[n][m];
                        Ox = O[np][mp].x;
                        Oy = O[np][mp].y;
                        s3x = Ms[n][m].x;
                        s3y = Ms[n][m].y;
                        (*l).x += temp * (Ox * s3x - Oy * s3y);
                        (*l).y += temp * (Ox * s3y + Oy * s3x);
                        mp = k - m;
                        temp = s3_c[n][-m];
                        if(mp >= 0){
                            Ox = O[np][mp].x;
                            Oy = O[np][mp].y;
                        } else {
                            mp = -mp;
                            Ox = O[np][mp].x;
                            Oy = -O[np][mp].y;
                        }
                        s3x = Ms[n][m].x;
                        s3y = -Ms[n][m].y;
                        (*l).x += temp * (Ox * s3x - Oy * s3y);
                        (*l).y += temp * (Ox * s3y + Oy * s3x);
                    }
                }
            }
        }
    }

}

/*
 * MForce computes the force and potential caused by the particles
 * contained in a distant cell
 * It uses a single local expansion to represent the effects of
 * the distant particles
 * The expansion is evaluated directly for the potential and the gradient
 * of the expansion is computed for the force
 *
 * Adapted from the PFMA code written by Jim Leathrum
 */
void MForce(int Cell){
    int i,n,m;
    Complex *c;
    Mtype ML;
    Vector center;
    SphVector z0;
    ParticlePtr cell_particles;
    double temp1,temp2,temp3,rterm,x,y,z;
    double r,a,b,sum1,sum2,sum3,costerm,sinterm,pot;
    double costheta,sintheta,cosphi,sinphi,norminv,sqrtinv;

    ML = CellTable[Cell].l;
    center = CellTable[Cell].p;
    cell_particles = CellTable[Cell].pa;
    for(i=0;i < CellTable[Cell].n;i++){
        x = cell_particles[i].p.x - center.x;
        y = cell_particles[i].p.y - center.y;
        z = cell_particles[i].p.z - center.z;
        z0.r = sqrt(x*x + y*y + z*z);
        z0.a = acos(z/z0.r);
        z0.b = atan2(y,x);
        x = cos(z0.a);
        Legendre(MLegendre,x,Mp);
        norminv = x/(1.0 - x*x);
        sqrtinv = 1.0/sqrt(1.0 - x*x);
        pot = ML[0][0].x;
        r = a = b = 0.0;
        rterm = z0.r;
        for(n=1;n <= Mp;n++){
            c = ML[n];
            sum1 = sum2 = sum3 = 0.0;
            /* m > 0 */
            for(m=1;m <= n;m++){
                temp1 = MYconstant[n][m] * MLegendre[n][m];
                temp2 = (double) m * z0.b;
                costerm = cos(temp2);
                sinterm = sin(temp2);
                temp3 = (costerm*(c[m].x) - sinterm*(c[m].y));
                sum1 += temp1 * temp3;
                if(m < n){
                    temp2 = ((double) (-m)) * norminv * MLegendre[n][m]
                        - MLegendre[n][m+1] * sqrtinv;
                    temp2 *= MYconstant[n][m];
                    sum2 += temp2 * temp3;
                } else {
                    temp2 = ((double) (-m)) * norminv * MLegendre[n][m];
                    temp2 *= MYconstant[n][m];
                    sum2 += temp2 * temp3;
                }
                sum3 += (double) m * temp1 *
                    (sinterm*(c[m].x)+costerm*(c[m].y));
            }
            /* add in m == 0 terms */
            r += ((double) n) * rterm * (2.0 * sum1 + MLegendre[n][0] * c[0].x);
            pot += rterm * (2.0 * sum1 + MLegendre[n][0] * c[0].x);
            a += rterm * (2.0 * sum2 - (MLegendre[n][1]*sqrtinv) * c[0].x);
            b += rterm * sum3;
            rterm *= z0.r;
        }
        r /= z0.r;
        a *= -sin(z0.a)/z0.r;
        b *= -2.0/(z0.r*sin(z0.a));
        costheta = cos(z0.a);
        sintheta = sin(z0.a);
        cosphi = cos(z0.b);
        sinphi = sin(z0.b);
        x = cell_particles[i].q *
            (r*sintheta*cosphi + a*costheta*cosphi - b*sinphi);
        y = cell_particles[i].q *
            (r*sintheta*sinphi + a*costheta*sinphi + b*cosphi);
        z = cell_particles[i].q * (r*costheta - a*sintheta);
        cell_particles[i].v += pot;
        cell_particles[i].f.x -= x;
        cell_particles[i].f.y -= y;
        cell_particles[i].f.z -= z;
    }

}
