/*
 * PMTAanalysis.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 * 
 * Version 4.0, April 20, 1994
 *
 *
 */

static char rcsid[] = "$Id: PMTAanalysis.c,v 1.3 1994/11/11 21:50:28 lambert Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAanalysis.c,v $
 * Revision 1.3  1994/11/11  21:50:28  lambert
 * *** empty log message ***
 *
 * Revision 1.2  1994/11/11  19:45:44  lambert
 * All necessary parameters are now passed to PrintOutliers,
 * as OutVcutoff and OutFcutoff no longer global.
 *
 * Same with ErrorAnalysis -- it gets 3 parameters instead of 0.
 *
 * Removed declared variable k that was never used.
 *
 * Around line 260, there is questionable code, cell_length is assigned
 * a value and never does anything with it.  So I put comment:
 * "why is this here BUG?" on that line.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 *
 */


#include "include/PMTA.h"

void PrintOutliers(double,double,double,double);

void InteractAnalysis(void){
    int i,j,max_cells,min_ime,min_iddt,min_isdt,max_ime,max_iddt,max_isdt;
    int tot_ime,tot_iddt,tot_isdt,temp_ime;
    int all_ime,all_iddt,all_isdt;
    int level_sum,min_downpass;
    
    fputc('\n',Output);
    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\nCell Interaction Analysis (Downward Pass)\n\n");

	fprintf(Output,"Total number of M2L and Direct interactions per distant "
		"cell level:\n");
    for(i=1;i <= NumLevels;i++){
        for(j=level_sum=0;j < NumProcessors;j++){ 
            level_sum += ILevelAnalysis[j][i];
        }
        fprintf(Output,"level %4d:  %8d\n",i,level_sum);
    }
    fputc('\n',Output);
        
    if(PFMA) 
        min_downpass = 2;
    else
        min_downpass = NumLevels;
    all_ime = all_iddt = all_isdt = 0;
    for(j=min_downpass;j < NumLevels;j++){  
        min_ime = Power8[NumLevels+1];
        max_ime = tot_ime = 0; 
        max_cells = LevelLocate[j+1];
        for(i=LevelLocate[j];i < max_cells;i++){
            temp_ime = CellTable[i].ime + 8;
            if(temp_ime > max_ime) max_ime = temp_ime;
            if(temp_ime < min_ime) min_ime = temp_ime;
            tot_ime += temp_ime;
        }
        all_ime += tot_ime;
    
        fprintf(Output,"Interaction lists per level %d cell:\n",j);
        fprintf(Output,"multipole:     mean = %-5d maximum = %-5d minimum = "
            "%-5d  (M2L and L2L)\n\n",tot_ime/Power8[j],max_ime,min_ime);
    }
    min_ime = min_iddt = min_isdt = Power8[NumLevels+1];
    max_ime = max_iddt = max_isdt = tot_ime = tot_iddt = tot_isdt = 0; 
    max_cells = LevelLocate[NumLevels+1];
    for(i=LevelLocate[NumLevels];i < max_cells;i++){
        if(CellTable[i].ime > max_ime) max_ime = CellTable[i].ime;
        if(CellTable[i].ime < min_ime) min_ime = CellTable[i].ime;
        if(CellTable[i].iddt > max_iddt) max_iddt = CellTable[i].iddt;
        if(CellTable[i].iddt < min_iddt) min_iddt = CellTable[i].iddt;
        if(CellTable[i].isdt > max_isdt) max_isdt = CellTable[i].isdt;
        if(CellTable[i].isdt < min_isdt) min_isdt = CellTable[i].isdt;
        tot_ime += CellTable[i].ime;
        tot_iddt += CellTable[i].iddt;
        tot_isdt += CellTable[i].isdt;
    }
    all_ime += tot_ime;
    all_iddt += tot_iddt;
    all_isdt += tot_isdt;

    fprintf(Output,"Interaction lists per level %d cell:\n",NumLevels);
    fprintf(Output,"double direct: mean = %-5d maximum = %-5d "
        "minimum = %-5d\n",1+tot_iddt/Power8[NumLevels],1+max_iddt,
        1+min_iddt);
    fprintf(Output,"single direct: mean = %-5d maximum = %-5d "
        "minimum = %-5d\n",tot_isdt/Power8[NumLevels],max_isdt,min_isdt);
    fprintf(Output,"multipole:     mean = %-5d maximum = %-5d minimum = %-5d"
        "  (M2L)\n\n",tot_ime/Power8[NumLevels],max_ime,min_ime);
    
	fprintf(Output,"Sum of interactions for all cells:\n");
    fprintf(Output,"double direct = %-10d\n"
        "single direct = %-10d\n"
        "multipole     = %-10d  (M2L and L2L)\n"
        "total         = %-10d\n\n",
        all_iddt+Power8[NumLevels],all_isdt,all_ime,
        all_ime+all_iddt+all_isdt+Power8[NumLevels]);
        
    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\n");

}

void CellParticleAnalysis(void){
    int i,max_cells,min,max,nonempty;

    min = NumParticles;
    max = 0;
    max_cells = LevelLocate[NumLevels+1];
    nonempty = 0;
    for(i=LevelLocate[NumLevels];i < max_cells;i++){
        if(CellTable[i].n < min) min = CellTable[i].n;
        if(CellTable[i].n > max) max = CellTable[i].n;
	if(CellTable[i].n > 0) nonempty=nonempty+1;
    }

    fputc('\n',Output);
    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\nCell Particle Analysis\n\n");

    fprintf(Output,"particles per level %d cell:\n\n",NumLevels);
    fprintf(Output,"mean = %-7d  maximum = %-7d  minimum = %-7d\n\n",
//        NumParticles/Power8[NumLevels],max,min);
        NumParticles/nonempty,max,min);

    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\n");

}

void ErrorAnalysis(int Outliers,double OutVcutoff,double OutFcutoff){
    int i;
    double v_max_error,v_total_error;
    double f_max_error,f_total_error;
    double v_error,f_error;
    double v_total,f_total;

    if(Verbose) fprintf(Output,"Analyzing the accuracy of the results...\n");

    v_max_error = v_total_error = f_max_error = f_total_error = 0;
    v_total = f_total = 0;
    for(i=0;i < NumParticles;i++){
        v_error = ParticleTable[i].vdirect - ParticleTable[i].v;
        if(v_error < 0) v_error -= v_error;
        v_total_error += v_error;
        if(v_error > v_max_error) v_max_error = v_error;

        if(ParticleTable[i].vdirect > 0){
            v_total += ParticleTable[i].vdirect;
        } else {
            v_total -= ParticleTable[i].vdirect;
        }

        f_error = ParticleTable[i].fdirect.x - ParticleTable[i].f.x;
        if(f_error < 0) f_error -= f_error;
        f_total_error += f_error;
        if(f_error > f_max_error) f_max_error = f_error;

        if(ParticleTable[i].fdirect.x > 0){
            f_total += ParticleTable[i].fdirect.x;
        } else {
            f_total -= ParticleTable[i].fdirect.x;
        }

        f_error = ParticleTable[i].fdirect.y - ParticleTable[i].f.y;
        if(f_error < 0) f_error -= f_error;
        f_total_error += f_error;
        if(f_error > f_max_error) f_max_error = f_error;

        if(ParticleTable[i].fdirect.y > 0){
            f_total += ParticleTable[i].fdirect.y;
        } else {
            f_total -= ParticleTable[i].fdirect.y;
        }

        f_error = ParticleTable[i].fdirect.z - ParticleTable[i].f.z;
        if(f_error < 0) f_error -= f_error;
        f_total_error += f_error;
        if(f_error > f_max_error) f_max_error = f_error;

        if(ParticleTable[i].fdirect.z > 0){
            f_total += ParticleTable[i].fdirect.z;
        } else {
            f_total -= ParticleTable[i].fdirect.z;
        }
    }

    fputc('\n',Output);
    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\nForce and Potential Error Analysis\n\n");

    fprintf(Output,"potential: (absolute) mean = %g, "
        "maximum = %g\n",v_total_error/NumParticles,v_max_error);
    fprintf(Output,"           (relative) mean = %g, "
        "maximum = %g\n\n",v_total_error/v_total,
        v_max_error/(v_total/NumParticles));
    fprintf(Output,"force:     (absolute) mean = %g, "
        "maximum = %g\n",f_total_error/(3*NumParticles),f_max_error);
    fprintf(Output,"           (relative) mean = %g, "
        "maximum = %g\n\n",f_total_error/f_total,
        f_max_error/(f_total/(3*NumParticles)));

    if(Outliers)
	PrintOutliers(v_total/NumParticles,f_total/(3*NumParticles),OutVcutoff,OutFcutoff);

    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\n");

}

void PrintOutliers(double v_mean, double f_mean,double OutVcutoff,double OutFcutoff){
    int i,j,x,y,z,mul,cell,count;
    double rel_p_x,rel_p_y,rel_p_z;
    double v_error,f_error,f_max_error,half_cube_length,cell_length;

    fprintf(Output,"\nList of outliers which exceed a cutoff percentage "
        "relative accuracy:\n\n");
    fprintf(Output,"Cutoff for potential = %-3g %%,  mean potential = %g\n",
        OutVcutoff,v_mean);
    fprintf(Output,"Cutoff for force     = %-3g %%,  mean force     = %g\n\n",
        OutFcutoff,f_mean);

    count = 0;
    OutVcutoff /= 100;
    OutFcutoff /= 100;
    half_cube_length = CubeLength/2;
    for(i=0;i < NumParticles;i++){
        v_error = ParticleTable[i].vdirect - ParticleTable[i].v;
        if(v_error < 0) v_error -= v_error;
        f_max_error = ParticleTable[i].fdirect.x - ParticleTable[i].f.x;
        if(f_max_error < 0) f_max_error -= f_max_error;
        f_error = ParticleTable[i].fdirect.y - ParticleTable[i].f.y;
        if(f_error < 0) f_error -= f_error;
        if(f_error > f_max_error) f_max_error = f_error;
        f_error = ParticleTable[i].fdirect.z - ParticleTable[i].f.z;
        if(f_error < 0) f_error -= f_error;
        if(f_error > f_max_error) f_max_error = f_error;

        if((v_error/v_mean) >= OutVcutoff || 
            (f_max_error/f_mean) >= OutFcutoff){

            count++;
            /*
             * calculate cell location 
             */
            mul = 1;
            x = y = z = 0;
            cell = ParticleTable[i].c - LevelLocate[NumLevels];
            for(j=0;j < NumLevels;j++){
                x |= (cell & 01) << j;
                cell >>= 1;
                y |= (cell & 01) << j;
                cell >>= 1;
                z |= (cell & 01) << j;
                cell >>= 1;
                mul <<= 1;
            }
            cell = ParticleTable[i].c;
            cell_length = CubeLength/((double) mul); /* why is this here BUG?! */
            /*
             * calculate position of particle relative to cube 
             */
            rel_p_x = (ParticleTable[i].p.x - CUBE_CTR_X + 
                half_cube_length)/CubeLength;
            rel_p_y = (ParticleTable[i].p.y - CUBE_CTR_Y + 
                half_cube_length)/CubeLength;
            rel_p_z = (ParticleTable[i].p.z - CUBE_CTR_Z + 
                half_cube_length)/CubeLength;

            fprintf(Output,"cell: %d,  cell location = {%d, %d, %d}, number "
                "of particles = %d\n",cell,x,y,z,CellTable[cell].n);
            fprintf(Output,"particle: %d,  q = %g\nposition = {%g, %g, %g}\n",
                i,ParticleTable[i].q,ParticleTable[i].p.x,
                ParticleTable[i].p.y,ParticleTable[i].p.z);
            fprintf(Output,"position relative to cube = {%g, %g, %g}\n",
                rel_p_x,rel_p_y,rel_p_z);
            fprintf(Output,"position relative to cell = {%g, %g, %g}\n",
                rel_p_x*(double)mul - (double)x,
                rel_p_y*(double)mul - (double)y,
                rel_p_z*(double)mul - (double)z);
            fprintf(Output,"ap: v=%-.9g, f={%-.9g,%-.9g,%-.9g}\n",
                ParticleTable[i].v,ParticleTable[i].f.x,
                ParticleTable[i].f.y,ParticleTable[i].f.z);
            fprintf(Output,"dt: v=%-.9g, f={%-.9g,%-.9g,%-.9g}\n",
                ParticleTable[i].vdirect,ParticleTable[i].fdirect.x,
                ParticleTable[i].fdirect.y,ParticleTable[i].fdirect.z);
            fprintf(Output,"percentage relative potential error: %g %%\n",
                v_error/v_mean*100);
            fprintf(Output,"maximum percentage relative force error: %g %%\n\n",
                f_max_error/f_mean*100);
        }
    }
    fprintf(Output,"Number of Outliers = %d\n\n",count);

}

void TimingAnalysis(void){
    int i,j;
    double MaxTiming[TIME_NUM][2],TotTiming[TIME_NUM][2];
    static char *f[] = {
        "Initializing Cells in CellTable",
        "Identifying Particles with Cells",
        "Distributing Particles to Cells",
		"Reading Particle Positions",
        "Computing Upward Pass (M2M)",
        "Computing Downward Pass",
        "  Direct Computations",
        "  M2L and L2L Computations",
        "Placing Results in ParticleTable",
        "Approximate Method Per Timestep",
        "Direct Method Per Timestep",
    };

    fputc('\n',Output);
    for(i=0;i<75;i++) fputc('=',Output);
#ifndef SERIAL
    if(Pmon){
        fprintf(Output,"\n\nTiming and Performance Analyses\n");
    } else {
        fprintf(Output,"\n\nTiming Analysis\n");
    }
#else
    fprintf(Output,"\n\nTiming Analysis\n\n");
#endif

    for(j=0;j < TIME_NUM;j++){
        TotTiming[j][TIME_USER] = TotTiming[j][TIME_WALL] = 0;
        MaxTiming[j][TIME_USER] = MaxTiming[j][TIME_WALL] = 0;
    }

    for(i=0;i < NumProcessors;i++){
#ifndef SERIAL
        fprintf(Output,"\nPTHREAD %d:\n\n",i);
#endif
        for(j=0;j < TIME_NUM;j++){
            if(j == 3 || j == (TIME_NUM-2)) fputc('\n',Output);
            fprintf(Output,"%-35s:  User  %-9.3g Wall  %-9.3g\n",
                f[j],Timing[i][j][TIME_USER],Timing[i][j][TIME_WALL]);

            TotTiming[j][TIME_USER] += Timing[i][j][TIME_USER];
            TotTiming[j][TIME_WALL] += Timing[i][j][TIME_WALL];
            if(Timing[i][j][TIME_USER] > MaxTiming[j][TIME_USER])
                MaxTiming[j][TIME_USER] = Timing[i][j][TIME_USER];
            if(Timing[i][j][TIME_WALL] > MaxTiming[j][TIME_WALL])
                MaxTiming[j][TIME_WALL] = Timing[i][j][TIME_WALL];
        }
#ifndef SERIAL
        if(Pmon){
            fprintf(Output,"\nPMON Approximate Method Per Timestep:\n\n");
            pmon_print(Output,&PmonAnalysis[i][PMON_AM]);
            if(Direct){
                fprintf(Output,"\nPMON Direct Method Per Timestep:\n\n");
                pmon_print(Output,&PmonAnalysis[i][PMON_DM]);
            }
        }
#endif
        fputc('\n',Output);
    }

#ifndef SERIAL
    fprintf(Output,"\nMEAN:\n\n");
    for(j=0;j < TIME_NUM;j++){
        if(j == 3 || j == (TIME_NUM-2)) fputc('\n',Output);
        fprintf(Output,"%-35s:  User  %-9.3g Wall  %-9.3g\n",
            f[j],TotTiming[j][TIME_USER]/NumProcessors,
            TotTiming[j][TIME_WALL]/NumProcessors);
    }

    fprintf(Output,"\n\nMAXIMUM:\n\n");
    for(j=0;j < TIME_NUM;j++){
        if(j == 3 || j == (TIME_NUM-2)) fputc('\n',Output);
        fprintf(Output,"%-35s:  User  %-9.3g Wall  %-9.3g\n",
            f[j],MaxTiming[j][TIME_USER],MaxTiming[j][TIME_WALL]);
    }
    fputc('\n',Output);
#endif

    for(i=0;i<75;i++) fputc('=',Output);
    fprintf(Output,"\n\n");

}
