/*
 * PMTAinteract.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAinteract.c,v 1.2 1994/11/11 20:26:18 lambert Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAinteract.c,v $
 * Revision 1.2  1994/11/11  20:26:18  lambert
 * Couple of variables that were declared and never used were deleted.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 * adapted for BEM, C.Fochesato, CMLA, 2003
 * allocation of coefficients for storage
 */


#include "include/PMTA.h"

/*
 * Local function declarations
 *
 */
void    CellInteract(int, int, int, int);
int     MAC(int, int);
int     CheckUpperLists(int, int, int, int);
int     FindMultipoleInteract(int, int);
int     FindAndEraseSDirect(int, int);


void PMTAInteractLists(void){
    int     level,k,i,max_cells,iddt_size,isdt_size,ime_size;
    int		*interact,distant_cell;
#ifndef SERIAL
    int error;
#endif
    
    iddt_size = isdt_size = ime_size = ILIST_INC;
    for(level=(EPMTA ? 2:NumLevels);level <= NumLevels;level++){
        max_cells = PthreadCT[level][PTHREAD_START];
        for(k=PthreadCT[level][PTHREAD_END]-1;k >= max_cells;k--){
            if(level == NumLevels){
                CellTable[k].iddirect = (int *) malloc(iddt_size * sizeof(int));
                CellTable[k].coefqddirect1 = (Coefs *) malloc(iddt_size * sizeof(Coefs));
                CellTable[k].coefuddirect1 = (Coefs *) malloc(iddt_size * sizeof(Coefs));
                CellTable[k].coefqddirect2 = (Coefs *) malloc(iddt_size * sizeof(Coefs));
                CellTable[k].coefuddirect2 = (Coefs *) malloc(iddt_size * sizeof(Coefs));
                CellTable[k].isdirect = (int *) malloc(isdt_size * sizeof(int));
                CellTable[k].coefqsdirect = (Coefs *) malloc(isdt_size * sizeof(Coefs));
                CellTable[k].coefusdirect = (Coefs *) malloc(isdt_size * sizeof(Coefs));
                if(((CellTable[k].coefqddirect1 == NULL ||
                     CellTable[k].coefuddirect1 == NULL ||
		     CellTable[k].coefqddirect2 == NULL ||
		     CellTable[k].coefuddirect2 == NULL ||
		     CellTable[k].iddirect == NULL) && CellTable[k].iddt != 0) ||
                   ((CellTable[k].coefqsdirect == NULL ||
		     CellTable[k].coefusdirect == NULL ||
		     CellTable[k].isdirect == NULL) && CellTable[k].isdt != 0))
                    ERROR("(PMTAInteractLists) malloc failure",NULL,0);
                CellTable[k].temp_iddt = iddt_size;
                CellTable[k].temp_isdt = isdt_size;
            }
            CellTable[k].imultipole = (int *) malloc(ime_size * sizeof(int));
            if(CellTable[k].imultipole == NULL && CellTable[k].ime != 0)
                ERROR("(PMTAInteractLists) malloc failure",NULL,0);
            CellTable[k].temp_ime = ime_size;

            for(i=1;i <= 8;i++){
                CellInteract(k,level,i,1);
            }

            if(level == NumLevels){
                CellTable[k].iddirect = (int *) realloc(CellTable[k].iddirect,
                    CellTable[k].iddt * sizeof(int));
                CellTable[k].coefqddirect1 = (Coefs *) realloc(CellTable[k].coefqddirect1,
                    CellTable[k].iddt * sizeof(Coefs));
                CellTable[k].coefuddirect1 = (Coefs *) realloc(CellTable[k].coefuddirect1,
                    CellTable[k].iddt * sizeof(Coefs));
                CellTable[k].coefqddirect2 = (Coefs *) realloc(CellTable[k].coefqddirect2,
                    CellTable[k].iddt * sizeof(Coefs));
                CellTable[k].coefuddirect2 = (Coefs *) realloc(CellTable[k].coefuddirect2,
                    CellTable[k].iddt * sizeof(Coefs));

                CellTable[k].isdirect = (int *) realloc(CellTable[k].coefqsdirect,
                    CellTable[k].isdt * sizeof(int));
                CellTable[k].coefqsdirect = (Coefs *) realloc(CellTable[k].coefqsdirect,
                    CellTable[k].isdt * sizeof(Coefs));
                CellTable[k].coefusdirect = (Coefs *) realloc(CellTable[k].coefusdirect,
                    CellTable[k].isdt * sizeof(Coefs));
                if(((CellTable[k].coefqddirect1 == NULL ||
                     CellTable[k].coefuddirect1 == NULL ||
		     CellTable[k].coefqddirect2 == NULL ||
		     CellTable[k].coefuddirect2 == NULL ||
		     CellTable[k].iddirect == NULL) && CellTable[k].iddt != 0) ||
                    ((CellTable[k].coefqsdirect == NULL ||
		     CellTable[k].coefusdirect == NULL ||
		     CellTable[k].isdirect == NULL) && CellTable[k].isdt != 0))
                    ERROR("(PMTAInteractLists) realloc failure",NULL,0);
                iddt_size = CellTable[k].iddt;
                isdt_size = CellTable[k].isdt;
            }
            CellTable[k].imultipole = (int *) realloc(CellTable[k].imultipole,
                CellTable[k].ime * sizeof(int));
            if(CellTable[k].imultipole == NULL && CellTable[k].ime != 0)
                ERROR("(PMTAInteractLists) realloc failure",NULL,0);
            ime_size = CellTable[k].ime;
        }
    }

	if(BALANCED){
#ifndef SERIAL
    	error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    	if(error == -1) ERROR("(PMTAInteractLists) pthread_barrier_checkout "
			"failure",NULL,0);

    	error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    	if(error == -1) ERROR("(PMTAInteractLists) pthread_barrier_checkin "
			"failure",NULL,0);
#endif
    	max_cells = PthreadCT[NumLevels][PTHREAD_END];
    	for(k=PthreadCT[NumLevels][PTHREAD_START];k < max_cells;k++){
#ifndef SERIAL
			_gspwt(&CellTable[k].p.x);
#endif
			/* allocate space for possible new iddirect interactions */
            CellTable[k].iddirect = (int *) realloc(CellTable[k].iddirect,
                (CellTable[k].iddt + CellTable[k].isdt) * sizeof(int));
            CellTable[k].coefqddirect1 = (Coefs *) realloc(CellTable[k].coefqddirect1,
                (CellTable[k].iddt + CellTable[k].isdt) * sizeof(Coefs));
            CellTable[k].coefuddirect1 = (Coefs *) realloc(CellTable[k].coefuddirect1,
                (CellTable[k].iddt + CellTable[k].isdt) * sizeof(Coefs));
            CellTable[k].coefqddirect2 = (Coefs *) realloc(CellTable[k].coefqddirect2,
                (CellTable[k].iddt + CellTable[k].isdt) * sizeof(Coefs));
            CellTable[k].coefuddirect2 = (Coefs *) realloc(CellTable[k].coefuddirect2,
                (CellTable[k].iddt + CellTable[k].isdt) * sizeof(Coefs));
            if(((CellTable[k].coefqddirect1 == NULL ||
                     CellTable[k].coefuddirect1 == NULL ||
		     CellTable[k].coefqddirect2 == NULL ||
		     CellTable[k].coefuddirect2 == NULL ||
		     CellTable[k].iddirect == NULL) &&
				(CellTable[k].iddt + CellTable[k].isdt) != 0))
                ERROR("(PMTAInteractLists) realloc failure",NULL,0);
			interact = CellTable[k].isdirect;
			for(i=0;i < CellTable[k].isdt;i++){
				distant_cell = interact[i];
				if(k < distant_cell){
#ifndef SERIAL
					_gspwt(&CellTable[distant_cell].p.x);
#endif
            		if(FindAndEraseSDirect(distant_cell,k)){
                		CellTable[k].iddirect[CellTable[k].iddt++] = 
							distant_cell;
						interact[i] = interact[CellTable[k].isdt-1];
						CellTable[k].isdt--;
						i--;
						PthreadILA[NumLevels]--;
					} 
#ifndef SERIAL
					_rsp(&CellTable[distant_cell].p.x);
#endif
				}
			}
#ifndef SERIAL
            CellTable[k].iddirect = (int *) realloc(CellTable[k].iddirect,
                CellTable[k].iddt * sizeof(int));
            CellTable[k].coefqddirect1 = (Coefs *) realloc(CellTable[k].coefqddirect1,
                CellTable[k].iddt * sizeof(Coefs));
            CellTable[k].coefuddirect1 = (Coefs *) realloc(CellTable[k].coefuddirect1,
                CellTable[k].iddt * sizeof(Coefs));
            CellTable[k].coefqddirect2 = (Coefs *) realloc(CellTable[k].coefqddirect2,
                CellTable[k].iddt * sizeof(Coefs));
            CellTable[k].coefuddirect2 = (Coefs *) realloc(CellTable[k].coefuddirect2,
                CellTable[k].iddt * sizeof(Coefs));

            if(((CellTable[k].coefqddirect1 == NULL ||
                     CellTable[k].coefuddirect1 == NULL ||
		     CellTable[k].coefqddirect2 == NULL ||
		     CellTable[k].coefuddirect2 == NULL ||
		     CellTable[k].iddirect == NULL) && CellTable[k].iddt != 0))
                ERROR("(PMTAInteractLists) realloc failure",NULL,0);
			_rsp(&CellTable[k].p.x);
#endif
		}
#ifndef SERIAL
    	error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    	if(error == -1) ERROR("(PMTAInteractLists) pthread_barrier_checkout "
			"failure",NULL,0);

    	error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    	if(error == -1) ERROR("(PMTAInteractLists) pthread_barrier_checkin "
			"failure",NULL,0);
#endif
	}
	
    max_cells = PthreadCT[NumLevels][PTHREAD_END];
    for(k=PthreadCT[NumLevels][PTHREAD_START];k < max_cells;k++){
        CellTable[k].isdirect = (int *) realloc(CellTable[k].isdirect,
            CellTable[k].isdt * sizeof(int));
        CellTable[k].coefqsdirect = (Coefs *) realloc(CellTable[k].coefqsdirect,
            CellTable[k].isdt * sizeof(Coefs));
        CellTable[k].coefusdirect = (Coefs *) realloc(CellTable[k].coefusdirect,
            CellTable[k].isdt * sizeof(Coefs));
        if((CellTable[k].coefqsdirect == NULL ||
		     CellTable[k].coefusdirect == NULL ||
		     CellTable[k].isdirect == NULL) && CellTable[k].isdt != 0)
            ERROR("(PMTAInteractLists) realloc failure",NULL,0);
    }

}
	

void CellInteract(int LocalCell, int LocalLevel, int DistantCell, 
    int DistantLevel){
    int interact_start,interact_end,i;

    if(LocalLevel == NumLevels && 
        (CellTable[LocalCell].temp_iddt - CellTable[LocalCell].iddt) == 0){
        CellTable[LocalCell].temp_iddt += ILIST_INC;
        CellTable[LocalCell].iddirect = (int *)
            realloc(CellTable[LocalCell].iddirect,
                CellTable[LocalCell].temp_iddt * sizeof(int));
        CellTable[LocalCell].coefqddirect1 = (Coefs *)
            realloc(CellTable[LocalCell].coefqddirect1,
                CellTable[LocalCell].temp_iddt * sizeof(Coefs));
        CellTable[LocalCell].coefuddirect1 = (Coefs *)
            realloc(CellTable[LocalCell].coefuddirect1,
                CellTable[LocalCell].temp_iddt * sizeof(Coefs));
        CellTable[LocalCell].coefqddirect2 = (Coefs *)
            realloc(CellTable[LocalCell].coefqddirect2,
                CellTable[LocalCell].temp_iddt * sizeof(Coefs));
        CellTable[LocalCell].coefuddirect2 = (Coefs *)
            realloc(CellTable[LocalCell].coefuddirect2,
                CellTable[LocalCell].temp_iddt * sizeof(Coefs));
        if(CellTable[LocalCell].coefqddirect1 == NULL ||
                     CellTable[LocalCell].coefuddirect1 == NULL ||
		     CellTable[LocalCell].coefqddirect2 == NULL ||
		     CellTable[LocalCell].coefuddirect2 == NULL ||
		     CellTable[LocalCell].iddirect == NULL)
            ERROR("(CellInteract) realloc failure",NULL,0);
    }
    if(LocalLevel == NumLevels && 
        (CellTable[LocalCell].temp_isdt - CellTable[LocalCell].isdt) == 0){
        CellTable[LocalCell].temp_isdt += ILIST_INC;
        CellTable[LocalCell].isdirect = (int *)
            realloc(CellTable[LocalCell].isdirect,
                CellTable[LocalCell].temp_isdt * sizeof(int));
        CellTable[LocalCell].coefqsdirect = (Coefs *)
            realloc(CellTable[LocalCell].coefqsdirect,
                CellTable[LocalCell].temp_isdt * sizeof(Coefs));
        CellTable[LocalCell].coefusdirect = (Coefs *)
            realloc(CellTable[LocalCell].coefusdirect,
                CellTable[LocalCell].temp_isdt * sizeof(Coefs));
        if(CellTable[LocalCell].coefqsdirect == NULL ||
		     CellTable[LocalCell].coefusdirect == NULL ||
		     CellTable[LocalCell].isdirect == NULL)
            ERROR("(CellInteract) realloc failure",NULL,0);
    }
    if((CellTable[LocalCell].temp_ime - CellTable[LocalCell].ime) == 0){
        CellTable[LocalCell].temp_ime += ILIST_INC;
        CellTable[LocalCell].imultipole = (int *)
            realloc(CellTable[LocalCell].imultipole,
                CellTable[LocalCell].temp_ime * sizeof(int));
        if(CellTable[LocalCell].imultipole == NULL)
            ERROR("(CellInteract) realloc failure",NULL,0);
    }

    /*
     * first check if DistantLevel is the lowest level in the tree
     * If so, then interact either through particle-particle interactions
     * or particle-cell depending on the MAC
     *
     * if DistantLevel is higher than the lowest level in the tree, then
     * only interact with DistantCell if the MAC succeeds, DistantCell
     * is not a parent of LocalCell and that any parent of LocalCell does
     * not have DistantCell or any of its parents or children in 
	 * its interaction list.
	 *
     * there are three interaction lists per cell: multipole, single direct,
     * and double direct
     * multipole records multipole interactions
     * single direct records direct interactions with cells on other 
	 * processors if no load balancing is used
     * double direct records direct interactions with cells on same processor
     * or even on other processors if load balancing is used
     */
    if(DistantLevel == NumLevels){
        if(MAC(LocalCell,DistantCell) && LocalCell != DistantCell){
            CellTable[LocalCell].imultipole[CellTable[LocalCell].ime++] = 
                DistantCell;
            PthreadILA[DistantLevel]++;
        } else if((DistantCell < PthreadCT[DistantLevel][PTHREAD_START] ||
            DistantCell >= PthreadCT[DistantLevel][PTHREAD_END])){
            CellTable[LocalCell].isdirect[CellTable[LocalCell].isdt++] = 
                DistantCell;
            PthreadILA[DistantLevel]++;
        } else if((LocalCell > DistantCell)){
            CellTable[LocalCell].isdirect[CellTable[LocalCell].isdt++] = 
                DistantCell;
            PthreadILA[DistantLevel]++;
        } else if((LocalCell < DistantCell)){
            if(FindAndEraseSDirect(DistantCell,LocalCell)){
                CellTable[LocalCell].iddirect[CellTable[LocalCell].iddt++] = 
                    DistantCell;
            } else {
                CellTable[LocalCell].isdirect[CellTable[LocalCell].isdt++] = 
                    DistantCell;
                PthreadILA[DistantLevel]++;
            }
        }
    } else if(MAC(LocalCell,DistantCell) && 
        (((LocalCell - LevelLocate[LocalLevel]) >> 
        (3*(LocalLevel - DistantLevel))) != DistantCell) && 
		FirstUpperCheck(LocalCell,LocalLevel,DistantCell,DistantLevel)){ 
        if(CheckUpperLists(LocalCell,LocalLevel,DistantCell,DistantLevel)){
            CellTable[LocalCell].imultipole[CellTable[LocalCell].ime++] = 
                DistantCell;
            PthreadILA[DistantLevel]++;
		}
    } else if(DistantLevel < LocalLevel){
        interact_start = ((DistantCell - LevelLocate[DistantLevel]) << 3) +
            LevelLocate[DistantLevel+1];
        interact_end = interact_start + 8;
        DistantLevel++;
        for(i=interact_start;i < interact_end;i++){
            CellInteract(LocalCell,LocalLevel,i,DistantLevel);
        }
	}

}

int MAC(int LocalCell, int DistantCell){
    double dx,dy,dz;
    double sum_radii;     
    double test;        /* h (of DistantCell) / Theta */
    double r;

    /*
     * MAC succeeds when r > (h / Theta) 
     */

    test = CellTable[DistantCell].h * iTheta;
    sum_radii = (CellTable[DistantCell].h + CellTable[LocalCell].h) *
        0.866025403;

    dx = CellTable[LocalCell].p.x-CellTable[DistantCell].p.x;
    dy = CellTable[LocalCell].p.y-CellTable[DistantCell].p.y;
    dz = CellTable[LocalCell].p.z-CellTable[DistantCell].p.z;
    r = sqrt(dx*dx + dy*dy + dz*dz) - sum_radii;

    if(r > test){
        return TRUE;
    } else {
        return FALSE;
    }

}

/*
 * FirstUpperCheck checks that a parent of LocalCell is not interacting with
 * any children of DistantCell.
 */
int FirstUpperCheck(int LocalCell, int LocalLevel, int DistantCell, 
    int DistantLevel){
    int i,j;
    int k,check_end;

	if(!EPMTA) return TRUE;

    for(i=LocalLevel-1;i > 1;i--){
        LocalCell = ((LocalCell - LevelLocate[i+1]) >> 3) + LevelLocate[i];
        for(j=DistantLevel+1;j <= i;j++){
            k = ((DistantCell - LevelLocate[DistantLevel]) << 
                (3*(j - DistantLevel))) + LevelLocate[j];
            check_end = k + Power8[j - DistantLevel];
            for(;k < check_end;k++){
                if(FindMultipoleInteract(LocalCell,k)) return FALSE;
            }
        }
    }
    return TRUE;

}

/*
 * CheckUpperLists checks that a DistantCell or any parent or
 * grand parent of DistantCell are not on any of the interaction lists 
 * owned by the parents of LocalCell
 */
int CheckUpperLists(int LocalCell, int LocalLevel, int DistantCell, 
    int DistantLevel){
    int     i,j,check_distantcell;

	if(!EPMTA) return TRUE;

    for(i=LocalLevel-1;i > 1;i--){
        LocalCell = ((LocalCell - LevelLocate[i+1]) >> 3) + LevelLocate[i];
        if(DistantLevel > i){
            j = i;
            check_distantcell = ((DistantCell - LevelLocate[DistantLevel])
                >> (3*(DistantLevel - i))) + LevelLocate[i];
        } else {
            j = DistantLevel;
            check_distantcell = DistantCell;
        }
        for(;j > 0;j--){
            if(FindMultipoleInteract(LocalCell,check_distantcell)) return FALSE;
            check_distantcell = ((check_distantcell - LevelLocate[j]) >> 3) +
                LevelLocate[j-1];
        }
    }
    return TRUE;

}

/*
 * FindMultipoleInteract returns TRUE if a DistantCell is on the multipole 
 * interaction list of a LocalCell else it returns FALSE
 */
int FindMultipoleInteract(int LocalCell, int DistantCell){
    int i;
    int *list;

    list = CellTable[LocalCell].imultipole;
    for(i=0;i < CellTable[LocalCell].ime;i++){
        if(DistantCell == list[i]) return TRUE;
    }
    return FALSE;

}

/*
 * FindAndEraseSDirect returns TRUE if a DistantCell is on the single
 * direct interaction list of a LocalCell.  It also deletes this
 * interaction from the single direct list.  Otherwise, it returns FALSE
 */
int FindAndEraseSDirect(int LocalCell, int DistantCell){
    int i;
    int *list;

    list = CellTable[LocalCell].isdirect;
    for(i=0;i < CellTable[LocalCell].isdt;i++){
        if(DistantCell == list[i]){
            list[i] = list[CellTable[LocalCell].isdt-1];
            CellTable[LocalCell].isdt--;
            return TRUE;
        }
    }
    return FALSE;

}
