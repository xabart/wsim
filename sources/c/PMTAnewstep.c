/*
 * PMTAnewstep.c
 *
 * routine for updating data structures when severzl calls from BEM
 *
 * C.Fochesato, CMLA, 2003
 */

#include "include/PMTA.h"


void InitializeNewStep(void){

      ParticleCells();
      CellPlists();
      ParticleCellsCoef(1);

}

void ParticleCellsNewStep(void){
    int             i,j,bit,shift,x,y,z,locate,nie,n,interj;
    int             size_p,start,end;
    unsigned int    c;
    double          num,hCLx,hCLy,hCLz;
#ifndef SERIAL
    int error,size_sp,cell;
#endif

    PthreadStartTiming(TIME_PC);

    if(Verbose && PthreadNum == 0)
        fprintf(Output,"Assigning particles and elements to cells...\n");

    /*
     * set the number of particles and elements in each cell in the CellTable
     * to zero
     */
    start = PthreadCT[NumLevels][PTHREAD_START];
    end = PthreadCT[NumLevels][PTHREAD_END];
    for(i=start;i < end;i++){
        CellTable[i].n = 0;
        CellTable[i].mesh.nbelts = 0;
    }

    locate = LevelLocate[NumLevels];
    num = pow(2.0,(double) NumLevels);
    hCLx = CubeLength/2 - CUBE_CTR_X;
    hCLy = CubeLength/2 - CUBE_CTR_Y;
    hCLz = CubeLength/2 - CUBE_CTR_Z;
    for(i=PthreadPTStart;i < PthreadPTEnd;i++){
        x = (int) ((ParticleTable[i].p.x + hCLx)/CubeLength * num);
        if(x == num) x--;
        y = (int) ((ParticleTable[i].p.y + hCLy)/CubeLength * num);
        if(y == num) y--;
        z = (int) ((ParticleTable[i].p.z + hCLz)/CubeLength * num);
        if(z == num) z--;
        for(j=0,c=0,bit=1,shift=0;j < NumLevels;j++){
            c |= (bit & x) << shift;
            shift++;
            c |= (bit & y) << shift;
            shift++;
            c |= (bit & z) << shift;
            bit <<= 1;
        }
        c += locate;
        ParticleTable[i].c = c;
#ifdef SERIAL
        CellTable[c].n++;
#endif
    }
    for(i=PthreadETStart;i < PthreadETEnd;i++){
        x = (int) ((ElementTable[i].p.x + hCLx)/CubeLength * num);
        if(x == num) x--;
        y = (int) ((ElementTable[i].p.y + hCLy)/CubeLength * num);
        if(y == num) y--;
        z = (int) ((ElementTable[i].p.z + hCLz)/CubeLength * num);
        if(z == num) z--;
        for(j=0,c=0,bit=1,shift=0;j < NumLevels;j++){
            c |= (bit & x) << shift;
            shift++;
            c |= (bit & y) << shift;
            shift++;
            c |= (bit & z) << shift;
            bit <<= 1;
        }
        c += locate;
        ElementTable[i].c = c;
#ifdef SERIAL
        CellTable[c].mesh.nbelts++;
#endif
    }
#ifndef SERIAL
    error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkout failure",NULL,0);

    error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
    if(error == -1) ERROR("(Parallel) pthread_barrier_checkin failure",NULL,0);

    /*
     * calculate the number of particles in each cell and the nodes indexes
     */
    for(i=0;i < NumParticles;i++){
        cell = ParticleTable[i].c;
        if(cell >= start && cell < end)
            CellTable[cell].n++;
    }
    /*
     * calculate the number of elements in each cell and the element indexes
     */
    for(i=0;i < NumElements;i++){
        cell = ElementTable[i].c;
        if(cell >= start && cell < end)
            CellTable[cell].mesh.nbelts++;
    }
#endif

    /*
     * allocate space for the particle lists
     */
    size_p = sizeof(Particle);
#ifndef SERIAL
    size_sp = sizeof(SmallParticle);
#endif
    for(i=start;i < end;i++){
        free(CellTable[i].pa);
#ifndef SERIAL
        free(CellTable[i].spa);
#endif
        if(CellTable[i].n == 0){
            CellTable[i].pa = NULL;
#ifndef SERIAL
            CellTable[i].spa = NULL;
#endif
        } else {
            CellTable[i].pa = (ParticlePtr) malloc(CellTable[i].n * size_p);
#ifndef SERIAL
            CellTable[i].spa = (SmallParticlePtr)
                malloc(CellTable[i].n * size_sp);
#endif
            if(CellTable[i].pa == NULL
#ifndef SERIAL
                || CellTable[i].spa == NULL
#endif
                )
                ERROR("(ParticleCells) malloc failure",NULL,0);
        }
    }
    /*
     * allocate space for the element lists
     */
    size_p = sizeof(int);
    for(i=start;i < end;i++){
        free(CellTable[i].mesh.ielts);
        free(CellTable[i].mesh.inodes);
        if(CellTable[i].mesh.nbelts == 0){
            CellTable[i].mesh.ielts = NULL;
        } else {
            CellTable[i].mesh.ielts = (int*) malloc(CellTable[i].mesh.nbelts * size_p);
            if(CellTable[i].mesh.ielts == NULL
                )
                ERROR("(ParticleCells) malloc failure",NULL,0);
        }
        if(CellTable[i].n == 0){
            CellTable[i].mesh.inodes = NULL;
        } else {
            CellTable[i].mesh.inodes = (int*) malloc(CellTable[i].n * size_p);
            if(CellTable[i].mesh.inodes == NULL
                )
                ERROR("(ParticleCells) malloc failure",NULL,0);
        }
    }
    for(i=start;i < end;i++){
        nie=CellTable[i].mesh.nbelts;
        n=CellTable[i].n;

	free(CellTable[i].coefqselfdirect);
        free(CellTable[i].coefuselfdirect);
	for(j=0;j<CellTable[i].iddt;j++){
	    free(CellTable[i].coefqddirect1[j]);
	    free(CellTable[i].coefuddirect1[j]);
	    free(CellTable[i].coefqddirect2[j]);
	    free(CellTable[i].coefuddirect2[j]);
	}
	for(j=0;j<CellTable[i].isdt;j++){
	    free(CellTable[i].coefqsdirect[j]);
	    free(CellTable[i].coefusdirect[j]);
	}
	free(CellTable[i].coefqxmultipole);
	free(CellTable[i].coefqymultipole);
	free(CellTable[i].coefuxmultipole);
	free(CellTable[i].coefuymultipole);

        if(n>0 && nie>0){
	  CellTable[i].coefqselfdirect = (double *) malloc(nie*n*MNODM * sizeof(double));
          CellTable[i].coefuselfdirect = (double *) malloc(nie*n*MNODM * sizeof(double));
	}
	else{
	  CellTable[i].coefqselfdirect = NULL;
          CellTable[i].coefuselfdirect = NULL;
	}
	for(j=0;j<CellTable[i].iddt;j++){
	  interj=CellTable[i].iddirect[j];
          if(CellTable[interj].n>0 && nie>0){
            CellTable[i].coefqddirect1[j] = (double *) malloc(
	    		nie*CellTable[interj].n*MNODM * sizeof(double));
            CellTable[i].coefuddirect1[j] = (double *) malloc(
	    		nie*CellTable[interj].n*MNODM * sizeof(double));
	  }
	  else{
            CellTable[i].coefqddirect1[j] = NULL;
            CellTable[i].coefuddirect1[j] = NULL;
	  }
          if(n>0 && CellTable[interj].mesh.nbelts>0){
            CellTable[i].coefqddirect2[j] = (double *) malloc(
	    		CellTable[interj].mesh.nbelts*n*MNODM * sizeof(double));
            CellTable[i].coefuddirect2[j] = (double *) malloc(
	    		CellTable[interj].mesh.nbelts*n*MNODM * sizeof(double));
	  }else{
            CellTable[i].coefqddirect2[j] = NULL;
            CellTable[i].coefuddirect2[j] = NULL;
	  }
	}
	for(j=0;j<CellTable[i].isdt;j++){
	  interj=CellTable[i].isdirect[j];
          if(CellTable[interj].n>0 && nie>0){
            CellTable[i].coefqsdirect[j] = (double *) malloc(
	    		nie*CellTable[interj].n*MNODM * sizeof(double));
            CellTable[i].coefusdirect[j] = (double *) malloc(
	    		nie*CellTable[interj].n*MNODM * sizeof(double));
	  }else{
            CellTable[i].coefqsdirect[j] = NULL;
            CellTable[i].coefusdirect[j] = NULL;
	  }
	}
	/* cf allocation alloc_mpe dans cellsetup */
	if(nie>0){
          CellTable[i].coefqxmultipole = (double *) malloc(nie*MNODM*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefqymultipole = (double *) malloc(nie*MNODM*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefuxmultipole = (double *) malloc(nie*MNODM*(Mp+1)*(Mp+1) * sizeof(double));
          CellTable[i].coefuymultipole = (double *) malloc(nie*MNODM*(Mp+1)*(Mp+1) * sizeof(double));
	}else{
          CellTable[i].coefqxmultipole = NULL;
          CellTable[i].coefqymultipole = NULL;
          CellTable[i].coefuxmultipole = NULL;
          CellTable[i].coefuymultipole = NULL;
	}
    }


    PthreadStopTiming(TIME_PC);

}
