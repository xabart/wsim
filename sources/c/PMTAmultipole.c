/*
 * PMTAmultipole.c
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

static char rcsid[] = "$Id: PMTAmultipole.c,v 1.2 1994/11/11 20:43:44 lambert Exp $";

/*
 * Revision History:
 *
 * $Log: PMTAmultipole.c,v $
 * Revision 1.2  1994/11/11  20:43:44  lambert
 * A couple of vars were declared but never used -- deleted them.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 * Adapted for BEM, C.Fochesato, CMLA, 2003
 *
 */
 
#include "include/PMTA.h"
#include "PMTAlegendre.h"
#include "PMTAfft.h"

/* 
 * Local function declarations
 */

void Multipole(int, int);
void ShiftMultipole(int, int);

/* 
 * CellMultipoles calls Multipole and ShiftMultipole to compute the
 * multipole expansions for all sub-cells
 *
 * Multipole is used to calculate the multipole expansions for all
 * cells at level NumLevels
 * for all lower level cells, the multipole expansions are calculated
 * by shifting child multipoles using ShiftMultipole
 */
void CellMultipoles(void){
    int i,j,k,n,m,cell_end,child_start,child_end,MShiftLevel;
    int MEndLevel;
    Mtype M;
#if (ISYM==1 || IBOT==10)
    Mtype Msym;
#endif
#if (ISYM==1 && IBOT==10)
    Mtype Msym2;
    Mtype Msym3;
#endif
#ifndef SERIAL
    int error;
#endif    

    PthreadStartTiming(TIME_CM);
    
    if(Verbose && PthreadNum == 0)
        fprintf(Output,"Computing cell multipoles...\n");

    MShiftLevel = NumLevels - 1;
    for(i=NumLevels;i > MShiftLevel;i--){
        cell_end = PthreadCT[i][PTHREAD_END];
        for(j=PthreadCT[i][PTHREAD_START];j < cell_end;j++){
            Multipole(i,j);
        }
    }
    
    if(PFMA && !EPMTA){
        MEndLevel = 1;
    } else {
        MEndLevel = 0;
    }
    for(i=MShiftLevel;i > MEndLevel;i--){
#ifndef SERIAL
        if(i < ProcessorLevel){
            error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
            if(error == -1) ERROR("(CellMultipoles) pthread_barrier_checkout "
                "failure",NULL,0);

            error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
            if(error == -1) ERROR("(CellMultipoles) pthread_barrier_checkin "
                "failure",NULL,0);
        }
#endif
        cell_end = PthreadCT[i][PTHREAD_END];
        for(j=PthreadCT[i][PTHREAD_START];j < cell_end;j++){
            /* 
             * initialize the parent expansion 
             */
            M = CellTable[j].m;
            for(n=0;n <= Mp;n++){
                for(m=0;m <= n;m++){
                    M[n][m].x = 0.0;
                    M[n][m].y = 0.0;
                }
            }
#if (ISYM==1 || IBOT==10)
            Msym = CellTable[j].msym;
            for(n=0;n <= Mp;n++){
                for(m=0;m <= n;m++){
                    Msym[n][m].x = 0.0;
                    Msym[n][m].y = 0.0;
                }
            }
#endif
#if (ISYM==1 && IBOT==10)
            Msym2 = CellTable[j].msym2;
            Msym3 = CellTable[j].msym3;
            for(n=0;n <= Mp;n++){
                for(m=0;m <= n;m++){
                    Msym2[n][m].x = 0.0;
                    Msym2[n][m].y = 0.0;
                    Msym3[n][m].x = 0.0;
                    Msym3[n][m].y = 0.0;
                }
            }
#endif
            child_start = ((j - LevelLocate[i]) << 3) + LevelLocate[i+1];
            child_end = child_start + 8;
            CellTable[j].vl = FALSE;
            for(k=child_start;k < child_end;k++){
                if(CellTable[k].vl) {
                    ShiftMultipole(j,k);
                    CellTable[j].vl = TRUE;
                } /* if child has a multipole expansion */
            } /* for k (all the children) */ 
        } /* for j */
    } /* for i */
    
    if(FFT){
#ifndef SERIAL
        if((MEndLevel+1) < ProcessorLevel){
            error = pthread_barrier_checkout(&PthreadBarrier,PthreadNum);
            if(error == -1) ERROR("(CellMultipoles) pthread_barrier_checkout "
                "failure",NULL,0);

            error = pthread_barrier_checkin(&PthreadBarrier,PthreadNum);
            if(error == -1) ERROR("(CellMultipoles) pthread_barrier_checkin "
                "failure",NULL,0);
        }
#endif
        for(i=MEndLevel+1;i <= NumLevels;i++){
            cell_end = PthreadCT[i][PTHREAD_END];
            for(j=PthreadCT[i][PTHREAD_START];j < cell_end;j++){
                FFTMultipole(j);
            }
        }
    } /* if FFT */

    PthreadStopTiming(TIME_CM);

} /* CellMultipoles */

/*
 * Multipole calculates the multipole expansion (M) of the particles
 * in Cell about the center of the cube
 *
 * Adapted from the PFMA code written by Jim Leathrum
 */
void Multipole(int Level, int Cell){
    int i,j,n,m,jg,k,ie;
    int cell_start,cell_end;
    double rterm,temp,q;
#ifndef SERIAL
    SmallParticlePtr cell_particles;
#else
    ParticlePtr cell_particles;
#endif
    SphVector z0;
    Vector x0,place;
    Mtype M;
#if (ISYM==1 || IBOT==10)
    Mtype Msym;
#endif
#if (ISYM==1 && IBOT==10)
    Mtype Msym2;
    Mtype Msym3;
#endif
    int nie;
    int *elts;
    double *MYcstV;

extern struct {
   double q[NUMPART];
   double sol[NUMPART];
   int ibemanalysis;
   int irightleftrigid;
} charge_;
extern struct {
   double zmax;
   int incond[NUMPART];
   int inface[NUMPART];
} dbnode_;

    MYcstV=(double*) malloc((Mp+1)*(Mp+1)*sizeof(double));
    if(MYcstV == NULL)
       ERROR("(multipole) malloc failure",NULL,0);
    /*
     * initialize the expansion
     */
    M = CellTable[Cell].m;
    for(n=0;n <= Mp;n++){
        for(m=0;m <= n;m++){
            M[n][m].x = 0.0;
            M[n][m].y = 0.0;
        }
    }
#if (ISYM==1 || IBOT==10)
            Msym = CellTable[Cell].msym;
            for(n=0;n <= Mp;n++){
                for(m=0;m <= n;m++){
                    Msym[n][m].x = 0.0;
                    Msym[n][m].y = 0.0;
                }
            }
#endif
#if (ISYM==1 && IBOT==10)
            Msym2 = CellTable[Cell].msym2;
            Msym3 = CellTable[Cell].msym3;
            for(n=0;n <= Mp;n++){
                for(m=0;m <= n;m++){
                    Msym2[n][m].x = 0.0;
                    Msym2[n][m].y = 0.0;
                    Msym3[n][m].x = 0.0;
                    Msym3[n][m].y = 0.0;
                }
            }
#endif
    /*
     * loop through all elements within the given cell
     */
    place  = CellTable[Cell].p;
    cell_start = ((Cell - LevelLocate[Level]) << (3*(NumLevels - Level))) +
        LevelLocate[NumLevels];
    cell_end = cell_start + Power8[NumLevels - Level];
    CellTable[Cell].vl = FALSE;
    for(i=cell_start;i < cell_end;i++){
	nie=CellTable[i].mesh.nbelts;
	if(nie>0){
          CellTable[Cell].vl = TRUE;
	  elts=CellTable[i].mesh.ielts;
	  if(charge_.ibemanalysis){
            for(n=0;n <= Mp;n++){
              for(m=0;m <= Mp;m++){
	        if (m<=n){
		  /* only 1D vector for Fortran subroutine */
                  MYcstV[n*(Mp+1)+m] = MYconstant[n][m];
	        }
	        else{
                  MYcstV[n*(Mp+1)+m] = 0.0;
	        }
	      }
	    }
	    /*
	    * BEM analysis
	    */
#if (ISYM==0 && IBOT!=10)
	    bemkfma_(CellTable[i].coefqxmultipole,CellTable[i].coefqymultipole,CellTable[i].coefuxmultipole,
	             CellTable[i].coefuymultipole,&(CellTable[i].mesh.nnie),
		     &(place.x),&(place.y),&(place.z),&nie,elts,&Mp,MYcstV);
#else
#if (ISYM==1 && IBOT==10)
            /*
             * saves also the symmetrical contribution
             */
	    bemksym2_(CellTable[i].coefqxmultipole,CellTable[i].coefqymultipole,
	             CellTable[i].coefuxmultipole,CellTable[i].coefuymultipole,
		     CellTable[i].coefqxmultisym,CellTable[i].coefqymultisym,
	             CellTable[i].coefuxmultisym,CellTable[i].coefuymultisym,
		     CellTable[i].coefqxmultisym2,CellTable[i].coefqymultisym2,
	             CellTable[i].coefuxmultisym2,CellTable[i].coefuymultisym2,
		     CellTable[i].coefqxmultisym3,CellTable[i].coefqymultisym3,
	             CellTable[i].coefuxmultisym3,CellTable[i].coefuymultisym3,
		     &(CellTable[i].mesh.nnie),
		     &(place.x),&(place.y),&(place.z),&nie,elts,&Mp,MYcstV);
#else
	    bemksym_(CellTable[i].coefqxmultipole,CellTable[i].coefqymultipole,
	             CellTable[i].coefuxmultipole,CellTable[i].coefuymultipole,
		     CellTable[i].coefqxmultisym,CellTable[i].coefqymultisym,
	             CellTable[i].coefuxmultisym,CellTable[i].coefuymultisym,
		     &(CellTable[i].mesh.nnie),
		     &(place.x),&(place.y),&(place.z),&nie,elts,&Mp,MYcstV);
#endif
#endif
	  }
	  for(j=0;j<CellTable[i].mesh.nnie;j++){
	    jg=CellTable[i].mesh.jgcoef[j];
	    for(n=0;n<=Mp;n++){
	      for(m=0;m<=n;m++){
		k=(j*(Mp+1)+n)*(Mp+1)+m;
	        if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==1){
                  M[n][m].x-=charge_.q[jg-1]*CellTable[i].coefqxmultipole[k];
                  M[n][m].y-=charge_.q[jg-1]*CellTable[i].coefqymultipole[k];
	        }
	        if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==1){
                  M[n][m].x+=charge_.q[jg-1]*CellTable[i].coefuxmultipole[k];
                  M[n][m].y+=charge_.q[jg-1]*CellTable[i].coefuymultipole[k];
	        }
	        if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==2){
                  M[n][m].x-=charge_.q[jg-1]*CellTable[i].coefuxmultipole[k];
                  M[n][m].y-=charge_.q[jg-1]*CellTable[i].coefuymultipole[k];
	        }
	        if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==2){
                  M[n][m].x+=charge_.q[jg-1]*CellTable[i].coefqxmultipole[k];
                  M[n][m].y+=charge_.q[jg-1]*CellTable[i].coefqymultipole[k];
	        }
	        if(charge_.irightleftrigid==3){
                  M[n][m].x+=CellTable[i].coefqxmultipole[k];
                  M[n][m].y+=CellTable[i].coefqymultipole[k];
	        }
#if (ISYM==1 && IBOT==10)
	        if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==1){
                  Msym[n][m].x-=charge_.q[jg-1]*CellTable[i].coefqxmultisym[k];
                  Msym[n][m].y-=charge_.q[jg-1]*CellTable[i].coefqymultisym[k];
                  Msym2[n][m].x-=charge_.q[jg-1]*CellTable[i].coefqxmultisym2[k];
                  Msym2[n][m].y-=charge_.q[jg-1]*CellTable[i].coefqymultisym2[k];
                  Msym3[n][m].x-=charge_.q[jg-1]*CellTable[i].coefqxmultisym3[k];
                  Msym3[n][m].y-=charge_.q[jg-1]*CellTable[i].coefqymultisym3[k];
	        }
	        if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==1){
                  Msym[n][m].x+=charge_.q[jg-1]*CellTable[i].coefuxmultisym[k];
                  Msym[n][m].y+=charge_.q[jg-1]*CellTable[i].coefuymultisym[k];
                  Msym2[n][m].x+=charge_.q[jg-1]*CellTable[i].coefuxmultisym2[k];
                  Msym2[n][m].y+=charge_.q[jg-1]*CellTable[i].coefuymultisym2[k];
                  Msym3[n][m].x+=charge_.q[jg-1]*CellTable[i].coefuxmultisym3[k];
                  Msym3[n][m].y+=charge_.q[jg-1]*CellTable[i].coefuymultisym3[k];
	        }
	        if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==2){
                  Msym[n][m].x-=charge_.q[jg-1]*CellTable[i].coefuxmultisym[k];
                  Msym[n][m].y-=charge_.q[jg-1]*CellTable[i].coefuymultisym[k];
                  Msym2[n][m].x-=charge_.q[jg-1]*CellTable[i].coefuxmultisym2[k];
                  Msym2[n][m].y-=charge_.q[jg-1]*CellTable[i].coefuymultisym2[k];
                  Msym3[n][m].x-=charge_.q[jg-1]*CellTable[i].coefuxmultisym3[k];
                  Msym3[n][m].y-=charge_.q[jg-1]*CellTable[i].coefuymultisym3[k];
	        }
	        if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==2){
                  Msym[n][m].x+=charge_.q[jg-1]*CellTable[i].coefqxmultisym[k];
                  Msym[n][m].y+=charge_.q[jg-1]*CellTable[i].coefqymultisym[k];
                  Msym2[n][m].x+=charge_.q[jg-1]*CellTable[i].coefqxmultisym2[k];
                  Msym2[n][m].y+=charge_.q[jg-1]*CellTable[i].coefqymultisym2[k];
                  Msym3[n][m].x+=charge_.q[jg-1]*CellTable[i].coefqxmultisym3[k];
                  Msym3[n][m].y+=charge_.q[jg-1]*CellTable[i].coefqymultisym3[k];
	        }
	        if(charge_.irightleftrigid==3){
                  Msym[n][m].x+=CellTable[i].coefqxmultisym[k];
                  Msym[n][m].y+=CellTable[i].coefqymultisym[k];
                  Msym2[n][m].x+=CellTable[i].coefqxmultisym2[k];
                  Msym2[n][m].y+=CellTable[i].coefqymultisym2[k];
                  Msym3[n][m].x+=CellTable[i].coefqxmultisym3[k];
                  Msym3[n][m].y+=CellTable[i].coefqymultisym3[k];
	        }
#else
#if (ISYM==1 || IBOT==10)
	        if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==1){
                  Msym[n][m].x-=charge_.q[jg-1]*CellTable[i].coefqxmultisym[k];
                  Msym[n][m].y-=charge_.q[jg-1]*CellTable[i].coefqymultisym[k];
	        }
	        if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==1){
                  Msym[n][m].x+=charge_.q[jg-1]*CellTable[i].coefuxmultisym[k];
                  Msym[n][m].y+=charge_.q[jg-1]*CellTable[i].coefuymultisym[k];
	        }
	        if(dbnode_.incond[jg-1]==1 && charge_.irightleftrigid==2){
                  Msym[n][m].x-=charge_.q[jg-1]*CellTable[i].coefuxmultisym[k];
                  Msym[n][m].y-=charge_.q[jg-1]*CellTable[i].coefuymultisym[k];
	        }
	        if(dbnode_.incond[jg-1]!=1 && charge_.irightleftrigid==2){
                  Msym[n][m].x+=charge_.q[jg-1]*CellTable[i].coefqxmultisym[k];
                  Msym[n][m].y+=charge_.q[jg-1]*CellTable[i].coefqymultisym[k];
	        }
	        if(charge_.irightleftrigid==3){
                  Msym[n][m].x+=CellTable[i].coefqxmultisym[k];
                  Msym[n][m].y+=CellTable[i].coefqymultisym[k];
	        }
#endif
#endif
              }
	    }
	  }
	}
    }
    free(MYcstV);
    MYcstV=NULL;

}

/*
 * ShiftMultipole translates a child's multipole expansion (O) to an
 * expansion (M) about its parent's box center
 * the translated child multipole expansion is added to the existing
 * parent multipole expansion
 *
 * Adapted from the PFMA code written by Jim Leathrum
 */
void ShiftMultipole(int ParentCell, int ChildCell){
    int j,k,n,m,km,jn;
    double r,temp,Ox,Oy,s2x,s2y;
    double **s2_c;
    Vector x0;
    SphVector center;
    Complex *t;
    Mtype O,M;
#if (ISYM==1 || IBOT==10)
    Mtype Osym,Msym;
#endif
#if (ISYM==1 && IBOT==10)
    Mtype Osym2,Msym2;
    Mtype Osym3,Msym3;
#endif

    M = CellTable[ParentCell].m;
    O = CellTable[ChildCell].m;

    /*
     * calculate the center-of-cube of the child cell in spherical
     * coordinates relative to the center-of-cube of the parent cell
     * centered at the origin
     */
    x0.x = CellTable[ChildCell].p.x - CellTable[ParentCell].p.x;
    x0.y = CellTable[ChildCell].p.y - CellTable[ParentCell].p.y;
    x0.z = CellTable[ChildCell].p.z - CellTable[ParentCell].p.z;
    center.r = sqrt(x0.x*x0.x + x0.y*x0.y + x0.z*x0.z);
    center.a = acos(x0.z/center.r);
    center.b = atan2(x0.y,x0.x);

    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin(-(double) m * center.b);
    }

    /*
     * compute the Legendre Polynomial
     */
    temp = cos(center.a);
    Legendre(MLegendre,temp,Mp);

    /*
     * precomputed terms
     */
    r = 1.0;
    for(n=0;n <= Mp;n++){
        for(m=0;m <= n;m++){
            temp = r * MLegendre[n][m];
            Ms[n][m].x = temp * Mt1[m].x;
            Ms[n][m].y = temp * Mt1[m].y;
        }
        r *= center.r;
    }

    /*
     * now perform the shift
     */
    for(j=0;j <= Mp;j++){
        for(k=0;k <= j;k++){
            t = &M[j][k];
            s2_c = Ms2constant[j][k];
            for(n=0;n <= j;n++){
                jn = j-n;
                /* m == 0 */
                temp = s2_c[n][0];
                Ox = O[jn][k].x;
                Oy = O[jn][k].y;
                s2x = Ms[n][0].x;
                (*t).x += temp * Ox * s2x;
                (*t).y += temp * Oy * s2x;
                /*
                 * s2y = Ms[n][0].y;
                 * (*t).x += temp * (Ox * s2x - Oy * s2y);
                 * (*t).y += temp * (Ox * s2y + Oy * s2x);
                 */
                /* compute m and -m in one pass for m > 0 */
                for(m=1;m <= n;m++){
                    /* m >= 0 */
                    km = k-m;
                    if(abs(km) <= jn){
                        temp = s2_c[n][m];
                        if(km >= 0){
                            Ox = O[jn][km].x;
                            Oy = O[jn][km].y;
                        } else {
                            km = -km;
                            Ox = O[jn][km].x;
                            Oy = -O[jn][km].y;
                        }
                        s2x = Ms[n][m].x;
                        s2y = Ms[n][m].y;
                        (*t).x += temp * (Ox * s2x - Oy * s2y);
                        (*t).y += temp * (Ox * s2y + Oy * s2x);
                    }
                    /* m <= 0 */
                    km = k+m;
                    if(km <= jn){
                        temp = s2_c[n][-m];
                        Ox = O[jn][km].x;
                        Oy = O[jn][km].y;
                        s2x = Ms[n][m].x;
                        s2y = -Ms[n][m].y;
                        (*t).x += temp * (Ox * s2x - Oy * s2y);
                        (*t).y += temp * (Ox * s2y + Oy * s2x);
                    }
                }
            }
        }
    }
#if (ISYM==1 && IBOT==10)
    Msym = CellTable[ParentCell].msym;
    Osym = CellTable[ChildCell].msym;
    Msym2 = CellTable[ParentCell].msym2;
    Osym2 = CellTable[ChildCell].msym2;
    Msym3 = CellTable[ParentCell].msym3;
    Osym3 = CellTable[ChildCell].msym3;

// symmetry in y

    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin((double) m * center.b);
    }
    /*
     * precomputed terms
     */
    r = 1.0;
    for(n=0;n <= Mp;n++){
        for(m=0;m <= n;m++){
            temp = r * MLegendre[n][m];
            Ms[n][m].x = temp * Mt1[m].x;
            Ms[n][m].y = temp * Mt1[m].y;
        }
        r *= center.r;
    }

    /*
     * now perform the shift for the symmetrical multipole
     */
    for(j=0;j <= Mp;j++){
        for(k=0;k <= j;k++){
            t = &Msym[j][k];
            s2_c = Ms2constant[j][k];
            for(n=0;n <= j;n++){
                jn = j-n;
                /* m == 0 */
                temp = s2_c[n][0];
                Ox = Osym[jn][k].x;
                Oy = Osym[jn][k].y;
                s2x = Ms[n][0].x;
                (*t).x += temp * Ox * s2x;
                (*t).y += temp * Oy * s2x;
                /*
                 * s2y = Ms[n][0].y;
                 * (*t).x += temp * (Ox * s2x - Oy * s2y);
                 * (*t).y += temp * (Ox * s2y + Oy * s2x);
                 */
                /* compute m and -m in one pass for m > 0 */
                for(m=1;m <= n;m++){
                    /* m >= 0 */
                    km = k-m;
                    if(abs(km) <= jn){
                        temp = s2_c[n][m];
                        if(km >= 0){
                            Ox = Osym[jn][km].x;
                            Oy = Osym[jn][km].y;
                        } else {
                            km = -km;
                            Ox = Osym[jn][km].x;
                            Oy = -Osym[jn][km].y;
                        }
                        s2x = Ms[n][m].x;
                        s2y = Ms[n][m].y;
                        (*t).x += temp * (Ox * s2x - Oy * s2y);
                        (*t).y += temp * (Ox * s2y + Oy * s2x);
                    }
                    /* m <= 0 */
                    km = k+m;
                    if(km <= jn){
                        temp = s2_c[n][-m];
                        Ox = Osym[jn][km].x;
                        Oy = Osym[jn][km].y;
                        s2x = Ms[n][m].x;
                        s2y = -Ms[n][m].y;
                        (*t).x += temp * (Ox * s2x - Oy * s2y);
                        (*t).y += temp * (Ox * s2y + Oy * s2x);
                    }
                }
            }
        }
    }
// symmetry on the flat bottom

    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin(-(double) m * center.b);
    }
    /*
     * precomputed terms
     */
    r = 1.0;
    for(n=0;n <= Mp;n++){
        for(m=0;m <= n;m++){
	  if(div(n+m,2).rem==0){
            temp = r * MLegendre[n][m];}
	  else{
            temp = - r * MLegendre[n][m];
	  }
            Ms[n][m].x = temp * Mt1[m].x;
            Ms[n][m].y = temp * Mt1[m].y;
        }
        r *= center.r;
    }

    /*
     * now perform the shift for the symmetrical multipole
     */
    for(j=0;j <= Mp;j++){
        for(k=0;k <= j;k++){
            t = &Msym2[j][k];
            s2_c = Ms2constant[j][k];
            for(n=0;n <= j;n++){
                jn = j-n;
                /* m == 0 */
                temp = s2_c[n][0];
                Ox = Osym2[jn][k].x;
                Oy = Osym2[jn][k].y;
                s2x = Ms[n][0].x;
                (*t).x += temp * Ox * s2x;
                (*t).y += temp * Oy * s2x;
                /*
                 * s2y = Ms[n][0].y;
                 * (*t).x += temp * (Ox * s2x - Oy * s2y);
                 * (*t).y += temp * (Ox * s2y + Oy * s2x);
                 */
                /* compute m and -m in one pass for m > 0 */
                for(m=1;m <= n;m++){
                    /* m >= 0 */
                    km = k-m;
                    if(abs(km) <= jn){
                        temp = s2_c[n][m];
                        if(km >= 0){
                            Ox = Osym2[jn][km].x;
                            Oy = Osym2[jn][km].y;
                        } else {
                            km = -km;
                            Ox = Osym2[jn][km].x;
                            Oy = -Osym2[jn][km].y;
                        }
                        s2x = Ms[n][m].x;
                        s2y = Ms[n][m].y;
                        (*t).x += temp * (Ox * s2x - Oy * s2y);
                        (*t).y += temp * (Ox * s2y + Oy * s2x);
                    }
                    /* m <= 0 */
                    km = k+m;
                    if(km <= jn){
                        temp = s2_c[n][-m];
                        Ox = Osym2[jn][km].x;
                        Oy = Osym2[jn][km].y;
                        s2x = Ms[n][m].x;
                        s2y = -Ms[n][m].y;
                        (*t).x += temp * (Ox * s2x - Oy * s2y);
                        (*t).y += temp * (Ox * s2y + Oy * s2x);
                    }
                }
            }
        }
    }
// symmetry in y+flat bottom

    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin((double) m * center.b);
    }
    /*
     * precomputed terms
     */
    r = 1.0;
    for(n=0;n <= Mp;n++){
        for(m=0;m <= n;m++){
	  if(div(n+m,2).rem==0){
            temp = r * MLegendre[n][m];}
	  else{
            temp = - r * MLegendre[n][m];
	  }
            Ms[n][m].x = temp * Mt1[m].x;
            Ms[n][m].y = temp * Mt1[m].y;
        }
        r *= center.r;
    }

    /*
     * now perform the shift for the symmetrical multipole
     */
    for(j=0;j <= Mp;j++){
        for(k=0;k <= j;k++){
            t = &Msym3[j][k];
            s2_c = Ms2constant[j][k];
            for(n=0;n <= j;n++){
                jn = j-n;
                /* m == 0 */
                temp = s2_c[n][0];
                Ox = Osym3[jn][k].x;
                Oy = Osym3[jn][k].y;
                s2x = Ms[n][0].x;
                (*t).x += temp * Ox * s2x;
                (*t).y += temp * Oy * s2x;
                /*
                 * s2y = Ms[n][0].y;
                 * (*t).x += temp * (Ox * s2x - Oy * s2y);
                 * (*t).y += temp * (Ox * s2y + Oy * s2x);
                 */
                /* compute m and -m in one pass for m > 0 */
                for(m=1;m <= n;m++){
                    /* m >= 0 */
                    km = k-m;
                    if(abs(km) <= jn){
                        temp = s2_c[n][m];
                        if(km >= 0){
                            Ox = Osym3[jn][km].x;
                            Oy = Osym3[jn][km].y;
                        } else {
                            km = -km;
                            Ox = Osym3[jn][km].x;
                            Oy = -Osym3[jn][km].y;
                        }
                        s2x = Ms[n][m].x;
                        s2y = Ms[n][m].y;
                        (*t).x += temp * (Ox * s2x - Oy * s2y);
                        (*t).y += temp * (Ox * s2y + Oy * s2x);
                    }
                    /* m <= 0 */
                    km = k+m;
                    if(km <= jn){
                        temp = s2_c[n][-m];
                        Ox = Osym3[jn][km].x;
                        Oy = Osym3[jn][km].y;
                        s2x = Ms[n][m].x;
                        s2y = -Ms[n][m].y;
                        (*t).x += temp * (Ox * s2x - Oy * s2y);
                        (*t).y += temp * (Ox * s2y + Oy * s2x);
                    }
                }
            }
        }
    }
#else
#if (ISYM==1 || IBOT==10)
    Msym = CellTable[ParentCell].msym;
    Osym = CellTable[ChildCell].msym;

#if (ISYM==1)
// symmetry in y

    /*
     * precompute e^(imb)
     */
    Mt1[0].x = 1.0;
    Mt1[0].y = 0.0;
    for(m=1;m <= Mp;m++){
        Mt1[m].x = cos((double) m * center.b);
        Mt1[m].y = sin((double) m * center.b);
    }
    /*
     * precomputed terms
     */
    r = 1.0;
    for(n=0;n <= Mp;n++){
        for(m=0;m <= n;m++){
            temp = r * MLegendre[n][m];
            Ms[n][m].x = temp * Mt1[m].x;
            Ms[n][m].y = temp * Mt1[m].y;
        }
        r *= center.r;
    }

#endif

#if (IBOT==10)
    /*
     * precomputed terms
     */
    r = 1.0;
    for(n=0;n <= Mp;n++){
        for(m=0;m <= n;m++){
	  if(div(n+m,2).rem==0){
            temp = r * MLegendre[n][m];}
	  else{
            temp = - r * MLegendre[n][m];
	  }
            Ms[n][m].x = temp * Mt1[m].x;
            Ms[n][m].y = temp * Mt1[m].y;
        }
        r *= center.r;
    }
#endif

    /*
     * now perform the shift for the symmetrical multipole
     */
    for(j=0;j <= Mp;j++){
        for(k=0;k <= j;k++){
            t = &Msym[j][k];
            s2_c = Ms2constant[j][k];
            for(n=0;n <= j;n++){
                jn = j-n;
                /* m == 0 */
                temp = s2_c[n][0];
                Ox = Osym[jn][k].x;
                Oy = Osym[jn][k].y;
                s2x = Ms[n][0].x;
                (*t).x += temp * Ox * s2x;
                (*t).y += temp * Oy * s2x;
                /*
                 * s2y = Ms[n][0].y;
                 * (*t).x += temp * (Ox * s2x - Oy * s2y);
                 * (*t).y += temp * (Ox * s2y + Oy * s2x);
                 */
                /* compute m and -m in one pass for m > 0 */
                for(m=1;m <= n;m++){
                    /* m >= 0 */
                    km = k-m;
                    if(abs(km) <= jn){
                        temp = s2_c[n][m];
                        if(km >= 0){
                            Ox = Osym[jn][km].x;
                            Oy = Osym[jn][km].y;
                        } else {
                            km = -km;
                            Ox = Osym[jn][km].x;
                            Oy = -Osym[jn][km].y;
                        }
                        s2x = Ms[n][m].x;
                        s2y = Ms[n][m].y;
                        (*t).x += temp * (Ox * s2x - Oy * s2y);
                        (*t).y += temp * (Ox * s2y + Oy * s2x);
                    }
                    /* m <= 0 */
                    km = k+m;
                    if(km <= jn){
                        temp = s2_c[n][-m];
                        Ox = Osym[jn][km].x;
                        Oy = Osym[jn][km].y;
                        s2x = Ms[n][m].x;
                        s2y = -Ms[n][m].y;
                        (*t).x += temp * (Ox * s2x - Oy * s2y);
                        (*t).y += temp * (Ox * s2y + Oy * s2x);
                    }
                }
            }
        }
    }
#endif
#endif

}
