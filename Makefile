#find C compiler for ifort
export LC_ALL=C

# compilers
#CC = gcc
CC = icc
FC = ifort

# flags
MANNINGF = -xHost -m64 -par-report2 -w  -O3 -autodouble -unroll -fpe0 -fp-model strict -static #-mcmodel=medium -shared_intel#-parallel -static
#MANNINGF = -xsse4.2 -par-report2 -w -arch:SSE4.1 -O3 -autodouble -unroll -i4 -fpe0 -mcmodel=large -shared-intel#-parallel -static 
#MANNINGC = -DSERIAL -xsse4.2 -O2 -unroll -mcmodel=large -shared-intel #-static
MANNINGC = -DSERIAL -m64 -xHost -O3 -unroll  -fp-model strict -static #-mcmodel=medium -shared_intel#-static
MCLARENF = -w -xW -O3 -autodouble -unroll -i4 -mtune=itanium2
MCLARENC = -O3 -DSERIAL -mtune=itanium2 -w  -unroll
XEF      = -O3 -autodouble -unroll -i4 -fpe0 -static 
XEC      = -O3 -DSERIAL -unroll
VAYUF      = -O3 -autodouble -unroll -i4 -fpe0 -static 
VAYUC      = -O3 -DSERIAL -unroll
#CFLAGS = -O2 -DSERIAL
CFLAGS = $(MANNINGC)
FFLAGS = $(MANNINGF)

#flags if debugging
#FFLAGS = -autodouble -diag-enable  sc-include -i4 -static -sox -O0  -ftrapuv -fpe0 -fp-model strict -debug all -g -traceback -check all -gen-interfaces -warn interfaces -f77rtl  -fixed -xHost -unroll 
#CFLAGS = -DSERIAL -static  -diag-enable  sc-include -static -sox -O0  -ftrapuv -debug all -g -traceback -warn -xHost -unroll 


# include
FINC = -I$(PWD)  -I$(PWD)/sources/include
CINC =

# sources
SRCF = sources/fortran/
SRCC = sources/c/

# archive
ARNWT = libnwt.a
AR = ar -rv

# bin
EXEC=N5A0.33X10D2

# objects
OBJF = objects/fortran/
OBJC = objects/c/
ALLOBJ =        $(OBJF)preprocess.o \
               $(OBJF)bem.o            \
               $(OBJF)bem2.o           \
               $(OBJF)bemfma.o         \
               $(OBJF)gage.o           \
               $(OBJF)gener.o          \
               $(OBJF)lateralbc.o      \
               $(OBJF)polyleg.o        \
               $(OBJF)postprocess.o    \
               $(OBJF)resol.o          \
               $(OBJF)shape.o          \
               $(OBJF)solver.o         \
               $(OBJF)solver2.o        \
               $(OBJF)solwave.o        \
               $(OBJF)update.o         \
               $(OBJC)PMTA_BEM.o       \
               $(OBJC)PMTAmultipole_setup.o  \
               $(OBJC)PMTAcell_setup.o \
               $(OBJC)PMTAfile.o       \
               $(OBJC)PMTAparticle.o   \
               $(OBJC)PMTAmultipole.o  \
               $(OBJC)PMTAlocal.o      \
               $(OBJC)PMTAforce.o      \
               $(OBJC)PMTAparallel.o   \
               $(OBJC)PMTAanalysis.o   \
               $(OBJC)PMTApfma.o       \
               $(OBJC)PMTAcoulomb.o    \
               $(OBJC)PMTAfft.o        \
               $(OBJC)PMTAbalance.o    \
               $(OBJC)PMTAinteract.o   \
               $(OBJC)PMTAnewstep.o    \


# targets
build:  $(ARNWT) $(OBJF)nwtank.o
	$(FC) -o $(EXEC) $(OBJF)nwtank.o $(ARNWT) $(FFLAGS) -Bstatic -lsvml -lm

$(ARNWT): $(ALLOBJ)
	$(AR) $(ARNWT) $(ALLOBJ)

$(OBJF)nwtank.o: $(SRCF)nwtank.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)nwtank.f -o $(OBJF)nwtank.o

$(OBJF)preprocess.o: $(SRCF)preprocess.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)preprocess.f -o $(OBJF)preprocess.o

$(OBJF)bem.o: $(SRCF)bem.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)bem.f -o $(OBJF)bem.o

$(OBJF)bem2.o: $(SRCF)bem2.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)bem2.f -o $(OBJF)bem2.o

$(OBJF)bemfma.o: $(SRCF)bemfma.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)bemfma.f -o $(OBJF)bemfma.o

$(OBJF)gage.o: $(SRCF)gage.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)gage.f -o $(OBJF)gage.o

$(OBJF)gener.o: $(SRCF)gener.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)gener.f -o $(OBJF)gener.o

$(OBJF)lateralbc.o: $(SRCF)lateralbc.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)lateralbc.f -o $(OBJF)lateralbc.o

$(OBJF)polyleg.o: $(SRCF)polyleg.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)polyleg.f -o $(OBJF)polyleg.o

$(OBJF)postprocess.o: $(SRCF)postprocess.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)postprocess.f -o $(OBJF)postprocess.o

$(OBJF)resol.o: $(SRCF)resol.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)resol.f -o $(OBJF)resol.o

$(OBJF)shape.o: $(SRCF)shape.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)shape.f -o $(OBJF)shape.o

$(OBJF)solver.o: $(SRCF)solver.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)solver.f -o $(OBJF)solver.o

$(OBJF)solver2.o: $(SRCF)solver2.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)solver2.f -o $(OBJF)solver2.o

$(OBJF)solwave.o: $(SRCF)solwave.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)solwave.f -o $(OBJF)solwave.o

$(OBJF)update.o: $(SRCF)update.f
	$(FC) $(FFLAGS) -c $(FINC) $(SRCF)update.f -o $(OBJF)update.o

$(OBJC)PMTA_BEM.o: $(SRCC)PMTA_BEM.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTA_BEM.c -o $(OBJC)PMTA_BEM.o

$(OBJC)PMTAmultipole_setup.o: $(SRCC)PMTAmultipole_setup.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAmultipole_setup.c -o $(OBJC)PMTAmultipole_setup.o

$(OBJC)PMTAcell_setup.o: $(SRCC)PMTAcell_setup.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAcell_setup.c -o $(OBJC)PMTAcell_setup.o

$(OBJC)PMTAfile.o: $(SRCC)PMTAfile.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAfile.c -o $(OBJC)PMTAfile.o

$(OBJC)PMTAparticle.o: $(SRCC)PMTAparticle.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAparticle.c -o $(OBJC)PMTAparticle.o

$(OBJC)PMTAmultipole.o: $(SRCC)PMTAmultipole.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAmultipole.c -o $(OBJC)PMTAmultipole.o

$(OBJC)PMTAlocal.o: $(SRCC)PMTAlocal.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAlocal.c -o $(OBJC)PMTAlocal.o

$(OBJC)PMTAforce.o: $(SRCC)PMTAforce.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAforce.c -o $(OBJC)PMTAforce.o

$(OBJC)PMTAparallel.o: $(SRCC)PMTAparallel.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAparallel.c -o $(OBJC)PMTAparallel.o

$(OBJC)PMTAanalysis.o: $(SRCC)PMTAanalysis.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAanalysis.c -o $(OBJC)PMTAanalysis.o

$(OBJC)PMTApfma.o: $(SRCC)PMTApfma.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTApfma.c -o $(OBJC)PMTApfma.o

$(OBJC)PMTAcoulomb.o: $(SRCC)PMTAcoulomb.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAcoulomb.c -o $(OBJC)PMTAcoulomb.o

$(OBJC)PMTAfft.o: $(SRCC)PMTAfft.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAfft.c -o $(OBJC)PMTAfft.o

$(OBJC)PMTAbalance.o: $(SRCC)PMTAbalance.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAbalance.c -o $(OBJC)PMTAbalance.o

$(OBJC)PMTAinteract.o: $(SRCC)PMTAinteract.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAinteract.c -o $(OBJC)PMTAinteract.o

$(OBJC)PMTAnewstep.o: $(SRCC)PMTAnewstep.c
	$(CC) $(CFLAGS) -c $(CINC) $(SRCC)PMTAnewstep.c -o $(OBJC)PMTAnewstep.o

clean:
	-rm -f $(EXEC) $(OBJF)*.o $(OBJC)*.o $(ARNWT)

output_clean:
	-rm -f output/*.dat fort.* output/vtk/*.vtk output/intpoints/*.dat output/intspeeds/*.dat

rebuild: clean build
