C     General Parameters

      integer nxm,nym,nzm,nomm,momm,mnod,mnodm,noim,nfmax,nhmax,
     .        mymax,ndm,ndm2,nintm,lsavef,ngam,mpm

      parameter (nxm=191,nym=5,nzm=13)

      parameter (nomm=2*(nxm*nym+nxm*nzm+nym*nzm),
     .           momm=nomm-4*(nxm+nym+nzm)+6)

      parameter (mnod=4,mnodm=mnod*mnod,
     .           noim=30000,
     .           nfmax=30,
     .           nhmax=30,
     .           mymax=100,
     .           ndm=5,ndm2=ndm*ndm,
     .           nintm=6,lsavef=nintm*nintm*(7+3*mnodm),
     .           ngam=30000)

      parameter (mpm=7)

C     moins de stockage si methode des images pour le fond
C
C      PARAMETER(NOMM=2*(NXM*NYM + NXM*NZM + NYM*NZM)-(NXM*NYM),
C     .          MOMM=2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))
C     .              -((NXM-1)*(NYM-1)))
C
C     moins de stockage si methode des images pour la symetrie en y
C
C      PARAMETER(NOMM=2*(NXM*NYM + NXM*NZM + NYM*NZM)-(NXM*NZM),
C     .          MOMM=2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))
C     .              -((NXM-1)*(NZM-1)))
C
C     moins de stockage si methode des images pour le fond+symetrie
C
C      PARAMETER(NOMM=2*(NXM*NYM + NXM*NZM + NYM*NZM)
C     .               -(NXM*NYM)-(NXM*NZM),
C     .          MOMM=2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))
C     .              -((NXM-1)*(NYM-1))-((NXM-1)*(NZM-1)))
C
C MPM=MP-1 !
