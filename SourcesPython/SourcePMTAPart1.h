
#if (ISYM==0 && IBOT!=10)

#define NUMPART  2*(NXM*NYM+NXM*NZM+NYM*NZM)
#define NUMELEM  (NUMPART-4*(NXM+NYM+NZM)+6)

#else

/*
 *    moins de stockage si methode des images pour le fond
 */
#if (IBOT==10 && ISYM==0)
#define NUMPART  (2*(NXM*NYM+NXM*NZM+NYM*NZM)-(NXM*NYM))
#define NUMELEM  (2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))-((NXM-1)*(NYM-1)))
#endif

/*
 *    moins de stockage si methode des images pour la symetrie en y
 */
#if (ISYM==1 && IBOT!=10)
#define NUMPART  (2*(NXM*NYM+NXM*NZM+NYM*NZM)-(NXM*NZM))
#define NUMELEM  (2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))-((NXM-1)*(NZM-1)))
#endif

/*
 *    moins de stockage si methode des images pour la symetrie en y+pour le fond
 */
#if (ISYM==1 && IBOT==10)
#define NUMPART  (2*(NXM*NYM+NXM*NZM+NYM*NZM)-(NXM*NYM)-(NXM*NZM))
#define NUMELEM  (2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))-((NXM-1)*(NYM-1))-((NXM-1)*(NZM-1)))
#endif

#endif

