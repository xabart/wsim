#define MNODM    MNOD*MNOD
/*
 * PMTA.h
 *
 * Copyright (c) 1994 Duke University
 * All Rights Reserved.
 *
 * Version 4.0, April 20, 1994
 *
 */

/*
 * RCS Info: $Id: PMTA.h,v 1.4 1995/01/17 17:13:56 welliott Exp $
 *
 * Revision History:
 *
 * $Log: PMTA.h,v $
 * Revision 1.4  1995/01/17  17:13:56  welliott
 * Commented out random.h for KSR version (KSR has changed include files)
 *
 * Revision 1.3  1994/12/02  18:06:09  wrankin
 * added CLK_TCK definition and include <sys/times> for timing fixes
 *
 * Revision 1.2  1994/11/11  19:34:20  lambert
 * min(a,b) was system macro, so redefining it causes warnings with
 * some compilers.  Redefined to Min(a,b)
 *
 * No need to have Outliers as global variable, removed from PMTA.h.
 *
 * iTheta made global when library interface cleaned up.
 *
 * ParallelSetup and Parallel were incorrectly defined as returning
 * void POINTERS, so prototype fixed.  A couple other prototypes
 * cleaned up.
 *
 * Revision 1.1  1994/10/08  04:34:10  wrankin
 * Initial revision
 *
 * Modified by C.Fochesato, CMLA, 2003 : Bem structure definition
 */



#ifndef PMTA_HDR
#define PMTA_HDR

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <stdlib.h>
#include <values.h>             /* for MAXINT */
#ifndef SERIAL
/* #include <random.h>*/ /* KSR isn't the same */
#include <pthread.h>
#include <ksr/sqrt.h>
#include <ksr/ksr_timers.h>
#include <ksr/pmon.h>
#else
#include <sys/time.h>           /* for timing */
#include <sys/times.h>
#include <sys/resource.h>       /* for timing */
#endif

/*
 * Constant definitions
 */

#define TRUE            1
#define FALSE           0

#define LEVELS_MAX      9

#define PTABLE_INC      5000

#define ILIST_INC       16

#define RESULTS_MAX     20

#define DIR_X           1
#define DIR_Y           2
#define DIR_Z           4

