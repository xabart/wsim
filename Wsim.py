##############################################################################################
##############################################################################################
#
#	Parameter files set up
#
#	The script construct all the parameter files where they are needed
#	All the parameters are set in the main, at the end of this file
#
#	X.Barthelemy, 01/2010
#
##############################################################################################
##############################################################################################
import os
import re
import os.path
import random
from datetime import datetime
try:
	import numpy 
	import scipy 
except:	print "No numpy or scipy"
try:
	# These symbols might not exist, depending on the underlying hardware
	from numpy import float128, complex256
#	Typecodes[float128] = ('128 bit float', TFloat)
#	Typecodes[complex256] = ('256 bit complex', Any)
except  ImportError:
	print "Pas de Quadruple Precision"	
	pass


##############################################################################################

def CreateFmdata(CUBE):
	FmdataFile= open('input/fmadata.dat2','w')
#	FmdataFile= open('input/fmdata.dat3','w')

#	CUBE=  11.0
	LEVELS=4
	IFFT=  0
	MP=    8
	IPFMA= 1
	IEPMTA=0
	THETA= 0.
	NPROC= 2
	IDIRECT=0
	IBALANCED=0
	IVERBOSE=0
	IPMON= 0
	
	lign=str(CUBE)+' '+str(LEVELS)+' '+str(IFFT)+' '+str(MP)+'        CUBE,LEVELS,IFFT,MP\n'
	FmdataFile.write(lign)
	lign=str(IPFMA)+' '+str(IEPMTA)+' '+str(THETA)+' '+str(NPROC)+' '+'         IPFMA,IEPMTA,THETA,NPROC\n'
	FmdataFile.write(lign)
	lign=str(IDIRECT)+' '+str(IBALANCED)+' '+str(IVERBOSE)+' '+str(IPMON)+'         IDIRECT,IBALANCED,IVERBOSE,IPMON \n'
	FmdataFile.write(lign)
	FmdataFile.write('\n')
	FmdataFile.write('description parametres : cf PMTA')
 	FmdataFile.close
	return
##############################################################################################

def CreateInput(NXM,NYM,NZM,ZL0,W0,H0,X0,D0,TSTART,TMAX,ISTART,IBOT,SLOPE,ZK,IBCOND1,IBCOND2,IBCOND3,IBCOND4,IBCOND5,IBCOND6,IPTYP,V0H,OMEGA,NHT,TOLMAX,XLDAMP,INFIE,NOI,NXI,NYI,NZI,NGA,NFR,TDAMP,DT,ILOOP0,IGRID,XMIN,DXMIN,DYMIN,IPRINT,ISYM,DE1,DE2):
	InputFile= open('input/input.dat2','w')
#	InputFile= open('input/input.dat3','w')
	
#	ILOOP0=0
#	MX=40
#	MY=4
#	MZ=10
	NINTR=6
#	NSUBC=2
	NSUBC=4
	ALMAXB=0.7
	ALMAXI=0.7
#	ZL0=5.0
#	W0=1.0
#	H0=1.0
#	X0=0.0
#	D0=10.0
#	IBOT=1
#	SLOPE=0.0
#	ZK=0.0
	
#	DT=0.05
#	TSTART=0.0
#	TMAX=21.0
	RHO=1.0
	CPRESS=0.0
	CPREST=0.0
	GE=1.0
#	TDAMP=4.202
	TDOWN=60.0
#	ISTART=0
#	IGRID=0
#	IBCOND1=1
#	IBCOND2=2
#	IBCOND3=0
#	IBCOND4=0
#	IBCOND5=0
#	IBCOND6=0
	ILMAX=8000
#	IPTYP=1
	NBS=8
#	IPRINT=30
	TPRINT=0.0	

#	DE1=1.0
#	DE2=1.0
#	OMEGA=2.990
#	V0H=0.0015
#	INFIE=0
#	NOI=125
#	NFR=1
#	NHT=1
#	TOLMAX=0.02
#	XLDAMP=0.
	DNUO=0.
	TRESH=0.
	SUTENS=0.
	NPOLY=2
#	NXI=5
#	NYI=5
#	NZI=-5
#	NGA=24
#	XMIN=17
#	DXMIN=0.01
#	DYMIN=0.02
#	ISYM=0
	IPOST=2
	IVECT=2

# Interval numbers = Nodes number - 1
	MX=NXM -1
	MY=NYM -1
	MZ=NZM -1
	
	
	lign=str(ILOOP0)+' '+str(MX)+' '+str(MY)+' '+str(MZ)+' '+str(NINTR)+' '+str(NSUBC)+' '+str(ALMAXB)+' '+str(ALMAXI)+'         ILOOP0,MX,MY,MZ,NINTR,NSUBC,ALMAXB,ALMAXI\n'
	InputFile.write(lign)
	lign=str(ZL0)+' '+str(W0)+' '+str(H0)+' '+str(X0)+' '+str(D0)+' '+str(IBOT)+' '+str(SLOPE)+' '+str(ZK)+'      ZL0,W0,H0,X0,D0,IBOT,SLOPE,ZK\n'
	InputFile.write(lign)
	
	
	lign=str(IBCOND1)+' '+str(IBCOND2)+' '+str(IBCOND3)+' '+str(IBCOND4)+' '+str(IBCOND5)+' '+str(IBCOND6)+'                                       (IBCOND(K),K=1,6)\n'
	InputFile.write(lign)
	lign=str(ILMAX)+' '+str(IPTYP)+' '+str(NBS)+' '+str(IPRINT)+' '+str(TPRINT)+'                                    ILMAX,IPTYP,NBS,IPRINT,TPRINT\n'
	InputFile.write(lign)
	lign=str(DT)+' '+str(TSTART)+' '+str(TMAX)+' '+str(RHO)+' '+str(CPRESS)+' '+str(CPREST)+' '+str(GE)+' '+str(TDAMP)+' '+str(TDOWN)+ '               DT,TSTART,TMAX,RHO,CPRESS,CPREST,GE,TDAMP,TDOWN\n'
	InputFile.write(lign)
 	lign=str(DE1)+' '+str(DE2)+' '+str(OMEGA)+' '+str(V0H)+' '+str(INFIE)+' '+str(NOI)+' '+str(NFR)+' '+str(NHT)+' '+str(TOLMAX)+ '                   DE1,DE2,OMEGA,V0H,INFIE,NOI,NFR,NHT,TOLMAX\n'
	InputFile.write(lign)
	lign=str(XLDAMP)+' '+str(DNUO)+' '+str(TRESH)+' '+str(SUTENS)+' '+str(NPOLY)+' '+str(NXI)+' '+str(NYI)+' '+str(NZI)+' '+str(NGA)+' 			   XLDAMP,DNUO,TRESH,SUTENS,NPOLY,NXI,NYI,NZI,NGA\n'
	InputFile.write(lign)
 	lign=str(ISYM)+' '+str(IPOST)+' '+str(IVECT)+'						 ISYM,IPOST,IVECT\n'
	InputFile.write(lign)
#	data='''0 2 2						 ISYM,IPOST,IVECT
#'''
#	InputFile.write(data)
	lign=str(ISTART)+' '+str(IGRID)+'						 ISTART,IGRID\n'
	InputFile.write(lign)
	lign=str(XMIN)+' '+str(DXMIN)+' '+str(DYMIN)+'					 XMIN, DXMIN, DYMIN\n'
	InputFile.write(lign)
	data='''2.80260 0.085
2.83800 0.085
2.88110 0.085
2.89710 0.085
2.89710 0.085
2.91520 0.085
2	NOI=125
	NFR=1
#	NHT=1
#	TOLMAX=0.02
#	XLDAMP=0.
	DNUO=0.
	TRESH=0.
	SUTENS=0.
	NPOLY=2
	NXI=5
	NYI=5
	NZI=-5
	NGA=24
.93880 0.085
2.97420 0.085
3.01730 0.085
3.03330 0.085
3.03330 0.085
3.05140 0.085
3.13240 0.085
3.16780 0.085
3.21090 0.085
3.21980 0.085
3.22690 0.085
3.22690 0.085
3.24500 0.085
3.25520 0.085
3.29830 0.085
3.31430 0.085
3.31430 0.085
3.33240 0.085

 
	'''
	InputFile.write(data)
 	InputFile.close
	return
	
##############################################################################################
	
def CreatePaddle(DISTF,TFOC,ALPHAF,WMIDTH,IFREQ,DELFR,Xconv,Yconv,Iconv,Icurved,INN):
	PaddleFile= open('input/paddle.dat2','w')
#	PaddleFile= open('input/paddle.dat3','w')

#	DISTF=3.6
#	TFOC=0.0
#	ALPHAF=45.0
#	WMIDTH=0.0
#	IFREQ=0
#	DELFR=0.5
	
	lign=str(DISTF)+' '+str(TFOC)+' '+str(ALPHAF)+' '+str(WMIDTH)+' '+str(IFREQ)+' '+str(DELFR)+'      DISTF,TFOC,ALPHAF,WMIDTH,IFREQ,DELFR\n'
	PaddleFile.write(lign)
	lign=str(Xconv)+' '+str(Yconv)+' '+str(Iconv)+' '+str(Icurved)+' '+str(INN)+'\n'
	PaddleFile.write(lign)
	
	PaddleFile.write('0.\n')
 	PaddleFile.close
	return

##############################################################################################

def CreateParam(NXM,NYM,NZM):
	ParamFile= open('input/param.dat2','w')
#	ParamFile= open('input/param.dat3','w')
	
#	NXM=41
#	NYM=5
#	NZM=11

	MNOD=4
	NOIM=30000
	NFMAX=30
#	ZL0=5.0
#	W0=1.0
#	H0=1.0
#	X0=0.0
#	D0=10.0
	NHMAX=30
#	MYMAX=100
	MYMAX=NYM+1
	NDM=5
	NINTM=6
	NGAM=30000
	MPM=7
	
	data= '''#   General Parameters
#-------------------------------------------------------------------------------
#   NXM,NYM,NZM 
'''

	ParamFile.write(data)
	
	lign=str(NXM)+' '+str(NYM)+' '+str(NZM)	
	ParamFile.write(lign)
	data= '''
#   MNOD,NOIM,NFMAX,NHMAX,MYMAX,NDM,NINTM,NGAM 
'''
	lign=str(MNOD)+' '+str(NOIM)+' '+str(NFMAX)+str(NHMAX)+' '+str(MYMAX)+' '+str(NDM)+' '+str(NINTM)+' '+str(NGAM)+'\n'
	ParamFile.write(lign)
	ParamFile.write('''#   MPM\n''')
	ParamFile.write(str(MPM))

 	ParamFile.close
	return


##############################################################################################
def CreateParamInc(NXM,NYM,NZM):
	ParamIncFile= open('param.inc','w')
#	ParamIncFile= open('param.inc2','w')
	
#	NXM=41
#	NYM=5
#	NZM=11
	MNOD=4
	NOIM=30000
	NFMAX=30
	NHMAX=30
#	MYMAX=100
	MYMAX=NYM+1
	NDM=5
	NINTM=6
	NGAM=30000
	MPM=7
	
	data='''C     General Parameters

      integer nxm,nym,nzm,nomm,momm,mnod,mnodm,noim,nfmax,nhmax,
     .        mymax,ndm,ndm2,nintm,lsavef,ngam,mpm

      integer NSIZE
'''     
	ParamIncFile.write(data)
	lign='      parameter (nxm='+str(NXM)+',nym='+str(NYM)+',nzm='+str(NZM)+')\n'
	ParamIncFile.write(lign)
	data='''
      parameter (nomm=2*(nxm*nym+nxm*nzm+nym*nzm),
     .           momm=nomm-4*(nxm+nym+nzm)+6)\n'''
	ParamIncFile.write(data)
	ParamIncFile.write('\n')
	data='''
      parameter (NSIZE=2*(nxm*nym+nxm*nzm+nym*nzm))\n'''
	ParamIncFile.write(data)
	ParamIncFile.write('\n')

	lign='      parameter (mnod='+str(MNOD)+',mnodm=mnod*mnod,\n'
	ParamIncFile.write(lign)
	lign='     .           noim='+str(NOIM)+',\n'
	ParamIncFile.write(lign)
	lign='     .           nfmax='+str(NFMAX)+',\n'
	ParamIncFile.write(lign)
	lign='     .           nhmax='+str(NHMAX)+',\n'
	ParamIncFile.write(lign)
	lign='     .           mymax='+str(MYMAX)+',\n'
	ParamIncFile.write(lign)
	lign='     .           ndm='+str(NDM)+',ndm2=ndm*ndm,\n'
	ParamIncFile.write(lign)
	lign='     .           nintm='+str(NINTM)+',lsavef=nintm*nintm*(7+3*mnodm),\n'
	ParamIncFile.write(lign)
	lign='     .           ngam='+str(NGAM)+')\n'
	ParamIncFile.write(lign)
	ParamIncFile.write('\n')
	lign='      parameter (mpm='+str(MPM)+')\n'
	ParamIncFile.write(lign)
	
	data='''
C     moins de stockage si methode des images pour le fond
C
C      PARAMETER(NOMM=2*(NXM*NYM + NXM*NZM + NYM*NZM)-(NXM*NYM),
C     .          MOMM=2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))
C     .              -((NXM-1)*(NYM-1)))
C
C     moins de stockage si methode des images pour la symetrie en y
C
C      PARAMETER(NOMM=2*(NXM*NYM + NXM*NZM + NYM*NZM)-(NXM*NZM),
C     .          MOMM=2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))
C     .              -((NXM-1)*(NZM-1)))
C
C     moins de stockage si methode des images pour le fond+symetrie
C
C      PARAMETER(NOMM=2*(NXM*NYM + NXM*NZM + NYM*NZM)
C     .               -(NXM*NYM)-(NXM*NZM),
C     .          MOMM=2*((NXM-1)*(NYM-1)+(NXM-1)*(NZM-1)+(NYM-1)*(NZM-1))
C     .              -((NXM-1)*(NYM-1))-((NXM-1)*(NZM-1)))
C
C MPM=MP-1 !
'''
	ParamIncFile.write(data)
	
 	ParamIncFile.close
	return

########################NXM,NYM,NZM######################################################################
def CreatePMTA(NXM,NYM,NZM,CUBE_CTR_X,CUBE_CTR_Y,CUBE_CTR_Z,IBOT,ISYM):
	PMTAFile= open('sources/c/include/PMTA.h','w')
#	PMTAFile= open('sources/c/include/PMTA.h2','w')
	PMTAPart1=open('SourcesPython/SourcePMTAPart1.h','r')
	PMTAPart2=open('SourcesPython/SourcePMTAPart2.h','r')
	PMTAPart3=open('SourcesPython/SourcePMTAPart3.h','r')
	
#	NXM=41
#	NYM=5
#	NZM=11
#	ISYM=0
#	IBOT=1
	MNOD=4
#	CUBE_CTR_X=5.5
#	CUBE_CTR_Y=0.5
#	CUBE_CTR_Z=0.0
	
	
	 
	data='''/*
 * Prescribed values for BEM (= param.inc)
 *
 * C.Fochesato, CMLA, 2003
 */
'''
	PMTAFile.write(data)
	lign='#define NXM      '+str(NXM)+'\n'
	PMTAFile.write(lign)
	lign='#define NYM      '+str(NYM)+'\n'
	PMTAFile.write(lign)
	lign='#define NZM      '+str(NZM)+'\n'
	PMTAFile.write(lign)
	PMTAFile.write('\n')
	lign='#define ISYM     '+str(ISYM)+'\n'
	PMTAFile.write(lign)
	lign='#define IBOT     '+str(IBOT)+'\n'
	PMTAFile.write(lign)
	
	
	data=PMTAPart1.read()
	PMTAFile.write(data)
	
	lign='#define MNOD      '+str(MNOD)+'\n'
	PMTAFile.write(lign)
	
	data=PMTAPart2.read()
	PMTAFile.write(data)

	lign='#define CUBE_CTR_X      '+str(CUBE_CTR_X)+'\n' 
	PMTAFile.write(lign)
	lign='#define CUBE_CTR_Y      '+str(CUBE_CTR_Y)+'\n'
	PMTAFile.write(lign)
	lign='#define CUBE_CTR_Z      '+str(CUBE_CTR_Z)+'\n'
	PMTAFile.write(lign)


	data=PMTAPart3.read()
	PMTAFile.write(data)

	PMTAPart1.close
	PMTAPart2.close
	PMTAPart3.close
	PMTAFile.close
	return
	
##############################################################################################
	
def CreateSinewaves(NHT,SineAmplitude,SinePulsation,SinePhase):
	SineFile= open('input/sinepaddles.dat','w')

	for ii in range(NHT):
#		print 'ii',ii
		lign=str(SineAmplitude[ii])+' '+str(SinePulsation[ii])+' '+str(SinePhase[ii])+'      !Amplitude,Omega,Phase\n'
		SineFile.write(lign)
                     
 	SineFile.close
	return

def CreateSuffixFile():
	SuffixFile= open('suffix.outputfile','w')
	date=datetime.today()
	date2=date.isoformat('H')
#	print date2
	SuffixFile.write('"'+str(date2)[:16]+'"')
	SuffixFile.close
	return
	
##############################################################################################
##############################################################################################
#	Main
##############################################################################################
##############################################################################################

############################
# Physical Domain
############################
#englobing Cube size
CUBE=  27.0
#CUBE=  51.0
# Cube center coordinates
CUBE_CTR_X=13.5
#CUBE_CTR_X=25.5
#CUBE_CTR_Y=0.5
CUBE_CTR_Y=0.
CUBE_CTR_Z=0.0



############################
# Numerical Domain
############################
# Nodes numbers in X,Y,Z
NXM=191
NXM=151
#NXM=383
#NXM=701

NYM=5
#NYM=11
#NYM=105
NYM=69

NZM=13
#NZM=9
NZM=17

# Dimensions of the tank
#     ZL0      : Numerical wave tank length 
#     W0       : Numerical wave tank width
#     H0       : Numerical wave tank ocean depth
#     X0       : Numerical wave tank leftward x-coord.
#     D0       : Numerical wave tank length of h=H0 part

#ZL0=50.0
ZL0=25.0
ZL0=20.0

#Wavebasin 2D
W0=0.5
#Wavebasin 7.75/0.55=14.09
W0=14.

H0=2.
#H0=1.
#H0=0.75
#H0=0.5
#H0=0.4

X0=0.0
#D0=10.0
D0=ZL0
DE1=H0
DE2=H0
# Bottom geometry
#     IBOT  = 1 Flat bottom z=-H0 (image method is used)
#           = 2 Sloping bottom s=SLOPE in x-dir, starting at x=D0
#           = 3 Sloping bottom s=SLOPE in x-dir, starting at x=D0
#             with sech^2(ZK*y) taper scaled to z=-H0 at +-W0/2
#           = 4 Sloping bottom s=SLOPE in x-dir, starting at x=D0
#             with sech^2(ZK*y) to y = +-W0/2
#           = 5 Sloping bottom s=SLOPE in x-dir, starting at x=D0
#	      till x=L0-ZL2
#             with sech^2(ZK*y) to y = +-W0/2 and sech^2(zk*xi) for
#             underwater landslide
#           = 6 same as 5 but symmetry for y=0
#           = 7 CK: Flat bottom z=-H0 + heavyside function
#           = 10 flat bottom at z=-H0, image method => both NOM and MOM
#             are adjusted since boundary 6 is skipped in generation of data

IBOT=1
SLOPE=0.0
ZK=0.0

# Symetry of calculus domain  y=0 
ISYM=0

############################
# Time Domain
############################

TSTART=0.0
#TMAX=1.50 
TMAX=140.00 

# Restarting a run
# 0=no 1=yes (modify TSTART if yes) 
ISTART=0
#ISTART=1
#if restarting, number of the last step time, i.e. the last 'vtk' file number.
ILOOP0=0
# On a restart, regrid? (0=no, 1=yes,2=yes with refinement with xmin, dxmin,dymin)
IGRID=0
# On a restart, IGRID0=2 with refinement around xmin, dxmin,dymin)
# XMIN is the location of the minimum spatial step DXMIN
XMIN=17
DXMIN=0.01
DYMIN=0.02
# first time step or last time step for a restart
DT=0.001 

# write a restart file every IPRINT iter
IPRINT=20

############################
# Boundary conditions
############################


#
#     IBCOND(K) : BC for boundary K : 1 Dirichlet
#                                     0 Impermeable Neuman
#                                     2 Wavemaker with IPTYP
#                                     3 AP
#                                     4 slideing bottom

IBCOND1=1
IBCOND2=2
IBCOND3=0
IBCOND4=3
IBCOND5=0
IBCOND6=0
#     IPTYP = 0 Exact solitary wave H=V0H,eps=TOLMAX
#           = 1 Periodic sine wave TUNE=V0H,om=OMEGA,NHT=0
#           = 1 Sum of n period. sine waves TUNE=V0H,NHT=n,
#             READ paddle.dat2 (a,om,phase)
#           = 2 Solitary wave by pist. wm. H=V0H
#           = 3 Cnoidal wave by pist. wm. H=V0H,om=OMEGA
#           = 4 Zero-mass flux SFW H=V0H,om=OMEGA
#           = 8 wave focusing with flap wavemaker
#           = 7 wave focusing with flap wavemaker, ECN wavemaker geometry
#           = 9 Underwater landslide with IBOT = 5,6 (symmetry)
#           = 10 Underwater slump with IBOT = 5,6 (symmetry)
IPTYP=1
IPTYP=7

#     TDAMP  : Damping Nb. of periods for initial moption and IPTYP=1,4
#     OMEGA,V0H : wave parameters, IPTYP=1,2,3,4
#     NHT    : Nb. of sine components for IPTYP=1
#     NFR    : number of frequency components for wavemaker

TDAMP=4.202
OMEGA=2.990
V0H=0.0015
NHT=0
TOLMAX=0.02
XLDAMP=0.
NFR=1

V0H=0.01
NHT=1
OMEGA=8.2
OMEGA=1.78

#in case of sinepaddles,  iptyp=1, nht paddles <>0
AMPL=-0.025*0.339
V0H=AMPL
SineAmplitude=[AMPL,AMPL,AMPL]
SinePulsation=[OMEGA,OMEGA,OMEGA]
SinePhase=[0.,0.,0.]

############################
# Paddle Motion
############################

DISTF=3.6
TFOC=0.0
ALPHAF=45.0
WMIDTH=0.0
IFREQ=0
DELFR=0.5


#Xconv= 10./0.6
Xconv= 16.66
#Xconv= 10./0.55
Xconv= 18.18
Yconv= 0.
Iconv=1
Icurved=0

#Type3 N paddle
INN=5

############################
# Interior Points
############################
#     NXI,NYI,NZI : Number of internal points in each dir to be automatically generated,
#                   with NOI=NXI*NYI*NZI. If 0,0,0 read NOI points from intern.dat
#     NGA : Number of internal gages of coordinates (x,y) to be read from gage.dat.
#		if 0 auto create NXI*NYI gauges
#           If 0 and INFIE.EQ.1, generate NGA=NXI*NYI (x,y) coords identical to internal points
# 1st sol: number of points specified by the user :NOI=NXI*NYI*NZI if NZI>0 mobile points in z else fixed points
#	

INFIE=0
#NOI=125
#NXI=NXM
NXI=3
NYI=3
NZI=-3
NOI=abs(NXI*NYI*NZI)
#NGA=24
NGA=0



##############################################################################################
try:
       os.mkdir(output)
       os.mkdir(output/vtk)
       os.mkdir(output/intspeeds)
       os.mkdir(output/intpoints)
except:
       	pass
CreateSuffixFile()	
CreateFmdata(CUBE)	
CreateInput(NXM,NYM,NZM,ZL0,W0,H0,X0,D0,TSTART,TMAX,ISTART,IBOT,SLOPE,ZK,IBCOND1,IBCOND2,IBCOND3,IBCOND4,IBCOND5,IBCOND6,IPTYP,V0H,OMEGA,NHT,TOLMAX,XLDAMP,INFIE,NOI,NXI,NYI,NZI,NGA,NFR,TDAMP,DT,ILOOP0,IGRID,XMIN,DXMIN,DYMIN,IPRINT,ISYM,DE1,DE2)
CreatePaddle(DISTF,TFOC,ALPHAF,WMIDTH,IFREQ,DELFR,Xconv,Yconv,Iconv,Icurved,INN)
CreateParam(NXM,NYM,NZM)
CreateParamInc(NXM,NYM,NZM)
CreatePMTA(NXM,NYM,NZM,CUBE_CTR_X,CUBE_CTR_Y,CUBE_CTR_Z,IBOT,ISYM)
CreateSinewaves(NHT,SineAmplitude,SinePulsation,SinePhase)
